<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress Dan
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'theone');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K1LmZsMl<n| l=Xry4tJ]+k!8+-.MV>jKopH1_.XEi.]oYFw:sW>#q)eCL;t4l1 ');
define('SECURE_AUTH_KEY',  '|E_$ ,1&{NwjuV0%^q|%Z*&.[NCKF[FZ`dy=zMC>Bee$E!-i)5rJq`@H7=QN>L>A');
define('LOGGED_IN_KEY',    'ID}Wh*^N{S8ZsU+#;btp^bX*816HRRN6(#;5joWd=-4]xcnheqaOP|P.D7Ut*aVQ');
define('NONCE_KEY',        '@8wPppHKsv(YX/lT2[(*jP9j&>5~9vgO?R@D7-lK,w=@YX--~DBA53i&fy3j0`1y');
define('AUTH_SALT',        'sTS/Z#HGj|d<FQOKL-,[J8?6zA[}kI.j49|dasfYgOAE6<tEaF@/G6dM4#LT;}0N');
define('SECURE_AUTH_SALT', 'V>kZ%gHIqk^t`f4:<<2p%+GFK[MeBeVBx?;t4BdwmIC+mCk23z/jNu}67lG`lea9');
define('LOGGED_IN_SALT',   '3.`oQ&%FX+Z!0GN[a5$pjY 4f:u~Tko[+}b>!L@a}XiW1Ln6KOiR2lu+9~(/{f%1');
define('NONCE_SALT',       'R1QAg%}>Cj,r12-jm!@*CH7fi.pUsjKy$+(V$[-d=s_%2k~$WwaQF,3u5=,Wt sH');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'to_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
