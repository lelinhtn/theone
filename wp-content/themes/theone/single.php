<?php 
/**
* single.php
* The main post loop in THE ONE
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header();
global $theone;

$blog_layout = isset( $theone['opt-blog-layout'] ) ? $theone['opt-blog-layout'] : '3';
$blog_layout_style = isset( $theone['opt-blog-layout-style'] ) ? $theone['opt-blog-layout-style'] : 'list';
$blog_metas_options = isset( $theone['opt-blog-metas'] ) ? $theone['opt-blog-metas']: array();

the_post(); 
    
$format = get_post_format(); 
if( false === $format ):
    $format = 'standard';
endif;

$format = ( $format == 'image' || $format == 'gallery' ) ? 'image' : $format;
$format = ( $format == 'aside' || $format == 'chat' ) ? 'standard' : $format;
    
ts_update_post_view_count( get_the_ID() );

if ( empty($_ts_post_format)) {
    $_ts_post_format_class = 'ts_post_format_empty';
} else {
    $_ts_post_format_class = null;
}

if ( isset( $_REQUEST['sidebar'] ) ) {
    
    switch ( trim( strtolower( $_REQUEST['sidebar'] ) ) ) {
        case 'left':
            $_REQUEST['sidebar'] = '1';
            break;
        case 'right':
            $_REQUEST['sidebar'] = '2';
            break;
    }
    
    $blog_layout = $_REQUEST['sidebar'];
    if ( trim( $blog_layout ) != '1' && trim( $blog_layout ) != '2' ) {
        $blog_layout = '3';
    }
}


?>

    <!--Article-->

    <!-- Content -->
    <div id="content" class="site-content">
        <div class="container">
            <div class="single-post-row row">
                <?php switch ( $blog_layout ) {
                    case '1': // Sidebar left
                ?>
                <?php get_sidebar('left' ); ?>
                <article class="col-md-9 col-sm-8">
                    <div id="main-content" class="main-content">
                        <section class="section section-blog blog-standard single-post-section">
                            <div class="ts-latestnews">
                                <?php $_sticky_class = is_sticky() ? 'sticky' : null; ?>
                                <div <?php post_class("$_ts_post_format_class $_sticky_class item-post"); ?>>
                                    <?php get_template_part( 'post-formats/post', $format ); ?>
                                    <div class="info-item-post">
                                        <div class="info-post">
                                            <h3><?php the_title(); ?></h3>
                                            <?php get_template_part( 'content-parts/blog', 'metas' ); ?>
                                        </div>
                                        <div class="content-post">
                                            <?php the_content(); ?>
                                        </div> 
                                        <?php if( in_array( 'tags', $blog_metas_options ) ) :  ?>
                                            <?php
                                                $posttags = get_the_tags();
                                                if ( $posttags ) {
                                                    $tag_val = array();
                                                    foreach( $posttags as $tag ):
                                                        $tag_link = get_tag_link( $tag->term_id );
                                                        $tag_val[] = '<a href="'.$tag_link.'">' . $tag->name . '</a>'; 
                                                    endforeach;
                                            ?>
                                                <div class="tags">
                                                    <h5><?php _e( 'Tags', 'themestudio' ); ?></h5>
                                                    <?php echo implode(' ', $tag_val); ?>
                                                </div>
                                            <?php } ?>
                                        <?php endif; ?>
                                        
                                        <?php get_template_part( 'content-parts/blog', 'social' ); ?>
                                    </div><!-- /.info-item-post -->    
                                </div><!-- /.item-post -->
                                
                                <?php get_template_part( 'content-parts/author', 'bio' ); ?>
                                
                                <?php if ( comments_open() ): ?>
                                
                                    <?php comments_template(); ?>
                                
                                <?php endif; ?>
                                
                            </div><!-- /.ts-latestnews -->
                        </section>
                    </div><!-- /#main-content -->
                    
                </article>
                <?php
                    break;
                    case '3': // No sidebar
                ?>
                <article class="col-md-12 col-sm-12">
                    <div id="main-content" class="main-content">
                        <section class="section section-blog blog-standard single-post-section">
                            <div class="ts-latestnews">
                                <?php $_sticky_class = is_sticky() ? 'sticky' : null; ?>
                                <div <?php post_class("$_ts_post_format_class $_sticky_class item-post"); ?>>
                                    <?php get_template_part( 'post-formats/post', $format ); ?>
                                    <div class="info-item-post">
                                        <div class="info-post">
                                            <h3><?php the_title(); ?></h3>
                                            <?php get_template_part( 'content-parts/blog', 'metas' ); ?>
                                        </div>
                                        <div class="content-post">
                                            <?php the_content(); ?>
                                        </div>
                                        <?php if( in_array( 'tags', $blog_metas_options ) ) :  ?>
                                            <?php
                                                $posttags = get_the_tags();
                                                if ( $posttags ) {
                                                    $tag_val = array();
                                                    foreach( $posttags as $tag ):
                                                        $tag_link = get_tag_link( $tag->term_id );
                                                        $tag_val[] = '<a href="'.$tag_link.'">' . $tag->name . '</a>'; 
                                                    endforeach;
                                            ?>
                                                <div class="tags">
                                                    <h5><?php _e( 'Tags', 'themestudio' ); ?></h5>
                                                    <?php echo implode(' ', $tag_val); ?>
                                                </div>
                                            <?php } ?>
                                        <?php endif; ?>
                                        
                                        <?php get_template_part( 'content-parts/blog', 'social' ); ?>
                                    </div><!-- /.info-item-post -->
                                </div><!-- /.item-post -->
                                
                                <?php get_template_part( 'content-parts/author', 'bio' ); ?>
                                
                                <?php if ( comments_open() ): ?>
                                
                                    <?php comments_template(); ?>
                                
                                <?php endif; ?>
                                
                            </div><!-- /.ts-latestnews -->
                        </section>
                    </div><!-- /#main-content -->
                    
                </article>
                <?php
                    break;
                    
                    default: // Sidebar right
                ?>
                <article class="col-md-9 col-sm-8">
                    <div id="main-content" class="main-content">
                        <section class="section section-blog blog-standard single-post-section">
                            <div class="ts-latestnews">
                                <?php $_sticky_class = is_sticky() ? 'sticky' : null; ?>
                                <div <?php post_class("$_ts_post_format_class $_sticky_class item-post"); ?>>
                                    <?php get_template_part( 'post-formats/post', $format ); ?>
                                    <div class="info-item-post">
                                        <div class="info-post">
                                            <h3><?php the_title(); ?></h3>
                                            <?php get_template_part( 'content-parts/blog', 'metas' ); ?>
                                        </div>
                                        <div class="content-post">
                                            <?php the_content(); ?>
                                        </div>
                                        <?php if( in_array( 'tags', $blog_metas_options ) ) :  ?>
                                            <?php
                                                $posttags = get_the_tags();
                                                if ( $posttags ) {
                                                    $tag_val = array();
                                                    foreach( $posttags as $tag ):
                                                        $tag_link = get_tag_link( $tag->term_id );
                                                        $tag_val[] = '<a href="'.$tag_link.'">' . $tag->name . '</a>'; 
                                                    endforeach;
                                            ?>
                                                <div class="tags">
                                                    <h5><?php _e( 'Tags', 'themestudio' ); ?></h5>
                                                    <?php echo implode(' ', $tag_val); ?>
                                                </div>
                                            <?php } ?>
                                        <?php endif; ?>
                                        
                                        <?php get_template_part( 'content-parts/blog', 'social' ); ?>
                                        
                                    </div><!-- /.info-item-post -->
                                </div><!-- /.item-post -->
                                
                                <?php get_template_part( 'content-parts/author', 'bio' ); ?>
                                
                                <?php if ( comments_open() ): ?>
                                
                                    <?php comments_template(); ?>
                                
                                <?php endif; ?>
                                
                            </div><!-- /.ts-latestnews -->
                        </section>
                    </div><!-- /#main-content -->
                    
                </article>
                <!-- Sidebar -->
                <?php get_sidebar('right' ); ?>
                <!-- End / Sidebar -->
                <?php
                    break;
                } ?>
            </div><!-- /.single-post-row -->
        </div><!-- /.container -->
    </div>
    <!-- End / #content -->

<?php get_footer(); ?>