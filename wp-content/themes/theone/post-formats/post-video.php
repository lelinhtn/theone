<?php

$thumb_src = function_exists( 'theone_get_img_src_by_id' ) ? theone_get_img_src_by_id( get_post_thumbnail_id(), '1170x613' ) : '';

?>
<?php if ( has_post_thumbnail() && $thumb_src != '' ): ?>
    
	<div class="img-post post-thumbnail">
		<figure><a href="<?php echo get_permalink(); ?>"><img src="<?php echo esc_url( $thumb_src ); ?>" alt="" /></a></figure>
		<span class="icon-post-type"><i class="fa fa-video-camera"></i></span>
	</div>
	
<?php endif ?>