<?php 
	global $post;
	
	$attachments = get_post_meta( $post->ID, 'tstheme_image_gallery', true );
	
	if ( $attachments ) :
?>
	<div id="owl-blog-list" class="head">
		<?php foreach ( $attachments as $attachment ){ ?>
        	<div class="item"><img src="<?php echo $attachment; ?>" alt="<?php get_the_title() ?>"></div>
  		<?php } ?>
    </div>
<?php 
	endif;
?>