<?php
global $theone;
if ( have_posts() ) : 
    
    if ( is_search() ) {
        while ( have_posts() ) : the_post();
    	
        	/**
        	 * Get blog posts by blog layout.
        	 */
        	get_template_part( 'loop/loop-search-list', 'item' );
    
        endwhile;  
    }
    else{
        while ( have_posts() ) : the_post();
    	
        	/**
        	 * Get blog posts by blog layout.
        	 */
        	get_template_part( 'loop/loop-blog-list', 'item' );
    
        endwhile;     
    }
    
else : 

/**
 * Display no posts message if none are found.
 */
get_template_part('loop/content','none');

endif;
?>