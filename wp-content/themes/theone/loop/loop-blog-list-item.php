<?php 
global $theone;
$format = get_post_format(); 

$icon_class = 'fa fa-file-text';

if( false === $format ) {
    $format = 'standard';    
}
switch ( $format ):
	
    case 'aside':
        $icon_class = 'fa fa-file-text';
        break;
    case 'chat':
        $icon_class = 'fa fa-comments-o';
        break;
    case 'gallery':
        $icon_class = 'fa fa-image';
        break;
    case 'link':
        $icon_class = 'fa fa-link';
        break;
    case 'image':
        $icon_class = 'fa fa-image';
        break;
    case 'quote':
        $icon_class = 'fa fa-quote-left';
        break;
    case 'status':
        $icon_class = 'fa fa-file-text';
        break;
    case 'video':
        $icon_class = 'fa fa-video-camera';
        break;
    case 'audio':
        $icon_class = 'fa fa-file-audio-o';
        break;
    default:
        $icon_class = 'fa fa-file-text';
        break;
            
endswitch;

$item_class = '';

$thumb_src = '';
if ( has_post_thumbnail() ) {
    $thumb_src = function_exists( 'theone_get_img_src_by_id' ) ? theone_get_img_src_by_id( get_post_thumbnail_id(), '1170x613' ) : '';
    $item_class .= ' item-has-thumbnail';
}
else{
    $item_class .= ' item-no-thumbnail';
    //$thumb_src = function_exists( 'theone_no_image' ) ? theone_no_image( array( 'width' => 1170, 'height' => 613 ), false, false ) : '';
}

//$excerpt = function_exists( 'theone_get_the_excerpt_max_charlength' ) ? theone_get_the_excerpt_max_charlength( 610 ): get_the_excerpt();
$is_blog_loop_content_type = 1; // Default show the content
if ( isset( $theone['opt-blog-loop-content-type'] ) ) {
    $is_blog_loop_content_type = $theone['opt-blog-loop-content-type'];
}
$the_excerpt_max_chars = isset( $theone['opt-excerpt-max-char-length'] ) ? max( 1, intval( $theone['opt-excerpt-max-char-length'] ) ) : 300;

$post_categories = wp_get_post_categories( get_the_ID() );
$cats = array();

if ( !empty( $post_categories ) ) {
    
    foreach( $post_categories as $c ){
    	$cat = get_category( $c );
        $cats[] = '<a href="' . get_category_link( $cat->term_id ) . '">' . $cat->name . '</a>';
    }
    
}

$cats = implode( ', ', $cats );

$item_class .= is_sticky() ? ' ts-item-sticky': '';

?>

<div <?php post_class( 'item-post ' . $item_class ); ?> id="post-<?php the_ID(); ?>" >
    
	<div class="img-post">
        
        <?php if ( has_post_thumbnail() ): ?>
		  <figure><a href="<?php echo get_permalink(); ?>"><img src="<?php echo esc_url( $thumb_src ); ?>" alt="" /></a></figure>
        <?php endif; ?>
		<span class="icon-post-type"><i class="<?php echo esc_attr( $icon_class ); ?>"></i></span>
        
	</div><!-- /.img-post -->
    
	<div class="info-post">
    
		<h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
		<?php get_template_part( 'content-parts/blog', 'metas' ); ?>
        <?php if ( $is_blog_loop_content_type == 1 ): // Show the content ?>
            <?php the_content(); ?>
        <?php else: // Show the excerpt ?>
            <div class="content-post the-excerpt-content">
                <p><?php echo function_exists( 'theone_get_the_excerpt_max_charlength' ) ? theone_get_the_excerpt_max_charlength( $the_excerpt_max_chars ): get_the_excerpt(); ?></p>
            </div>
            <?php
                $read_more_text = isset( $theone['opt-blog-continue-reading'] ) ? sanitize_text_field( $theone['opt-blog-continue-reading'] ) : __( 'Read more', 'theone-core' );
                echo '<p>' . ts_modify_read_more_link() . '</p>' 
            ?>
        <?php endif; ?>
        
	</div><!-- /info-post -->
    
</div><!-- /.item-post -->
