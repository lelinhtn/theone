<?php 
global $theone;

$blog_layout = isset( $theone['opt-blog-layout'] ) ? $theone['opt-blog-layout'] : '3';

if ( isset( $_REQUEST['sidebar'] ) ) {
    
    switch ( trim( strtolower( $_REQUEST['sidebar'] ) ) ) {
        case 'left':
            $_REQUEST['sidebar'] = '1';
            break;
        case 'right':
            $_REQUEST['sidebar'] = '2';
            break;
    }
    
    $blog_layout = $_REQUEST['sidebar'];
    if ( trim( $blog_layout ) != '1' && trim( $blog_layout ) != '2' ) {
        $blog_layout = '3';
    }
}

$icon_class = 'fa fa-file-text';
switch ( $blog_layout ):
    case 1: // Sidebar left
        $item_class = 'col-md-4 col-sm-6 col-xs-6';
        break;
    case 2: // Sidebar right
        $item_class = 'col-md-4 col-sm-6 col-xs-6';
        break;
    case 3: // No sidebar
        $item_class = 'col-md-3 col-sm-4 col-xs-6 col-lg-3';
        break;
endswitch;


$format = get_post_format(); 
if( false === $format ) {
    $format = 'standard';    
}
	
switch ( $format ):
	
    case 'aside':
        $icon_class = 'fa fa-file-text';
        break;
    case 'chat':
        $icon_class = 'fa fa-comments-o';
        break;
    case 'gallery':
        $icon_class = 'fa fa-image';
        break;
    case 'link':
        $icon_class = 'fa fa-link';
        break;
    case 'image':
        $icon_class = 'fa fa-image';
        break;
    case 'quote':
        $icon_class = 'fa fa-quote-left';
        break;
    case 'status':
        $icon_class = 'fa fa-file-text';
        break;
    case 'video':
        $icon_class = 'fa fa-video-camera';
        break;
    case 'audio':
        $icon_class = 'fa fa-file-audio-o';
        break;
    default:
        $icon_class = 'fa fa-file-text';
        break;
            
endswitch;

$thumb_src = '';
if ( has_post_thumbnail() ) {
    $thumb_src = function_exists( 'theone_get_img_src_by_id' ) ? theone_get_img_src_by_id( get_post_thumbnail_id(), '540x430' ) : '';
}
else{
    $thumb_src = function_exists( 'theone_no_image' ) ? theone_no_image( array( 'width' => 540, 'height' => 430 ), false, false ) : '';
}

$excerpt = function_exists( 'theone_get_the_excerpt_max_charlength' ) ? theone_get_the_excerpt_max_charlength( 85 ): get_the_excerpt(); 

?>

<div <?php post_class( $item_class ); ?> id="post-<?php the_ID(); ?>">
    
    <div class="item-post">
        
        <div class="img-post">
            
            <figure><a href="<?php echo get_permalink(); ?>"><img src="<?php echo esc_url( $thumb_src ); ?>" alt="" /></a></figure>
            <span class="icon-post-type"><i class="<?php echo esc_attr( $icon_class ); ?>"></i></span>
            
        </div><!-- /.img-post -->
        
        <div class="info-post">
			<h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
			<ul class="meta-post">
				<li><?php _e( 'On : ', 'themestudio' ); ?><span class="date"><a href="#"><?php the_time( get_option( 'date_format' ) ); ?></a></span></li>
				<li><?php _e( 'By : ', 'themestudio' ) ?><span class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></span></li>
			</ul>
			<div><?php echo $excerpt; ?></div>
		</div>
        
    </div><!-- /.item-post -->

</div>