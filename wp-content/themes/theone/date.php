<?php 
    /**
    * date.php
    * The main post loop in THE ONE
    * @author Theme Studio
    * @package THE ONE
    * @since 1.0.0
    */

    get_header();
    global $theone;
    $blog_layout = isset( $theone['opt-blog-layout'] ) ? $theone['opt-blog-layout'] : '3';
    $blog_layout_style = isset( $theone['opt-blog-layout-style'] ) ? $theone['opt-blog-layout-style'] : 'list';

?>
    <!-- content -->
    <div id="content" class="site-content">
    
        <section class="section section-blog blog-grid layout-<?php echo esc_attr( $blog_layout ); ?>">
            
            <div class="container">
                
                <div class="ts-latestnews">
                        
                    <?php switch ( $blog_layout ) {
                    	case '1': // Sidebar left
                    ?>
                        <div class="row">
                            <div class="col-md-9 col-sm-8 loop-container">
                                <?php get_template_part( 'loop/loop-blog', $blog_layout_style ); ?>
                                <?php echo function_exists('ts_pagination') ? ts_pagination() : posts_nav_link(); ?>
                            </div>
                            <?php get_sidebar('left' ); ?>
                        </div>
                    <?php
                    	break;
                        case '3': // No sidebar
                    ?>
                        <?php get_template_part( 'loop/loop-blog', $blog_layout_style ); ?>
                        <?php echo function_exists('ts_pagination') ? ts_pagination() : posts_nav_link(); ?>
                    <?php
                    		
                    	break;
                    	
                    	default: // Sidebar right
                    ?>
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
    						<?php get_template_part( 'loop/loop-blog', $blog_layout_style ); ?>
                            <?php echo function_exists('ts_pagination') ? ts_pagination() : posts_nav_link(); ?>
                        </div>
						<?php get_sidebar('right' ); ?>
                    </div>
                    <?php
                    	break;
                    } ?>
                    
                </div><!-- /.ts-latestnews -->
                
            </div><!-- /.container -->
            
        </section><!-- /.section-blog -->
        
    </div>
    <!-- End / #content -->

<?php get_footer(); ?>