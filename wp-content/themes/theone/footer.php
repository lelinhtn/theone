<?php 
    /**
    * footer.php
    * Footer
    * @author Theme Studio
    * @package THE ONE
    * @since 1.0.0
    */
    global $theone;
    $copyright_text = isset( $theone['opt-footer-copyright-text'] ) ? $theone['opt-footer-copyright-text'] : '';
    $footer_1_postion = isset( $theone['opt-footer-top-position'] ) ? $theone['opt-footer-top-position'] : 'top';
    $show_sep = isset( $theone['opt-enable-sep-between-2-footer-part'] ) ? $theone['opt-enable-sep-between-2-footer-part'] == 1 : false;
    $footer_class = 'site-footer';
    
    $is_plugin_core_active = ts_is_plugin_active( 'theone-core/theone-core.php' );
    if ( !$is_plugin_core_active ) {
        $seven_oroof_url = 'http://7oroof.com';
        $copyright_text = 'The One &#64; All Rights Reserved. With Love by <a href="' . esc_url( $seven_oroof_url ) . '">7oroof.com</a><label class="footer-link"><a href="#">Privacy Policy</a> - <a href="#">Terms of Use</a></label>';
        $footer_class .= ' no-plugin-core';
    }
    
?>
    </div><!-- /.site-content -->
    
    <?php do_action( 'ts_before_footer' ); ?>
    
    <!-- Footer -->
    <footer id="footer" class="<?php echo esc_attr( $footer_class ); ?>">
        <!-- Go top -->
        <div class="back-to-top"><i class="fa fa-angle-up"></i></div>
        <!-- End / Go top -->
        <div class="container">
            <?php if ( $footer_1_postion == 'top' ): ?>
                <?php get_template_part( 'content-parts/footer-part-1' ); ?>
                <?php if ( $show_sep ): ?>
                    <div class="footer-sep"><hr /></div>
                <?php endif; ?>
                <?php get_template_part( 'content-parts/footer-part-2' ); ?>
            <?php else: ?>
                <?php get_template_part( 'content-parts/footer-part-2' ); ?>
                <?php if ( $show_sep ): ?>
                    <div class="footer-sep"><hr /></div>
                <?php endif; ?>
                <?php get_template_part( 'content-parts/footer-part-1' ); ?>
            <?php endif; ?>
        </div>
    </footer>
    <!-- End / Footer -->
    
    <div id="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7">
                    <div class="copyright">
                        <?php echo $copyright_text; ?>
                    </div><!--/.copyright-->
                </div>
                
                <div class="col-md-5 col-sm-5">
                    <?php if ( has_nav_menu( 'footer_menu' ) ): ?>
                        <div class="footer-nav">
                            <?php wp_nav_menu( array( 
                                'theme_location'    => 'footer_menu', 
                                'menu_class'        => '',
                                'container'         => false
                            ) ); ?>
                        </div>
                    <?php else: ?>
                        <?php if ( !$is_plugin_core_active ): ?>
                            <div class="footer-nav">
                                <ul id="menu-footer">
                                    <li class="menu-item">
                                        <a href="<?php menu_page_url( 'nav-menus' ); ?>"><?php _e( 'Add a menu', 'themestudio' ); ?></a>
                                    </li>
                                </ul>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /#bottom -->
    
    <?php
        /**
         *  ts_add_hidden_sidebar 
         **/
        do_action( 'ts_after_footer' ); 
    ?>
    
</div>
<!-- End / Site main -->
<?php wp_footer(); ?>
</body>
</html>