<?php 
	/**
	* comments.php
	* The main post loop in THE ONE
	* @author Theme Studio
	* @package THE ONE
	* @since 1.0.0
	*/
	$fields =  array(
		'author' => '<div class="col-sm-6"><input type="text" name="author" id="name" class="input-form" placeholder="Name*" />',
		'email'  => '<input type="text" name="email" id="email" class="input-form" placeholder="Email*"/>',
		'submit'  => '<input class="submit  logged-is-out" type="submit" value="Add Comment" name="submit"></div>',
		//'website'  => '<label class="col-md-4 col-sm-12" for="website"><h3>Website</h3><input type="text" name="website" id="website" /></label>',
	);

	$custom_comment_form = array( 
		'fields' => apply_filters( 'comment_form_default_fields', $fields ),
	  	'comment_field' => '
	  	<div class="col-sm-6 message-comment"><textarea name="comment" id="message" rows="5" class="textarea-form" placeholder="Your comment*" ></textarea></div>',
	  	'logged_in_as' => '<p class="logged-in-as col-md-12 col-sm-12">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a> <a href="%3$s">Log out?</a>','themestudio' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ) ) . '</p>',
	  	'cancel_reply_link' => __( 'Cancel' , 'themestudio' ),
	  	'comment_notes_before' => '<h4 class="col-md-12">' . __( 'Leave Comment', 'themestudio' ) . '</h4>',
	  	'comment_notes_after' => '',
	  	'title_reply' => '',
	  	'label_submit' => __( 'Add Comment' , 'themestudio' ),
	);
?>
				
	<div id="comment" class="comment">
		<h4><?php comments_number( __( '0 Comment', 'themestudio'), __( '1 Comment', 'themestudio' ), __( '% Comments', 'themestudio' ) ); ?></h4>
		<?php  if( have_comments() ): ?>
            <ul class="comment-list">
                <?php wp_list_comments( 'type=comment&callback=ts_theme_comment' ); ?>
            </ul>
		<?php endif; ?>
		<?php paginate_comments_links(); ?>
	</div>

	<div class="cmt-form">
		
		<div class="row">
			  <!-- Add comment Form field -->
			  <?php comment_form( $custom_comment_form ); ?>
		</div>
	</div>