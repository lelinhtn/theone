<!DOCTYPE html>
<html <?php language_attributes(); ?>> <!-- This is the comment for test -->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="utf-8" />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <!--[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!--Wrapper-->
<div id="main" class="site-main" >
    
    <!-- Header -->
    <header id="header" class="site-header">
        
        <div class="container">
            <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_class' => 'nav-menu', 'menu_id' => 'main-menu' ) ); ?>
        </div>
        
    </header>
    <!-- End / Header -->
    
    <section id="hero-section" class="hero-section-image bg-parallax" >
		<div class="overlay"></div>
		<div class="hero-section-content">
			<h2>Text Boxes</h2>
		</div>
	</section>
    
    <div class="site-content">