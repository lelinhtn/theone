<?php 
    /**
    * page.php
    * The main post loop in THE ONE
    * @author Theme Studio
    * @package THE ONE
    * @since 1.0.0
    * Changed
    */

    get_header();
    the_post();
    global $theone;
    
    

?>

    <!-- Content -->
    <div id="content" class="site-content">
        <!-- Blog list -->
        <div id="main-content" class="main-content" >
            <div  class="container" >
                <article class="page-ct">
                    <?php
						the_content();
						wp_link_pages();
					?>
                </article>
            </div><!-- /.container -->
        </div><!-- /#main-content -->
    </div>
    <!-- End /#content -->


<?php get_footer(); ?>