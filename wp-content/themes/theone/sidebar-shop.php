<?php 
	/**
	* sidebar-shop.php
	* The main post loop in THE ONE
	* @author Theme Studio
	* @package THE ONE
	* @since 1.0.0
	*/
?>

<?php if ( is_active_sidebar( 'primary' ) ) : ?>
    <div class="col-md-3 col-sm-4 sidebar-shop">
        <div id="sidebar" class="sidebar">
            <?php dynamic_sidebar( 'ts-wc-sidebar' ); ?>
        </div>
    </div>
<?php endif; ?>