<?php
/**
* Template Name: Fullwidth Template
*
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header();
the_post();
global $theone;

?>

    <!-- Content -->
    <div id="content" class="fullwidth site-content">
        <?php
			the_content();
			wp_link_pages();
		?>
    </div>
    <!-- End / Content -->
<?php get_footer(); ?>