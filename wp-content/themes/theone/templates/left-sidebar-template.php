<?php
/**
* Template Name: Left Sidebar Template
*
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header();
the_post();
global $theone;

?>
    <!-- Content -->
    <div id="content" class="left-sidebar-content site-content">
        <div class="container">
            <div class="row">
                <?php get_sidebar( 'left' ); ?>
                <div class="col-md-9 col-sm-8">
                    <?php
            			the_content();
            			wp_link_pages();
            		?>
                </div>
            </div>
        </div>
    </div>
    <!-- End / Content -->
    
<?php get_footer(); ?>