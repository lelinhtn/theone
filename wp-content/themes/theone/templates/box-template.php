<?php
/**
* Template Name: Box Template
*
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header( 'box' );
the_post();
global $theone;

?>

    <!-- Content -->
    <div id="content" class="fullwidth site-content container box-container">
        <?php
			the_content();
			wp_link_pages();
		?>
    </div>
    <!-- End / Content -->
<?php get_footer( 'box' ); ?>