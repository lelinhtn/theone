<?php
/**
* Template Name: Right Sidebar Template
*
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header();
the_post();
global $theone;

?>
    <!-- Content -->
    <div id="content" class="right-sidebar-content site-content">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <?php
            			the_content();
            			wp_link_pages();
            		?>
                </div>
                <?php get_sidebar( 'right' ); ?>
            </div>
        </div>
    </div>
    <!-- End / Content -->
    
<?php get_footer(); ?>