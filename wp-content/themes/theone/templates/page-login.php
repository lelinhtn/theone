<?php
/**
* Template Name: Login Page
*
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header();
the_post();
global $theone;

$blog_layout = isset( $theone['opt-blog-layout'] ) ? $theone['opt-blog-layout'] : '3';

?>

    <!-- Content -->
    <div id="content" class="fullwidth site-content">
        
        <div class="container">
            
            <div class="login-register-page-row main-row row">
                
                <article class="col-md-12 col-sm-12">
                    <div id="main-content" class="main-content">
                        <section class="section section-login section-page page-standard">
                            <?php get_template_part( 'content-parts/content-page', 'login' ); ?>
                            <?php
                    			the_content();
                    			wp_link_pages();
                    		?>
                        </section>
                    </div><!-- /#main-content -->
                    
                </article>
                
            </div><!-- /.login-register-page-row -->
            
        </div><!-- /.container -->
        
    </div>
    <!-- End / Content -->
<?php get_footer(); ?>