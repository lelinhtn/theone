<?php
/**
* Template Name: One Page Template
*
* @author Theme Studio
* @package THE ONE
* @since 1.0.0
*/

get_header();

global $theone;

?>

<?php get_template_part('content-parts/home-style', 'header'); ?>

<div id="main-content">
	<!-- <div id="page-<?php the_ID(); ?>" <?php post_class(); ?>> -->

		<?php
			wp_nav_menu( 
				array(
					'theme_location'    => 'onepage_menu',
					'container'         => false,
	                'container_class'   => false,
	                'menu_class'        => '',
	                'items_wrap'        => '%3$s',
	                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
		            'walker' => new ts_home_walker()
	            )
	        );
		?>

	<!-- </div> -->
</div>
<?php get_footer(); ?>