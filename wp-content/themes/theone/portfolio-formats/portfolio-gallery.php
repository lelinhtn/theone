<?php 
	global $post;
	
	$attachments = get_post_meta( $post->ID, 'tstheme_portfolio_slider', true );
	
	if ( $attachments ) :
?>
    <div class="owl-box">
        <?php foreach ( $attachments as $attachment ){ ?>
        	<div class="item"><img src="<?php echo $attachment; ?>" alt="<?php get_the_title() ?>"></div>
  		<?php } ?>
    </div>
<?php 
	endif;
?>