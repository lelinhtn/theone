<?php if ( get_post_meta( $post->ID, "tstheme_portfolio_soundcloud", true ) ) : ?>
	<div class="audio head container_audio">
		<?php echo apply_filters('the_content', get_post_meta( $post->ID, "tstheme_portfolio_soundcloud", true )); ?>
	</div>
	<script>
		jQuery(document).ready(function ($) {
			$(".container_audio p iframe").css({"width":"100%","height":"100"});
		});
	</script>
<?php endif; ?>