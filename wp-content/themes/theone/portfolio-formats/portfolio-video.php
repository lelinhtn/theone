<?php if ( get_post_meta( $post->ID, "tstheme_portfolio_video", true ) ) : ?>
	<div id="blog-video" class="video head">
		<?php echo apply_filters('the_content', get_post_meta( $post->ID, "tstheme_portfolio_video", true )); ?>
	</div>
	<div class="clearfix"></div>
	
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$("#blog-video").fitVids();
		});
	</script>
<?php endif; ?>