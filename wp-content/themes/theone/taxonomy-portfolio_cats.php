<?php 
	/**
	 * archive-dslc_projects.php
	 * The project archive used in Liwo
	 * @author Theme Studio
	 * @package Liwo
	 * @since 1.0.0
	 */
	get_header();
	global $theone;
	
?>
	<?php if ($theone['portfolio_style']==1): ?>
	    <!-- Main content -->
	    <div id="main-content">
	        <!-- Work -->
	        <div id="work">
	            <div class="container">
	                <div class="row">
	                    <div class="title">
	                        <h1>Works</h1>
	                        <span class="icofont moon-pyramid"></span>
	                    </div>

	                    <?php get_template_part('content-parts/portfolio-ajax', 'section'); ?>

	                    <div class="ct wow fadeInUp" data-wow-delay=".4s">
	                        <?php get_template_part('content-parts/portfolio', 'filter'); ?>
	                        
							<div id="container-work" class="">
							  <?php get_template_part('loop/loop-blog', 'portfolio'); ?>
	                        </div>
						</div>
		    		</div>
		    	</div>
		    </div>
		</div>
	<?php else: ?>
		<!-- Main content -->
	    <div id="main-content">
	        <!-- //////////////////// Section Work //////////////////// -->
	        <div id="work" class="va-work">
	            <div class="container">
	                <div class="row">
	                    <div class="title">
	                        <h1>Works</h1>
	                        <span class="icofont moon-pyramid"></span>
	                    </div>
	                </div>
	            </div>
	            <div class="container">
                <div class="row">         
                    <div class="ct wow fadeInUp" data-wow-delay=".4s">
                        <div id="owl">
							<?php get_template_part('loop/loop-lightbox', 'portfolio'); ?>
                        </div>
                    </div>
                </div>
	        </div>
	    </div>
	<?php endif ?>
    
  
<?php get_footer(); ?>