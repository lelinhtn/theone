<?php

global $theone;
$enable_share_post = isset( $theone['opt-enable-share-post'] ) ? $theone['opt-enable-share-post'] == 1 : false;
$socials_shared = isset( $theone['opt-single-share-socials'] ) ? $theone['opt-single-share-socials'] : array();

?>

<?php if ( $enable_share_post ): ?>

<div class="share-post">
    <div class="row">
        <div class="col-md-6">
            <h5><?php _e( 'Share This Portfolio :', 'themestudio' ); ?></h5>
        </div>
        <div class="col-md-6">
            <div class="icons">
                <?php if ( in_array( 'facebook', $socials_shared ) ): ?>
                    <a class="style1" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                <?php endif; ?>
                <?php if ( in_array( 'twitter', $socials_shared ) ): ?>
                    <a class="style2" href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                <?php endif; ?>
                <?php if ( in_array( 'gplus', $socials_shared ) ): ?>
                    <a class="style3" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                <?php endif; ?>
                <?php if ( in_array( 'pinterest', $socials_shared ) ): ?>
                    <a class="style4" href="https://pinterest.com/pin/create/button/?url=&amp;media=&amp;description=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
                <?php endif; ?>
                <?php if ( in_array( 'linkedin', $socials_shared ) ): ?>
                    <a class="style6" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=&amp;title=&amp;summary=&amp;source=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>
