<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

global $theone, $current_user;

$blog_layout = isset( $theone['opt-blog-layout'] ) ? $theone['opt-blog-layout'] : '3'; // Default 3 = no sidebar

$action = !empty( $_GET['action'] ) && ($_GET['action'] == 'register' || $_GET['action'] == 'forgot' || $_GET['action'] == 'resetpass') ? $_GET['action'] : 'login';
$success = !empty( $_GET['success'] );
$failed = !empty( $_GET['failed'] ) ? $_GET['failed'] : false;

$class_login = is_user_logged_in() ? 'logged-in' : 'not-logged-in';
//$class_cols = $blog_layout == 3 ? 'col-lg-4 col-lg-push-4' : 'col-lg-6 col-lg-push-3';

$redirect = ts_get_login_redirect_url();

$login_form_args = array(
	'echo' => true,
	'redirect' => $redirect, // Default redirect is back to the current page
	'form_id' => 'loginform',
	'label_username' => __( 'User name', 'themestudio' ),
	'label_password' => __( 'Password', 'themestudio' ),
	'label_remember' => __( 'Remember Me', 'themestudio' ),
	'label_log_in' => __( 'Log In', 'themestudio' ),
	'id_username' => 'user_login',
	'id_password' => 'user_pass',
	'id_remember' => 'rememberme',
	'id_submit' => 'wp-submit',
	'remember' => true,
	'value_username' => '',
	'value_remember' => false, // Set this to true to default the "Remember me" checkbox to checked
);

?>

<div class="content-inner <?php echo esc_attr( $class_login ); ?>">
    <?php if ( is_user_logged_in() ): ?>
    
        <h3><?php _e( 'Welcome', 'themestudio' ); ?></h3>
        
        <p><?php echo sprintf( __( 'Hello %s!', 'themestudio' ), $current_user->display_name ); ?></p>
        
    <?php else: ?>
        
        <div class="login-container">
            <div class="login-wrap">
                
            <?php if ( $action == 'login' ): ?>
            
                <div class="section-title">
                    <div class="subtitle"><?php _e( 'Welcome Back', 'themestudio' ); ?></div>
                    <h3><?php _e( 'Login Area', 'themestudio' ); ?></h3>
                </div>
                
                <?php ts_custom_login( $login_form_args ); ?>
            
            <?php endif; // End if ( $action == 'login' ) ?>
            
            <?php if ( $action == 'register' && get_option( 'users_can_register' ) ): ?>
                <?php
                    $terms_of_use_url = isset( $theone['opt-terms-of-use-url'] ) ? esc_url( $theone['opt-terms-of-use-url'] ) : '';
                ?>
                <div class="section-title">
                    <div class="subtitle"><?php _e( 'Hello', 'themestudio' ) ?></div>
                    <h3><?php _e( 'Register Form', 'themestudio' ); ?></h3>
                </div>
                <div class="ts-login">
                    <form name="registerform" class="ts-register-form" method="POST" >
                        <div><input type="text" name="username" placeholder="<?php _e( 'User Name', 'themestudio' ); ?>" /></div>
                        <div><input type="text" name="email" placeholder="<?php _e( 'Email', 'themestudio' ); ?>" /></div>
                        <div><input type="password" name="password" placeholder="<?php _e( 'Password', 'themestudio' ); ?>" /></div>
                        <div><input type="password" name="confirm-password" placeholder="<?php _e( 'Confirm Password', 'themestudio' ); ?>" /></div>
                        <div class="remember">
                            <label><input type="checkbox" name="agree" /> <?php echo __( 'I Agree To The ', 'themestudio' ); ?>
                                <?php if ( trim( $terms_of_use_url ) != '' ): ?>
                                    <a href="<?php echo esc_url( $terms_of_use_url ); ?>" target="_blank"><?php _e( 'Terms Of Use ?', 'themestudio' ); ?></a>
                                <?php else: ?>
                                    <?php _e( 'Terms Of Use ?', 'themestudio' ); ?>
                                <?php endif; ?>
                            </label>
                        </div>
                        <?php wp_nonce_field( 'ajax-register-nonce', 'register-ajax-nonce' ); ?>
                        <div><input type="submit" value="<?php _e( 'Register', 'themestudio' ); ?>" /></div>
                        <div class="login-footer"><a href="<?php echo get_permalink(); ?>"><?php _e( 'Have an account? Login Here', 'themestudio' ); ?></a></div>
                    </form>
                </div>
                    
            <?php endif; // End if ( $action == 'register' && get_option( 'users_can_register' ) ) ?>
            
            <?php if ( $action == 'forgot' ): ?>
                
                <div class="section-title">
                    <div class="subtitle"><?php _e( 'Need help !', 'themestudio' ) ?></div>
                    <h3><?php _e( 'Forget Password', 'themestudio' ); ?></h3>
                </div>
                <div class="ts-login">
                    <form name="forgotpassform" class="ts-forgot-pass-form" method="POST" >
                        <div><input type="text" name="username" placeholder="<?php _e( 'User Name', 'themestudio' ); ?>" /></div>
                        <div><input type="text" name="email" placeholder="<?php _e( 'Email', 'themestudio' ); ?>" /></div>
                        <div><input type="submit" value="<?php _e( 'Reset Password', 'themestudio' ); ?>" /></div>
                    </form>
                </div>
            
            <?php endif; ?>
                
                
            </div><!-- /.login-wrap -->
        </div><!-- /.login-container -->
        
    <?php endif; // End if ( is_user_logged_in() ) ?>
</div><!-- /.content-inner -->
