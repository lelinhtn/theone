<?php 
global $theone;

$views_count = ts_get_post_view_count( get_the_ID() );
$archive_year  = get_the_time('Y'); 
$archive_month = get_the_time('m'); 
$archive_day   = get_the_time('d'); 

$meta_li_html = '';

$default_blog_metas = array( 'date', 'author', 'comment', 'category', 'tags' );
$blog_metas = isset( $theone['opt-blog-metas'] ) ? $theone['opt-blog-metas'] : $default_blog_metas;

ob_start();

if ( isset( $blog_metas ) ) {
    ?>
    <?php if( in_array( 'date', $blog_metas ) ) : ?>
    	   <li><?php _e( 'On : ', 'themestudio' ); ?><span class="date"><a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day ); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a></span></li>
        <?php endif; ?>
        <?php if( in_array( 'author', $blog_metas ) ) : ?>
    	   <li><?php _e( 'By : ', 'themestudio' ) ?><span class="author"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author_meta( 'display_name' ); ?></a></span></li>
        <?php endif; ?>
        <?php if( in_array( 'comment', $blog_metas ) && comments_open() ) : ?>
            <li><?php _e( 'Comments : ', 'themestudio' ); ?><span class="the-comments"><a href="<?php echo comments_link(); ?>"><?php comments_number( '0', '1', '%' ); ?> </a></span></li>
        <?php endif; ?>
        <?php if( in_array( 'category', $blog_metas )  && has_category() ) :  ?>
            <li><?php _e( 'Category : ', 'themestudio' ) ?><span class="the-category"><?php the_category(', '); ?></span></li>
        <?php endif; ?>
        <?php if( in_array( 'tags', $blog_metas )  && has_category() ) :  ?>
            <li><?php _e( 'Tags : ', 'themestudio' ) ?><span class="the-tags"><?php the_tags('', ', '); ?></span></li>
        <?php endif; ?>
        <?php if( in_array( 'views', $blog_metas ) && has_category() ) :  ?>
            <li><?php _e( 'View : ', 'themestudio' ); ?><?php echo $views_count; ?></li>
        <?php endif; ?>
    <?php
}

$meta_li_html .= ob_get_clean();

?>

<?php if ( trim( $meta_li_html ) != '' ): ?>

    <ul class="meta-post">
        <?php echo $meta_li_html; ?>
    </ul>

<?php endif; ?>
