<?php

wp_nav_menu( array( 
    'theme_location'    => 'main_menu_1', 
    'menu_class' => 'nav navbar-nav',
    'container' => false,
    'fallback_cb' => 'ts_bootstrap_navwalker::fallback',
    'walker' => new ts_bootstrap_navwalker()
) );