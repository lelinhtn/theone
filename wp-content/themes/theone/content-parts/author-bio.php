<?php 
global $theone;

$enable_author_bio = isset( $theone['opt-blog-single-post-bio'] ) ? $theone['opt-blog-single-post-bio'] == 1 : false;

?>

<?php if ( $enable_author_bio ): ?>

<div class="author-box">
    <?php echo get_avatar( get_the_author_meta( 'ID' ), 64, '', get_the_author_meta( 'display_name' ) ); ?> 
    <h6><?php echo get_the_author_meta( 'display_name' ); ?></h6>
    <div><?php echo get_the_author_meta( 'description' ); ?></div>
</div>

<?php endif; ?>