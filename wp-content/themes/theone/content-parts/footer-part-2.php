<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

global $theone;

$footer_1_postion = isset( $theone['opt-footer-top-position'] ) ? $theone['opt-footer-top-position'] : 'top';
$footer_2_postion = $footer_1_postion == 'top' ? 'bottom' : 'top';
$footer_2_border_class = isset( $theone['opt-footer-top-border'] ) ? $theone['opt-footer-top-border'] == 1 ? 'has-border' : '' : '';
$footer_2_class = 'footer-bottom footer-part-position-' . esc_attr( $footer_2_postion . ' ' . $footer_2_border_class );

?>

<div class="<?php echo esc_attr( $footer_2_class ); ?>">
    <div class="row">
        <?php 
        
            $numberOfFooterWidgets = min( 4, max( 0, intval( $theone['opt-footer-sidebar-layout'] ) ) );
            $columns = 0;
            if ( $numberOfFooterWidgets > 0 ):
                $columns = max(0, intval( 12/$numberOfFooterWidgets ) );
            endif;
        ?>

        <?php if ($columns > 0): ?>
            
            <?php for ($i = 1; $i <= $numberOfFooterWidgets; $i++): ?>
            
            <div class="footer-col-<?php echo $i; ?> col-xs-12 col-md-<?php echo $columns; ?>">
                <?php dynamic_sidebar( 'ts-sidebar-footer-'.$i ); ?>
            </div>
            
            <?php endfor; ?>
            
        <?php endif ?>

    </div>
</div><!-- /.footer-bottom -->