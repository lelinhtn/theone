<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

global $theone;

$footer_1_postion = isset( $theone['opt-footer-top-position'] ) ? $theone['opt-footer-top-position'] : 'top';
$footer_1_border_class = isset( $theone['opt-footer-top-border'] ) ? $theone['opt-footer-top-border'] == 1 ? 'has-border' : '' : '';
$footer_1_class = 'footer-top footer-part-position-' . esc_attr( $footer_1_postion . ' ' . $footer_1_border_class );

?>

<div class="<?php echo esc_attr( $footer_1_class ); ?>">
    <div class="row">
    
        <?php
            $numberOfFooterTopWidgets = isset( $theone['opt-footer-top-sidebar-layout'] ) ? min( 4, max( 0, intval( $theone['opt-footer-top-sidebar-layout'] ) ) ): 4;
        ?>
        
        <?php if ($numberOfFooterTopWidgets > 0): ?>
            
            <?php for ($i = 1; $i <= $numberOfFooterTopWidgets; $i++): ?>
            <?php
                $columns = isset( $theone['opt-footer-top-wg-' . $i . '-cols'] ) ? min( 12, max( 0, $theone['opt-footer-top-wg-' . $i . '-cols'] ) ) : 4;
            ?>
            <div class="col-xs-12 col-md-<?php echo $columns; ?>">
                <?php dynamic_sidebar( 'ts-sidebar-footer-top-' . $i ); ?>
            </div>
            
            <?php endfor; ?>
            
        <?php endif ?>
        
    </div>
</div><!-- /.footer-top -->