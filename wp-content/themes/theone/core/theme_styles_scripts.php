<?php 

if ( !function_exists( 'theone_fonts_url' ) ) {
    
    /**
     * Register Google fonts for The One.
     *
     * @since The One 1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function theone_fonts_url() {
    	$fonts_url = '';
    	$fonts     = array();
    	$subsets   = 'latin,latin-ext';
    
    	/* translators: If there are characters in your language that are not supported by Varela Round, translate this to 'off'. Do not translate into your own language. */
    	if ( 'off' !== _x( 'on', 'Varela Round font: on or off', 'themestudio' ) ) {
    		$fonts[] = 'Varela Round:400';
    	}
    
    	/* translators: If there are characters in your language that are not supported by Roboto, translate this to 'off'. Do not translate into your own language. */
    	if ( 'off' !== _x( 'on', 'Roboto: on or off', 'themestudio' ) ) {
    		$fonts[] = 'Roboto:400,100,300,700';
    	}
    
    	/* translators: To add an additional character subset specific to your language, translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language. */
    	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'themestudio' );
    
    	if ( 'cyrillic' == $subset ) {
    		$subsets .= ',cyrillic,cyrillic-ext';
    	} elseif ( 'greek' == $subset ) {
    		$subsets .= ',greek,greek-ext';
    	} elseif ( 'devanagari' == $subset ) {
    		$subsets .= ',devanagari';
    	} elseif ( 'vietnamese' == $subset ) {
    		$subsets .= ',vietnamese';
    	}
    
    	if ( $fonts ) {
    		$fonts_url = add_query_arg( array(
    			'family' => urlencode( implode( '|', $fonts ) ),
    			'subset' => urlencode( $subsets ),
    		), '//fonts.googleapis.com/css' );
    	}
    
    	return $fonts_url;
    }   
}

if ( !function_exists( 'ts_theone_js' ) ) {

	/*
	 * Load jquery
	*/
	function ts_theone_js(){
		if ( !is_admin() ){
            global $theme_global_localize;
            
            $google_map_js_url = 'https://maps.googleapis.com/maps/api/js?sensor=false';
            
			wp_enqueue_script('jquery');
            
            wp_register_script( 'theone-html5shiv.min', THE_ONE_LIBS_URL . '/html5shiv/html5shiv.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-html5shiv.min' );
            
            wp_register_script( 'theone-jquery.debouncedresize', THE_ONE_LIBS_URL . '/debouncedresize/jquery.debouncedresize.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.debouncedresize' );
            
            wp_register_script( 'theone-modernizr', THE_ONE_LIBS_URL . '/modernizr/Modernizr.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-modernizr' );
            
            //wp_register_script( 'theone-jquery.easing.min', THE_ONE_LIBS_URL . '/jquery.easing/jquery.easing.min.js', false, THE_ONE_THEME_VERSION, true );
//			wp_enqueue_script( 'theone-jquery.easing.min' );
            
            wp_register_script( 'theone-jquery.easypiechart.min', THE_ONE_LIBS_URL . '/easypiechart/jquery.easypiechart.min.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-jquery.easypiechart.min' );
            
            wp_register_script( 'theone-jquery.easypiechart.min', THE_ONE_LIBS_URL . '/easypiechart/jquery.easypiechart.min.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-jquery.easypiechart.min' );
            
            wp_register_script( 'theone-jquery.prettyPhoto', THE_ONE_LIBS_URL . '/pretty-photo/js/jquery.prettyPhoto.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.prettyPhoto' );
            
            wp_register_script( 'theone-fitvids', THE_ONE_LIBS_URL . '/fitvids/jquery.fitvids.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-fitvids' );
            
            wp_register_script( 'theone-jquery.parallax-1.1.3', THE_ONE_LIBS_URL . '/parallax/jquery.parallax-1.1.3.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.parallax-1.1.3' );
            
            wp_register_script( 'theone-snap.svg-min', THE_ONE_LIBS_URL . '/page-loading-effects/js/snap.svg-min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-snap.svg-min' );
            
            wp_register_script( 'theone-classie', THE_ONE_LIBS_URL . '/page-loading-effects/js/classie.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-classie' );
            
            wp_register_script( 'theone-svgLoader', THE_ONE_LIBS_URL . '/page-loading-effects/js/svgLoader.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-svgLoader' );
            
            wp_register_script( 'theone-page-loading-effects', THE_ONE_LIBS_URL . '/page-loading-effects/js/page-loading-effects.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-page-loading-effects' );
            
            wp_register_script( 'theone-jquery.placeholder', THE_ONE_LIBS_URL . '/jquery.placeholder/jquery.placeholder.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.placeholder' );
            
            wp_register_script( 'theone-bootstrap.min', THE_ONE_LIBS_URL . '/bootstrap/js/bootstrap.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-bootstrap.min' );
            
            wp_register_script( 'theone-owl.carousel.min', THE_ONE_LIBS_URL . '/owl-carousel/owl.carousel.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-owl.carousel.min' );

            wp_register_script( 'theone-chosen.jquery.min', THE_ONE_LIBS_URL . '/chosen/chosen.jquery.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-chosen.jquery.min' );
            
            wp_register_script( 'theone-jquery.sidr.min', THE_ONE_LIBS_URL . '/sidr/jquery.sidr.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.sidr.min' );
            
            if ( wp_is_mobile() ) {
                $theme_global_localize['is_mobile'] = 'true';
                
            	/* Display and echo mobile specific stuff here */
                wp_register_script( 'theone-jquery.touchwipe.min', THE_ONE_LIBS_URL . '/touchewipe/jquery.touchwipe.min.js', false, THE_ONE_THEME_VERSION, true );
                wp_enqueue_script( 'theone-jquery.touchwipe.min' );
            }
            
            wp_register_script( 'theone-nicescroll.min', THE_ONE_LIBS_URL . '/nicescroll/jquery.nicescroll.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-nicescroll.min' );
            
            wp_register_script( 'theone-SmoothScroll', THE_ONE_LIBS_URL . '/SmoothScroll/SmoothScroll.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-SmoothScroll' );
            
            wp_register_script( 'theone-jquery.countdown.min', THE_ONE_LIBS_URL . '/jquery.countdown/jquery.countdown.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.countdown.min' );
            
            wp_register_script( 'theone-skill-diagram', THE_ONE_JS . 'skill-diagram.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-skill-diagram' );
            
            wp_register_script( 'theone-custom', THE_ONE_JS . 'custom.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-custom' );
            
            wp_register_script( 'theone-skill-diagram', THE_ONE_JS . 'skill-diagram.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-skill-diagram' );
            
            wp_register_script( 'theone-google-map', $google_map_js_url, false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-google-map' );
            
            wp_register_script( 'theone-frontend-jquery', THE_ONE_JS . 'theone-frontend-jquery.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-frontend-jquery' );
            
            wp_localize_script( 'theone-frontend-jquery', 'theme_global_localize', $theme_global_localize );
            wp_localize_script( 'theone-woocomerce', 'ajaxurl', get_admin_url() . '/admin-ajax.php');
            
            if ( is_child_theme() ) {
                // js for frontend custom panel
                wp_register_script( 'theone-frontend-custom-panel', THE_ONE_LIBS_URL . '/frontend-custom-panel/frontend-custom-panel.js', false, THE_ONE_THEME_VERSION, true );
    			wp_enqueue_script( 'theone-frontend-custom-panel' );
                
                wp_register_script( 'theone-spectrum', THE_ONE_LIBS_URL . '/spectrum/spectrum.js', array( 'theone-frontend-custom-panel' ), THE_ONE_THEME_VERSION, true );
    			wp_enqueue_script( 'theone-spectrum' );   
            }
            
		}
	}
	add_action( 'wp_enqueue_scripts', 'ts_theone_js' );

}


if ( !function_exists( 'ts_theone_admin_js' ) ) {

	/*
	 * Load js
	*/
	function ts_theone_admin_js() {
        global $theme_global_localize;
        
        // Include frontend js in frontend edit mode
        if ( isset( $_REQUEST['vc_action'] ) ) {
            
            wp_register_script( 'theone-jquery.sidr.min', THE_ONE_LIBS_URL . '/sidr/jquery.sidr.min.js', false, THE_ONE_THEME_VERSION, true );
            wp_enqueue_script( 'theone-jquery.sidr.min' );
            
            wp_register_script( 'theone-frontend-jquery', THE_ONE_JS . 'theone-frontend-jquery.js', false, THE_ONE_THEME_VERSION, true );
			wp_enqueue_script( 'theone-frontend-jquery' );   
        }
        
        wp_register_script( 'theone-SmoothScroll', THE_ONE_LIBS_URL . '/SmoothScroll/SmoothScroll.js', false, THE_ONE_THEME_VERSION, true );
        wp_enqueue_script( 'theone-SmoothScroll' );
        
        wp_register_script( 'theone-admin-jquery', THE_ONE_JS . '/theone-admin-jquery.js', false, THE_ONE_THEME_VERSION, true );
        wp_enqueue_script( 'theone-admin-jquery' );
        wp_localize_script( 'theone-admin-jquery', 'theme_global_localize', $theme_global_localize );

	}
	add_action( 'admin_enqueue_scripts', 'ts_theone_admin_js' );

}


if ( !function_exists( 'ts_theone_css' ) ) {

	/*
	 * Load css
	*/
	function ts_theone_css() {
        global $theone;
        
        wp_register_style( 'theone-linea_arrows', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_arrows.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_arrows' );
        wp_register_style( 'theone-linea_basic', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_basic.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_basic' );
        wp_register_style( 'theone-linea_basic_elaboration', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_basic_elaboration.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_basic_elaboration' );
        wp_register_style( 'theone-linea_ecommerce', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_ecommerce.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_ecommerce' );
        wp_register_style( 'theone-linea_music', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_music.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_music' );
        wp_register_style( 'theone-linea_software', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_software.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_software' );
        wp_register_style( 'theone-linea_weather', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_weather.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_weather' );
        
        wp_register_style( 'theone-linea_weather', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_weather.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-linea_weather' );
               
        wp_register_style( 'theone-prettyPhoto', THE_ONE_LIBS_URL. '/pretty-photo/css/prettyPhoto.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-prettyPhoto' );
        
        // Add custom fonts, used in the main stylesheet.
        wp_enqueue_style( 'theone-fonts', theone_fonts_url(), array(), null );
        
        wp_register_style( 'theone-font-awesome.min', THE_ONE_LIBS_URL. '/font-awesome/css/font-awesome.min.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-font-awesome.min' );
        
        //wp_register_style( 'theone-stroke-gap-icons-style', THE_ONE_LIBS_URL. '/stroke-gap-icons/style.css', false, THE_ONE_THEME_VERSION, 'all');
//		wp_enqueue_style( 'theone-stroke-gap-icons-style' );
        
        //wp_register_style( 'theone-eleganticons-style', THE_ONE_LIBS_URL. '/eleganticons/style.css', false, THE_ONE_THEME_VERSION, 'all');
//		wp_enqueue_style( 'theone-eleganticons-style' );
        
        wp_register_style( 'theone-bootstrap.min', THE_ONE_LIBS_URL. '/bootstrap/css/bootstrap.min.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-bootstrap.min' );
        
        wp_register_style( 'theone-owl.carousel', THE_ONE_LIBS_URL. '/owl-carousel/owl.carousel.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-owl.carousel' );
        
        wp_register_style( 'theone-chosen.min', THE_ONE_LIBS_URL. '/chosen/chosen.min.css', false, THE_ONE_THEME_VERSION, 'all');
        wp_enqueue_style( 'theone-chosen.min' );
        
        wp_register_style( 'theone-jquery.sidr.light', THE_ONE_LIBS_URL. '/sidr/stylesheets/jquery.sidr.light.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-jquery.sidr.light' );
        
        wp_register_style( 'theone-style', THE_ONE_CSS. 'style.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-style' );
        
        wp_register_style( 'theone-dev', THE_ONE_CSS. 'dev.css', false, THE_ONE_THEME_VERSION, 'all');
		wp_enqueue_style( 'theone-dev' );
        
        if ( is_child_theme() && file_exists( get_stylesheet_directory() . '/assets/css/styles.css' ) ) {
            wp_enqueue_style( 'theone-assets-child-styles', get_stylesheet_directory_uri() . '/assets/css/styles.css', array( 'theone-styles', 'theone-custom' ) );
        }

        /**
         * Check if WooCommerce is active
         **/
        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
            // Put your plugin code here

            $chosen_css = plugins_url('/woocommerce/assets/css/chosen.css');
            $chosen_js = plugins_url('/woocommerce/assets/js/chosen/chosen.jquery.min.js');

            wp_register_style( 'woocommerce_chosen_styles', $chosen_css, false, THE_ONE_THEME_VERSION, 'screen' );
            wp_enqueue_style( 'woocommerce_chosen_styles' );

            wp_register_script('wc-chosen', $chosen_js, false, THE_ONE_THEME_VERSION, true);
            wp_enqueue_script( 'wc-chosen' );

            wp_register_style('ts-woocommerce', get_template_directory_uri(). '/woocommerce/woocommerce.css', false, THE_ONE_THEME_VERSION, 'screen' );
            wp_enqueue_style( 'ts-woocommerce' );
            
        }
        
        wp_enqueue_style( 'theone-customizer-style', admin_url( 'admin-ajax.php' ) . '?action=ts_enqueue_style_via_ajax' , false, THE_ONE_THEME_VERSION );
        
        if ( is_child_theme() ) {
            // Style for frontend custom panel
            wp_register_style( 'theone-frontend-custom-panel', THE_ONE_LIBS_URL. '/frontend-custom-panel/frontend-custom-panel.css', false, THE_ONE_THEME_VERSION, 'all' );
    		wp_enqueue_style( 'theone-frontend-custom-panel' );
            
            wp_register_style( 'theone-spectrum', THE_ONE_LIBS_URL. '/spectrum/spectrum.css', array( 'theone-frontend-custom-panel' ), THE_ONE_THEME_VERSION, 'all' );
    		wp_enqueue_style( 'theone-spectrum' );   
        }

	}
	add_action( 'wp_enqueue_scripts', 'ts_theone_css' );

}


if ( !function_exists( 'ts_theone_admin_css' ) ) {

	/*
	 * Load css
	*/
	function ts_theone_admin_css() {
	   
        $is_vc_edit_inline = isset( $_REQUEST['vc_action'] ); // Frontend
        $is_edit = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] == 'edit' : false;
        if ( $is_vc_edit_inline || $is_edit )  {
            wp_register_style( 'theone-linea_arrows', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_arrows.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_arrows' );
            wp_register_style( 'theone-linea_basic', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_basic.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_basic' );
            wp_register_style( 'theone-linea_basic_elaboration', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_basic_elaboration.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_basic_elaboration' );
            wp_register_style( 'theone-linea_ecommerce', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_ecommerce.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_ecommerce' );
            wp_register_style( 'theone-linea_music', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_music.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_music' );
            wp_register_style( 'theone-linea_software', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_software.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_software' );
            wp_register_style( 'theone-linea_weather', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_weather.css', false, THE_ONE_THEME_VERSION, 'all');
    		wp_enqueue_style( 'theone-linea_weather' );
        }
        
        // Linea fonts for backend
        $screen = get_current_screen();
        
        if ( isset( $screen->post_type ) ) {
            if ( $screen->post_type == 'animated_column' ) {
                
                wp_register_style( 'theone-linea_arrows', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_arrows.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_arrows' );
                wp_register_style( 'theone-linea_basic', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_basic.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_basic' );
                wp_register_style( 'theone-linea_basic_elaboration', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_basic_elaboration.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_basic_elaboration' );
                wp_register_style( 'theone-linea_ecommerce', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_ecommerce.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_ecommerce' );
                wp_register_style( 'theone-linea_music', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_music.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_music' );
                wp_register_style( 'theone-linea_software', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_software.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_software' );
                wp_register_style( 'theone-linea_weather', THE_ONE_LIBS_URL. '/linea-fonts/css/linea_weather.css', false, THE_ONE_THEME_VERSION, 'all');
        		wp_enqueue_style( 'theone-linea_weather' );
                
            }   
        }
        
        wp_register_style( 'theone-admin-style', THE_ONE_CSS. '/admin-style.css', false, THE_ONE_THEME_VERSION, 'screen');
    	wp_enqueue_style( 'theone-admin-style' );

	}
	add_action( 'admin_enqueue_scripts', 'ts_theone_admin_css' );

}

if ( !function_exists( 'ts_get_custom_style' ) ){
	
	/*
	 * get css custom
	*/
	function ts_get_custom_style() {
        global $theone;
        
	}
	add_action( 'wp_enqueue_scripts', 'ts_get_custom_style' );
	
}

?>