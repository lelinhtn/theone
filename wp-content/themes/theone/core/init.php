<?php
    
    /*
	 * Load import functions
	*/
	require_once get_template_directory() . "/core/import-functions.php";
        
	/*
	 * Load Theme options
	*/
	if ( file_exists( dirname( __FILE__ ) . '/theme_options.php' ) ) {
	    require_once dirname( __FILE__ ) . '/theme_options.php';
	}

	/*
	 * Be sure to rename this function to something more unique
	*/
	function removeDemoModeLink() {
	    if ( class_exists('ReduxFrameworkPlugin') ) {
	        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
	    }
	    if ( class_exists('ReduxFrameworkPlugin') ) {
	        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );   
	    }
	}
	add_action('init', 'removeDemoModeLink');
    
    $template_directory = get_template_directory();
    
	/*
	 * Load Required/Recommended Plugins
	*/
	require_once $template_directory . "/core/apis/class-tgm-plugin-activation.php";
	require_once $template_directory . "/core/plugins_load.php";
    
    
    /*
	 * Load global localize
	*/
	require_once $template_directory . "/core/theme_global_localize.php";

	/*
	 * Load Bootstrap Menu Walker
	*/
	require_once $template_directory . "/core/apis/ts_bootstrap_navwalker.php";

	/*
	 * Load fastwp Warker
	*/
	//require_once $template_directory . '/core/apis/fastwp_walker.php';
    
    /*
	 * Load flickr api
	*/
    require_once $template_directory . '/core/apis/api_flickr.php';
    
    /*
	 * Load customizer css and
	*/
	require_once $template_directory . "/core/theme_ajax_css.php";


	/*
	 * Load custom css and js
	*/
	require_once $template_directory . "/core/theme_styles_scripts.php";

	/*
	 * Load theme register
	*/
	require_once $template_directory . "/core/theme_register.php";

	/*
	 * Load theme support
	*/
	require_once $template_directory . "/core/theme_support.php";

	/*
	 * Load theme functions
	*/
	require_once $template_directory . "/core/theme_functions.php";
    
    /*
	 * Load theme ajax functions
	*/
    require_once $template_directory . "/core/theme_ajax.php";

	/*
	 * Load custom widgets
	*/
    require_once $template_directory . "/core/widgets/widget-lastest-tweets.php";
    require_once $template_directory . "/core/widgets/widget-new-posts.php";
    require_once $template_directory . "/core/widgets/widget-text-shortcode.php";
    require_once $template_directory . "/core/widgets/widget-login.php";
    
    /*
	 * Load theme breadcrumb
	*/
	require_once $template_directory . "/libs/breadcrumb/breadcrumb-trail.php";
    
    /*
	 * Load Theme Header Builder
	*/
    //require_once $template_directory . "/libs/header-builder/header-builder.php";
    
    
    /*
	 * Load Theme Coming Soon Template
	*/
    require_once $template_directory . "/core/theme_coming_soon_template.php";
    
    /*
	 * Load Theme One Click Import Data and Widgets
	*/
    //require_once $template_directory . "/libs/one-click-import-data/one-click-import-data.php";
    
    if ( is_child_theme() ) {
        /*
    	 * Load Frontend custom panel (for demo page only)
    	*/
        require_once $template_directory . "/libs/frontend-custom-panel/frontend-custom-panel.php";   
    }


?>