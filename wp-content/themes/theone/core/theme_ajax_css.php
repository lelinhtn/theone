<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}




function ts_enqueue_style_via_ajax() {
    global $theone;
    
    $base_color = isset( $theone['opt-general-accent-color'] ) ? $theone['opt-general-accent-color'] : array( 'color' => '#ffc40e', 'alpha' => 1 ); // Default is Contruction design base color (like yellow)
    $root_base_color = $base_color['color'];
    $base_color = ts_color_hex2rgba( $base_color['color'], $base_color['alpha'] );
    
    // For custom frontend if needed
    if ( isset( $_REQUEST['base_color'] ) ) {
        if ( trim( isset( $_REQUEST['base_color'] ) ) != '' ) {
            $base_color = trim( $_REQUEST['base_color'] );
            $root_base_color = $base_color;
        }
    } 
    
    header( 'Content-type: text/css; charset: UTF-8' );
    
    $css = '';
    
    // Override styles.css
    $css .= '';
    echo $css;
    
    wp_die();
}
add_action( 'wp_ajax_ts_enqueue_style_via_ajax', 'ts_enqueue_style_via_ajax' );
add_action( 'wp_ajax_nopriv_ts_enqueue_style_via_ajax', 'ts_enqueue_style_via_ajax' );


/**
 * Convert color hex to rgba format 
 **/
if ( !function_exists( 'ts_color_hex2rgba' ) ) {
    function ts_color_hex2rgba( $hex, $alpha = 1 ) {
        $hex = str_replace( "#", "", $hex );
        
        if( strlen($hex) == 3 ) {
          $r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
          $g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
          $b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
        } else {
          $r = hexdec( substr( $hex, 0, 2 ) );
          $g = hexdec( substr( $hex, 2, 2 ) );
          $b = hexdec( substr( $hex, 4, 2 ) );
        }
        $rgb = array( $r,  $g,  $b );
        return 'rgba( ' . implode( ', ', $rgb ) . ', ' . $alpha . ' )'; // returns the rgb values separated by commas
    }
}

if ( !function_exists( 'ts_color_rgb2hex' ) ) {
    function ts_color_rgb2hex( $rgb ) {
       $hex = '#';
       $hex .= str_pad( dechex($rgb[0]), 2, '0', STR_PAD_LEFT );
       $hex .= str_pad( dechex($rgb[1]), 2, '0', STR_PAD_LEFT );
       $hex .= str_pad( dechex($rgb[2]), 2, '0', STR_PAD_LEFT );
    
       return $hex; // returns the hex value including the number sign (#)
    }
}





