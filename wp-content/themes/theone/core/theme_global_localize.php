<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


// Define Global localize variable
global $theme_global_localize, $theone;

/**
 * Get login redirect URL 
 **/
function ts_get_login_redirect_url() {
    global $theone;
    $redirect = isset( $theone['opt-login-redirect'] ) ? esc_url( $theone['opt-login-redirect'] ) : '';
    $redirect = trim( $redirect ) == '' ? ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] : $redirect;
    return $redirect;
}


// theme_global_localize
$theme_global_localize = array(
    'is_mobile' => 'false',
    'products_filter_ajax' => get_option( 'ts_enable_products_ajax_filter', 'yes' ),
    'login_redirect_url' => ts_get_login_redirect_url(), 
    'custom_style_via_ajax_url' =>  admin_url( 'admin-ajax.php' ) . '?action=ts_enqueue_style_via_ajax',  
    'html'      => array(
        'countdown' =>  '<div class="row">
                            <div class="col-sm-3 col-xs-3">
                                <div class="counter-item">
                                    <span class="number">%D</span>
                                    <span class="lbl">' . __( 'Days', 'themestudio' ) . '</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                                <div class="counter-item">
                                    <span class="number">%H</span>
                                    <span class="lbl">' . __( 'Hours', 'themestudio' ) . '</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                                <div class="counter-item">
                                    <span class="number">%M</span>
                                    <span class="lbl">' . __( 'Minutes', 'themestudio' ) . '</span>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-3">
                                <div class="counter-item">
                                    <span class="number">%S</span>
                                    <span class="lbl">' . __( 'Seconds', 'themestudio' ) . '</span>
                                </div>
                            </div>
                        </div>'
    ),
    'text'      => array(
        'price'                         =>  __( 'Price', 'themestudio' ),
        'load_header_layout_default'    =>  __( 'Are you sure you want to load global header layout?', 'themestudio' )
    )
);

