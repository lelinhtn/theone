<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}






function ts_products_filter_via_ajax() {
    global $wpdb, $theone;
    
    $response = array(
        'products_wraps' => '',
        'result_count' => '',
        'pagination' => ''
    ); 
    
    //$min_price = isset( $_POST['min_price'] ) ? $_POST['min_price'] : '';
//    $max_price = isset( $_POST['max_price'] ) ? $_POST['max_price'] : '';
    $min_price = max( 0, intval( $_POST['min_price'] ) );
    $max_price = max( $min_price, intval( $_POST['max_price'] ) );
    $s = isset( $_POST['s'] ) ? sanitize_text_field( $_POST['s'] ) : '';
    $product_cat_search_slug = isset( $_POST['product_cat_search_slug'] ) ? trim( $_POST['product_cat_search_slug'] ) : '';
    $posttype = isset( $_POST['posttype'] ) ? $_POST['posttype'] : '';
    //$product_cat = isset( $_POST['product_cat'] ) ? $_POST['product_cat'] : '';
    //$product_tag = isset( $_POST['product_tag'] ) ? $_POST['product_tag'] : '';
    $queried_obj_tax = isset( $_POST['queried_obj_tax'] ) ? wc_clean( $_POST['queried_obj_tax'] ) : '';
    $queried_obj_id = isset( $_POST['queried_obj_id'] ) ? intval( $_POST['queried_obj_id'] ) : 0;
    $cur_orderby = isset( $_POST['orderby'] ) ? wc_clean( $_POST['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
    $orderby_value = $cur_orderby;
    $atts_filter = isset( $_POST['atts_filter'] ) ? $_POST['atts_filter'] : array();
    $view_type = isset( $_POST['view_type'] ) ? $_POST['view_type'] : '';
    $paged = isset( $_POST['paged'] ) ? max( 1, intval( $_POST['paged'] ) ) : 1;
    
    // Get order + orderby args from string
    $order = '';
	$orderby_value = explode( '-', $orderby_value ); 
	$orderby       = esc_attr( $orderby_value[0] );
	$order         = ! empty( $orderby_value[1] ) ? $orderby_value[1] : $order;
    
    $orderby = strtolower( $orderby );
	$order   = strtoupper( $order );
    
    $args = array(
        'post_type'     =>  'product',
        'post_status'   =>  'publish',
        'paged'         =>  $paged
    );
    
    // default - menu_order
	$args['orderby']  = 'menu_order title';
	$args['order']    = $order == 'DESC' ? 'DESC' : 'ASC';
	$args['meta_key'] = '';
    
    switch ( $orderby ) :
		case 'rand' :
			$args['orderby']  = 'rand';
		break;
		case 'date' :
			$args['orderby']  = 'date';
			$args['order']    = $order == 'ASC' ? 'ASC' : 'DESC';
		break;
		case 'price' :
			$args['orderby']  = "meta_value_num {$wpdb->posts}.ID";
			$args['order']    = $order == 'DESC' ? 'DESC' : 'ASC';
			$args['meta_key'] = '_price';
		break;
		case 'popularity' :
			$args['meta_key'] = 'total_sales';

			// Sorting handled later though a hook
			add_filter( 'posts_clauses', 'ts_order_by_popularity_post_clauses' );
		break;
		case 'rating' :
			// Sorting handled later though a hook
			add_filter( 'posts_clauses', 'ts_order_by_rating_post_clauses' );
		break;
		case 'title' :
			$args['orderby']  = 'title';
			$args['order']    = $order == 'DESC' ? 'DESC' : 'ASC';
		break;
	endswitch;
    
    $tax_query = array();
    
    if ( $queried_obj_tax != '' ):
        $tax_query[] = array(
            'taxonomy'   => $queried_obj_tax,
			'terms'      => $queried_obj_id,
            'field'      => 'id'
        );
    endif;
    
    // Filter atts
    if ( is_array( $atts_filter ) && !empty( $atts_filter ) ):
        
        foreach ( $atts_filter as $att_key => $att_ids ):
        
            $pa_attr = str_replace( 'filter_', 'pa_', $att_key );
            
            if ( trim( $att_ids ) != '' ) {
                $att_ids = explode( ',', $att_ids );
                
                if ( !empty( $att_ids ) ):
                
                    $tax_query[] = array(
            			'taxonomy'   => $pa_attr,
            			'terms'      => $att_ids,
                        'field'      => 'ids'
            		);
                    
                endif;
            }
        
        endforeach;
        
    endif;
    
    // If is search 
    if ( trim( $s ) != '' ):
    
        $args['s'] = $s;
        
        if ( $product_cat_search_slug != '' ):
            
            $tax_query[] = array(
                'taxonomy'  =>  'product_cat',
                'field'     =>  'slug',
                'terms'     =>  array( $product_cat_search_slug ),
                'include_children' => false
            );
            
        endif;
    
    endif;
    
    if ( count( $tax_query ) > 1 ):
        
        $tax_query['relation'] = 'AND';
        
    endif;
    
    if ( !empty( $tax_query ) ):
        
        $args['tax_query'] = $tax_query;
    
    endif;
    
    $meta_query = array();
    
    if ( $min_price <= $max_price && $min_price > 0 ):
        
        $meta_query = array(
            array(
                'key'       =>  '_price',
    			'value'     =>  array( $min_price, $max_price ),
                'compare'   =>  'BETWEEN',
                'type'      =>  'NUMERIC'
            )
		);
        
    endif;
    
    if ( !empty( $meta_query ) ):
        
        $args['meta_query'] = $meta_query;
    
    endif;
    
    $query_products = query_posts( $args );
    
    ob_start();
    if ( have_posts() ) {
        
        while ( have_posts() ):
            
            the_post();
            wc_get_template_part( 'content', 'product' );
        
        endwhile;
        
    }
    $response['products_wraps'] = ob_get_clean();
    
    ob_start();
    wc_get_template( 'loop/result-count.php' );
    $response['result_count'] = ob_get_clean();
    
    ob_start();
    /**
	 * woocommerce_after_shop_loop hook
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
    $response['pagination'] = ob_get_clean();
    
    wp_reset_query();
    
    wp_send_json( $response );
    
    
    die();
}
add_action( 'wp_ajax_ts_products_filter_via_ajax', 'ts_products_filter_via_ajax' );
add_action( 'wp_ajax_nopriv_ts_products_filter_via_ajax', 'ts_products_filter_via_ajax' );




function ts_do_login_via_ajax() {
    
    $response = array(
        'html' => '',
        'is_logged_in' => is_user_logged_in() ? 'yes' : 'no',
        'message' => ''
    );
    
    if ( $response['is_logged_in'] == 'yes' ) {
        $response['message'] = '<p class="text-primary bg-primary login-message">' . __( 'You are logged in!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : '';
    $user_pass = isset( $_POST['user_pass'] ) ? $_POST['user_pass'] : '';
    $rememberme = isset( $_POST['rememberme'] ) ? $_POST['rememberme'] == 'yes' : false;
    //$redirect_to = isset( $_POST['redirect_to'] ) ? esc_url( $_POST['redirect_to'] ) : '';
    $login_nonce = isset( $_POST['login_nonce'] ) ? $_POST['login_nonce'] : '';
    
    if ( !wp_verify_nonce( $login_nonce, 'ajax-login-nonce' ) ) {
        $response['message'] = '<p class="text-danger bg-danger login-message">' . __( 'Security check error!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die(); 
    }
    
    if ( trim( $user_login ) == '' ) {
        $response['message'] = '<p class="text-danger bg-danger login-message">' . __( 'User name can not be empty!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    $info = array();
    $info['user_login'] = $user_login;
    $info['user_password'] = $user_pass;
    $info['remember'] = $rememberme;
    
    $user_signon = wp_signon( $info, false );
    
    if ( is_wp_error( $user_signon ) ) {
        $response['message'] = '<p class="text-danger bg-danger login-message">' . __( 'Wrong username or password.', 'themestudio' ) . '</p>';
    } else {
        $response['is_logged_in'] = 'yes';
        $response['message'] = '<p class="text-success bg-success login-message">' . __( 'Login successful, redirecting...', 'themestudio' ) . '</p>';
    }
    
    wp_send_json( $response );
    
    die();
}
add_action( 'wp_ajax_nopriv_ts_do_login_via_ajax', 'ts_do_login_via_ajax' );
add_action( 'wp_ajax_ts_do_login_via_ajax', 'ts_do_login_via_ajax' );


function ts_do_register_via_ajax() {
    
    $response = array(
        'html' => '',
        'register_ok' => 'no',
        'message' => ''
    );
    
    $username = isset( $_POST['username'] ) ? $_POST['username'] : '';
    $email = isset( $_POST['email'] ) ? $_POST['email'] : '';
    $password = isset( $_POST['password'] ) ? $_POST['password'] : '';
    $repassword = isset( $_POST['repassword'] ) ? $_POST['repassword'] : '';
    $agree = isset( $_POST['agree'] ) ? $_POST['agree'] : 'no';
    $register_nonce = isset( $_POST['register_nonce'] ) ? $_POST['register_nonce'] : '';
    
    if ( !wp_verify_nonce( $register_nonce, 'ajax-register-nonce' ) ) {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'Security check error!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die(); 
    }
    
    if ( trim( $username ) == '' ) {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'User name can not be empty!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    if ( !is_email( $email ) ) {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'The Email Address is in an invalid format!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    if ( trim( $password ) == '' ) {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'Please enter a password!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    if ( trim( $password ) != trim( $repassword ) ) {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'Passwords did not match. Please try again!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    if ( trim( $agree ) != 'yes' ) {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'You must agree to our terms of use!', 'themestudio' ) . '</p>';
        wp_send_json( $response );
        die();
    }
    
    $user_id = username_exists( $username );
    
    if ( !$user_id and email_exists( $email ) == false ) {
    	$user_id = wp_create_user( $username, $password, $email );
        if ( !is_wp_error( $user_id ) ) {
            $response['register_ok'] = 'yes';
            $response['message'] = '<p class="text-success bg-success register-message">' . __( 'Thank you! Register successful, now you can login.', 'themestudio' ) . '</p>';
        }
        else{
            $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'Registration failed. Please try again latter!', 'themestudio' ) . '</p>';
        }
    } else {
        $response['message'] = '<p class="text-danger bg-danger register-message">' . __( 'User already exists.', 'themestudio' ) . '</p>';
    }
    
    wp_send_json( $response );
    
    die();
}
add_action( 'wp_ajax_nopriv_ts_do_register_via_ajax', 'ts_do_register_via_ajax' );


function ts_retrieve_password_via_ajax() {
	global $wpdb, $wp_hasher;
    
    $response = array(
        'html'      =>  '',
        'message'   =>  ''
    );
    
	$errors = new WP_Error();
    
    // User can reset password by user name or user email
    $user_name_post = isset( $_POST['username'] ) ? $_POST['username'] : '';
    $user_email_post = isset( $_POST['email'] ) ? $_POST['email']: '';
    if ( !username_exists( $user_name_post ) ) {
        if ( email_exists( $user_email_post ) ) {
            $user_name_post = $user_email_post;
        }
    }
    else {
        if ( trim( $user_name_post ) != '' && trim( $user_email_post ) != '' ) {
            $user = get_user_by( 'login', $user_name_post );
            if ( trim( strtolower( $user_email_post ) ) != trim( strtolower( $user->data->user_email ) ) ) {
                $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( '<strong>ERROR</strong>: User <strong>' . sanitize_text_field( $user_name_post ) . '</strong> does not match with email <strong>' . sanitize_email( $user_email_post ) . '</strong>', 'themestudio' ) . '</p>';
                wp_send_json( $response );
            }
        }
    }

	if ( empty( $user_name_post ) ) {
		$errors->add( 'empty_username', __( '<strong>ERROR</strong>: Enter a username or e-mail address.', 'themestudio' ) );
        $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( '<strong>ERROR</strong>: Enter a username or e-mail address.', 'themestudio' ) . '</p>';
	} elseif ( strpos( $user_name_post, '@' ) ) {
		$user_data = get_user_by( 'email', trim( $user_name_post ) );
		if ( empty( $user_data ) ) {
            $errors->add( 'invalid_email', __( '<strong>ERROR</strong>: There is no user registered with that email address.', 'themestudio' ) );
            $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( '<strong>ERROR</strong>: There is no user registered with that email address.', 'themestudio' ) . '</p>'; 
		}
	} else {
		$login = trim($user_name_post);
		$user_data = get_user_by('login', $login);
	}

	/**
	 * Fires before errors are returned from a password reset request.
	 *
	 * @since 2.1.0
	 */
	do_action( 'lostpassword_post' );

	if ( $errors->get_error_code() ) {
	   wp_send_json( $response );  
	}

	if ( !$user_data ) {
		$errors->add( 'invalidcombo', __('<strong>ERROR</strong>: Invalid username or e-mail.', 'themestudio' ) );
        $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( '<strong>ERROR</strong>: Invalid username or e-mail.', 'themestudio' ) . '</p>';
		wp_send_json( $response );
	}

	// Redefining user_login ensures we return the right case in the email.
	$user_login = $user_data->user_login;
	$user_email = $user_data->user_email;

	/**
	 * Fires before a new password is retrieved.
	 *
	 * @since 1.5.0
	 * @deprecated 1.5.1 Misspelled. Use 'retrieve_password' hook instead.
	 *
	 * @param string $user_login The user login name.
	 */
	do_action( 'retreive_password', $user_login );

	/**
	 * Fires before a new password is retrieved.
	 *
	 * @since 1.5.1
	 *
	 * @param string $user_login The user login name.
	 */
	do_action( 'retrieve_password', $user_login );

	/**
	 * Filter whether to allow a password to be reset.
	 *
	 * @since 2.7.0
	 *
	 * @param bool true           Whether to allow the password to be reset. Default true.
	 * @param int  $user_data->ID The ID of the user attempting to reset a password.
	 */
	$allow = apply_filters( 'allow_password_reset', true, $user_data->ID );

	if ( ! $allow ) {
		$err = new WP_Error( 'no_password_reset', __( 'Password reset is not allowed for this user', 'themestudio' ) );
        $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( 'Password reset is not allowed for this user', 'themestudio' ) . '</p>';
        wp_send_json( $response );
	} elseif ( is_wp_error( $allow ) ) {
	    $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( 'Unknow error', 'themestudio' ) . '</p>';
		wp_send_json( $response );
	}

	// Generate something random for a password reset key.
	$key = wp_generate_password( 20, false );

	/**
	 * Fires when a password reset key is generated.
	 *
	 * @since 2.5.0
	 *
	 * @param string $user_login The username for the user.
	 * @param string $key        The generated password reset key.
	 */
	do_action( 'retrieve_password_key', $user_login, $key );

	// Now insert the key, hashed, into the DB.
	if ( empty( $wp_hasher ) ) {
		require_once ABSPATH . WPINC . '/class-phpass.php';
		$wp_hasher = new PasswordHash( 8, true );
	}
	$hashed = $wp_hasher->HashPassword( $key );
	$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

	$message = __( 'Someone requested that the password be reset for the following account:', 'themestudio' ) . "\r\n\r\n";
	$message .= network_home_url( '/' ) . "\r\n\r\n";
	$message .= sprintf( __( 'Username: %s', 'themestudio' ), $user_login ) . "\r\n\r\n";
	$message .= __( 'If this was a mistake, just ignore this email and nothing will happen.', 'themestudio' ) . "\r\n\r\n";
	$message .= __( 'To reset your password, visit the following address:', 'themestudio' ) . "\r\n\r\n";
	$message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

	if ( is_multisite() )
		$blogname = $GLOBALS['current_site']->site_name;
	else
		/*
		 * The blogname option is escaped with esc_html on the way into the database
		 * in sanitize_option we want to reverse this for the plain text arena of emails.
		 */
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

	$title = sprintf( __( '[%s] Password Reset', 'themestudio' ), $blogname );

	/**
	 * Filter the subject of the password reset email.
	 *
	 * @since 2.8.0
	 *
	 * @param string $title Default email title.
	 */
	$title = apply_filters( 'retrieve_password_title', $title );

	/**
	 * Filter the message body of the password reset mail.
	 *
	 * @since 2.8.0
	 * @since 4.1.0 Added `$user_login` and `$user_data` parameters.
	 *
	 * @param string  $message    Default mail message.
	 * @param string  $key        The activation key.
	 * @param string  $user_login The username for the user.
	 * @param WP_User $user_data  WP_User object.
	 */
	$message = apply_filters( 'retrieve_password_message', $message, $key, $user_login, $user_data );

	if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) ) {
	   //wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );
       $response['message'] = '<p class="text-danger bg-danger forgot-message">' . __( 'The e-mail could not be sent. Possible reason: your host may have disabled the mail() function.', 'themestudio' ) . '</p>';  
	}
    else {
        $response['message'] = '<p class="text-success bg-success register-message">' . __( 'Check your e-mail for the confirmation link.', 'themestudio' ) . '</p>';
    }
    
    wp_send_json( $response );
    die();
}
add_action( 'wp_ajax_nopriv_ts_retrieve_password_via_ajax', 'ts_retrieve_password_via_ajax' );


if ( !function_exists( 'ts_added_to_cart_via_ajax' ) ) {
    function ts_added_to_cart_via_ajax() {
        
        $data = array(
        	'fragments' => '',
        	'cart_hash' => ''
        );
        
        if ( class_exists( 'WooCommerce' ) ):
            
            $args = array(
        		'list_class' => ''
        	);
            
            ob_start();
        	wc_get_template( 'cart/mini-cart.php', $args );
            $mini_cart = ob_get_clean();
            $data = array(
        		'fragments' => apply_filters( 'add_to_cart_fragments', array(
        				'mini_cart' => $mini_cart
        			)
        		),
        		'cart_hash' => WC()->cart->get_cart() ? md5( json_encode( WC()->cart->get_cart() ) ) : ''
        	);
            
        endif;
        
        wp_send_json( $data );
        
        die();

    }
    add_action( 'wp_ajax_nopriv_ts_added_to_cart_via_ajax', 'ts_added_to_cart_via_ajax' );
    add_action( 'wp_ajax_ts_added_to_cart_via_ajax', 'ts_added_to_cart_via_ajax' );
}







