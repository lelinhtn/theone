<?php
/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://github.com/ReduxFramework/ReduxFramework/wiki
 * */

if (!class_exists( 'TheOne_Redux_Framework_config' )) {

    class TheOne_Redux_Framework_config {

        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if ( !class_exists("ReduxFramework" ) ) {
                return;
            }

            $this->initSettings();
        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

            // Function to test the compiler hook and demo CSS output.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            // Dynamically add a section. Can be also used to modify sections/fields
            add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field   set with compiler=>true is changed.

         * */
        function compiler_action($options, $css) {
            
        }
        
        function ts_redux_update_options_user_can_register( $options, $css ) {
            global $theone;
            $users_can_register = isset( $theone['opt-users-can-register'] ) ? $theone['opt-users-can-register'] : 0;
            update_option( 'users_can_register', $users_can_register );
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => __('Section via hook', 'themestudio'),
                'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'themestudio'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = "Testing filter hook!";

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));

            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode(".", $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[] = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct = wp_get_theme();
            $this->theme = $ct;
            $item_name = $this->theme->get('Name');
            $tags = $this->theme->Tags;
            $screenshot = $this->theme->get_screenshot();
            $class = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'themestudio'), $this->theme->display('Name'));
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
            <?php endif; ?>

                <h4>
            <?php echo $this->theme->display('Name'); ?>
                </h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'themestudio'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'themestudio'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'themestudio') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
                <?php
                if ($this->theme->parent()) {
                    printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'themestudio'), $this->theme->parent()->display('Name'));
                }
                ?>

                </div>

            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $sampleHTML = '';
            if (file_exists(dirname(__FILE__) . '/info-html.html')) {
                /** @global WP_Filesystem_Direct $wp_filesystem  */
                global $wp_filesystem;
                if ( empty( $wp_filesystem ) ) {
                    require_once ABSPATH . '/wp-admin/includes/file.php';
                    WP_Filesystem();
                }
                $sampleHTML = $wp_filesystem->get_contents( dirname(__FILE__) . '/info-html.html' );
            }


            
            /*--General Settings--*/
            $this->sections[] = array(
                'icon' => 'el-icon-cogs',
                'title' => __('General Settings', 'themestudio'),
                'fields' => array(
                    array(
                        'id'    => 'opt-general-introduction',
                        'type'  => 'info',
                        'style' => 'success',
                        'title' => __('Welcome to THE ONE Theme Option Panel', 'redux-framework-demo'),
                        'icon'  => 'el-icon-info-sign',
                        'desc'  => __( 'From here you can config THE ONE theme in the way you need.', 'redux-framework-demo')
                    ),
                    array(
                        'id' => 'opt-general-logo',
                        'type' => 'media',
                        'url' => true,
                        'title' => __('Logo Upload', 'themestudio'),
                        'compiler' => 'true',
                        //'mode' => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                        'desc' => __('Upload your logo image', 'themestudio'),
                        'subtitle' => __('Upload your custom logo image', 'themestudio'),
                        'default' => array( 'url' => get_template_directory_uri() . '/assets/images/logo.png' ),
                        'hint' => array(
                            'title'     => 'Hint Title',
                            'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                        )
                    ),
                    array(
                        'id' => 'opt-general-favicon',
                        'type' => 'media',
                        'title' => __('Upload favicon', 'themestudio'),
                        'desc' => __('Upload a 16px x 16px .png or .gif image that will be your favicon.', 'themestudio'),
                        'subtitle' => __('Upload your custom favicon image', 'themestudio'),
                        'default' => array( 'url' => get_template_directory_uri() . '/assets/images/favicon.png' ),

                    ),
                    array(
                        'id'        => 'opt-general-accent-color',
                        'type'      => 'color_rgba',
                        'title'     => __('Base color', 'themestudio'),
                        'subtitle'  => __('Base color for all pages on the frontend', 'redux-framework-demo'),
                        'default'   => array(
                            'color'     => '#ffc40e', // Contruction design
                            'alpha'     => 1
                        ),
                        'options'       => array(
                            'show_input'                => true,
                            'show_initial'              => true,
                            'show_alpha'                => false,
                            'show_palette'              => true,
                            'show_palette_only'         => false,
                            'show_selection_palette'    => true,
                            'max_palette_size'          => 10,
                            'allow_empty'               => true,
                            'clickout_fires_change'     => false,
                            'choose_text'               => __( 'Choose', 'themestudio' ),
                            'cancel_text'               => __( 'Cancel', 'themestudio' ),
                            'show_buttons'              => true,
                            'use_extended_classes'      => true,
                            'palette'                   => array(
                                '#ffc40e', '#03529f', '#3e50b4', '#fe5621', '#d22e2e', '#bf8b5b', '#0474df', '#fe9700', '#c0392b', '#f39c12', '#2b54ae'
                                // Construction, Plumber, Mechanic, Cleaning, Autoshop, Carpenter, Maintenance, Metal_Construction, Electrician, Mining, Renovation 
                            ),
                            'input_text'                => __( 'Select Color', 'themestudio' )
                        ), 
                    ),
                    array(
                        'id' => 'opt-enable-page-preload',
                        'type' => 'switch',
                        'title' => __('Enable page preload', 'themestudio'),
                        'subtitle' => __('Turn on/off page preload', 'themestudio'),
                        'default' => 1,
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                    ),
                    array(
                        'id'       => 'opt-site-layout',
                        'type'     => 'select',
                        'multi'    => false,
                        'title'    => __('Site layout', 'themestudio'),
                        'options'  => array(
                            'fullwidth' => __( 'Fullwidth', 'themestudio' ),
                            'box'       => __( 'Box', 'themestudio' )
                        ),
                        'default' => 'fullwidth'
                    ),
                    array(
                        'id'       => 'opt-box-padding',
                        'type'     => 'switch',
                        'multi'    => false,
                        'title'    => __('Box padding', 'themestudio'),
                        'default' => 1,
                        'on' => 'Yes',
                        'off' => 'No',
                        'required'      => array( 'opt-site-layout', '=', 'box' ),
                    ),
                    array(         
                        'id'       => 'opt-body-box-background',
                        'type'     => 'background',
                        'title'    => __('Body Background', 'themestudio'),
                        'subtitle' => __('Body background with image, color, etc.', 'themestudio'),
                        'desc'     => __('Body box background', 'themestudio'),
                        'default'  => array(
                            'background-color' => '#ffffff',
                        ),
                        'output'  => array( 
                            'background' => 'body.body-box' 
                        ),
                        'required'      => array( 'opt-site-layout', '=', 'box' ),
                    ),
                    array(         
                        'id'       => 'opt-box-site-main-background',
                        'type'     => 'background',
                        'title'    => __('Box Background', 'themestudio'),
                        'subtitle' => __('Box background with image, color, etc.', 'themestudio'),
                        'desc'     => __('Box background', 'themestudio'),
                        'default'  => array(
                            'background-color' => '#ffffff',
                        ),
                        'output'  => array( 
                            'background' => 'body.body-box > .site-main' 
                        ),
                        'required'      => array( 'opt-site-layout', '=', 'box' ),
                    ),
                    array(
                        'id' => 'opt-general-css-code',
                        'type' => 'ace_editor',
                        'title' => __('Custom CSS', 'themestudio'),
                        'subtitle' => __('Paste your custom CSS code here.', 'themestudio'),
                        'mode' => 'css',
                        'theme' => 'monokai',
                        'desc' => 'Custom css code.',
                        'default' => ""
                    ),
                    array(
                        'id' => 'opt-general-js-code',
                        'type' => 'ace_editor',
                        'title' => __('Custom JS ', 'themestudio'),
                        'subtitle' => __('Paste your custom JS code here.', 'themestudio'),
                        'mode' => 'javascript',
                        'theme' => 'chrome',
                        'desc' => 'Custom javascript code',
                        'default' => "jQuery(document).ready(function(){\n\n});"
                    )
                )
            ); 

            /*--Typograply Options--*/
            $this->sections[] = array(
                'icon' => 'el-icon-font',
                'title' => __('Typography Options', 'themestudio'),
                'fields' => array(
                    array(
                        'id' => 'opt-typography-body-font',
                        'type' => 'typography',
                        'title' => __('Body Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the body font properties.', 'themestudio'),
                        'google' => true,
                        'output' => 'body',
                        'default' => array(
                            'font-family' => 'Ubuntu',
                        ),
                    ),
                    array(
                        'id' => 'opt-typography-menu-font',
                        'type' => 'typography',
                        'title' => __('Menu Item Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the menu font properties.', 'themestudio'),
                        'output' => array( 'nav' ),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway'
                        ),
                    ),
                 
                    array(
                        'id' => 'opt-typography-h1-font',
                        'type' => 'typography',
                        'title' => __('Heading 1(H1) Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the H1 tag font properties.', 'themestudio'),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway',
                            'font-weight' => 700
                        ),
                        'output' => 'h1',
                    ),
      
                    array(
                        'id' => 'opt-typography-h2-font',
                        'type' => 'typography',
                        'title' => __('Heading 2(H2) Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the H2 tag font properties.', 'themestudio'),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway',
                            'font-weight' => 700
                        ),
                        'output' => 'h2',
                    ),
      
                    array(
                        'id' => 'opt-typography-h3-font',
                        'type' => 'typography',
                        'title' => __('Heading 3(H3) Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the H3 tag font properties.', 'themestudio'),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway',
                            'font-weight' => 700
                        ),
                        'output' => 'h3',
                    ),
      
                    array(
                        'id' => 'opt-typography-h4-font',
                        'type' => 'typography',
                        'title' => __('Heading 4(H4) Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the H4 tag font properties.', 'themestudio'),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway',
                            'font-weight' => 700
                        ),
                        'output' => 'h4',
                    ),
      
                    array(
                        'id' => 'opt-typography-h5-font',
                        'type' => 'typography',
                        'title' => __('Heading 5(H5) Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the H5 tag font properties.', 'themestudio'),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway',
                            'font-weight' => 700
                        ),
                        'output' => 'h5',
                    ),

                    array(
                        'id' => 'opt-typography-h6-font',
                        'type' => 'typography',
                        'title' => __('Heading 6(H6) Font Setting', 'themestudio'),
                        'subtitle' => __('Specify the H6 tag font properties.', 'themestudio'),
                        'google' => true,
                        'default' => array(
                            'font-family' => 'Raleway',
                            'font-weight' => 700
                        ),
                        'output' => 'h6',
                    ),
                )
            );
            
            /*--Header setting--*/
            /*$this->sections[] = array(
                'title' => __('Color Schemes', 'themestudio'),
                'desc' => __('Color Schemes', 'themestudio'),
                'icon' => 'el-icon-credit-card',
                'fields' => array(
                    array(
                        'id'        => 'opt-base-color',
                        'type'      => 'color_rgba',
                        'title'     => __('Base color', 'themestudio'),
                        'subtitle'  => __('Base color for all pages on the frontend', 'redux-framework-demo'),
                        'default'   => array(
                            'color'     => '#ffc40e', // Contruction design
                            'alpha'     => 1
                        ),
                        'options'       => array(
                            'show_input'                => true,
                            'show_initial'              => true,
                            'show_alpha'                => false,
                            'show_palette'              => true,
                            'show_palette_only'         => false,
                            'show_selection_palette'    => true,
                            'max_palette_size'          => 10,
                            'allow_empty'               => true,
                            'clickout_fires_change'     => false,
                            'choose_text'               => 'Choose',
                            'cancel_text'               => 'Cancel',
                            'show_buttons'              => true,
                            'use_extended_classes'      => true,
                            'palette'                   => array(
                                '#ffc40e', '#03529f', '#3e50b4', '#fe5621', '#d22e2e', '#bf8b5b', '#0474df', '#fe9700', '#c0392b', '#f39c12', '#2b54ae'
                                // Construction, Plumber, Mechanic, Cleaning, Autoshop, Carpenter, Maintenance, Metal_Construction, Electrician, Mining, Renovation 
                            ),
                            'input_text'                => 'Select Color'
                        ), 
                    ),
                )
            );*/
            


            /*--Footer setting--*/
            $this->sections[] = array(
                'title' => __('Footer Options', 'themestudio'),
                'desc' => __('Footer Options', 'themestudio'),
                'icon' => 'el-icon-credit-card',
                'fields' => array(
                    // Start footer 1 section 
                    array(
                        'id' => 'opt-footer-top-section-start',
                        'type' => 'section',
                        'title' => __( 'Footer Section 1', 'themestudio' ),
                        'subtitle' => __('', 'themestudio' ),
                        'indent' => true 
                    ),
                    array(
                        'id'        => 'opt-footer-top-position',
                        'type'      => 'select',
                        'title'     => __('Footer 1 Position', 'themestudio'),
                        'subtitle'  => __('Select position for footer 1 part', 'themestudio'),
                        'options'   => array(
                            'top'   => __( 'Top', 'themestudio' ),
                            'bottom' => __( 'Bottom', 'themestudio' )
                        ),
                        'default'   => 'top'
                    ),
                    array(
                        'id'        => 'opt-footer-top-sidebar-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Footer top sidebar layout', 'themestudio'),
                        'subtitle'  => __('Select number of footer top sidebars.', 'themestudio'),
                        'options'   => array(
                            '1' => array('alt' => '1 Column ',      'img' =>get_template_directory_uri() . '/assets/images/1column.png'),
                            '2' => array('alt' => '2 Columns',      'img' =>get_template_directory_uri() . '/assets/images/2columns.png'),
                            '3' => array('alt' => '3 Columns',      'img' =>get_template_directory_uri() . '/assets/images/3columns.png'),
                            '4' => array('alt' => '4 Columns',      'img' =>get_template_directory_uri() . '/assets/images/4columns.png')
                        ),
                        'default'   => '2'
                    ),
                    array(
                        'id'        => 'opt-footer-top-wg-1-cols',
                        'type'      => 'select',
                        'compiler'  => true,
                        'title'     => __('Widget 1 columns width', 'themestudio'),
                        'subtitle'  => __('Select number of columns width for this widget', 'themestudio'),
                        'options'  => array(
                            '12' => __( '12/12 columns', 'themestudio' ),
                            '11' => __( '11/12 columns', 'themestudio' ),
                            '10' => __( '10/12 columns', 'themestudio' ),
                            '9' => __( '9/12 columns', 'themestudio' ),
                            '8' => __( '8/12 columns', 'themestudio' ),
                            '7' => __( '7/12 columns', 'themestudio' ),
                            '6' => __( '6/12 columns', 'themestudio' ),
                            '5' => __( '5/12 columns', 'themestudio' ),
                            '4' => __( '4/12 columns', 'themestudio' ),
                            '3' => __( '3/12 columns', 'themestudio' ),
                            '2' => __( '2/12 columns', 'themestudio' )
                        ),
                        'default'   => '8'
                    ), 
                    array(
                        'id'        => 'opt-footer-top-wg-2-cols',
                        'type'      => 'select',
                        'compiler'  => true,
                        'title'     => __('Widget 2 columns width', 'themestudio'),
                        'subtitle'  => __('Select number of columns width for this widget', 'themestudio'),
                        'options'  => array(
                            '12' => __( '12/12 columns', 'themestudio' ),
                            '11' => __( '11/12 columns', 'themestudio' ),
                            '10' => __( '10/12 columns', 'themestudio' ),
                            '9' => __( '9/12 columns', 'themestudio' ),
                            '8' => __( '8/12 columns', 'themestudio' ),
                            '7' => __( '7/12 columns', 'themestudio' ),
                            '6' => __( '6/12 columns', 'themestudio' ),
                            '5' => __( '5/12 columns', 'themestudio' ),
                            '4' => __( '4/12 columns', 'themestudio' ),
                            '3' => __( '3/12 columns', 'themestudio' ),
                            '2' => __( '2/12 columns', 'themestudio' )
                        ),
                        'default'   => '4',
                        'required' => array('opt-footer-top-sidebar-layout', 'greater', 1),
                    ),  
                    array(
                        'id'        => 'opt-footer-top-wg-3-cols',
                        'type'      => 'select',
                        'compiler'  => true,
                        'title'     => __('Widget 3 columns width', 'themestudio'),
                        'subtitle'  => __('Select number of columns width for this widget', 'themestudio'),
                        'options'  => array(
                            '12' => __( '12/12 columns', 'themestudio' ),
                            '11' => __( '11/12 columns', 'themestudio' ),
                            '10' => __( '10/12 columns', 'themestudio' ),
                            '9' => __( '9/12 columns', 'themestudio' ),
                            '8' => __( '8/12 columns', 'themestudio' ),
                            '7' => __( '7/12 columns', 'themestudio' ),
                            '6' => __( '6/12 columns', 'themestudio' ),
                            '5' => __( '5/12 columns', 'themestudio' ),
                            '4' => __( '4/12 columns', 'themestudio' ),
                            '3' => __( '3/12 columns', 'themestudio' ),
                            '2' => __( '2/12 columns', 'themestudio' )
                        ),
                        'default'   => '3',
                        'required' => array('opt-footer-top-sidebar-layout', 'greater', 2),
                    ),    
                    array(
                        'id'        => 'opt-footer-top-wg-4-cols',
                        'type'      => 'select',
                        'compiler'  => true,
                        'title'     => __('Widget 4 columns width', 'themestudio'),
                        'subtitle'  => __('Select number of columns width for this widget', 'themestudio'),
                        'options'  => array(
                            '12' => __( '12/12 columns', 'themestudio' ),
                            '11' => __( '11/12 columns', 'themestudio' ),
                            '10' => __( '10/12 columns', 'themestudio' ),
                            '9' => __( '9/12 columns', 'themestudio' ),
                            '8' => __( '8/12 columns', 'themestudio' ),
                            '7' => __( '7/12 columns', 'themestudio' ),
                            '6' => __( '6/12 columns', 'themestudio' ),
                            '5' => __( '5/12 columns', 'themestudio' ),
                            '4' => __( '4/12 columns', 'themestudio' ),
                            '3' => __( '3/12 columns', 'themestudio' ),
                            '2' => __( '2/12 columns', 'themestudio' )
                        ),
                        'default'   => '4',
                        'required' => array('opt-footer-top-sidebar-layout', 'greater', 3),
                    ), 
                    
                    array(
                        'id'        =>  'opt-footer-top-border',
                        'type'      =>  'switch',
                        'title'     =>  __( 'Show border', 'themestudio' ),
                        'default'   =>  0,
                        'on'        =>  __( 'Show', 'themestudio' ),
                        'off'       =>  __( 'Don\'t show', 'themestudio' )
                    ),
                    
                    // End footer 1 section 
                    array(
                        'id' => 'opt-footer-top-section-end',
                        'type' => 'section',
                        'indent' => false
                    ),
                    
                    
                    // Start footer 2 section 
                    array(
                        'id' => 'opt-footer-bottom-section-start',
                        'type' => 'section',
                        'title' => __( 'Footer Section 2', 'themestudio' ),
                        'subtitle' => __('', 'themestudio' ),
                        'indent' => true 
                    ),                          
                    array(
                        'id'        => 'opt-footer-sidebar-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Footer bottom sidebar layout', 'themestudio'),
                        'subtitle'  => __('Select number of footer bottom sidebars.', 'themestudio'),
                        'options'   => array(
                            '1' => array('alt' => '1 Column ',       'img' =>get_template_directory_uri() . '/assets/images/1column.png'),
                            '2' => array('alt' => '2 Columns',      'img' =>get_template_directory_uri() . '/assets/images/2columns.png'),
                            '3' => array('alt' => '3 Columns',      'img' =>get_template_directory_uri() . '/assets/images/3columns.png'),
                            '4' => array('alt' => '4 Columns',      'img' =>get_template_directory_uri() . '/assets/images/4columns.png')
                        ),
                        'default'   => '4'
                    ),
                    array(
                        'id'        =>  'opt-footer-bottom-border',
                        'type'      =>  'switch',
                        'title'     =>  __( 'Show border', 'themestudio' ),
                        'default'   =>  0,
                        'on'        =>  __( 'Show', 'themestudio' ),
                        'off'       =>  __( 'Don\'t show', 'themestudio' )
                    ),
                    
                    // End footer 2 section 
                    array(
                        'id' => 'opt-footer-bottom-section-end',
                        'type' => 'section',
                        'indent' => false
                    ),
                    
                    
                    array(
                        'id'        =>  'opt-enable-sep-between-2-footer-part',
                        'type'      =>  'switch',
                        'title'     =>  __( 'Show separator between two footer parts', 'themestudio' ),
                        'default'   =>  0,
                        'on'        =>  __( 'Show', 'themestudio' ),
                        'off'       =>  __( 'Don\'t show', 'themestudio' )
                    ),
                    
                    array(
                        'id' => 'opt-footer-copyright-text',
                        'type' => 'editor',
                        'title' => __('Footer copyright Text', 'themestudio'),
                        'subtitle' => __('Copyright text', 'themestudio'),
                        'default' => __( 'The One &copy; All Rights Reserved. With Love by <a href="http://7oroof.com">7oroof.com</a><label>&nbsp;&nbsp;&nbsp;</label><label class="footer-link"><a href="#">Privacy Policy</a> - <a href="#">Terms of Use</a></label>', 'themestudio' ),
                    ),
                    array(
                        'id' => 'opt-footer-social-icon',
                        'type' => 'switch',
                        'title' => __('Show Social Icon', 'themestudio'),
                        'subtitle' => __('Social icon on/off', 'themestudio'),
                        "default" => 1,
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                    ),
                )
            );
            
            /*--Social Settings--*/
            $this->sections[] = array(
                'title' => __('Social Settings', 'themestudio'),
                'desc' => __('', 'themestudio'),
                'icon' => 'el-icon-credit-card',
                'fields' => array(
                    array(
                        'id' => 'opt-fb-link',
                        'type' => 'text',
                        'title' => __('Facebook', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                    array(
                        'id' => 'opt-google-plus-link',
                        'type' => 'text',
                        'title' => __('Google Plus', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                    array(
                        'id' => 'opt-twitter-link',
                        'type' => 'text',
                        'title' => __('Twitter', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                    array(
                        'id' => 'opt-youtube-link',
                        'type' => 'text',
                        'title' => __('Youtube', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                    array(
                        'id' => 'opt-vimeo-link',
                        'type' => 'text',
                        'title' => __('Vimeo', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                    array(
                        'id' => 'opt-linkedin-link',
                        'type' => 'text',
                        'title' => __('Linkedin', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                    array(
                        'id' => 'opt-rss-link',
                        'type' => 'text',
                        'title' => __('RSS', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '',
                        'validate' => 'url'
                    ),
                )
            );

        
            /*--Blog--*/
            $this->sections[] = array(
                'title' => __('Blog Settings', 'themestudio'),
                'desc' => __('Blog Settings', 'themestudio'),
                'icon' => 'el-icon-th-list',
                'fields' => array(
                    array(
                        'id'            => 'opt-blog-title',
                        'type'          => 'text',
                        'title'         => __('Blog Title', 'themestudio'),
                        'subtitle'      => __('Show title blog page', 'themestudio'),
                        'default'       => 'Blog',
                    ),
                    array(
                        'id'            => 'opt-blog-continue-reading',
                        'type'          => 'text',
                        'title'         => __('Continue reading', 'themestudio'),
                        'subtitle'      => __('Continue reading text', 'themestudio'),
                        'default'       => __( 'Read more', 'themestudio' ),
                    ),
                    array(
                        'id'        => 'opt-blog-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Sidebar position', 'themestudio'),
                        'subtitle'  => __('Select sidebar position.', 'themestudio'),
                        'desc' => __('Select sidebar on left or right', 'themestudio'),
                        'options'   => array(
                            '1' => array('alt' => '1 Column Left',      'img' => get_template_directory_uri() . '/assets/images/2cl.png'),
                            '2' => array('alt' => '2 Column Right',     'img' => get_template_directory_uri() . '/assets/images/2cr.png'),
                            '3' => array('alt' => 'Full Width',         'img' => get_template_directory_uri() . '/assets/images/1column.png'),
                        ),
                        'default'   => '2',
                    ),
                    array(
                        'id'        => 'opt-blog-layout-style',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Blog layout style', 'themestudio'),
                        'subtitle'  => __('Select blog layout style.', 'themestudio'),
                        'desc' => __('Select sidebar on left or right', 'themestudio'),
                        'options'   => array(
                            'list' => array('alt' => 'Blog List',      'img' => get_template_directory_uri() . '/assets/images/list.png'),
                            'grid' => array('alt' => 'Blog Grid',      'img' => get_template_directory_uri() . '/assets/images/grid.png'),
                        ),
                        'default'   => 'list',
                    ),
                    array(
                        'id' => 'opt-blog-loop-content-type',
                        'type' => 'switch',
                        'title' => __('Bog list loop content', 'themestudio'),
                        'subtitle' => __('Show the blog content or the excerpt on loop', 'themestudio'),
                        'default' => '1',
                        'on' => __( 'The content', 'themestudio' ),
                        'off' => __( 'The excerpt', 'themestudio' ),
                        'required' => array('opt-blog-layout-style', '=', 'list'),
                    ),
                    array(
                        'id'            => 'opt-excerpt-max-char-length',
                        'type'          => 'text',
                        'title'         => __('The excerpt max chars length', 'themestudio'),
                        'default'       => 300,
                        'validate'      => 'numeric',
                        'required'      => array('opt-blog-loop-content-type', '!=', '1'),
                    ),
                    array(
                        'id'       => 'opt-blog-metas',
                        'type'     => 'select',
                        'multi'    => true,
                        'title'    => __('Blog metas', 'themestudio'),
                        'options'  => array(
                            'author'    => 'Author',
                            'date'      => 'Date time',
                            'category'  => 'Category',
                            'comment'   => 'Comment',
                            'tags'      => 'Tags',
                            'views'     => 'Views count',
                        ),
                        'default'  => array( 'author', 'date', 'category', 'comment', 'views' )
                    ),
                    array(
                        'id' => 'opt-blog-single-post-bio',
                        'type' => 'switch',
                        'title' => __('Enable bio author in single post', 'themestudio'),
                        'subtitle' => __('Enable bio author on/off', 'themestudio'),
                        'default' => '1',
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                    ),
                    array(
                        'id' => 'opt-enable-share-post',
                        'type' => 'switch',
                        'title' => __('Enable single post share links', 'themestudio'),
                        'subtitle' => __('', 'themestudio'),
                        'default' => '1',
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                    ),
                    array(
                        'id'       => 'opt-single-share-socials',
                        'type'     => 'select',
                        'multi'    => true,
                        'title'    => __('Choose socials to share single post', 'themestudio'),
                        'options'  => array(
                            'facebook'  => 'Facebook',
                            'gplus'     => 'Google Plus',
                            'twitter'   => 'Twitter',
                            'pinterest' => 'Pinterest',
                            'linkedin'  => 'Linkedin'
                        ),
                        'default'  => array( 'facebook', 'gplus', 'twitter', 'pinterest', 'linkedin' ),
                        'required' => array('opt-enable-share-post', '=', '1'),
                    ),
                ),
            );


            /*--Portfolio Setting--*/
            $this->sections[] = array(
                'title' => __('Portfolio ', 'themestudio'),
                'desc' => __('Portfolio Settings', 'themestudio'),
                'icon' => 'el-icon-folder',
                'fields' => array(
                    array(
                        'id'        => 'opt-portfolio-layout',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Sidebar position', 'themestudio'),
                        'subtitle'  => __('Select sidebar position.', 'themestudio'),
                        'desc' => __('Select sidebar on left or right', 'themestudio'),
                        'options'   => array(
                            '1' => array('alt' => '1 Column Left',      'img' => get_template_directory_uri() . '/assets/images/2cl.png'),
                            '2' => array('alt' => '2 Column Right',     'img' => get_template_directory_uri() . '/assets/images/2cr.png'),
                            '3' => array('alt' => 'Full Width',         'img' => get_template_directory_uri() . '/assets/images/1column.png'),
                        ),
                        'default'   => '3',
                    ),
                    array(
                        'id'        => 'opt-portfolio-column',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => __('Portfolio columns', 'themestudio'),
                        'subtitle'  => __('Select columns.', 'themestudio'),
                        'desc' => __('Select 1|2|3|4 columns', 'themestudio'),
                        'options'   => array(
                            '1' => array('alt' => '1 Column',       'img' => get_template_directory_uri() . '/assets/images/1column.png'),
                            '2' => array('alt' => '2 Columns',      'img' => get_template_directory_uri() . '/assets/images/2columns.png'),
                            '3' => array('alt' => '3 Columns',      'img' => get_template_directory_uri() . '/assets/images/3columns.png'),
                            '4' => array('alt' => '4 Columns',      'img' => get_template_directory_uri() . '/assets/images/4columns.png'),
                        ),
                        'default'   => '4',
                    ),
                    array(
                        'id' => 'opt-portfolio-switch-filter',
                        'type' => 'switch',
                        'title' => __('Enable porfolio filter', 'themestudio'),
                        'subtitle' => __('Enable porfolio filter on/off', 'themestudio'),
                        "default" => 1,
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                    ),
                    array(
                        'id' => 'opt-portfolio-switch-pagination',
                        'type' => 'switch',
                        'title' => __('Enable porfolio pagination', 'themestudio'),
                        'subtitle' => __('Enable porfolio pagination on/off', 'themestudio'),
                        'default' => 1,
                        'on' => 'Enabled',
                        'off' => 'Disabled',
                    ),
                ),
            );
            /**
             * Check if WooCommerce is active
             **/
            if ( class_exists( 'WooCommerce' ) ) {
                // Put your plugin code here

                /*-- Woocommerce Setting--*/
                $this->sections[] = array(
                    'title' => __('WooCommerce', 'themestudio'),
                    'desc' => __('WooCommerce Settings', 'themestudio'),
                    'icon' => 'el-icon-shopping-cart',
                    'fields' => array(
                        array(
                            'id'        => 'woo_layout',
                            'type'      => 'image_select',
                            'compiler'  => true,
                            'title'     => __('Woocommerce sidebar position', 'themestudio'),
                            'subtitle'  => __('Select Woocommerce sidebar position.', 'themestudio'),
                            'desc' => __('Select sidebar on left or right', 'themestudio'),
                            'options'   => array(
                                'left-sidebar' => array('alt' => '1 Column Left',      'img' => get_template_directory_uri() . '/assets/images/2cl.png'),
                                'right-sidebar' => array('alt' => '2 Column Right',     'img' => get_template_directory_uri() . '/assets/images/2cr.png'),
                                'fullwidth' => array('alt' => 'Full Width',         'img' => get_template_directory_uri() . '/assets/images/1column.png'),
                            ),  
                            'default'   => 'left-sidebar',
                        ),
                        array(
                            'id'        => 'woo_shop_display_style',
                            'type'      => 'image_select',
                            'compiler'  => true,
                            'title'     => __('Shop display style', 'themestudio'),
                            'subtitle'  => __('', 'themestudio'),
                            'desc' => __('Select shop default display style', 'themestudio'),
                            'options'   => array(
                                'list' => array('alt' => 'Shop List',      'img' => get_template_directory_uri() . '/assets/images/list.png'),
                                'grid' => array('alt' => 'Shop Grid',      'img' => get_template_directory_uri() . '/assets/images/grid.png'),
                            ),
                            'default'   => 'grid',
                        ),
                        array(
                            'id'        => 'woo_product_layout',
                            'type'      => 'image_select',
                            'compiler'  => true,
                            'title'     => __('Woocommerce product layout', 'themestudio'),
                            'subtitle'  => __('Set number column in product archive page.', 'themestudio'),
                            'options'   => array(
                                '3' => array('alt' => '3 Column ',      'img' => get_template_directory_uri() . '/assets/images/3columns.png'),
                                '4' => array('alt' => '4 Column ',      'img' => get_template_directory_uri() . '/assets/images/4columns.png')
                            ),
                            'default'   => '3',
                        ),
                        array(
                            'id'            => 'woo_number_of_related_products_no_sidebar',
                            'type'          => 'text',
                            'title'         => __('Number of related products no sidebar', 'themestudio'),
                            'subtitle'      => __('', 'themestudio'),
                            'desc'          => __('Number of related products display on the single product page (no sidebar)', 'themestudio'),
                            'default'       => '4',
                            'validate'      => 'numeric',
                            'required'      => array('woo_layout', '=', 'fullwidth'),
                        ),
                        array(
                            'id'            => 'woo_number_of_related_products_sidebar',
                            'type'          => 'text',
                            'title'         => __('Number of related products', 'themestudio'),
                            'subtitle'      => __('', 'themestudio'),
                            'desc'          => __('Number of related products display on the single product page (has sidebar)', 'themestudio'),
                            'default'       => '3',
                            'validate'      => 'numeric',
                            'required'      => array('woo_layout', '!=', 'fullwidth'),
                        ),
                        array(
                            'id' => 'opt-enable-share-product',
                            'type' => 'switch',
                            'title' => __('Enable single product share links', 'themestudio'),
                            'subtitle' => __('', 'themestudio'),
                            'default' => '1',
                            'on' => 'Enabled',
                            'off' => 'Disabled',
                        ),
                        array(
                            'id'       => 'opt-single-product-socials-share',
                            'type'     => 'select',
                            'multi'    => true,
                            'title'    => __('Share product on', 'themestudio'),
                            'subtitle' => __('Display sharing buttons on the single product pages', 'themestudio'),
                            'options'  => array(
                                'facebook'  => 'Facebook',
                                'gplus'     => 'Google Plus',
                                'twitter'   => 'Twitter',
                                'pinterest' => 'Pinterest',
                                'linkedin'  => 'Linkedin'
                            ),
                            'default'  => array( 'facebook', 'gplus', 'twitter', 'pinterest', 'linkedin' ),
                            'required' => array('opt-enable-share-product', '=', '1'),
                        )
                    ),
                );
            }
            
            
            /*-- Login/Register Setting--*/
            $this->sections[] = array(
                'title' => __('Login/Register', 'themestudio'),
                'desc' => __('Login/Register Settings', 'themestudio'),
                'icon' => 'el-icon-user',
                'fields' => array(
                    array(
                        'id'            => 'opt-login-redirect',
                        'type'          => 'text',
                        'title'         => __('Login redirect', 'themestudio'),
                        'subtitle'      => __('URL redirect after logged in', 'themestudio'),
                        'default'       => '',
                        'validate'      => 'url',
                        'desc'          => __( 'If this field is empty, default redirect is back to the current page', 'themestudio' )
                    ),
                    array(
                        'id'            => 'opt-enable-default-login',
                        'type'          => 'switch',
                        'title'         => __( 'Default login/register', 'themestudio' ),
                        'subtitle'      => __( 'Enable/Disable Wordpress default login/register page. <br /> <strong>Note: </strong><em>This option has no effect if coming soon mode is activated.</em>', 'themestudio' ),
                        'desc'          => __( 'IMPORTAINT NOTE: If "Disable", please make sure that you have a login page and visitors can see the link of this page on the frontend. If not, visitors (and you) can not login.', 'themestudio' ),
                        'default'       => 1,
                        'on'            => __( 'Enabled', 'themestudio' ),
                        'off'           => __( 'Disabled', 'themestudio' ),
                    ),
                    array(
                        'id'            => 'opt-redirect-from-default-login-page',
                        'type'          => 'text',
                        'title'         => __('Redirect from default WP login page', 'themestudio'),
                        'subtitle'      => __('If default login page is disabled, user will be redirect to custom login page when access the WordPress default login page', 'themestudio'),
                        'default'       => get_home_url(),
                        'validate'      => 'url',
                        'desc'          => __( 'If this field is empty, default redirect is back to home page', 'themestudio' ),
                        'required'      => array('opt-enable-default-login', '=', 0),
                    ),
                    array(
                        'id'            => 'opt-terms-of-use-url',
                        'type'          => 'text',
                        'title'         => __('Terms of use link', 'themestudio'),
                        'subtitle'      => __('The terms of use link page show on register form', 'themestudio'),
                        'default'       => '',
                        'validate'      => 'url',
                        'desc'          => __( '', 'themestudio' ),
                        //'required' => array('opt-enable-default-login', '=', 0),
                    ),
                ),
            );
            
            /*-- Sliding Sidebar Setting--*/
            $this->sections[] = array(
                'title' => __('Sliding Sidebar Settings', 'themestudio'),
                'desc' => __('Settings for hidden sliding sidebar', 'themestudio'),
                'icon' => 'el-icon-th-list',
                'fields' => array(
                    array(
                        'id'       => 'opt-hidden-sidebar-pos',
                        'type'     => 'select',
                        'multi'    => false,
                        'title'    => __('Position', 'themestudio'),
                        'desc'     => __( 'Select sliding sidebar position. Default is "Right".', 'themestudio' ),
                        'options'  => array(
                            'left'      => __( 'Left', 'themestudio' ),
                            'right'     => __( 'Right', 'themestudio' )
                        ),
                        'default'       => 'right',
                        'placeholder'   => __( 'Select sliding sidebar position. Default is "Right".', 'themestudio' )
                    )
                ),
            );
            
            /*-- Contact Setting--*/
            $this->sections[] = array(
                'title' => __('Contact Settings', 'themestudio'),
                'desc' => __('Contact info settings', 'themestudio'),
                'icon' => 'el-icon-envelope',
                'fields' => array(
                    array(
                        'id'            => 'opt-phone-numbers',
                        'type'          => 'text',
                        'title'         => __('Phone numbers', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('+ 2 0106 5370701', 'themestudio'),
                    ),
                    array(
                        'id'            => 'opt-email',
                        'type'          => 'text',
                        'title'         => __('Email', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('7oroof@7oroof.com', 'themestudio'),
                        'va;idate'      => 'email'
                    ),
                    array(
                        'id'            => 'opt-address',
                        'type'          => 'text',
                        'title'         => __('Address', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('', 'themestudio'),
                    ),
                ),
            );
            
            
            /*-- Coming Soon Setting--*/
            $this->sections[] = array(
                'title' => __('Coming Soon Settings', 'themestudio'),
                'desc' => __('', 'themestudio'),
                'icon' => 'el-icon-time',
                'fields' => array(
                    array(
                        'id'            => 'opt-enable-coming-soon-mode',
                        'type'          => 'switch',
                        'title'         => __( 'Enable/Disable coming soon mode', 'themestudio' ),
                        'subtitle'      => __( 'Turn coming soon mode on/off', 'themestudio' ),
                        'desc'          => __( 'If turn on, URL redirect to coming soon page must available', 'themestudio' ),
                        'default'       => 0,
                        'on'            => __( 'On', 'themestudio' ),
                        'off'           => __( 'Off', 'themestudio' ),
                    ),
                    array(
                        'id'            => 'opt-coming-soon-title',
                        'type'          => 'text',
                        'title'         => __('Coming soon title', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('WE ARE COMING SOON', 'themestudio'),
                        'required'      => array( 'opt-enable-coming-soon-mode', '=', 1 ),
                    ),
                    array(
                        'id'            => 'opt-coming-soon-text',
                        'type'          => 'editor',
                        'title'         => __('Coming soon text', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('The One is a business theme which perfectly suited Construction company, Cleaning agency, Mechanic workshop and any kind of handyman business.', 'themestudio'),
                        'required'      => array( 'opt-enable-coming-soon-mode', '=', 1 ),
                    ),
                    array(
                        'id'            => 'opt-coming-soon-date',
                        'type'          => 'date',
                        'title'         => __('Coming soon date', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('', 'themestudio'),
                        'required'      => array( 'opt-enable-coming-soon-mode', '=', 1 ),
                    ),
                    array(
                        'id'            => 'opt-disable-coming-soon-when-date-small',
                        'type'          => 'switch',
                        'title'         => __( 'Enable/Disable coming soon if coming date is smaller than current date', 'themestudio' ),
                        'subtitle'      => __( '', 'themestudio' ),
                        'desc'          => __( 'Disable coming soon if coming date is smaller than current date', 'themestudio' ),
                        'default'       => 0,
                        'on'            => __( 'On', 'themestudio' ),
                        'off'           => __( 'Off', 'themestudio' ),
                        'required'      => array( 'opt-enable-coming-soon-mode', '=', 1 ),
                    ),
                ),
            );
            
            
            /*-- 404 Setting--*/
            $this->sections[] = array(
                'title' => __('404 Settings', 'themestudio'),
                'desc' => __('Setting for 404 error page', 'themestudio'),
                'icon' => 'el-icon-bell',
                'fields' => array(
                    array(
                        'id'            => 'opt-404-header-title',
                        'type'          => 'text',
                        'title'         => __('404 header title', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('Error Page', 'themestudio'),
                    ),
                    array(
                        'id'            => 'opt-404-header-subtitle',
                        'type'          => 'text',
                        'title'         => __('404 header subtitle', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('Sorry :(', 'themestudio'),
                    ),
                    array(
                        'id'            => 'opt-404-title',
                        'type'          => 'text',
                        'title'         => __('404 title', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('404', 'themestudio')
                    ),
                    array(
                        'id'            => 'opt-404-text',
                        'type'          => 'editor',
                        'title'         => __('Text', 'themestudio'),
                        'subtitle'      => __('', 'themestudio'),
                        'default'       => __('The One is a business theme which perfectly suited Construction company, Cleaning agency, Mechanic workshop and any kind of handyman business.', 'themestudio'),
                    ),
                ),
            );
            
        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id' => 'redux-opts-1',
                'title' => __('Theme Information 1', 'themestudio'),
                'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'themestudio')
            );

            $this->args['help_tabs'][] = array(
                'id' => 'redux-opts-2',
                'title' => __('Theme Information 2', 'themestudio'),
                'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'themestudio')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'themestudio');
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'              => 'theone', // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'          => $theme->get('Name'), // Name that appears at the top of your panel
                'display_version'       => $theme->get('Version'), // Version that appears at the top of your panel
                'menu_type'             => 'submenu', //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'        => false, // Show the sections below the admin menu item or not
                'menu_title'            => __('THE ONE', 'themestudio'),
                'page_title'            => __('THE ONE', 'themestudio'),
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key'        => 'AIzaSyBnxzHttO52IQDpDkbZNbT48HL3o8YNb-k', // Must be defined to add google fonts to the typography module
                //'async_typography'    => true, // Use a asynchronous font on the front end or font string
                //'admin_bar'           => false, // Show the panel pages on the admin bar
                'global_variable'       => 'theone', // Set a different name for your global variable other than the opt_name
                'dev_mode'              => false, // Show the time the page took to load, etc
                'customizer'            => true, // Enable basic customizer support
                // OPTIONAL -> Give you extra features
                'page_priority'         => null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'           => 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'      => 'manage_options', // Permissions needed to access the options panel.
                'menu_icon'             => '', // Specify a custom URL to an icon
                'last_tab'              => '', // Force your panel to always open to a specific tab (by id)
                'page_icon'             => 'icon-themes', // Icon displayed in the admin panel next to your menu_title
                'page_slug'             => 'theone_options', // Page slug used to denote the panel
                'save_defaults'         => true, // On load save the defaults to DB before user clicks save or not
                'default_show'          => false, // If true, shows the default value next to each field that is not the default value.
                'default_mark'          => '', // What to print by the field's title if the value shown is default. Suggested: *
                // CAREFUL -> These options are for advanced use only
                'transient_time'        => 60 * MINUTE_IN_SECONDS,
                'output'                => true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'            => true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                //'domain'              => 'redux-framework', // Translation domain key. Don't change this unless you want to retranslate all of Redux.
                'footer_credit'         => __('Theme Studio Wordpress Team', 'themestudio'), // Disable the footer credit of Redux. Please leave if you can help it.
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'show_import_export'    => true, // REMOVE
                'system_info'           => false, // REMOVE
                'help_tabs'             => array(),
                'help_sidebar' => '', // __( '', $this->args['domain'] );
                'hints' => array(
                    'icon'              => 'icon-question-sign',
                    'icon_position'     => 'right',
                    'icon_color'        => 'lightgray',
                    'icon_size'         => 'normal',

                    'tip_style'         => array(
                        'color'     => 'light',
                        'shadow'    => true,
                        'rounded'   => false,
                        'style'     => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'  => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'mouseover',
                        ),
                        'hide'  => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );

            $this->args['share_icons'][] = array(
                'url' => 'https://www.facebook.com/thuydungcafe',
                'title' => 'Like us on Facebook',
                'icon' => 'el-icon-facebook'
            );
            $this->args['share_icons'][] = array(
                'url' => 'http://twitter.com/',
                'title' => 'Follow us on Twitter',
                'icon' => 'el-icon-twitter'
            );

            // Panel Intro text -> before the form
            if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
                if (!empty($this->args['global_variable'])) {
                    $v = $this->args['global_variable'];
                } else {
                    $v = str_replace("-", "_", $this->args['opt_name']);
                }
                // $this->args['intro_text'] = sprintf(__('<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'themestudio'), $v);
            } else {
                // $this->args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'themestudio');
            }

            // Add content after the form.
            // $this->args['footer_text'] = __('<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'themestudio');
        }

    }

    new TheOne_Redux_Framework_config();
}


/**

  Custom function for the callback referenced above

 */
if (!function_exists('redux_my_custom_field')):

    function redux_my_custom_field($field, $value) {
        print_r($field);
        print_r($value);
    }

endif;

/**

  Custom function for the callback validation referenced above

 * */
if ( !function_exists( 'redux_validate_callback_function' ) ):

    function redux_validate_callback_function( $field, $value, $existing_value ) {
        $error = false;
        $value = 'just testing';

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }


endif;