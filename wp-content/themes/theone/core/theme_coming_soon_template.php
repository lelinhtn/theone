<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}



if ( !function_exists( 'ts_coming_soon_html' ) ) {
    
    function ts_coming_soon_html() {
        global $theone;
        
        $date = isset( $theone['opt-coming-soon-date'] ) ? $theone['opt-coming-soon-date'] : date();
        $title = isset( $theone['opt-coming-soon-title'] ) ? sanitize_text_field( $theone['opt-coming-soon-title'] ) : '';
        $text = isset( $theone['opt-coming-soon-text'] ) ? wpautop( $theone['opt-coming-soon-text'] ) : '';
        
        get_header( 'soon' );
        
        $html = '';
        $html .= '<div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-12 section-title clearfix align-center ts-need-animate wpb_animate_when_almost_visible">';
        
        if ( $title != '' ) {
            $html .=    '<h2>' . $title . '</h2>';   
        }        
        
        if ( trim( $text ) != '' ) {
            $html .=    '<div class="title-description">
                            ' . $text . '
                        </div>';   
        }
                        
        $html .=    '</div><!-- /.section-title -->
                  </div><!-- /.row -->';
        
        $html .= '<div class="ts-countdown-wrap counter" data-countdown="' . esc_attr( $date ) . '"></div><!-- /.ts-countdown-wrap -->';
        
        $html = '<div class="section-soon">
                    <div class="container">
                    ' . $html . '
                    </div>
                </div>';
        
        echo $html;
        get_footer( 'soon' );
        
        
    }
    
    
}