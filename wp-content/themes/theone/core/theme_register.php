<?php  

global $theone;
/*
 * register navigation menus
*/
register_nav_menus(
	array(
        'main_menu'       => __( 'Main menu', 'themestudio' ),
        'footer_menu'       => __('Footer Menu', 'themestudio'),
        'onepage_menu'      => __('Onepage Menu', 'themestudio'),
        'top_menu'          => __('Top Menu', 'themestudio'),
	)
);


if ( !function_exists('ts_register_sidebars') ){
   
    function ts_register_sidebars() {
        
        /*
		 * Register sidebar
		*/
		register_sidebar(
			array(
				'name' => __( 'Primary Sidebar(Right Sidebar)', 'themestudio' ),
				'id'            => 'primary',
			    'description' => esc_html__( 'This is land of page sidebar','themestudio' ),
				'before_title' =>'<h3 class="sidebar_title">',
				'after_title' =>'</h3>',
				'before_widget' => '<div  id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
			)
		);
        
        $numberOfFooterTopWidgets = isset( $theone['opt-footer-top-sidebar-layout'] ) ? min( 4, max( 0, intval( $theone['opt-footer-top-sidebar-layout'] ) ) ): 4;
       
        /*
	     * Register sidebar footer top widgets
	    */
        if ( $numberOfFooterTopWidgets > 0 ):
        
            for ( $i = 1; $i <= $numberOfFooterTopWidgets; $i++ ):
                register_sidebar(
              		array(
        				'name' => sprintf( esc_html__( 'Footer top widget %d', 'themestudio' ), $i ),
                        'id' => 'ts-sidebar-footer-top-' . $i,
        				'description' => esc_html__( 'This is a footer top widget','themestudio' ),
        				'before_title' =>'<h2 class="widget-title">',
        				'after_title' =>'</h2>',			  
        				'before_widget' => '<div class="%1$s widget %2$s">',
        				'after_widget' => '</div>',
        			)
              	);
            endfor;
            
        endif;
        
        
        $numberOfFooterWidgets = isset( $theone['opt-footer-sidebar-layout'] ) ? min( 4, max( 0, intval( $theone['opt-footer-sidebar-layout'] ) ) ): 4;

	    /*
	     * Register sidebar footer widgets
	    */
        if ( $numberOfFooterWidgets > 0 ):
        
            for ( $i = 1; $i <= $numberOfFooterWidgets; $i++ ):
                register_sidebar(
              		array(
        				'name' => sprintf( esc_html__( 'Footer bottom widget %d', 'themestudio' ), $i ),
                        'id' => 'ts-sidebar-footer-'.$i,
        				'description' => esc_html__( 'This is a footer bottom widget','themestudio' ),
        				'before_title' =>'<h2 class="widget-title">',
        				'after_title' =>'</h2>',			  
        				'before_widget' => '<div class="%1$s widget %2$s">',
        				'after_widget' => '</div>',
        			)
              	);
            endfor;
            
        endif;
        
        if ( class_exists( 'WooCommerce' ) ) {
            /**
             * WooCommerce sidebar 
             **/
            register_sidebar(
          		array(
    				'name' => esc_html__( 'The One WooCommerce Sidebar', 'themestudio' ),
                    'id' => 'ts-wc-sidebar',
    				'description' => esc_html__( 'This is the sidebar for WooComerce pages','themestudio' ),
    				'before_title' =>'<h2 class="widget-title">',
    				'after_title' =>'</h2>',			  
    				'before_widget' => '<div class="%1$s widget %2$s">',
    				'after_widget' => '</div>',
    			)
          	);   
        }
        
        /**
         * Sidebar hidden 
         **/
        register_sidebar(
      		array(
				'name' => esc_html__( 'The One hidden sidebar', 'themestudio' ),
                'id' => 'ts-hidden-sidebar',
				'description' => esc_html__( 'This is a hidden sidebar. Show when you click menu icon on the header','themestudio' ),
				'before_title' =>'<h2 class="widget-title">',
				'after_title' =>'</h2>',			  
				'before_widget' => '<div class="%1$s widget %2$s">',
				'after_widget' => '</div>',
			)
      	);
    }
   	add_action( 'widgets_init', 'ts_register_sidebars' );


}

?>