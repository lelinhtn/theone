<?php 
  if( !function_exists( 'ts_get_breadcrumbs' ) ) {

    /*
     * breadcrumbs
    */
    function ts_get_breadcrumbs($delimiter='/') {

      $home = __( 'Home', 'themestudio' ); // text for the 'Home' link
      $before = '<li class="active">'; // tag before the current crumb
      $after = '</li>'; // tag after the current crumb
      $return =''; 
      if ( !is_home() && !is_front_page() || is_paged() ) {
        global $post;
        $homeLink = home_url();
        $return .= '<ul class="breadcrumb"><li><a  href="' . $homeLink . '">' . $home . '</a> <span class="divider">' . $delimiter . '</span></li>';
     
        if ( is_category() ) {
          global $wp_query;
          $cat_obj = $wp_query->get_queried_object();
          $thisCat = $cat_obj->term_id;
          $thisCat = get_category($thisCat);
          $parentCat = get_category($thisCat->parent);
          if ($thisCat->parent != 0) $return .=(get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
          $return .= $before  . single_cat_title('', false) . $after;
     
        } elseif ( is_day() ) {
          $return .= '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
          $return .= '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> <span class="divider">' . $delimiter . '</span>';
          $return .= $before . get_the_time('d') . $after;
     
        } elseif ( is_month() ) {
          $return .= '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> <span class="divider">' . $delimiter . '</span>';
          $return .= $before . get_the_time('F') . $after;
     
        } elseif ( is_year() ) {
          $return .= $before . get_the_time('Y') . $after;
     
        } elseif ( is_single() && !is_attachment() ) {
            $cat = get_the_category(); 
            $cat = $cat[0];
            $return .= $before . $cat->name . $after;
     
        } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
          $post_type = get_post_type_object(get_post_type()); 
          //$return .= $before . $post_type->labels->singular_name . $after;
     
        } elseif ( is_attachment() ) {
          $parent = get_post($post->post_parent);
          $cat = get_the_category($parent->ID); $cat = $cat[0];
          $return .= get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
          $return .= '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> <span class="divider">' . $delimiter . '</span>';
          $return .= $before . get_the_title() . $after;
     
        } elseif ( is_page() && !$post->post_parent ) {
          $return .= $before . get_the_title() . $after;
     
        } elseif ( is_page() && $post->post_parent ) {
          $parent_id  = $post->post_parent;
          $breadcrumbs = array();
          while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
            $parent_id  = $page->post_parent;
          }
          $breadcrumbs = array_reverse($breadcrumbs);
          foreach ($breadcrumbs as $crumb) $return .= $crumb . ' <span class="divider">' . $delimiter . '</span>';
          $return .= $before . get_the_title() . $after;
     
        } elseif ( is_search() ) {
          //$return .= $before . 'Search' . $after;
     
        } elseif ( is_tag() ) {
          $return .= $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
     
        } elseif ( is_author() ) {
           global $author;
          $userdata = get_userdata($author);
          $return .= $before . 'Articles posted by ' . $userdata->display_name . $after;
        } elseif ( is_404() ) {
          $return .= $before . 'Error 404' . $after;
        }
     
        if ( get_query_var('paged') ) {
          if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) $return .= ' (';
          $return .= __('Page','themestudio') . ' ' . get_query_var('paged');
          if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) $return .= ')';
        }
        $return .='</ul>';
     
        $return .= '';
     
      }

    	echo $return;
    }

  }


  if( !function_exists( 'ts_page_menu' ) ) {

    /*
     * page menu
    */
    function ts_page_menu() 
    {

    	$level = 3;
    	$args = array(
    		'title_li' => '0',
    		'sort_column' => 'menu_order',
    		'depth' => $level,
    	);
    	
    	echo '<nav>';
    	echo '<ul class="main-menu">';
    	echo  wp_list_pages($args);
    	echo  '</ul>';
    	echo '</nav>';
    }

  }

  
  if( !function_exists( 'ts_favicon' ) ) {

    /*
     * favicon
    */
    add_action('wp_head','ts_favicon',2);
    function ts_favicon() {
    	global $theone;
    	$favicon = isset( $theone['opt-general-favicon']['url'] ) ? $theone['opt-general-favicon']['url'] : get_template_directory_uri() . '/assets/images/favicon.png';
    	if ( $favicon ) {
    	 echo '<link rel="shortcut icon" href="' . esc_url( $favicon ) . '" />',"\n";
    	}
    }

  }


  if( !function_exists( 'ts_header_metas' ) ) {

    /*
     * header metas
    */
    add_action('wp_head', 'ts_header_metas',1);
    function ts_header_metas()
    {
    	echo '<meta charset="utf-8">'."\n";
    	echo '<meta name="viewport" content="width=device-width">'."\n";
    	echo '<link rel="apple-touch-icon-precomposed" href="apple-touch-icon.png">'."\n";
    	echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-57x57.png" />'."\n";
    	echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png" />'."\n";
    	echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />'."\n";
    }

  }


  if( !function_exists( 'ts_get_tracking_code' ) ) {

    /*
     * Get tracking code
    */
    function ts_get_tracking_code()
    {
    	global $theone;

    	$return ='';
    	if( isset( $theone['opt-general-tracking-code'] ) ) {
    	$return .= stripslashes($theone['opt-general-tracking-code']);
    	}

    	echo  '<script>
                jQuery(function () {
                '.$return.'
                });
            </script>';
    }
    add_action('wp_head','ts_get_tracking_code');

  }


  if( !function_exists( 'ts_ie_js' ) ) {

    /*
     * ie script
    */
    function ts_ie_js() 
    {
    	preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches);
      if (count($matches)>1){
        //Then we're using IE
        $version = $matches[1];

        switch(true){
          case ($version<=8):

      	     echo '<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em>Upgrade to a different browser or install Google Chrome Frame to experience this site.</p><![endif]-->';
            break;

          case ($version<=9):
      		// Jquery html5.js
        		wp_register_script( 'html5.js.min.js', THE_ONE_JS. '/html5shiv.js', false, THE_ONE_THEME_VERSION ,true);
        		wp_enqueue_script('html5.js.min.js');
            break;
          case ($version=7):
            wp_register_script( 'icons-lte-ie7', THE_ONE_JS. '/fonts/Simple-Line-Icons/icons-lte-ie7.js', false, THE_ONE_THEME_VERSION ,true);
            wp_enqueue_script('icons-lte-ie7');
            break;  
          case ($version=8):
          ?>
            <!--[if lt IE 8]>
            <style>
            /* For IE < 8 (trigger haslayout) */
            .clearfix {
                zoom:1;
            }
             8="" (trigger="" haslayout)="" */="" .clearfix="" {="" zoom:1;="" }=""></ 8 (trigger haslayout) */
            .clearfix {
                zoom:1;
            }
            ></style>
            <![endif]-->

          <?php
          break;  
          default:
            //You get the idea
        }
      }
    }
    add_action('wp_head', 'ts_ie_js');

  }


  if( !function_exists( 'ts_wp_title' ) ) {

    /*
     * WP title
    */
    function ts_wp_title( $title, $separator ) 
    {
    	if ( is_feed() )
    		return $title;

    	global $paged, $page;

    	if ( is_search() ) {
    		$title = sprintf( esc_html__( 'Search results for %s', 'themestudio' ), '"' . get_search_query() . '"' );
    		if ( $paged >= 2 )
    			$title .= " $separator " . sprintf( esc_html__( 'Page %s', 'themestudio' ), $paged );
    			$title .= " $separator " . get_bloginfo( 'name', 'display' );
    		return $title;
    	}

    	$title .= get_bloginfo( 'name', 'display' );

    	$site_description = get_bloginfo( 'description', 'display' );
    	if ( $site_description && ( is_home() || is_front_page() ) )
    		$title .= " $separator " . $site_description;

    	if ( $paged >= 2 || $paged >= 2 )
    		$title .= " $separator " . sprintf( esc_html__( 'Page %s', 'themestudio' ), max( $paged, $page ) );

    	return $title;
    }
    add_filter( 'wp_title', 'ts_wp_title', 10, 2 );

  }


  if( !function_exists( 'ts_get_user_browser' ) ) {

    /*
     * Get User browser
    */
    function ts_get_user_browser() {
      $u_agent = $_SERVER['HTTP_USER_AGENT'];
      $ub = '';
      if(preg_match('/MSIE/i',$u_agent))
      {
          $ub = "ie";
      }
      elseif(preg_match('/Firefox/i',$u_agent))
      {
          $ub = "firefox";
      }
      elseif(preg_match('/Safari/i',$u_agent))
      {
          $ub = "safari";
      }
      elseif(preg_match('/Chrome/i',$u_agent))
      {
          $ub = "chrome";
      }
      elseif(preg_match('/Flock/i',$u_agent))
      {
          $ub = "flock";
      }
      elseif(preg_match('/Opera/i',$u_agent))
      {
          $ub = "opera";
      }

      return $ub;
    }

  }
  
  if ( ! function_exists( 'ts_resize_image' ) ) {
	/**
	 * @param int $attach_id
	 * @param string $img_url
	 * @param int $width
	 * @param int $height
	 * @param bool $crop
	 *
	 * @since 1.0
	 * @return array
	 */
	function ts_resize_image( $attach_id = null, $img_url = null, $width, $height, $crop = false ) {
		// this is an attachment, so we have the ID
		$image_src = array();
		if ( $attach_id ) {
			$image_src = wp_get_attachment_image_src( $attach_id, 'full' );
			$actual_file_path = get_attached_file( $attach_id );
			// this is not an attachment, let's use the image url
		} else if ( $img_url ) {
			$file_path = parse_url( $img_url );
			//			$actual_file_path = $_SERVER['DOCUMENT_ROOT'] . $file_path['path'];
			//			$actual_file_path = ltrim( $file_path['path'], '/' );
			$actual_file_path = rtrim( ABSPATH, '/' ) . $file_path['path'];
			$orig_size = getimagesize( $actual_file_path );
			$image_src[0] = $img_url;
			$image_src[1] = $orig_size[0];
			$image_src[2] = $orig_size[1];
		}
		if ( ! empty( $actual_file_path ) ) {
			$file_info = pathinfo( $actual_file_path );
			$extension = '.' . $file_info['extension'];

			// the image path without the extension
			$no_ext_path = $file_info['dirname'] . '/' . $file_info['filename'];

			$cropped_img_path = $no_ext_path . '-' . $width . 'x' . $height . $extension;

			// checking if the file size is larger than the target size
			// if it is smaller or the same size, stop right here and return
			if ( $image_src[1] > $width || $image_src[2] > $height ) {

				// the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
				if ( file_exists( $cropped_img_path ) ) {
					$cropped_img_url = str_replace( basename( $image_src[0] ), basename( $cropped_img_path ), $image_src[0] );
					$vt_image = array(
						'url' => $cropped_img_url,
						'width' => $width,
						'height' => $height
					);

					return $vt_image;
				}

				// $crop = false
				if ( $crop == false ) {
					// calculate the size proportionaly
					$proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
					$resized_img_path = $no_ext_path . '-' . $proportional_size[0] . 'x' . $proportional_size[1] . $extension;

					// checking if the file already exists
					if ( file_exists( $resized_img_path ) ) {
						$resized_img_url = str_replace( basename( $image_src[0] ), basename( $resized_img_path ), $image_src[0] );

						$vt_image = array(
							'url' => $resized_img_url,
							'width' => $proportional_size[0],
							'height' => $proportional_size[1]
						);

						return $vt_image;
					}
				}

				// no cache files - let's finally resize it
				$img_editor = wp_get_image_editor( $actual_file_path );

				if ( is_wp_error( $img_editor ) || is_wp_error( $img_editor->resize( $width, $height, $crop ) ) ) {
					return array(
						'url' => '',
						'width' => '',
						'height' => ''
					);
				}

				$new_img_path = $img_editor->generate_filename();

				if ( is_wp_error( $img_editor->save( $new_img_path ) ) ) {
					return array(
						'url' => '',
						'width' => '',
						'height' => ''
					);
				}
				if ( ! is_string( $new_img_path ) ) {
					return array(
						'url' => '',
						'width' => '',
						'height' => ''
					);
				}

				$new_img_size = getimagesize( $new_img_path );
				$new_img = str_replace( basename( $image_src[0] ), basename( $new_img_path ), $image_src[0] );

				// resized output
				$vt_image = array(
					'url' => $new_img,
					'width' => $new_img_size[0],
					'height' => $new_img_size[1]
				);

				return $vt_image;
			}

			// default output - without resizing
			$vt_image = array(
				'url' => $image_src[0],
				'width' => $image_src[1],
				'height' => $image_src[2]
			);

			return $vt_image;
		}

		return false;
	}
  }
  
  if ( !function_exists( 'ts_header_layout' ) ) {
    
    /**
     * Show header layout by seting in the Theme Options
     * @since 1.0 
     **/
    function ts_header_layout() {
        global $theone, $headers_has_secondary, $headers_has_title_default, $headers_has_subtitle_default, $headers_has_icon_default, $headers_has_breadcrumb_default;
        
        $is_plugin_core_active = ts_is_plugin_active( 'theone-core/theone-core.php' );
        
        $html = '';
        $main_section_html = '';
        $secondary_section_html = '';
        $bg_img_html = '';
        $slider_html = '';
        $bg_img_wrap_class = '';
        $slider_wrap_class = '';
        $header_class = ''; // HEADER CLASS
        
        // Names of header has secondary at the bottom (of header)
        $headers_has_secondary_bottom = array( 'electrician', 'metal_construction' ); // fuel_industry
        
        // Global header name that chosen in Theme Options
        $header_name = isset( $theone['opt-header-type-select'] ) ? $theone['opt-header-type-select'] : 'plumber';
        
        if ( is_array( $header_name ) ) {
            $header_name = 'plumber';
        }
        
        $header_class .= isset( $theone['opt-header-style-' . $header_name] ) ? $theone['opt-header-style-' . $header_name] . '_style' : 'plumber_style';
        
        $has_secondary_section = in_array( $header_name, $headers_has_secondary );
        $config = array(
            'main_section' => array(
                'call_to_action' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_call_to_action' ),
                'logo' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_logo' ),
                'main_menu' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_main_menu' ),
                'search_icon' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_search_icon' ),
                'sliding_menu_icon' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_sliding_menu_icon' ),
                'header_mini_cart' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_header_mini_cart' ),
                'contact_info' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_contact_info' ),
            ),
            'secondary_section' => array(
                'call_to_action' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_call_to_action' ),
                'logo' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_logo' ),
                'contact_info' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_contact_info' ),
                'header_socials' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_header_socials' ),
                'search_icon' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_search_icon' ),
                'sliding_menu_icon' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_sliding_menu_icon' ),
                'top_menu' => ts_header_is_show_elem_on( $header_name, 'secondary', 'opt_top_menu' ),
                'header_mini_cart' => ts_header_is_show_elem_on( $header_name, 'main', 'opt_header_mini_cart' ),
            ),
        ); 
        
        // Global header background image
        $header_bg = isset( $theone['opt-header-bg-img'] ) ? $theone['opt-header-bg-img'] : array();
        $header_bg_src = isset( $header_bg['url'] ) ?  $header_bg['url'] : '';        
        $logo_src = isset( $theone['opt-general-logo']['url'] ) ? esc_url( $theone['opt-general-logo']['url'] ) : get_template_directory_uri() . '/assets/images/logo.png';
        
        /** Logo **/
        $logo_html = '<div class="logo"><a href="' . home_url() . '"><img src="' . esc_url( $logo_src ) . '" alt="logo"></a></div>';
        
        /** Main Menu **/
        $main_menu_html = '';
        ob_start();
        ts_header_menu( 'main_menu_1' );
        $main_menu_html .= ob_get_clean();
        
        /** Top Menu **/
        $top_menu_html = '';
        $top_menu_html .= '<div class="ts-header-top-menu-wrap">';
        ob_start();
        ts_header_top_menu();
        $top_menu_html .= ob_get_clean();
        $top_menu_html .= '</div><!-- /. ts-header-top-menu-wrap -->';
        
        /** Sliding Sidebar **/
        $sliding_sidebar_side = isset( $theone['opt-hidden-sidebar-pos'] ) ? $theone['opt-hidden-sidebar-pos'] : 'right';
        $sliding_menu_icon_html = '<div class="ts-header-item ts-header-item-sliding_menu">    
                                        <a class="ts-hiddden-sidebar-icon" href="#ts-hidden-sidebar-wrap" data-side="' . esc_attr( $sliding_sidebar_side ) . '"><i class="fa fa-navicon"></i></a>
                                    </div><!-- /.ts-header-item-sliding_menu -->'; 
        
        /** Search Icon **/
        $search_form_html = '';
        ob_start();
        get_search_form();
        $search_form_html .= ob_get_clean();  
        $search_icon_html = '';
        $search_icon_html .= '<div class="ts-header-item ts-header-item-search">    
                                <a href="#"><i class="fa fa-search"></i></a>';
                                
        if ( !in_array( $header_name, array( 'construction', 'cleaning', 'autoshop', 'metal_construction', 'electrician' ) ) ) { // Construction, Cleaning, Metal Construction, Electrician are diff
            $search_icon_html .= $search_form_html;
        }
        
        $search_icon_html .= '</div><!-- /.ts-header-item-search -->';
        
        
        
        /** Mini Cart **/
        $mini_cart_icon_html = '<a href="#"><i class="fa fa-shopping-cart"></i></a>';
        $mini_cart_dropdown_html = '';
        ob_start();
        ts_mini_cart();
        $mini_cart_dropdown_html .= ob_get_clean();
        
        $mini_cart_html = '';
        $mini_cart_html .= '<div class="ts-header-item ts-header-item-cart">';
        $mini_cart_html .= $mini_cart_icon_html;
        $mini_cart_html .= $mini_cart_dropdown_html;
        $mini_cart_html .= '</div><!-- /.ts-header-item-cart -->';
        
        /** Contact Info **/
        $contact_info_html = '';
        $contact_info_html .= '<div class="ts-header-item ts-header-item-contact-info">';
        $contact_info_html .= ts_contact_info_text( false );
        $contact_info_html .= '</div><!-- /.ts-header-item-contact-info -->';
        
        /** Call To Action **/
        $call_to_action_html = '';
        $call_to_action_html .= '<div class="ts-header-item ts-header-item-call-to-action">';
        $call_to_action_html .= ts_header_call_to_action();
        $call_to_action_html .= '</div><!-- /.ts-header-item-call-to-action -->';
        
        /** Social Icons List **/
        $social_icons_html = '';
        $social_icons_html .= '<div class="ts-header-item ts-header-item-social-icons-list">';
        $social_icons_html .= ts_header_social_icons_list();
        $social_icons_html .= '</div><!-- /.ts-header-item-social-icons-list -->';
        
        /** --- Main Section --- **/
        $main_section_class = 'header-section header-section-main';
        $main_section_class .= in_array( $header_name, $headers_has_secondary_bottom ) ? ' header-top' : ' header-bottom';
        $main_section_html .= '<div class="' . esc_attr( $main_section_class ) . '">';
        $main_section_html .= '<div class="container ' . esc_attr( $header_name ) . '-container">';
        $main_section_html .= '<div class="inner-section-container">';
        
        // Main Section Part Left ---------------------------------------
        $main_section_html .= '<div class="ts-header-section-position-left header-left header-' . esc_attr( $header_name ) . '-main-left">';
        switch ( $header_name ):
        
            case 'plumber': case 'maintenance': case 'cleaning': case 'construction': case 'mechanic': case 'autoshop': case 'carpenter': case 'metal_construction': case 'electrician': case 'mining': case 'renovation': case 'movers': case 'logistics':
                if ( $config['main_section']['logo'] && trim( $logo_src ) != '' ) {
                    $main_section_html .= $logo_html;
                }
                break;
            
            /*
            case 'fuel_industry':
                if ( $config['main_section']['contact_info'] ) {
                    $main_section_html .= $contact_info_html;
                }
                break;
            */
            
            case 'gardner':
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
        
        endswitch;
        
        // Default logo if plugin core is not active
        if ( !$is_plugin_core_active ) {
            $main_section_html .= $logo_html;
        }
        
        $main_section_html .= '</div><!-- /.ts-header-section-position-left -->'; 
        
        
        // Main Section Part Center (for some headers) ---------------------------------------
        if ( in_array( $header_name, array( 'maintenance' ) ) ) {
            $main_section_html .= '<div class="ts-header-section-position-center header-center header-' . esc_attr( $header_name ) . '-main-center">';
            
            switch ( $header_name ):
                        
                case 'maintenance': 
                    if ( $config['main_section']['header_mini_cart'] ) {
                        $main_section_html .= $mini_cart_html;
                    }
                    
                    if ( $config['main_section']['search_icon'] ) {
                        $main_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </div><!-- /.ts-header-item-search -->';
                    }
                    if ( $config['main_section']['main_menu'] ) {
                        $main_section_html .= $main_menu_html;
                    }
                    break;
            
            endswitch;
            
            $main_section_html .= '</div><!-- /.ts-header-section-position-center -->'; 
        }
        
        
        // Main Section Part Right ---------------------------------------
        $main_section_html .= '<div class="ts-header-section-position-right header-right header-' . esc_attr( $header_name ) . '-main-right">';
        switch ( $header_name ):
        
            case 'plumber':
                if ( $config['main_section']['sliding_menu_icon'] ) {
                    $main_section_html .= $sliding_menu_icon_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= $search_icon_html;
                }
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'maintenance':
                if ( $config['main_section']['call_to_action'] ) {
                    $main_section_html .= $call_to_action_html;
                }
                break;
            
            case 'cleaning':
                if ( $config['main_section']['sliding_menu_icon'] ) {
                    $main_section_html .= $sliding_menu_icon_html;
                }
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </div><!-- /.ts-header-item-search -->';
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'construction': case 'logistics': 
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                            <a href="#"><i class="fa fa-search"></i></a>
                                        </div><!-- /.ts-header-item-search -->';
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'autoshop':
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                            <a href="#"><i class="fa fa-search"></i></a>
                                        </div><!-- /.ts-header-item-search -->';
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'mechanic': 
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </div><!-- /.ts-header-item-search -->';
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'carpenter': case 'renovation': case 'movers': case 'fuel_industry':
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= $search_icon_html;
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'metal_construction':
                if ( $config['main_section']['call_to_action'] ) {
                    $main_section_html .= $call_to_action_html;
                }
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                                <a href="#"><i class="fa fa-search"></i></a>
                                            </div><!-- /.ts-header-item-search -->';
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'electrician':
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'mining':
                if ( $config['main_section']['sliding_menu_icon'] ) {
                    $main_section_html .= $sliding_menu_icon_html;
                }
                if ( $config['main_section']['header_mini_cart'] && class_exists( 'WooCommerce' ) ) {
                    $main_section_html .= $mini_cart_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= $search_icon_html;
                }
                if ( $config['main_section']['call_to_action'] ) {
                    $main_section_html .= $call_to_action_html;
                }
                if ( $config['main_section']['main_menu'] ) {
                    $main_section_html .= $main_menu_html;
                }
                break;
            
            case 'gardner':
                if ( $config['main_section']['sliding_menu_icon'] ) {
                    $main_section_html .= $sliding_menu_icon_html;
                }
                if ( $config['main_section']['search_icon'] ) {
                    $main_section_html .= $search_icon_html;
                }
                if ( $config['main_section']['call_to_action'] ) {
                    $main_section_html .= $call_to_action_html;
                }
                break;
            
            default:
                // Do nothing
                break;
        
        endswitch;
        
        // Default menu if plugin core is not active
        if ( !$is_plugin_core_active ) {
            $main_section_html .= $main_menu_html;
        }
        
        $main_section_html .= '</div><!-- /.ts-header-section-position-right -->'; 
        
        $main_section_html .= '</div><!-- /.inner-section-container -->';
        
        // Mini cart outside: construction
        switch ( $header_name ):
            
            case 'construction':
                $main_section_html .= '<div class="ts-mini-cart-dropdown-outsite">' . $mini_cart_dropdown_html . '</div>';
                break;
            
        endswitch;
        
        $main_section_html .= '</div><!-- /.container .' . esc_attr( $header_name ) . '-container-->';
        $main_section_html .= '</div><!-- /.header-section-main -->';
        
        // Search form outside header section main for Cleaning, Mechanic, Maintenance, Metal Construction
        if ( in_array( $header_name, array( 'cleaning', 'mechanic', 'maintenance', 'metal_construction' ) ) && ( $config['main_section']['search_icon'] ) ) {
            $main_section_html .= '<div class="header-search-form-outside header-search-form-for-' . esc_attr( $header_name ) . '">
                                        <div class="container header-search-form-container">
                                            ' . $search_form_html . '
                                            <a href="#" class="close-search-form" title="' . __( 'Close', 'themestudio' ) . '"><i class="fa fa-times"></i></a>
                                        </div>
                                    </div>';
        }
        
        /** --- Secondary Section --- **/
        if ( $has_secondary_section ) {
            $secondary_section_class = 'header-section header-section-secondary header-section-second';
            $secondary_section_class .= in_array( $header_name, $headers_has_secondary_bottom ) ? ' header-bottom' : ' header-top';
            
            // Search form outside header section main (on the top of second) for Construction, Autoshop, Logistics
            if ( in_array( $header_name, array( 'construction', 'autoshop', 'logistics' ) ) && ( $config['main_section']['search_icon'] ) ) {
                $secondary_section_html .= '<div class="header-search-form-outside header-search-form-for-' . esc_attr( $header_name ) . '">
                                                <div class="container header-search-form-container">
                                                    ' . $search_form_html . '
                                                    <a href="#" class="close-search-form" title="' . __( 'Close', 'themestudio' ) . '"><i class="fa fa-times"></i></a>
                                                </div>
                                            </div>';
            }
            
            $secondary_section_html .= '<div class="' . esc_attr( $secondary_section_class ) . '">';
            $secondary_section_html .= '<div class="container ' . esc_attr( $header_name ) . '-container">';
            $secondary_section_html .= '<div class="inner-section-container">';
            
            // Headers has secondary left
            if ( in_array( $header_name, array( 'cleaning', 'construction', 'mechanic', 'autoshop', 'carpenter', 'metal_construction', 'electrician', 'movers', 'gardner', 'fuel_industry', 'logistics' ) ) ) { 
            
                $secondary_section_html .= '<div class="ts-header-section-position-left header-left header-' . esc_attr( $header_name ) . '-second-left">'; 
                switch ( $header_name ):
                    
                    case 'cleaning': case 'metal_construction': case 'carpenter': case 'movers': case 'logistics':
                        if ( $config['secondary_section']['contact_info'] ) {
                            $secondary_section_html .= $contact_info_html;
                        }
                        break;
                    
                    case 'autoshop':
                        if ( $config['secondary_section']['header_socials'] ) {
                            $secondary_section_html .= $social_icons_html;
                        }
                        if ( $config['secondary_section']['contact_info'] ) {
                            $secondary_section_html .= $contact_info_html;
                        }
                        break;
                    
                    case 'construction': case 'mechanic':
                        if ( $config['secondary_section']['call_to_action'] ) {
                            $secondary_section_html .= $call_to_action_html;
                        }
                        if ( $config['secondary_section']['contact_info'] ) {
                            $secondary_section_html .= $contact_info_html;
                        }
                        break;
                    
                    case 'electrician': 
                        if ( $config['secondary_section']['contact_info'] ) {
                            $secondary_section_html .= $contact_info_html;
                        }
                        if ( $config['secondary_section']['header_socials'] ) {
                            $secondary_section_html .= $social_icons_html;
                        }
                        break;
                    
                    case 'fuel_industry': case 'gardner':
                        if ( $config['secondary_section']['logo'] && trim( $logo_src ) != '' ) {
                            $secondary_section_html .= $logo_html;
                        }
                        break;
                        
                endswitch;
                $secondary_section_html .= '</div><!-- /.ts-header-section-position-left -->';
            }
            
            // Headers has secondary right
            if ( in_array( $header_name, array( 'plumber', 'cleaning', 'construction', 'mechanic', 'autoshop', 'carpenter', 'metal_construction', 'electrician', 'movers', 'fuel_industry', 'logistics' ) ) ) { 
                $secondary_section_html .= '<div class="ts-header-section-position-right header-right header-' . esc_attr( $header_name ) . '-second-right">';
                switch ( $header_name ):
                    
                    case 'plumber':
                        if ( $config['secondary_section']['contact_info'] ) {
                            $secondary_section_html .= $contact_info_html;
                        }
                        break;
                    
                    case 'cleaning':
                        if ( $config['secondary_section']['header_socials'] ) {
                            $secondary_section_html .= $social_icons_html;
                        }
                        if ( $config['secondary_section']['call_to_action'] ) {
                            $secondary_section_html .= $call_to_action_html;
                        }
                        break;
                    
                    case 'construction':
                        if ( $config['secondary_section']['header_socials'] ) {
                            $secondary_section_html .= $social_icons_html;
                        }
                        if ( $config['secondary_section']['header_mini_cart'] ) {
                            //$secondary_section_html .= $mini_cart_html;
                            $secondary_section_html .= '<div class="ts-header-item ts-header-item-cart-outside ts-header-item-cart-' . esc_attr( $header_name ) . '">';
                            $secondary_section_html .= $mini_cart_icon_html;
                            $secondary_section_html .= '</div><!-- /.ts-header-item-cart -->';
                        }
                        if ( $config['secondary_section']['sliding_menu_icon'] ) {
                            $secondary_section_html .= $sliding_menu_icon_html;
                        }
                        break;
                    
                    case 'mechanic': case 'carpenter': case 'metal_construction': case 'movers': case 'logistics':
                        if ( $config['secondary_section']['header_socials'] ) {
                            $secondary_section_html .= $social_icons_html;
                        }
                        break;
                    
                    case 'autoshop':
                        if ( $config['secondary_section']['top_menu'] ) {
                            $secondary_section_html .= $top_menu_html;
                        }
                        break;
                    
                    case 'electrician':
                        if ( $config['secondary_section']['call_to_action'] ) {
                            $secondary_section_html .= $call_to_action_html;
                        }
                        if ( $config['secondary_section']['search_icon'] ) {
                            $secondary_section_html .= '<div class="ts-header-item ts-header-item-search icon-for-search-form-outside ts-header-item-search-' . esc_attr( $header_name ) . '">    
                                                        <a href="#"><i class="fa fa-search"></i></a>
                                                    </div><!-- /.ts-header-item-search -->';
                        }
                        if ( $config['secondary_section']['header_mini_cart'] ) {
                            $secondary_section_html .= $mini_cart_html;
                        }
                        break;
                    
                    case 'fuel_industry':
                        /*if ( $config['secondary_section']['call_to_action'] ) {
                            $secondary_section_html .= $call_to_action_html;
                        }*/
                        if ( $config['secondary_section']['contact_info'] ) {
                            $secondary_section_html .= $contact_info_html;
                        }
                        break;
                    
                endswitch; 
                $secondary_section_html .= '</div><!-- /.ts-header-section-position-right -->';
            }
            
            $secondary_section_html .= '</div><!-- /.inner-section-container -->';
            $secondary_section_html .= '</div><!-- /.container .' . esc_attr( $header_name ) . '-container-->';
            $secondary_section_html .= '</div><!-- /.header-section-secondary -->';   
            // Search form outside header section secondary (at the bottom of second) for Electrician
            if ( in_array( $header_name, array( 'electrician' ) ) && ( $config['secondary_section']['search_icon'] ) ) {
                $secondary_section_html .= '<div class="header-search-form-outside header-search-form-for-' . esc_attr( $header_name ) . '">
                                                <div class="container header-search-form-container">
                                                    ' . $search_form_html . '
                                                    <a href="#" class="close-search-form" title="' . __( 'Close', 'themestudio' ) . '"><i class="fa fa-times"></i></a>
                                                </div>
                                            </div>';
            }
        } // End if ( $has_secondary_section )
        
        // The Title
        $title_bre_html = '';
        $title_html = '';
        $subtitle_html = '';
        $breadcrumbs_html = '';
        $icon_html = '';
        
        $is_blog_page = false;
        $show_breadcrumb = 'yes';
        $title = '';
        $subtitle = '';
        $icon_class = '';
        
        // Get default icon (global setting in the Theme Options)
        if ( in_array( $header_name, $headers_has_icon_default ) ) {
            $icon_class = isset( $theone['opt-header-icon-class'] ) ? $theone['opt-header-icon-class'] : '';
        }
        
        $show_title_by_default = !$is_plugin_core_active;
        $show_subtitle_by_default = false;
        $show_breadcrumb_by_default = !$is_plugin_core_active;
        
        if ( in_array( $header_name, $headers_has_title_default ) && isset( $theone['opt-header-title-enable'] ) ) {
            $show_title_by_default = $theone['opt-header-title-enable'] == 1;
        } 
        
        if ( in_array( $header_name, $headers_has_subtitle_default ) && isset( $theone['opt-header-subtitle-enable'] ) ) {
            $show_subtitle_by_default = $theone['opt-header-subtitle-enable'] == 1;
        } 
        
        if ( in_array( $header_name, $headers_has_breadcrumb_default ) && isset( $theone['opt-header-breadcrumb-enable'] ) ) {
            $show_breadcrumb_by_default = $theone['opt-header-breadcrumb-enable'] == 1;
        } 
        
        $post_id = 0; // For singular
        if ( is_front_page() && is_home() ) {
            // Default homepage
            if ( !$is_plugin_core_active ) { // Default in case of plugin core is not active
                $title = get_bloginfo( 'name' );
            }
        } elseif ( is_front_page() ) {
            // static homepage
            $post_id = get_the_ID();
            $title = get_the_title();
        } elseif ( is_home() ) {
            // blog page
            $post_id = get_option( 'page_for_posts' );
            $title = get_the_title( $post_id );
            $is_blog_page = true;
        } else {
            //everything else
            if ( is_singular() ) {
                $post_id = get_the_ID();
                $title = get_the_title();
            }
            else{
                if ( is_archive() ) {
                    $title = post_type_archive_title( '', false );
                    
                    // Checking for shop archive
                    if ( function_exists( 'is_shop' ) ) {
                        if ( is_shop() ) {
                            $shop_page_id = get_option( 'woocommerce_shop_page_id' );
                            $post_id = $shop_page_id;
                            $title = get_the_title( $post_id );
                        }   
                    }
                    
                }
                else{
                    $title = single_cat_title( '', false );   
                }
            }
        }
        
        $use_custom_title = get_post_meta( $post_id, '_ts_use_custom_title', true );
        
        if ( trim( $use_custom_title ) == 'yes' ) {
            $title = get_post_meta( $post_id, '_ts_custom_header_title', true );
        }
        else{
            if ( $post_id > 0 && !$show_title_by_default ) { // If is singular and global setting is disabled title
                $title = '';
            }
        }
        
        if ( $post_id > 0 ) {
            $icon_class = get_post_meta( $post_id, '_ts_header_font_class', true );
            $subtitle = get_post_meta( $post_id, '_ts_header_subtitle', true );
            $show_breadcrumb = get_post_meta( $post_id, '_ts_header_show_breadcrumb', true );
        }
        else{
            $show_breadcrumb = $show_breadcrumb_by_default ? 'yes' : 'no';
        }
        
        
        
        if ( is_search() ) {
            $title = sprintf( __( 'Search results for', 'themestudio' ) . ' "%s"', get_search_query());
            $icon_class = 'icon-Search';
        }
        
        if ( is_404() ) {
            $title = isset( $theone['opt-404-header-title'] ) ? sanitize_text_field( $theone['opt-404-header-title'] ) : '';
            $subtitle = isset( $theone['opt-404-header-subtitle'] ) ? sanitize_text_field( $theone['opt-404-header-subtitle'] ) : '';
            $icon_class = 'icon-Ringer';
        }
        
        if ( trim( $title ) != '' && in_array( $header_name, $headers_has_title_default ) ) { // Check if title available and it's allowed to show on this header
            $title_html .= '<h2 class="page-title">' . $title . '</h2>';
            if ( $post_id <= 0 && !$show_title_by_default ) {
                $title_html = '';
            }
        }
        
        if ( trim( $subtitle ) != '' && in_array( $header_name, $headers_has_subtitle_default ) ) { // Check if subtitle available and it's allowed to show on this header
            $subtitle_html .= '<h6>' . sanitize_text_field( $subtitle ) . '</h6>';
            if ( $post_id <= 0 && !$show_subtitle_by_default ) {
                $subtitle_html = '';
            }
        }
        
        if ( trim( $icon_class ) != '' && in_array( $header_name, $headers_has_icon_default ) ) {
            $icon_html = '<div class="title-icon"><span class="icon ' . esc_attr( $icon_class ) . '"></span></div>';
        }
        
        if ( $show_breadcrumb == 'yes' ) {
            ob_start();
            
            if ( function_exists( 'breadcrumb_trail' ) ) {
                breadcrumb_trail( array(
                    'show_browse'   =>  false
                ) );
            }
            else{
                ts_get_breadcrumbs();
            }
            $breadcrumbs_html = ob_get_clean();
        }
        
        // construction, mechanic --> subtile before the main title
        if ( in_array( $header_name, array( 'construction', 'mechanic' ) ) ) {
            $title_html = $subtitle_html . $title_html;
        }
        else{ // subtile after the main title 
            $title_html = $title_html . $subtitle_html;
        }
        
        $title_html = trim( $title_html ) != '' ? '<div class="title-text">' . $title_html . '</div>' : '';
        $title_wrap_class = 'title-wrap';
        if ( trim( $icon_html . $title_html . $breadcrumbs_html ) == '' ) {
            $title_wrap_class .= ' empty';
            $bg_img_wrap_class .= ' no-title no-icon no-breadcrumb empty-content';
            $slider_wrap_class .= ' no-title no-icon no-breadcrumb empty-content';
        }
        else {
            $bg_img_wrap_class .= trim( $icon_html ) != '' ? ' has-icon' : ' no-icon';
            $bg_img_wrap_class .= trim( $title_html ) != '' ? ' has-title' : ' no-title';
            $bg_img_wrap_class .= trim( $breadcrumbs_html ) != '' ? ' has-breadcrumb' : ' no-breadcrumb';
            $slider_wrap_class .= trim( $icon_html ) != '' && !( is_front_page() || is_home() ) ? ' has-icon' : ' no-icon';
            $slider_wrap_class .= trim( $title_html ) != '' && !( is_front_page() || is_home() ) ? ' has-title' : ' no-title';
            $slider_wrap_class .= trim( $breadcrumbs_html ) != '' && !( is_front_page() || is_home() ) ? ' has-breadcrumb' : ' no-breadcrumb'; 
        }
        
        if ( in_array( $header_name, array( 'electrician' )  ) ) { // Title structure for Electrician
            $title_bre_html .= '<div class="' . esc_attr( $title_wrap_class ) . '">'; // title-wrap
            $title_bre_html .= '<div class="container title-container">';
            $title_bre_html .= '<div class="inner-title-container">';
            $title_bre_html .=      '<div class="row">
                                        <div class="col-md-9 col-sm-9">
                                            ' .  $title_html . $breadcrumbs_html . '
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            ' . $icon_html . '
                                        </div>
                                    </div><!-- /.row -->';
            $title_bre_html .= '</div><!-- /.inner-title-container -->';
            $title_bre_html .= '</div><!-- /.title-container -->';
            $title_bre_html .= '</div><!-- /.title-wrap -->';
        }
        else{ // All other
            $title_bre_html .= '<div class="' . esc_attr( $title_wrap_class ) . '">'; // title-wrap
            $title_bre_html .= '<div class="container title-container">';
            $title_bre_html .= '<div class="inner-title-container">';
            $title_bre_html .=      '<div class="row">
                                        <div class="col-md-9 col-sm-9">
                                            ' . $icon_html . $title_html . '
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            ' . $breadcrumbs_html . '
                                        </div>
                                    </div><!-- /.row -->';
            $title_bre_html .= '</div><!-- /.inner-title-container -->';
            $title_bre_html .= '</div><!-- /.title-container -->';
            $title_bre_html .= '</div><!-- /.title-wrap -->';   
        }
        
        
        /** --- Header Background --- **/
        // Check for custom header background type 
        $header_bg_type = 'global';
        $rev_slider_alias = '';
        if ( $post_id > 0 ) {
            $header_bg_type = get_post_meta( $post_id, '_ts_header_bg_type', true );
            if ( $header_bg_type == 'image' ) {
                $header_bg_src = get_post_meta( $post_id, '_ts_header_bg_src', true );
                $header_class .= ' has-custom-bg-image';
            }
            
            if ( $header_bg_type == 'slider' ) {
                //$header_bg_src = get_post_meta( $post_id, '_ts_header_bg_src', true );
                $rev_slider_alias = get_post_meta( $post_id, '_ts_header_rev_slider_alias', true );
            }
        }
        
        if ( $header_bg_type != 'slider' ) { // Header bg is custom image or using global image
            
            $header_class .= trim( $header_bg_src ) != '' ? ' has-bg-image': ' no-bg-image';
            $header_bg_img_style = trim( $header_bg_src ) != '' ? 'style="background-image: url(' . trim( $header_bg_src ) . ');"' : '';
            $bg_img_html .= '<div class="header-bg-img header-bg-for-' . esc_attr( $header_name . ' ' . $bg_img_wrap_class ) . '" ' . $header_bg_img_style . '>'; // Begin bg img html 
            
            /** Overlay if set **/
            $enable_header_img_overlay = isset( $theone['opt-header-img-overlay'] ) ? $theone['opt-header-img-overlay'] == '1' : false;
            if ( $enable_header_img_overlay ) {
                $bg_img_html .= '<div class="header-bg-img-overlay"></div>';   
            }
            
            // Don't display the title and breadcrumb on the home page or the front page
            if ( is_front_page() && is_home() ) {
                // Default homepage
                if ( !$is_plugin_core_active ) {
                    $bg_img_html .= $title_bre_html;
                }
            } elseif ( is_front_page() ) {
                // static homepage
                if ( !$is_plugin_core_active ) {
                    $bg_img_html .= $title_bre_html;
                }
            } elseif ( is_home() ) { // $is_blog_page == true
                // blog page
                $bg_img_html .= $title_bre_html;
            } else {
                //everything else
                $bg_img_html .= $title_bre_html;
            }
            
            $bg_img_html.= '</div><!-- /.header-bg-img -->'; // End bg img html 
        }
        else { // Header bg is slider
            if ( trim( $rev_slider_alias ) != '' ) {            
                
                $rev_slider = '';
                ob_start();
                echo do_shortcode( '[rev_slider ' . $rev_slider_alias . ']' );
                $rev_slider = ob_get_clean();
                
                $slider_html = '<div id="slider-wrap" class="slider-wrap ' . esc_attr( $slider_wrap_class ) . '">';
                                
                $slider_html .= $rev_slider;
                if ( is_front_page() && is_home() ) {
                    // Default homepage
                } elseif ( is_front_page() ) {
                    // static homepage
                } elseif ( is_home() ) { // $is_blog_page == true
                    // blog page
                    $slider_html .= $title_bre_html;
                } else {
                    //everything else
                    $slider_html .= $title_bre_html;
                }
                $slider_html .= '</div>';
                
                $header_class .= ' has-slider';
            }
            else {
                $header_class .= ' no-slider';
            }
        }
        
        
        /** === HEADER MAIN === **/
        $header_class .= ' header-main';
        $header_class .= ' style-' . $header_name;
        
        $html .= '<div id="header-main" class="' . esc_attr( trim( $header_class ) ) . '">'; // no-slider has-bg-image
        if ( in_array( $header_name, $headers_has_secondary_bottom ) ) {
            $html .= $main_section_html . $secondary_section_html;   
        }
        else{
            $html .= $secondary_section_html . $main_section_html;
        }
        $html .= '</div><!-- /.header-main -->';
        $html .= $bg_img_html;
        
        // Slider on top
        if ( in_array( $header_name, array( 'mining' ) ) ) {
            $html = $slider_html . $html;
        }
        else{
            $html .= $slider_html;
        }
        
        echo $html;
    }
  }
  
  if ( !function_exists( 'ts_header_is_show_elem_on' ) ) {
    
    /**
     *  $header_name = construction, plumber...
     *  $section_name = main, secondary
     *  $elem = opt_logo, opt_main_menu...
     **/
    function ts_header_is_show_elem_on( $header_name, $section_name, $elem ) {
        // Global vars from init.php
        global $headers_allow_logo, $headers_allow_main_menu, $headers_allow_top_menu, $headers_allow_search_icon, $headers_allow_call_to_action, 
        $headers_allow_contact_info, $headers_allow_socials, $headers_allow_sliding_menu, $headers_allow_mini_cart;
        global $theone;
        
        $allow_args = array();
        
        switch ( $elem ):
        
            case 'opt_call_to_action': 
                $allow_args = $headers_allow_call_to_action;
                break;
            
            case 'opt_logo': 
                $allow_args = $headers_allow_logo;
                break;
            
            case 'opt_main_menu': 
                $allow_args = $headers_allow_main_menu;
                break;
            
            case 'opt_search_icon': 
                $allow_args = $headers_allow_search_icon;
                break;
            
            case 'opt_sliding_menu_icon': 
                $allow_args = $headers_allow_sliding_menu;
                break;
            
            case 'opt_contact_info': 
                $allow_args = $headers_allow_contact_info;
                break;
            
            case 'opt_header_socials': 
                $allow_args = $headers_allow_socials;
                break;
            
            case 'opt_top_menu': 
                $allow_args = $headers_allow_top_menu;
                break;
            
            case 'opt_header_mini_cart': 
                $allow_args = $headers_allow_mini_cart;
                break;
        
        endswitch;
        
        if ( !isset( $theone[$elem . '_' . $section_name] ) ) {
            return false;
        }
        
        return $theone[$elem . '_' . $section_name] == 1 && in_array( $header_name, $allow_args );
        
    }
  }
  
  if ( !function_exists( 'ts_header_menu' ) ) {
    function ts_header_menu( $location = '', $show_mobile = true ) {
        ?>
        
        <?php if ( has_nav_menu( $location ) ): ?>
            <div class="navbar navbar-default navbar-static-top main-nav">
                <?php if ( $show_mobile ): ?>
                    <div class="navbar-header">
                      <button aria-controls="navbar" aria-expanded="true" data-target="#navbar" data-toggle="collapse" class="navbar-toggle kebab collapsed" type="button">
                        <span></span>
                        <span></span>
                        <span></span>
                        <p class="cross middle">x</p>
                        <span></span>
                        <span></span>
                        <span></span>
                      </button>
                      <a href="#" class="navbar-brand"><?php _e( 'Project name', 'themestudio' ); ?></a>
                    </div>
                <?php endif; ?>
                <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="menubar">
                    <?php wp_nav_menu( array( 
                        'theme_location'    => $location, 
                        'menu_class' => 'nav navbar-nav',
                        'container' => false,
                        'fallback_cb' => 'ts_bootstrap_navwalker::fallback',
                        'walker' => new ts_bootstrap_navwalker_2()
                    ) ); ?>
                </div>
            </div>
        <?php else: ?>
            <div class="navbar navbar-default navbar-static-top main-nav">
                <?php if ( $show_mobile ): ?>
                    <div class="navbar-header">
                      <button aria-controls="navbar" aria-expanded="true" data-target="#navbar" data-toggle="collapse" class="navbar-toggle kebab collapsed" type="button">
                        <span></span>
                        <span></span>
                        <span></span>
                        <p class="cross middle">x</p>
                        <span></span>
                        <span></span>
                        <span></span>
                      </button>
                      <a href="#" class="navbar-brand"><?php _e( 'Project name', 'themestudio' ); ?></a>
                    </div>
                <?php endif; ?>
                <div id="navbar" class="navbar-collapse collapse" aria-expanded="true" role="menubar">
                    <ul id="menu-main-menu" class="nav navbar-nav">
                        <li class="menu-item">
                            <a href="<?php menu_page_url( 'nav-menus' ); ?>"><?php _e( 'Add a menu', 'themestudio' ); ?></a>
                        </li>
                    </ul>
                </div>
            </div>
        <?php endif; ?>
        
        <?php
    }
  }
  
  if ( !function_exists( 'ts_header_top_menu' ) ) {
    function ts_header_top_menu() {
        ?>
        
        <?php if ( has_nav_menu( 'top_menu' ) ): ?>
            <?php wp_nav_menu( array( 
                'theme_location'    => 'top_menu', 
                'menu_class' => 'header-menu-top',
                'container' => false,
                'fallback_cb' => 'ts_bootstrap_navwalker::fallback',
                'walker' => new ts_bootstrap_navwalker()
            ) ); ?>
        <?php endif; ?>
        
        <?php
    }
  }
  
  if ( !function_exists( 'ts_contact_info_text' ) ) {
    function ts_contact_info_text( $echo = true ) {
        global $theone;
        
        $html = '';
        $html_args = array(); 
        
        if ( isset( $theone['opt-phone-numbers'] ) ) {
            if ( trim( $theone['opt-phone-numbers'] ) != '' ) {
                $html_args[] = '<div class="contact-text">' . __( '<span>Phone :</span>', 'themestudio' ) . sanitize_text_field( $theone['opt-phone-numbers'] ) . '</div>';
            }
        }
        
        if ( isset( $theone['opt-email'] ) ) {
            if ( trim( $theone['opt-email'] ) != '' ) {
                $html_args[] = '<div class="contact-text">' . __( '<span>Email :</span>', 'themestudio' ) . sanitize_email( $theone['opt-email'] ) . '</div>';
            }
        }
        
        if ( isset( $theone['opt-address'] ) ) {
            if ( trim( $theone['opt-address'] ) != '' ) {
                $html_args[] = '<div class="contact-text">' . __( '<span>Address :</span>', 'themestudio' ) . sanitize_text_field( $theone['opt-address'] ) . '</div>';   
            }
        }
        
        //$html = isset( $theone['opt-header-text'] ) ? $theone['opt-header-text'] : '';
        $html .= implode( '<span class="sep">|</span>', $html_args );
        
        if ( $echo ) {
            echo $html;
        }
        else{
            return $html;
        }
    }
  }
  


  if( !function_exists( 'ts_search_form' ) ) {

    /*
     * Filter Search form
    */
    function ts_search_form( $form ) {

        $form = '<form role="search" method="get" action="' . home_url( '/' ) . '" >
                  <input type="text" value="" placeholder="' . __( 'Search words here', 'themestudio' ) . '" class="search" id="s" name="s">
                  <button type="submit" class="search-submit"><i class="fa fa-search"></i> </button>
                </form>';

        return $form;
    }
    add_filter( 'get_search_form', 'ts_search_form' );

  }
    
  
  if ( !function_exists( 'ts_header_call_to_action' ) ) {
    /**
     * Return Header Call To Action Html 
     **/
    function ts_header_call_to_action() {
        global $theone;
        
        $html = '';
        
        if ( isset( $theone['opt-header-btn-text'] ) ) {
            if ( trim( $theone['opt-header-btn-text'] ) != '' ) {
                if ( trim( $theone['opt-header-btn-link'] ) != '' ) {
                    $html .= '<span class="header-text has-link"><a class="header-link" href="' . esc_url( $theone['opt-header-btn-link'] ) . '">' . sanitize_text_field( $theone['opt-header-btn-text'] ) . '</a></span>';
                }
                else{
                    $html .= '<span class="header-text no-link">' . sanitize_text_field( $theone['opt-header-btn-text'] ) . '</span>';
                }
            }
        }
        
        return $html;
    }
  }
  
  if ( !function_exists( 'ts_header_social_icons_list' ) ) {
    /**
     * Return html of social icons list 
     **/
    function ts_header_social_icons_list() {
        global $theone;
        
        $html = '';
        
        if ( isset( $theone['opt-fb-link'] ) ) {
            $html .= trim( $theone['opt-fb-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-fb-link'] ) . '" target="_blank"><i class="fa fa-facebook"></i></a>' : '';
        }
        if ( isset( $theone['opt-google-plus-link'] ) ) {
            $html .= trim( $theone['opt-google-plus-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-google-plus-link'] ) . '" target="_blank"><i class="fa fa-google-plus"></i></a>' : '';   
        }
        if ( isset( $theone['opt-twitter-link'] ) ) {
            $html .= trim( $theone['opt-twitter-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-twitter-link'] ) . '" target="_blank"><i class="fa fa-twitter"></i></a>' : '';   
        }
        if ( isset( $theone['opt-youtube-link'] ) ) {
            $html .= trim( $theone['opt-youtube-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-youtube-link'] ) . '" target="_blank"><i class="fa fa-youtube"></i></a>' : '';   
        }
        if ( isset( $theone['opt-vimeo-link'] ) ) {
            $html .= trim( $theone['opt-vimeo-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-vimeo-link'] ) . '" target="_blank"><i class="fa fa-vimeo-square"></i></a>' : '';   
        }
        if ( isset( $theone['opt-linkedin-link'] ) ) {
            $html .= trim( $theone['opt-linkedin-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-linkedin-link'] ) . '" target="_blank"><i class="fa fa-linkedin"></i></a>' : '';   
        }
        if ( isset( $theone['opt-rss-link'] ) ) {
            $html .= trim( $theone['opt-rss-link'] ) != '' ? '<a href="' . esc_url( $theone['opt-rss-link'] ) . '" target="_blank"><i class="fa fa-rss"></i></a>' : '';   
        }
        
        if ( trim( $html ) != '' ) {
            $html = '<div class="header-icons">' . $html . '</div>';
        }
        
        return $html;
        
    }
  }

  if( !function_exists( 'get_archives_link_custom' ) ) {

    /*
     * get archives link custom
    */
    function get_archives_link_custom($url) {
      $link = str_replace('<li>', '<li class="cat-item">', $url);
      return $link;
    }
    add_filter('get_archives_link','get_archives_link_custom');

  }

  if(!( function_exists('ts_the_isotope_terms') )){

    /*
     * the isotope terms
    */
    function ts_the_isotope_terms() {
      global $post;
      if( get_the_terms($post->ID,'ts_portfolio_cats') ) {
        $terms = get_the_terms($post->ID,'ts_portfolio_cats','','','');
        $terms = array_map('_isotope_cb', $terms);
        return implode(' ', $terms);
      }
    }

  }

  if(!( function_exists('_isotope_cb') )){
    
    function _isotope_cb($t) {  
      return $t->slug; 
    }

  }

  if(!( function_exists('ts_pagination') )){

    /*
     * function pahgination
    */
    function ts_pagination($pages = '', $range = 2){
      $showitems = ($range * 2)+1;
      
      global $paged;
      if(empty($paged)) $paged = 1;
      
      if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
          if(!$pages) {
            $pages = 1;
          }
      }
      
      $output = '';
      
      if(1 != $pages){
        $output .= "<div class='pagination'><ul>";
        if ($paged > 1) {
          // $output .= previous_posts_link( __("prev",'themestudio') );
          $output.= '<li>'.get_previous_posts_link( '<i class="fa fa-angle-left"></i>' ).'</li>';
        } else {
          $output.= '<li style="display: none"><a href="#"><i class="fa fa-angle-right"></i></a></li>';
        }
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) $output .= "<li><a href='".get_pagenum_link(1)."'><i class='fa fa-angle-left'></i></a></li> ";
        
        for ($i=1; $i <= $pages; $i++){
          if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
            $output .= ($paged == $i)? "<li class='page-active'><a href='".get_pagenum_link($i)."'>".$i."</a></li> ":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li> ";
          }
        }
      
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) $output .= "<li><a href='".get_pagenum_link($pages)."'><i class='fa fa-angle-right'></i></a></li> ";
        if ($paged < $pages) {
          // $output.= next_posts_link( __("next",'themestudio') );
          $output.= '<li>'.get_next_posts_link( '<i class="fa fa-angle-right"></i>' ).'</li>';
        } else {
          $output.= '<li style="display: none"><a href="#"><i class="fa fa-angle-right"></i></a></li>';
        }
        $output.= "</ul></div>";
      }
      
      return $output;
    }

  }
  
  if ( !function_exists( 'ts_update_post_view_count' ) ) {
    
    function ts_update_post_view_count( $post_id = 0 ) {
        
        $post_id = intval( $post_id );
        
        if ( $post_id == 0 ) {
            $post_id = get_the_ID();
        }
        
        if ( $post_id > 0 && is_singular() ) {
            $count = max( 0, intval( get_post_meta( $post_id, '_ts_post_views_count', true ) ) );
            $count++;
            update_post_meta( $post_id, '_ts_post_views_count', $count );   
        }
        
    }
    
  }
  
  if ( !function_exists( 'ts_get_post_view_count' ) ) {
    
    function ts_get_post_view_count( $post_id = 0 ) {
        
        $post_id = intval( $post_id );
        
        if ( $post_id == 0 ) {
            $post_id = get_the_ID();
        }
        
        $count = max( 0, intval( get_post_meta( $post_id, '_ts_post_views_count', true ) ) );
        
        return $count;
        
    }
    
  }
  
  if ( !function_exists( 'ts_theme_comment' ) ) {
    
    function ts_theme_comment($comment, $args, $depth) {
    	$GLOBALS['comment'] = $comment;
    	extract($args, EXTR_SKIP);
    
    	if ( 'div' == $args['style'] ) {
    		$tag = 'div';
    		$add_below = 'comment';
    	} else {
    		$tag = 'li';
    		$add_below = 'div-comment';
    	}
    ?>
    	<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    	<?php if ( 'div' != $args['style'] ) : ?>
    	<div id="div-comment-<?php comment_ID() ?>" class="comment-body comment-item">
    	<?php endif; ?>
    	<div class="comment-author vcard">
    	<?php echo get_avatar( $comment, 44 ); ?>
    	<?php printf( __( '<cite class="fn">%s</cite>' ), get_comment_author_link() ); ?>
    	</div>
    	<?php if ( $comment->comment_approved == '0' ) : ?>
    		<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'themestudio' ); ?></em>
    		<br />
    	<?php endif; ?>
    
    	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
    		<?php
    			/* translators: 1: date, 2: time */
    			printf( __( '%1$s at %2$s', 'themestudio' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'themestudio' ), '  ', '' );
    		?>
    	</div>
    
    	<div class="comment-text-wrap">
            <?php comment_text(); ?>
        </div>
    
    	<div class="reply">
    	<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
    	</div>
    	<?php if ( 'div' != $args['style'] ) : ?>
    	</div>
    	<?php endif; ?>
    <?php
    }
    
  }
  
  if ( !function_exists( 'ts_is_plugin_active' ) ) {
    
    /**
     * Check for plugin is active or not
     * @param   $plugin     Plugin path. Ex: 'plugin-directory/plugin-file.php'
     * @since   1.0.0 
     **/
    function ts_is_plugin_active( $plugin = '' ) {
        if ( !is_admin() ) {
            /**
             * Detect plugin. For use on Front End only.
             */
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }
        
        return is_plugin_active( $plugin );
    }
  }
  
  if ( !function_exists( 'ts_mini_cart' ) ) {
    /**
     *  Mini cart on top menu
     *  @since 1.0 
     **/
    function ts_mini_cart() {
        
        if ( class_exists( 'WooCommerce' ) ):
            
            $args = array(
        		'list_class' => ''
        	);
            
            ob_start();
        	wc_get_template( 'cart/mini-cart.php', $args );
            $mini_cart = ob_get_clean();
            
            echo    '<div class="ts-mini-cart-wrap">
                        <div class="ts-mini-cart-wrap-content">
                            ' . $mini_cart . '
                        </div>
                    </div>';
            
        endif;
    }
  }
  
  
  if ( !function_exists( 'ts_add_hidden_sidebar' ) ) {
    
    function ts_add_hidden_sidebar() {
        
        if ( is_active_sidebar( 'ts-hidden-sidebar' ) ) :
            echo '<div id="ts-hidden-sidebar-wrap" class="side-nav">
                    <span class="close-hidden-sidebar" title="' . __( 'Close', 'themestudio' ) . '"><i class="icon_close"></i></span>
                    <div class="side-nav-inner">';
                        dynamic_sidebar( 'ts-hidden-sidebar' );
            echo    '</div><!-- /.side-nav-inner -->
                </div><!-- /#ts-hidden-sidebar-wrap -->
                <div class="side-nav-overlay"></div>
                ';
        endif;
        
    }
    //add_action( 'ts_after_header_builder', 'ts_add_hidden_sidebar' );
    add_action( 'ts_after_footer', 'ts_add_hidden_sidebar' );
    
  }
  
  if ( !function_exists( 'ts_loading_page' ) ) {
    
    function ts_loading_page() {
        global $theone;
        
        $enable_page_preload = isset( $theone['opt-enable-page-preload'] ) ? $theone['opt-enable-page-preload'] == 1 : true;
        
        if ( $enable_page_preload ) {
            $xmlns = 'http://www.w3.org/2000/svg';
            echo '<div id="loader" class="pageload-overlay" data-opening="m -10,-10 0,80 100,0 0,-80 z m 50,-30.5 0,70.5 0,70 0,-70 z">
    				<svg xmlns="' . esc_url( $xmlns ) . '" width="100%" height="100%" viewBox="0 0 80 60" preserveAspectRatio="none" >
    					<path d="m -10,-10 0,80 100,0 0,-80 z M 40,-40.5 120,30 40,100 -40,30 z"/>
    				</svg>
    			</div><!-- /pageload-overlay -->';
        }
        
    }
    add_action( 'ts_after_footer', 'ts_loading_page' );
    
  }
  
  if ( !function_exists( 'ts_product_general_settings' ) ) {
    
    function ts_product_general_settings( $settings, $current_section ) {
        
    	/**
    	 * Check the current section is what we want
    	 **/
    
    	if ( $current_section == '' ) {
    	   
           $settings[] = array(
                'title' => __( 'Helmet Product Options', 'themestudio' ),
                'type'  => 'title',
                'id'    => 'helmet_products_options'
           );
           
           $settings[] = array(
                'title'     => __( 'Products Filter Via Ajax', 'themestudio' ),
                'desc'      => __( 'Enable products filtering via ajax on the shop page', 'themestudio' ),
                'id'        => 'ts_enable_products_ajax_filter',
                'default'   => 'yes',
                'type'      => 'checkbox',
                'checkboxgroup' => 'start',
                'show_if_checked' => 'option',
                'autoload'  => true
           );  
           
           $settings[] = array(
                'type'  => 'sectionend',
                'id'    => 'helmet_products_options'
           ); 
    
            return $settings;
    	
    	/**
    	 * If not, return the standard settings
    	 **/
    
    	} else {
    
    		return $settings;
    
    	}
    }
    add_filter( 'woocommerce_get_settings_products', 'ts_product_general_settings', 10, 2 ); 
  }
  
  
  if ( !function_exists( 'ts_order_by_popularity_post_clauses' ) ) {
    function ts_order_by_popularity_post_clauses( $args ) {
    	global $wpdb;
    
    	$args['orderby'] = "$wpdb->postmeta.meta_value+0 DESC, $wpdb->posts.post_date DESC";
    
    	return $args;
    }   
  }
  
  if ( !function_exists( 'ts_order_by_rating_post_clauses' ) ) {
        
    /**
     * ts_order_by_rating_post_clauses function.
     *
     * @param array $args
     * @return array
     * @since 1.0
     */
    function ts_order_by_rating_post_clauses( $args ) {
    	global $wpdb;
    
    	$args['fields'] .= ", AVG( $wpdb->commentmeta.meta_value ) as average_rating ";
    
    	$args['where'] .= " AND ( $wpdb->commentmeta.meta_key = 'rating' OR $wpdb->commentmeta.meta_key IS null ) ";
    
    	$args['join'] .= "
    		LEFT OUTER JOIN $wpdb->comments ON($wpdb->posts.ID = $wpdb->comments.comment_post_ID)
    		LEFT JOIN $wpdb->commentmeta ON($wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id)
    	";
    
    	$args['orderby'] = "average_rating DESC, $wpdb->posts.post_date DESC";
    
    	$args['groupby'] = "$wpdb->posts.ID";
    
    	return $args;
    }
  }
  
  
  if ( !function_exists( 'ts_custom_login' ) ) {
    
    /**
     * Custom login inherit from wp_login_form 
     **/
    
    /**
     * Provides a simple login form for use anywhere within WordPress. By default, it echoes
     * the HTML immediately. Pass array('echo'=>false) to return the string instead.
     *
     * @since 3.0.0
     *
     * @param array $args Configuration options to modify the form output.
     * @return string|null String when retrieving, null when displaying.
     */
    function ts_custom_login( $args = array() ) {
    	$defaults = array(
    		'echo' => true,
    		'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], // Default redirect is back to the current page
    		'form_id' => 'loginform',
    		'label_username' => __( 'Username', 'themestudio' ),
    		'label_password' => __( 'Password', 'themestudio' ),
    		'label_remember' => __( 'Remember Me', 'themestudio' ),
    		'label_log_in' => __( 'Log In', 'themestudio' ),
    		'id_username' => 'user_login',
    		'id_password' => 'user_pass',
    		'id_remember' => 'rememberme',
    		'id_submit' => 'wp-submit',
    		'remember' => true,
    		'value_username' => '',
    		'value_remember' => false, // Set this to true to default the "Remember me" checkbox to checked
            'lost_pass_link' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], // Default redirect is back to the current page
            'show_lost_pass_link' => true,
            'show_register_link' => true,
    	);
    
    	/**
    	 * Filter the default login form output arguments.
    	 *
    	 * @since 3.0.0
    	 *
    	 * @see wp_login_form()
    	 *
    	 * @param array $defaults An array of default login form arguments.
    	 */
    	$args = wp_parse_args( $args, apply_filters( 'login_form_defaults', $defaults ) );
    
    	/**
    	 * Filter content to display at the top of the login form.
    	 *
    	 * The filter evaluates just following the opening form tag element.
    	 *
    	 * @since 3.0.0
    	 *
    	 * @param string $content Content to display. Default empty.
    	 * @param array  $args    Array of login form arguments.
    	 */
    	$login_form_top = apply_filters( 'login_form_top', '', $args );
    
    	/**
    	 * Filter content to display in the middle of the login form.
    	 *
    	 * The filter evaluates just following the location where the 'login-password'
    	 * field is displayed.
    	 *
    	 * @since 3.0.0
    	 *
    	 * @param string $content Content to display. Default empty.
    	 * @param array  $args    Array of login form arguments.
    	 */
    	$login_form_middle = apply_filters( 'login_form_middle', '', $args );
    
    	/**
    	 * Filter content to display at the bottom of the login form.
    	 *
    	 * The filter evaluates just preceding the closing form tag element.
    	 *
    	 * @since 3.0.0
    	 *
    	 * @param string $content Content to display. Default empty.
    	 * @param array  $args    Array of login form arguments.
    	 */
    	$login_form_bottom = apply_filters( 'login_form_bottom', '', $args );
        
        $lost_pass_link = '';
        if ( $args['show_lost_pass_link'] === true ) {
            $lost_pass_link = '<a class="lost-pass-link" href="' . wp_lostpassword_url( get_permalink() ) . '" title="' . __( 'Lost Password', 'themestudio' ) . '">' . __( 'Lost Password', 'themestudio' ) . '</a>';
        }
        
        $register_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $register_url = esc_url( add_query_arg( array( 'action' => 'register' ), $register_url ) );
        $register_link = '<a href="' . $register_url . '" title="' . __( 'Create New Account', 'themestudio' ) . '">' . __( 'Create New Account', 'themestudio' ) . '</a>';
    
    	$form = '
    		<form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" class="ts-login-form" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
    			' . $login_form_top . '
    			<div class="login-username">
    				<label for="' . esc_attr( $args['id_username'] ) . '" class="lb-user-login">' . esc_html( $args['label_username'] ) . '</label>
    				<input type="text" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="input" value="' . esc_attr( $args['value_username'] ) . '" size="20" placeholder="' . esc_html( $args['label_username'] ) . '" />
    			</div>
    			<div class="login-password">
    				<label for="' . esc_attr( $args['id_password'] ) . '" class="lb-user-pw">' . esc_html( $args['label_password'] ) . '</label>
    				<input type="password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="input" value="" size="20" placeholder="' . esc_html( $args['label_password'] ) . '" />
    			</div>
    			' . $login_form_middle . '
    			' . ( $args['remember'] ? '<p class="login-remember"><label class="lb-remember"><input name="rememberme" type="checkbox" id="' . esc_attr( $args['id_remember'] ) . '" value="forever"' . ( $args['value_remember'] ? ' checked="checked"' : '' ) . ' /> ' . esc_html( $args['label_remember'] ) . '</label></p>' : '' ) . '
                ' . $lost_pass_link . '
    			<div class="login-submit">
    				<input type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="button-primary" value="' . esc_attr( $args['label_log_in'] ) . '" />
    				<input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />
    			</div>
                ' . ( $args['show_register_link'] && get_option('users_can_register') ? '<p class="register-link">' . $register_link . '</p>' : '' ) . '
                ' . wp_nonce_field( 'ajax-login-nonce', 'login-ajax-nonce', true, false ) . '
    			' . $login_form_bottom . '
    		</form>';
    
    	if ( $args['echo'] )
    		echo $form;
    	else
    		return $form;
    }
    
    
  }
  
  if ( !function_exists( 'ts_lost_password_page' ) ) {
    
    /**
     *  Custom lost password page 
     **/
    function ts_lost_password_page( $lostpassword_url, $redirect ) {
        if ( is_singular() ) {
            $lostpassword_url = esc_url( add_query_arg( array( 'action' => 'forgot', 'redirect_to' => $redirect ), get_permalink() ) );
        }
        else {
            $lostpassword_url = esc_url( add_query_arg( array( 'action' => 'forgot', 'redirect_to' => $redirect ), $lostpassword_url ) );
        }
        return $lostpassword_url;
    }   
    add_filter( 'lostpassword_url', 'ts_lost_password_page', 10, 2 );
  }  
  
  
  if ( !function_exists( 'ts_redirect_wp_login_page' ) ) {
    
    /**
     * Disable Wordpress default login page 
     **/
    function ts_redirect_wp_login_page() {
        
        global $pagenow, $theone;
         
        $redirect = isset( $theone['opt-redirect-from-default-login-page'] ) ? $theone['opt-redirect-from-default-login-page'] : home_url();
        $disable_default_login_page = isset( $theone['opt-enable-default-login'] ) ? $theone['opt-enable-default-login'] != 1 : false;
        $is_coming_soon_mode = isset( $theone['opt-enable-coming-soon-mode'] ) ? $theone['opt-enable-coming-soon-mode'] == '1' : false;
        
        // Don't disable default WP login if coming soon mode is on
        if ( $is_coming_soon_mode ) {
            return;
        }
        
        if( $pagenow == 'wp-login.php' && $_SERVER['REQUEST_METHOD'] == 'GET' && $disable_default_login_page ) {
            wp_redirect( $redirect );
            exit; 
        }
        
        $post_user_name = isset( $_POST['user_login'] ) ? trim( $_POST['user_login'] ) : '';
        if ( $pagenow == 'wp-login.php' && $post_user_name == '' && $disable_default_login_page && !defined( 'DOING_AJAX' ) ) {
            wp_redirect( $redirect );
            exit;   
        }
    }
    
    if ( !is_user_logged_in() ){
        add_action( 'init', 'ts_redirect_wp_login_page' ); 
    }
    
  }
  
  
  if ( !function_exists( 'ts_coming_soon_redirect' ) ) {
    function ts_coming_soon_redirect() {
        global $theone;
        
        $default_login_is_disabled = isset( $theone['opt-enable-default-login'] ) ? $theone['opt-enable-default-login'] != '1' : true;
        $is_coming_soon_mode = isset( $theone['opt-enable-coming-soon-mode'] ) ? $theone['opt-enable-coming-soon-mode'] == '1' : false;
        $disable_if_date_smaller_than_current = isset( $theone['opt-disable-coming-soon-when-date-small'] ) ? $theone['opt-disable-coming-soon-when-date-small'] == '1' : false;
        $coming_date = isset( $theone['opt-coming-soon-date'] ) ? $theone['opt-coming-soon-date'] : '';
        
        $today = date( 'm/d/Y' );
        
        if ( trim( $coming_date ) == '' || strtotime( $coming_date ) <= strtotime( $today ) ) {
            if ( $disable_if_date_smaller_than_current ) {
                $is_coming_soon_mode = false;
            }
        }
        
        // Dont't show coming soon page if is user logged in or is not coming soon mode on
        if ( is_user_logged_in() || !$is_coming_soon_mode ) {
            return;
        }
        
        ts_coming_soon_html(); // Locate in theme_coming_soon_template.php
        
        exit();
    }
    add_action( 'template_redirect', 'ts_coming_soon_redirect' );
  }
  
  if ( !function_exists( 'ts_modify_read_more_link' ) ) {
    function ts_modify_read_more_link() {
        global $theone;
        $read_more_text = isset( $theone['opt-blog-continue-reading'] ) ? sanitize_text_field( $theone['opt-blog-continue-reading'] ) : __( 'Read more', 'themestudio' );
        return '<a class="btn blue" href="' . get_permalink() . '">' . $read_more_text . '</a>';
    }
    add_filter( 'the_content_more_link', 'ts_modify_read_more_link' );
  }
    
  
  
  if ( !function_exists( 'ts_single_product_sharing' ) ) {
    function ts_single_product_sharing() {
        global $theone;
        $enable_share_post = isset( $theone['opt-enable-share-product'] ) ? $theone['opt-enable-share-product'] == 1 : false;
        $socials_shared = isset( $theone['opt-single-product-socials-share'] ) ? $theone['opt-single-product-socials-share'] : array();
        
        ?>
        
        <?php if ( $enable_share_post ): ?>

            <div class="share-product share-post">
                <h5 class="share-product-title"><?php _e( 'Share Product :', 'themestudio' ); ?></h5>
                <div class="product-socials-share-wrap">
                    <div class="icons">
                        <?php if ( in_array( 'facebook', $socials_shared ) ): ?>
                            <a class="style1" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                        <?php endif; ?>
                        <?php if ( in_array( 'gplus', $socials_shared ) ): ?>
                            <a class="style2" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <?php endif; ?>
                        <?php if ( in_array( 'twitter', $socials_shared ) ): ?>
                            <a class="style3" href="https://twitter.com/home?status=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                        <?php endif; ?>
                        <?php if ( in_array( 'pinterest', $socials_shared ) ): ?>
                            <a class="style4" href="https://pinterest.com/pin/create/button/?url=&media=&description=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
                        <?php endif; ?>
                        <?php if ( in_array( 'linkedin', $socials_shared ) ): ?>
                            <a class="style6" href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source=<?php the_permalink(); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <?php endif; ?>
                    </div>
                </div><!-- /.product-socials-share-wrap -->
            </div>
        
        <?php endif; ?>
        
        <?php
        
    }
  }
  
  
  if ( !function_exists( 'ts_wc_cart_totals_shipping_html' ) ) {
    /**
     * Get shipping methods
     *
     * @access public
     * @return void
     */
    function ts_wc_cart_totals_shipping_html() {
    	$packages = WC()->shipping->get_packages();
    
    	foreach ( $packages as $i => $package ) {
    		$chosen_method = isset( WC()->session->chosen_shipping_methods[ $i ] ) ? WC()->session->chosen_shipping_methods[ $i ] : '';
    
    		wc_get_template( 'cart/ts-cart-shipping.php', array( 'package' => $package, 'available_methods' => $package['rates'], 'show_package_details' => ( sizeof( $packages ) > 1 ), 'index' => $i, 'chosen_method' => $chosen_method ) );
    	}
    }
  }
  
  if ( !function_exists( 'ts_loop_columns' ) ) {
    /**
     * Change WooCommerce default comlumns number
     **/
    function ts_loop_columns() {
        global $theone;
        $layout = isset( $theone['woo_layout'] ) ? $theone['woo_layout'] : 'left-sidebar';
        if ( $layout == 'fullwidth' ) {
            return 4;
        }
        return 3;
    }
    add_filter( 'loop_shop_columns', 'ts_loop_columns' );
  }
  
  
  if ( !function_exists( 'ts_related_products_args' ) ) {
    /**
     *  Number of related products display 
     **/
    function ts_related_products_args( $args ) {
        global $theone;
        
        $woo_layout = isset( $theone['woo_layout'] ) ? $theone['woo_layout'] : 'left-sidebar'; 
        $number_of_related_products = isset( $theone['woo_number_of_related_products_sidebar'] ) ? intval( $theone['woo_number_of_related_products_sidebar'] ) : 3; // Default is 3
        if ( $woo_layout == 'fullwidth' ) {
            $number_of_related_products = isset( $theone['woo_number_of_related_products_no_sidebar'] ) ? intval( $theone['woo_number_of_related_products_no_sidebar'] ) : 4; // Default is 4 for no sidebar
        }
        
    	$args['posts_per_page'] = $number_of_related_products;
    	return $args;
    }
    add_filter( 'woocommerce_output_related_products_args', 'ts_related_products_args' ); 
  }
    
  
  //add_action( 'woocommerce_after_add_to_cart_button', 'themestudio' );
  
  
  add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
  add_action( 'woocommerce_share', 'ts_single_product_sharing', 10 ); 
  
  // Remove WooCommerce default breadcrumb
  remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
  remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
  
  

?>