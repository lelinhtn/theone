<?php
	/**
	 * ThemeStudio Framework functions and definitions.
	 *
	 * @package WordPress
	 * @subpackage ThemeStudio.Net
	 * @since ThemeStudio Framework 1.0
	*/
	
	if ( ! isset( $content_width ) ) {
		$content_width = 960;
	}

	/**
	 * Sets up theme defaults and registers the various WordPress features that
	 * ThemeStudio Framework supports.
	 *
	 * @uses load_theme_textdomain() For translation/localization support.
	 * @uses add_editor_style() To add a Visual Editor stylesheet.
	 * @uses add_theme_support() To add support for post thumbnails, automatic feed links,
	 * 	custom background, and post formats.
	 * @uses register_nav_menu() To add support for navigation menus.
	 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
	 *
	 * @since ThemeStudio Framework 1.0
	*/

	if( !function_exists( 'tstheme_setup' ) ) {

		/*
		 * setup text domain and style
		*/
		function tstheme_setup() {
			load_theme_textdomain( 'theone', get_template_directory() . '/languages' );
			add_editor_style();
			add_theme_support( 'automatic-feed-links' );
			add_theme_support( 'post-formats', array(  'image', 'gallery','video','audio' ) );
			add_theme_support( 'woocommerce' );
				
			if ( isset($theone) ) {
				global $theone;
				$args = array(
					'default-image' => $theone['menu_background_color']['background-image'],
				);

				add_theme_support( 'custom-header', $args );
				add_theme_support( 'custom-background', $args );

			}

			//add image sizes
			if(function_exists('add_image_size')) {
				add_image_size('full-size',  9999, 9999, false);
				add_image_size('small-thumb',  90, 90, true);
			}
		}
		add_action( 'after_setup_theme', 'tstheme_setup' );

	}

	add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );

	if( !function_exists( 'ts_add_thumbnail_size' ) ) {

		/*
		 * Add thumb size image when upload
		*/
		function ts_add_thumbnail_size($thumb_size){	
			foreach ($thumb_size['imgSize'] as $sizeName => $size)
			{
				if($sizeName == 'base')
				{
					set_post_thumbnail_size($thumb_size['imgSize'][$sizeName]['width'], $thumb_size[$sizeName]['height'], true);
				} else {	
					add_image_size(	 
						$sizeName,
						$thumb_size['imgSize'][$sizeName]['width'], 
						$thumb_size['imgSize'][$sizeName]['height'], 
						true
					);
				}
			}
		}

		
		
        $thumb_size['imgSize']['555x393'] = array( 'width' => 555, 'height' => 393 ); // Services style: 12, 1, 14, 16, 19
        $thumb_size['imgSize']['405x402'] = array( 'width' => 405, 'height' => 402 ); // Services style: 17, 20, 33, 25
        $thumb_size['imgSize']['410x407'] = array( 'width' => 410, 'height' => 407 ); // Services style: 11, 24
        $thumb_size['imgSize']['160x160'] = array( 'width' => 160, 'height' => 160 ); // Services style: 13
        $thumb_size['imgSize']['444x444'] = array( 'width' => 444, 'height' => 444 ); // Services style: 21, 26, 9, 3, 2, 10, 34
        
        
        //$thumb_size['imgSize']['555x393'] = array( 'width' => 555, 'height' => 393 ); // Services Plumber
        $thumb_size['imgSize']['540x536'] = array( 'width' => 540, 'height' => 536 ); // Services Construction
        $thumb_size['imgSize']['740x524'] = array( 'width' => 740, 'height' => 524 ); // Services
        $thumb_size['imgSize']['536x426'] = array( 'width' => 536, 'height' => 426 ); // Lastest News
        $thumb_size['imgSize']['184x106'] = array( 'width' => 184, 'height' => 106 ); // Client Logos
        $thumb_size['imgSize']['536x588'] = array( 'width' => 536, 'height' => 588 ); // Members Grid (268x294 * 2)
        $thumb_size['imgSize']['150x150'] = array( 'width' => 150, 'height' => 150 ); // Widget Post News (Thumbnail size)
        $thumb_size['imgSize']['540x430'] = array( 'width' => 540, 'height' => 430 ); // Loop grid
        $thumb_size['imgSize']['868x455'] = array( 'width' => 868, 'height' => 455 ); // Loop list (not use)
        $thumb_size['imgSize']['1170x613'] = array( 'width' => 1170, 'height' => 613 ); // Loop list (no sidebar)

		ts_add_thumbnail_size($thumb_size);

	}


	if( !function_exists( 'tstheme_scripts_styles' ) ) {
		
		/**
		 * Enqueues scripts and styles for front-end.
		 *
		 * @since ThemeStudio Framework 1.0
		 */
		function tstheme_scripts_styles() {
			global $wp_styles;

			/*
			 * Adds JavaScript to pages with the comment form to support
			 * sites with threaded comments (when in use).
			 */
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
				wp_enqueue_script( 'comment-reply' );

			// IE style

			wp_enqueue_style( 'tstheme-ie', get_template_directory_uri() . '/css/ie.css', array( 'tstheme-style' ), '30042013' );
			$wp_styles->add_data( 'tstheme-ie', 'conditional', 'lt IE 9' );
		}
		add_action( 'wp_enqueue_scripts', 'tstheme_scripts_styles' );

	}

    
    if ( !function_exists( 'ts_register_sidebar' ) ) {
        
        function ts_register_sidebar() {
            
            $sidebar = 'Primary Sidebar';
            
            /*
    		 * Register sidebar
    		*/
    		register_sidebar(
    			array(
                    'name'          => str_replace("_"," ",$sidebar),
                    'id'            => 'primary',
                    'description'   => esc_html__( 'This is land of page sidebar','themestudio' ),		
                    'before_title'  => '<h1 class="title">',
                    'after_title'   => '</h1>',			
                    'before_widget' => '<div  id="%1$s" class="widget %2$s">',
                    'after_widget'  => '</div>',			  
    			)
    		);   
        }
        add_action( 'widgets_init', 'ts_register_sidebar' );
    }


	// Define constant
	$get_theme = wp_get_theme();
 
	define('THE_ONE_THEME_NAME', $get_theme);
	define('THE_ONE_THEME_VERSION', '1.0.0');
	define('THE_ONE_THEME_SLUG', 'theone');
	define('THE_ONE_BASE_URL', get_template_directory_uri());
	define('THE_ONE_BASE', get_template_directory());
	define('THE_ONE_LIBS', THE_ONE_BASE . '/libs/');
    define('THE_ONE_LIBS_URL', THE_ONE_BASE_URL . '/libs');
    define('THE_ONE_CORE', THE_ONE_BASE . '/core/');
	define('THE_ONE_LOOP', THE_ONE_BASE . '/loop/');
	define('THE_ONE_WIDGET', THE_ONE_BASE . '/core/widgets/');
	define('THE_ONE_SHORTCODE', THE_ONE_BASE . '/core/shortcodes/');
	define('THE_ONE_FUNCTIONS', THE_ONE_BASE . '/core/');
	define('THE_ONE_METAS', THE_ONE_BASE . '/core/functions/');
	define('THE_ONE_OPTION', THE_ONE_BASE . '/core/admin/');
	define('THE_ONE_API', THE_ONE_FUNCTIONS . '/apis/');
	define('THE_ONE_JS', THE_ONE_BASE_URL . '/assets/js/');
	define('THE_ONE_CSS', THE_ONE_BASE_URL . '/assets/css/');
	define('THE_ONE_IMAGES', THE_ONE_BASE_URL . '/assets/images/');
	define('THE_ONE_IMG', THE_ONE_BASE_URL . '/assets/img/');
	define('THE_ONE_TEMPLATE', THE_ONE_BASE_URL . '/templates/');
?>