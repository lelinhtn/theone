<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
} 

/**
 * Widget Name: The One New Posts
 * Widget Description: Display the lastest posts of selected category
 * Widget Function Name: _new_posts
 * Widget Text Domain: themestudio
 * 
 * @package OViC Theme 1.0
 * @author OViC Team
 */
 
class ts_widget_new_posts extends WP_Widget { 
     
    function __construct() {
        $widget_ops = array( 
            'classname'     =>  '', 
            'description'   =>  __( 'Display the lastest posts of selected category', 'themestudio' ) 
        );
        
        $control_ops = array( 'width' => 400, 'height' => 0); 
        parent::__construct( 
            'ts_widget_new_posts', 
            __('The One New Posts', 'themestudio'), 
            $widget_ops, $control_ops
        );
        
    }
    
    
    public function widget( $args, $instance ) {
        global $post; 
        
        $title = apply_filters( 'widget_title', $instance['title'] ); 
        $cat_id = isset( $instance['cat_id'] ) ? intval( $instance['cat_id'] ) : 0;
        $limit = isset( $instance['limit'] ) ? intval( $instance['limit'] ) : 2;
        
        // before and after widget arguments are defined by themes 
        echo $args['before_widget'];   
        
        $query_args = array(
            'showposts'     =>  $limit,
            'post_status'   =>  array( 'publish' )
        );
        
        if ( $cat_id > 0 ) {
            $query_args['cat'] = $cat_id;
        }
        
        $query_posts = new WP_Query( $query_args );
        ?>
        
        <?php if ( trim( $title ) != '' ): ?>
            <h5 class="widget-title"><?php echo $title; ?></h5>
        <?php endif; ?>
        
        <?php if ( $query_posts->have_posts() ): ?>
            <ul class="news">
            <?php while ( $query_posts->have_posts() ): $query_posts->the_post(); ?>
                
                <?php
                    
                    $thumb_src = '';
                    if ( has_post_thumbnail() ) {
                        $thumb_src = function_exists( 'theone_get_img_src_by_id' ) ? theone_get_img_src_by_id( get_post_thumbnail_id(), '150x150' ) : '';
                    }
                    else{
                        $thumb_src = function_exists( 'theone_no_image' ) ? theone_no_image( array( 'width' => 150, 'height' => 150 ), false, false ) : '';
                    }
                    
                    $archive_year  = get_the_time('Y'); 
                    $archive_month = get_the_time('m'); 
                    $archive_day   = get_the_time('d');
                    
                ?>
                
                <li>
                    <?php if ( $thumb_src != '' ): ?>
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo esc_url( $thumb_src ); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" /></a>
                    <?php endif; ?>
                    <h6><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
                    <span class="news-date"><?php _e( 'On : ', 'themestudio' ); ?><a href="<?php echo get_day_link( $archive_year, $archive_month, $archive_day ); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a></span>
                </li>
                
            <?php endwhile; ?>
            </ul>
        <?php endif; ?>
        <?php    
        
        wp_reset_postdata();
        
        echo $args['after_widget']; 
    }
    
    
    
    public function form( $instance ) {
        if ( isset( $instance['title'] )) { 
            $title = $instance['title'];  
        }
        else { 
            $title = __('New Posts', 'themestudio'); 
        }
        
        $cat_id = isset( $instance['cat_id'] ) ? intval( $instance['cat_id'] ) : 0;
        $limit = isset( $instance['limit'] ) ? intval( $instance['limit'] ) : 2;
        
        
        // Widget admin form
        ?> 
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'themestudio' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"  />
        </p>
        
        <?php if ( function_exists( 'theone_custom_tax_select' ) ): ?>
            <p>
                <label for="<?php echo $this->get_field_id( 'cat_id' ); ?>"><?php _e( 'Select category', 'themestudio' ); ?></label>
                <?php 
                    echo theone_custom_tax_select( 
                        array( $cat_id ), 
                        array( 
                            'tax'   => 'category',
                            'class' => 'ts-cat-select widefat', 
                            'id'    => $this->get_field_id( 'cat_id' ),
                            'name'  => $this->get_field_name( 'cat_id' ),
                            'first_option' => true,
                            'first_option_val'  =>  '0',
                            'first_option_text' =>  __( ' --- All Categories --- ', 'themestudio' )
                        ) 
                    );
                ?>
            </p>
        <?php else: ?>
            <?php _e( 'Please install and active plugin The One Core', 'themestudio' ); ?>
        <?php endif; ?>
        
        <p>
            <label for="<?php echo $this->get_field_id( 'limit' ); ?>"><?php _e( 'Limit', 'themestudio' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="text" value="<?php echo esc_attr( $limit ); ?>"  />
        </p>
        
        <?php 
    }
    
    
    
    public function update( $new_instance, $old_instance ) {
        $instance = array(); 
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['cat_id'] = ( ! empty( $new_instance['cat_id'] ) ) ? intval( $new_instance['cat_id'] ) : 0;
        $instance['limit'] = ( ! empty( $new_instance['limit'] ) ) ? intval( $new_instance['limit'] ) : 2;
        return $instance;
    }
    
} // End class ts_widget_lastest_news 

function ts_load_widget_new_posts() {
    register_widget( 'ts_widget_new_posts' );
}
add_action( 'widgets_init', 'ts_load_widget_new_posts' );