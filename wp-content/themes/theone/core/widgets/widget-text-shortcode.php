<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
} 

/**
 * Widget Name: The One Text - Shortcode
 * Widget Description: Display text or do shortcode
 * Widget Function Name: _text_shortcode
 * Widget Text Domain: themestudio
 * 
 * @package OViC Theme 1.0
 * @author OViC Team
 */
 
class ts_widget_text_shortcode extends WP_Widget { 
     
    function __construct() {
        $widget_ops = array( 
            'classname'     =>  '', 
            'description'   =>  __( 'Display text or do shortcode', 'themestudio' ) 
        );
        
        $control_ops = array( 'width' => 400, 'height' => 0); 
        parent::__construct( 
            'ts_widget_text_shortcode', 
            __('The One Text - Shortcode', 'themestudio'), 
            $widget_ops, $control_ops
        );
        
    }
    
    
    public function widget( $args, $instance ) {
        global $post; 
        
        $title = apply_filters( 'widget_title', $instance['title'] ); 
        $text = isset( $instance['text'] ) ? strip_tags( $instance['text'] ) : '';
        
        // before and after widget arguments are defined by themes 
        echo $args['before_widget'];   
        
        ?>
        <?php if ( trim( $title ) != '' ): ?>
            <h5 class="widget-title"><?php echo $title; ?></h5>
        <?php endif; ?>
        
        <div class="ts-text-shortcode-content">
            <?php echo do_shortcode( $text ); ?>
        </div>
        
        <?php
        
        echo $args['after_widget']; 
    }
    
    
    
    public function form( $instance ) {
        if ( isset( $instance['title'] )) { 
            $title = $instance['title'];  
        }
        else { 
            $title = __('New Posts', 'themestudio'); 
        }
        
        $text = isset( $instance['text'] ) ? strip_tags( $instance['text'] ) : '';
        
        
        // Widget admin form
        ?> 
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'themestudio' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"  />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Text or shortcode', 'themestudio' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>"  />
        </p>
        
        <?php 
    }
    
    
    
    public function update( $new_instance, $old_instance ) {
        $instance = array(); 
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
        return $instance;
    }
    
} // End class ts_widget_lastest_news 

function ts_load_widget_text_shortcode() {
    register_widget( 'ts_widget_text_shortcode' );
}
add_action( 'widgets_init', 'ts_load_widget_text_shortcode' );