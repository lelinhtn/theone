<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
} 

/**
 * Widget Name: The One Latest Tweets
 * Widget Description: Display Lastest Tweets
 * Widget Function Name: _lastest_tweets
 * Widget Text Domain: themestudio
 * 
 * @package OViC Theme 1.0
 * @author OViC Team
 */
 
class ts_widget_lastest_tweets extends WP_Widget { 
     
    function __construct() {
        $widget_ops = array( 
            'classname'     =>  '', 
            'description'   =>  __('Display Lastest Tweets', 'themestudio') 
        );
        
        $control_ops = array( 'width' => 400, 'height' => 0); 
        parent::__construct( 
            'ts_widget_lastest_tweets', 
            __('The One Latest Tweets', 'themestudio'), 
            $widget_ops, $control_ops
        );
        
    }
    
    
    public function widget( $args, $instance ) {
        global $post; 
        
        $title = apply_filters( 'widget_title', $instance['title'] ); 
        
        $tweets_html = '';
        
        if ( function_exists( 'getTweets' ) ) {
            $tweets = getTweets( false, 2 );
            
            if ( is_array( $tweets ) ) {
                
                $tweets_li_html = '';
                foreach ( $tweets as $tweet ):
                
                    if ( $tweet['text'] ) {
                        $tweets_li_html .= '<li>';
                        
                        $created_time = '';
                        if ( isset( $tweet['created_at'] ) ) {
                            $created_time = human_time_diff( strtotime($tweet['created_at']), current_time('timestamp') );   
                        }
                        
                        $tweets_li_html .= '<a href="http://www.twitter.com/' . get_option( 'tdf_user_timeline' ) . '/status/' . $tweet['id_str'] . '" target="_blank"><i class="fa fa-twitter"></i> @' . get_option( 'tdf_user_timeline' ) . '</a> ';
                        $tweets_li_html .= $tweet['text'];
                        $tweets_li_html .= '<span class="twitter-time">' . sprintf( __( '%s ago', 'themestudio' ), $created_time ) . '</span>';
                        $tweets_li_html .= '</li>';
                        
                    }
                
                endforeach;
                
                $tweets_html .= '<ul class="twitter-list">';
                $tweets_html .= $tweets_li_html;
                $tweets_html .= '</ul><!-- /.twitter-list -->';
                
            }
            
        }
        
        // before and after widget arguments are defined by themes 
        echo $args['before_widget'];   
        ?>
        
        <h5><?php echo $title; ?></h5>
        <?php echo $tweets_html; ?>
        
        <?php
        echo $args['after_widget']; 
    }
    
    
    
    public function form( $instance ) {
        if ( isset( $instance['title'] )) { 
            $title = $instance['title'];  
        }
        else { 
            $title = __('The One Latest Tweets', 'themestudio'); 
        }
        // Widget admin form
        ?> 
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'themestudio' ); ?>: </label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>"  />
        </p>
        <?php 
    }
    
    
    
    public function update( $new_instance, $old_instance ) {
        $instance = array(); 
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
    
} // End class ts_widget_lastest_news 

function ts_load_widget_lastest_tweets() {
    register_widget( 'ts_widget_lastest_tweets' );
}
add_action( 'widgets_init', 'ts_load_widget_lastest_tweets' );