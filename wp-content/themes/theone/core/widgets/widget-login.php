<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    exit;
} 

/**
 * Widget Name: The One Login
 * Widget Description: Display the lastest posts of selected category
 * Widget Function Name: _login
 * Widget Text Domain: themestudio
 * 
 * @package OViC Theme 1.0
 * @author OViC Team
 */
 
class ts_widget_login extends WP_Widget { 
     
    function __construct() {
        $widget_ops = array( 
            'classname'     =>  '', 
            'description'   =>  __( 'Display the lastest posts of selected category', 'themestudio' ) 
        );
        
        $control_ops = array( 'width' => 400, 'height' => 0); 
        parent::__construct( 
            'ts_widget_login', 
            __('The One Login', 'themestudio'), 
            $widget_ops, $control_ops
        );
        
    }
    
    
    public function widget( $args, $instance ) {
        
        // before and after widget arguments are defined by themes 
        echo $args['before_widget'];   
        
        get_template_part( 'content-parts/content-page', 'login' );
        
        echo $args['after_widget']; 
    }
    
    
    
    public function form( $instance ) {
        // Nothing to do
    }
    
    
    
    public function update( $new_instance, $old_instance ) {
        // Nothing to do
    }
    
} // End class ts_widget_lastest_news 

function ts_load_widget_login() {
    register_widget( 'ts_widget_login' );
}
add_action( 'widgets_init', 'ts_load_widget_login' );