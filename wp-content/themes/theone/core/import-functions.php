<?php
/*
 * @package     WBC_Importer - Extension for Importing demo content
 * @author      Webcreations907
 * @version     1.0
 */


/************************************************************************
* Importer will auto load, there is no settings required to put in your
* Reduxframework config file.
*
* BUT- If you want to put the demo importer in a different position on 
* the panel, use the below within your config for Redux.
*************************************************************************/
// $this->sections[] = array(
            //     'id' => 'wbc_importer_section',
            //     'title'  => esc_html__( 'Demo Content', 'themestudio' ),
            //     'desc'   => esc_html__( 'Description Goes Here', 'themestudio' ),
            //     'icon'   => 'el-icon-website',
            //     'fields' => array(
            //                     array(
            //                         'id'   => 'wbc_demo_importer',
            //                         'type' => 'wbc_importer'
            //                         )
            //                 )
            //     );

/************************************************************************
* Example functions/filters
*************************************************************************/
if ( !function_exists( 'wbc_after_content_import' ) ) {

	/**
	 * Function/action ran after import of content.xml file
	 *
	 * @param (array) $demo_active_import       Example below
	 * [wbc-import-1] => Array
	 *      (
	 *            [directory] => current demo data folder name
	 *            [content_file] => content.xml
	 *            [image] => screen-image.png
	 *            [theme_options] => theme-options.txt
	 *            [widgets] => widgets.json
	 *            [imported] => imported
	 *        )
	 * @param (string) $demo_data_directory_path path to current demo folder being imported.
	 *
	 */

	function wbc_after_content_import( $demo_active_import , $demo_data_directory_path ) {
		//Do something
	}

	// Uncomment the below
	// add_action( 'wbc_importer_after_content_import', 'wbc_after_content_import', 10, 2 );
}

if ( !function_exists( 'wbc_filter_title' ) ) {

	/**
	 * Filter for changing demo title in options panel so it's not folder name.
	 *
	 * @param [string] $title name of demo data folder
	 *
	 * @return [string] return title for demo name.
	 */

	function wbc_filter_title( $title ) {
	       
        $title_arg = explode( '-', $title );
        if ( isset( $title_arg[1] ) ) {
            $title = $title_arg[1];
        }
        return trim( ucwords( preg_replace('/(_)+/', ' ', $title) ) );
	}

	// Uncomment the below
	add_filter( 'wbc_importer_directory_title', 'wbc_filter_title', 10 );
}

if ( !function_exists( 'wbc_importer_description_text' ) ) {

	/**
	 * Filter for changing importer description info in options panel
	 * when not setting in Redux config file.
	 *
	 * @param [string] $title description above demos
	 *
	 * @return [string] return.
	 */

	function wbc_importer_description_text( $description ) {

		$message = '<p>'. esc_html__( 'Works best to import on a new install of WordPress. Images are for demo purpose only.', 'themestudio' ) .'</p>';

		return $message;
	}

	// Uncomment the below
	add_filter( 'wbc_importer_description', 'wbc_importer_description_text', 10 );
}

if ( !function_exists( 'wbc_importer_label_text' ) ) {

	/**
	 * Filter for changing importer label/tab for redux section in options panel
	 * when not setting in Redux config file.
	 *
	 * @param [string] $title label above demos
	 *
	 * @return [string] return no html
	 */

	function wbc_importer_label_text( $label_text ) {

		$label_text = __( 'The One Importer', 'themestudio' );

		return $label_text;
	}

	// Uncomment the below
	add_filter( 'wbc_importer_label', 'wbc_importer_label_text', 10 );
}

if ( !function_exists( 'wbc_change_demo_directory_path' ) ) {

	/**
	 * Change the path to the directory that contains demo data folders.
	 *
	 * @param [string] $demo_directory_path
	 *
	 * @return [string]
	 */

	function wbc_change_demo_directory_path( $demo_directory_path ) {

		$demo_directory_path = get_template_directory() . '/libs/demo-data/';

		return $demo_directory_path;

	}

	// Uncomment the below
	add_filter('wbc_importer_dir_path', 'wbc_change_demo_directory_path' );
}

if ( !function_exists( 'wbc_importer_before_widget' ) ) {

	/**
	 * Function/action ran before widgets get imported
	 *
	 * @param (array) $demo_active_import       Example below
	 * [wbc-import-1] => Array
	 *      (
	 *            [directory] => current demo data folder name
	 *            [content_file] => content.xml
	 *            [image] => screen-image.png
	 *            [theme_options] => theme-options.txt
	 *            [widgets] => widgets.json
	 *            [imported] => imported
	 *        )
	 * @param (string) $demo_data_directory_path path to current demo folder being imported.
	 *
	 * @return nothing
	 */

	function wbc_importer_before_widget( $demo_active_import , $demo_data_directory_path ) {

		//Do Something
        update_option( 'tdf_consumer_key', '83FGxUqbnH8mvFu50HuWxA' );
        update_option( 'tdf_consumer_secret', '2HXI9MBanKddMe3Lg8FPEtwsbO3imQakIcZIBpt7hv4' );
        update_option( 'tdf_access_token', '101961223-ZxYVkbSCZhVDktPkJXbEwgn3PsdHLvzCc95IaL7D' );
        update_option( 'tdf_access_token_secret', 'mIWqEBMaCqDABJWsF8gbatcbFi2UMkTQJ555uC6iiRvke' );
        update_option( 'tdf_cache_expire', '3600' );
        update_option( 'tdf_user_timeline', 'Envato' );

	}

	// Uncomment the below
	add_action('wbc_importer_before_widget_import', 'wbc_importer_before_widget', 10, 2 );
}

if ( !function_exists( 'wbc_after_theme_options' ) ) {

	/**
	 * Function/action ran after theme options set
	 *
	 * @param (array) $demo_active_import       Example below
	 * [wbc-import-1] => Array
	 *      (
	 *            [directory] => current demo data folder name
	 *            [content_file] => content.xml
	 *            [image] => screen-image.png
	 *            [theme_options] => theme-options.txt
	 *            [widgets] => widgets.json
	 *            [imported] => imported
	 *        )
	 * @param (string) $demo_data_directory_path path to current demo folder being imported.
	 *
	 * @return nothing
	 */

	function wbc_after_theme_options( $demo_active_import , $demo_data_directory_path ) {

		//Do Something

	}

	// Uncomment the below
	// add_action('wbc_importer_after_theme_options_import', 'wbc_after_theme_options', 10, 2 );
}


/************************************************************************
* Extended Example:
* Way to set menu, import revolution slider, and set home page.
*************************************************************************/

if ( !function_exists( 'ts_extended_import' ) ) {
	function ts_extended_import( $demo_active_import , $demo_directory_path ) {

		reset( $demo_active_import );
		$current_key = key( $demo_active_import );		

		/************************************************************************
		* Setting Menus
		*************************************************************************/
        
        $wbc_menu_array = array(
            'a-construction',
            'b-plumber',  
            'c-mechanic',
            'd-cleaning',
            'e-autoshop',
            'f-carpenter',
            'g-maintenance',
            'h-metal_construction',
            'i-electrician',
            'j-mining',
            'k-renovation',
            'l-movers',
            'm-gardner',
            'n-fuel_industry',
            'o-logistics',
        );

		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && in_array( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
			$main_menu = get_term_by( 'name', 'Main menu', 'nav_menu' );

			if ( isset( $main_menu->term_id ) ) {
				set_theme_mod( 'nav_menu_locations', array(
						'main_menu_1' => $main_menu->term_id,						
					)
				);
			}

		}
        
        
        /************************************************************************
		* Import slider(s) for the current demo being imported
		*************************************************************************/
		if ( class_exists( 'RevSlider' ) ) {
            
            $wbc_sliders_array = array(
				'b-plumber' => array(
                    'home-plumber-slider.zip',
                ), //Set slider zip name
                //'demo2' => array(
//                    'home-business-slide1.zip',
//                    'home-business-slide2.zip',
//                    'home-business-slide3.zip'
//                )
			);
            
			if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_sliders_array ) ) {
			     //$wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];
                $wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];
                if ( is_array( $wbc_slider_import ) ) {
                    if ( !empty( $wbc_slider_import ) ) {
                        foreach ( $wbc_slider_import as $wbc_slider_import_name ) {
                            if ( file_exists( $demo_directory_path.$wbc_slider_import_name ) ) {
            					$slider = new RevSlider();
            					$slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import_name );
            				}
                        }
                    }
                }
                else{
                    if ( file_exists( $demo_directory_path.$wbc_slider_import ) ) {
    					$slider = new RevSlider();
    					$slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import );
    				}    
                }
                
			}
		}
        

		/************************************************************************
		* Set HomePage
		*************************************************************************/
        
        // array of demos/homepages to check/select from
        $wbc_home_pages = array(
            'a-construction' => 'Home 1',
            'b-plumber' => 'Home 1',  
            'c-mechanic' => 'Home 1',
            'd-cleaning' => 'Home 1',
            'e-autoshop' => 'Home 1',
            'f-carpenter' => 'Home 1',
            'g-maintenance' => 'Home 1',
            'h-metal_construction' => 'Home 1',
            'i-electrician' => 'Home 1',
            'j-mining' => 'Home 1',
            'k-renovation' => 'Home 1',
            'l-movers' => 'Home 1',
            'm-gardner' => 'Home 1',
            'n-fuel_industry' => 'Home 1',
            'o-logistics' => 'Home 1',
        );
        	
		if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
			$page 		= get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );			
			if ( isset( $page->ID ) ) {
				update_option( 'page_on_front', $page->ID );				
				update_option( 'show_on_front', 'page' );
			}
		}

	}
    
	add_action( 'wbc_importer_after_content_import', 'ts_extended_import', 10, 2 );
}

?>