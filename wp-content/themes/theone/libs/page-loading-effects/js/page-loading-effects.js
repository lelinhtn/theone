(function() {
    
    var pageWrap = jQuery('.site-main');
    
    if ( jQuery( '#loader' ).length ) {
        var loader = new SVGLoader( document.getElementById( 'loader' ), { speedIn : 500 } );
        
        loader.show();
        
        jQuery(window).load(function(){
            setTimeout( function() {
        		loader.hide();
        	}, 2000 );
            
            // Make sure the loader will be removed after window loaded
            setTimeout( function(){
                jQuery('#loader').removeClass('pageload-loading');
            }, 5000 );
        });
        
        window.onbeforeunload = function(){
            loader.show();  
        };
    }
    
    setTimeout( function() {
        pageWrap.css({
            'padding-top': ''
        });
	}, 2000 );
	
})();