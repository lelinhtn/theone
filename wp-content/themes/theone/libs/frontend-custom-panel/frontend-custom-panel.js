jQuery(document).ready(function($){
    
    "use strict";
    
    // Frontend custom panel
    $(document).on('click', '.ts-frontend-panel-tools-wrap .ts-handler', function(e){
        
        var $this = $(this);
        var thisPanel = $this.closest('.ts-frontend-panel-tools-wrap');
        thisPanel.toggleClass('panel-show');
        
        e.preventDefault();
         
    });
    
    $(document).on('click', '.ts-frontend-panel-tools-wrap .site-layout-select', function(e){
        
        if ( !$(this).hasClass('current-site-layout') ) {
            $('.ts-frontend-panel-tools-wrap .site-layout-select').removeClass('current-site-layout');  
            $(this).addClass('current-site-layout'); 
            ts_update_layout_preview_style();
        }
        
        e.preventDefault();
         
    });
    
    $(document).on('change', '.ts-frontend-panel-tools-wrap input, .ts-frontend-panel-tools-wrap select', function(){
        ts_update_layout_preview_style();
    });
    
    $(document).on('click', '.bg-img-preview-wrap .bg-img-preview', function(e){
        if ( !$(this).hasClass('current-bg') ) {
            $('.bg-img-preview-wrap .bg-img-preview').removeClass('current-bg');
            $(this).addClass('current-bg');
            ts_update_layout_preview_style();
        }
        e.preventDefault(); 
    });
    
    // Change base color
    $('.ts-frontend-panel-tools-wrap .ts-color-picker').each(function(){
        var $this = $(this);
        $this.spectrum({
            showPalette: true,
            showSelectionPalette: true, // true by default
            //selectionPalette: ["red", "green", "blue"],
            palette: [
                ['#ffc40e', '#03529f', '#3e50b4', '#fe5621', '#d22e2e', '#bf8b5b', '#0474df'], // Construction, Plumber, Mechanic, Cleaning, Autoshop, Carpenter, Maintenance
                ['#fe9700', '#c0392b', '#f39c12', '#2b54ae'], // Metal_Construction, Electrician, Mining, Renovation 
            ],
            change: function(color) {
                if ( $this.hasClass('base-color') ) {
                    ts_update_color_scheme_preview_frontend(color.toHexString());   
                }
                if ( $this.hasClass('bg-color-preview') ) {
                    ts_update_box_bg_color_preview_frontend(color.toHexString());
                }
            },
            move: function(color) {
                if ( $this.hasClass('base-color') ) {
                    ts_update_color_scheme_preview_frontend(color.toHexString());   
                }
                if ( $this.hasClass('bg-color-preview') ) {
                    ts_update_box_bg_color_preview_frontend(color.toHexString());
                }
            }
        }); 
    });
    
    
    function ts_update_color_scheme_preview_frontend( base_color ) {
        var css_url_old = theme_global_localize['custom_style_via_ajax_url'];
        var css_url_new = ts_insert_param( css_url_old, 'base_color', base_color );
        
        $('#theone-customizer-style-css').attr('href', css_url_new);
        
    }
    
    function ts_update_box_bg_color_preview_frontend( bg_color ) {
        if ( $('body').hasClass('body-box') ) {
            $('body .site-main').css({
                'background-color': bg_color
            });
        } 
        
    }
    
    function ts_update_layout_preview_style() {
        
        var site_layout = $('.ts-frontend-panel-tools-wrap .current-site-layout').attr('data-layout');
        var current_body_bg_img = $('.ts-frontend-panel-tools-wrap .bg-img-preview-wrap .current-bg').attr('data-img-src');
        var current_box_bg_color = $('.ts-frontend-panel-tools-wrap .bg-color-preview-wrap .bg-color-preview').val();
        
        $('body').removeClass('body-fullwidth body-box').addClass('body-' + site_layout);
        if ( $('body').hasClass('body-box') ) {
            $('body').css({
                'background-image': 'url(' + current_body_bg_img + ')'
            });
            $('body .site-main').css({
                'background-color': current_box_bg_color
            });
        }
        else{
            $('body').css({
                'background-image': ''
            });
            $('body .site-main').css({
                'background-color': ''
            });
        }
        
    }
    
    
    // Insert param to URL
    function ts_insert_param(uri, key, value) {
        key = encodeURIComponent(key); value = encodeURIComponent(value);
    
        var kvp = uri.split('&');
    
        var i=kvp.length; var x; while(i--) 
        {
            x = kvp[i].split('=');
    
            if (x[0]==key)
            {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }
    
        if(i<0) {kvp[kvp.length] = [key,value].join('=');}
        
        // Return URL with a param inserted
        return kvp.join('&'); 
    }
      
});