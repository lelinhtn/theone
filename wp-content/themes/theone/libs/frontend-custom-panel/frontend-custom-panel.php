<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Step to add demo site for customize frontend:
 *      
 *      1. Add demo name: $demo_names.
 *      2. Add demo link item: $demo_links. Key must be in $demo_names. Value is demo link
 *      3. Upload image preview for the demo. Image name should be: {$demo_name}_preview.png and stored in "libs/frontend-custom-panel/images/sites/". Image sie = 440x430px.
 **/



/**
 * Frontend custom style panel form
 * @since 1.0 
 **/
function ts_frontend_custom_style_panel() {
    global $theone;
    $sidebar_side = isset( $theone['opt-hidden-sidebar-pos'] ) ? $theone['opt-hidden-sidebar-pos'] : 'right'; // Position of hidden sidebar
    $sidebar_side = $sidebar_side == 'left' ? 'right' : 'left';
    
    $base_color = isset( $theone['opt-general-accent-color'] ) ? $theone['opt-general-accent-color'] : array( 'color' => '#ffc40e', 'alpha' => 1 ); // Default is Contruction design base color (like yellow)
    $root_base_color = $base_color['color'];
    $base_color = ts_color_hex2rgba( $base_color['color'], $base_color['alpha'] );
    
    $site_layout = isset( $theone['opt-site-layout'] ) ? esc_attr( $theone['opt-site-layout'] ) : 'fullwidth';
    
    $current_box_bg_img = isset( $theone['opt-body-box-background']['background-image'] ) ? esc_url( $theone['opt-body-box-background']['background-image'] ) : '';
    $current_box_site_main_bg_color = isset( $theone['opt-box-site-main-background']['background-color'] ) ? esc_url( $theone['opt-box-site-main-background']['background-color'] ) : '';
    
    $demo_names = array( 'construction', 'plumber', 'mechanic', 'cleaning', 'carpenter', 'maintenance', 'electrician', 'gardner', 'renovation', 'logistics', 'mining',
                        'metal_construction', 'fuel_industry', 'autoshop', 'movers' );
    $demo_links = array( 
        'construction'          =>  'http://dev.themestudio.net/contruction/',
        'plumber'               =>  'http://dev.themestudio.net/helmets/', 
        'mechanic'              =>  'http://dev.themestudio.net/mechanic/',
        'cleaning'              =>  'http://dev.themestudio.net/cleaning/',
        'carpenter'             =>  'http://dev.themestudio.net/helmets/carpenter/', 
        'maintenance'           =>  'http://dev.themestudio.net/maintenance/',
        'electrician'           =>  'http://dev.themestudio.net/helmets/electrician/', 
        'gardner'               =>  'http://dev.themestudio.net/helmets/gardner/', 
        'renovation'            =>  'http://dev.themestudio.net/helmets/renovation/',
        'logistics'             =>  'http://dev.themestudio.net/helmets/logistics/', 
        'mining'                =>  'http://dev.themestudio.net/helmets/mining/',
        'metal_construction'    =>  'http://dev.themestudio.net/helmets/metal_construction/',
        'fuel_industry'         =>  'http://dev.themestudio.net/helmets/fuel_industry/',
        'autoshop'              =>  'http://dev.themestudio.net/helmets/autoshop/',
        'movers'                =>  'http://dev.themestudio.net/helmets/movers/', 
    );
    
    $total_demos = count( $demo_names );
    
    $html = '';
    ob_start();
    ?>
    
    <div class="ts-frontend-panel-tools-wrap tools-wrap-<?php echo esc_attr( $sidebar_side ); ?>">
        <a class="ts-handler" href="#" title="<?php _e( 'Custom style', 'themestudio' ); ?>" data-side="<?php echo esc_attr( $sidebar_side ); ?>"><i class="fa fa-cog fa-spin"></i></a>
        <div class="ts-frontend-custom-style-form-wrap">
            <div class="style-form">
                
                <h3><?php _e( 'Customize Template', 'themestudio' ); ?></h3>
                
                <div class="fields-group">
                    <h4><?php _e( 'Base color:', 'themestudio' ) ?></h4>
                    <input class="base-color ts-color-picker" value="<?php echo esc_attr( $base_color ); ?>" />
                </div>
                
                <div class="fields-group">
                    <h4><?php _e( 'Site layout:', 'themestudio' ) ?></h4>
                    <div data-layout="fullwidth" class="site-layout-select <?php echo $site_layout == 'fullwidth' ? 'current-site-layout': ''; ?>"><?php _e( 'Fullwidth', 'themestudio' ); ?></div>
                    <div data-layout="box" class="site-layout-select <?php echo $site_layout == 'box' ? 'current-site-layout': ''; ?>"><?php _e( 'Box', 'themestudio' ); ?></div>
                </div>
                
                <div class="fields-group field-bg-group">
                    <h4><?php _e( 'Body Bg Image:', 'themestudio' ) ?></h4>
                    <div class="bg-img-preview-wrap">
                        
                        <?php if ( $current_box_bg_img != '' ): ?>
                            <div class="bg-img-preview current-bg" data-img-src="<?php echo esc_url( $current_box_bg_img ); ?>" style="background-image: url(<?php echo esc_url( $current_box_bg_img ); ?>);">
                            </div>
                        <?php endif; ?>
                        
                        <?php for ( $i = 0; $i <= 24; $i++ ): ?>
                            <?php if ( file_exists( THE_ONE_LIBS . '/frontend-custom-panel/images/patterns/bg' . $i . '.png' ) ): ?>
                                <div class="bg-img-preview" data-img-src="<?php echo esc_url( THE_ONE_LIBS_URL . '/frontend-custom-panel/images/patterns/bg' . $i . '.png' ); ?>" style="background-image: url(<?php echo esc_url( THE_ONE_LIBS_URL . '/frontend-custom-panel/images/patterns/bg' . $i . '.png' ); ?>);">
                                </div>
                            <?php endif; ?>
                        <?php endfor; ?>
                        
                    </div><!-- /.bg-img-preview-wrap -->
                </div>
                
                <div class="fields-group field-bg-color-group">
                    <h4><?php _e( 'Box Bg Color:', 'themestudio' ) ?></h4>
                    <div class="bg-color-preview-wrap">
                        
                        <input class="bg-color-preview ts-color-picker" value="<?php echo esc_attr( $current_box_site_main_bg_color ); ?>" />
                        
                    </div><!-- /.bg-color-preview-wrap -->
                </div>
                
                <div class="fields-group fields-demos-group">
                    <h4><?php echo sprintf( __( 'Experience our %d other demos:', 'themestudio' ), ( $total_demos - 1 ) ); ?></h4>
                    <div class="fields-demos-group-inner">
                        <?php
                            $i = 0; 
                        ?>
                        <?php foreach ( $demo_names as $demo_name ): ?>
                            <?php
                                $demo_link = isset( $demo_links[$demo_name] ) ? $demo_links[$demo_name] : '#';
                            ?>
                            <?php if ( file_exists( THE_ONE_LIBS . '/frontend-custom-panel/images/sites/' . $demo_name . '_preview.png' ) ): ?>
                                <?php
                                    $i++;
                                ?>
                                <div class="site-demo-preview">
                                    <a href="<?php echo esc_attr( $demo_link ) ?>" title="<?php echo esc_attr( sprintf( __( 'Go to %s', 'themestudio' ), $demo_name ) ); ?>">
                                        <img src="<?php echo esc_url( THE_ONE_LIBS_URL . '/frontend-custom-panel/images/sites/' . $demo_name . '_preview.png' ); ?>" alt="<?php echo esc_attr( $demo_name ); ?>" />
                                    </a>
                                    
                                    <?php
                                        $demo_name_display = str_replace( '_', ' ', $demo_name );
                                    ?>
                                    <p><?php echo $demo_name_display; ?></p>
                                </div>
                                <?php if ( $i % 2 == 0 ): ?>
                                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div><!-- /.fields-demos-group-inner -->
                </div><!-- /.fields-demos-group -->
                
            </div><!-- /.style-form -->
        </div><!-- /.ts-frontend-custom-style-form-wrap -->
    </div><!-- /.ts-frontend-panel-tools-wrap -->
    
    <?php
    $html .= ob_get_clean();
    echo $html;
    
}


function ts_enqueue_style_switcher_via_ajax() {
    global $theone;
    
    $css = '';
    
    header( 'Content-type: text/css; charset: UTF-8' );
    
    
    echo $css;
    
    die();
}
add_action( 'wp_ajax_ts_enqueue_style_switcher_via_ajax', 'ts_enqueue_style_switcher_via_ajax' );
add_action( 'wp_ajax_nopriv_ts_enqueue_style_switcher_via_ajax', 'ts_enqueue_style_switcher_via_ajax' );