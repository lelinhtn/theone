<?php 
	/**
	* 404.php
	* The main post loop in THE ONE
	* @author Theme Studio
	* @package THE ONE
	* @since 1.0.0
	*/
	get_header();
    
    global $theone;
    $title_404 = isset( $theone['opt-404-title'] ) ? sanitize_text_field( $theone['opt-404-title'] ) : '';
    $text_404 = isset( $theone['opt-404-text'] ) ? apply_filters( 'the_content', $theone['opt-404-text'] ) : '';
?>

	<div id="content" class="site-content">
		<div id="page-not-found">
            <section class="section section-404">
        		<div class="container">
					<div class="row">
                        <div class="col-lg-8 col-lg-push-2">
                            <h1><?php echo $title_404; ?></h1>
                            <?php echo $text_404; ?>
                            <a href="<?php echo home_url(); ?>" class="btn blue"><?php _e( 'Back To Homepage', 'themestudio' ); ?></a>    
                        </div>
                    </div>
        		</div>
        	</section>
		</div>
	</div><!-- /.site-content -->

<?php get_footer(); ?>