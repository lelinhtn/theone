jQuery(document).ready(function($){
    
    "use strict";
    
    ts_update_progress_bars_unit_pos();
    // Update progress bars unit position
    function ts_update_progress_bars_unit_pos() {
        
        $('.wpb_wrapper .vc_progress_bar .vc_single_bar').each(function(){
            
            var $this = $(this);
            var label_unit = $this.find('.vc_label_units');
            var percent_val = $this.find('.vc_bar').attr('data-percentage-value');
            
            label_unit.css({
                'left':  percent_val + '%'
            });
             
        });
        
    }
    
    // Hidden sidebar
    ts_open_hidden_sidebar();
    function ts_open_hidden_sidebar() {
        var sidebar_side = $('.ts-hiddden-sidebar-icon').attr('data-side');
        sidebar_side = ( typeof sidebar_side == 'undefined' || typeof sidebar_side == false ) ? 'right' : sidebar_side;
        
        if ( $('#ts-hidden-sidebar-wrap').length ) {
            
            $('.ts-hiddden-sidebar-icon').sidr({
                name: 'ts-hidden-sidebar-wrap',
                side: sidebar_side,
                onOpen: function(){
                    $('.side-nav-overlay').css({
                        'display': 'block'
                    });
                    
                    // Nice Sroll For sliding sidebar (hidden sidebar)
                    $('#ts-hidden-sidebar-wrap').niceScroll({
                        cursorborderradius: '0',
                        scrollspeed: 70,
                        mousescrollstep: 8 * 8,
                    });
                },
                onClose: function() {
                    $('.side-nav-overlay').css({
                        'display': 'none'
                    });
                    $('#ts-hidden-sidebar-wrap').niceScroll().remove();
                }
                
            });   
            
            // Close sidebar when click on overlay
            $('.side-nav-overlay, .close-hidden-sidebar').bind('click touchstart', function(e){
                
                $.sidr('close', 'ts-hidden-sidebar-wrap');
                e.preventDefault();

            });
             
        }
    }
    
    
    // Nice scroll for style swicher frontend
    ts_style_swicher_nicescroll();
    function ts_style_swicher_nicescroll() {
        if ( $('.ts-frontend-panel-tools-wrap .ts-frontend-custom-style-form-wrap').length ) {
            $('.ts-frontend-panel-tools-wrap .ts-frontend-custom-style-form-wrap').niceScroll({
                cursorborderradius: '0',
                scrollspeed: 70,
                mousescrollstep: 8 * 8,
                autohidemode: false
            });
        }
    }
    
    if ( $('.ts-nicescroll').length ) {
        $('.ts-nicescroll').niceScroll({
            cursorborderradius: '0',
            scrollspeed: 70,
            mousescrollstep: 8 * 8
        });   
    }
    
    
    // Ajax login
    $(document).on('submit', '.ts-login-form', function(e){
        
        var $this = $(this);
        
        if ( $this.hasClass('logging') ) {
            return false;
        }
        
        var user_login = $this.find('input[name=log]').val();
        var user_pass = $this.find('input[name=pwd]').val();
        var rememberme = $this.find('input[name=rememberme]').is(':checked') ? 'yes': 'no';
        var redirect_to = $this.find('input[name=redirect_to]').val();
        var login_nonce = $this.find('input[name="login-ajax-nonce"]').val();
        
        var data = {
            action: 'ts_do_login_via_ajax',
            user_login: user_login,
            user_pass: user_pass,
            rememberme: rememberme,
            redirect_to: redirect_to,
            login_nonce: login_nonce
        };
        
        $('.ts-login-form').addClass('logging loading');
        if ( !$('.ts-login-form .ts-loading').length ) {
            $('.ts-login-form').prepend('<div class="ts-loading"><i class="fa fa-circle-o-notch fa-spin"></i></div>');
        }
        
        $.post(ajaxurl, data, function(response){
            
            if ( $.trim( response['is_logged_in'] ) == 'yes' ) {
                $('.ts-login-form').replaceWith(response['message']);
                document.location.href = theme_global_localize['login_redirect_url'];
            }
            else{
                $('.ts-login-form').find('.login-message').remove();
                $('.ts-login-form').append(response['message']);
            }
            
            $('.ts-login-form').removeClass('logging loading');
            
        });   
        
        return false; 
         
    });
    
    
    // Ajax Register
    $(document).on('submit', '.ts-register-form', function(e){
        
        var $this = $(this);
        
        if ( $this.hasClass('registering') ) {
            return false;
        }
        
        var username = $this.find('input[name=username]').val();
        var email = $this.find('input[name=email]').val();
        var password = $this.find('input[name=password]').val();
        var repassword = $this.find('input[name=confirm-password]').val();
        var agree = $this.find('input[name=agree]').is(':checked') ? 'yes': 'no';
        var register_nonce = $this.find('input[name="register-ajax-nonce"]').val();
        
        var data = {
            action: 'ts_do_register_via_ajax',
            username: username,
            email: email,
            password: password,
            repassword: repassword,
            agree: agree,
            register_nonce: register_nonce
        };
        
        $('.ts-register-form').addClass('registering loading');
        if ( !$('.ts-register-form .ts-loading').length ) {
            $('.ts-register-form').prepend('<div class="ts-loading"><i class="fa fa-circle-o-notch fa-spin"></i></div>');
        }
        
        $.post(ajaxurl, data, function(response){
            
            if ( $.trim( response['register_ok'] ) == 'yes' ) {
                $('.ts-register-form').replaceWith(response['message']);
            }
            else{
                $('.ts-register-form').find('.register-message').remove();
                $('.ts-register-form').append(response['message']);
            }
            
            console.log(response);
            $('.ts-register-form').removeClass('registering loading');
            
        });   
        
        return false;
         
    });
    
    
    // Ajax forgot pass
    $(document).on('submit', '.ts-forgot-pass-form', function(e){
        
        var $this = $(this);
        var username = $this.find('input[name=username]').val();
        var email = $this.find('input[name=email]').val();
        
        var data = {
            action: 'ts_retrieve_password_via_ajax',
            username: username,
            email: email
        };
        
        $('.ts-forgot-pass-form').addClass('processing loading');
        if ( !$('.ts-forgot-pass-form .ts-loading').length ) {
            $('.ts-forgot-pass-form').prepend('<div class="ts-loading"><i class="fa fa-circle-o-notch fa-spin"></i></div>');
        }
        
        $.post(ajaxurl, data, function(response){
            
            $('.ts-forgot-pass-form').find('.forgot-message').remove();
            $('.ts-forgot-pass-form').append(response['message']);
            
            console.log(response);
            $('.ts-forgot-pass-form').removeClass('processing loading');
            
        });   
        
        return false; 
    });
    
    
    $('.back-to-top').bind('click', function(e){
        $('html, body').animate({scrollTop: 0}, 450);
        e.preventDefault();
    });
    
    
    $(window).scroll(function() {
        ts_update_back_to_top();
    });
    
    ts_update_back_to_top();
    function ts_update_back_to_top() {
        if($(window).scrollTop() > 180) {
            $('.back-to-top').stop().css({
                'display': 'block'
            }).animate({
                'bottom': '80'
            }, 180);
        } else {
            $('.back-to-top').stop().animate({
                'bottom': '-50'
            }, 180);
        }  
    }   
    
    // Coming Soon Countdown
    $('.ts-countdown-wrap').each(function(){
        var $this = $(this);
        var countdown_to_date = $this.attr('data-countdown');
        $this.countdown(countdown_to_date, function(event) {
            $(this).html(
                event.strftime(theme_global_localize['html']['countdown'])
            );
        });
        
         
    });

    //Select 2
    //$('.shipping-calculator-form select, #pa_color, .woocommerce-ordering .orderby, .widget select, .ts-select').chosen({
//        disable_search_threshold: 10
//    });
    
    
    
    
    /********** FROM THE ONE *************/
    
    // Icon box css via js
    $('.ts-iconbox.icon-css-via-js').each(function(){
        
        var $this = $(this);
        var icon_color = $this.attr('data-icon-color');
        var icon_hover_color = $this.attr('data-icon-hover-color');
        var icon_bg_color = $this.attr('data-icon-bg-color');
        var icon_hover_bg_color = $this.attr('data-icon-hover-bg-color');
        var icon_current_border_color = $this.find('.icon').length ? $this.find('.icon').css('border-color') : '';
        var text_color = $this.attr('data-text-color');
        
        $this.css({
            'color': text_color
        });
        
        $this.find('.icon').css({
            'color':  icon_color,
            'background-color': icon_bg_color,
        });
        
        $this.find('h3, a').css({
            'color': text_color
        });
        
        $this.hover(function(){
            $(this).find('.icon').css({
                'color': icon_hover_color,
                'background-color': icon_hover_bg_color,
                'border-color': icon_hover_bg_color
            });
        }, function(){
            $(this).find('.icon').css({
                'color': icon_color,
                'background-color': icon_bg_color,
                'border-color': icon_current_border_color
            });
        });
         
    });
    
    // Icon box readmore css via js
    $('.ts-iconbox .ts-readmore').each(function(){
        var this_color = $(this).css('color');
        var thi_icon_box_id = $(this).closest('.ts-iconbox').attr('id');
        
        if ( (typeof thi_icon_box_id != 'undefined') && (typeof thi_icon_box_id != false) ){
            $('head').append('<style type="text/css">#' + thi_icon_box_id + ' .ts-readmore:before{background-color: ' + this_color +';}</style>');   
        }
        
    });
    
    // THE ONE OWL CAROUSEL
    function theone_init_owl_carousel() {
        $('.ts-owl-carousel').each(function(){
            
            var $this = $(this),
                $loop = $this.attr('data-loop') == "true",
                $numberItem = parseInt($this.attr('data-number')),
    			$Nav = $this.attr('data-navControl') == "true",
          		$Dots = $this.attr('data-Dots') == "true",
          		$autoplay = $this.attr('data-autoPlay') == "true",
                $autoplayTimeout = parseInt($this.attr('data-autoPlayTimeout')),
          		$marginItem = parseInt($this.attr('data-margin')),
                $rtl = $this.attr('data-rtl') == "true",
          		$resNumber; // Responsive Settings
                
                $numberItem = (isNaN($numberItem)) ? 4: $numberItem;
                $autoplayTimeout = (isNaN($autoplayTimeout)) ? 4000 : $autoplayTimeout;
                $marginItem = (isNaN($marginItem)) ? 0 : $marginItem;
                
            switch ( $numberItem ) {
                
                case 1 :
                    $resNumber = {
                        0:{
        		            items:1
        		        }
                    }
                    break;
                  
                case 2 :
                    $resNumber = {
                        0:{
        		            items:1
        		        },
                        480:{
        		            items:1
        		        },
                        768:{
        		            items:2
        		        },
        		        992:{
        		            items:$numberItem
        		        }
                    }
                    break;
                
                case 3 : case 4 :
                    $resNumber = {
                        0:{
        		            items:1
        		        },
                        480:{
        		            items:1
        		        },
                        768:{
        		            items:2
        		        },
                        992:{
        		            items:3
        		        },
                        1200:{
        		        	items: $numberItem
        		        }
                    }
                    break;
                
                default : // $numberItem > 4
                    $resNumber = {
                        0:{
        		            items:1
        		        },
                        480:{
        		            items:2
        		        },
                        768:{
        		            items:3
        		        },
                        992:{
        		            items:4
        		        },
                        1200:{
        		        	items: $numberItem
        		        }
                    }
                    break;
            } // Endswitch
            
            $(this).owlCarousel({
    			loop:$loop,
    			nav:$Nav,
    			navText:['<span class="icon icon-arrows-left"></span>','<span class="icon icon-arrows-right"></span>'],
    		   	dots : $Dots,
    		    autoplay:$autoplay,
                autoplayTimeout: $autoplayTimeout,
    		    margin:$marginItem,
    		    //responsiveClass:true,
                rtl:$rtl,
    		    responsive: $resNumber,
                autoplayHoverPause: true,
                //center: true,
                onRefreshed: function(){
                    var total_active = $this.find('.owl-item.active').length;
                    var i = 0;
                    
                    $this.find('.owl-item').removeClass('active-first active-last');
                    $this.find('.owl-item.active').each(function(){
                        i++;
                        if ( i == 1 ) {
                            $(this).addClass('active-first');
                        }
                        if ( i == total_active ) {
                            $(this).addClass('active-last');
                        }
                    });
                },
                onTranslated: function(){
                    var total_active = $this.find('.owl-item.active').length;
                    var i = 0;
                    
                    $this.find('.owl-item').removeClass('active-first active-last');
                    $this.find('.owl-item.active').each(function(){
                        i++;
                        if ( i == 1 ) {
                            $(this).addClass('active-first');
                        }
                        if ( i == total_active ) {
                            $(this).addClass('active-last');
                        }
                    });
                }
    		});
            
        });
    }
    //theone_init_owl_carousel();
    
    function theone_reinit_owl_carousel(){
        $(window).load(function(){
            // Trigger resize event after window load to make sure some sliders responsive (fix Visual Composer)
            $('.ts-owl-carousel').each(function(){
                var $this = $(this);
                $this.trigger('destroy.owl.carousel');
                $this.html($this.find('.owl-stage-outer').html()).removeClass('owl-loaded');
                //$this.find('.owl-stage-outer').children().unwrap();
            });
            // Init OWL Carousel again
            theone_init_owl_carousel();
            
            // Reinit Lightbox for live elements
            if ( $("a[rel^='prettyPhoto']").length ) {
                $("a[rel^='prettyPhoto']").prettyPhoto({
        			show_title: true,
        			allow_resize: true,
        			default_width: 800,
        			default_height: 400,
        			social_tools: false,
                    deeplinking: false
        		});   
            }
            
            console.log('The One OWL Carousel Reinited');
        });
    }	
    theone_reinit_owl_carousel();
    
    
   
    
    
    /** Page preload **/
    //$('body').addClass('start-preload');
//    $(window).load(function(){
//        
//        $('body').removeClass('start-preload').addClass('loaded');
//         
//    });
            
});