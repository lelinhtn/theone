(function($){
	$(document).ready(function(){
		"use strict";

		//ACORDION
		$(".ts-acordion").each(function() {
			var $this = $(this);
			var $tab_active = parseInt($this.attr('data-tab-active'));
			$(this).accordion({
				active:$tab_active,
				collapsible: true
			});
		});

		//ANIMATED NUMBER
	    $('.ts-animate-number').each(function(){
	    	var count_element = $(this).find('.number').attr('data-number');
	    	if (count_element != '') {
	    		$(this).find('.number').countTo({
	    			from: 0,
	    			to: count_element,
	    			speed: 2500,
		            refreshInterval: 50,
	    		})
	    	};
	    });

	    //OPACITY CONTENT HERO SECTION IMAGE 
	    $(window).load(function(){
    		var heightImgHeroSection = $('.hero-section-image').outerHeight();
		    $(window).scroll(function() {
		        var scroll = getCurrentScroll();
		        if(scroll > 10){
		        	var opacityContent = 1 - (scroll/heightImgHeroSection);
		        	$('.hero-section-content').css("opacity",opacityContent);
		        }
		        else{
		        	$('.hero-section-content').css("opacity",1)
		        }
			});
		    function getCurrentScroll() {
		        return window.pageYOffset || document.documentElement.scrollTop;
		    }

        });

	    //POSITION CONTENT HERO IMAGE
	    var heightHeader = $('#header').outerHeight();
	    var heightHeroImg	= $('#hero-section').outerHeight();
	    var topContent = (heightHeroImg - heightHeader)/2 + heightHeader;
	    $('.hero-section-content').css("top",topContent);

	    //BACKGROUND PRALLAX
        $(window).bind('load', function () {
            parallaxInit();
        });
        function parallaxInit() {
            testMobile = isMobile.any();
            if (testMobile == null)
            {

                $('.bg-parallax').each(function(){
                    $(this).parallax('50%', 0.3);
                });
            }
            testMobile = isMobile.iOS()
        }
	    //Mobile Detect
	    var testMobile;
	    var isMobile = {
	        Android: function() {
	            return navigator.userAgent.match(/Android/i);
	        },
	        BlackBerry: function() {
	            return navigator.userAgent.match(/BlackBerry/i);
	        },
	        iOS: function() {
	            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	        },
	        Opera: function() {
	            return navigator.userAgent.match(/Opera Mini/i);
	        },
	        Windows: function() {
	            return navigator.userAgent.match(/IEMobile/i);
	        },
	        any: function() {
	            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	        }
	    };

	    //BUTTON HOVER
	    $('.ts-button-shortcode').each(function(){
	    	var $bgHover = $(this).attr('data-hoverbg');
	    	var $colorHover = $(this).attr('data-colorhover');
	    	var $bg = $(this).attr('data-bg');
	    	var $color = $(this).attr('data-color');
	    	var $colorShadow = $(this).attr('data-colorshadow');
	    	var $hoverShadow = $(this).attr('data-shadowhover');
	    	$(this).css({
	    		'background':$bg,
	    		'color':$color
	    	});
	    	if($(this).hasClass('button-3d')){
	    		if($colorShadow!=''){
		    		$(this).css({
		    			'box-shadow': '0 3px 0 '+$colorShadow,
		    			'-moz-box-shadow': '0 3px 0 '+$colorShadow,
		    			'-webkit-box-shadow': '0 3px 0 '+$colorShadow
		    		});
	    		}
	    		if($colorShadow !='' && $hoverShadow != ''){
		    		$(this).hover(function(){
		    			$(this).css({
			    			'box-shadow': '0 3px 0 '+$hoverShadow,
			    			'-moz-box-shadow': '0 3px 0 '+$hoverShadow,
			    			'-webkit-box-shadow': '0 3px 0 '+$hoverShadow
			    		});
		    		},function(){
		    			$(this).css({
			    			'box-shadow': '0 3px 0 '+$colorShadow,
			    			'-moz-box-shadow': '0 3px 0 '+$colorShadow,
			    			'-webkit-box-shadow': '0 3px 0 '+$colorShadow
			    		});
		    		});
		    	}
	    	};

	    	if($(this).hasClass('button-outline')){
	    		$(this).css({
	    			'border-color': $color
	    		})
	    		$(this).hover(function(){
	    			$(this).css("border-color",$bgHover);
	    		},function(){
	    			$(this).css("border-color",$color);
	    		});
	    	}

	    	$(this).hover(function(){
	    		$(this).find('.ts-button-hover').css("background",$bgHover);
	    		$(this).find('.ts-button-text').css("color",$colorHover);
	    		$(this).find('.ts-icon-button').css("color",$colorHover);
	    	},function(){
	    		$(this).find('.ts-button-hover').css("background",'');
	    		$(this).find('.ts-button-text').css("color",'');
	    		$(this).find('.ts-icon-button').css("color",'');
	    	})

	    	
	    });

		//CLIENT CAROUSEL 
		$('.ts-client-shortcode1').each(function(){
			var $numberItemrow = parseInt($(this).attr('data-itemrow'));
			var $classItem;
			var $resItem;
			if($numberItemrow == 3){
				$classItem = 'three-item';
				$resItem = {
			        0:{
			            items:1
			        },
			        480:{
			            items:2
			        },
			        768:{
			            items:$numberItemrow
			        }
				};
			}
			if($numberItemrow == 4){
				$classItem = 'four-item';
				$resItem = {
			        0:{
			            items:1
			        },
			        480:{
			            items:2
			        },
			        768:{
			            items:3
			        },
			        992:{
			            items:$numberItemrow
			        }
				};
			}
			if($numberItemrow == 5){
				$classItem = 'five-item';
				$resItem = {
			        0:{
			            items:1
			        },
			        480:{
			            items:2
			        },
			        768:{
			            items:3
			        },
			        992:{
			            items:4
			        },
			        1200:{
			        	items:$numberItemrow
			        }
				};
			}
			if($numberItemrow == 6){
				$classItem = 'six-item';
				$resItem = {
			        0:{
			            items:1
			        },
			        480:{
			            items:2
			        },
			        768:{
			            items:3
			        },
			        992:{
			            items:4
			        },
			        1200:{
			        	items:$numberItemrow
			        }
				};
			}

			if($(this).hasClass('has-slide')){
				$(this).owlCarousel({
				    loop:true,
				    dots : false,
				    autoplay:true,
				    responsiveClass:true,
				    responsive: $resItem
				})
			}else{
				$(this).addClass($classItem)
			}
		});
        
        
        


		//TEAM CAROUSEL
		$('.ts-team-carousel').each(function(){
	      	var	$autoplay = $(this).attr('data-autoPlay') == "true" ? true : false;

			$(this).owlCarousel({
				loop:true,
				nav: false,
				dots:false,
				margin: 30,
				autoplay:$autoplay,
				responsiveClass: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:1
					},
					768:{
						items:2
					},
					992:{
						items:3
					}
				}
			})
		});

		//ICONS BOX CAROUSEL
		$('.ts-iconsbox-carousel').each(function(){
			$(this).owlCarousel({
				loop:true,
				nav: false,
				dots:true,
				margin: 30,
				responsiveClass: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:1
					},
					768:{
						items:2
					},
					992:{
						items:3
					}
				}
			})
		});

		//BLOG CAROUSEL
		$('.ts-blog-carousel').each(function(){
			var $itemBlog = $(this).attr('data-number-item'),
				$showDots = $(this).attr('data-dots') == "true" ? true : false,
				$showNav = $(this).attr('data-nav') == "true" ? true : false,
	      		$autoplay = $(this).attr('data-autoPlay') == "true" ? true : false;

			$(this).owlCarousel({
				loop:true,
				nav: $showNav,
				dots:$showDots,
				margin: 30,
				autoplay:$autoplay,
				responsiveClass: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:1
					},
					768:{
						items:2
					},
					992:{
						items:$itemBlog
					}
				}
			})
		});


		//SLIDE DEVICED
        ts_devicedslide_init();
        $(window).load(function() {
            ts_deviced_responsive_calculator();
        });
        $(window).on("debouncedresize", function() {
            ts_deviced_responsive_calculator();
        });

        //Audio HTML5
	    $('.audio-wraper').each(function(){
	    	var $linkAudio = $(this).attr('data-audio');
	    	$("#jquery_jplayer_1").jPlayer({
    			ready: function (event) {
    				$(this).jPlayer("setMedia", {
    					free:true,
    					mp3: $linkAudio
    				});
    			},
    			swfPath: "js",
    			supplied: "mp3, wav, ogg, all",
    			useStateClassSkin: true,
    			smoothPlayBar: true,
    			globalVolume: true,
    			keyEnabled: true
    		});

    		var widthAudio = $(this).find('.jp-interface').width();
    		var widthVolumn = $(this).find('.jp-volume-controls').outerWidth();
    		var widthControl = $(this).find('.jp-controls').outerWidth();
    		var widthProgress = widthAudio - widthVolumn - widthControl -14;
    		$(this).find('.jp-progress').css("width",widthProgress);
	    })

	    //VIDEO RESPONSIVE
	    $('.main-container').fitVids();

	    //MENU TOGOLE 
	    $('.togole-navigation').click(function(){
	    	$('.ts-main-menu').slideToggle(500);
	    })

	    //MENU RESPONSIVE
	    var ts_is_mobile = (Modernizr.touch) ? true : false;
		if (ts_is_mobile === true){
			console.log('b');
			$(document).on('click', '.ts-main-menu .menu-item-has-children > a', function(e){
				var licurrent = $(this).closest('li');
				var liItem = $('.ts-main-menu .menu-item-has-children');
				if ( !licurrent.hasClass('ts-show-menu') ) {
					liItem.removeClass('ts-show-menu');
					licurrent.addClass('ts-show-menu');

	                // Close all child submenu if parent submenu is closed
	                if ( !licurrent.hasClass('ts-show-menu') ) {
	                    licurrent.find('li').removeClass('ts-show-menu');
	                }
	                return false;
	                e.preventDefault();
				}else{
					var href = $this.attr('href');
	                if ( $.trim( href ) == '' || $.trim( href ) == '#' ) {
	                    licurrent.toggleClass('ts-show-menu');
	                }
	                else{
	                    window.location = href;
	                } 
				}
				// Close all child submenu if parent submenu is closed
	            if ( !licurrent.hasClass('ts-show-menu') ) {
	                licurrent.find('li').removeClass('ts-show-menu');

	            }
	            e.stopPropagation();
			});
			$(document).on('click', function(e){
            var target = $( e.target );
            if ( !target.closest('.ts-show-menu').length || !target.closest('.navigation').length ) {
                $('.ts-show-menu').removeClass('ts-show-menu');

            }
        }); 
	
		}else{
			$('.ts-main-menu .menu-item-has-children').hover(function(){
        		$(this).addClass('ts-show-menu');
        	}, function(){
        		$(this).removeClass('ts-show-menu'); 
        	});

		}

		//ANIMATE COLUMN
		ts_height_animate_column();
		$(window).on("resize", function() {
             setTimeout(ts_height_animate_column, 200);
        });
        
        // Really resize
        window.addEventListener("resize", function() {
            // Mr Linh
        }, false);

		//SECTION ICONBOX
    	$(window).load(function(){
    		// Padding content
	    	var widthW = $(window).width();
	    	var widthContent = $('.container').outerWidth();
	    	var paddingContent = (widthW - widthContent)/2;
	    	$('.ts-section-iconbox').each(function(){
	    		if ($(this).hasClass('left-bg')) {
	    			$(this).find('.list-iconbox').css("padding-right",paddingContent)
	    		};
	    		if ($(this).hasClass('right-bg')) {
	    			$(this).find('.list-iconbox').css("padding-left",paddingContent)
	    		};
	    	});

	    	//height bgimg
			if($(window).width() > 991){
				$('.ts-section-iconbox').each(function(){
					var _$this = $(this);
					var $heightContent = _$this.find('.list-iconbox').outerHeight();
					console.log($heightContent);
					var $bgIconbox = _$this.find('.bg-iconbox').attr('data-background');
					_$this.find('.bg-iconbox').css({
						"height":$heightContent,
						"background-image": "url("+$bgIconbox+")"
					});
				});
			}
            
            ts_height_animate_column();
    	});
			
		$(window).on("debouncedresize", function() {
			// Padding content
	    	var widthW = $(window).width();
	    	var widthContent = $('.container').outerWidth();
	    	var paddingContent = (widthW - widthContent)/2;
	    	$('.ts-section-iconbox').each(function(){
	    		if ($(this).hasClass('left-bg')) {
	    			$(this).find('.list-iconbox').css("padding-right",paddingContent)
	    		};
	    		if ($(this).hasClass('right-bg')) {
	    			$(this).find('.list-iconbox').css("padding-left",paddingContent)
	    		};
	    	});

			if($(window).width() > 991){
	            $('.ts-section-iconbox').each(function(){
					var _$this = $(this);
					var $heightContent = _$this.find('.list-iconbox').outerHeight();
					_$this.find('.bg-iconbox').css({
						"height":$heightContent,
					});
				});
	        }
        });

     	//DIVIDER SCROOL TOP BUTTON
     	$('.divider').each(function(){
     		if ($(this).hasClass('go-bottom')) {
				var scrollBottom = $(window).scrollTop() + $(window).height();
     			$(this).find('.scroll-bottom').click(function(){
     				$('html, body').animate({scrollTop : scrollBottom},800);
            		return false;
     			})
     		};
     		if ($(this).hasClass('go-top')) {
     			console.log('a');
     			$(this).find('.scroll-top').click(function(){
     				$('html, body').animate({scrollTop : 0},800);
            		return false;
     			})
     		};
     	})

     	//SKILL BAR
     	$('.item-skillbar').each(function(){
     		var $heightSkill = $(this).attr('data-height'),
     			$percentSkill = $(this).attr('data-percent'),
     			$bgSkill = $(this).attr('data-bgskill');

     		$(this).find('.skill-bar-bg').css("height",$heightSkill);
	        $(this).find('.skillbar-bar').animate({
	            'width':$percentSkill+'%'
	        },6000);
	        if($bgSkill != ''){
	        	 $(this).find('.skillbar-bar').css({
            		'background-color':$bgSkill
            	});
            };
	    });

	    // PROGRESS BAR
	    // $('.ts-progressbar').appear(function() {
		    $(".ts-chart").each(function() {
			    var size = $(this).attr('data-size'),
			    	barColor = $(this).attr('data-barColor'),
			    	trackColor = $(this).attr('data-trackColor'),
			    	lineWidth = $(this).attr('data-lineWidth');
	            $(this).easyPieChart({
	                easing: 'easeInOutQuad',
	                barColor: barColor,
	                animate: 2000,
	                trackColor: trackColor,
	                lineWidth: lineWidth,
	                size: size,
	                scaleColor: false,
	                lineCap:'square',
	                onStep: function(from, to, percent) {
	                    $(this.el).find('.chart-percent').text(Math.round(percent)+'%');
	                }
	            });
	           	$(this).find('span').css('line-height',size+'px');
		    });
		
		// });

		//LIGHT BOX
		$("a[rel^='prettyPhoto']").prettyPhoto({
			show_title: true,
			allow_resize: true,
			default_width: 800,
			default_height: 400,
			social_tools: false,
            deeplinking: false
		});

		//IMAGE SLIDE
		$('.ts-imageslide-flat').each(function(){
			var numberItem = 4;
			var navText  = ['',''];
			if($(this).hasClass('full-width')){
				numberItem = 5;
				navText=['<span class="icon icon-arrows-left"></span>','<span class="icon icon-arrows-right"></span>'];
			}
			var margin = 0;
			if($(this).hasClass('grid-style')){
				margin = 30;
			}
			$(this).owlCarousel({
				loop:true,
				nav: true,
				navText: navText,
				dots:false,
				margin: margin,
				autoplay:true,
				responsiveClass: false,
				responsive:{
					0:{
						items:1
					},
					480:{
						items:1
					},
					768:{
						items:2
					},
					992:{
						items:3
					},
					1200:{
						items: numberItem
					}
				}
			});
		});

	});
	new WOW().init();

	
	/* Mobile and Tablet Slideshow Responsive Calculator */
	function ts_deviced_responsive_calculator() {
	    var $browserSlide = jQuery(".ts-browser-slide");
	    var $destopSlide = jQuery(".ts-desktop-slide");
	    var $laptopSlide = jQuery(".ts-laptop-slide");
	    var $mobileSlide1 = jQuery(".ts-mobile-slide1");
	    var $mobileSlide2 = jQuery(".ts-mobile-slide2");

	    if (jQuery(".ts-desktop-slide").length) {
	        $destopSlide.each(function() {
	            var $this = jQuery(this),
	                $width = $this.outerWidth(),
	                $height = $this.outerHeight(),
	                $paddingTop = 28,
	                $paddingRight = 25,
	                $paddingBottom = 188,
	                $paddingLeft = 23;
	                console.log($height);

	            var $player = $this.find(".slide-container");
	            $player.css({
	                "padding-left": parseInt(($width * $paddingLeft) / 758),
	                "padding-right": parseInt(($width * $paddingRight) / 758),
	                "padding-top": parseInt(($height * $paddingTop) / 616),
	                "padding-bottom": parseInt(($height * $paddingBottom) / 616),
	            });

	            $this.hover(function(){
	            	$this.find('.flex-direction-nav > li > a.flex-prev').css("left", parseInt(($width * $paddingLeft) / 758));
	            	$this.find('.flex-direction-nav > li > a.flex-next').css("right", parseInt(($width * $paddingRight) / 758));
	            }, function(){
	            	$this.find('.flex-direction-nav > li > a.flex-prev').css("left",50);
	            	$this.find('.flex-direction-nav > li > a.flex-next').css("right",50 );
	            });

	            $this.find('.flex-control-nav').css("bottom",parseInt(($height * $paddingBottom) / 616)+20);
	        });
	    }

	    if (jQuery(".ts-laptop-slide").length) {
	        $laptopSlide.each(function() {
	            var $this = jQuery(this),
	                $width = $this.outerWidth(),
	                $height = $this.outerHeight(),
	                $paddingTop = 21,
	                $paddingRight = 83,
	                $paddingBottom = 43,
	                $paddingLeft = 84;

	            var $player = $this.find(".slide-container");
	            $player.css({
	                "padding-left": parseInt(($width * $paddingLeft) / 705),
	                "padding-right": parseInt(($width * $paddingRight) / 705),
	                "padding-top": parseInt(($height * $paddingTop) / 406),
	                "padding-bottom": parseInt(($height * $paddingBottom) / 406),
	            });

	            $this.find('.flex-control-nav').css("bottom",parseInt(($height * $paddingBottom) / 406)+20);

	        });
	    }
	    
	    if (jQuery(".ts-mobile-slide1").length) {
	        $mobileSlide1.each(function() {
	            var $this = jQuery(this),
	                $width = $this.outerWidth(),
	                $height = $this.outerHeight(),
	                $paddingTop = 60,
	                $paddingRight = 16,
	                $paddingBottom = 60,
	                $paddingLeft = 17;

	            var $player = $this.find(".slide-container");
	            $player.css({
	                "padding-left": parseInt(($width * $paddingLeft) / 269),
	                "padding-right": parseInt(($width * $paddingRight) / 269),
	                "padding-top": parseInt(($height * $paddingTop) / 538),
	                "padding-bottom": parseInt(($height * $paddingBottom) / 538),
	            });

	            $this.find('.flex-control-nav').css("bottom",parseInt(($height * $paddingBottom) / 538)+20);
	        });
	    }

	    if (jQuery(".ts-mobile-slide2").length) {
	        $mobileSlide2.each(function() {
	            var $this = jQuery(this),
	                $width = $this.outerWidth(),
	                $height = $this.outerHeight(),
	                $paddingTop = 46,
	                $paddingRight = 13,
	                $paddingBottom = 46,
	                $paddingLeft = 12;

	            var $player = $this.find(".slide-container");
	            $player.css({
	                "padding-left": parseInt(($width * $paddingLeft) / 277),
	                "padding-right": parseInt(($width * $paddingRight) / 277),
	                "padding-top": parseInt(($height * $paddingTop) / 540),
	                "padding-bottom": parseInt(($height * $paddingBottom) / 540),
	            });

	            $this.find('.flex-control-nav').css("bottom",parseInt(($height * $paddingBottom) / 540)+20);
	        });
	    }
	}

	function ts_devicedslide_init() {

	  jQuery('.ts-devicedslide').each(function () {

	  	var $this = jQuery(this),
	      	$controlNav = $this.attr('data-Nav') == "true" ? true : false,
	      	$controlDots = $this.attr('data-Dots') == "true" ? true : false;

	  	var $selector_class = ".ts-deviced-slide > li";
	    $this.flexslider({
			selector: $selector_class,
			slideshow: true,
			animation: 'fade',
			controlNav: $controlDots,
			directionNav: $controlNav,
	      	slideshowSpeed: 7000,
	      	animationSpeed: 700,
			pauseOnHover: true
	    });

	  });

	}

	//HEIGHT ANIMATE COULUMN
	function ts_height_animate_column(){
        $('.shortcode-column .column-item').css({
            'height' : 'auto' 
        });
		$('.shortcode-column').each(function(index, elem){

			var $this = $(this);
			var $items = $this.find('.column-item');
			var $max_height_item = $items.eq(0).outerHeight();
			$items.each(function(index, elem) {
				if ( $(elem).outerHeight() > $max_height_item) $max_height_item = $(elem).outerHeight();
			});

			$items.each(function(){
				$(this).css({'height' : $max_height_item});
			});
		});
        
	}

}(jQuery));