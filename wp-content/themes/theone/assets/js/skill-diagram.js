var o = {
	init: function(){
		this.diagram();
	},
	random: function(l, u){
		return Math.floor((Math.random()*(u-l+1))+l);
	},
	diagram: function(){
		$('.ts-skill-diagram').each(function(){
			var t = $(this),
				// numberSkill = t.find('.ts-skill-chart .skill-chart-item').length(),
				// strokeWidth = t.attr('data-troke-width'),
				// gapCircle = t.attr('data-gap-circle'),
				dimension = t.attr('data-dimension'),
                radius = (dimension/2),
                center_color = t.attr('data-circlecolor'),
                default_textcolor = t.attr('data-default-textcolor'),
                default_text = t.attr('data-default-text'),
                diagram_id = t.find('.ts-diagram').attr('id');

			var r = Raphael(diagram_id, dimension, dimension),
				rad = 73,
				defaultText = default_text,
				speed = 250;
			
			r.circle(radius, radius, 85).attr({ stroke: 'none', fill: center_color });
			
			var title = r.text(radius, radius, defaultText).attr({
				font: '20px Roboto',
				fill: default_textcolor
			}).toFront();
			
			r.customAttributes.arc = function(value, color, rad){
				var v = 3.6*value,
					alpha = v == 360 ? 359.99 : v,
					random = o.random(91, 240),
					a = (random-alpha) * Math.PI/180,
					b = random * Math.PI/180,
					sx = radius + rad * Math.cos(b),
					sy = radius - rad * Math.sin(b),
					x = radius + rad * Math.cos(a),
					y = radius - rad * Math.sin(a),
					path = [['M', sx, sy], ['A', rad, rad, 0, +(alpha > 180), 1, x, y]];
				return { path: path, stroke: color }
			}
			
			$('.ts-skill-chart').find('.skill-chart-item').each(function(i){
				var t = $(this), 
					color = t.find('.color').val(),
					value = t.find('.percent').val(),
					text = t.find('.text').text();
				
				rad += 30;	
				var z = r.path().attr({ arc: [value, color, rad], 'stroke-width': 28 });
				
				z.mouseover(function(){
	                this.animate({ 'stroke-width': 50, opacity: .75 }, 1000, 'elastic');
	                if(Raphael.type != 'VML') //solves IE problem
					this.toFront();
					title.stop().animate({ opacity: 0 }, speed, '>', function(){
						this.attr({ text: text + '\n' + value + '%' }).animate({ opacity: 1 }, speed, '<');
					});
	            }).mouseout(function(){
					this.stop().animate({ 'stroke-width': 28, opacity: 1 }, speed*4, 'elastic');
					title.stop().animate({ opacity: 0 }, speed, '>', function(){
						title.attr({ text: defaultText }).animate({ opacity: 1 }, speed, '<');
					});	
	            });
			});
		});
		
	}
}
jQuery(function(){ 
    
   // if (jQuery('#ts-diagram').length > 0) {
//	   o.init();
//	}
//            
	if (jQuery('.ts-skill-diagram').length > 0) {
	   jQuery('.ts-skill-diagram').each(function(){
	       o.init(); 
	   });
	}
});