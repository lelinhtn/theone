<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

?>


<div class="wrap">
    <h2 class="tsm-page-title"><?php echo tsm_get_page_title(); ?></h2>
    
    <div class="tsm-content-wrap">
        
        <h1 class="title">TSM <i class='r'>C</i><i class='p'>o</i><i class='v'>l</i><i class='b'>o</i><i class='g'>r</i> <i class='y'>S</i><i class='o'>w</i><i class='r'>i</i><i class='p'>t</i><i class='v'>c</i><i class='b'>h</i><i class='g'>e</i><i class='y'>r</i></h1>
    
    	<!-- Theme Studio Mega Menu -->
    	<ul onClick="" class="ts-megamenu tsm-response-switch tsm-full-width">
    		<!-- Logo -->
    		<li class="tsm-logo"><a href="#"><img title="tsMegamenu logo image" src="<?php echo TSM_URL; ?>/img/ovicsoft-logo.png" alt=""></a></li>
    		<!-- /Logo -->
    
    		<!-- Simple Item -->
    		<li class="tsm-active"><a href="#"><i class="tsm-single-icon fa fa-home"></i></a></li>
    		<!-- /Simple Item -->
    
    		<!-- Simple Item -->
    		<li><a href="#">Simple</a></li>
    		<!-- /Simple Item -->
    
    		<!-- Multilevel Dropdown Menu -->
    		<li><a>Drop<i class="tsm-caret fa fa-angle-down"></i></a>
    			<ul class="w-200">
    				<li><a><i class="fa fa-picture-o"></i><i class="tsm-caret fa fa-angle-right"></i>Themeforest</a>
    					<ul class="w-200">
    						<li><a href="#"><i class="fa fa-tachometer"></i>WordPress</a></li>
    						<li><a href="#"><i class="fa fa-html5"></i>HTML</a></li>
    						<li><a href="#"><i class="fa fa-globe"></i>Marketing</a></li>
    						<li><a href="#"><i class="fa fa-cogs"></i>CMS</a></li>
    						<li><a href="#"><i class="fa fa-shopping-cart"></i>eCommerce</a></li>
    						<li><a href="#"><i class="fa fa-eye"></i>PSD</a></li>
    					</ul>
    				</li>
    				<li><a><i class="fa fa-code"></i><i class="tsm-caret fa fa-angle-right"></i>Codecanyon</a>
    					<ul class="w-200">
    						<li><a href="#"><i class="fa fa-css3"></i>CSS3</a></li>
    						<li><a href="#"><i class="fa fa-html5"></i>HTML5</a></li>
    						<li><a href="#"><i class="fa fa-code"></i>JavaScript</a></li>
    						<li><a href="#"><i class="fa fa-code"></i>PHP Scripts</a></li>
    					</ul>
    				</li>
    				<li><a><i class="fa fa-film"></i>Videohive</a></li>
    				<li><a><i class="fa fa-music"></i>Audiojungle</i></a></li>
    			</ul>
    		</li>
    		<!-- /Multilevel Dropdown Menu -->
    
    		<!-- Gallery -->
    		<li class="tsm-content-full"><a><i class="tsm-single-icon fa fa-picture-o"></i></a>
    			<div>
    				<h3>Images</h3>
    				<div class="tsm-row">
    					<div class="tsm-col c-4">
    						<img src="<?php echo TSM_URL; ?>/img/media-img-1.png" alt="">
    					</div>
    					<div class="tsm-col c-4">
    						<img src="<?php echo TSM_URL; ?>/img/media-img-2.png" alt="">
    					</div>
    					<div class="tsm-col c-4">
    						<img src="<?php echo TSM_URL; ?>/img/media-img-3.png" alt="">
    					</div>
    				</div>
    			</div>
    		</li>
    		<!-- /Gallery -->
    
    		<!-- Links -->
    		<li class="tsm-content-full"><a><i class="tsm-single-icon fa fa-link"></i></a>
    			<div>
    				<div class="tsm-row">
    					<div class="tsm-col c-3">
    						<ul>
    							<h3>Heading Title</h3>
    							<li><a href="#">HTML5</a></li>
    							<li><a href="#">CSS3</a></li>
    							<li><a href="#">JS</a></li>
    							<li><a href="#">PHP</a></li>
    							<li><a href="#">WordPress</a></li>
    							<li><a href="#">jQuery</a></li>
    						</ul>
    					</div>
    					<div class="tsm-col c-3">
    						<ul>
    							<h3>Heading Title</h3>
    							<li><a href="#">HTML5</a></li>
    							<li><a href="#">CSS3</a></li>
    							<li><a href="#">JS</a></li>
    							<li><a href="#">PHP</a></li>
    							<li><a href="#">WordPress</a></li>
    							<li><a href="#">jQuery</a></li>
    						</ul>
    					</div>
    					<div class="tsm-col c-3">
    						<ul>
    							<h3>Heading Title</h3>
    							<li><a href="#">HTML5</a></li>
    							<li><a href="#">CSS3</a></li>
    							<li><a href="#">JS</a></li>
    							<li><a href="#">PHP</a></li>
    							<li><a href="#">WordPress</a></li>
    							<li><a href="#">jQuery</a></li>
    						</ul>
    					</div>
    					<div class="tsm-col c-3">
    						<ul>
    							<h3>Heading Title</h3>
    							<li><a href="#">HTML5</a></li>
    							<li><a href="#">CSS3</a></li>
    							<li><a href="#">JS</a></li>
    							<li><a href="#">PHP</a></li>
    							<li><a href="#">WordPress</a></li>
    							<li><a href="#">jQuery</a></li>
    						</ul>
    					</div>
    				</div>
    			</div>
    		</li>
    		<!-- /Links -->
    
    		<!-- Content Dropdown + Grid -->
    		<li class="tsm-content-full tsm-grid"><a><i class="tsm-single-icon fa fa-align-justify"></i></a>
    			<div>
    				<div class="tsm-row">
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-11"><div class="tsm-grid-wrapper">
    						<center>Eleven</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-10"><div class="tsm-grid-wrapper">
    						<center>Ten</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-3"><div class="tsm-grid-wrapper">
    						<center>Three</center>
    					</div></div>
    					<div class="tsm-col c-9"><div class="tsm-grid-wrapper">
    						<center>Nine</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-8"><div class="tsm-grid-wrapper">
    						<center>Eight</center>
    					</div></div>
    					<div class="tsm-col c-4"><div class="tsm-grid-wrapper">
    						<center>Four</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-5"><div class="tsm-grid-wrapper">
    						<center>Five</center>
    					</div></div>
    					<div class="tsm-col c-7"><div class="tsm-grid-wrapper">
    						<center>Seven</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-4"><div class="tsm-grid-wrapper">
    						<center>Four</center>
    					</div></div>
    					<div class="tsm-col c-4"><div class="tsm-grid-wrapper">
    						<center>Four</center>
    					</div></div>
    					<div class="tsm-col c-4"><div class="tsm-grid-wrapper">
    						<center>Four</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    					<div class="tsm-col c-7"><div class="tsm-grid-wrapper">
    						<center>Seven</center>
    					</div></div>
    					<div class="tsm-col c-3"><div class="tsm-grid-wrapper">
    						<center>Three</center>
    					</div></div>
    				</div>
    				<div class="tsm-row">
    					<div class="tsm-col c-4"><div class="tsm-grid-wrapper">
    						<center>Four</center>
    					</div></div>
    					<div class="tsm-col c-1"><div class="tsm-grid-wrapper">
    						<center>One</center>
    					</div></div>
    					<div class="tsm-col c-5"><div class="tsm-grid-wrapper">
    						<center>Five</center>
    					</div></div>
    					<div class="tsm-col c-2"><div class="tsm-grid-wrapper">
    						<center>Two</center>
    					</div></div>
    				</div>
    			</div>
    		</li>
    		<!-- /Content Dropdown + Grid -->
    
    		<!-- Send Message -->
    		<li class="tsm-content tsm-right-align"><a><i class="tsm-single-icon fa fa-envelope"></i></a>
    			<div class="w-350">
    				<h3>Message</h3>
    				<form action="">
    					<input placeholder="Name" name="name" type="text">
    					<input placeholder="e-mail" name="email" type="text">
    					<textarea placeholder="Your message" name="message" rows="10"></textarea>
    					<input class="tsm-button" type="submit" value="Submit">
    				</form>
    			</div>
    		</li>
    		<!-- /Send Message -->
    
    		<!-- Search Place -->
    		<li class="tsm-search tsm-right-item">
    			<form action="">
    				<input id="search-1" name="search" type="search">
    				<label for="search-1" class="fa fa-search"></label>
    			</form>
    		</li>
    		<!-- /Search Place -->
    	</ul>
    	<!-- /Theme Studio Mega Menu -->
    	
        <?php
            tsm_choose_css_temp_megamenu();
        ?>
        
    </div><!-- .tsm-content-wrap -->

</div><!-- .wrap -->