<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Load admin javascript modules
 *
 * @package TSM
 * @since 1.0
 */
function tsm_admin_scripts() {
    global $tsmAdminMenus, $tsm_script_localize, $tsm_live_editor_options, $tsm_nonce, $tsm_live_preview_options;
    
    $screen = get_current_screen();
    
    if ( $screen->base == 'nav-menus' ):
    
        wp_enqueue_media();
        wp_enqueue_script( 'jquery-ui-core' );
        wp_enqueue_script( 'jquery-ui-tabs' );
    	wp_enqueue_script( 'jquery-ui-sortable' );
    	wp_enqueue_script( 'jquery-ui-draggable' );
    	wp_enqueue_script( 'jquery-ui-droppable' );
    	wp_enqueue_script( 'jquery-ui-resizable' );
        wp_enqueue_script( 'jquery-ui-autocomplete' );
        wp_enqueue_script( 'jquery-ui-slider' );
        wp_enqueue_script( 'jquery-ui-accordion' );
        
        wp_enqueue_script( 'jquery.easing', TSM_URL . '/js/jquery.easing.js', array( 'jquery' ), '1.3', true );
        
        // Chosen jquery
        wp_enqueue_script( 'chosen.jquery', TSM_URL . '/js/chosen.jquery.js', array( 'jquery' ), TSM_VERSION, true );
        
    	// Bootstrap JS components - Drop a custom build in your child theme's 'js' folder to override this one.
    	//wp_enqueue_script( 'bootstrap.min', TSM_URL . '/inc/bootstrap/js/bootstrap.min.js', array( 'jquery' ), TSM_VERSION, true );
        
        // Boostrap date picker
        wp_enqueue_script( 'bootstrap-datepicker', TSM_URL . '/js/bootstrap-datepicker.js', false, TSM_VERSION, true );
        
        // Magnific popup
        wp_enqueue_script( 'jquery.magnific-popup', TSM_URL . '/js/jquery.magnific-popup.js', array( 'jquery' ), TSM_VERSION, true );
        
        // Get, set css of selectors
        //wp_enqueue_script( 'jss.min', TSM_URL . '/js/jss.min.js', array( 'jquery' ), TSM_VERSION, true );
        
        // Live css editor
        //wp_enqueue_script( 'microtpl', TSM_URL . '/live-css-editor/js/microtpl.js', array( 'jquery' ), TSM_VERSION, false );
        //wp_enqueue_script( 'jquery.livecsseditor', TSM_URL . '/live-css-editor/js/jquery.livecsseditor.js', array( 'jquery' ), TSM_VERSION, false );
        //wp_enqueue_script( 'lce.editors', TSM_URL . '/live-css-editor/js/lce.editors.js', array( 'jquery' ), TSM_VERSION, false );
        //wp_enqueue_script( 'colorpicker-bootstrap', TSM_URL . '/live-css-editor/plugins/colorpicker/js/colorpicker-bootstrap.js', array( 'jquery' ), TSM_VERSION, false );
        //wp_enqueue_script( 'bootstrap.min', TSM_URL . '/live-css-editor/plugins/bootstrap/js/bootstrap.min.js', array( 'jquery' ), TSM_VERSION, false );
        
        wp_enqueue_script( 'tsm-backend-jquery', TSM_URL . '/js/tsm-backend-jquery.js', array( 'jquery' ), TSM_VERSION, true );
        
        wp_localize_script( 'tsm-backend-jquery', 'tsm_nonce', $tsm_nonce );
        
        // Script Localize
        wp_localize_script( 'tsm-backend-jquery', 'tsm_script_localize', $tsm_script_localize );
    
    endif;
    
    // Load preview via ajax
    if ( $screen->base == 'nav-menus' ):
        
        $menu_id = tsm_get_selected_menu_id();
        $menu_id = max( 0, intval( $menu_id ) );
        
        if ( $menu_id > 0 ):
            
            //wp_enqueue_script( 'tsm-preview-form', admin_url( 'admin-ajax.php' ) . '?action=tsm_load_tsm_preview_form_via_ajax&menu_id=' . $menu_id, array( 'jquery' ), TSM_VERSION, true );
            
        endif;    
        
        //wp_enqueue_script( 'tsm-script', admin_url( 'admin-ajax.php' ) . '?action=tsm_load_tsm_preview_form_via_ajax', array( 'jquery' ), TSM_VERSION, true );
        
    endif;
    
    if ( $screen->base == 'nav-menus' || $screen->base == 'toplevel_page_tsm-options' ):
        
        wp_enqueue_script( 'tsm-validate', TSM_URL . '/js/tsm-validate.js', array( 'jquery' ), TSM_VERSION, true );
        wp_enqueue_script( 'less.min', TSM_URL . '/js/less.min.js', array( 'jquery' ), TSM_VERSION, true );
        wp_enqueue_script( 'spectrum', TSM_URL . '/js/spectrum.js', array( 'jquery' ), TSM_VERSION, true );
        wp_enqueue_script( 'jszip.min', TSM_URL . '/js/jszip.min.js', array( 'jquery' ), TSM_VERSION, true );
        wp_enqueue_script( 'colorSwitcher', TSM_URL . '/js/colorSwitcher.js', array( 'jquery' ), TSM_VERSION, true );
        
    endif;

    // Script for TSM options
    if ( $screen->base == 'toplevel_page_tsm-options' ):
        
        wp_localize_script( 'colorSwitcher', 'tsm_nonce', $tsm_nonce );
        wp_localize_script( 'colorSwitcher', 'tsm_script_localize', $tsm_script_localize );
        
    endif;
}
add_action( 'admin_enqueue_scripts', 'tsm_admin_scripts', 9999 );

/**
 *  Load frontend script 
 *  @since 1.0
 **/
function tsm_frontend_scripts() {
    
    //wp_enqueue_script( 'jquery' );
    
    if ( !is_admin() ) {
        wp_enqueue_script( 'jquery.min', TSM_URL . '/js/jquery.min.js', false, TSM_VERSION, true );
        wp_enqueue_script( 'tsm.jquery', TSM_URL . '/js/tsm.jquery.js', array( 'jquery.min' ), TSM_VERSION, true );
        wp_enqueue_script( 'tsm-frontend-jquery', TSM_URL . '/js/tsm-frontend-jquery.js', array( 'jquery' ), TSM_VERSION, true );
        
        wp_enqueue_script( 'tsm-script', admin_url( 'admin-ajax.php' ) . '?action=tsm_enqueue_script_via_ajax', array( 'jquery' ), TSM_VERSION, true );   
    }
    
    
}
add_action( 'wp_enqueue_scripts', 'tsm_frontend_scripts', 9999 );


/**
 *  Enqueue mega menu script via ajax
 *  @since 1.0
 **/
function tsm_enqueue_script_via_ajax() {
    
    $locations = get_nav_menu_locations();
    
    header( 'Content-type: text/javascript; charset: UTF-8' );
    
    if ( !empty( $locations ) ):
        $i = 1;
        foreach ( $locations as $location => $menu_id ):
            
            $mega_menu_settings = ( array ) get_post_meta( $menu_id, 'tsm_mega_menu_settings', true );
            $is_enable_mega_menu = ( isset( $mega_menu_settings['enable_mega_menu'] ) ) ? $mega_menu_settings['enable_mega_menu'] == 'yes' : false;
            
            if ( is_array( $mega_menu_settings ) && !empty( $mega_menu_settings ) && $is_enable_mega_menu ):
                
                $mega_menu_settings_default = array(
                    'enable_mega_menu'  =>  'no',
                    'menu_css_data'     =>  array(),
                    'menu_css_frontend' =>  '',
                    'breakpoint'        =>  768,
                    'zindex'            =>  101,
                    'position'          =>  'top',
                    'showon'            =>  'hover',
                    'responsive'        =>  'switch',
                    'fullwidth'         =>  'yes',
                    'sticky'            =>  'no',
                    'fixed'             =>  'no',
                    'effect'            =>  'fade',     // Old version
                    'easing'            =>  'linear',   // Old version
                    'speed'             =>  500,        // Old version
                    'delay'             =>  0,          // Old version
                    'show_effect'       =>  'zoomIn',   // Modern version
                    'show_speed'        =>  500,        // Modern version
                    'show_delay'        =>  0,          // Modern version
                    'hide_effect'       =>  'fade',     // Modern version
                    'hide_speed'        =>  500,        // Modern version
                    'hide_delay'        =>  0           // Modern version
                );
                
                $mega_menu_settings = array_merge( $mega_menu_settings_default, $mega_menu_settings );
                
                $fullwidth = ( $mega_menu_settings['fullwidth'] == 'yes' ) ? 'true' : 'false';
                $sticky = ( $mega_menu_settings['sticky'] == 'yes' ) ? 'true' : 'false';
                $fixed = ( $mega_menu_settings['fixed'] == 'yes' ) ? 'true' : 'false';
                
                /* Old version
                $megamenu_script = '
                        jQuery(document).ready(function($){
                            "use strict";
                            
                            $(".tsm-menu-wrap-' . $location . '-' . $menu_id . ' > .ts-megamenu").tsMegamenu({
                                position:   "' . $mega_menu_settings['position'] . '",
                                showOn:     "' . $mega_menu_settings['showon'] . '",
                                responsive: "' . $mega_menu_settings['responsive'] . '",
                                fullWidth:  ' . $fullwidth . ',
                                sticky:     ' . $sticky . ',
                                fixed:      ' . $fixed . ',
                                effect:     {
                                            name:   "' . $mega_menu_settings['effect'] . '",
                                            easing: "' . $mega_menu_settings['easing'] . '",
                                            speed:  ' . $mega_menu_settings['speed'] . ',
                                            delay:  ' . $mega_menu_settings['delay'] . '
                                }
                            });
                            
                        });';
                */
                
                
                $megamenu_script = '$(".tsm-menu-wrap-' . $location . '-' . $menu_id . ' > .ts-megamenu").tsMegamenu({
                    position:   "' . $mega_menu_settings['position'] . '",
                    showOn:     "' . $mega_menu_settings['showon'] . '",
                    responsive: "' . $mega_menu_settings['responsive'] . '",
                    fullWidth:  ' . $fullwidth . ',
                    sticky:     ' . $sticky . ',
                    fixed:      ' . $fixed . ',
                    responsiveClick: true,
                    breakpoint: ' . $mega_menu_settings['breakpoint'] . ',
                    showEffect:     {
                        name:   "' . $mega_menu_settings['show_effect'] . '",
                        speed:  ' . $mega_menu_settings['show_speed'] . ',
                        delay:  ' . $mega_menu_settings['show_delay'] . '
                    },
                    hideEffect:     {
                        name:   "' . $mega_menu_settings['hide_effect'] . '",
                        speed:  ' . $mega_menu_settings['hide_speed'] . ',
                        delay:  ' . $mega_menu_settings['hide_delay'] . '
                    }
                });';
                
                $script = '
                        jQuery(document).ready(function($){
                            "use strict";
                            
                            tsm_update_megamenu();
                            function tsm_update_megamenu(){
                                ' . $megamenu_script . '
                            }
                            
                        });';
                    
                echo $script;
                
            endif;
        
        endforeach;
        
    endif;
    
    wp_die();
}
add_action( 'wp_ajax_tsm_enqueue_script_via_ajax', 'tsm_enqueue_script_via_ajax' );
add_action( 'wp_ajax_nopriv_tsm_enqueue_script_via_ajax', 'tsm_enqueue_script_via_ajax' );






