<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if( !function_exists( 'wp_get_current_user' ) ) {
    include( ABSPATH . 'wp-includes/pluggable.php' ); 
}

$current_user = wp_get_current_user();

/** Nonce for security check **/
global $tsm_nonce;
$tsm_nonce = array(
    'ajax_nonce'  =>  wp_create_nonce( 'tsm-ajax-nonce' )
);

// Load defines
require_once TSM_DIR . '/inc/define.php';

// Load google fonts
require_once TSM_DIR . '/inc/google-fonts.php';

// Load supported elements
require_once TSM_DIR . '/inc/elements.php';

// Load defaul color schemes args
require_once TSM_DIR . '/inc/default-color-schemes.php';

// Load style manager
require_once TSM_DIR . '/inc/style-manager.php';

// Load live css editor options
require_once TSM_DIR . '/inc/live-css-editor-manager.php';

// Load functions
require_once TSM_DIR . '/inc/functions.php';

// Load font icons list chooser
require_once TSM_DIR . '/inc/font-icons-list-chooser.php';

// Load shortcode
require_once TSM_DIR . '/inc/shortcode.php';

// Load css list
require_once TSM_DIR . '/inc/tsm-css-list.php'; 

// Load mega menu manager
require_once TSM_DIR . '/inc/menu-manager.php';

// Load walker frontend
require_once TSM_DIR . '/classes/walkerFrontendClass.php';

// Load admin menus
//require_once TSM_DIR . '/inc/admin-menus.php'; 

// Load ajax files tương ứng với menu slug 
require_once TSM_DIR . '/inc/load-menu-pages-functions.php';

// Load localize translate for scripts
require_once TSM_DIR . '/inc/localize-scripts.php';

// Load scripts
require_once TSM_DIR . '/inc/scripts.php';

// Load stylesheets
require_once TSM_DIR . '/inc/stylesheets.php';



