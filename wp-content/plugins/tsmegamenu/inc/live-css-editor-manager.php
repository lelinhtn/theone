<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


global $tsm_live_editor_options;

$height_args = array(
    'title'     =>  __( 'Height', 'tsm' ),
    'type'      =>  'height',
    'selected'  =>  false,
    'important' =>  false
);

$width_args = array(
    'title'     =>  __( 'Width', 'tsm' ),
    'type'      =>  'width',
    'selected'  =>  false,
    'important' =>  false
);

$bg_color_agrs = array(
    'title'     =>  __( 'Background Color', 'tsm' ),
    'type'      =>  'color',
    'selected'  =>  false,
    'important' =>  false
);

$padding_args = array( // top, right, bottom, left
    'title'     =>  __( 'Padding', 'tsm' ),
    'type'      =>  'padding',
    'selected'  =>  false,
    'important' =>  false
);

$margin_args = array( // top, right, bottom, left
    'title'     =>  __( 'Margin', 'tsm' ),
    'type'      =>  'margin',
    'selected'  =>  false,
    'important' =>  false
);

$color_agrs = array(
    'title'     =>  __( 'Color', 'tsm' ),
    'type'      =>  'color',
    'selected'  =>  false,
    'important' =>  false
);

$border_top_args = array(
    'title'     =>  __( 'Boder Top', 'tsm' ),
    'type'      =>  'border',
    'selected'  =>  false,
    'important' =>  false
);

$border_bottom_args = array(
    'title'     =>  __( 'Boder Bottom', 'tsm' ),
    'type'      =>  'border',
    'selected'  =>  false,
    'important' =>  false
);

$border_left_args = array(
    'title'     =>  __( 'Boder Left', 'tsm' ),
    'type'      =>  'border',
    'selected'  =>  false,
    'important' =>  false
);

$border_right_args = array(
    'title'     =>  __( 'Boder Right', 'tsm' ),
    'type'      =>  'border',
    'selected'  =>  false,
    'important' =>  false
);

$font_family_args = array(
    'title'     =>  __( 'Font Family', 'tsm' ),
    'type'      =>  'font-family',
    'selected'  =>  false,
    'important' =>  false
);
$font_size_args =  array(
    'title'     =>  __( 'Font Size', 'tsm' ),
    'type'      =>  'font-size',
    'selected'  =>  false,
    'important' =>  false
);

$box_shadow_args =  array(
    'title'     =>  __( 'Box Shadow', 'tsm' ),
    'type'      =>  'box-shadow',
    'selected'  =>  false,
    'important' =>  false
);


$tsm_live_editor_options = array(
    'main'      =>  array(
        'title'         =>  __( 'Main', 'tsm' ),
        'selector'      =>  '{prefix} ul.ts-megamenu',
        'avai_props'    =>  array(
            'height'    =>  $height_args,
            'background-color'  =>  $bg_color_agrs,
            'color'  => $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args
        )
    ),
    'drop'  =>  array(
        'title'         =>  __( 'Dropdown', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li.tsm-content > div, {prefix} .ts-megamenu > li.tsm-content-full > div, {prefix} .ts-megamenu > li > ul, {prefix} .ts-megamenu > li > ul ul, {prefix} .ts-megamenu .tsm-multi-column, {prefix} .ts-megamenu .tsm-multi-column > ul ul',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'box-shadow' => $box_shadow_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'main_li' =>  array(
        'title'         =>  __( 'Main Items', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'main_li_hover' =>  array(
        'title'         =>  __( 'Main Items:hover:active', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li:hover, {prefix} .ts-megamenu > li.tsm-active, {prefix} .ts-megamenu > li.tsm-opened',
        'avai_props'    =>  array(
            'background-color'  =>  array(
                'title'     =>  __( 'Background Color', 'tsm' ),
                'type'      =>  'color',
                'selected'  =>  false,
                //'inherit_selector'  =>  '#tsm-main_item_hover_bg_color',
                'important' =>  false
            ),
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'main_li_a' =>  array(
        'title'         =>  __( 'Main Items Link', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > a, {prefix} .ts-megamenu > li > a:visited',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'color'  =>  $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'main_li_a_hover' =>  array(
        'title'         =>  __( 'Main Items Link:hover', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > a:hover, {prefix} .ts-megamenu > li > a.tsm-link-hover',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'color'  =>  $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'main_links' =>  array(
        'title'         =>  __( 'Main Links', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > a, {prefix} .ts-megamenu a:visited',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'color'  =>  $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args
        )
    ),
    'drop_li' =>  array(
        'title'         =>  __( 'Dropdown Item', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu ul > li',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'drop_li_hover' =>  array(
        'title'         =>  __( 'Dropdown Item:hover', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > ul li:hover, {prefix} .ts-megamenu > li > ul ul li:hover, {prefix} .ts-megamenu .tsm-multi-column li:hover, {prefix} .ts-megamenu .tsm-multi-column > ul ul li:hover, {prefix} .ts-megamenu > ul ul li.tsm-li-hover',
        'avai_props'    =>  array(
            'background-color'  =>  array(
                'title'     =>  __( 'Background Color', 'tsm' ),
                'type'      =>  'color',
                'selected'  =>  false,
                'inherit_selector'  =>  '#tsm-drop_item_hover_bg_color',
                'important' =>  false
            ),
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'drop_li_first' =>  array(
        'title'         =>  __( 'Dropdown Item:first-child', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu ul > li:first-child',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'font-family'  =>  $font_family_args,
            'font-size'  =>  $font_size_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'drop_li_last' =>  array(
        'title'         =>  __( 'Dropdown Item:last-child', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu ul > li:last-child',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'font-family'  =>  $font_family_args,
            'font-size'  =>  $font_size_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'drop_li_last_links' =>  array(
        'title'         =>  __( 'Dropdown Item:last-child Links', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > ul li:last-child > a, {prefix} .ts-megamenu > li > ul li:last-child > a:visited, {prefix} .ts-megamenu > li > ul ul li:last-child > a, {prefix} .ts-megamenu > li > ul ul li:last-child > a:visited, {prefix} .ts-megamenu .tsm-multi-column li:last-child > a, {prefix} .ts-megamenu .tsm-multi-column li:last-child > a:visited, {prefix} .ts-megamenu .tsm-multi-column > ul ul li:last-child > a, {prefix} .ts-megamenu .tsm-multi-column > ul ul li:last-child > a:visited',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'color'  =>  $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'drop_links' =>  array(
        'title'         =>  __( 'Dropdown Links', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > ul li > a, {prefix} .ts-megamenu > li > ul li > a:visited, {prefix} .ts-megamenu > li > ul ul li > a, {prefix} .ts-megamenu > li > ul ul li > a:visited, {prefix} .ts-megamenu .tsm-multi-column li > a, {prefix} .ts-megamenu .tsm-multi-column li > a:visited, {prefix} .ts-megamenu .tsm-multi-column > ul ul li > a, {prefix} .ts-megamenu .tsm-multi-column > ul ul li > a:visited',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'color'  =>  $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
    'drop_li_links_hover' =>  array(
        'title'         =>  __( 'Dropdown Item Links:hover', 'tsm' ),
        'selector'      =>  '{prefix} .ts-megamenu > li > ul li > a.tsm-link-hover, {prefix} .ts-megamenu > li > ul li > a:visited.tsm-link-hover, {prefix} .ts-megamenu > li > ul ul li > a.tsm-link-hover, {prefix} .ts-megamenu > li > ul ul li > a:visited.tsm-link-hover, {prefix} .ts-megamenu .tsm-multi-column li > a.tsm-link-hover, {prefix} .ts-megamenu .tsm-multi-column li > a:visited.tsm-link-hover, {prefix} .ts-megamenu .tsm-multi-column > ul ul li > a.tsm-link-hover, {prefix} .ts-megamenu .tsm-multi-column > ul ul li > a:visited.tsm-link-hover, 
                            {prefix} .ts-megamenu > li > ul li > a:hover, {prefix} .ts-megamenu > li > ul li > a:visited:hover, {prefix} .ts-megamenu > li > ul ul li > a:hover, {prefix} .ts-megamenu > li > ul ul li > a:visited:hover, {prefix} .ts-megamenu .tsm-multi-column li > a:hover, {prefix} .ts-megamenu .tsm-multi-column li > a:visited:hover, {prefix} .ts-megamenu .tsm-multi-column > ul ul li > a:hover, {prefix} .ts-megamenu .tsm-multi-column > ul ul li > a:visited:hover',
        'avai_props'    =>  array(
            'background-color'  =>  $bg_color_agrs,
            'color'  =>  $color_agrs,
            'padding'  =>  $padding_args,
            'margin'  =>  $margin_args,
            'border-top'  =>  $border_top_args,
            'border-right'  =>  $border_right_args,
            'border-bottom'  =>  $border_bottom_args,
            'border-left'  =>  $border_left_args
        )
    ),
);

/**
 *  Insert live css selector prefix
 *  @since 1.0 
 **/
function tsm_insert_live_css_selector_prefix( $prefix ){
    global $tsm_live_editor_options;
    
    if ( !empty( $tsm_live_editor_options ) ):
        $temp = $tsm_live_editor_options;
        foreach ( $temp as $key => $args ):
            
            if ( isset( $args['selector'] ) ):
                
                $tsm_live_editor_options[$key]['selector'] = str_replace( '{prefix}', $prefix, $args['selector'] );
                
            endif;
            
        endforeach;
    
    endif;
    
    return $tsm_live_editor_options;
}

/**
 *  Live CSS Editor control via ajax
 *  @since 1.0 
 **/
function tsm_live_css_editor_control_via_ajax(){
    global $tsm_live_editor_options;
    
	$errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
 
 	if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'you do not have enough permissions to edit menus!', 'tsm' );
        
    endif;
    
    $html = '';
    
    if ( empty( $errors ) ):
        
        $menu_id = $_POST['menu_id'];
        tsm_insert_live_css_selector_prefix( '.tsm-menu-wrap-' . $menu_id );
        $menu_settings = get_post_meta( $menu_id, 'tsm_mega_menu_settings', true );
        $tsm_live_editor_settings = array();
        if ( isset( $menu_settings['live_css_editor_settings'] ) ):
            
            $tsm_live_editor_settings = $menu_settings['live_css_editor_settings'];
            $tsm_live_editor_options = tsm_array_merge_recursive_replaces( $tsm_live_editor_options, $tsm_live_editor_settings );

        endif;
        
        if ( !empty( $tsm_live_editor_options ) ):
            
            $section_html = '';
            foreach ( $tsm_live_editor_options as $key => $args ):
                
                $props_dropdown_html = '';
                
                if ( isset( $args['avai_props'] ) ):
                    
                    $props_dropdown_html .= '<select data-placeholder="' . __( 'Select Style Properties', 'tsm' ) . '" multiple="multiple" class="tsm-props-select tsm-select tsm-select2">';
                    
                    foreach ( $args['avai_props'] as $prop => $prop_info ):
                        $data_important = ( $prop_info['important'] ) ? 'yes': 'no';
                        $inherit_selector = ( isset( $prop_info['inherit_selector'] ) ) ? $prop_info['inherit_selector'] : '';
                        $selected = ( $prop_info['selected'] === true || $prop_info['selected'] === 'true' ) ? 'selected="selected"': '';
                        $props_dropdown_html .= '<option data-inherit-selector="' . $inherit_selector . '" data-type="' . $prop_info['type'] . '" data-important="' . $data_important . '" ' . $selected . ' value="' . $prop . '">' . sanitize_text_field( $prop_info['title'] ) . '</option>';
                        
                    endforeach;
                    
                    $props_dropdown_html .= '</select><!-- .tsm-props-select -->';
                    
                endif;
                
                $section_html .=    '<h3>' . $args['title'] . '</h3>';
                $section_html .=    '<div data-section-key="' . $key . '" data-selector="' . $args['selector'] . '" class="tsm-control-section">
                                        ' . $props_dropdown_html . '
                                        <div class="tsm-live-css-control-form-wrap"></div>
                                        <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                        <a href="#" class="tsm-update-live-css-preview-btn tsm-updated tsm-btn button"><i class="fa fa-check"></i>' . __( 'Update Preview CSS', 'tsm' ) . '</a>
                                    </div>';
                
            endforeach;
            
            $html .= '<div class="tsm-live-editor-control-accordion tsm-accordion">';
            $html .= $section_html;
            $html .= '</div>';
            
        endif;
    
    endif;
    
    echo $html;
    tsm_display_note( $errors );
    
    die();
}
add_action( 'wp_ajax_tsm_live_css_editor_control_via_ajax', 'tsm_live_css_editor_control_via_ajax' );


/**
 *  Live preview base html
 *  @since 1.0 
 **/
function tsm_live_preview_base_html( $echo = true ) {
    
    $html = '<div id="tsm-live-preview-wrap" class="tsm-live-preview-wrap">
                <div id="tsm-live-preview-header" class="container-fluid tsm-live-preview-header">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-8">
                            <h2>' . __( 'Mega Menu Live CSS Editor', 'tsm' ) . '<a href="#" class="tsm-reload-preview-iframe" title="' . __( 'Reload preview menu', 'tsm' ) . '"><i class="fa fa-refresh fa-spin"></i></a></h2>
                        </div>
                        <div class="tsm-action-wrap col-xs-12 col-sm-5 col-md-4">
                            <a href="#" class="button tsm-btn tsm-close-preview"><i class="fa fa-long-arrow-left"></i>' . __( 'Back To Menu Editor', 'tsm' ) . '</a>
                            <a href="#" class="button tsm-btn tsm-show-hide-control"><i class="fa fa-arrow-circle-right"></i>' . __( 'Hidden Accordion', 'tsm' ) . '</a>
                        </div>
                    </div>
                </div><!-- .container-fluid -->
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                <div id="tsm-live-preview" class="col-xs-12 col-sm-7 col-md-8">
                    <iframe src="" frameborder="0" class="tsm-iframe"></iframe>
                </div><!-- #tsm-live-preview -->
                <div id="tsm-live-css-control" class="col-xs-12 col-sm-5 col-md-4"></div>
            </div><!-- #tsm-live-preview-wrap -->'; 
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
}

/**
 *  Live CSS Editor control section html form
 *  @since 1.0 
 *  @return Array of form html
 **/
function tsm_live_css_control_section_form(){
    
    $suported_css_props = array(
        'height'        =>  array(
            'title'     =>  __( 'Height', 'tsm' ),
            'type'      =>  'height'
        ),
        'width'        =>  array(
            'title'     =>  __( 'Width', 'tsm' ),
            'type'      =>  'width'
        ),
        'background-color'  =>  array(
            'title'     =>  __( 'Background Color', 'tsm' ),
            'type'      =>  'color'
        ),
        'color'  =>  array(
            'title'     =>  __( 'Color', 'tsm' ),
            'type'      =>  'color'
        ),
        'font-size'     =>  array(
            'title'     =>  __( 'Font Size', 'tsm' ),
            'type'      =>  'font-size'
        ),
        'font-family'     =>  array(
            'title'     =>  __( 'Font Family', 'tsm' ),
            'type'      =>  'font-family'
        ),
        'padding'  =>  array( // top, right, bottom, left
            'title'     =>  __( 'Padding', 'tsm' ),
            'type'      =>  'padding'
        ),
        'margin'  =>  array( // top, right, bottom, left
            'title'     =>  __( 'Margin', 'tsm' ),
            'type'      =>  'margin'
        ),
        'border-top'  =>  array(
            'title'     =>  __( 'Boder Top', 'tsm' ),
            'type'      =>  'border'
        ),
        'border-right'  =>  array(
            'title'     =>  __( 'Boder Right', 'tsm' ),
            'type'      =>  'border'
        ),
        'border-bottom'  =>  array(
            'title'     =>  __( 'Boder Bottom', 'tsm' ),
            'type'      =>  'border'
        ),
        'border-left'  =>  array(
            'title'     =>  __( 'Boder Left', 'tsm' ),
            'type'      =>  'border'
        ),
        'box-shadow'    =>  array(
            'title'     =>  __( 'Box Shadow', 'tsm' ),
            'type'      =>  'box-shadow'
        )
    );
    
    $html_args = array();
    foreach ( $suported_css_props as $prop => $prop_info ):
        
        $input_html = '';
        switch ( $prop_info['type'] ):
            
            case 'color':
                $input_html .= '<input data-color="" data-prop-type="' . $prop_info['type'] . '" value="" type="text" class="color tsm-prop-val tsm-prop-color tsm-color-picker" />';
                break;
            case 'font-size':
                $input_html .= '<input data-prop-type="' . $prop_info['type'] . '" value="" type="text" class="tsm-prop-val tsm-prop-font-size" />';
                break;
            case 'font-family':
                //$input_html .= '<input data-prop-type="' . $prop_info['type'] . '" value="" type="text" class="tsm-prop-val tsm-prop-font-family" />';
                $input_html .= tsm_google_fonts_select( '', '', 'tsm-prop-val tsm-prop-font-family', 'data-prop-type="' . $prop_info['type'] . '" data-placeholder="' . __( 'Select Font', 'tsm' ) . '"', false );
                break;
            case 'padding': case 'margin':
                $table_html = '';
                $cols_num = 4;
                
                $table_html .= '<table class="tsm-table-input tsm-table-noborder">
                                    <thead></thead>
                                    <tbody>
                                        <tr>';
                                        
                for ( $i = 0; $i < $cols_num; $i++ ):
                    // top, right, bottom, left in pixel
                    $pos = '';
                    switch ( $i ):
                        case 0:
                            $pos = 'top';
                            break;
                        case 1:
                            $pos = 'right';
                            break;
                        case 2:
                            $pos = 'bottom';
                            break;
                        case 3:
                            $pos = 'left';
                            break;
                    endswitch;
                    $table_html .= '<td>
                                        <p>
                                            <span>' . ucfirst( $pos ) . '</span><br />
                                            <input type="text" data-prop-type="' . $prop_info['type'] . '" value="" data-pos="' . $pos . '" class="tsm-input tsm-prop-val tsm-prop-' . $prop_info['type'] . '" />
                                        </p>
                                    </td>';
                    
                endfor;
                
                $table_html .= '            </tr>
                                        </tbody>
                                    <tfoot></tfoot>
                                </table>';
                
                $input_html .= $table_html;
                break; 
            
            case 'border':
                    
                    $style_html = '<p><span>' . __( 'Border Style', 'tsm' ) . '</span><br />';
                    $style_html .= '<select data-width="90px" data-prop-type="' . $prop_info['type'] . '-style" data-placeholder="' . __( 'Select Border Style', 'tsm' ) . '" class="tsm-select tsm-select2 tsm-select-border-style tsm-prop-val tsm-prop-' . $prop_info['type'] . '-style">';
                    $style_html .= '<option value="none">none</option>
                                    <option value="hidden">hidden</option>
                                    <option value="dotted">dotted</option>
                                    <option value="dashed">dashed</option>
                                    <option value="solid">solid</option>
                                    <option value="double">double</option>
                                    <option value="groove">groove</option>
                                    <option value="ridge">ridge</option>
                                    <option value="inset">inset</option>
                                    <option value="outset">outset</option>
                                    <option value="initial">initial</option>
                                    <option value="inherit">inherit</option>';
                    $style_html .= '</select>';
                    $style_html .= '</p>';
                    
                    $color_html =   '<p><span>' . __( 'Border Color', 'tsm' ) . '</span><br />
                                        <input data-color="" data-prop-type="' . $prop_info['type'] . '-color" value="" type="text" class="color tsm-prop-val tsm-prop-' . $prop_info['type'] . '-color tsm-color-picker" />
                                    </p>';
                    
                    $width_html =   '<p><span>' . __( 'Border Width', 'tsm' ) . '</span><br />
                                        <input type="text" data-prop-type="' . $prop_info['type'] . '-width" value="" class="tsm-input tsm-prop-val tsm-prop-' . $prop_info['type'] . '-width" />
                                    </p>';
                    
                    $table_html =   '<table class="tsm-table-input tsm-table-noborder">
                                        <thead></thead>
                                        <tbody>
                                            <tr>';
                    $table_html .=              '<td>' . $style_html . '</td>' . '<td>' . $color_html . '</td>' . '<td>' . $width_html . '</td>';
                    $table_html .=          '</tr>
                                        </tbody>
                                        <tfoot></tfoot>
                                    </table>';
                    
                    $input_html .= $table_html;
                    
                break;
            
            default:
                $input_html .= '<input data-prop-type="' . $prop_info['type'] . '" value="" type="text" class="tsm-input tsm-prop-val tsm-prop-' . $prop_info['type'] . '" />';
                break;
            
        endswitch;
        
        $html_args[$prop] = '<div data-prop="' . $prop . '" class="tsm-live-css-control-section-form tsm-need-refresh">
                                <h4 class="tsm-secondary-title">' . sanitize_text_field( $prop_info['title'] ) . '</h4>
                                ' . $input_html . '
                            </div>';
        
    endforeach;
    
    return $html_args;
}

