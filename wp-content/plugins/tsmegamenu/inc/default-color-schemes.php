<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

global $tsm_color_schemes_list, $tsm_primary_colors;

$tsm_primary_colors = array(
    'blue'  =>  array(
        'primary_color' =>  '#3598DC',
        'name'          =>  'blue',
        'display_name'  =>  __( 'Blue', 'tsm' )
    ),
    'green'  =>  array(
        'primary_color' =>  '#2FCC71',
        'name'          =>  'green',
        'display_name'  =>  __( 'Green', 'tsm' )
    ),
    'orange'  =>  array(
        'primary_color' =>  '#D55401',
        'name'          =>  'orange',
        'display_name'  =>  __( 'Orange', 'tsm' )
    ),
    'purple'  =>  array(
        'primary_color' =>  '#9C59B8',
        'name'          =>  'purple',
        'display_name'  =>  __( 'Purple', 'tsm' )
    ),
    'red'  =>  array(
        'primary_color' =>  '#E84C3D',
        'name'          =>  'red',
        'display_name'  =>  __( 'Red', 'tsm' )
    ),
    'yellow'  =>  array(
        'primary_color' =>  '#F1C40F',
        'name'          =>  'yellow',
        'display_name'  =>  __( 'Yellow', 'tsm' )
    ),
    'pink'  =>  array(
        'primary_color' =>  '#FF4062',
        'name'          =>  'pink',
        'display_name'  =>  __( 'Pink', 'tsm' )
    ),
    'bamboo'  =>  array(
        'primary_color' =>  '#EACC52',
        'name'          =>  'bamboo',
        'display_name'  =>  __( 'Bamboo', 'tsm' )
    ),
    'emberglow'  =>  array(
        'primary_color' =>  '#FE846D',
        'name'          =>  'emberglow',
        'display_name'  =>  __( 'Emberglow', 'tsm' )
    ),
    'phlox'  =>  array(
        'primary_color' =>  '#854083',
        'name'          =>  'phlox',
        'display_name'  =>  __( 'Phlox', 'tsm' )
    ),
    'honeysuckle'  =>  array(
        'primary_color' =>  '#F47C98',
        'name'          =>  'honeysuckle',
        'display_name'  =>  __( 'Honeysuckle', 'tsm' )
    ),
    'cedar'  =>  array(
        'primary_color' =>  '#AAAC7D',
        'name'          =>  'cedar',
        'display_name'  =>  __( 'Cedar', 'tsm' )
    ),
    'teal'  =>  array(
        'primary_color' =>  '#015B64',
        'name'          =>  'teal',
        'display_name'  =>  __( 'Teal', 'tsm' )
    ),
    'coffee'  =>  array(
        'primary_color' =>  '#856A57',
        'name'          =>  'coffee',
        'display_name'  =>  __( 'Coffee', 'tsm' )
    ),
    'nougat'  =>  array(
        'primary_color' =>  '#D6BCA1',
        'name'          =>  'nougat',
        'display_name'  =>  __( 'Nougat', 'tsm' )
    ),
    'quarry'  =>  array(
        'primary_color' =>  '#9FBCC2',
        'name'          =>  'quarry',
        'display_name'  =>  __( 'Quarry', 'tsm' )
    )
); 

$base_color_scheme_dark_default = array(
    'first'     =>  '#354A5F',
    'second'    =>  '#3598DC',
    'third'     =>  '#fff',
    'fourth'    =>  '#283847'
);

$base_color_scheme_default = array(
    'first'     =>  '#354A5F',
    'second'    =>  '#3598DC',
    'third'     =>  '#fff',
    'fourth'    =>  '#EAEAEA'
);

$base_color_scheme_impressive = array(
    'first'     =>  '#fff',
    'second'    =>  '#EEEEEE',
    'third'     =>  '#555',
    'fourth'    =>  '#3598DC',
    'six'       =>  'BBBBBB'
);


/**
 *  Gen color scheme from base
 *  @since 1.0 
 **/
function tsm_init_color_scheme_args( $base_colors = array(), $base_main_color_key = '' ){
    global $tsm_primary_colors;
    
    $color_scheme = array();
    
    if ( !is_array( $base_colors ) ):
        
        return $color_scheme;
        
    endif;
    
    if ( empty( $base_colors ) || $base_main_color_key == '' ):
        
        return $color_scheme;
        
    endif;
    
    $color_scheme = array();
    
    foreach ( $tsm_primary_colors as $key => $primary_color_info ):
        
        $base_colors[$base_main_color_key] = $primary_color_info['primary_color'];
        
        foreach ( $primary_color_info as $info_key => $info_val ):
            
            $color_scheme[$key][$info_key] = $info_val;
            
        endforeach;
        
        $color_scheme[$key]['colors'] = $base_colors;
        
    endforeach;
    
    return $color_scheme;
    
}

/** Color scheme list **/
$tsm_color_schemes_list = array(
    'dark_default'  =>  tsm_init_color_scheme_args( $base_color_scheme_dark_default, 'second' ),
    'default'       =>  tsm_init_color_scheme_args( $base_color_scheme_default, 'second' ),
    'impressive'    =>  tsm_init_color_scheme_args( $base_color_scheme_impressive, 'fourth' )
);


