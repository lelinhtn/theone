<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *  Display wp errors (notes) messages
 *  @param $notes: array or string of notes
 *  @since 1.0 
 **/
function tsm_display_note( $notes, $type='error', $echo = true ){
    $html = '';
        
    if( !empty( $notes ) ){
         $class = ( $type == 'error' ) ? 'error' : 'updated';
         $class .= ' tsm-mesage';
        
        $html .= '<div class="' . $class . ' below-h2" >'; // error
        if ( is_array( $notes ) ){
           $html .= '<ul>' ;
            foreach( $notes as $e ){
                 $html .= '<li>' . ( $e ) . '</li>';
            }
           $html .= '</ul>'; 
        }else {
            $html .= '<p>' . ( $notes ) . '</p>';
        }
        
        $html .= '</div>';
    }
    
    if ( $echo ) {
        echo $html;
    }else {
        return $html;
    }
}

/**
 *  Save color scheme as... via ajax 
 *  @since 1.0
 **/
function tsm_save_color_temp_as_via_ajax() {
    global $tsm_css_list_default;
    
	$errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'you do not have enough permissions to save color scheme!', 'tsm' );
        
    endif;
    
    if ( empty( $errors ) ):
        
        $template = $_POST['template'];
        $css_arr_key = sanitize_key( $_POST['css_arr_key'] );
        $new_temp_name = sanitize_text_field( $_POST['new_temp_name'] );
        $new_color_scheme = trim( $_POST['new_color_scheme'] );
        $tmp_colors = $_POST['tmp_colors'];
        $css_base = urldecode( $_POST['css_base']);
        
        $tmp_colors = ( is_array( $tmp_colors ) ) ? array_filter( $tmp_colors ) : array();
        
        // Get list of saved temps
        $saved_temps = get_option( 'tsm_color_temps', array() );
        
        $all_temps = array_merge( $tsm_css_list_default, $saved_temps );
        
        $css_arr_key_new = $css_arr_key;
        
        $i = 0;
        // Check $css_arr_key exists
        while ( array_key_exists( $css_arr_key_new, $all_temps ) ):
        
            $i++; 
            $css_arr_key_new = $css_arr_key . '_' . $i;
        
        endwhile;
        
        $template .= '_' . $i;
        
        $color_schemes_id = 'custom_color_scheme_' . $template;
        $color_schemes_class = $color_schemes_id;
        $color_schemes_code = $new_color_scheme;
        
        $tmp_colors = tsm_array_filter_recursive( $tmp_colors );
        
        $new_color_scheme_temp = array(
            'css_key'               =>  $template,
            'css_name'              =>  $new_temp_name,
            'tmp_colors'            =>  $tmp_colors,
            'color_schemes_id'      =>  '',
            'color_schemes_class'   =>  '',
            'color_schemes_code'    =>  '',
            'css_base'              =>  $css_base
        );
        
        if ( $color_schemes_code != '' ):
            
            $new_color_scheme_temp['color_schemes_id'] = $color_schemes_id;
            $new_color_scheme_temp['color_schemes_class'] = $color_schemes_class;
            $new_color_scheme_temp['color_schemes_code'] = $color_schemes_code;
            
        endif;
        
        $new_color_scheme_temp = array_filter( $new_color_scheme_temp );
        
        $saved_temps[$css_arr_key_new] = $new_color_scheme_temp;
        
        update_option( 'tsm_color_temps', $saved_temps );
    
    endif;
    
    tsm_display_note( $errors );
    
    die();
}
add_action( 'wp_ajax_tsm_save_color_temp_as_via_ajax', 'tsm_save_color_temp_as_via_ajax' );

/**
 *  Save color scheme via ajax 
 *  @since 1.0
 **/
function tsm_save_color_temp_via_ajax() {
    global $tsm_css_list_default;
    
	$errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'you do not have enough permissions to save color scheme!', 'tsm' );
        
    endif;
    
    $css_arr_key = sanitize_key( $_POST['css_arr_key'] );
    
    if ( array_key_exists( $css_arr_key, $tsm_css_list_default ) ):
            
        $errors[] = __( 'Can not modify of delete default templates. Please choose save as...', 'tsm' );
        
    endif;
    
    if ( empty( $errors ) ):
        
        $template = $_POST['template'];
        $new_temp_name = sanitize_text_field( $_POST['new_temp_name'] );
        $new_color_scheme = trim( $_POST['new_color_scheme'] );
        $tmp_colors = $_POST['tmp_colors'];
        $css_base = urldecode( $_POST['css_base']);
        $tmp_colors = ( is_array( $tmp_colors ) ) ? array_filter( $tmp_colors ) : array();
        
        // Get list of saved temps
        $saved_temps = get_option( 'tsm_color_temps', array() );
        
        $color_schemes_id = 'custom_color_scheme_' . $template;
        $color_schemes_class = $color_schemes_id;
        $color_schemes_code = $new_color_scheme;
        
        $tmp_colors = tsm_array_filter_recursive( $tmp_colors );
        
        $new_color_scheme_temp = array(
            'css_key'               =>  $template,
            'css_name'              =>  $new_temp_name,
            'tmp_colors'            =>  $tmp_colors,
            'color_schemes_id'      =>  '',
            'color_schemes_class'   =>  '',
            'color_schemes_code'    =>  '',
            'css_base'              =>  $css_base
        );
        
        if ( $color_schemes_code != '' ):
            
            $new_color_scheme_temp['color_schemes_id'] = $color_schemes_id;
            $new_color_scheme_temp['color_schemes_class'] = $color_schemes_class;
            $new_color_scheme_temp['color_schemes_code'] = $color_schemes_code;
            
        endif;
        
        $new_color_scheme_temp = array_filter( $new_color_scheme_temp );
        
        unset( $saved_temps[$css_arr_key] );
        $saved_temps[$css_arr_key] = $new_color_scheme_temp;
        
        update_option( 'tsm_color_temps', $saved_temps );
    
    endif;
    
    tsm_display_note( $errors );
    
    die();
}
add_action( 'wp_ajax_tsm_save_color_temp_via_ajax', 'tsm_save_color_temp_via_ajax' );

/**
 *  Delete a custom CSS template via ajax
 *  @since 1.0 
 **/
function tsm_del_color_temp_via_ajax() {
    global $tsm_css_list_default;
    
	$errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'You do not have enough permissions to delete color scheme!', 'tsm' );
        
    endif;
    
    $css_arr_key = sanitize_key( $_POST['css_arr_key'] );
    
    if ( array_key_exists( $css_arr_key, $tsm_css_list_default ) ):
            
        $errors[] = __( 'Can not modify of delete default templates. Please choose save as...', 'tsm' );
        
    endif;
    
    if ( empty( $errors ) ):
        
        // Get list of saved temps
        $saved_temps = get_option( 'tsm_color_temps', array() );
        
        if ( array_key_exists( $css_arr_key, $saved_temps ) ):
        
            unset( $saved_temps[$css_arr_key] );
            update_option( 'tsm_color_temps', $saved_temps );
            
        endif;
        
    endif;
    
    tsm_display_note( $errors );
    
    die();
}
add_action( 'wp_ajax_tsm_del_color_temp_via_ajax', 'tsm_del_color_temp_via_ajax' );


/**
 *  Display Select List
 *  @since 1.0 
 **/
function tsm_select_list( $args = array(), $options = array(), $echo = true ) {
    
    $options_default = array(
        'before'    =>  '<p>',
        'after'     =>  '</p>',  
        'label'     =>  '',
        'id'        =>  uniqid( 'tsm-select-' ),
        'class'     =>  'tsm-select',
        'selected'  =>  '',
        'attrs'     =>  ''
    );
    
    if ( is_object( $options ) ):
        
        $options = ( array ) $options;
        
    endif;
    
    if ( !is_array( $options ) ):
        
        $options = array();
        
    endif;
    
    $options = array_merge( $options_default, $options );
    
    if ( is_object( $args ) ):
        
        $args = ( array ) $args;
        
    endif;
    
    if ( !is_array( $args ) ):
        
        $args = array();
        
    endif;
    
    $html = '';
    $select_html = '';
    $label_html = '';
    
    if ( !empty( $args ) ):
    
        $val_default = array(
            0   =>  '', // value
            1   =>  ''  // display
        );
        
        $select_id = esc_attr( $options['id'] );
        $select_class = sanitize_text_field( $options['class'] );
        
        if ( $options['label'] != '' ):
            
            $label_html .= '<label class=\'tsm-seting-label\' for=\'' . $select_id . '\'>' . sanitize_text_field( $options['label'] ) . '</label>';
            
        endif;
        
        $select_html .= '<select id=\'' . $select_id . '\' class=\'' . $select_class . '\' ' . sanitize_text_field( $options['attrs'] ) . ' >';
        
        foreach ( $args as $key => $val ):
            
            if ( !is_array( $val ) && !is_object( $val ) ):
                
                $select_html .= '<option ' . selected( $options['selected'] == $val, true, false ) . ' value=\'' . $val . '\'>' . $val . '</option>';
            
            else:
                
                if ( is_array( $val ) ):
                    
                    $val = array_merge( $val, $val_default );
                    
                    $select_html .= '<option ' . selected( $options['selected'] == $val[0], true, false ) . ' value=\'' . $val[0] . '\'>' . $val[1] . '</option>';
                    
                endif;
                
            endif;
            
        endforeach;
        
        $select_html .= '</select>';
        
    endif;
    
    $html .= $options['before'] . $label_html . $select_html . $options['after'];
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}

/**
* tsm_array_merge_recursive_replaces()
*
* Similar to array_merge_recursive but keyed-valued are always overwritten.
* $paArray2 overwrite $paArray1
* Priority goes to the 2nd array.
* @since 1.0
* @static yes
* @public yes
* @param $paArray1 array
* @param $paArray2 array
* @return array
*/
function tsm_array_merge_recursive_replaces( $paArray1, $paArray2 ){
    if ( !is_array( $paArray1 ) or !is_array( $paArray2 ) ) { return $paArray2; }
    foreach ( $paArray2 AS $sKey2 => $sValue2 )
    {
        $paArray1[$sKey2] = tsm_array_merge_recursive_replaces( @$paArray1[$sKey2], $sValue2 );
    }
    return $paArray1;
}


/**
 *  Array filter recursive
 *  @since 1.0 
 **/
function tsm_array_filter_recursive( $array = array() ) {
    
    if ( is_object( $array ) ):
        
        $array = ( array ) $array;
        
    endif;
    
    if ( !is_array( $array ) ):
        
        return false;
        
    endif;
    
    $result = array_map( 'array_filter', $array );
    //$count = array_map( 'count', $result );
    //$count_sum = array_sum( $count );
    
    return $result;    
}

/**
 *  Gen shortcode from elem data
 *  @since 1.0 
 **/
function tsm_gen_shortcode( $data_elem, $class = '' ) {
    
    $data_elem_default = array(
        'name'          =>  '',
        'desc'          =>  '',
        'fields'        =>  array(),
        'short_code'    =>  ''
    );
    
    $data_elem = array_merge( $data_elem_default, $data_elem );
    $class = wp_strip_all_tags( $class );
    
    $shortcode = '';
    if ( is_array( $data_elem ) && !empty( $data_elem ) ):
    
        if ( $data_elem['short_code'] != '' ):
            
            $shortcode_atts = ' ';
            
            if ( !empty( $data_elem['fields'] ) ):
                
                foreach ( $data_elem['fields'] as $att => $att_info ):
                    
                    //$att_val = ( isset( $att_info['val'] ) ) ? urldecode( $att_info['val'] ) : '';
                    $att_val = ( isset( $att_info['val'] ) ) ? $att_info['val'] : '';
                    $shortcode_atts .= esc_attr( $att ) . '="' . $att_val . '" ';
                    
                endforeach;
                
            endif;
            
            $shortcode_atts .= 'class="' . $class . '"';
            
            // Version 1.0 only support self close type
            $shortcode .= '[' . $data_elem['short_code'] . $shortcode_atts . ']';
            
        endif;
    
    endif;
    
    return $shortcode;
}

/**
 *  TSM show widget by tsm widget data
 *  @since 1.0 
 **/
function tsm_show_widget( $elem_widget_args, $class = '' ) {
    
    $elem_widget_default_args = array(
        'elem_type'         =>  'widget',
        'widget_key'        =>  '',
        'widget_instance'   =>  '',
        'class'             =>  '',
        'additional_class'  =>  ''
    );
    
    //$elem_widget_args = array_merge( $elem_widget_default_args, $elem_widget_args );
    $elem_widget_args = wp_parse_args( $elem_widget_args, $elem_widget_default_args );
    $class = trim( wp_strip_all_tags( $class, true ) );
    $additional_class = trim( wp_strip_all_tags( $elem_widget_args['additional_class'], true ) );
    
    $widget = ( isset( $elem_widget_args['widget_key'] ) ) ? trim( $elem_widget_args['widget_key'] ) : '';
    $instance_str = ( isset( $elem_widget_args['widget_instance'] ) ) ? urldecode( $elem_widget_args['widget_instance'] ) : '';
    parse_str( $instance_str, $instance );
    
    $html = '';
    $html .= '<div class="tsm-mega-menu-item-wrap tsm-widget-wrap ' . $class . ' ' . $additional_class . '">';
    
    ob_start();
	the_widget( $widget, $instance );
	$html .= ob_get_clean();
    
    $html .= '</div><!-- .tsm-mega-menu-item-wrap -->';
    
    return $html;
}

if ( !function_exists( 'array_replace_recursive' ) ) {
    /**
     *  array_replace_recursive fix for php <= 5.2 
     **/
    function array_replace_recursive( $base, $replacements ) {
        foreach ( array_slice( func_get_args(), 1 ) as $replacements ) {
    		$bref_stack = array( &$base );
    		$head_stack = array( $replacements );
    
    		do {
    			end( $bref_stack );
    
    			$bref = &$bref_stack[ key( $bref_stack ) ];
    			$head = array_pop($head_stack);
    
    			unset( $bref_stack[key( $bref_stack )] );
    
    			foreach ( array_keys( $head ) as $key ) {
    				if ( isset( $bref[$key] ) && is_array( $bref[$key] ) && is_array( $head[$key] ) ) {
    					$bref_stack[] = &$bref[$key];
    					$head_stack[] = $head[$key];
    				} else {
    					$bref[$key] = $head[$key];
    				}
    			}
    		} while( count( $head_stack ) );
    	}
    
    	return $base;
    }
} // End if ( !function_exists( 'array_replace_recursive' ) )

/**
 *  Recursively convert an object to an array
 *  @since 1.0 
 **/
function tsm_object_to_array_recursive( $obj ) {
    
    if( is_object( $obj ) ) $obj = (array) $obj;
    
    if( is_array( $obj ) ):
    
        $new = array();
        foreach( $obj as $key => $val ):
        
            $new[$key] = tsm_object_to_array_recursive( $val );
            
        endforeach;
        
    else:
    
        $new = $obj;
        
    endif;
    
    return $new;   
      
}

/**
 *  Edit dropdown li form
 *  @since 1.0 
 **/
function tsm_edit_dropdown_list_li( $echo = true ) {
    global $tsm_script_localize;
    
    $form_id = uniqid();
    
    $args_builtin = array(
        'public'    =>  true,
        '_builtin'  =>  true
    );
    
    $args_not_builtin = array(
        'public'    =>  true,
        '_builtin'  =>  false
    );
    
    $output = 'objects'; // names or objects
    $post_types_builtin = get_post_types( $args_builtin, $output );
    
    $post_types_select_html = '<select id="tsm-li-select-link-type-' . $form_id . '" data-placeholder="' . __( 'Chose a custom post type', 'tsm' ) . '" class="tsm-select tsm-li-select-link-type">';
    $post_types_select_html .= '<option data-link-type="custom" value="custom">' . __( 'Custom', 'tsm' ) . '</option>';
    $post_types_select_html .= '<optgroup label="' . __( 'Post types', 'tsm' ) . '">';
    foreach ( $post_types_builtin as $post_type ):
        
        $post_types_select_html .= '<option data-link-type="post_type" value="' . $post_type->name . '">' . $post_type->label . '</option>';
       
    endforeach;
    
    $post_types_not_builtin = get_post_types( $args_not_builtin, $output, 'and' );
    
    if ( $post_types_not_builtin ):
        
        foreach ( $post_types_not_builtin as $post_type ):
            
            $post_types_select_html .= '<option data-link-type="post_type" value="' . $post_type->name . '">' . $post_type->label . '</option>';
           
        endforeach;
        
    endif;
    
    $post_types_select_html .= '</optgroup>';
    $taxonomies_builtin = get_taxonomies( $args_builtin, $output ); 
    
    $post_types_select_html .= '<optgroup label="' . __( 'Taxonomies', 'tsm' ) . '">';
    foreach ( $taxonomies_builtin as $taxonomy ):
        
        $post_types_select_html .= '<option data-link-type="taxonomy" value="' . $taxonomy->name . '">' . $taxonomy->label . '</option>';
        
    endforeach;
    
    
    $taxonomies_not_builtin = get_taxonomies( $args_not_builtin, $output, 'and' ); 
    
    if ( $taxonomies_not_builtin ):
        
        foreach ( $taxonomies_not_builtin as $taxonomy ):
            
            $post_types_select_html .= '<option data-link-type="taxonomy" value="' . $taxonomy->name . '">' . $taxonomy->label . '</option>';
            
        endforeach;
        
    endif;
    
    $post_types_select_html .= '</optgroup>';
    $post_types_select_html .= '</select>';
    
    $html = '<div class=\'tsm-edit-dropdown-list-li-form\'>
                <label for=\'tsm-li-select-link-type-' . $form_id . '\' class="tsm-change-li-link-type" title="' . __( 'Click to change', 'tsm' ) . '">' . __( 'Link Type:', 'tsm' ) . ' <span>' . __( 'Custom', 'tsm' ) . '</span></label><br />
                <div class="tsm-li-link-type-choser-wrap tsm-box" style="display: none;">
                    ' . $post_types_select_html . ' <br />
                    <div id=\'tsm-postypes-terms-chosen-wrap-' . $form_id . '\' class=\'tsm-box tsm-postypes-terms-chosen-wrap\'>
                        ' . tsm_load_posttypes_terms_chosen() . '
                    </div><!-- .tsm-postypes-terms-chosen-wrap -->
                </div>
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                <label for=\'tsm-li-text-input-' . $form_id . '\'>' . __( 'Text', 'tsm' ) . '</label><br />
                <input type=\'text\' id=\'tsm-li-text-input-' . $form_id . '\' data-field-required=\'yes\' class=\'tsm-li-text-input tsm-elem-field tsm-input widefat\' />
                <label for=\'tsm-li-link-input-' . $form_id . '\'>' . __( 'Link', 'tsm' ) . '</label><br />
                <input type=\'text\' id=\'tsm-li-link-input-' . $form_id . '\' class=\'tsm-li-link-input tsm-url-input tsm-input widefat\' />
                <label for=\'tsm-li-addition-class-input-' . $form_id . '\'>' . __( 'Additional Class', 'tsm' ) . '</label><br />
                <input type=\'text\' id=\'tsm-li-addition-class-input-' . $form_id . '\' class=\'tsm-li-addition-class-input tsm-input tsm-no-special-chars-input widefat\' />
                <label for=\'tsm-li-right-dropdown-select-' . $form_id . '\'>' . __( 'Right Align', 'tsm' ) . '</label><br />
                <select id=\'tsm-li-right-dropdown-select-' . $form_id . '\' class=\'tsm-li-right-dropdown-select tsm-select\'>
                    <option value=\'no\'>' . __( 'No', 'tsm' ) . '</option>
                    <option value=\'yes\'>' . __( 'Yes', 'tsm' ) . '</option>
                </select>
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                <label for=\'tsm-li-icon-' . $form_id . '\' class=\'tsm-choose-li-icon-label tsm-choose-icon-label\'>
                    <span class="tsm-menu-item-large-title">' . __( 'Icon', 'tsm' ) . '</span>
                    <span data-icon-class="" class="tsm-icon-chosen">
                            <i>' . __( 'No icon', 'tsm' ) . '</i>
                    </span>
                </label><br />
                <div id=\'tsm-li-icon-' . $form_id . '\' class=\'tsm-li-icon tsm-menu-item-icon\' style=\'display: none;\'>
                </div>
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                <a href=\'#\' class=\'tsm-close-btn tsm-btn button\'><i class=\'fa fa-times\'></i>' . __( 'Close', 'tsm' ) . '</a>
            </div>';
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
}

/**
 *  Load posttypes, terms chosen via ajax
 *  @since 1.0 
 **/
function tsm_load_posttypes_terms_chosen_via_ajax(){
    
    $posttype_or_term_name = sanitize_text_field( $_POST['obj_name'] );
    $obj_type = sanitize_key( $_POST['obj_type'] );
    $limit = $_POST['limit'];
    $paged = $_POST['paged'];
    
    $html = tsm_load_posttypes_terms_chosen( $posttype_or_term_name, $obj_type, $limit, $paged );
    
    echo $html;
    
    die();
}
add_action( 'wp_ajax_tsm_load_posttypes_terms_chosen_via_ajax', 'tsm_load_posttypes_terms_chosen_via_ajax' );

/**
 *  @since 1.0
 **/
function tsm_load_posttypes_terms_chosen( $obj_name = '', $obj_type = '', $limit = 15, $paged = 1 ) {
    
    $posttype_or_term_name = $obj_name;
    $limit = ( max( 1, intval( $limit ) ) );
    $paged = ( max( 1, intval( $paged ) ) );
    $html = '';
    
    switch ( $obj_type ):
    
        case 'post_type':
            $posttypes_list_html = '';
            $args = array(
                'post_type' => $posttype_or_term_name,
                'post_status' => 'publish',
                'posts_per_page' => $limit,
                'paged' => $paged
            );
            $posttypes_query = new WP_Query( $args );
            $total_posts = 0;
            if( $posttypes_query->have_posts() ):
                $total_posts = $posttypes_query->found_posts;
                $ratio_name = uniqid( 'tsm-ratio-posttypes-' );
                $i = 0;
                while ($posttypes_query->have_posts()) : $posttypes_query->the_post(); 
                
                    $i++;
                    $ratio_id = $ratio_name . '-' . $i;
                    $posttypes_list_html .= '<label for="' . $ratio_id . '">
                                                <input value="' . get_the_ID() . '" data-original-title="' . get_the_title() . '" type="radio" id="' . $ratio_id . '" class="tsm-ratio radio" name="' . $ratio_name . '" />
                                                ' . get_the_title() . '
                                            </label><br />';
                
                endwhile;
            endif;
            wp_reset_query();  // Restore global post data stomped by the_post().
            $html = '<div class="tsm-postypes-terms-chosen">
                        ' . $posttypes_list_html . '
                    </div>';
            if ( $total_posts > $limit ):
                
                $total_pages = ceil( $total_posts / $limit );
                
                $html .= '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
                
                $html .= '<ul class="tsm-pagination tsm-link-type-pagination pagination">';
                for ( $i = 1; $i <= $total_pages; $i++ ):
                    
                    $li_class = ( $i == $paged ) ? 'tsm-cur-page active' : '';
                    $html .= '<li data-obj-name="' . $posttype_or_term_name . '" data-link-type="' . $obj_type . '" data-page="' . $i . '" class="' . $li_class . '"><span>' . $i . '</span></li>';
                    
                endfor;
                $html .= '</ul>';
                
            endif;
            
            break;
        case 'taxonomy':
            
            $terms_list_html = '';
            
            $taxonomies = array( $posttype_or_term_name );
            
            $args = array(
                'hide_empty'    =>  false,
                'number'        =>  $limit,
                'offset'        =>  ( $paged - 1 ) * $limit
            );
            $terms = get_terms( $taxonomies, $args );
            
            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):
                
                //$total_terms = count( $terms );
                $total_terms = wp_count_terms( $taxonomies, array( 'hide_empty' => false ) );
                
                $ratio_name = uniqid( 'tsm-ratio-term-' );
                $i = 0;
                
                foreach ( $terms as $term ):
                    $i++;
                    $ratio_id = $ratio_name . '-' . $i;
                    
                    $root_name = '';
                    $root_term_ids = get_ancestors( $term->term_id, $posttype_or_term_name );
                    
                    if ( !empty( $root_term_ids ) ):
                        $root_term_id = $root_term_ids[0];
                        $root_term = get_term( $root_term_ids[0], $posttype_or_term_name );
                        $root_name = $root_term->name;
                    endif;
                    
                    $terms_list_html .= '<label for="' . $ratio_id . '">
                                            <input value="' . $term->term_id. '" data-original-title="' . $term->name . '" type="radio" id="' . $ratio_id . '" class="tsm-ratio radio" name="' . $ratio_name . '" />
                                            ' . $term->name . '
                                            <span class="root-name">' . $root_name . '</span>
                                        </label><br />';
                    
                endforeach;
                
                
                //tsm_custom_taxonomy_ratio_walker( $terms_list_html, $ratio_name, 0, $posttype_or_term_name );
                
                if ( $total_terms > $limit ):
                    
                    $total_pages = ceil( $total_terms / $limit );
                    
                    $terms_list_html .= '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
                    
                    $terms_list_html .= '<ul class="tsm-pagination tsm-link-type-pagination pagination">';
                    for ( $i = 1; $i <= $total_pages; $i++ ):
                    
                        $li_class = ( $i == $paged ) ? 'tsm-cur-page active' : '';
                        $terms_list_html .= '<li data-obj-name="' . $posttype_or_term_name . '" data-link-type="' . $obj_type . '" data-page="' . $i . '" class="' . $li_class . '"><span>' . $i . '</span></li>';
                        
                    endfor;
                    $terms_list_html .= '</ul>';
                    
                endif;
                
            endif;
            
            $html = '<div class="tsm-postypes-terms-chosen">
                        ' . $terms_list_html . '
                    </div>';
            
            break;
        
    endswitch;
    
    $html .= '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
    $html .= '<a class="button tsm-btn tsm-update-li-type-btn"><i class="fa fa-link"></i>' . __( 'Update', 'tsm' ) . '</a>';
    //$html .= '<a class="button tsm-btn tsm-close-li-type-btn">' . __( 'Close', 'tsm' ) . '</a>';
    
    return $html;
    
}

/**
 *  Insert prefix to each selector css
 *  @since 1.0 
 **/
function tsm_insert_css_prefix( $prefix = '', $css = '', $breakpoint = 768, $echo = true ) {
    
    // Remove all css comments
    //$regex = '/(^|\n)\s*\/\*.*?\*\/\s*/s';
    $regex = '!/\*[^*]*\*+([^/][^*]*\*+)*/!';
    $css = preg_replace( $regex, '', $css );
    
    // Replacing multiple spaces with a single space
    $css = preg_replace( '!\s+!', ' ', $css );
    
    // Replacing commas within brackets
    $css = preg_replace_callback( '|\((.*)\)|', 'tsm_replace_commas', $css );
    
    $parts = explode( '}', $css );
    foreach ( $parts as &$part ):
    
        if ( empty( $part ) ):
            continue;
        endif;
    
        $subParts = explode( ',', $part );
        foreach ( $subParts as &$subPart ):
            $subPartTemp = preg_replace( '!\s+!', '', $subPart );
            // Don't add prefix before @
            if ( !tsm_str_starts_with( trim( $subPartTemp ), '@' ) ):
                
                // Insert if not startwith number or "}"
                if ( !preg_match( '/^[0-9]/', trim( $subPartTemp ) ) && $subPartTemp != '' ):
                    
                    $subPart = $prefix . ' ' . trim( $subPart );
                    
                endif;  
            
            else:
                
                if ( strpos( $subPart, "@media screen and (max-width: " . $breakpoint . "px) {" ) > 0 ):
                    
                    $str_search = "@media screen and (max-width: " . $breakpoint . "px) {";
                    $str_replace = "@media screen and (max-width: " . $breakpoint . "px) { " . $prefix . " ";
                    
                    $subPart = str_replace( $str_search, $str_replace, $subPart );
                    
                endif;
                 
            endif;
        endforeach;
        
        $part = implode( ', ', $subParts );
        
    endforeach;
    
    $prefixedCss = implode( "}\n", $parts );
    
    $prefixedCss = str_replace( '___TSM_COMMA___', ',', $prefixedCss );
    
    if ( $echo ):
        echo $prefixedCss;
    else:
        return $prefixedCss;
    endif;
    
}

/**
 *  Replace all commas in $text by ___TSM_COMMA___
 *  $text = preg_replace_callback( '|\((.*)\)|', 'tsm_replace_commas', $text);
 *  @since 1.0 
 **/
function tsm_replace_commas( $s ) {
  return str_replace( ',', '___TSM_COMMA___', $s[0] );
}

/**
 *  var_dump( tsm_str_starts_with( 'hello world', 'hello' ) ); // true 
 *  @since 1.0
 **/
function tsm_str_starts_with( $haystack, $needle ) {
    return $needle === "" || strpos( $haystack, $needle ) === 0;
}

/**
 *  var_dump( tsm_str_ends_with( 'hello world', 'world' ) ); // true 
 *  @since 1.0
 **/
function tsm_str_ends_with( $haystack, $needle ) {
    return $needle === "" || substr( $haystack, -strlen( $needle ) ) === $needle;
}


/*function to_utf8( $string ) { 
// From http://w3.org/International/questions/qa-forms-utf-8.html 
    if ( preg_match('%^(?: 
      [\x09\x0A\x0D\x20-\x7E]            # ASCII 
    | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte 
    | \xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs 
    | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte 
    | \xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates 
    | \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3 
    | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15 
    | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16 
)*$%xs', $string) ) { 
        return $string; 
    } else { 
        return iconv( 'CP1252', 'UTF-8', $string); 
    } 
} 
*/
function tsm_utf8_urldecode($str) {
$str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
return html_entity_decode($str,null,'UTF-8');;
}

function tsm_get_page_title(){
    global $tsmAdminMenus;
    
    if ( isset( $_REQUEST['page'] ) ):
        
        $page = $_REQUEST['page'];
        foreach( $tsmAdminMenus as $menu ):
        
            if ( $menu['slug'] == $page ):
            
                return $menu['page_title'];
                
            endif;
            
        endforeach;        
    endif;
}

/**
 *  Load icons list via ajax
 *  @since 1.0 
 **/
function tsm_load_icons_list_page_via_ajax() {
    
    $data_page = ( isset( $_POST['data_page'] ) ) ? max( 1, intval( $_POST['data_page'] ) ) : 1;
    $data_limit = ( isset( $_POST['data_limit'] ) ) ? max( 4, intval( $_POST['data_limit'] ) ) : 30;
    $show_class = ( isset( $_POST['data_show_class'] ) ) ? esc_attr( $_POST['data_show_class'] ) == 'yes' : true;
    
    tsm_font_icons_chooser( $data_page, $data_limit, true, $show_class );
    
    die();
}
add_action( 'wp_ajax_tsm_load_icons_list_page_via_ajax', 'tsm_load_icons_list_page_via_ajax' );

/**
 *  File get content
 *  @since 1.0 
 **/
function tsm_file_get_content( $file_path = '' ){
    global $wp_filesystem;
    
    if ( !file_exists( $file_path ) ):
        
        return '';
        
    endif;
    
    if ( empty( $wp_filesystem ) ):
    
        require_once ABSPATH . '/wp-admin/includes/file.php';
        WP_Filesystem();
        
    endif;
    
    if ( !empty( $wp_filesystem ) ):
        
        return $wp_filesystem->get_contents( $file_path );
        
    endif;
    
    return '';
}

/**
 * No image generator 
 * @since 1.0
 * @param $size: array, image size
 * @param $echo: bool, echo or return no image url
 **/
function tsm_no_image( $size = array( 'width' => 500, 'height' => 500 ), $echo = false, $transparent = false ) {
    
    $suffix = ( $transparent ) ? '_transparent' : '';
    
    if ( !is_array( $size ) || empty( $size ) ):
        $size = array( 'width' => 500, 'height' => 500 );
    endif;
    
    if ( !is_numeric( $size['width'] ) && $size['width'] == '' || $size['width'] == null ):
        $size['width'] = 'auto';
    endif;
    
    if ( !is_numeric( $size['height'] ) && $size['height'] == '' || $size['height'] == null ):
        $size['height'] = 'auto';
    endif;
    
    // base image must be exist
    $img_base_fullpath = TSM_DIR . '/img/noimage/no_image' . $suffix . '.png';
    $no_image_src = TSM_URL . '/img/noimage/no_image' . $suffix . '.png';
    
    
    // Check no image exist or not
    if ( !file_exists( TSM_DIR.'/img/noimage/no_image' . $suffix . '-' . $size['width'] . 'x' . $size['height'] . '.png' ) ):
    
        $no_image = wp_get_image_editor( $img_base_fullpath );
        
        if ( !is_wp_error( $no_image ) ):
            $no_image->resize( $size['width'], $size['height'], true );
        	$no_image_name = $no_image->generate_filename( $size['width'] . 'x' . $size['height'], TSM_DIR . 'img/noimage/', null );
            $no_image->save( $no_image_name );
        endif;
        
    endif;
    
    // Check no image exist after resize
    $noimage_path_exist_after_resize = TSM_DIR . '/img/noimage/no_image' . $suffix . '-' . $size['width'] . 'x' . $size['height'] . '.png';
    
    if ( file_exists( $noimage_path_exist_after_resize ) ):
        $no_image_src = TSM_URL . '/img/noimage/no_image' . $suffix . '-' . $size['width'] . 'x' . $size['height'] . '.png';
    endif;
    
    if ( $echo ):
        echo $no_image_src;
    else:
        return $no_image_src;
    endif;

}

add_action( 'wp_ajax_tsm_load_editor_via_ajax', 'tsm_load_editor_via_ajax' );
function tsm_load_editor_via_ajax(){

	//$index = (int) $_POST['ov_index'];
	$editor_id = ( isset( $_POST['editor_id'] ) ) ? sanitize_key( $_POST['editor_id'] ) : uniqid( 'tsm-wp-editor-' );
    $editor_content = ( isset( $_POST['editor_content'] ) ) ? urldecode( trim( $_POST['editor_content'] ) ) : '';
    
	wp_editor( $editor_content, $editor_id, array( 'editor_class' => 'tsm-wp-editor' ) );

	die();
}

/**
 *  Load wp editor
 *  @since 1.0 
 **/
function tsm_load_editor( $editor_id = 'tsm-invisible-editor' ) {
    wp_editor( ' ', $editor_id, array( 'editor_class' => 'tsm-wp-editor' ) );
}

/**
 *  Choose menu style themes
 *  @since 1.0 
 **/
function tsm_choose_css_theme( $selected = '', $options = array(), $echo = true ) {
    global $tsm_theme_list;
    
    $options_default = array(
        'id'        =>  uniqid( 'tsm-theme-select-' ),
        'class'     =>  'tsm-theme-select',
        'attrs'     =>  ''
    );
    
    if ( !is_array( $options ) ):
        
        $options = ( array ) $options;
        
    endif;
    
    $options = array_merge( $options_default, $options );
    
    $html = '';
    
    $html .= '<select id="' . $options['id'] . '" class="tsm-select2 tsm-select ' . $options['class'] . '" ' . $options['attrs'] . '>';
    $html .= '<option value="">' . __( 'Don\'t load menu theme', 'tsm' ) . '</option>';
    foreach ( $tsm_theme_list  as $theme_key => $theme ):
        
        $html .= '<option ' . selected( $selected == $theme_key, true, false ) . ' value="' . $theme_key . '">' . sanitize_text_field( $theme['display_name'] ) . '</option>';
    
    endforeach;
    $html .= '</select>';
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
}

/**
 *  Choose CSS Template mega menu
 *  @since 1.0 
 **/
function tsm_choose_css_temp_megamenu( $options = array(), $echo = true ) {
    global $tsm_css_list_default, $default_color_schemes;
    
    $tsm_css_list_saved = get_option( 'tsm_color_temps', array() );
    $tsm_css_list = array_merge( $tsm_css_list_default, $tsm_css_list_saved );

    
    $options_default = array(
        'hidden_get_css'        =>  'no',
        'hidden_light'          =>  'no',
        'hidden_temp_list'      =>  'yes',
        'enable_actions_btns'   =>  'yes'
    );
    
    $options = array_merge( $options_default, $options );
    
    
    $html = '';
    $choose_temp_html = '';
    $change_temp_colors_html = '';
    $cutom_change_temp_colors_ratio_html = ''; // Of custom temp color
    $cutom_change_temp_colors_label_html = ''; // Of custom temp color
    $show_match_class = '';
    $temp_list_html = ''; 
    $temp_list_style = ( $options['hidden_temp_list'] == 'yes' ) ? 'style=\'display: none;\'' : '';
    if ( !empty( $tsm_css_list ) ):
        
        $choose_temp_html .= '<div class=\'tsm-choose-css-template-wrap\'>';
        $choose_temp_html .= '<h1>' . __( 'Choose a Template', 'tsm' ) . '</h1>';
        $choose_temp_html .= '<select name="template" id="chooseTemplate">';
        
        $change_temp_colors_html .= '<div id="changeTemplateColors">';
        $change_temp_colors_html .= '<h1>' . __( 'Change Main Template Colors', 'tsm' ) . '</h1>';
        
        $temp_list_html .= '<div id="templates" ' . $temp_list_style . '>';
        
        $i = 0;
        foreach ( $tsm_css_list as $tsm_css_key => $tsm_css_args ):
            
            if ( isset( $tsm_css_args['css_key'] ) ):
                
                $i++;
                $css_key = esc_attr( $tsm_css_args['css_key'] );
                
                // Custom change temp colors
                if ( isset( $tsm_css_args['color_schemes_id'] ) && isset( $tsm_css_args['color_schemes_class'] ) && isset( $tsm_css_args['color_schemes_code'] ) ):
                    
                    $show_match_class .= ' tsm-show-match-temp-' . $css_key;
                    if ( $tsm_css_args['color_schemes_code'] != '' ):
                        
                        $cutom_change_temp_colors_ratio_html .= '<input type="radio" name="predefinedColor" data-color="' . trim( $tsm_css_args['color_schemes_code'] ) . '" id="c' . esc_attr( $tsm_css_args['color_schemes_id'] ) . '" class="tsm-show-match-temp tsm-show-match-temp-' . $css_key . '">';
                        $cutom_change_temp_colors_label_html .= '<label for="c' . esc_attr( $tsm_css_args['color_schemes_id'] ) . '" class="' . esc_attr( $tsm_css_args['color_schemes_class'] ) . ' tsm-show-match-temp tsm-show-match-temp-' . $css_key . '" style="background:' . $tsm_css_args['color_schemes_code'] . '"></label>';
                        
                    endif; 
                    
                endif;
                
                $css_name = ( isset( $tsm_css_args['css_name'] ) ) ? sanitize_text_field( $tsm_css_args['css_name'] ): $css_key;
                $choose_temp_html .= '<option value=\'' . $css_key . '\'>' . $css_name . '</option>';
                

                $change_temp_color_style = ( $i == 1 ) ? '' : 'style=\'display: none;\'';
                $change_temp_colors_html .= '<div ' . $change_temp_color_style . ' class=\'' . $css_key . '\'>';
                
                $temp_list_html .= '<div id="' . $css_key . '">';
                
                if ( !empty( $tsm_css_args['tmp_colors'] ) ):
                    
                    $input_num = 0;
                    foreach ( $tsm_css_args['tmp_colors'] as $color_args ):
                        
                        $input_num++;
                        $input_type = ( isset( $color_args['type'] ) ) ? 'type=\'' . esc_attr( $color_args['type'] ) . '\'' : '';
                        $input_attrs = '';
                        
                        if ( $input_num == 1 ):
                            
                            $input_attrs .= 'class=\'color main\'';
                        
                        else:
                            
                            $input_attrs .= 'class=\'color\'';
                            
                        endif;
                        
                        if ( !empty( $color_args ) ):
                            
                            foreach ( $color_args as $key => $val ):
                                
                                $key = esc_attr( $key );
                                if ( $key != 'type' && $val != '' ):
                                    
                                    $input_attrs .= ' data-' . $key . '=\'' . $val . '\'';
                                    if ( $key == 'def' ):
                                    
                                        $input_attrs .= ' value=\'' . $val . '\'';
                                        
                                    endif;
                                    
                                endif;
                                
                            endforeach;
                            
                        endif;
                        
                        $change_temp_colors_html .= '<input ' . $input_attrs . ' ' . $input_type . ' >';
                        
                    endforeach;
                    
                endif;  // End if ( !empty( $tsm_css_args['tmp_colors'] ) )
                
                $change_temp_colors_html .= '</div><!-- .' . $css_key . ' -->';
                
                
                if ( isset( $tsm_css_args['css_base'] ) ):
                    
                    $temp_list_html .= $tsm_css_args['css_base'];
                    
                endif;
                
                
                $temp_list_html .= '</div><!-- #' . $css_key . ' -->';
                
            
            endif; // End if isset( $tsm_css_args['css_key'] )
            
        endforeach;
        
        $choose_temp_html .= '</select><!-- #chooseTemplate -->';
        $choose_temp_html .= '</div><!-- .tsm-choose-css-template-wrap -->';
        
        if ( !empty( $default_color_schemes ) ):
            
            $change_temp_colors_html .= '<br />';
            $change_temp_colors_html .= '<h4>' . __( 'Predefined Color Schemes', 'tsm' ) . '</h4>';
            $change_temp_colors_html .= '<span id="predefined">';
            
            $ratio_html = '';
            $label_html = '';
            
            foreach ( $default_color_schemes as $color_schemes_args ):
                
                if ( isset( $color_schemes_args['color'] ) && isset( $color_schemes_args['id'] ) ):
                    
                    $ratio_html .= '<input type="radio" name="predefinedColor" data-color="' . trim( $color_schemes_args['color'] ) . '" id="c' . esc_attr( $color_schemes_args['id'] ) . '">';
                    $label_html .= '<label for="c' . esc_attr( $color_schemes_args['id'] ) . '" class="' . esc_attr( $color_schemes_args['id'] ) . '"></label>';
                    
                endif;
                
            endforeach;
            
            $change_temp_colors_html .= $ratio_html . $cutom_change_temp_colors_ratio_html;
            $change_temp_colors_html .= $label_html . $cutom_change_temp_colors_label_html;
            $change_temp_colors_html .= '</span><!-- #predefined -->';
            
        endif;
        
        $change_temp_colors_html .= '</div><!-- #changeTemplateColors -->';
        
        $temp_list_html .= '</div><!-- #templates -->';
        
    endif;
    
    $get_css_style = '';
    if ( isset( $options['hidden_get_css'] ) ):
        $get_css_style = ( $options['hidden_get_css'] == 'yes' ) ? 'style=\'display: none;\'' : '';
    endif;
    
    $light_css_style = '';
    if ( isset( $options['hidden_light'] ) ):
        $light_css_style = ( $options['hidden_light'] == 'yes' ) ? 'style=\'display: none;\'' : '';
    endif;
    
    $actions_btns_html = '<div>
                        <a href="#" class="tsm-save-color-temp-as-btn tsm-btn button-primary">
                            <i class="fa fa-floppy-o"></i>
                            ' . __( 'Save this template as...', 'tsm' ) . '
                        </a>
                        <a href="#" class="tsm-save-color-temp-btn tsm-btn tsm-float-right button-primary tsm-show-match-temp ' . $show_match_class . '">
                            <i class="fa fa-floppy-o"></i>
                            ' . __( 'Save template', 'tsm' ) . '
                        </a>
                        <a href="#" class="tsm-del-color-temp-btn tsm-del tsm-btn tsm-action tsm-float-right button-primary tsm-show-match-temp ' . $show_match_class . '">
                            <i class="fa fa-times"></i>
                            ' . __( 'Delete Template', 'tsm' ) . '
                        </a>
                    </div>';
    
    if ( isset( $options['enable_actions_btns'] ) ):
        if ( $options['enable_actions_btns'] == 'no' ):
            $actions_btns_html = '';
        endif;
    endif;
    
    $html .= '<div id="cont">
                ' . $choose_temp_html . $change_temp_colors_html . '
                ' . $actions_btns_html . '
                <div id="getCss" ' . $get_css_style . '>
        			<h1>' . __( 'Get CSS', 'tsm' ) . '</h1>
        			<textarea>' . __( 'none', 'tsm' ) . '</textarea>
        		</div>
            </div><!-- #cont -->
            <div id="light" ' . $light_css_style . '>
        		<span class="l"></span>
        		<span class="d"></span>
        	</div>
            ' . $temp_list_html;
    
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}


/**
 *  $taxonomy walker for elems options
 *  @since 1.0
 **/
function tsm_custom_taxonomy_opt_walker( &$terms_select_opts = array(), $taxonomy, $parent = 0, $lv = 0 ) {
    
    $terms = get_terms( $taxonomy, array( 'parent' => $parent, 'hide_empty' => false ) );
    
    if ( $parent > 0 ):
        $lv++;
    endif;
    
    //If there are terms
    if( count( $terms ) > 0 ):
        $prefix = '';
        if ( $lv > 0 ):
            for ( $i = 1; $i <= $lv; $i++ ):
                $prefix .= '-- ';
            endfor;
        endif;
        
        //Cycle though the terms
        foreach ( $terms as $term ):
            $terms_select_opts[$term->term_id] = htmlentities2( $prefix . $term->name );
            
            //Secret sauce.  Function calls itself to display child elements, if any
            tsm_custom_taxonomy_opt_walker( $terms_select_opts, $taxonomy, $term->term_id, $lv ); 
        endforeach;
        
        //return $terms_select_opts;
    endif;
    
    //return $terms_select_opts;
}

/**
 *  $taxonomy walker for elems ratio chosen
 *  @since 1.0
 **/
function tsm_custom_taxonomy_ratio_walker( &$terms_list_html = '', $ratio_name = '', $ratio_num = 0, $taxonomy, $parent = 0, $lv = 0 ) {
    
    //echo "<pre>";
//        print_r($taxonomy);
//        echo "</pre>";
//    return;
    
    $terms = get_terms( $taxonomy, array( 'parent' => $parent, 'hide_empty' => false ) );
    echo "<pre>";
        print_r($terms);
        echo "</pre>";
    return;
    $ratio_num++;
    
    if ( $parent > 0 ):
        $lv++;
    endif;
    
    //If there are terms
    if( count( $terms ) > 0 && !is_wp_error( $terms ) ):
        $prefix = '';
        if ( $lv > 0 ):
            for ( $i = 1; $i <= $lv; $i++ ):
                $prefix .= '-- ';
            endfor;
        endif;
        
        //Cycle though the terms
        foreach ( $terms as $term ):
            //$terms_select_opts[$term->term_id] = htmlentities2( $prefix . $term->name );
            $term_display_name = htmlentities2( $prefix . $term->name );
            $ratio_id = $ratio_name . '-' . $ratio_num;
            $terms_list_html .= '<label for="' . $ratio_id . '">
                                    <input value="' . $term->term_id. '" data-original-title="' . htmlentities2( $term->name ) . '" type="radio" id="' . $ratio_id . '" class="tsm-ratio radio" name="' . $ratio_name . '" />
                                    ' . $term_display_name . '
                                </label><br />';
            
            //Secret sauce.  Function calls itself to display child elements, if any
            tsm_custom_taxonomy_ratio_walker( $terms_list_html, $ratio_name, $ratio_num, $taxonomy, $parent, $lv );
        endforeach;
        
        return $terms_list_html;
    endif;
    
    return $terms_list_html;
}


/**
 *  Choose number list
 *  @since 1.0 
 **/
function tsm_number_select( $from = 0, $to = 72, $selected = '', $id = '', $class = '', $attrs = '', $echo = true ) {
    
    $from = intval( $from );
    $to = max( $from, intval( $to ) );
    $id = esc_attr( $id );
    $attrs = sanitize_text_field( $attrs );
    
    $options = array(
        'before'    =>  '',
        'after'     =>  '',  
        'label'     =>  '',
        'id'        =>  $id,
        'class'     =>  $class,
        'selected'  =>  $selected,
        'attrs'     =>  $attrs
    );
    
    $args = array( 1 => '' );
    for ( $i = $from; $i <= $to; $i++ ):
       $args[] = $i;  
    endfor;
    
    $html = tsm_select_list( $args, $options, false );
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
}

/**
 *  Make excerpt from string
 *  @since 1.0 
 **/
function tsm_excerpts_from_str( $txt = '', $length = 30, $more_txt = '...' ){
    
    $txt = wp_strip_all_tags( $txt, true );
    if ( $txt != '' && strlen( $txt ) > $length ):
        $txt = substr( $txt, 0, $length ) . $more_txt;
    endif;
    
    return $txt;
    
}

/**
 *  Google font list
 *  @since 1.0
 **/
function tsm_google_fonts_select( $selected = '', $id = '', $class = '', $attrs = '', $echo = true ) {
    global $google_fonts_args;
    
    $default_fonts = array(
        'arial'     =>  array(
            'family'    =>  'Arial'
        ),
        'timesnewroman' =>  array(
            'family'    =>  'Times New Roman'
        )
    );
    
    // Try to load google font if not loaded before
    if ( empty( $google_fonts_args ) ):
    
        //$google_fonts = tsm_google_fonts();
        tsm_google_fonts();
    
    endif;
    
    $html = '';
    $default_fonts_html = '';
    $google_fonts_html = '';
    
    $default_fonts_html .= '<optgroup label=\'' . __( 'Default Fonts', 'tsm' ) . '\'>';
    foreach ( $default_fonts as $font_key => $font ):
        
        $default_fonts_html .= '<option data-font-source="default" ' . selected( $selected == $font['family'], true, false ) . ' value=\'' . $font['family'] . '\'>' . $font['family'] . '</option>';
    
    endforeach;
    $default_fonts_html .= '</optgroup>';
    
    if ( !empty( $google_fonts_args ) ):
        
        $google_fonts_html .= '<optgroup label=\'' . __( 'Google Fonts', 'tsm' ) . '\'>';
        foreach ( $google_fonts_args as $gfont_key => $gfont ):
            
            $google_fonts_html .= '<option data-font-source="google" ' . selected( $selected == $gfont['family'], true, false ) . ' value=\'' . $gfont['family'] . '\'>' . $gfont['family'] . '</option>';
            
        endforeach;
        $google_fonts_html .= '</optgroup>';
    
    else:
    
        $google_fonts_html .= '<optgroup label=\'' . __( 'Google Fonts', 'tsm' ) . '\'>';
        $google_fonts_html .= '<option value=\'\'>' . __( 'Sory, Google Fonts did\'t load.' ) . '</option>';
        $google_fonts_html .= '</optgroup>';
    
    endif;
    
    $html_id = '';
    $id = esc_attr( $id );
    if ( $id != '' ):
        $html_id = 'id=\'' . $id . '\'';
    endif;
    
    $html .= '<select ' . $html_id . ' class=\'tsm-select tsm-select2 tsm-google-font-select ' . $class . '\' ' . $attrs . '>';
    $html .= '<option value=\'\'></option>';
    $html .= $default_fonts_html . $google_fonts_html;
    $html .= '</select>';
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
}

/**
 *  Google args 
 *  @since 1.0 
 **/
function tsm_google_fonts(){
    
    //$google_fonts_api_link = 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyDfVXdKIdr1b55qYaJlw--zgsM2SanlzEo';

    if ( file_exists( TSM_DIR . '/inc/google-fonts.php' ) ):
        
        require_once TSM_DIR . '/inc/google-fonts.php';
                
    endif;
    
}
//add_action( 'init', 'tsm_google_fonts', 1 );

/**
 *  Load google font url
 *  @since 1.0 
 *  @param $fonts: string
 **/

function tsm_google_font_url( $fonts = '' ){
    
    $fonts_url = '';
    
    if ( $fonts != '' ):
        
        if ( file_exists( TSM_DIR . '/inc/google-fonts.php' ) ):
            
            require_once TSM_DIR . '/inc/google-fonts.php';
            
        endif;
        
        global $google_fonts_args;
        
        $font_families = array();
        $fonts_args = explode( ',', $fonts );
        
        foreach ( $fonts_args as $font ):
            
            $font = sanitize_key( $font );
            
            $font_vars = '';
                
            if ( isset( $google_fonts_args[$font]['vars'] ) && ( isset( $google_fonts_args[$font]['family'] ) ) ):
                
                $font_vars = implode( ',', $google_fonts_args[$font]['vars'] );
                $font_families[] = $google_fonts_args[$font]['family'] . ':' . $font_vars;
                
            endif;
            
            
        endforeach;
        
        $query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,vietnamese,greek-ext,latin-ext,cyrillic-ext,cyrillic,greek' ),
		);
        $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
        
    endif;
    
    return $fonts_url;
}

