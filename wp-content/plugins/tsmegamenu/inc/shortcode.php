<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


// Link/Image shortcode
function tsm_shortcode_tsm_link_img( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'link'               => '',
			'text'               => '',
			'img'                => '',
            'target'             => '_blank',
            'class'              => '',
            'addition_class'     => ''
		), $atts )
	);
    
    $link = esc_url( urldecode( $link  ) );
    $text = sanitize_text_field( urldecode( $text ) );
    $img = intval( $img );  // Image id
    $target = esc_attr( $target );
    $class = trim( wp_strip_all_tags( $class, true ) );
    $addition_class = trim( wp_strip_all_tags( $addition_class, true ) );
    
    $html = '';
    $a_start_html = '';
    $a_end_html = '';
    $text_html = '';
    $img_html = '';
    
    if ( $link != '' ):
        
        $addition_class .= ' tsm-has-link';
        $a_start_html = '<a href="' . $link . '" title="' . $text . '">';
        $a_end_html = '</a>';
    
    else:
        
        $addition_class .= ' tsm-no-link';
        
    endif;
    
    if ( $text != '' ):
        
        $addition_class .= ' tsm-has-text';
        $text_html = '<span class="tsm-link-text">' . $text . '</span>';
    
    else:
    
        $addition_class .= ' tsm-no-text';
        
    endif;
    
    if ( $img > 0 ):
        
        $addition_class .= ' tsm-has-img';
        $thumb = wp_get_attachment_image_src( $img, 'full' );
        $img_src = $thumb['0'];
        
        $img_html = '<img class="tsm-img" src="' . $img_src . '" alt="" />
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
    
    else:
        
        $addition_class .= ' tsm-no-img';
        
    endif;
    
    
    $html .=    '<div class="tsm-mega-menu-item-wrap ' . $class . ' ' . $addition_class . '">
                    ' . $a_start_html . '
                    ' . $img_html . '
                    ' . $text_html . '
                    ' . $a_end_html . '
                </div><!-- .tsm-mega-menu-item-wrap -->';
    
    return $html;
}
add_shortcode( 'tsm_link_img', 'tsm_shortcode_tsm_link_img' );


// Posttype Link/Image shortcode
function tsm_shortcode_tsm_posttype_link_img( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'posttype_id'        => 0,
			'text'               => '',
			'img'                => '',
            'target'             => '_blank',
            'class'              => '',
            'addition_class'     => ''
		), $atts )
	);
    
    $posttype_id = intval( $posttype_id  );
    $text = sanitize_text_field( urldecode( $text ) );
    $img = intval( $img );  // Image id
    $target = esc_attr( $target );
    $class = trim( wp_strip_all_tags( $class, true ) );
    $addition_class = trim( wp_strip_all_tags( $addition_class, true ) );
    
    $html = '';
    $a_start_html = '';
    $a_end_html = '';
    $text_html = '';
    $img_html = '';
    
    if ( $posttype_id > 0 ):
        
        $addition_class .= ' tsm-has-link';
        $a_start_html = '<a href="' . get_permalink( $posttype_id ) . '" title="' . $text . '">';
        $a_end_html = '</a>';
    
    else:
        
        $addition_class .= ' tsm-no-link';
        
    endif;
    
    if ( $text != '' ):
        
        $addition_class .= ' tsm-has-text';
        $text_html = '<span class="tsm-link-text">' . $text . '</span>';
    
    else:
    
        $addition_class .= ' tsm-no-text';
        
    endif;
    
    if ( $img > 0 ):
        
        $addition_class .= ' tsm-has-img';
        $thumb = wp_get_attachment_image_src( $img, 'full' );
        $img_src = $thumb['0'];
        
        $img_html = '<img class="tsm-img" src="' . $img_src . '" alt="" />
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
    
    else:
        
        $addition_class .= ' tsm-no-img';
        
    endif;
    
    
    $html .=    '<div class="tsm-mega-menu-item-wrap tsm-posttype-link ' . $class . ' ' . $addition_class . '">
                    ' . $a_start_html . '
                    ' . $img_html . '
                    ' . $text_html . '
                    ' . $a_end_html . '
                </div><!-- .tsm-mega-menu-item-wrap -->';
    
    return $html;
}
add_shortcode( 'tsm_posttype_link_img', 'tsm_shortcode_tsm_posttype_link_img' );

// Term Link/Image shortcode
function tsm_shortcode_tsm_term_link_img( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
			'term_id'            => 0,
            'tax'                => '',
			'text'               => '',
			'img'                => '',
            'target'             => '_blank',
            'class'              => '',
            'addition_class'     => ''
		), $atts )
	);
    
    $term_id = intval( $term_id  );
    $tax = wp_strip_all_tags( $tax );
    $text = sanitize_text_field( urldecode( $text ) );
    $img = intval( $img );  // Image id
    $target = esc_attr( $target );
    $class = trim( wp_strip_all_tags( $class, true ) );
    $addition_class = trim( wp_strip_all_tags( $addition_class, true ) );
    
    $html = '';
    $a_start_html = '';
    $a_end_html = '';
    $text_html = '';
    $img_html = '';
    
    if ( $term_id > 0 ):
        
        $term_link = get_term_link( $term_id, $tax );
        if ( is_wp_error( $term_link ) ):
            $term_link = '';
        endif;
        
        if ( $term_link != '' ):
            $addition_class .= ' tsm-has-link';
            $a_start_html = '<a href="' . esc_url( $term_link ) . '" title="' . $text . '">';
            $a_end_html = '</a>';
        endif;
    
    else:
        
        $addition_class .= ' tsm-no-link';
        
    endif;
    
    if ( $text != '' ):
        
        $addition_class .= ' tsm-has-text';
        $text_html = '<span class="tsm-link-text">' . $text . '</span>';
    
    else:
    
        $addition_class .= ' tsm-no-text';
        
    endif;
    
    if ( $img > 0 ):
        
        $addition_class .= ' tsm-has-img';
        $thumb = wp_get_attachment_image_src( $img, 'full' );
        $img_src = $thumb['0'];
        
        $img_html = '<img class="tsm-img" src="' . $img_src . '" alt="" />
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
    
    else:
        
        $addition_class .= ' tsm-no-img';
        
    endif;
    
    
    $html .=    '<div data-term-id="' . $term_id . '" class="tsm-mega-menu-item-wrap tsm-term-link ' . $class . ' ' . $addition_class . '">
                    ' . $a_start_html . '
                    ' . $img_html . '
                    ' . $text_html . '
                    ' . $a_end_html . '
                </div><!-- .tsm-mega-menu-item-wrap -->';
    
    return $html;
}
add_shortcode( 'tsm_term_link_img', 'tsm_shortcode_tsm_term_link_img' );


/**
 *  Editor (Html content) shortcode
 *  @since 1.0 
 **/
function tsm_shortcode_text_html( $atts ) {
    
    // Attributes
	extract( shortcode_atts(
		array(
            'title'             =>  '',
			'editor'             => '',
            'class'              => '',
			'addition_class'     => ''
		), $atts )
	);
    
    $title  = apply_filters( 'tsm_title', $title );
    $editor_content = $editor;
    $class = trim( wp_strip_all_tags( $class, true ) );
    $addition_class = trim( wp_strip_all_tags( $addition_class, true ) );
    
    $html = '';
    
    $html .=    '<div class="tsm-mega-menu-item-wrap tsm-text-html-item ' . $class . ' ' . $addition_class . '">
                    <h2 class="tsm-title header">' . urldecode( $title ) . '</h2>
                    ' . wpautop ( do_shortcode( urldecode( $editor_content ) ) ) . '
                </div><!-- .tsm-mega-menu-item-wrap -->';
    
    return $html;
}
add_shortcode( 'tsm_text_html', 'tsm_shortcode_text_html' );

/**
 *  Default elem shortcode
 *  @since 1.0 
 **/
function tsm_shortcode_default( $atts ) {
   
    ob_start();
    echo "<pre>";
    print_r( $atts );
    echo "</pre>";
    return ob_get_clean(); 
    
}
add_shortcode( 'tsm_default_short_code', 'tsm_shortcode_default' );


