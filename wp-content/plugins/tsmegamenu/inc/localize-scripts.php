<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

global $tsm_script_localize, $tsm_list_data_for_add_new, $tsm_item_data_for_add_new, $tsm_default_list_w;

$tsm_default_list_w = 250;

$tsm_item_data_for_add_new = array(
    'link'                  =>  '#',
    'text'                  =>  __( 'New Item', 'tsm' ),
    'right_align_dropdown'  =>  'no',
    'addition_class'        =>  '',
    'children'              =>  array()  // Sub list
);

$tsm_list_data_for_add_new = array(
    'list_w'            =>  $tsm_default_list_w,
    'addition_class'    =>  '',
    'items_data'        =>  array(
        $tsm_item_data_for_add_new
    )
);

// tsm_script_localize
$tsm_script_localize = array(
    'text'      =>  array(
        'choose_img'        =>  __( 'Choose image', 'tsm' ),
        'no_icon'           =>  __( 'No icon', 'tsm' ),
        'use_dropdown'      =>  __( 'Switch to Dropdown Type', 'tsm' ),
        'use_grid'          =>  __( 'Switch to Grid Type', 'tsm' ),
        'new_list_text'     =>  __( 'Enter list width' ),
        'max_total_list_width'  =>  __( 'Maximum total list width is ', 'tsm' ) . ' 1200px',
        'new_temp_name'     =>  __( 'New Template Name', 'tsm' ),
        'new_item'          =>  __( 'New Item', 'tsm' ),
        'custom'            =>  __( 'Custom', 'tsm' ),
        'custom_col_class'  =>  __( 'Enter column custom class', 'tsm' ),
        'custom_row_class'  =>  __( 'Enter row custom class', 'tsm' ),
        'this_menu_css_temp'=>  __( 'This Menu CSS Template', 'tsm' ),
        'hidden_accordion'  =>  __( 'Hidden Accordion', 'tsm' ),
        'show_accordion'    =>  __( 'Show Accordion', 'tsm' ),
        'prepare_saving'    =>  __( 'Prepare data for saving', 'tsm' ),
        'saving'            =>  __( 'Saving...', 'tsm' ),
        'tsm_saved'         =>  __( 'Mega menu saved', 'tsm' ),
        'tsm_loading'       =>  __( 'Loading mega menu...', 'tsm' ),
        'saving_take_too_long'  =>  __( 'Saving take too long!', 'tsm' ),
        'wait_for_preview_loaded'   =>  __( 'Preview menu is still loading... Please wait for a moment!', 'tsm' ),
        'updating_current_li'       =>  __( 'Updating current editing li', 'tsm' ),
        'updating_last_list_lv_0'   =>  __( 'Updating last list level 0', 'tsm' ),
        'updating_list_level'       =>  __( 'Updating list level', 'tsm' ),
        'updating_sublist_level'    =>  __( 'Updating sub list level', 'tsm' ),
        'loading_live_css_editor'   =>  __( 'Loading live css editor...', 'tsm' ),
        'updating_live_css_editor_control'  =>  __( 'Updating live css editor control', 'tsm' ),
        'updating_control_section_prop_val' =>  __( 'Updating control section properties value', 'tsm' ),
        'updating_live_preview_val_ui'      =>  __( 'Updating live preview values and UI', 'tsm' ),
        'loading_preview_menu'              =>  __( 'Loading preview menu...', 'tsm' ),
        'updating_live_preview_css'         =>  __( 'Updating live preview css', 'tsm' ),
        'updating_sortable'                 =>  __( 'Updating sortable', 'tsm' ),
        'updating_chosen_style'             =>  __( 'Updating chosen style', 'tsm' ),
        'updating_accordion_style'          =>  __( 'Updating accordion style', 'tsm' ),
        'updating_color_picker'             =>  __( 'Updating color picker', 'tsm' )
    ),
    'confirm'   =>  array(
        'del_elem'          =>  __( 'Are you sure you want to delete this element?', 'tsm' ),
        'del_widget'        =>  __( 'Are you sure you want to delete this widget?', 'tsm' ),
        'del_col'           =>  __( 'Are you sure you want to delete this column?', 'tsm' ),
        'del_row'           =>  __( 'Are you sure you want to delete this row?', 'tsm' ),
        'del_li_item'       =>  __( 'Are you sure you want to delete this item?', 'tsm' ),
        'del_list'          =>  __( 'Are you sure you want to delete this list?', 'tsm' ),
        'del_css_temp'      =>  __( 'Are you sure you want to delete this CSS Template?', 'tsm' )
    ),
    'vars'      =>  array(
        'default_list_w'    =>  $tsm_default_list_w,
        'plugin_url'        =>  TSM_URL
    ),
    'html'      =>  array(
        'list'              =>  tsm_list_form( $tsm_list_data_for_add_new, 1, false ),
        'list_item'         =>  tsm_item_for_list_form( $tsm_item_data_for_add_new, '', false ), // li item of a list type
        'list_lv_0'         =>  tsm_list_form( $tsm_list_data_for_add_new, 0, false ),
        'edit_li_form'      =>  '', //tsm_edit_dropdown_list_li( false ),
        'edit_li_control'   =>  tsm_list_li_actions_form( false ),
        'live_css_control_section_forms'    =>  tsm_live_css_control_section_form(),
        'live_preview_base_html'    =>  tsm_live_preview_base_html( false )
    )
);
