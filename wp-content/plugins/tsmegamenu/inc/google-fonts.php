<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

global $google_fonts_args;

$google_fonts_args = array(
    'abeezee' => array(
    'family' => 'ABeeZee',
    'vars' => array('regular','italic'),
    ),
    'abel' => array(
    'family' => 'Abel',
    'vars' => array('regular'),
    ),
    'abrilfatface' => array(
    'family' => 'Abril Fatface',
    'vars' => array('regular'),
    ),
    'aclonica' => array(
    'family' => 'Aclonica',
    'vars' => array('regular'),
    ),
    'acme' => array(
    'family' => 'Acme',
    'vars' => array('regular'),
    ),
    'actor' => array(
    'family' => 'Actor',
    'vars' => array('regular'),
    ),
    'adamina' => array(
    'family' => 'Adamina',
    'vars' => array('regular'),
    ),
    'adventpro' => array(
    'family' => 'Advent Pro',
    'vars' => array('100','200','300','regular','500','600','700'),
    ),
    'aguafinascript' => array(
    'family' => 'Aguafina Script',
    'vars' => array('regular'),
    ),
    'akronim' => array(
    'family' => 'Akronim',
    'vars' => array('regular'),
    ),
    'aladin' => array(
    'family' => 'Aladin',
    'vars' => array('regular'),
    ),
    'aldrich' => array(
    'family' => 'Aldrich',
    'vars' => array('regular'),
    ),
    'alef' => array(
    'family' => 'Alef',
    'vars' => array('regular','700'),
    ),
    'alegreya' => array(
    'family' => 'Alegreya',
    'vars' => array('regular','italic','700','700italic','900','900italic'),
    ),
    'alegreyasc' => array(
    'family' => 'Alegreya SC',
    'vars' => array('regular','italic','700','700italic','900','900italic'),
    ),
    'alegreyasans' => array(
    'family' => 'Alegreya Sans',
    'vars' => array('100','100italic','300','300italic','regular','italic','500','500italic','700','700italic','800','800italic','900','900italic'),
    ),
    'alegreyasanssc' => array(
    'family' => 'Alegreya Sans SC',
    'vars' => array('100','100italic','300','300italic','regular','italic','500','500italic','700','700italic','800','800italic','900','900italic'),
    ),
    'alexbrush' => array(
    'family' => 'Alex Brush',
    'vars' => array('regular'),
    ),
    'alfaslabone' => array(
    'family' => 'Alfa Slab One',
    'vars' => array('regular'),
    ),
    'alice' => array(
    'family' => 'Alice',
    'vars' => array('regular'),
    ),
    'alike' => array(
    'family' => 'Alike',
    'vars' => array('regular'),
    ),
    'alikeangular' => array(
    'family' => 'Alike Angular',
    'vars' => array('regular'),
    ),
    'allan' => array(
    'family' => 'Allan',
    'vars' => array('regular','700'),
    ),
    'allerta' => array(
    'family' => 'Allerta',
    'vars' => array('regular'),
    ),
    'allertastencil' => array(
    'family' => 'Allerta Stencil',
    'vars' => array('regular'),
    ),
    'allura' => array(
    'family' => 'Allura',
    'vars' => array('regular'),
    ),
    'almendra' => array(
    'family' => 'Almendra',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'almendradisplay' => array(
    'family' => 'Almendra Display',
    'vars' => array('regular'),
    ),
    'almendrasc' => array(
    'family' => 'Almendra SC',
    'vars' => array('regular'),
    ),
    'amarante' => array(
    'family' => 'Amarante',
    'vars' => array('regular'),
    ),
    'amaranth' => array(
    'family' => 'Amaranth',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'amaticsc' => array(
    'family' => 'Amatic SC',
    'vars' => array('regular','700'),
    ),
    'amethysta' => array(
    'family' => 'Amethysta',
    'vars' => array('regular'),
    ),
    'anaheim' => array(
    'family' => 'Anaheim',
    'vars' => array('regular'),
    ),
    'andada' => array(
    'family' => 'Andada',
    'vars' => array('regular'),
    ),
    'andika' => array(
    'family' => 'Andika',
    'vars' => array('regular'),
    ),
    'angkor' => array(
    'family' => 'Angkor',
    'vars' => array('regular'),
    ),
    'annieuseyourtelescope' => array(
    'family' => 'Annie Use Your Telescope',
    'vars' => array('regular'),
    ),
    'anonymouspro' => array(
    'family' => 'Anonymous Pro',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'antic' => array(
    'family' => 'Antic',
    'vars' => array('regular'),
    ),
    'anticdidone' => array(
    'family' => 'Antic Didone',
    'vars' => array('regular'),
    ),
    'anticslab' => array(
    'family' => 'Antic Slab',
    'vars' => array('regular'),
    ),
    'anton' => array(
    'family' => 'Anton',
    'vars' => array('regular'),
    ),
    'arapey' => array(
    'family' => 'Arapey',
    'vars' => array('regular','italic'),
    ),
    'arbutus' => array(
    'family' => 'Arbutus',
    'vars' => array('regular'),
    ),
    'arbutusslab' => array(
    'family' => 'Arbutus Slab',
    'vars' => array('regular'),
    ),
    'architectsdaughter' => array(
    'family' => 'Architects Daughter',
    'vars' => array('regular'),
    ),
    'archivoblack' => array(
    'family' => 'Archivo Black',
    'vars' => array('regular'),
    ),
    'archivonarrow' => array(
    'family' => 'Archivo Narrow',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'arimo' => array(
    'family' => 'Arimo',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'arizonia' => array(
    'family' => 'Arizonia',
    'vars' => array('regular'),
    ),
    'armata' => array(
    'family' => 'Armata',
    'vars' => array('regular'),
    ),
    'artifika' => array(
    'family' => 'Artifika',
    'vars' => array('regular'),
    ),
    'arvo' => array(
    'family' => 'Arvo',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'asap' => array(
    'family' => 'Asap',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'asset' => array(
    'family' => 'Asset',
    'vars' => array('regular'),
    ),
    'astloch' => array(
    'family' => 'Astloch',
    'vars' => array('regular','700'),
    ),
    'asul' => array(
    'family' => 'Asul',
    'vars' => array('regular','700'),
    ),
    'atomicage' => array(
    'family' => 'Atomic Age',
    'vars' => array('regular'),
    ),
    'aubrey' => array(
    'family' => 'Aubrey',
    'vars' => array('regular'),
    ),
    'audiowide' => array(
    'family' => 'Audiowide',
    'vars' => array('regular'),
    ),
    'autourone' => array(
    'family' => 'Autour One',
    'vars' => array('regular'),
    ),
    'average' => array(
    'family' => 'Average',
    'vars' => array('regular'),
    ),
    'averagesans' => array(
    'family' => 'Average Sans',
    'vars' => array('regular'),
    ),
    'averiagruesalibre' => array(
    'family' => 'Averia Gruesa Libre',
    'vars' => array('regular'),
    ),
    'averialibre' => array(
    'family' => 'Averia Libre',
    'vars' => array('300','300italic','regular','italic','700','700italic'),
    ),
    'averiasanslibre' => array(
    'family' => 'Averia Sans Libre',
    'vars' => array('300','300italic','regular','italic','700','700italic'),
    ),
    'averiaseriflibre' => array(
    'family' => 'Averia Serif Libre',
    'vars' => array('300','300italic','regular','italic','700','700italic'),
    ),
    'badscript' => array(
    'family' => 'Bad Script',
    'vars' => array('regular'),
    ),
    'balthazar' => array(
    'family' => 'Balthazar',
    'vars' => array('regular'),
    ),
    'bangers' => array(
    'family' => 'Bangers',
    'vars' => array('regular'),
    ),
    'basic' => array(
    'family' => 'Basic',
    'vars' => array('regular'),
    ),
    'battambang' => array(
    'family' => 'Battambang',
    'vars' => array('regular','700'),
    ),
    'baumans' => array(
    'family' => 'Baumans',
    'vars' => array('regular'),
    ),
    'bayon' => array(
    'family' => 'Bayon',
    'vars' => array('regular'),
    ),
    'belgrano' => array(
    'family' => 'Belgrano',
    'vars' => array('regular'),
    ),
    'belleza' => array(
    'family' => 'Belleza',
    'vars' => array('regular'),
    ),
    'benchnine' => array(
    'family' => 'BenchNine',
    'vars' => array('300','regular','700'),
    ),
    'bentham' => array(
    'family' => 'Bentham',
    'vars' => array('regular'),
    ),
    'berkshireswash' => array(
    'family' => 'Berkshire Swash',
    'vars' => array('regular'),
    ),
    'bevan' => array(
    'family' => 'Bevan',
    'vars' => array('regular'),
    ),
    'bigelowrules' => array(
    'family' => 'Bigelow Rules',
    'vars' => array('regular'),
    ),
    'bigshotone' => array(
    'family' => 'Bigshot One',
    'vars' => array('regular'),
    ),
    'bilbo' => array(
    'family' => 'Bilbo',
    'vars' => array('regular'),
    ),
    'bilboswashcaps' => array(
    'family' => 'Bilbo Swash Caps',
    'vars' => array('regular'),
    ),
    'bitter' => array(
    'family' => 'Bitter',
    'vars' => array('regular','italic','700'),
    ),
    'blackopsone' => array(
    'family' => 'Black Ops One',
    'vars' => array('regular'),
    ),
    'bokor' => array(
    'family' => 'Bokor',
    'vars' => array('regular'),
    ),
    'bonbon' => array(
    'family' => 'Bonbon',
    'vars' => array('regular'),
    ),
    'boogaloo' => array(
    'family' => 'Boogaloo',
    'vars' => array('regular'),
    ),
    'bowlbyone' => array(
    'family' => 'Bowlby One',
    'vars' => array('regular'),
    ),
    'bowlbyonesc' => array(
    'family' => 'Bowlby One SC',
    'vars' => array('regular'),
    ),
    'brawler' => array(
    'family' => 'Brawler',
    'vars' => array('regular'),
    ),
    'breeserif' => array(
    'family' => 'Bree Serif',
    'vars' => array('regular'),
    ),
    'bubblegumsans' => array(
    'family' => 'Bubblegum Sans',
    'vars' => array('regular'),
    ),
    'bubblerone' => array(
    'family' => 'Bubbler One',
    'vars' => array('regular'),
    ),
    'buda' => array(
    'family' => 'Buda',
    'vars' => array('300'),
    ),
    'buenard' => array(
    'family' => 'Buenard',
    'vars' => array('regular','700'),
    ),
    'butcherman' => array(
    'family' => 'Butcherman',
    'vars' => array('regular'),
    ),
    'butterflykids' => array(
    'family' => 'Butterfly Kids',
    'vars' => array('regular'),
    ),
    'cabin' => array(
    'family' => 'Cabin',
    'vars' => array('regular','italic','500','500italic','600','600italic','700','700italic'),
    ),
    'cabincondensed' => array(
    'family' => 'Cabin Condensed',
    'vars' => array('regular','500','600','700'),
    ),
    'cabinsketch' => array(
    'family' => 'Cabin Sketch',
    'vars' => array('regular','700'),
    ),
    'caesardressing' => array(
    'family' => 'Caesar Dressing',
    'vars' => array('regular'),
    ),
    'cagliostro' => array(
    'family' => 'Cagliostro',
    'vars' => array('regular'),
    ),
    'calligraffitti' => array(
    'family' => 'Calligraffitti',
    'vars' => array('regular'),
    ),
    'cambo' => array(
    'family' => 'Cambo',
    'vars' => array('regular'),
    ),
    'candal' => array(
    'family' => 'Candal',
    'vars' => array('regular'),
    ),
    'cantarell' => array(
    'family' => 'Cantarell',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'cantataone' => array(
    'family' => 'Cantata One',
    'vars' => array('regular'),
    ),
    'cantoraone' => array(
    'family' => 'Cantora One',
    'vars' => array('regular'),
    ),
    'capriola' => array(
    'family' => 'Capriola',
    'vars' => array('regular'),
    ),
    'cardo' => array(
    'family' => 'Cardo',
    'vars' => array('regular','italic','700'),
    ),
    'carme' => array(
    'family' => 'Carme',
    'vars' => array('regular'),
    ),
    'carroisgothic' => array(
    'family' => 'Carrois Gothic',
    'vars' => array('regular'),
    ),
    'carroisgothicsc' => array(
    'family' => 'Carrois Gothic SC',
    'vars' => array('regular'),
    ),
    'carterone' => array(
    'family' => 'Carter One',
    'vars' => array('regular'),
    ),
    'caudex' => array(
    'family' => 'Caudex',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'cedarvillecursive' => array(
    'family' => 'Cedarville Cursive',
    'vars' => array('regular'),
    ),
    'cevicheone' => array(
    'family' => 'Ceviche One',
    'vars' => array('regular'),
    ),
    'changaone' => array(
    'family' => 'Changa One',
    'vars' => array('regular','italic'),
    ),
    'chango' => array(
    'family' => 'Chango',
    'vars' => array('regular'),
    ),
    'chauphilomeneone' => array(
    'family' => 'Chau Philomene One',
    'vars' => array('regular','italic'),
    ),
    'chelaone' => array(
    'family' => 'Chela One',
    'vars' => array('regular'),
    ),
    'chelseamarket' => array(
    'family' => 'Chelsea Market',
    'vars' => array('regular'),
    ),
    'chenla' => array(
    'family' => 'Chenla',
    'vars' => array('regular'),
    ),
    'cherrycreamsoda' => array(
    'family' => 'Cherry Cream Soda',
    'vars' => array('regular'),
    ),
    'cherryswash' => array(
    'family' => 'Cherry Swash',
    'vars' => array('regular','700'),
    ),
    'chewy' => array(
    'family' => 'Chewy',
    'vars' => array('regular'),
    ),
    'chicle' => array(
    'family' => 'Chicle',
    'vars' => array('regular'),
    ),
    'chivo' => array(
    'family' => 'Chivo',
    'vars' => array('regular','italic','900','900italic'),
    ),
    'cinzel' => array(
    'family' => 'Cinzel',
    'vars' => array('regular','700','900'),
    ),
    'cinzeldecorative' => array(
    'family' => 'Cinzel Decorative',
    'vars' => array('regular','700','900'),
    ),
    'clickerscript' => array(
    'family' => 'Clicker Script',
    'vars' => array('regular'),
    ),
    'coda' => array(
    'family' => 'Coda',
    'vars' => array('regular','800'),
    ),
    'codacaption' => array(
    'family' => 'Coda Caption',
    'vars' => array('800'),
    ),
    'codystar' => array(
    'family' => 'Codystar',
    'vars' => array('300','regular'),
    ),
    'combo' => array(
    'family' => 'Combo',
    'vars' => array('regular'),
    ),
    'comfortaa' => array(
    'family' => 'Comfortaa',
    'vars' => array('300','regular','700'),
    ),
    'comingsoon' => array(
    'family' => 'Coming Soon',
    'vars' => array('regular'),
    ),
    'concertone' => array(
    'family' => 'Concert One',
    'vars' => array('regular'),
    ),
    'condiment' => array(
    'family' => 'Condiment',
    'vars' => array('regular'),
    ),
    'content' => array(
    'family' => 'Content',
    'vars' => array('regular','700'),
    ),
    'contrailone' => array(
    'family' => 'Contrail One',
    'vars' => array('regular'),
    ),
    'convergence' => array(
    'family' => 'Convergence',
    'vars' => array('regular'),
    ),
    'cookie' => array(
    'family' => 'Cookie',
    'vars' => array('regular'),
    ),
    'copse' => array(
    'family' => 'Copse',
    'vars' => array('regular'),
    ),
    'corben' => array(
    'family' => 'Corben',
    'vars' => array('regular','700'),
    ),
    'courgette' => array(
    'family' => 'Courgette',
    'vars' => array('regular'),
    ),
    'cousine' => array(
    'family' => 'Cousine',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'coustard' => array(
    'family' => 'Coustard',
    'vars' => array('regular','900'),
    ),
    'coveredbyyourgrace' => array(
    'family' => 'Covered By Your Grace',
    'vars' => array('regular'),
    ),
    'craftygirls' => array(
    'family' => 'Crafty Girls',
    'vars' => array('regular'),
    ),
    'creepster' => array(
    'family' => 'Creepster',
    'vars' => array('regular'),
    ),
    'creteround' => array(
    'family' => 'Crete Round',
    'vars' => array('regular','italic'),
    ),
    'crimsontext' => array(
    'family' => 'Crimson Text',
    'vars' => array('regular','italic','600','600italic','700','700italic'),
    ),
    'croissantone' => array(
    'family' => 'Croissant One',
    'vars' => array('regular'),
    ),
    'crushed' => array(
    'family' => 'Crushed',
    'vars' => array('regular'),
    ),
    'cuprum' => array(
    'family' => 'Cuprum',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'cutive' => array(
    'family' => 'Cutive',
    'vars' => array('regular'),
    ),
    'cutivemono' => array(
    'family' => 'Cutive Mono',
    'vars' => array('regular'),
    ),
    'damion' => array(
    'family' => 'Damion',
    'vars' => array('regular'),
    ),
    'dancingscript' => array(
    'family' => 'Dancing Script',
    'vars' => array('regular','700'),
    ),
    'dangrek' => array(
    'family' => 'Dangrek',
    'vars' => array('regular'),
    ),
    'dawningofanewday' => array(
    'family' => 'Dawning of a New Day',
    'vars' => array('regular'),
    ),
    'daysone' => array(
    'family' => 'Days One',
    'vars' => array('regular'),
    ),
    'delius' => array(
    'family' => 'Delius',
    'vars' => array('regular'),
    ),
    'deliusswashcaps' => array(
    'family' => 'Delius Swash Caps',
    'vars' => array('regular'),
    ),
    'deliusunicase' => array(
    'family' => 'Delius Unicase',
    'vars' => array('regular','700'),
    ),
    'dellarespira' => array(
    'family' => 'Della Respira',
    'vars' => array('regular'),
    ),
    'denkone' => array(
    'family' => 'Denk One',
    'vars' => array('regular'),
    ),
    'devonshire' => array(
    'family' => 'Devonshire',
    'vars' => array('regular'),
    ),
    'didactgothic' => array(
    'family' => 'Didact Gothic',
    'vars' => array('regular'),
    ),
    'diplomata' => array(
    'family' => 'Diplomata',
    'vars' => array('regular'),
    ),
    'diplomatasc' => array(
    'family' => 'Diplomata SC',
    'vars' => array('regular'),
    ),
    'domine' => array(
    'family' => 'Domine',
    'vars' => array('regular','700'),
    ),
    'donegalone' => array(
    'family' => 'Donegal One',
    'vars' => array('regular'),
    ),
    'doppioone' => array(
    'family' => 'Doppio One',
    'vars' => array('regular'),
    ),
    'dorsa' => array(
    'family' => 'Dorsa',
    'vars' => array('regular'),
    ),
    'dosis' => array(
    'family' => 'Dosis',
    'vars' => array('200','300','regular','500','600','700','800'),
    ),
    'drsugiyama' => array(
    'family' => 'Dr Sugiyama',
    'vars' => array('regular'),
    ),
    'droidsans' => array(
    'family' => 'Droid Sans',
    'vars' => array('regular','700'),
    ),
    'droidsansmono' => array(
    'family' => 'Droid Sans Mono',
    'vars' => array('regular'),
    ),
    'droidserif' => array(
    'family' => 'Droid Serif',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'durusans' => array(
    'family' => 'Duru Sans',
    'vars' => array('regular'),
    ),
    'dynalight' => array(
    'family' => 'Dynalight',
    'vars' => array('regular'),
    ),
    'ebgaramond' => array(
    'family' => 'EB Garamond',
    'vars' => array('regular'),
    ),
    'eaglelake' => array(
    'family' => 'Eagle Lake',
    'vars' => array('regular'),
    ),
    'eater' => array(
    'family' => 'Eater',
    'vars' => array('regular'),
    ),
    'economica' => array(
    'family' => 'Economica',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'ekmukta' => array(
    'family' => 'Ek Mukta',
    'vars' => array('200','300','regular','500','600','700','800'),
    ),
    'electrolize' => array(
    'family' => 'Electrolize',
    'vars' => array('regular'),
    ),
    'elsie' => array(
    'family' => 'Elsie',
    'vars' => array('regular','900'),
    ),
    'elsieswashcaps' => array(
    'family' => 'Elsie Swash Caps',
    'vars' => array('regular','900'),
    ),
    'emblemaone' => array(
    'family' => 'Emblema One',
    'vars' => array('regular'),
    ),
    'emilyscandy' => array(
    'family' => 'Emilys Candy',
    'vars' => array('regular'),
    ),
    'engagement' => array(
    'family' => 'Engagement',
    'vars' => array('regular'),
    ),
    'englebert' => array(
    'family' => 'Englebert',
    'vars' => array('regular'),
    ),
    'enriqueta' => array(
    'family' => 'Enriqueta',
    'vars' => array('regular','700'),
    ),
    'ericaone' => array(
    'family' => 'Erica One',
    'vars' => array('regular'),
    ),
    'esteban' => array(
    'family' => 'Esteban',
    'vars' => array('regular'),
    ),
    'euphoriascript' => array(
    'family' => 'Euphoria Script',
    'vars' => array('regular'),
    ),
    'ewert' => array(
    'family' => 'Ewert',
    'vars' => array('regular'),
    ),
    'exo' => array(
    'family' => 'Exo',
    'vars' => array('100','100italic','200','200italic','300','300italic','regular','italic','500','500italic','600','600italic','700','700italic','800','800italic','900','900italic'),
    ),
    'exo2' => array(
    'family' => 'Exo 2',
    'vars' => array('100','100italic','200','200italic','300','300italic','regular','italic','500','500italic','600','600italic','700','700italic','800','800italic','900','900italic'),
    ),
    'expletussans' => array(
    'family' => 'Expletus Sans',
    'vars' => array('regular','italic','500','500italic','600','600italic','700','700italic'),
    ),
    'fanwoodtext' => array(
    'family' => 'Fanwood Text',
    'vars' => array('regular','italic'),
    ),
    'fascinate' => array(
    'family' => 'Fascinate',
    'vars' => array('regular'),
    ),
    'fascinateinline' => array(
    'family' => 'Fascinate Inline',
    'vars' => array('regular'),
    ),
    'fasterone' => array(
    'family' => 'Faster One',
    'vars' => array('regular'),
    ),
    'fasthand' => array(
    'family' => 'Fasthand',
    'vars' => array('regular'),
    ),
    'faunaone' => array(
    'family' => 'Fauna One',
    'vars' => array('regular'),
    ),
    'federant' => array(
    'family' => 'Federant',
    'vars' => array('regular'),
    ),
    'federo' => array(
    'family' => 'Federo',
    'vars' => array('regular'),
    ),
    'felipa' => array(
    'family' => 'Felipa',
    'vars' => array('regular'),
    ),
    'fenix' => array(
    'family' => 'Fenix',
    'vars' => array('regular'),
    ),
    'fingerpaint' => array(
    'family' => 'Finger Paint',
    'vars' => array('regular'),
    ),
    'firamono' => array(
    'family' => 'Fira Mono',
    'vars' => array('regular','700'),
    ),
    'firasans' => array(
    'family' => 'Fira Sans',
    'vars' => array('300','300italic','regular','italic','500','500italic','700','700italic'),
    ),
    'fjallaone' => array(
    'family' => 'Fjalla One',
    'vars' => array('regular'),
    ),
    'fjordone' => array(
    'family' => 'Fjord One',
    'vars' => array('regular'),
    ),
    'flamenco' => array(
    'family' => 'Flamenco',
    'vars' => array('300','regular'),
    ),
    'flavors' => array(
    'family' => 'Flavors',
    'vars' => array('regular'),
    ),
    'fondamento' => array(
    'family' => 'Fondamento',
    'vars' => array('regular','italic'),
    ),
    'fontdinerswanky' => array(
    'family' => 'Fontdiner Swanky',
    'vars' => array('regular'),
    ),
    'forum' => array(
    'family' => 'Forum',
    'vars' => array('regular'),
    ),
    'francoisone' => array(
    'family' => 'Francois One',
    'vars' => array('regular'),
    ),
    'freckleface' => array(
    'family' => 'Freckle Face',
    'vars' => array('regular'),
    ),
    'frederickathegreat' => array(
    'family' => 'Fredericka the Great',
    'vars' => array('regular'),
    ),
    'fredokaone' => array(
    'family' => 'Fredoka One',
    'vars' => array('regular'),
    ),
    'freehand' => array(
    'family' => 'Freehand',
    'vars' => array('regular'),
    ),
    'fresca' => array(
    'family' => 'Fresca',
    'vars' => array('regular'),
    ),
    'frijole' => array(
    'family' => 'Frijole',
    'vars' => array('regular'),
    ),
    'fruktur' => array(
    'family' => 'Fruktur',
    'vars' => array('regular'),
    ),
    'fugazone' => array(
    'family' => 'Fugaz One',
    'vars' => array('regular'),
    ),
    'gfsdidot' => array(
    'family' => 'GFS Didot',
    'vars' => array('regular'),
    ),
    'gfsneohellenic' => array(
    'family' => 'GFS Neohellenic',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'gabriela' => array(
    'family' => 'Gabriela',
    'vars' => array('regular'),
    ),
    'gafata' => array(
    'family' => 'Gafata',
    'vars' => array('regular'),
    ),
    'galdeano' => array(
    'family' => 'Galdeano',
    'vars' => array('regular'),
    ),
    'galindo' => array(
    'family' => 'Galindo',
    'vars' => array('regular'),
    ),
    'gentiumbasic' => array(
    'family' => 'Gentium Basic',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'gentiumbookbasic' => array(
    'family' => 'Gentium Book Basic',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'geo' => array(
    'family' => 'Geo',
    'vars' => array('regular','italic'),
    ),
    'geostar' => array(
    'family' => 'Geostar',
    'vars' => array('regular'),
    ),
    'geostarfill' => array(
    'family' => 'Geostar Fill',
    'vars' => array('regular'),
    ),
    'germaniaone' => array(
    'family' => 'Germania One',
    'vars' => array('regular'),
    ),
    'gildadisplay' => array(
    'family' => 'Gilda Display',
    'vars' => array('regular'),
    ),
    'giveyouglory' => array(
    'family' => 'Give You Glory',
    'vars' => array('regular'),
    ),
    'glassantiqua' => array(
    'family' => 'Glass Antiqua',
    'vars' => array('regular'),
    ),
    'glegoo' => array(
    'family' => 'Glegoo',
    'vars' => array('regular','700'),
    ),
    'gloriahallelujah' => array(
    'family' => 'Gloria Hallelujah',
    'vars' => array('regular'),
    ),
    'goblinone' => array(
    'family' => 'Goblin One',
    'vars' => array('regular'),
    ),
    'gochihand' => array(
    'family' => 'Gochi Hand',
    'vars' => array('regular'),
    ),
    'gorditas' => array(
    'family' => 'Gorditas',
    'vars' => array('regular','700'),
    ),
    'goudybookletter1911' => array(
    'family' => 'Goudy Bookletter 1911',
    'vars' => array('regular'),
    ),
    'graduate' => array(
    'family' => 'Graduate',
    'vars' => array('regular'),
    ),
    'grandhotel' => array(
    'family' => 'Grand Hotel',
    'vars' => array('regular'),
    ),
    'gravitasone' => array(
    'family' => 'Gravitas One',
    'vars' => array('regular'),
    ),
    'greatvibes' => array(
    'family' => 'Great Vibes',
    'vars' => array('regular'),
    ),
    'griffy' => array(
    'family' => 'Griffy',
    'vars' => array('regular'),
    ),
    'gruppo' => array(
    'family' => 'Gruppo',
    'vars' => array('regular'),
    ),
    'gudea' => array(
    'family' => 'Gudea',
    'vars' => array('regular','italic','700'),
    ),
    'habibi' => array(
    'family' => 'Habibi',
    'vars' => array('regular'),
    ),
    'halant' => array(
    'family' => 'Halant',
    'vars' => array('300','regular','500','600','700'),
    ),
    'hammersmithone' => array(
    'family' => 'Hammersmith One',
    'vars' => array('regular'),
    ),
    'hanalei' => array(
    'family' => 'Hanalei',
    'vars' => array('regular'),
    ),
    'hanaleifill' => array(
    'family' => 'Hanalei Fill',
    'vars' => array('regular'),
    ),
    'handlee' => array(
    'family' => 'Handlee',
    'vars' => array('regular'),
    ),
    'hanuman' => array(
    'family' => 'Hanuman',
    'vars' => array('regular','700'),
    ),
    'happymonkey' => array(
    'family' => 'Happy Monkey',
    'vars' => array('regular'),
    ),
    'headlandone' => array(
    'family' => 'Headland One',
    'vars' => array('regular'),
    ),
    'hennypenny' => array(
    'family' => 'Henny Penny',
    'vars' => array('regular'),
    ),
    'herrvonmuellerhoff' => array(
    'family' => 'Herr Von Muellerhoff',
    'vars' => array('regular'),
    ),
    'hind' => array(
    'family' => 'Hind',
    'vars' => array('300','regular','500','600','700'),
    ),
    'holtwoodonesc' => array(
    'family' => 'Holtwood One SC',
    'vars' => array('regular'),
    ),
    'homemadeapple' => array(
    'family' => 'Homemade Apple',
    'vars' => array('regular'),
    ),
    'homenaje' => array(
    'family' => 'Homenaje',
    'vars' => array('regular'),
    ),
    'imfelldwpica' => array(
    'family' => 'IM Fell DW Pica',
    'vars' => array('regular','italic'),
    ),
    'imfelldwpicasc' => array(
    'family' => 'IM Fell DW Pica SC',
    'vars' => array('regular'),
    ),
    'imfelldoublepica' => array(
    'family' => 'IM Fell Double Pica',
    'vars' => array('regular','italic'),
    ),
    'imfelldoublepicasc' => array(
    'family' => 'IM Fell Double Pica SC',
    'vars' => array('regular'),
    ),
    'imfellenglish' => array(
    'family' => 'IM Fell English',
    'vars' => array('regular','italic'),
    ),
    'imfellenglishsc' => array(
    'family' => 'IM Fell English SC',
    'vars' => array('regular'),
    ),
    'imfellfrenchcanon' => array(
    'family' => 'IM Fell French Canon',
    'vars' => array('regular','italic'),
    ),
    'imfellfrenchcanonsc' => array(
    'family' => 'IM Fell French Canon SC',
    'vars' => array('regular'),
    ),
    'imfellgreatprimer' => array(
    'family' => 'IM Fell Great Primer',
    'vars' => array('regular','italic'),
    ),
    'imfellgreatprimersc' => array(
    'family' => 'IM Fell Great Primer SC',
    'vars' => array('regular'),
    ),
    'iceberg' => array(
    'family' => 'Iceberg',
    'vars' => array('regular'),
    ),
    'iceland' => array(
    'family' => 'Iceland',
    'vars' => array('regular'),
    ),
    'imprima' => array(
    'family' => 'Imprima',
    'vars' => array('regular'),
    ),
    'inconsolata' => array(
    'family' => 'Inconsolata',
    'vars' => array('regular','700'),
    ),
    'inder' => array(
    'family' => 'Inder',
    'vars' => array('regular'),
    ),
    'indieflower' => array(
    'family' => 'Indie Flower',
    'vars' => array('regular'),
    ),
    'inika' => array(
    'family' => 'Inika',
    'vars' => array('regular','700'),
    ),
    'irishgrover' => array(
    'family' => 'Irish Grover',
    'vars' => array('regular'),
    ),
    'istokweb' => array(
    'family' => 'Istok Web',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'italiana' => array(
    'family' => 'Italiana',
    'vars' => array('regular'),
    ),
    'italianno' => array(
    'family' => 'Italianno',
    'vars' => array('regular'),
    ),
    'jacquesfrancois' => array(
    'family' => 'Jacques Francois',
    'vars' => array('regular'),
    ),
    'jacquesfrancoisshadow' => array(
    'family' => 'Jacques Francois Shadow',
    'vars' => array('regular'),
    ),
    'jimnightshade' => array(
    'family' => 'Jim Nightshade',
    'vars' => array('regular'),
    ),
    'jockeyone' => array(
    'family' => 'Jockey One',
    'vars' => array('regular'),
    ),
    'jollylodger' => array(
    'family' => 'Jolly Lodger',
    'vars' => array('regular'),
    ),
    'josefinsans' => array(
    'family' => 'Josefin Sans',
    'vars' => array('100','100italic','300','300italic','regular','italic','600','600italic','700','700italic'),
    ),
    'josefinslab' => array(
    'family' => 'Josefin Slab',
    'vars' => array('100','100italic','300','300italic','regular','italic','600','600italic','700','700italic'),
    ),
    'jotione' => array(
    'family' => 'Joti One',
    'vars' => array('regular'),
    ),
    'judson' => array(
    'family' => 'Judson',
    'vars' => array('regular','italic','700'),
    ),
    'julee' => array(
    'family' => 'Julee',
    'vars' => array('regular'),
    ),
    'juliussansone' => array(
    'family' => 'Julius Sans One',
    'vars' => array('regular'),
    ),
    'junge' => array(
    'family' => 'Junge',
    'vars' => array('regular'),
    ),
    'jura' => array(
    'family' => 'Jura',
    'vars' => array('300','regular','500','600'),
    ),
    'justanotherhand' => array(
    'family' => 'Just Another Hand',
    'vars' => array('regular'),
    ),
    'justmeagaindownhere' => array(
    'family' => 'Just Me Again Down Here',
    'vars' => array('regular'),
    ),
    'kalam' => array(
    'family' => 'Kalam',
    'vars' => array('300','regular','700'),
    ),
    'kameron' => array(
    'family' => 'Kameron',
    'vars' => array('regular','700'),
    ),
    'kantumruy' => array(
    'family' => 'Kantumruy',
    'vars' => array('300','regular','700'),
    ),
    'karla' => array(
    'family' => 'Karla',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'karma' => array(
    'family' => 'Karma',
    'vars' => array('300','regular','500','600','700'),
    ),
    'kaushanscript' => array(
    'family' => 'Kaushan Script',
    'vars' => array('regular'),
    ),
    'kavoon' => array(
    'family' => 'Kavoon',
    'vars' => array('regular'),
    ),
    'kdamthmor' => array(
    'family' => 'Kdam Thmor',
    'vars' => array('regular'),
    ),
    'keaniaone' => array(
    'family' => 'Keania One',
    'vars' => array('regular'),
    ),
    'kellyslab' => array(
    'family' => 'Kelly Slab',
    'vars' => array('regular'),
    ),
    'kenia' => array(
    'family' => 'Kenia',
    'vars' => array('regular'),
    ),
    'khand' => array(
    'family' => 'Khand',
    'vars' => array('300','regular','500','600','700'),
    ),
    'khmer' => array(
    'family' => 'Khmer',
    'vars' => array('regular'),
    ),
    'kiteone' => array(
    'family' => 'Kite One',
    'vars' => array('regular'),
    ),
    'knewave' => array(
    'family' => 'Knewave',
    'vars' => array('regular'),
    ),
    'kottaone' => array(
    'family' => 'Kotta One',
    'vars' => array('regular'),
    ),
    'koulen' => array(
    'family' => 'Koulen',
    'vars' => array('regular'),
    ),
    'kranky' => array(
    'family' => 'Kranky',
    'vars' => array('regular'),
    ),
    'kreon' => array(
    'family' => 'Kreon',
    'vars' => array('300','regular','700'),
    ),
    'kristi' => array(
    'family' => 'Kristi',
    'vars' => array('regular'),
    ),
    'kronaone' => array(
    'family' => 'Krona One',
    'vars' => array('regular'),
    ),
    'labelleaurore' => array(
    'family' => 'La Belle Aurore',
    'vars' => array('regular'),
    ),
    'laila' => array(
    'family' => 'Laila',
    'vars' => array('300','regular','500','600','700'),
    ),
    'lancelot' => array(
    'family' => 'Lancelot',
    'vars' => array('regular'),
    ),
    'lato' => array(
    'family' => 'Lato',
    'vars' => array('100','100italic','300','300italic','regular','italic','700','700italic','900','900italic'),
    ),
    'leaguescript' => array(
    'family' => 'League Script',
    'vars' => array('regular'),
    ),
    'leckerlione' => array(
    'family' => 'Leckerli One',
    'vars' => array('regular'),
    ),
    'ledger' => array(
    'family' => 'Ledger',
    'vars' => array('regular'),
    ),
    'lekton' => array(
    'family' => 'Lekton',
    'vars' => array('regular','italic','700'),
    ),
    'lemon' => array(
    'family' => 'Lemon',
    'vars' => array('regular'),
    ),
    'librebaskerville' => array(
    'family' => 'Libre Baskerville',
    'vars' => array('regular','italic','700'),
    ),
    'lifesavers' => array(
    'family' => 'Life Savers',
    'vars' => array('regular','700'),
    ),
    'lilitaone' => array(
    'family' => 'Lilita One',
    'vars' => array('regular'),
    ),
    'lilyscriptone' => array(
    'family' => 'Lily Script One',
    'vars' => array('regular'),
    ),
    'limelight' => array(
    'family' => 'Limelight',
    'vars' => array('regular'),
    ),
    'lindenhill' => array(
    'family' => 'Linden Hill',
    'vars' => array('regular','italic'),
    ),
    'lobster' => array(
    'family' => 'Lobster',
    'vars' => array('regular'),
    ),
    'lobstertwo' => array(
    'family' => 'Lobster Two',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'londrinaoutline' => array(
    'family' => 'Londrina Outline',
    'vars' => array('regular'),
    ),
    'londrinashadow' => array(
    'family' => 'Londrina Shadow',
    'vars' => array('regular'),
    ),
    'londrinasketch' => array(
    'family' => 'Londrina Sketch',
    'vars' => array('regular'),
    ),
    'londrinasolid' => array(
    'family' => 'Londrina Solid',
    'vars' => array('regular'),
    ),
    'lora' => array(
    'family' => 'Lora',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'loveyalikeasister' => array(
    'family' => 'Love Ya Like A Sister',
    'vars' => array('regular'),
    ),
    'lovedbytheking' => array(
    'family' => 'Loved by the King',
    'vars' => array('regular'),
    ),
    'loversquarrel' => array(
    'family' => 'Lovers Quarrel',
    'vars' => array('regular'),
    ),
    'luckiestguy' => array(
    'family' => 'Luckiest Guy',
    'vars' => array('regular'),
    ),
    'lusitana' => array(
    'family' => 'Lusitana',
    'vars' => array('regular','700'),
    ),
    'lustria' => array(
    'family' => 'Lustria',
    'vars' => array('regular'),
    ),
    'macondo' => array(
    'family' => 'Macondo',
    'vars' => array('regular'),
    ),
    'macondoswashcaps' => array(
    'family' => 'Macondo Swash Caps',
    'vars' => array('regular'),
    ),
    'magra' => array(
    'family' => 'Magra',
    'vars' => array('regular','700'),
    ),
    'maidenorange' => array(
    'family' => 'Maiden Orange',
    'vars' => array('regular'),
    ),
    'mako' => array(
    'family' => 'Mako',
    'vars' => array('regular'),
    ),
    'marcellus' => array(
    'family' => 'Marcellus',
    'vars' => array('regular'),
    ),
    'marcellussc' => array(
    'family' => 'Marcellus SC',
    'vars' => array('regular'),
    ),
    'marckscript' => array(
    'family' => 'Marck Script',
    'vars' => array('regular'),
    ),
    'margarine' => array(
    'family' => 'Margarine',
    'vars' => array('regular'),
    ),
    'markoone' => array(
    'family' => 'Marko One',
    'vars' => array('regular'),
    ),
    'marmelad' => array(
    'family' => 'Marmelad',
    'vars' => array('regular'),
    ),
    'marvel' => array(
    'family' => 'Marvel',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'mate' => array(
    'family' => 'Mate',
    'vars' => array('regular','italic'),
    ),
    'matesc' => array(
    'family' => 'Mate SC',
    'vars' => array('regular'),
    ),
    'mavenpro' => array(
    'family' => 'Maven Pro',
    'vars' => array('regular','500','700','900'),
    ),
    'mclaren' => array(
    'family' => 'McLaren',
    'vars' => array('regular'),
    ),
    'meddon' => array(
    'family' => 'Meddon',
    'vars' => array('regular'),
    ),
    'medievalsharp' => array(
    'family' => 'MedievalSharp',
    'vars' => array('regular'),
    ),
    'medulaone' => array(
    'family' => 'Medula One',
    'vars' => array('regular'),
    ),
    'megrim' => array(
    'family' => 'Megrim',
    'vars' => array('regular'),
    ),
    'meiescript' => array(
    'family' => 'Meie Script',
    'vars' => array('regular'),
    ),
    'merienda' => array(
    'family' => 'Merienda',
    'vars' => array('regular','700'),
    ),
    'meriendaone' => array(
    'family' => 'Merienda One',
    'vars' => array('regular'),
    ),
    'merriweather' => array(
    'family' => 'Merriweather',
    'vars' => array('300','300italic','regular','italic','700','700italic','900','900italic'),
    ),
    'merriweathersans' => array(
    'family' => 'Merriweather Sans',
    'vars' => array('300','300italic','regular','italic','700','700italic','800','800italic'),
    ),
    'metal' => array(
    'family' => 'Metal',
    'vars' => array('regular'),
    ),
    'metalmania' => array(
    'family' => 'Metal Mania',
    'vars' => array('regular'),
    ),
    'metamorphous' => array(
    'family' => 'Metamorphous',
    'vars' => array('regular'),
    ),
    'metrophobic' => array(
    'family' => 'Metrophobic',
    'vars' => array('regular'),
    ),
    'michroma' => array(
    'family' => 'Michroma',
    'vars' => array('regular'),
    ),
    'milonga' => array(
    'family' => 'Milonga',
    'vars' => array('regular'),
    ),
    'miltonian' => array(
    'family' => 'Miltonian',
    'vars' => array('regular'),
    ),
    'miltoniantattoo' => array(
    'family' => 'Miltonian Tattoo',
    'vars' => array('regular'),
    ),
    'miniver' => array(
    'family' => 'Miniver',
    'vars' => array('regular'),
    ),
    'missfajardose' => array(
    'family' => 'Miss Fajardose',
    'vars' => array('regular'),
    ),
    'modernantiqua' => array(
    'family' => 'Modern Antiqua',
    'vars' => array('regular'),
    ),
    'molengo' => array(
    'family' => 'Molengo',
    'vars' => array('regular'),
    ),
    'molle' => array(
    'family' => 'Molle',
    'vars' => array('italic'),
    ),
    'monda' => array(
    'family' => 'Monda',
    'vars' => array('regular','700'),
    ),
    'monofett' => array(
    'family' => 'Monofett',
    'vars' => array('regular'),
    ),
    'monoton' => array(
    'family' => 'Monoton',
    'vars' => array('regular'),
    ),
    'monsieurladoulaise' => array(
    'family' => 'Monsieur La Doulaise',
    'vars' => array('regular'),
    ),
    'montaga' => array(
    'family' => 'Montaga',
    'vars' => array('regular'),
    ),
    'montez' => array(
    'family' => 'Montez',
    'vars' => array('regular'),
    ),
    'montserrat' => array(
    'family' => 'Montserrat',
    'vars' => array('regular','700'),
    ),
    'montserratalternates' => array(
    'family' => 'Montserrat Alternates',
    'vars' => array('regular','700'),
    ),
    'montserratsubrayada' => array(
    'family' => 'Montserrat Subrayada',
    'vars' => array('regular','700'),
    ),
    'moul' => array(
    'family' => 'Moul',
    'vars' => array('regular'),
    ),
    'moulpali' => array(
    'family' => 'Moulpali',
    'vars' => array('regular'),
    ),
    'mountainsofchristmas' => array(
    'family' => 'Mountains of Christmas',
    'vars' => array('regular','700'),
    ),
    'mousememoirs' => array(
    'family' => 'Mouse Memoirs',
    'vars' => array('regular'),
    ),
    'mrbedfort' => array(
    'family' => 'Mr Bedfort',
    'vars' => array('regular'),
    ),
    'mrdafoe' => array(
    'family' => 'Mr Dafoe',
    'vars' => array('regular'),
    ),
    'mrdehaviland' => array(
    'family' => 'Mr De Haviland',
    'vars' => array('regular'),
    ),
    'mrssaintdelafield' => array(
    'family' => 'Mrs Saint Delafield',
    'vars' => array('regular'),
    ),
    'mrssheppards' => array(
    'family' => 'Mrs Sheppards',
    'vars' => array('regular'),
    ),
    'muli' => array(
    'family' => 'Muli',
    'vars' => array('300','300italic','regular','italic'),
    ),
    'mysteryquest' => array(
    'family' => 'Mystery Quest',
    'vars' => array('regular'),
    ),
    'neucha' => array(
    'family' => 'Neucha',
    'vars' => array('regular'),
    ),
    'neuton' => array(
    'family' => 'Neuton',
    'vars' => array('200','300','regular','italic','700','800'),
    ),
    'newrocker' => array(
    'family' => 'New Rocker',
    'vars' => array('regular'),
    ),
    'newscycle' => array(
    'family' => 'News Cycle',
    'vars' => array('regular','700'),
    ),
    'niconne' => array(
    'family' => 'Niconne',
    'vars' => array('regular'),
    ),
    'nixieone' => array(
    'family' => 'Nixie One',
    'vars' => array('regular'),
    ),
    'nobile' => array(
    'family' => 'Nobile',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'nokora' => array(
    'family' => 'Nokora',
    'vars' => array('regular','700'),
    ),
    'norican' => array(
    'family' => 'Norican',
    'vars' => array('regular'),
    ),
    'nosifer' => array(
    'family' => 'Nosifer',
    'vars' => array('regular'),
    ),
    'nothingyoucoulddo' => array(
    'family' => 'Nothing You Could Do',
    'vars' => array('regular'),
    ),
    'noticiatext' => array(
    'family' => 'Noticia Text',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'notosans' => array(
    'family' => 'Noto Sans',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'notoserif' => array(
    'family' => 'Noto Serif',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'novacut' => array(
    'family' => 'Nova Cut',
    'vars' => array('regular'),
    ),
    'novaflat' => array(
    'family' => 'Nova Flat',
    'vars' => array('regular'),
    ),
    'novamono' => array(
    'family' => 'Nova Mono',
    'vars' => array('regular'),
    ),
    'novaoval' => array(
    'family' => 'Nova Oval',
    'vars' => array('regular'),
    ),
    'novaround' => array(
    'family' => 'Nova Round',
    'vars' => array('regular'),
    ),
    'novascript' => array(
    'family' => 'Nova Script',
    'vars' => array('regular'),
    ),
    'novaslim' => array(
    'family' => 'Nova Slim',
    'vars' => array('regular'),
    ),
    'novasquare' => array(
    'family' => 'Nova Square',
    'vars' => array('regular'),
    ),
    'numans' => array(
    'family' => 'Numans',
    'vars' => array('regular'),
    ),
    'nunito' => array(
    'family' => 'Nunito',
    'vars' => array('300','regular','700'),
    ),
    'odormeanchey' => array(
    'family' => 'Odor Mean Chey',
    'vars' => array('regular'),
    ),
    'offside' => array(
    'family' => 'Offside',
    'vars' => array('regular'),
    ),
    'oldstandardtt' => array(
    'family' => 'Old Standard TT',
    'vars' => array('regular','italic','700'),
    ),
    'oldenburg' => array(
    'family' => 'Oldenburg',
    'vars' => array('regular'),
    ),
    'oleoscript' => array(
    'family' => 'Oleo Script',
    'vars' => array('regular','700'),
    ),
    'oleoscriptswashcaps' => array(
    'family' => 'Oleo Script Swash Caps',
    'vars' => array('regular','700'),
    ),
    'opensans' => array(
    'family' => 'Open Sans',
    'vars' => array('300','300italic','regular','italic','600','600italic','700','700italic','800','800italic'),
    ),
    'opensanscondensed' => array(
    'family' => 'Open Sans Condensed',
    'vars' => array('300','300italic','700'),
    ),
    'oranienbaum' => array(
    'family' => 'Oranienbaum',
    'vars' => array('regular'),
    ),
    'orbitron' => array(
    'family' => 'Orbitron',
    'vars' => array('regular','500','700','900'),
    ),
    'oregano' => array(
    'family' => 'Oregano',
    'vars' => array('regular','italic'),
    ),
    'orienta' => array(
    'family' => 'Orienta',
    'vars' => array('regular'),
    ),
    'originalsurfer' => array(
    'family' => 'Original Surfer',
    'vars' => array('regular'),
    ),
    'oswald' => array(
    'family' => 'Oswald',
    'vars' => array('300','regular','700'),
    ),
    'overtherainbow' => array(
    'family' => 'Over the Rainbow',
    'vars' => array('regular'),
    ),
    'overlock' => array(
    'family' => 'Overlock',
    'vars' => array('regular','italic','700','700italic','900','900italic'),
    ),
    'overlocksc' => array(
    'family' => 'Overlock SC',
    'vars' => array('regular'),
    ),
    'ovo' => array(
    'family' => 'Ovo',
    'vars' => array('regular'),
    ),
    'oxygen' => array(
    'family' => 'Oxygen',
    'vars' => array('300','regular','700'),
    ),
    'oxygenmono' => array(
    'family' => 'Oxygen Mono',
    'vars' => array('regular'),
    ),
    'ptmono' => array(
    'family' => 'PT Mono',
    'vars' => array('regular'),
    ),
    'ptsans' => array(
    'family' => 'PT Sans',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'ptsanscaption' => array(
    'family' => 'PT Sans Caption',
    'vars' => array('regular','700'),
    ),
    'ptsansnarrow' => array(
    'family' => 'PT Sans Narrow',
    'vars' => array('regular','700'),
    ),
    'ptserif' => array(
    'family' => 'PT Serif',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'ptserifcaption' => array(
    'family' => 'PT Serif Caption',
    'vars' => array('regular','italic'),
    ),
    'pacifico' => array(
    'family' => 'Pacifico',
    'vars' => array('regular'),
    ),
    'paprika' => array(
    'family' => 'Paprika',
    'vars' => array('regular'),
    ),
    'parisienne' => array(
    'family' => 'Parisienne',
    'vars' => array('regular'),
    ),
    'passeroone' => array(
    'family' => 'Passero One',
    'vars' => array('regular'),
    ),
    'passionone' => array(
    'family' => 'Passion One',
    'vars' => array('regular','700','900'),
    ),
    'pathwaygothicone' => array(
    'family' => 'Pathway Gothic One',
    'vars' => array('regular'),
    ),
    'patrickhand' => array(
    'family' => 'Patrick Hand',
    'vars' => array('regular'),
    ),
    'patrickhandsc' => array(
    'family' => 'Patrick Hand SC',
    'vars' => array('regular'),
    ),
    'patuaone' => array(
    'family' => 'Patua One',
    'vars' => array('regular'),
    ),
    'paytoneone' => array(
    'family' => 'Paytone One',
    'vars' => array('regular'),
    ),
    'peralta' => array(
    'family' => 'Peralta',
    'vars' => array('regular'),
    ),
    'permanentmarker' => array(
    'family' => 'Permanent Marker',
    'vars' => array('regular'),
    ),
    'petitformalscript' => array(
    'family' => 'Petit Formal Script',
    'vars' => array('regular'),
    ),
    'petrona' => array(
    'family' => 'Petrona',
    'vars' => array('regular'),
    ),
    'philosopher' => array(
    'family' => 'Philosopher',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'piedra' => array(
    'family' => 'Piedra',
    'vars' => array('regular'),
    ),
    'pinyonscript' => array(
    'family' => 'Pinyon Script',
    'vars' => array('regular'),
    ),
    'pirataone' => array(
    'family' => 'Pirata One',
    'vars' => array('regular'),
    ),
    'plaster' => array(
    'family' => 'Plaster',
    'vars' => array('regular'),
    ),
    'play' => array(
    'family' => 'Play',
    'vars' => array('regular','700'),
    ),
    'playball' => array(
    'family' => 'Playball',
    'vars' => array('regular'),
    ),
    'playfairdisplay' => array(
    'family' => 'Playfair Display',
    'vars' => array('regular','italic','700','700italic','900','900italic'),
    ),
    'playfairdisplaysc' => array(
    'family' => 'Playfair Display SC',
    'vars' => array('regular','italic','700','700italic','900','900italic'),
    ),
    'podkova' => array(
    'family' => 'Podkova',
    'vars' => array('regular','700'),
    ),
    'poiretone' => array(
    'family' => 'Poiret One',
    'vars' => array('regular'),
    ),
    'pollerone' => array(
    'family' => 'Poller One',
    'vars' => array('regular'),
    ),
    'poly' => array(
    'family' => 'Poly',
    'vars' => array('regular','italic'),
    ),
    'pompiere' => array(
    'family' => 'Pompiere',
    'vars' => array('regular'),
    ),
    'pontanosans' => array(
    'family' => 'Pontano Sans',
    'vars' => array('regular'),
    ),
    'portlligatsans' => array(
    'family' => 'Port Lligat Sans',
    'vars' => array('regular'),
    ),
    'portlligatslab' => array(
    'family' => 'Port Lligat Slab',
    'vars' => array('regular'),
    ),
    'prata' => array(
    'family' => 'Prata',
    'vars' => array('regular'),
    ),
    'preahvihear' => array(
    'family' => 'Preahvihear',
    'vars' => array('regular'),
    ),
    'pressstart2p' => array(
    'family' => 'Press Start 2P',
    'vars' => array('regular'),
    ),
    'princesssofia' => array(
    'family' => 'Princess Sofia',
    'vars' => array('regular'),
    ),
    'prociono' => array(
    'family' => 'Prociono',
    'vars' => array('regular'),
    ),
    'prostoone' => array(
    'family' => 'Prosto One',
    'vars' => array('regular'),
    ),
    'puritan' => array(
    'family' => 'Puritan',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'purplepurse' => array(
    'family' => 'Purple Purse',
    'vars' => array('regular'),
    ),
    'quando' => array(
    'family' => 'Quando',
    'vars' => array('regular'),
    ),
    'quantico' => array(
    'family' => 'Quantico',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'quattrocento' => array(
    'family' => 'Quattrocento',
    'vars' => array('regular','700'),
    ),
    'quattrocentosans' => array(
    'family' => 'Quattrocento Sans',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'questrial' => array(
    'family' => 'Questrial',
    'vars' => array('regular'),
    ),
    'quicksand' => array(
    'family' => 'Quicksand',
    'vars' => array('300','regular','700'),
    ),
    'quintessential' => array(
    'family' => 'Quintessential',
    'vars' => array('regular'),
    ),
    'qwigley' => array(
    'family' => 'Qwigley',
    'vars' => array('regular'),
    ),
    'racingsansone' => array(
    'family' => 'Racing Sans One',
    'vars' => array('regular'),
    ),
    'radley' => array(
    'family' => 'Radley',
    'vars' => array('regular','italic'),
    ),
    'rajdhani' => array(
    'family' => 'Rajdhani',
    'vars' => array('300','regular','500','600','700'),
    ),
    'raleway' => array(
    'family' => 'Raleway',
    'vars' => array('100','200','300','regular','500','600','700','800','900'),
    ),
    'ralewaydots' => array(
    'family' => 'Raleway Dots',
    'vars' => array('regular'),
    ),
    'rambla' => array(
    'family' => 'Rambla',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'rammettoone' => array(
    'family' => 'Rammetto One',
    'vars' => array('regular'),
    ),
    'ranchers' => array(
    'family' => 'Ranchers',
    'vars' => array('regular'),
    ),
    'rancho' => array(
    'family' => 'Rancho',
    'vars' => array('regular'),
    ),
    'rationale' => array(
    'family' => 'Rationale',
    'vars' => array('regular'),
    ),
    'redressed' => array(
    'family' => 'Redressed',
    'vars' => array('regular'),
    ),
    'reeniebeanie' => array(
    'family' => 'Reenie Beanie',
    'vars' => array('regular'),
    ),
    'revalia' => array(
    'family' => 'Revalia',
    'vars' => array('regular'),
    ),
    'ribeye' => array(
    'family' => 'Ribeye',
    'vars' => array('regular'),
    ),
    'ribeyemarrow' => array(
    'family' => 'Ribeye Marrow',
    'vars' => array('regular'),
    ),
    'righteous' => array(
    'family' => 'Righteous',
    'vars' => array('regular'),
    ),
    'risque' => array(
    'family' => 'Risque',
    'vars' => array('regular'),
    ),
    'roboto' => array(
    'family' => 'Roboto',
    'vars' => array('100','100italic','300','300italic','regular','italic','500','500italic','700','700italic','900','900italic'),
    ),
    'robotocondensed' => array(
    'family' => 'Roboto Condensed',
    'vars' => array('300','300italic','regular','italic','700','700italic'),
    ),
    'robotoslab' => array(
    'family' => 'Roboto Slab',
    'vars' => array('100','300','regular','700'),
    ),
    'rochester' => array(
    'family' => 'Rochester',
    'vars' => array('regular'),
    ),
    'rocksalt' => array(
    'family' => 'Rock Salt',
    'vars' => array('regular'),
    ),
    'rokkitt' => array(
    'family' => 'Rokkitt',
    'vars' => array('regular','700'),
    ),
    'romanesco' => array(
    'family' => 'Romanesco',
    'vars' => array('regular'),
    ),
    'ropasans' => array(
    'family' => 'Ropa Sans',
    'vars' => array('regular','italic'),
    ),
    'rosario' => array(
    'family' => 'Rosario',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'rosarivo' => array(
    'family' => 'Rosarivo',
    'vars' => array('regular','italic'),
    ),
    'rougescript' => array(
    'family' => 'Rouge Script',
    'vars' => array('regular'),
    ),
    'rozhaone' => array(
    'family' => 'Rozha One',
    'vars' => array('regular'),
    ),
    'rubikmonoone' => array(
    'family' => 'Rubik Mono One',
    'vars' => array('regular'),
    ),
    'rubikone' => array(
    'family' => 'Rubik One',
    'vars' => array('regular'),
    ),
    'ruda' => array(
    'family' => 'Ruda',
    'vars' => array('regular','700','900'),
    ),
    'rufina' => array(
    'family' => 'Rufina',
    'vars' => array('regular','700'),
    ),
    'rugeboogie' => array(
    'family' => 'Ruge Boogie',
    'vars' => array('regular'),
    ),
    'ruluko' => array(
    'family' => 'Ruluko',
    'vars' => array('regular'),
    ),
    'rumraisin' => array(
    'family' => 'Rum Raisin',
    'vars' => array('regular'),
    ),
    'ruslandisplay' => array(
    'family' => 'Ruslan Display',
    'vars' => array('regular'),
    ),
    'russoone' => array(
    'family' => 'Russo One',
    'vars' => array('regular'),
    ),
    'ruthie' => array(
    'family' => 'Ruthie',
    'vars' => array('regular'),
    ),
    'rye' => array(
    'family' => 'Rye',
    'vars' => array('regular'),
    ),
    'sacramento' => array(
    'family' => 'Sacramento',
    'vars' => array('regular'),
    ),
    'sail' => array(
    'family' => 'Sail',
    'vars' => array('regular'),
    ),
    'salsa' => array(
    'family' => 'Salsa',
    'vars' => array('regular'),
    ),
    'sanchez' => array(
    'family' => 'Sanchez',
    'vars' => array('regular','italic'),
    ),
    'sancreek' => array(
    'family' => 'Sancreek',
    'vars' => array('regular'),
    ),
    'sansitaone' => array(
    'family' => 'Sansita One',
    'vars' => array('regular'),
    ),
    'sarina' => array(
    'family' => 'Sarina',
    'vars' => array('regular'),
    ),
    'sarpanch' => array(
    'family' => 'Sarpanch',
    'vars' => array('regular','500','600','700','800','900'),
    ),
    'satisfy' => array(
    'family' => 'Satisfy',
    'vars' => array('regular'),
    ),
    'scada' => array(
    'family' => 'Scada',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'schoolbell' => array(
    'family' => 'Schoolbell',
    'vars' => array('regular'),
    ),
    'seaweedscript' => array(
    'family' => 'Seaweed Script',
    'vars' => array('regular'),
    ),
    'sevillana' => array(
    'family' => 'Sevillana',
    'vars' => array('regular'),
    ),
    'seymourone' => array(
    'family' => 'Seymour One',
    'vars' => array('regular'),
    ),
    'shadowsintolight' => array(
    'family' => 'Shadows Into Light',
    'vars' => array('regular'),
    ),
    'shadowsintolighttwo' => array(
    'family' => 'Shadows Into Light Two',
    'vars' => array('regular'),
    ),
    'shanti' => array(
    'family' => 'Shanti',
    'vars' => array('regular'),
    ),
    'share' => array(
    'family' => 'Share',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'sharetech' => array(
    'family' => 'Share Tech',
    'vars' => array('regular'),
    ),
    'sharetechmono' => array(
    'family' => 'Share Tech Mono',
    'vars' => array('regular'),
    ),
    'shojumaru' => array(
    'family' => 'Shojumaru',
    'vars' => array('regular'),
    ),
    'shortstack' => array(
    'family' => 'Short Stack',
    'vars' => array('regular'),
    ),
    'siemreap' => array(
    'family' => 'Siemreap',
    'vars' => array('regular'),
    ),
    'sigmarone' => array(
    'family' => 'Sigmar One',
    'vars' => array('regular'),
    ),
    'signika' => array(
    'family' => 'Signika',
    'vars' => array('300','regular','600','700'),
    ),
    'signikanegative' => array(
    'family' => 'Signika Negative',
    'vars' => array('300','regular','600','700'),
    ),
    'simonetta' => array(
    'family' => 'Simonetta',
    'vars' => array('regular','italic','900','900italic'),
    ),
    'sintony' => array(
    'family' => 'Sintony',
    'vars' => array('regular','700'),
    ),
    'sirinstencil' => array(
    'family' => 'Sirin Stencil',
    'vars' => array('regular'),
    ),
    'sixcaps' => array(
    'family' => 'Six Caps',
    'vars' => array('regular'),
    ),
    'skranji' => array(
    'family' => 'Skranji',
    'vars' => array('regular','700'),
    ),
    'slabo13px' => array(
    'family' => 'Slabo 13px',
    'vars' => array('regular'),
    ),
    'slabo27px' => array(
    'family' => 'Slabo 27px',
    'vars' => array('regular'),
    ),
    'slackey' => array(
    'family' => 'Slackey',
    'vars' => array('regular'),
    ),
    'smokum' => array(
    'family' => 'Smokum',
    'vars' => array('regular'),
    ),
    'smythe' => array(
    'family' => 'Smythe',
    'vars' => array('regular'),
    ),
    'sniglet' => array(
    'family' => 'Sniglet',
    'vars' => array('regular','800'),
    ),
    'snippet' => array(
    'family' => 'Snippet',
    'vars' => array('regular'),
    ),
    'snowburstone' => array(
    'family' => 'Snowburst One',
    'vars' => array('regular'),
    ),
    'sofadione' => array(
    'family' => 'Sofadi One',
    'vars' => array('regular'),
    ),
    'sofia' => array(
    'family' => 'Sofia',
    'vars' => array('regular'),
    ),
    'sonsieone' => array(
    'family' => 'Sonsie One',
    'vars' => array('regular'),
    ),
    'sortsmillgoudy' => array(
    'family' => 'Sorts Mill Goudy',
    'vars' => array('regular','italic'),
    ),
    'sourcecodepro' => array(
    'family' => 'Source Code Pro',
    'vars' => array('200','300','regular','500','600','700','900'),
    ),
    'sourcesanspro' => array(
    'family' => 'Source Sans Pro',
    'vars' => array('200','200italic','300','300italic','regular','italic','600','600italic','700','700italic','900','900italic'),
    ),
    'sourceserifpro' => array(
    'family' => 'Source Serif Pro',
    'vars' => array('regular','600','700'),
    ),
    'specialelite' => array(
    'family' => 'Special Elite',
    'vars' => array('regular'),
    ),
    'spicyrice' => array(
    'family' => 'Spicy Rice',
    'vars' => array('regular'),
    ),
    'spinnaker' => array(
    'family' => 'Spinnaker',
    'vars' => array('regular'),
    ),
    'spirax' => array(
    'family' => 'Spirax',
    'vars' => array('regular'),
    ),
    'squadaone' => array(
    'family' => 'Squada One',
    'vars' => array('regular'),
    ),
    'stalemate' => array(
    'family' => 'Stalemate',
    'vars' => array('regular'),
    ),
    'stalinistone' => array(
    'family' => 'Stalinist One',
    'vars' => array('regular'),
    ),
    'stardosstencil' => array(
    'family' => 'Stardos Stencil',
    'vars' => array('regular','700'),
    ),
    'stintultracondensed' => array(
    'family' => 'Stint Ultra Condensed',
    'vars' => array('regular'),
    ),
    'stintultraexpanded' => array(
    'family' => 'Stint Ultra Expanded',
    'vars' => array('regular'),
    ),
    'stoke' => array(
    'family' => 'Stoke',
    'vars' => array('300','regular'),
    ),
    'strait' => array(
    'family' => 'Strait',
    'vars' => array('regular'),
    ),
    'sueellenfrancisco' => array(
    'family' => 'Sue Ellen Francisco',
    'vars' => array('regular'),
    ),
    'sunshiney' => array(
    'family' => 'Sunshiney',
    'vars' => array('regular'),
    ),
    'supermercadoone' => array(
    'family' => 'Supermercado One',
    'vars' => array('regular'),
    ),
    'suwannaphum' => array(
    'family' => 'Suwannaphum',
    'vars' => array('regular'),
    ),
    'swankyandmoomoo' => array(
    'family' => 'Swanky and Moo Moo',
    'vars' => array('regular'),
    ),
    'syncopate' => array(
    'family' => 'Syncopate',
    'vars' => array('regular','700'),
    ),
    'tangerine' => array(
    'family' => 'Tangerine',
    'vars' => array('regular','700'),
    ),
    'taprom' => array(
    'family' => 'Taprom',
    'vars' => array('regular'),
    ),
    'tauri' => array(
    'family' => 'Tauri',
    'vars' => array('regular'),
    ),
    'teko' => array(
    'family' => 'Teko',
    'vars' => array('300','regular','500','600','700'),
    ),
    'telex' => array(
    'family' => 'Telex',
    'vars' => array('regular'),
    ),
    'tenorsans' => array(
    'family' => 'Tenor Sans',
    'vars' => array('regular'),
    ),
    'textmeone' => array(
    'family' => 'Text Me One',
    'vars' => array('regular'),
    ),
    'thegirlnextdoor' => array(
    'family' => 'The Girl Next Door',
    'vars' => array('regular'),
    ),
    'tienne' => array(
    'family' => 'Tienne',
    'vars' => array('regular','700','900'),
    ),
    'tinos' => array(
    'family' => 'Tinos',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'titanone' => array(
    'family' => 'Titan One',
    'vars' => array('regular'),
    ),
    'titilliumweb' => array(
    'family' => 'Titillium Web',
    'vars' => array('200','200italic','300','300italic','regular','italic','600','600italic','700','700italic','900'),
    ),
    'tradewinds' => array(
    'family' => 'Trade Winds',
    'vars' => array('regular'),
    ),
    'trocchi' => array(
    'family' => 'Trocchi',
    'vars' => array('regular'),
    ),
    'trochut' => array(
    'family' => 'Trochut',
    'vars' => array('regular','italic','700'),
    ),
    'trykker' => array(
    'family' => 'Trykker',
    'vars' => array('regular'),
    ),
    'tulpenone' => array(
    'family' => 'Tulpen One',
    'vars' => array('regular'),
    ),
    'ubuntu' => array(
    'family' => 'Ubuntu',
    'vars' => array('300','300italic','regular','italic','500','500italic','700','700italic'),
    ),
    'ubuntucondensed' => array(
    'family' => 'Ubuntu Condensed',
    'vars' => array('regular'),
    ),
    'ubuntumono' => array(
    'family' => 'Ubuntu Mono',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'ultra' => array(
    'family' => 'Ultra',
    'vars' => array('regular'),
    ),
    'uncialantiqua' => array(
    'family' => 'Uncial Antiqua',
    'vars' => array('regular'),
    ),
    'underdog' => array(
    'family' => 'Underdog',
    'vars' => array('regular'),
    ),
    'unicaone' => array(
    'family' => 'Unica One',
    'vars' => array('regular'),
    ),
    'unifrakturcook' => array(
    'family' => 'UnifrakturCook',
    'vars' => array('700'),
    ),
    'unifrakturmaguntia' => array(
    'family' => 'UnifrakturMaguntia',
    'vars' => array('regular'),
    ),
    'unkempt' => array(
    'family' => 'Unkempt',
    'vars' => array('regular','700'),
    ),
    'unlock' => array(
    'family' => 'Unlock',
    'vars' => array('regular'),
    ),
    'unna' => array(
    'family' => 'Unna',
    'vars' => array('regular'),
    ),
    'vt323' => array(
    'family' => 'VT323',
    'vars' => array('regular'),
    ),
    'vampiroone' => array(
    'family' => 'Vampiro One',
    'vars' => array('regular'),
    ),
    'varela' => array(
    'family' => 'Varela',
    'vars' => array('regular'),
    ),
    'varelaround' => array(
    'family' => 'Varela Round',
    'vars' => array('regular'),
    ),
    'vastshadow' => array(
    'family' => 'Vast Shadow',
    'vars' => array('regular'),
    ),
    'vesperlibre' => array(
    'family' => 'Vesper Libre',
    'vars' => array('regular','500','700','900'),
    ),
    'vibur' => array(
    'family' => 'Vibur',
    'vars' => array('regular'),
    ),
    'vidaloka' => array(
    'family' => 'Vidaloka',
    'vars' => array('regular'),
    ),
    'viga' => array(
    'family' => 'Viga',
    'vars' => array('regular'),
    ),
    'voces' => array(
    'family' => 'Voces',
    'vars' => array('regular'),
    ),
    'volkhov' => array(
    'family' => 'Volkhov',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'vollkorn' => array(
    'family' => 'Vollkorn',
    'vars' => array('regular','italic','700','700italic'),
    ),
    'voltaire' => array(
    'family' => 'Voltaire',
    'vars' => array('regular'),
    ),
    'waitingforthesunrise' => array(
    'family' => 'Waiting for the Sunrise',
    'vars' => array('regular'),
    ),
    'wallpoet' => array(
    'family' => 'Wallpoet',
    'vars' => array('regular'),
    ),
    'walterturncoat' => array(
    'family' => 'Walter Turncoat',
    'vars' => array('regular'),
    ),
    'warnes' => array(
    'family' => 'Warnes',
    'vars' => array('regular'),
    ),
    'wellfleet' => array(
    'family' => 'Wellfleet',
    'vars' => array('regular'),
    ),
    'wendyone' => array(
    'family' => 'Wendy One',
    'vars' => array('regular'),
    ),
    'wireone' => array(
    'family' => 'Wire One',
    'vars' => array('regular'),
    ),
    'yanonekaffeesatz' => array(
    'family' => 'Yanone Kaffeesatz',
    'vars' => array('200','300','regular','700'),
    ),
    'yellowtail' => array(
    'family' => 'Yellowtail',
    'vars' => array('regular'),
    ),
    'yesevaone' => array(
    'family' => 'Yeseva One',
    'vars' => array('regular'),
    ),
    'yesteryear' => array(
    'family' => 'Yesteryear',
    'vars' => array('regular'),
    ),
    'zeyada' => array(
    'family' => 'Zeyada',
    'vars' => array('regular'),
    ),
);