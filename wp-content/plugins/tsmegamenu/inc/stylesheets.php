<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


/**
 *  Load admin stylesheets 
 *  @since 1.0
 **/
function tsm_admin_styles() {
    
    $screen = get_current_screen();
    
    if ( $screen->base == 'nav-menus' ):
    
        wp_enqueue_style( 'tsm-chosen', TSM_URL . '/css/chosen.css', array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-magnific-popup', TSM_URL . '/css/magnific-popup.css', array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-datepicker', TSM_URL . '/css/datepicker.css', array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-bootstrap-admin-preview', TSM_URL . '/css/bootstrap-admin-preview.css', array(), TSM_VERSION, 'all' );
        //wp_enqueue_style( 'bootstrap', TSM_URL . '/inc/bootstrap/css/bootstrap.css', array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-jquery-ui', TSM_URL . '/css/jquery-ui/jquery-ui.css', array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-jquery-ui-custom-style', TSM_URL . '/css/tsm-jquery-ui-custom-style.css', array(), TSM_VERSION, 'all' );
        
        // Live css editor
        //wp_enqueue_style( 'livecsseditor', TSM_URL . '/live-css-editor/css/livecsseditor.css' , array(), TSM_VERSION, 'all' );
        //wp_enqueue_style( 'bootstrap', TSM_URL . '/live-css-editor/plugins/bootstrap/css/bootstrap.css' , array(), TSM_VERSION, 'all' );
        //wp_enqueue_style( 'colorpicker-bootstrap', TSM_URL . '/live-css-editor/plugins/colorpicker/css/colorpicker-bootstrap.css' , array(), TSM_VERSION, 'all' );
        
        // SB Admin 2
        // http://startbootstrap.com/template-overviews/sb-admin-2/
        //wp_enqueue_style( 'sb-admin-2', TSM_URL . '/css/sb-admin-2.css' , array(), TSM_VERSION, 'all' );
        
        wp_enqueue_style( 'tsm-backend-style', TSM_URL . '/css/tsm-backend-style.css' , array(), TSM_VERSION, 'all' );
    
    endif;
    
    // Style for TSM options
    if ( $screen->base == 'nav-menus' || $screen->base == 'toplevel_page_tsm-options' ):
        
        wp_enqueue_style( 'tsm-font-awesome', TSM_URL . '/font-awesome/css/font-awesome.css', array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-spectrum', TSM_URL . '/css/spectrum.css' , array(), TSM_VERSION, 'all' );
        wp_enqueue_style( 'tsm-colorSwitcher', TSM_URL . '/css/colorSwitcher.css' , array(), TSM_VERSION, 'all' );
        
    endif;
    
}
add_action( 'admin_enqueue_scripts', 'tsm_admin_styles' );

/**
 *  Load frontend stylesheets
 *  @since 1.0
 **/
function tsm_frontend_styles() {
    
    wp_register_style( 'tsm-frontend-style', TSM_URL . '/css/tsm-frontend-style.css' , false, TSM_VERSION, 'all' );
    wp_enqueue_style( 'tsm-frontend-style' );
    
    //wp_register_style( 'ts-megamenu-impressive-blue', TSM_URL . '/css/themes/ts-megamenu-impressive/blue.css', false, TSM_VERSION, 'all' );
	//wp_enqueue_style( 'ts-megamenu-impressive-blue' );
    
    wp_register_style( 'tsm-font-awesome', TSM_URL . '/font-awesome/css/font-awesome.css', false, TSM_VERSION, 'all' );
	wp_enqueue_style( 'tsm-font-awesome' );
    
    $locations = get_nav_menu_locations();
    
    if ( !empty( $locations ) ):
        
        $fonts_need_load = '';
        
        foreach ( $locations as $location => $menu_id ):
            
            $mega_menu_settings = get_post_meta( $menu_id, 'tsm_mega_menu_settings', true );
            
            if ( isset( $mega_menu_settings['fonts'] ) ):
                
                if ( trim( $mega_menu_settings['fonts'] ) != '' ):
                    
                    $fonts_need_load .= ( $fonts_need_load == '' ) ? $mega_menu_settings['fonts'] : ',' . $mega_menu_settings['fonts'];
                    
                endif;
                
            endif;
            
        endforeach;
        
        $fonts_need_load = implode( ',', array_unique( explode( ',', $fonts_need_load ) ) );
        
        // Add Google Fonts
	   wp_enqueue_style( 'tsm-google-fonts', tsm_google_font_url( $fonts_need_load ), array(), null );
    
    endif;
    
    wp_enqueue_style( 'tsm-megamenu', admin_url( 'admin-ajax.php' ) . '?action=tsm_enqueue_style_via_ajax', false, TSM_VERSION );
    
    
}
add_action( 'wp_enqueue_scripts', 'tsm_frontend_styles' );


/**
 *  Enqueue mega menu style via ajax
 *  @since 1.0
 **/
function tsm_enqueue_style_via_ajax() {
    
    $locations = get_nav_menu_locations();
    
    header( 'Content-type: text/css; charset: UTF-8' );
    
    if ( !empty( $locations ) ):
        
        $menu_ids_has_been_load_css = array();
        
        foreach ( $locations as $location => $menu_id ):
        
            $mega_menu_settings = get_post_meta( $menu_id, 'tsm_mega_menu_settings', true );
            $is_enable_mega_menu = ( isset( $mega_menu_settings['enable_mega_menu'] ) ) ? $mega_menu_settings['enable_mega_menu'] == 'yes' : false;
            
            if ( is_array( $mega_menu_settings ) && !empty( $mega_menu_settings ) && $is_enable_mega_menu ):
                
                /*
                if ( isset( $mega_menu_settings['menu_css_frontend'] ) ): // Old version
                    
                    $zindex = ( isset( $mega_menu_settings['zindex'] ) ) ? max( 0, intval( $mega_menu_settings['zindex'] ) ) : 'inherit';
                    
                    echo    ".tsm-menu-wrap-{$menu_id} .ts-megamenu {
                                z-index: {$zindex};
                            }\n";
                    echo urldecode( $mega_menu_settings['menu_css_frontend'] );
                    
                endif;
                */
                if ( isset( $mega_menu_settings['menu_css_theme_frontend'] ) ): // Modern version
                    
                    if ( !in_array( $menu_id, $menu_ids_has_been_load_css ) ):
                    
                        $zindex = ( isset( $mega_menu_settings['zindex'] ) ) ? max( 0, intval( $mega_menu_settings['zindex'] ) ) : 'inherit';
                        
                        echo    ".tsm-menu-wrap-{$menu_id} .ts-megamenu {
                                    z-index: {$zindex};
                                }\n";
                        echo urldecode( $mega_menu_settings['menu_css_theme_frontend'] );
                        
                        if ( isset( $mega_menu_settings['additional_css'] ) ):
                        
                            echo $mega_menu_settings['additional_css'];
                            
                        endif;
                        
                        //$breakpoint = isset( $mega_menu_settings['breakpoint'] ) ? max( 0, intval( $mega_menu_settings['breakpoint'] ) ) : 768;
//                        
//                        echo    "@media screen and (max-width: " . ( $breakpoint - 1 ) . "px){
//                                    .tsm-menu-wrap-{$menu_id} .ts-megamenu {
//                                        position: absolute;
//                                        right: 15px;
//                                        width: 100%;
//                                    }\n
//                                }";
                        
                        $menu_ids_has_been_load_css[] = $menu_id;
                        
                    endif;
                    
                endif;
                
            endif;
        
        endforeach;
        
    endif;
    
    wp_die();
}
add_action( 'wp_ajax_tsm_enqueue_style_via_ajax', 'tsm_enqueue_style_via_ajax' );
add_action( 'wp_ajax_nopriv_tsm_enqueue_style_via_ajax', 'tsm_enqueue_style_via_ajax' );



