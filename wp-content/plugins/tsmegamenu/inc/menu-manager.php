<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'admin_init', 'tsm_register_nav_meta_box', 11 );


add_filter( 'wp_edit_nav_menu_walker', 'tsm_walker', 2001 );

/** Filter for mega menu **/
add_filter( 'wp_nav_menu_item_custom_fields', 'tsm_walker_add_fields', 10, 4 );

/**
 * Use our custom Nav Edit walker. This adds a filter which we can use to add
 * settings to menu items.
 *
 * @since 1.0
 * @param object $walker
 * @return string
 */
function tsm_walker( $walker ) {

    require_once TSM_DIR . '/classes/walkerBackendEditClass.php';

    return 'TSM_Menu_Walker_Edit';

}


/**
 * Show custom menu item fields.
 *
 * @since 1.0
 * @param object $item
 * @param int $depth
 * @param array $args
 * @param int $id
 */
function tsm_walker_add_fields( $id, $item, $depth, $args ) {
    global $tsm_script_localize;
    
    $menu_item_settings_default = array(
        'menu_item_id'      =>  0,
        'show_caret'        =>  'yes',
        'caret'             =>  'fa fa-angle-down',
        'drop_direction'    =>  'tsm-drop-right',
        'icon_class'        =>  '',
        'font_family'       =>  '',
        'mega_type'         =>  'grid'
    ); 
    
    $menu_item_settings = get_post_meta( $item->ID, 'tsm_menu_item_settings', true );
    
    if ( !is_array( $menu_item_settings ) ):
        
        $menu_item_settings = ( array ) $menu_item_settings;
        
    endif;
    
    $menu_item_settings = array_merge( $menu_item_settings_default, $menu_item_settings );
    
    $menu_item_icon_class = trim( $menu_item_settings['icon_class'] );
    $menu_item_meta_type = esc_attr( $menu_item_settings['mega_type'] );
    
    $menu_item_data = array_filter( (array) get_post_meta( $item->ID, 'tsm_menu_item_grid_data', true ) ); // Grid data
    $menu_item_grid_options = array_filter( (array) get_post_meta( $item->ID, 'tsm_menu_item_grid_options', true ) );    
    $menu_item_dropdown_data = array_filter( (array) get_post_meta( $item->ID, 'tsm_menu_item_dropdown_data', true ) );
    
    $menu_item_grid_options_default = array(
        'width'         =>  'auto',
        'fullwidth'     =>  'yes',
        'min_width'     =>  0,
        'max_width'     =>  980
    );
    
    $menu_item_grid_options = array_merge( $menu_item_grid_options_default, $menu_item_grid_options );
    
    $menu_item_id = esc_attr( $item->ID ); 
    $tsm_form_wrap_id = 'tsm-megamenu-form-wrap-' . $menu_item_id;
    $tsm_form_id = 'tsm-megamenu-form-' . $menu_item_id;
    $tsm_form_dropdown_id = 'tsm-megamenu-form-dropdown-' . $menu_item_id;
    ?>

    <div data-menu-item-id="<?php echo $menu_item_id; ?>" class="tsm-wrap tsm-menu-item-wrap description-wide">
        <h4><?php _e("Mega menu options", "megamenu"); ?></h4>
        <p class="description tsm-show-on-level-0-only">
            <a data-menu-item-id="<?php echo $menu_item_id; ?>" href="#<?php echo $tsm_form_wrap_id; ?>" class="button tsm-show-mega-form">
                <i class="fa fa-cog"></i>
                <?php _e( 'Configure', 'tsm' ); ?>
            </a>
            <a data-menu-item-id="<?php echo $menu_item_id; ?>" class="button-primary tsm-btn tsm-save-mega-menu tsm-float-right">
                <i class="fa fa-floppy-o"></i>
                <?php _e( 'Save Mega Data', 'tsm' ); ?>
            </a>
        </p>
        <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
        <div class="tsm-menu-item-options-wrap">
            <div class="row">
                <div class="col-xs-12 tsm-box">
                    <input <?php checked( $menu_item_settings['show_caret'] == 'yes' ); ?> data-menu-item-id="<?php echo $menu_item_id; ?>" type="checkbox" id="tsm-show-caret-<?php echo $item->ID; ?>" class="tsm-show-caret" />
                    <label for="tsm-show-caret-<?php echo $menu_item_id; ?>" title="<?php _e( 'Show caret if have children', 'tsm' ); ?>">
                        <?php _e( 'Show caret', 'tsm' ); ?>
                    </label>
                </div>
                
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                
                <div class="col-xs-6 tsm-box">
                    <label for="tsm-caret-<?php echo $menu_item_id; ?>" title="<?php _e( 'Choose Caret', 'tsm' ); ?>">
                        <?php _e( 'Caret', 'tsm' ); ?>
                    </label>
                    <select id="tsm-caret-<?php echo $menu_item_id; ?>" class="tsm-select tsm-caret">
                        <option <?php selected( trim( $menu_item_settings['caret'] ) == 'fa fa-angle-up' ); ?> value="fa fa-angle-up"><?php _e( 'Up', 'tsm' ); ?></option>
                        <option <?php selected( trim( $menu_item_settings['caret'] ) == 'fa fa-angle-down' ); ?> value="fa fa-angle-down"><?php _e( 'Down', 'tsm' ); ?></option>
                        <option <?php selected( trim( $menu_item_settings['caret'] ) == 'fa fa-angle-left' ); ?> value="fa fa-angle-left"><?php _e( 'Left', 'tsm' ); ?></option>
                        <option <?php selected( trim( $menu_item_settings['caret'] ) == 'fa fa-angle-right' ); ?> value="fa fa-angle-right"><?php _e( 'Right', 'tsm' ); ?></option>
                    </select>
                </div>
                
                <div class="col-xs-6 tsm-box tsm-align-right">
                    <label for="tsm-drop-direction-<?php echo $menu_item_id; ?>" title="<?php _e( 'Drop direction', 'tsm' ); ?>">
                        <?php _e( 'Drop', 'tsm' ); ?>
                    </label>
                    <select id="tsm-drop-direction-<?php echo $menu_item_id; ?>" class="tsm-select tsm-drop-direction">
                        <option <?php selected( trim( $menu_item_settings['drop_direction'] ) == 'tsm-drop-right' ); ?> value="tsm-drop-right"><?php _e( 'Drop Right', 'tsm' ); ?></option>
                        <option <?php selected( trim( $menu_item_settings['drop_direction'] ) == 'tsm-drop-left' ); ?> value="tsm-drop-left"><?php _e( 'Drop Left', 'tsm' ); ?></option>
                    </select>
                </div>
                
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                <div class="col-xs-12 tsm-box">
                    <label for="tsm-menu-item-icon-<?php echo $menu_item_id; ?>" class="tsm-choose-icon-label">
                        <span class="tsm-menu-item-large-title"><?php _e( 'Icon', 'tsm' ); ?></span>
                        <span data-icon-class="<?php echo $menu_item_icon_class; ?>" class="tsm-icon-chosen">
                            <?php if ( $menu_item_icon_class != '' ): ?>
                                <i class="<?php echo $menu_item_icon_class; ?>"></i>
                            <?php else: ?>
                                <i><?php echo $tsm_script_localize['text']['no_icon']; ?></i>
                            <?php endif; ?>
                        </span>
                    </label>
                    <div id="tsm-menu-item-icon-<?php echo $menu_item_id; ?>" class="tsm-menu-item-icon" style="display: none;">
                        
                    </div>
                </div>
                
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                
            </div><!-- .row -->
        </div><!-- .tsm-menu-item-options-wrap -->
        
        <div class="tsm-hidden">
            <div id="<?php echo $tsm_form_wrap_id; ?>" class="tsm-megamenu-form-wrap">
                
                <div data-menu-item-id="<?php echo $menu_item_id; ?>" id="<?php echo $tsm_form_id; ?>" class="tsm-megamenu-form col-xs-12" style="display: <?php echo ( $menu_item_meta_type == 'grid' ) ? 'block': 'none' ?>;">
                    <h3 class="tsm-form-title"><?php _e( 'Configure Mega Menu Grid', 'tsm' ); ?></h3>
                    <?php tsm_suported_elems_tabs(); ?>
                    
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                    
                    <div class="tsm-grid">
                        <?php echo tsm_menu_item_grid( $menu_item_data ); ?>
                        
                        <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                        <div class="col-xs-12 tsm-box">
                            <label for="tsm-grid-width-<?php echo $menu_item_id; ?>" title="<?php _e( 'Width', 'tsm' ); ?>">
                                <?php _e( 'Width: ', 'tsm' ); ?>
                                <input value="<?php echo $menu_item_grid_options['width']; ?>" type="text" id="tsm-grid-width-amount-<?php echo $menu_item_id; ?>" class="tsm-grid-width" placeholder="<?php _e( 'auto, percent, pixel...', 'tsm' ) ?>" />
                            </label>
                            <input data-menu-item-id="<?php echo $menu_item_id; ?>" <?php checked( $menu_item_grid_options['fullwidth'] == 'yes' ); ?> type="checkbox" id="tsm-submenu-full-with-<?php echo $item->ID; ?>" class="tsm-submenu-full-with" />
                            <label for="tsm-submenu-full-with-<?php echo $menu_item_id; ?>" title="<?php _e( 'Fullwidth', 'tsm' ); ?>">
                                <?php _e( 'Fullwidth', 'tsm' ); ?>
                            </label>
                            <br />
                            <div class="description tsm-desc">
                                <?php _e( 'Apply for grid type only', 'tsm' ) ?>
                            </div>
                        </div>
                        
                        <?php 
                            $grid_min_max_width_style = ( $menu_item_grid_options['fullwidth'] == 'yes' ) ? 'style="display: none;"' : '';
                        ?>
                        
                        <div class="col-xs-12 tsm-box tsm-min-max-width-wrap-<?php echo $menu_item_id; ?>" <?php echo $grid_min_max_width_style; ?>>
                            <label for="tsm-min-max-width-<?php echo $menu_item_id; ?>" title="<?php _e( 'Min width/Max width', 'tsm' ); ?>">
                                <?php _e( 'Min width/Max width: ', 'tsm' ); ?>
                                <input type="text" id="tsm-min-max-width-amount-<?php echo $menu_item_id; ?>" class="tsm-amount" readonly="readonly" style="border:0; color:#f6931f; font-weight:bold;" />
                            </label><br />
                            <div data-amount-id="tsm-min-max-width-amount-<?php echo $menu_item_id; ?>" data-min="0" data-max="1920" data-val-min="<?php echo intval( $menu_item_grid_options['min_width'] ); ?>" data-val-max="<?php echo intval( $menu_item_grid_options['max_width'] ); ?>" id="tsm-min-max-width-<?php echo $menu_item_id; ?>" class="tsm-min-max-width tsm-slider-range"></div>
                        </div>
                    </div><!-- .tsm-grid -->
                    
                    
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                    
                    <div class="tsm-edit-elem-form tsm-box">
                    </div><!-- .tsm-edit-elem-form -->
                    
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                    
                    <div class="tsm-actions-wrap">
                        <a href="#" class="tsm-add-row tsm-action tsm-btn button"><i class="fa fa-plus"></i><?php _e( 'Add Row', 'tsm' ); ?></a>
                    </div><!-- .tsm-actions-wrap -->
                </div><!-- .tsm-megamenu-form -->
                
                <div data-menu-item-id="<?php echo $menu_item_id; ?>" id="<?php echo $tsm_form_dropdown_id; ?>" class="tsm-megamenu-form-dropdown col-xs-12" style="display: <?php echo ( $menu_item_meta_type == 'dropdown' ) ? 'block': 'none' ?>;">
                    <h3 class="tsm-form-title"><?php _e( 'Configure Mega Menu Dropdown List', 'tsm' ); ?></h3>
                    
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                    
                    <div class="tsm-dropdown-grid ts-megamenu-backend-list">
                        <?php
                            
                            if ( !empty( $menu_item_dropdown_data ) ):
                            
                                foreach ( $menu_item_dropdown_data as $menu_item_dropdown ):
                                    tsm_list_form( $menu_item_dropdown );
                                endforeach;
                            
                            endif;
                            
                        ?>
                    </div><!-- .tsm-dropdown-grid -->
                    
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                    
                    <div class="tsm-actions-wrap">
                        <a href="#" class="tsm-add-dropdown-list-btn tsm-action tsm-btn button"><i class="fa fa-plus"></i><?php _e( 'Add List', 'tsm' ); ?></a>
                    </div><!-- .tsm-actions-wrap -->
                    
                    <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                    
                    <div class="tsm-desc">
                        <p><?php _e( 'Minimum width of each dropdown list is 50px. Maximum total dropdown list width is 1200px.', 'tsm' ); ?></p>
                    </div>
                    
                </div><!-- .tsm-megamenu-form-dropdown -->
                
                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                
                <div class="col-xs-12">
                    <a data-mega-menu-type="<?php echo $menu_item_meta_type; ?>" href="#" class="tsm-btn tsm-mega-menu-type-btn button">
                        <i class="fa fa-exchange"></i>
                        <?php if ( $menu_item_meta_type == 'grid' ): ?>
                            <?php echo $tsm_script_localize['text']['use_dropdown']; ?>
                        <?php else: ?>
                            <?php echo $tsm_script_localize['text']['use_grid']; ?>
                        <?php endif; ?>
                    </a>
                    <a href="#" class="tsm-btn tsm-close-megamenu-form-btn tsm-float-right button">
                        <?php _e( 'Close', 'tsm' ); ?>
                    </a>
                    <a class="button-primary tsm-btn tsm-close-and-save-mega-menu tsm-float-right"><i class="fa fa-floppy-o"></i><?php _e( 'Close And Save', 'tsm' ); ?></a>
                </div>
                
            </div><!-- .tsm-megamenu-form-wrap -->
        </div>
        
    </div><!-- .tsm-wrap .tsm-menu-item-wrap -->

    <?php
}

function tsm_suported_elems_tabs( $echo = true ) {
    global $tsm_elems_map, $wp_widget_factory;
    
    do_action( 'tsm_before_suported_elems_tabs' );
    
    $html = '';
    $html_tabs_header = '';
    $html_tabs_contents = '';
    $elems_tab_html = '';
    $widgets_tab_html = '';
    
    if ( is_array( $tsm_elems_map ) && !empty( $tsm_elems_map ) ):
        
        $elems_tab_id = uniqid( 'tsm-elems-tab-' );
        $widget_tab_id = uniqid( 'tsm-widgets-tab-' );
        
        /** Get elems tab **/
        
        // Default args for elem merge
        $default_elem_arg = array(
            'name'              =>  '',
            'desc'              =>  '',
            'fields'            =>  array(),
            'short_code'        =>  'tsm_default_short_code'  // Default short code to display this element
        );
        
        $elem_control_html = tsm_get_elem_control();
        
        $elems_tab_html .= '<div id="' . $elems_tab_id . '">'; // Open tab div
        $elems_tab_html .= '<div class="tsm-tab-content">'; // Open tab content div
        foreach ( $tsm_elems_map as $tsm_elem_key => $tsm_elem ):
            
            if ( $tsm_elem_key != '' ):
            
                $tsm_elem_html = '';
                $tsm_elem = array_merge( $default_elem_arg, $tsm_elem );
                $tsm_elem['name'] = ( trim( $tsm_elem['name'] ) == '' ) ? __( 'Unknown name', 'tsm' ) : trim( $tsm_elem['name'] );
                
                if ( !isset( $tsm_elem['required'] ) ):
                    $tsm_elem['required'] = 'no';
                endif;
                if ( !isset( $tsm_elem['validate_empty'] ) ):
                    $tsm_elem['validate_empty'] = 'no';
                endif;
                $tsm_elem_json = json_encode( $tsm_elem );
                
                $elem_index = 'data-elem-index=\'0\''; 
                
                $tsm_elem_html =    '<div data-elem-key=\'' . $tsm_elem_key . '\' ' . $elem_index . ' data-elem-json=\'' . $tsm_elem_json . '\' class=\'tsm-elem\' style=\'background: center center no-repeat transparent;\'>
                                        <div class=\'tsm-elem-inner\'>
                                            <label class="tsm-elem-title">' . $tsm_elem['name'] . '</label>
                                            ' . $elem_control_html . '
                                            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                            <div class="tsm-desc">' . $tsm_elem['desc'] . '</div>
                                            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                            <p class=\'tsm-preview-title\'></p>                                        
                                        </div>
                                    </div>';
                
                // Add elem to tab
                $elems_tab_html .= $tsm_elem_html;
                
            endif;
            
        endforeach;
        
        $elems_tab_html .= '</div>'; // Close tab content div
        $elems_tab_html .= '</div>'; // Close tab div
        
        
        /** Get widgets tab **/
        $total_widgets = count( $wp_widget_factory->widgets );
        if ( $total_widgets > 0 ):
            
            $widgets_tab_html .= '<div id="' . $widget_tab_id . '">'; // Open tab div
            $widgets_tab_html .= '<div class="tsm-tab-content">'; // Open tab content div
            
            $i = 0;
            $wg_per_page = 6;
            $total_wg_pages = ceil( $total_widgets / $wg_per_page );
            $cur_wg_page = 1;
            $wg_page_num = 1; 
            
            $page_nav_html = '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
            $page_nav_html .= '<div class="tsm-pagination-wrap">';
            $page_nav_html .= '<ul class="tsm-pagination pagination">';
            
            $li_html = '';
            for ( $i = 1; $i <= $total_wg_pages; $i++ ):
                
                $li_class = ( $i == $cur_wg_page ) ? 'tsm-cur-page active': '';
                $li_html .= '<li class="' . $li_class . '"><a data-page="' . $i . '" href="#">' . $i . '</a></li>';
                
            endfor;
            
            $page_nav_html .= $li_html;
            $page_nav_html .= '</ul><!-- .tsm-pagination -->';
            $page_nav_html .= '</div><!-- .tsm-pagination-wrap -->';
            
            $i = 0;
            $content_wg_tabs_html = '<div class=\'tsm-row tsm-row-' . $wg_page_num . ' tsm-cur-page-view row\'>';
            foreach( $wp_widget_factory->widgets as $class => $info ):
                
                $i++;
                $tsm_widget_html = '';
                $widget = new $class();
                $widget_name = sanitize_text_field( $widget->name );
                $widget_options = $widget->widget_options;
                $widget_desc = isset( $widget_options['description'] ) ? sanitize_text_field( $widget_options['description'] ) : '';
                $widget_json = json_encode($widget); 
                
                $tsm_widget_html =  '<div class="col-xs-12 col-sm-6 col-md-4">
                                        <div data-widget-key=\'' . $class . '\' data-wg-instance=\'\' data-widget=\'' . $widget_json . '\' class=\'tsm-elem tsm-widget\'>
                                            <label class="tsm-elem-title">' . $widget_name . '</label>
                                            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                            <div class="tsm-desc">' . $widget_desc . '</div>
                                            ' . $elem_control_html . '
                                        </div>
                                    </div>';
                
                $content_wg_tabs_html .= $tsm_widget_html;
                
                if ( $i % 3 == 0 ):
                    $content_wg_tabs_html .= '<div class="clearfix visible-md-block"></div>';
                endif;
                
                if ( $i % 2 == 0 ):
                    $content_wg_tabs_html .= '<div class="clearfix visible-sm-block"></div>';
                endif;
                
                if ( $i % $wg_per_page == 0 && $i < $total_widgets ):
                
                    $wg_page_num++;
                    $content_wg_tabs_html .= '</div><!-- .tsm-row -->'; 
                    $content_wg_tabs_html .= '<div class=\'tsm-row tsm-row-' . $wg_page_num . ' row tsm-hidden\'>';
                    
                endif;
                
            endforeach;
            
            $content_wg_tabs_html .= '</div><!-- .tsm-row -->';
            
            $widgets_tab_html .= $content_wg_tabs_html . $page_nav_html;
            $widgets_tab_html .= '</div>'; // Close tab content div
            $widgets_tab_html .= '</div>'; // Close tab div
            
        endif;
    
    endif;
    
    if ( $elems_tab_html != '' || $widgets_tab_html != '' ):
        
        $html_tabs_header .=    '<ul>';
        
        if ( $elems_tab_html != '' ):
            
            $html_tabs_header .= '<li><a href="#' . $elems_tab_id . '">' . __( 'Elements', 'tsm' ) . '</a></li>';
            $html_tabs_contents .= $elems_tab_html;
            
        endif;
        
        if ( $widgets_tab_html != '' ):
            
            $html_tabs_header .= '<li><a href="#' . $widget_tab_id . '">' . __( 'Widgets', 'tsm' ) . '</a></li>';
            $html_tabs_contents .= $widgets_tab_html;
            
        endif;
        
        $html_tabs_header .=    '</ul>';
        
        $html .=    '<div class="tsm-suported-elems-tabs tsm-tabs" style="display: none;">';
        $html .=    $html_tabs_header . $html_tabs_contents;
        $html .=    '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
        $html .=    '<a class="button tsm-cancel-add-elem tsm-btn"><i class="fa fa-times"></i>' . __( 'Cancel', 'tsm' ) . '</a>';
        $html .=    '</div><!-- .tsm-suported-elems-tabs -->';
        
    endif;
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}


/**
 * Adds the meta box container
 *
 * @since 1.0
 */
function tsm_register_nav_meta_box() {
    global $pagenow;

    if ( 'nav-menus.php' == $pagenow ):

        add_meta_box(
            'tsm_mega_menu_meta_box',
            __( 'Mega Menu Settings', 'tsm' ),
            'tsm_nav_metabox_contents',
            'nav-menus',
            'side',
            'high'
        );
        
    endif;
}

/**
 * Show the Meta Menu settings
 *
 * @since 1.0
 */
function tsm_nav_metabox_contents() {
    global $tsm_elems_map, $tsm_css_list_default, $default_color_schemes, $tsm_theme_list;
    
    $tsm_css_list_saved = get_option( 'tsm_color_temps', array() );
    $tsm_css_list = array_merge( $tsm_css_list_default, $tsm_css_list_saved );
        
    $menu_id = tsm_get_selected_menu_id();
    $menu_name = '';
    $menu_info = get_term_by( 'id', $menu_id, 'nav_menu' );
    
    if ( is_object( $menu_info ) ):
        
        $menu_name = $menu_info->name;
        
    endif;
    
    $mega_menu_data = get_post_meta( $menu_id, 'tsm_mega_menu_data', true );
    
    $mega_menu_settings_default = array(
        'enable_mega_menu'  =>  'no',
        'menu_css_data'     =>  array(),    // Old version
        'menu_theme_params' =>  array(),    // Modern version
        'menu_css_frontend' =>  '',
        'breakpoint'        =>  767,
        'zindex'            =>  101,
        'position'          =>  'top',
        'showon'            =>  'hover',
        'responsive'        =>  'switch',
        'fullwidth'         =>  'yes',
        'sticky'            =>  'no',
        'fixed'             =>  'no',
        'effect'            =>  'fade',     // Old version
        'easing'            =>  'linear',   // Old version
        'speed'             =>  500,        // Old version
        'delay'             =>  0,          // Old version
        'show_effect'       =>  'slideUpSmall',   // Modern version
        'show_speed'        =>  500,        // Modern version
        'show_delay'        =>  0,          // Modern version
        'hide_effect'       =>  'zoomOut',  // Modern version
        'hide_speed'        =>  500,        // Modern version
        'hide_delay'        =>  0           // Modern version
    );
    $mega_menu_settings = $mega_menu_settings_default;
    
    if ( isset( $mega_menu_data['settings'] ) ):
        
        $mega_menu_settings = array_merge( $mega_menu_settings_default, $mega_menu_data['settings'] );
        
    endif;
    
    /*
    if ( isset( $mega_menu_settings['menu_css_data']['tmp_colors'] ) && isset( $mega_menu_settings['menu_css_data']['css_base'] ) ):
        
        $this_menu_color_scheme_code = isset( $mega_menu_settings['menu_css_data']['new_color_scheme'] ) ? trim( $mega_menu_settings['menu_css_data']['new_color_scheme'] ): '';
        
        $this_menu_css_temp = array(
            'this_menu_css' =>  array(
                'css_key'               =>  'this_menu_css',
                'css_name'              =>  __( 'Current ', 'tsm' ). $menu_name . __( ' CSS', 'tsm' ),
                'tmp_colors'            =>  $mega_menu_settings['menu_css_data']['tmp_colors'],
                'color_schemes_id'      =>  'custom_color_scheme_this_menu_css',
                'color_schemes_class'   =>  'custom_color_scheme_this_menu_css',
                'color_schemes_code'    =>  $this_menu_color_scheme_code,
                'css_base'              =>  urldecode( $mega_menu_settings['menu_css_data']['css_base'] )
            )
        );
        
        $tsm_css_list_default = array_merge( $this_menu_css_temp, $tsm_css_list_default );
        
    endif;
    */
    
    $breakpoint = isset( $mega_menu_settings['breakpoint'] ) ? max( 0, intval( $mega_menu_settings['breakpoint'] ) ) : 768;
    $zindex = ( isset( $mega_menu_settings['zindex'] ) ) ? max( 0, intval( $mega_menu_settings['zindex'] ) ) : 100;
    
    // Setting select
    $settings_select_html = '';
    
    $pos_args = array(
        array( 'left', __( 'Left', 'tsm' ) ),
        array( 'right', __( 'Right', 'tsm' ) ),
        array( 'top', __( 'Top', 'tsm' ) ),
        array( 'bottom', __( 'Bottom', 'tsm' ) )
    );
    
    $pos_select_html = tsm_select_list(
        $pos_args,
        array(
            'label'     =>  __( 'Position', 'tsm' ),
            'id'        =>  'tsm-position-select',
            'class'     =>  'tsm-select tsm-select2 tsm-position-select',
            'selected'  =>  $mega_menu_settings['position'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $showon_args = array(
        array( 'hover', __( 'Hover', 'tsm' ) ),
        array( 'click', __( 'Click', 'tsm' ) ),
        array( 'toggle', __( 'Toggle', 'tsm' ) )
    );
    
    $showon_select_html = tsm_select_list(
        $showon_args,
        array(
            'label'     =>  __( 'Show On', 'tsm' ),
            'id'        =>  'tsm-show-on-select',
            'class'     =>  'tsm-select tsm-select2 tsm-show-on-select',
            'selected'  =>  $mega_menu_settings['showon'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $responsive_args = array(
        array( 'simple', __( 'Simple', 'tsm' ) ),
        array( 'switch', __( 'Switch', 'tsm' ) ),
        array( 'stack', __( 'Stack', 'tsm' ) ),
        array( 'switch-margin', __( 'Switch-Margin', 'tsm' ) ),
        array( 'stack-margin', __( 'Stack-Margin', 'tsm' ) ),
        array( 'no-responsive', __( 'Don\'t Responsive', 'tsm' ) ),
    );
    
    $responsive_select_html = tsm_select_list(
        $responsive_args,
        array(
            'label'     =>  __( 'Responsive', 'tsm' ),
            'id'        =>  'tsm-responsive-select',
            'class'     =>  'tsm-select tsm-select2 tsm-responsive-select',
            'selected'  =>  $mega_menu_settings['responsive'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $fullwidth_args = array(
        array( 'yes', __( 'Yes', 'tsm' ) ),
        array( 'no', __( 'No', 'tsm' ) )
    );
    
    $fullwidth_select_html = tsm_select_list(
        $fullwidth_args,
        array(
            'label'     =>  __( 'Fullwidth', 'tsm' ),
            'id'        =>  'tsm-fullwidth-select',
            'class'     =>  'tsm-select tsm-select2 tsm-fullwidth-select',
            'selected'  =>  $mega_menu_settings['fullwidth'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $sticky_args = array(
        array( 'yes', __( 'Yes', 'tsm' ) ),
        array( 'no', __( 'No', 'tsm' ) )
    );
    
    $sticky_select_html = tsm_select_list(
        $sticky_args,
        array(
            'label'     =>  __( 'Sticky', 'tsm' ),
            'id'        =>  'tsm-sticky-select',
            'class'     =>  'tsm-select tsm-select2 tsm-sticky-select',
            'selected'  =>  $mega_menu_settings['sticky'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $fixed_args = array(
        array( 'yes', __( 'Yes', 'tsm' ) ),
        array( 'no', __( 'No', 'tsm' ) )
    );
    
    $fixed_select_html = tsm_select_list(
        $fixed_args,
        array(
            'label'     =>  __( 'Fixed', 'tsm' ),
            'id'        =>  'tsm-fixed-select',
            'class'     =>  'tsm-select tsm-select2 tsm-fixed-select',
            'selected'  =>  $mega_menu_settings['fixed'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $effect_args = array(   // Old version effects
        array( 'fade', __( 'Fade', 'tsm' ) ),
        array( 'slide-top', __( 'Slide Top', 'tsm' ) ),
        array( 'slide-bottom', __( 'Slide Bottom', 'tsm' ) ),
        array( 'slide-left', __( 'Slide Left', 'tsm' ) ),
        array( 'slide-right', __( 'Slide Right', 'tsm' ) )
    );
    
    $effect_select_html = tsm_select_list(
        $effect_args,
        array(
            'before'    =>  '<div class="tsm-hidden" style="display: none;">',
            'after'     =>  '</div>',
            'label'     =>  __( 'Effect', 'tsm' ),
            'id'        =>  'tsm-effect-select',
            'class'     =>  'tsm-select tsm-select2 tsm-effect-select',
            'selected'  =>  $mega_menu_settings['effect'],
            'attrs'     =>  'data-width="120px"'
        ),
        false
    );
    
    $easing_args = array( // Easing for old version effects
        array( 'linear', __( 'linear', 'tsm' ) ),
        array( 'ease', __( 'ease', 'tsm' ) ),
        array( 'ease-in', __( 'ease-in', 'tsm' ) ),
        array( 'ease-out', __( 'ease-out', 'tsm' ) ),
        array( 'cubic-bezier(0.550, 0.085, 0.680, 0.530)', __( 'easeInQuad', 'tsm' ) ),
        array( 'cubic-bezier(0.550, 0.055, 0.675, 0.190)', __( 'easeInCubic', 'tsm' ) ),
        array( 'cubic-bezier(0.895, 0.030, 0.685, 0.220)', __( 'easeInQuart', 'tsm' ) ),
        array( 'cubic-bezier(0.755, 0.050, 0.855, 0.060)', __( 'easeInQuint', 'tsm' ) ),
        array( 'cubic-bezier(0.470, 0.000, 0.745, 0.715)', __( 'easeInSine', 'tsm' ) ),
        array( 'cubic-bezier(0.950, 0.050, 0.795, 0.035)', __( 'easeInExpo', 'tsm' ) ),
        array( 'cubic-bezier(0.600, 0.040, 0.980, 0.335)', __( 'easeInCirc', 'tsm' ) ),
        array( 'cubic-bezier(0.600, -0.280, 0.735, 0.045)', __( 'easeInBack', 'tsm' ) ),
        array( 'cubic-bezier(0.250, 0.460, 0.450, 0.940)', __( 'easeOutQuad', 'tsm' ) ),
        array( 'cubic-bezier(0.215, 0.610, 0.355, 1.000)', __( 'easeOutCubic', 'tsm' ) ),
        array( 'cubic-bezier(0.165, 0.840, 0.440, 1.000)', __( 'easeOutQuart', 'tsm' ) ),
        array( 'cubic-bezier(0.230, 1.000, 0.320, 1.000)', __( 'easeOutQuint', 'tsm' ) ),
        array( 'cubic-bezier(0.390, 0.575, 0.565, 1.000)', __( 'easeOutSine', 'tsm' ) ),
        array( 'cubic-bezier(0.190, 1.000, 0.220, 1.000)', __( 'easeOutExpo', 'tsm' ) ),
        array( 'cubic-bezier(0.075, 0.820, 0.165, 1.000)', __( 'easeOutCirc', 'tsm' ) ),
        array( 'cubic-bezier(0.175, 0.885, 0.320, 1.275)', __( 'easeOutBack', 'tsm' ) ),
        array( 'cubic-bezier(0.455, 0.030, 0.515, 0.955)', __( 'easeInOutQuad', 'tsm' ) ),
        array( 'cubic-bezier(0.645, 0.045, 0.355, 1.000)', __( 'easeInOutCubic', 'tsm' ) ),
        array( 'cubic-bezier(0.770, 0.000, 0.175, 1.000)', __( 'easeInOutQuart', 'tsm' ) ),
        array( 'cubic-bezier(0.860, 0.000, 0.070, 1.000)', __( 'easeInOutQuint', 'tsm' ) ),
        array( 'cubic-bezier(0.445, 0.050, 0.550, 0.950)', __( 'easeInOutSine', 'tsm' ) ),
        array( 'cubic-bezier(1.000, 0.000, 0.000, 1.000)', __( 'easeInOutExpo', 'tsm' ) ),
        array( 'cubic-bezier(0.785, 0.135, 0.150, 0.860)', __( 'easeInOutCirc', 'tsm' ) ),
        array( 'cubic-bezier(0.680, -0.550, 0.265, 1.550)', __( 'easeInOutBack', 'tsm' ) ),
    );
    
    $easing_select_html = tsm_select_list(
        $easing_args,
        array(
            'before'    =>  '<div class="tsm-hidden" style="display: none;">',
            'after'     =>  '</div>',
            'label'     =>  __( 'Easing', 'tsm' ),
            'id'        =>  'tsm-easing-select',
            'class'     =>  'tsm-select tsm-select2 tsm-easing-select tsm-hidden',
            'selected'  =>  $mega_menu_settings['easing'],
            'attrs'     =>  'data-width="170px"'
        ),
        false
    );
    
    // Modern version effects
    $effect_modern_in_args = array(
        array( 'bounce', __( 'Bounce', 'tsm' ) ),
        array( 'bounceIn', __( 'Bounce In', 'tsm' ) ),
        array( 'bounceInDown', __( 'Bounce In Down', 'tsm' ) ),
        array( 'bounceInLeft', __( 'Bounce In Left', 'tsm' ) ),
        array( 'bounceInRight', __( 'Bounce In Right', 'tsm' ) ),
        array( 'bounceInRight', __( 'Bounce In Right', 'tsm' ) ),
        array( 'fade', __( 'Fade', 'tsm' ) ),
        array( 'flash', __( 'Flash', 'tsm' ) ),
        array( 'lightSpeedIn', __( 'Light Speed In', 'tsm' ) ),
        array( 'pulse', __( 'Pulse', 'tsm' ) ),
        array( 'rollIn', __( 'RollIn', 'tsm' ) ),
        array( 'rubberBand', __( 'Rubber Band', 'tsm' ) ),
        array( 'shake', __( 'Shake', 'tsm' ) ),
        array( 'swing', __( 'Swing', 'tsm' ) ),
        array( 'tada', __( 'Tada', 'tsm' ) ),
        array( 'wobble', __( 'Wobble', 'tsm' ) ),
        array( 'flip', __( 'Flip', 'tsm' ) ),
        array( 'flipInX', __( 'Flip In X', 'tsm' ) ),
        array( 'flipInY', __( 'Flip In Y', 'tsm' ) ),
        array( 'glide', __( 'Glide', 'tsm' ) ),
        array( 'rotateIn', __( 'Rotate In', 'tsm' ) ),
        array( 'rotateInDownLeft', __( 'Rotate In Down Left', 'tsm' ) ),
        array( 'rotateInDownRight', __( 'Rotate In Down Right', 'tsm' ) ),
        array( 'rotateInUpLeft', __( 'Rotate In Up Left', 'tsm' ) ),
        array( 'rotateInUpRight', __( 'Rotate In Up Right', 'tsm' ) ),
        array( 'slideDown', __( 'Slide Down', 'tsm' ) ),
        array( 'slideDownBig', __( 'Slide Down Big', 'tsm' ) ),
        array( 'slideDownSmall', __( 'Slide Down Small', 'tsm' ) ),
        array( 'slideLeft', __( 'Slide Left', 'tsm' ) ),
        array( 'slideLeftBig', __( 'Slide Left Big', 'tsm' ) ),
        array( 'slideLeftSmall', __( 'Slide Left Small', 'tsm' ) ),
        array( 'slideRight', __( 'Slide Right', 'tsm' ) ),
        array( 'slideRightBig', __( 'Slide Right Big', 'tsm' ) ),
        array( 'slideRightSmall', __( 'Slide Right Small', 'tsm' ) ),
        array( 'slideUp', __( 'Slide Up', 'tsm' ) ),
        array( 'slideUpBig', __( 'Slide Up Big', 'tsm' ) ),
        array( 'slideUpSmall', __( 'Slide Up Small', 'tsm' ) ),
        array( 'zoomIn', __( 'Zoom In', 'tsm' ) ),
        array( 'zoomInDown', __( 'Zoom In Down', 'tsm' ) ),
        array( 'zoomInLeft', __( 'Zoom In Left', 'tsm' ) ),
        array( 'zoomInRight', __( 'Zoom In Right', 'tsm' ) ),
        array( 'zoomInUp', __( 'Zoom In Up', 'tsm' ) )
    );
    
    $effect_modern_exits_args = array(
        array( 'bounceOut', __( 'Bounce Out', 'tsm' ) ),
        array( 'bounceOutDown', __( 'Bounce Out Down', 'tsm' ) ),
        array( 'bounceOutLeft', __( 'Bounce Out Left', 'tsm' ) ),
        array( 'bounceOutRight', __( 'Bounce Out Right', 'tsm' ) ),
        array( 'bounceOutUp', __( 'Bounce Out Up', 'tsm' ) ),
        array( 'fadeOut', __( 'Fade Out', 'tsm' ) ),
        array( 'fadeOutDown', __( 'Fade Out Down', 'tsm' ) ),
        array( 'fadeOutDownBig', __( 'Fade Out Down Big', 'tsm' ) ),
        array( 'fadeOutLeft', __( 'Fade Out Left', 'tsm' ) ),
        array( 'fadeOutLeftBig', __( 'Fade Out Left Big', 'tsm' ) ),
        array( 'fadeOutRight', __( 'Fade Out Right', 'tsm' ) ),
        array( 'fadeOutRightBig', __( 'Fade Out Right Big', 'tsm' ) ),
        array( 'fadeOutUp', __( 'Fade Out Up', 'tsm' ) ),
        array( 'fadeOutUpBig', __( 'Fade Out Up Big', 'tsm' ) ),
        array( 'flipOutX', __( 'Flip Out X', 'tsm' ) ),
        array( 'flipOutY', __( 'Flip Out Y', 'tsm' ) ),
        array( 'lightSpeedOut', __( 'Light Speed Out', 'tsm' ) ),
        array( 'rollOut', __( 'Roll Out', 'tsm' ) ),
        array( 'rotateOut', __( 'Rotate Out', 'tsm' ) ),
        array( 'rotateOutDownLeft', __( 'Rotate Out Down Left', 'tsm' ) ),
        array( 'rotateOutDownRight', __( 'Rotate Out Down Right', 'tsm' ) ),
        array( 'rotateOutUpLeft', __( 'Rotate Out Up Left', 'tsm' ) ),
        array( 'rotateOutUpRight', __( 'Rotate Out Up Right', 'tsm' ) ),
        array( 'slideOutDown', __( 'Slide Out Down', 'tsm' ) ),
        array( 'slideOutLeft', __( 'Slide Out Left', 'tsm' ) ),
        array( 'slideOutRight', __( 'Slide Out Right', 'tsm' ) ),
        array( 'slideOutUp', __( 'Slide Out Up', 'tsm' ) ),
        array( 'zoomOut', __( 'Zoom Out', 'tsm' ) ),
        array( 'zoomOutDown', __( 'Zoom Out Down', 'tsm' ) ),
        array( 'zoomOutLeft', __( 'Zoom Out Left', 'tsm' ) ),
        array( 'zoomOutRight', __( 'Zoom Out Right', 'tsm' ) ),
        array( 'zoomOutUp', __( 'Zoom Out Up', 'tsm' ) )
    );
    
    $show_effect_modern_select_html = tsm_select_list(
        $effect_modern_in_args,
        array(
            'label'     =>  __( 'Show Effect', 'tsm' ),
            'id'        =>  'tsm-show-effect-select',
            'class'     =>  'tsm-select tsm-select2 tsm-show-effect-select',
            'selected'  =>  $mega_menu_settings['show_effect'],
            'attrs'     =>  'data-width="170px"'            
        ),
        false
    );
    
    $hidden_effect_modern_select_html = tsm_select_list(
        $effect_modern_exits_args,
        //$effect_modern_in_args,
        array(
            'label'     =>  __( 'Hide Effect', 'tsm' ),
            'id'        =>  'tsm-hide-effect-select',
            'class'     =>  'tsm-select tsm-select2 tsm-hide-effect-select',
            'selected'  =>  $mega_menu_settings['hide_effect'],
            'attrs'     =>  'data-width="170px"'
        ),
        false
    );
    
    
    //$settings_select_html .= $pos_select_html . $showon_select_html . $responsive_select_html . $fullwidth_select_html . $sticky_select_html . $fixed_select_html;
    $settings_select_html .= $fullwidth_select_html . $fixed_select_html;
    $settings_select_html .= $effect_select_html . $easing_select_html;
    
    $menu_theme_params = $mega_menu_settings['menu_theme_params'];
    $menu_theme_params_default = array(
        'theme_key'     =>  'default',
        'theme_params'  =>  $tsm_theme_list['default']['params']
    );
    
    $menu_theme_params = array_merge( $menu_theme_params_default, $menu_theme_params );
    
    
    ?>
        
        <div data-selected-menu-id="<?php echo $menu_id; ?>" id="tsm-mega-menu-settings-form" class="tsm-mega-menu-settings-form">
            <div class="tsm-box">
                <p>
                    <input <?php checked( true, $mega_menu_settings['enable_mega_menu'] == 'yes' ); ?> id="tsm-enable-mega-menu-settings" type="checkbox" class="tsm-cb tsm-enable-mega-menu-settings checkbox" />
                    <label for="tsm-enable-mega-menu-settings"><?php _e( 'Enable', 'tsm' ); ?></label>
                </p>
                
                <?php echo $settings_select_html; ?>
                
                <p style="display: none;">
                    <label for="tsm-speed-input"><?php _e( 'Speed', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $mega_menu_settings['speed']; ?>" data-min="0" id="tsm-speed-input" class="tsm-input tsm-speed-input tsm-non-negative-input" />
                </p>
                
                <p style="display: none;">
                    <label for="tsm-delay-input"><?php _e( 'Delay', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $mega_menu_settings['delay']; ?>" data-min="0" id="tsm-delay-input" class="tsm-input tsm-delay-input tsm-non-negative-input" />
                </p>
                
                <?php echo $show_effect_modern_select_html; ?>
                <p>
                    <label for="tsm-show-speed-input"><?php _e( 'Show Speed', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $mega_menu_settings['show_speed']; ?>" data-min="0" id="tsm-show-speed-input" class="tsm-input tsm-show-speed-input tsm-non-negative-input" />
                </p>
                
                <p>
                    <label for="tsm-show-delay-input"><?php _e( 'Show Delay', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $mega_menu_settings['show_delay']; ?>" data-min="0" id="tsm-show-delay-input" class="tsm-input tsm-show-delay-input tsm-non-negative-input" />
                </p>
                
                <?php echo $hidden_effect_modern_select_html; ?>
                <p>
                    <label for="tsm-hide-speed-input"><?php _e( 'Hide Speed', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $mega_menu_settings['hide_speed']; ?>" data-min="0" id="tsm-hide-speed-input" class="tsm-input tsm-hide-speed-input tsm-non-negative-input" />
                </p>
                
                <p>
                    <label for="tsm-hide-delay-input"><?php _e( 'Hide Delay', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $mega_menu_settings['hide_delay']; ?>" data-min="0" id="tsm-hide-delay-input" class="tsm-input tsm-hide-delay-input tsm-non-negative-input" />
                </p>
                
                <p>
                    <label for="tsm-break-point-input"><?php _e( 'Break Point', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $breakpoint; ?>" data-min="0" id="tsm-break-point-input" class="tsm-input tsm-delay-break-point tsm-non-negative-input" />
                </p>
                
                <p>
                    <label for="tsm-z-index-input"><?php _e( 'Z-index', 'tsm' ); ?></label>
                    <input type="text" value="<?php echo $zindex; ?>" data-min="0" id="tsm-z-index-input" class="tsm-input tsm-z-index-point tsm-non-negative-input" />
                </p>
                
                <p class="tsm-theme-select-wrap" style="display: none;">
                    <label for="tsm-theme-select"><?php _e( 'Menu Theme', 'tsm' ); ?></label>
                    <?php tsm_choose_css_theme( $menu_theme_params['theme_key'], array( 'id' => 'tsm-theme-select', 'attrs' => 'data-width="170px"' ) ); ?>
                </p>
                
                <div class="tsm-theme-settings-wrap" style="display: none;">
                    <?php tsm_load_theme_settings( $menu_theme_params['theme_key'], $menu_theme_params['theme_params'] ); ?>
                </div><!-- .tsm-theme-settings-wrap -->
            
            </div><!-- .tsm-box -->
            
            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
            
            <div class="tsm-choose-css-for-megamenu-wrap" style="display: none;">
                
                <?php
                    // This div may be removed in modern version
                    /*$choose_css_temp_options = array(
                        'hidden_get_css'        =>  'yes',
                        'hidden_light'          =>  'yes',
                        'hidden_temp_list'      =>  'yes',
                        'enable_actions_btns'   =>  'no'
                    );
                    tsm_choose_css_temp_megamenu( $choose_css_temp_options );
                    */
                ?>
                
            </div><!-- .tsm-choose-css-for-megamenu-wrap -->
            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
            
            <div id="tsm-status-bar" class="tsm-status-bar">
                <div class="tsm-box col-xs-12 col-sm-6">
                    <div data-saving-status="no" class="tsm-satus-info-wrap tsm-desc tsm-show-hidden">
                        
                    </div>
                </div>
                <div class="tsm-box col-xs-12 col-sm-6">
                    <a data-menu-id="<?php echo $menu_id; ?>" class="button-primary tsm-btn tsm-save-mega-menu tsm-float-right"><i class="fa fa-floppy-o"></i><?php _e( 'Save Mega Data', 'tsm' ); ?></a>
                    <a href="#tsm-live-preview-wrap" class="tsm-show-hide-live-css-preview tsm-btn tsm-float-right button" style="display: none;">
                        <i class="fa fa-magic"></i>
                        <?php _e( 'Live CSS Editor', 'tsm' ); ?>
                    </a>
                </div>
            </div><!-- #tsm-status-bar -->
                
            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
            
            <p>
                <a data-menu-id="<?php echo $menu_id; ?>" class="button-primary tsm-btn tsm-save-mega-menu tsm-float-right"><i class="fa fa-floppy-o"></i><?php _e( 'Save Mega Data', 'tsm' ); ?></a>
            </p>
        </div><!-- .tsm-mega-menu-settings-form -->
        
        <div style="display: none;" class="tsm-hidden">
            <?php
                /**
                 * Init hidden editor (for load via ajax) 
                 **/ 
                tsm_load_editor(); 
            ?>
        </div>
        
        <div class="tsm-row-col-form tsm-hidden">
            
            <div class="tsm-row-form">
                <?php tsm_row_form(); ?>
            </div>
            
            <div class="tsm-col-form">
                <?php tsm_col_form(); ?>
            </div>
            
        </div><!-- .tsm-row-col-form -->
        
        <div class="tsm-list-form-wrap tsm-hidden">
        
            <div class="tsm-list-form">
            
            </div>
        
        </div><!-- .tsm-list-form-wrap -->
        
        <div id="tsm-icons-chooser" class="tsm-icons-chooser tsm-hidden">
            
            <?php tsm_font_icons_chooser(); ?>
            
        </div>
        
        <div id="tsm-dropdown-li-form" class="tsm-dropdown-li-form tsm-hidden">
            
            <?php tsm_edit_dropdown_list_li(); ?>
            
        </div>
        
        <div id="tsm-icons-chooser-hidden-class" class="tsm-icons-chooser-hidden-class tsm-hidden">
            
            <?php tsm_font_icons_chooser(1, 30, true, false); ?>
            
        </div>
        
        <div class="tsm-info-temp">
            
        </div>
        
    <?php

    do_action( 'tsm_megamenu_save_settings' );

}

/**
 *  Load TSM live preview form via ajax
 *  @since 1.0 
 **/
function tsm_load_tsm_preview_form_via_ajax() {  // Old
    
    $menu_id = $_REQUEST['menu_id'];
    
    /** Live preview options **/
    //$tsm_live_preview_options = array(
//        admin_url( 'admin-ajax.php' ) . '?action=tsm_menu_preview_via_ajax&menu_id=' . $menu_id  =>  array(
//            'name'  =>  __( 'TSM Menu Preview', 'tsm' ),
//            'def'   =>  array(
//                '.tsm-menu-wrap-' . $menu_id . 'ul.ts-megamenu'  =>  array(
//                    'name'  =>  __( 'Main BG Color', 'tsm' ),
//                    'props' =>  '[\'background-color\']'
//                )
//            )
//        )
//    );
    
    $preview_url = admin_url( 'admin-ajax.php' ) . '?action=tsm_menu_preview_via_ajax&menu_id=' . $menu_id;
    $preview_name = __( 'TSM Menu Preview', 'tsm' );
    
    $script =   "preview_options['" . $preview_url . "'] = { // preview_url
                    name: '" . $preview_name . "',
                    def:{
                        '.tsm-menu-wrap-" . $menu_id . " ul.ts-megamenu': {
                            name: " . __( "Main BG Color", "tsm" ) . ",
                            props: ['background-color']
                        }
                    }
                };";
    
    echo $script;
    
    die();
}
add_action( 'wp_ajax_tsm_load_tsm_preview_form_via_ajax', 'tsm_load_tsm_preview_form_via_ajax' );

/**
 * Get the current menu ID.
 *
 * Most of this taken from wp-admin/nav-menus.php (no built in functions to do this)
 *
 * @since 1.0
 * @return int
 */
function tsm_get_selected_menu_id() {

    $nav_menus = wp_get_nav_menus( array('orderby' => 'name') );

    $menu_count = count( $nav_menus );

    $nav_menu_selected_id = isset( $_REQUEST['menu'] ) ? (int) $_REQUEST['menu'] : 0;

    $add_new_screen = ( isset( $_GET['menu'] ) && 0 == $_GET['menu'] ) ? true : false;

    // If we have one theme location, and zero menus, we take them right into editing their first menu
    $page_count = wp_count_posts( 'page' );
    $one_theme_location_no_menus = ( 1 == count( get_registered_nav_menus() ) && ! $add_new_screen && empty( $nav_menus ) && ! empty( $page_count->publish ) ) ? true : false;

    // Get recently edited nav menu
    $recently_edited = absint( get_user_option( 'nav_menu_recently_edited' ) );
    if ( empty( $recently_edited ) && is_nav_menu( $nav_menu_selected_id ) )
        $recently_edited = $nav_menu_selected_id;

    // Use $recently_edited if none are selected
    if ( empty( $nav_menu_selected_id ) && ! isset( $_GET['menu'] ) && is_nav_menu( $recently_edited ) )
        $nav_menu_selected_id = $recently_edited;

    // On deletion of menu, if another menu exists, show it
    if ( ! $add_new_screen && 0 < $menu_count && isset( $_GET['action'] ) && 'delete' == $_GET['action'] )
        $nav_menu_selected_id = $nav_menus[0]->term_id;

    // Set $nav_menu_selected_id to 0 if no menus
    if ( $one_theme_location_no_menus ) {
        $nav_menu_selected_id = 0;
    } elseif ( empty( $nav_menu_selected_id ) && ! empty( $nav_menus ) && ! $add_new_screen ) {
        // if we have no selection yet, and we have menus, set to the first one in the list
        $nav_menu_selected_id = $nav_menus[0]->term_id;
    }

    return $nav_menu_selected_id;
}

/**
 *  Display of each field type 
 *  @since 1.0
 **/
function tsm_get_field_display( $elem_args ) {
    
    $elem_type = ( isset( $elem_args['type'] ) ) ? esc_attr( $elem_args['type'] ) : '';
    
    if ( !isset( $elem_args['required'] ) ):
        $elem_args['required'] = 'no';  
    endif;
    
    if ( !isset( $elem_args['validate_empty'] ) ):
        $elem_args['validate_empty'] = 'no';  
    endif;
    
    $class = '';
    if ( isset( $elem_args['class'] ) ):
        $class .= wp_strip_all_tags( $elem_args['class'] );
    endif;
    
    $html = '';
    
    switch ( $elem_type ):
        
        case 'input_url':
                $title = ( isset( $elem_args['title'] ) ) ? sanitize_text_field( $elem_args['title'] ) : '';
                $placeholder = ( isset( $elem_args['placeholder'] ) ) ? esc_html( $elem_args['placeholder'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? esc_url( urldecode( $elem_args['val'] ) ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? esc_url( $elem_args['default'] ) : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? sanitize_text_field( $elem_args['desc'] ) : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                $html .=    '<h3 class="tsm-elem-title">' . $title . '</h3>
                            <input ' . $additional_attr . ' type="text" class="' . $class . ' tsm-input tsm-url-input widefat ' . $additional_class . '" value="' . $val . '" placeholder="' . $placeholder . '" />
                            <div class="tsm-desc">' . $desc . '</div>
                            ';
            break;
        
        case 'input_text':
                $title = ( isset( $elem_args['title'] ) ) ? sanitize_text_field( $elem_args['title'] ) : '';
                $placeholder = ( isset( $elem_args['placeholder'] ) ) ? esc_html( $elem_args['placeholder'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? sanitize_text_field( urldecode( $elem_args['val'] ) ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? sanitize_text_field( $elem_args['default'] ) : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? sanitize_text_field( $elem_args['desc'] ) : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                //$title_preview = ( isset( $elem_args['title_preview'] ) ) ? strtolower( wp_strip_all_tags( $elem_args['title_preview'] ) ) == 'yes': false;
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                $additional_attr .= ( isset( $elem_args['title_preview'] ) ) ? 'data-title-preview="' . strtolower( wp_strip_all_tags( $elem_args['title_preview'] ) ) . '" ' : 'data-title-preview="no"';
                
                $html .=    '<h3 class="tsm-elem-title">' . $title . '</h3>
                            <input ' . $additional_attr . ' type="text" class="' . $class . ' tsm-input tsm-text-input widefat ' . $additional_class . '" value="' . $val . '" placeholder="' . $placeholder . '" />
                            <div class="tsm-desc">' . $desc . '</div>
                            ';
            break;
        
        case 'input_hidden':
                $title = ( isset( $elem_args['title'] ) ) ? '<h3 class="tsm-elem-title">' . sanitize_text_field( $elem_args['title'] ) . '</h3>' : '';
                $placeholder = ( isset( $elem_args['placeholder'] ) ) ? esc_html( $elem_args['placeholder'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? sanitize_text_field( urldecode( $elem_args['val'] ) ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? sanitize_text_field( $elem_args['default'] ) : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? '<div class="tsm-desc">' . sanitize_text_field( $elem_args['desc'] ) . '</div>' : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                $html .=    $title . '<input ' . $additional_attr . ' type="hidden" class="' . $class . ' tsm-input tsm-hidden-input ' . $additional_class . '" value="' . $val . '" placeholder="' . $placeholder . '" />
                            ' . $desc;
            break;
        
        case 'img_upload':
                $title = ( isset( $elem_args['title'] ) ) ? sanitize_text_field( $elem_args['title'] ) : '';
                $placeholder = ( isset( $elem_args['placeholder'] ) ) ? esc_html( $elem_args['placeholder'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? intval( urldecode( $elem_args['val'] ) ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? intval( $elem_args['default'] ) : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? sanitize_text_field( $elem_args['desc'] ) : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                $img_preview_url = '';
                if ( $val != '' ):
                    $thumb = wp_get_attachment_image_src( $val, 'medium' );
                    $img_preview_url = $thumb['0'];
                else:
                    $img_preview_url = tsm_no_image( array( 'width' => 300, 'height' => 300 ) );
                endif;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                $target_id = uniqid( 'tsm-img-id-input-' );
                $target_src_id = uniqid( 'tsm-img-thumb-' );
                
                $html .=    '<h3 class="tsm-elem-title">' . $title . '</h3>
                            <div class="tsm-img-preview"><img id="' . $target_src_id . '" class="tsm-img-thumb" src="' . $img_preview_url . '" alt="" /></div>
                            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                            <a data-target-id="' . $target_id . '" data-target-src="' . $target_src_id . '" class="' . $class . ' tsm-wp-img-uploader button tsm-btn" href="" title="' . __( 'Upload', 'tsm' ) . '"><i class="fa fa-upload"></i>' . __( 'Upload', 'tsm' ) . '</a>
                            <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                            <input ' . $additional_attr . ' type="hidden" id="' . $target_id . '" class="tsm-input tsm-img-id-input widefat ' . $additional_class . '" value="' . $val . '" placeholder="' . $placeholder . '" readonly="readonly" />
                            <div class="tsm-desc">' . $desc . '</div>
                            ';
            break;
        
        case 'select':
                $title = ( isset( $elem_args['title'] ) ) ? sanitize_text_field( $elem_args['title'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? sanitize_text_field( urldecode( $elem_args['val'] ) ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? sanitize_text_field( $elem_args['default'] ) : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? sanitize_text_field( $elem_args['desc'] ) : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                
                $option_args = array();
                
                if ( isset( $elem_args['options'] ) ):
                    if ( !empty( $elem_args['options'] ) ):
                        $option_args = $elem_args['options'];
                    endif;
                endif;
                
                $options_html = '';
                if ( !empty( $option_args ) ):
                    
                    foreach ( $option_args as $opt_val => $opt_display ):
                        
                        $options_html .= '<option ' . selected( true, $val == $opt_val, false ) . ' value="' . $opt_val . '">' . $opt_display . '</option>';
                        
                    endforeach;
                    
                endif;
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                $html .= '<h3 class="tsm-elem-title">' . $title . '</h3>';
                if ( $options_html != '' ):
                    $html .=    '<select ' . $additional_attr . ' class="' . $class . ' tsm-select tsm-select2 widefat ' . $additional_class . '">
                                    ' . $options_html . '
                                </select>
                                <div class="tsm-desc">' . $desc . '</div>
                                ';
                endif;
                
            break;
        
        case 'no_special_char':
                $title = ( isset( $elem_args['title'] ) ) ? sanitize_text_field( $elem_args['title'] ) : '';
                $placeholder = ( isset( $elem_args['placeholder'] ) ) ? esc_html( $elem_args['placeholder'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? sanitize_text_field( urldecode( $elem_args['val'] ) ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? sanitize_text_field( $elem_args['default'] ) : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? sanitize_text_field( $elem_args['desc'] ) : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                $html .=    '<h3 class="tsm-elem-title">' . $title . '</h3>
                            <input ' . $additional_attr . ' type="text" class="' . $class . ' tsm-input tsm-no-special-chars-input widefat ' . $additional_class . '" value="' . $val . '" placeholder="' . $placeholder . '" />
                            <div class="tsm-desc">' . $desc . '</div>
                            ';
            break;
        
        case 'editor':
                $title = ( isset( $elem_args['title'] ) ) ? sanitize_text_field( $elem_args['title'] ) : '';
                $val = ( isset( $elem_args['val'] ) ) ? urldecode( $elem_args['val'] ) : '';
                $default = ( isset( $elem_args['default'] ) ) ? $elem_args['default'] : '';
                $desc = ( isset( $elem_args['desc'] ) ) ? sanitize_text_field( $elem_args['desc'] ) : '';
                $required = ( $elem_args['required'] == 'yes' );
                $validate_empty = ( $elem_args['validate_empty'] == 'yes' );
                $editor_id = ( isset( $elem_args['editor_id'] ) ) ? sanitize_key( $elem_args['editor_id'] ) : uniqid( 'tsm-wp-editor-' );
                
                $val = ( $val == '' ) ? $default : $val;
                $additional_attr = ' ';
                $additional_class = 'tsm-field-type-' . $elem_type;
                
                if ( $required ):
                    $additional_attr .= 'data-required="yes" ';
                    $additional_class .= 'tsm-required ';
                endif;
                
                if ( $validate_empty ):
                    $additional_attr .= 'data-validate-empty="yes" ';
                    $additional_class .= 'tsm-validate-empty ';
                endif;
                
                ob_start();
                wp_editor( $val, $editor_id, array( 'editor_class' => '' . $class . ' tsm-wp-editor', 'textarea_rows' => 8 ) );
                $editor_html = ob_get_contents();
                ob_clean();
                ob_end_flush();
                
                $html .=    '<h3 class="tsm-elem-title">' . $title . '</h3>
                                ' . $editor_html . '
                            <div class="tsm-desc">' . $desc . '</div>
                            ';
            break;
        
    endswitch;
    
    return $html;
}

/**
 *  Load element via ajax 
 **/
function tsm_load_elem_via_ajax() {
    global $tsm_elems_map;
    
	$errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'you do not have enough permissions to edit menus!', 'tsm' );
        
    endif;
    
    $elem_key = ( isset( $_POST['elem_key'] ) ) ? sanitize_text_field( $_POST['elem_key'] ) : '';
    $data_elem = ( isset( $_POST['data_elem'] ) ) ? $_POST['data_elem'] : '';
    $elem_index = ( isset( $_POST['elem_index'] ) ) ? intval( $_POST['elem_index'] ) : 1;
    $is_widget = ( isset( $_POST['widget_elem'] ) ) ? $_POST['widget_elem'] == 'yes' : false;
    
    $html = '';
    $elem_form_inner_html = '';
    //$elem_fields_html = '';
    
    if ( $elem_key != '' && empty( $errors ) ):
        
        if ( !$is_widget ):
            
            // Normal Element
            if ( array_key_exists( $elem_key, $tsm_elems_map ) ):
                
                $elem_form_inner_html .= '<div data-elem-index="' . $elem_index . '" class="tsm-elem-form-inner">';
                
                //$elem_args_default = $tsm_elems_map[$elem_key];    
                $elem_args = $data_elem; //$tsm_elems_map[$elem_key];
                
                // Default args for elem merge
                /*$default_elem_arg = array(
                    'name'              =>  '',
                    'desc'              =>  '',
                    'fields'            =>  array(),
                    'short_code'        =>  'tsm_default_short_code'  // Default short code to display this element
                );*/
                
                $default_elem_arg = $tsm_elems_map[$elem_key];
                
               // $elem_args = array_merge( $default_elem_arg, $elem_args );
                $elem_args = tsm_array_merge_recursive_replaces( $default_elem_arg, $elem_args );    
                
                $elem_name = sanitize_text_field( $elem_args['name'] );
                $elem_desc = sanitize_text_field( $elem_args['desc'] );  
                
                $elem_form_inner_html .= '<div class="col-xs-12 col-sm-4 tsm-edit-elem-left-part">';
                $elem_form_inner_html .= '<h3 class="tsm-elem-form-name">' . $elem_name . '</h3>';
                $elem_form_inner_html .= '<div class="tsm-desc">' . $elem_desc . '</div>';
                $elem_form_inner_html .= '</div><!-- .tsm-edit-elem-left-part -->';
                
                $elem_form_inner_html .= '<div class="col-xs-12 col-sm-8 tsm-edit-elem-right-part">';
                
                if ( !empty( $elem_args['fields'] ) ):
                    
                    $field_num = 0;
                    
                    foreach ( $elem_args['fields'] as $field_key => $field_args ):
                        
                        $field_num++;
                        $field_key = sanitize_key( $field_key );
                        $field_type = isset( $field_args['type'] ) ? sanitize_key( $field_args['type'] ) : '';
                        $field_required = isset( $field_args['required'] ) ? sanitize_key( $field_args['required'] ) : 'no';
                        $field_validate_empty = isset( $field_args['validate_empty'] ) ? sanitize_key( $field_args['validate_empty'] ) : 'no';
                        
                        if ( $field_type == 'editor' ):
                            
                            $field_args['editor_id'] = 'tsm-wp-editor-' . $elem_index . '_' . $field_num; // Id for wp editor only
                            
                        endif;
                        
                        if ( isset( $field_args['type'] ) ):
                            $elem_form_inner_style = ( $field_args['type'] == 'input_hidden' ) ? 'style="display: none;"': '';
                        endif; 
                        
                        $elem_form_inner_html .=    '<div data-field-num="' . $field_num . '" data-field-key="' . $field_key . '" data-field-type="' . $field_type . '" data-field-required="' . $field_required . '" data-field-validate-empty="' . $field_validate_empty . '" class="tsm-elem-field tsm-elem-field-' . $field_type . '" ' . $elem_form_inner_style . '>
                                                        ' . tsm_get_field_display( $field_args ) . '
                                                    </div>';
                        
                    endforeach;
                    
                endif;
                
                $elem_form_inner_html .= '</div><!-- .tsm-edit-elem-right-part -->';
                
                $elem_form_inner_html .= '</div><!-- .tsm-elem-form-inner -->';
            endif;
        
        // If is widget
        else:
            
            global $wp_widget_factory;
            
            $widget_class = $elem_key;  // In this case, $elem_key is widget class
            
            $instance_str = urldecode( $data_elem );  // query string format
            parse_str( $instance_str, $instance );
            
            foreach( $wp_widget_factory->widgets as $class => $info ):
            
                switch ( $class ):
                    case $widget_class:
                        $widget = new $class();
                        $widget->number = rand();
                        $widget_name = sanitize_text_field( $widget->name );
                        $widget_options = $widget->widget_options;
                        
                        $elem_form_inner_html .= '<div class="col-xs-12 col-sm-4 tsm-edit-elem-left-part">';
                        $elem_form_inner_html .= '<h3 class="tsm-elem-form-name">' . $widget_name . '</h3>';
                        $elem_form_inner_html .= '<div class="tsm-desc">' . $widget_options['description'] . '</div>';
                        $elem_form_inner_html .= '</div><!-- .tsm-edit-elem-left-part -->';
                        
                        $elem_form_inner_html .= '<div class="col-xs-12 col-sm-8 tsm-edit-elem-right-part">';
                                                
                        ob_start();
                    	$widget->form( $instance );
                    	$form = ob_get_clean();
                        
                    	// Convert the widget field naming into ones that panels uses
                    	$exp = preg_quote( $widget->get_field_name( '____' ) );
                    	$exp = str_replace( '____', '(.*?)', $exp );
                    	$form = preg_replace( '/' . $exp . '/', '$1', $form );
                        $elem_form_inner_html .= '<form>' . $form . '</form>';
                        
                        $elem_form_inner_html .= '</div><!-- .tsm-edit-elem-right-part -->';
                        
                        break;
                endswitch;
                
            endforeach;
            
        endif; // End if ( !$is_widget )
        
    endif;
    
    $elem_form_inner_html .=    '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                <div class="col-xs-12">
                                    <a href="#" class="button tsm-done-edit-elem">' . __( 'Done', 'tsm' ) . '</a>
                                </div>';
    
    $html .= $elem_form_inner_html;
    
    echo $html;
    
    tsm_display_note( $errors );
    
    die();
}
add_action( 'wp_ajax_tsm_load_elem_via_ajax', 'tsm_load_elem_via_ajax' );

/** 
 *  Load menu theme settings via ajax
 *  @since 1.0
 **/
function tsm_load_theme_settings_via_ajax() {
    
    $theme_key = $_POST['theme_key'];
    
    tsm_load_theme_settings( $theme_key );
    
    die();
} 
add_action( 'wp_ajax_tsm_load_theme_settings_via_ajax', 'tsm_load_theme_settings_via_ajax' );

/**
 *  Load menu theme settings
 *  @since 1.0 
 **/
function tsm_load_theme_settings( $theme_key = '', $params_vals = array(), $echo = true ) {
    global $tsm_theme_list, $tsm_color_schemes_list;
    
    $theme_key = sanitize_key( $theme_key );
    $params_vals = ( is_array( $params_vals ) ) ? $params_vals : ( array ) $params_vals;
    
    $html = '';
    $param_html = '';
    $this_theme_color_schemes_html = '';
    
    if ( array_key_exists( $theme_key, $tsm_color_schemes_list ) ):
    
        $color_scheme = $tsm_color_schemes_list[$theme_key];
        
        if ( !empty( $color_scheme ) ):
            
            $this_theme_color_schemes_html .= '<div class="tsm-color-scheme-wrap tsm-box">';
            
            foreach ( $color_scheme as $csch_key => $sdch_info ):
                
                $base_color_attrs = '';
                $base_color_attrs = 'data-base-color=\'' . json_encode( $sdch_info['colors'] ) . '\'';
                $this_style = 'style="background: ' . $sdch_info['primary_color'] . '"';
                
                $this_theme_color_schemes_html .= '<div data-color-scheme-key="' . $csch_key . '" ' . $base_color_attrs . ' ' . $this_style . ' class="tsm-color-scheme-choser"></div>';
                
            endforeach;
            
            $this_theme_color_schemes_html .= '</div><!-- .tsm-color-scheme-wrap -->';
            
        endif;
        
    endif;
    
    if ( array_key_exists( $theme_key, $tsm_theme_list ) ):
        
        $theme = $tsm_theme_list[$theme_key];
        $params = array_replace_recursive( $theme['params'], $params_vals );
        $params_odd_even_class = '';
        
        foreach ( $params as $param_key => $param ):
            
            if ( !isset( $param ) || empty( $param ) || !isset( $param['type'] ) ):
                continue;
            endif;
            
            $param_val = ( isset( $param['val'] ) ) ? $param['val'] : $param['default'];
            if ( is_array( $param_val ) ):
                $param_val = array_merge( $param_val, $param['default'] );
            endif;
            
            $params_odd_even_class = ( $params_odd_even_class == 'tsm-odd' ) ? 'tsm-even' : 'tsm-odd';
            
            $param_html .= '<div data-param-key="' . $param_key . '" data-param-type="' . $param['type'] . '" data-param-name="' . $param['param_name'] . '" class="tsm-theme-param-wrap tsm-box ' . $params_odd_even_class . '">';
            
            switch ( $param['type'] ):
                case 'color':
                        $base_color_key_attr = ( isset( $param['base_color_key'] ) ) ? 'data-base-color-key="' . $param['base_color_key'] . '"' : '';
                        $param_html .= '<label class="tsm-seting-color-label tsm-seting-label" for="tsm-' . $param_key . '">' . $param['display_name'] . '</label>';
                        $param_html .= '<input ' . $base_color_key_attr . ' data-color="' . $param_val . '" value="' . $param_val . '" type="text" id="tsm-' . $param_key . '" class="color tsm-param tsm-param-color tsm-color-picker" />';
                    break;
                case 'input_number':
                        $param_html .= '<label class="tsm-seting-num-label tsm-seting-label" for="tsm-' . $param_key . '">' . $param['display_name'] . '</label>';
                        $data_min = isset( $param['min'] ) ? 'data-min="' . intval( $param['min'] ) . '"' : '';
                        $data_max = isset( $param['max'] ) ? 'data-max="' . intval( $param['max'] ) . '"' : '';
                        $param_html .= '<input type="text" value="' . $param_val . '" ' . $data_min . ' ' . $data_max . ' id="tsm-' . $param_key . '" class="tsm-input tsm-param tsm-param-num tsm-non-negative-input" />';
                    break;
                case 'select':
                        if ( is_array( $param['options'] ) ):
                            if ( !empty( $param['options'] ) ):
                                
                                $param_html .= '<label class="tsm-seting-select-label tsm-seting-label" for="tsm-' . $param_key . '">' . $param['display_name'] . '</label>';
                                $select_html = '';
                                
                                $select_html .= '<select data-width="80px" id="tsm-' . $param_key . '" class="tsm-select2 tsm-select tsm-param tsm-param-select">';
                                foreach ( $param['options'] as $option_key => $option ):
                                    $val = ( isset( $option['val'] ) ) ? $option['val'] : '';
                                    $val_display = ( isset( $option['display'] ) ) ? $option['display'] : $val;
                                    if ( $val != '' ):
                                        $select_html .= '<option ' . selected( $param_val == $val, true, false ) . ' data-option-key="' . $option_key . '" value="' . $val . '">' . $val_display . '</option>';
                                    endif;
                                endforeach;
                                $select_html .= '</select>';
                                
                                $param_html .= $select_html;
                                
                            endif;
                        endif;
                    break;
                case 'input_number_table':
                        if ( isset( $param['cols_num'] ) ):
                            $param_html .= '<label class="tsm-seting-num-table-label tsm-seting-label" for="tsm-' . $param_key . '-0">' . $param['display_name'] . '</label>';
                            $table_html = '';
                            $cols_num = max( 1, intval( $param['cols_num'] ));
                            
                            $table_html .= '<table class="tsm-table-input tsm-table-noborder">
                                                <thead></thead>
                                                <tbody>
                                                    <tr>';
                                                    
                            for ( $i = 0; $i < $cols_num; $i++ ):
                                
                                $data_min = isset( $param['min'][$i] ) ? 'data-min="' . intval( $param['min'][$i] ) . '"' : '';
                                $data_max = isset( $param['max'][$i] ) ? 'data-max="' . intval( $param['max'][$i] ) . '"' : '';
                                $table_html .= '<td>
                                                <input type="text" value="' . $param_val[$i] . '" ' . $data_min . ' ' . $data_max . ' id="tsm-' . $param_key . '-' . $i . '" class="tsm-input tsm-param tsm-param-num-table tsm-non-negative-input" />
                                                </td>';
                                if ( ( $i + 1 ) % 2 == 0 && ( $i + 1 ) < $cols_num ):
                                    
                                    $table_html .= '</tr><tr>';
                                    
                                endif;
                                
                            endfor;
                            
                            $table_html .= '            </tr>
                                                    </tbody>
                                                <tfoot></tfoot>
                                            </table>';
                            
                            $param_html .= $table_html;
                            
                        endif;
                    break;
                
            endswitch;
            
            if ( isset( $param['desc'] ) ):
                
                if ( trim( $param['desc'] ) != '' ):
                    $param_html .= '<br /><div class="tsm-desc">' . sanitize_text_field( $param['desc'] ) . '</div>';
                endif;
                
            endif;
            
            $param_html .= '</div><!-- .tsm-theme-param-wrap -->';
            $param_html .= '<div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>';
            
        endforeach;
        
        $html = $this_theme_color_schemes_html . $html;
        $html .= '<div data-theme-key="' . $theme_key . '" class="tsm-theme-settings">';
        $html .= '<a href="#" data-target=".tsm-theme-params-wrap" class="tsm-show-adv">' . __( 'Toggle Menu Theme Options', 'tsm' ) . '</a>';
        $html .= '<div class="tsm-theme-params-wrap" style="display: none;">' . $param_html . '</div><!-- .tsm-theme-params-wrap -->';
        $html .= '</div><!-- .tsm-theme-settings -->';
        
    endif;
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}

/**
 *  Save mega menu data via ajax
 *  @since 1.0 
 **/
function tsm_save_mega_menu_data_via_ajax() {
    global $tsm_theme_list, $wp_filesystem;
    
	$errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'you do not have enough permissions to edit menus!', 'tsm' );
        
    endif;
    
    if ( empty( $errors ) ):
        
        $menu_data = isset( $_POST['menu_data'] ) ? $_POST['menu_data'] : array();
        $menu_id = isset( $_POST['menu_id'] ) ? $_POST['menu_id'] : '';
        
        $mega_menu_settings = isset( $menu_data['settings'] ) ? $menu_data['settings'] : array();
        $mega_menu_layout = isset( $menu_data['layout'] ) ? $menu_data['layout'] : array();
        
        if ( isset( $mega_menu_settings['menu_css_data']['tmp_colors'] ) ):
            
            $mega_menu_settings['menu_css_data']['tmp_colors'] = tsm_array_filter_recursive( $mega_menu_settings['menu_css_data']['tmp_colors'] );
            
        endif;
        
        /*
        if ( isset( $mega_menu_settings['menu_css_data']['css_base'] ) ): // Old version
        
            $breakpoint = isset( $mega_menu_settings['breakpoint'] ) ? max( 0, intval( $mega_menu_settings['breakpoint'] ) ) : 768;
            
            $menu_css_base_decode = urldecode( $mega_menu_settings['menu_css_data']['css_base'] );
            $mega_menu_settings['menu_css_frontend'] = tsm_insert_css_prefix( '.tsm-menu-wrap-' . $menu_id, $menu_css_base_decode, $breakpoint, false );
            $mega_menu_settings['menu_css_frontend'] = urlencode( $mega_menu_settings['menu_css_frontend'] );
            
        endif;
        */
        
        if ( isset( $mega_menu_settings['menu_theme_params'] ) ): // Modern version
            
            $breakpoint = isset( $mega_menu_settings['breakpoint'] ) ? max( 0, intval( $mega_menu_settings['breakpoint'] ) ) : 768;
            
            // Compile less to css
            if ( !class_exists( 'lessc' ) && file_exists( TSM_DIR . '/inc/lessc.inc.php' ) ):
                
                include_once TSM_DIR . '/inc/lessc.inc.php';
                
            endif;
            
            if ( class_exists( 'lessc' ) ):
            
                if ( empty( $wp_filesystem ) ):
                    require_once ABSPATH . '/wp-admin/includes/file.php';
                    WP_Filesystem();
                endif;
                
                $css_less = '';
                $css_compile_from_less = '';
                
                $less = new lessc; 
                
                $theme_key = ( isset( $mega_menu_settings['menu_theme_params']['theme_key'] ) ) ? $mega_menu_settings['menu_theme_params']['theme_key'] : '';
                
                if ( array_key_exists( $theme_key, $tsm_theme_list ) ):
                    
                    $theme_params_need_save = ( isset( $mega_menu_settings['menu_theme_params']['theme_params'] ) ) ? $mega_menu_settings['menu_theme_params']['theme_params'] : array();
                    $theme_params_default = $tsm_theme_list[$theme_key]['params'];
                    
                    // Import default params
                    if ( isset( $tsm_theme_list[$theme_key]['dir'] ) ):
                        
                        if ( file_exists( $tsm_theme_list[$theme_key]['dir'] . '/_params_only.less' ) ):
                            
                            $css_less .= tsm_file_get_content( $tsm_theme_list[$theme_key]['dir'] . '/_params_only.less' );
                            $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/_default.less' );
                            $css_less .= '@breakpoint : ' . $breakpoint . 'px;';
                            $css_less .= tsm_file_get_content( $tsm_theme_list[$theme_key]['dir'] . '/_blue.less' );
                            $css_less .= tsm_file_get_content( $tsm_theme_list[$theme_key]['dir'] . '/_params_only.less' );
                            
                        endif;
                    endif;
                    
                    foreach ( $theme_params_default as $theme_params_key => $theme_params_args ):
                        
                        $param_name = '@' . $theme_params_args['param_name'];
                        $param_val = '';
                        $param_unit = ( isset( $theme_params_args['unit'] ) ) ? $theme_params_args['unit'] : '';
                        
                        if ( array_key_exists( $theme_params_key, $theme_params_need_save ) ):
                            
                            if ( is_array( $theme_params_need_save[$theme_params_key]['val'] ) ):
                            
                                foreach ( $theme_params_need_save[$theme_params_key]['val'] as $val ):
                                    $param_val .= ' ' . $val . $param_unit;
                                endforeach;
                                
                            else:
                                
                                $param_val = $theme_params_need_save[$theme_params_key]['val'] . $param_unit;
                                
                            endif;
                            
                        else:
                        
                            if ( is_array( $theme_params_args['default'] ) ):
                            
                                foreach ( $theme_params_args['default'] as $val_default ):
                                    $param_val .= ' ' . $val_default . $param_unit;
                                endforeach;
                                
                            else:
                                
                                $param_val = $theme_params_args['default'] . $param_unit;
                                
                            endif;
                            
                        endif;
                        
                        $css_less .= $param_name . ':' . $param_val . ';';
                        
                    endforeach;
                    
                    if ( file_exists( TSM_DIR . '/inc/less/components.less' ) ):
                    
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/main.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/logo.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/simple-drop-down.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/content-drop-down.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/grid-system.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/search-place.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/_animations.less' );
                        
                        $animate_in_args = array( 'bounce', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInRight', 'fade', 'flash', 'lightSpeedIn', 'pulse', 'rollIn', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'flip', 'flipInX', 'flipInY', 'glide', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'slideDown', 'slideDownBig', 'slideDownSmall', 'slideLeft', 'slideLeftBig', 'slideLeftSmall', 'slideRight', 'slideRightBig', 'slideRightSmall', 'slideUp', 'slideUpBig', 'slideUpSmall', 'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp' );
                        $animate_exits_args = array( 'bounceOut', 'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp', 'fadeOut', 'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig', 'fadeOutUp', 'fadeOutUpBig', 'flipOutX', 'flipOutY', 'lightSpeedOut', 'rollOut', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'slideOutUp', 'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp' );
                        foreach ( $animate_in_args as $animate_file_name ):
                            
                            $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/animate.css/' . $animate_file_name . '.less' );
                            
                        endforeach;
                        
                        foreach ( $animate_exits_args as $animate_file_name ):
                            
                            $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/animate.exits.css/' . $animate_file_name . '.less' );
                            
                        endforeach;
                        
                        
                    endif;
                    
                    if ( file_exists( TSM_DIR . '/inc/less/_functions.less' ) ):
                        
                        $css_less .= '// ====================== CALL FUNCTIONS ======================
                                    // interval between items
                                    .mainItemInterval(@mainItemInterval);
                                    .dropItemInterval(@dropItemInterval);
                                    // rounded corners
                                    .mainRoundedCorners(@mainRadiusTL, @mainRadiusTR, @mainRadiusBL, @mainRadiusBR);
                                    .dropRoundedCorners(@dropRadiusTL, @dropRadiusTR, @dropRadiusBL, @dropRadiusBR);
                                    // drop margin
                                    .dropMargin(@dropFirstMargin, @dropMargin);
                                    // arrows
                                    .arrows(@arrowsSize, @arrowsOffset, @arrowsColor);
                                    // ====================== RESPONSIVE ======================';
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/responsive.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/_functions.less' );
                        
                    endif;
                    
                    // Compile
                    if ( $css_less != '' ):
                        try {
                            //echo $css_less;
                            $css_compile_from_less = $less->compile( $css_less );
                        } catch (exception $e) {
                            //$css_compile_from_less = "fatal error: " . $e->getMessage();
                            $css_compile_from_less = '';
                        }
                        
                    endif;
                    
                endif;
                
                // Insert prefix
                if ( $css_compile_from_less != '' ):
                    
                    $css_compile_from_less = tsm_insert_css_prefix( '.tsm-menu-wrap-' . $menu_id, $css_compile_from_less, $breakpoint, false );
                    
                    $mega_menu_settings['menu_css_theme_frontend'] = $css_compile_from_less;
                    $mega_menu_settings['menu_css_theme_frontend'] = urlencode( $mega_menu_settings['menu_css_theme_frontend'] );
                    
                endif;
                
            endif;
        
        endif;
        
        // Update mega menu settings
        update_post_meta( $menu_id, 'tsm_mega_menu_settings', $mega_menu_settings );
        
        // Update mega menu layout settings 
        if ( !empty( $mega_menu_layout ) && isset( $mega_menu_layout['layout_settings'] ) ):
            
            $menu_item_settings_default = array(
                'menu_item_id'      =>  0,
                'show_caret'        =>  'yes',
                'caret'             =>  'fa fa-angle-down',
                'drop_direction'    =>  'tsm-drop-right',
                'icon_class'        =>  '',
                'font_family'       =>  '',
                'mega_type'         =>  'grid'
            ); 
            
            foreach ( $mega_menu_layout['layout_settings'] as $menu_item_settings ):
            
                $menu_item_id = ( isset( $menu_item_settings['menu_item_id'] ) ) ? intval( $menu_item_settings['menu_item_id'] ) : '';
                $menu_item_settings = array_merge( $menu_item_settings_default, $menu_item_settings );
                update_post_meta( $menu_item_id, 'tsm_menu_item_settings', $menu_item_settings );
            
            endforeach;
            
        endif;
        
        // Update mega menu layout (grid data)
        if ( !empty( $mega_menu_layout ) && isset( $mega_menu_layout['grid'] ) ):
            
            foreach ( $mega_menu_layout['grid'] as $menu_item_data ):
                
                $menu_item_id = ( isset( $menu_item_data['menu_item_id'] ) ) ? intval( $menu_item_data['menu_item_id'] ) : '';
                $menu_item_grid_data = ( isset( $menu_item_data['grid_data'] ) ) ? $menu_item_data['grid_data'] : array();
                update_post_meta( $menu_item_id, 'tsm_menu_item_grid_data', $menu_item_grid_data );
                $grid_width = ( isset( $menu_item_data['width'] ) ) ? wp_strip_all_tags( $menu_item_data['width'] ) : 'auto';
                $grid_full_width = ( isset( $menu_item_data['fullwidth'] ) ) ? sanitize_key( $menu_item_data['fullwidth'] ) : 'no';
                $grid_min_width = ( isset( $menu_item_data['min_width'] ) ) ? max( 0, intval( $menu_item_data['min_width'] ) ) : 0;
                $grid_max_width = ( isset( $menu_item_data['max_width'] ) ) ? max( $grid_min_width, intval( $menu_item_data['max_width'] ) ) : 980;
                $menu_item_grid_options = array(
                    'width'         =>  $grid_width,
                    'fullwidth'     =>  $grid_full_width,
                    'min_width'     =>  $grid_min_width,
                    'max_width'     =>  $grid_max_width
                ); 
                update_post_meta( $menu_item_id, 'tsm_menu_item_grid_options', $menu_item_grid_options );
                
            endforeach;
            
        endif;
        
        // Update mega menu dropdown layout
        if ( !empty( $mega_menu_layout ) && isset( $mega_menu_layout['dropdown'] ) ):
            
            foreach ( $mega_menu_layout['dropdown'] as $menu_item_data ):
                
                $menu_item_id = ( isset( $menu_item_data['menu_item_id'] ) ) ? intval( $menu_item_data['menu_item_id'] ) : '';
                $menu_item_dropdown_data = ( isset( $menu_item_data['list_data'] ) ) ? $menu_item_data['list_data'] : array();
                update_post_meta( $menu_item_id, 'tsm_menu_item_dropdown_data', $menu_item_dropdown_data );
                
            endforeach;
            
        endif;
        
        // Update all mega menu data
        update_post_meta( $menu_id, 'tsm_mega_menu_data', $menu_data );
        
    endif;
    
    if ( $errors ):
        
        tsm_display_note( $errors );
        
    endif;
    
    die();
}
add_action( 'wp_ajax_tsm_save_mega_menu_data_via_ajax', 'tsm_save_mega_menu_data_via_ajax' );


/**
 *  Load css for preview via ajax
 *  @since 1.0 
 **/
function tsm_load_css_for_preview_via_ajax() {
    
    $errors = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'tsm-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'tsm' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'you do not have enough permissions to edit menus!', 'tsm' );
        
    endif;
    
    $preview_css = '';
    
    if ( empty( $errors ) ):
        
        $menu_id = isset( $_POST['menu_id'] ) ? $_POST['menu_id'] : '';
        $params_args = $_POST['menu_theme_params'];
        $breakpoint = isset( $_POST['breakpoint'] ) ? max( 0, intval( $_POST['breakpoint'] ) ) : 768;
        $fonts = $_POST['fonts'];
        $preview_css = tsm_less_to_css_from_params( $params_args, $breakpoint );
        
        // Insert prefix
        if ( $preview_css != '' ):
            
            $preview_css = tsm_insert_css_prefix( '.tsm-menu-wrap-' . $menu_id, $preview_css, $breakpoint, false );
            
        endif;
    
    endif;
    
    $return = array(
        'preview_css'       =>  $preview_css,
        'google_font_url'   =>  tsm_google_font_url( $fonts ),
        'errors'            =>  tsm_display_note( $errors, 'error', false )
    );
    
    echo json_encode( $return );
    
    die();
}
add_action( 'wp_ajax_tsm_load_css_for_preview_via_ajax', 'tsm_load_css_for_preview_via_ajax' );

/**
 *  Compile less to css
 *  @since 1.0 
 **/
function tsm_less_to_css_from_params( $params_args, $breakpoint ){
    global $tsm_theme_list;
    
    $css_less = '';
    $css_compile_from_less = '';
    
    if ( isset( $params_args ) ): // Modern version
        
        $breakpoint = max( 0, intval( $breakpoint ) );
        
        // Compile less to css
        if ( !class_exists( 'lessc' ) && file_exists( TSM_DIR . '/inc/lessc.inc.php' ) ):
            
            include_once TSM_DIR . '/inc/lessc.inc.php';
            
        endif;
        
        if ( class_exists( 'lessc' ) ):
        
            if ( empty( $wp_filesystem ) ):
                require_once ABSPATH . '/wp-admin/includes/file.php';
                WP_Filesystem();
            endif;
            
            $less = new lessc; 
            
            $theme_key = ( isset( $params_args['theme_key'] ) ) ? $params_args['theme_key'] : '';
            
            if ( array_key_exists( $theme_key, $tsm_theme_list ) ):
                
                $theme_params_need_save = ( isset( $params_args['theme_params'] ) ) ? $params_args['theme_params'] : array();
                $theme_params_default = $tsm_theme_list[$theme_key]['params'];
                
                // Import default params
                if ( isset( $tsm_theme_list[$theme_key]['dir'] ) ):
                    
                    if ( file_exists( $tsm_theme_list[$theme_key]['dir'] . '/_params_only.less' ) ):
                        
                        $css_less .= tsm_file_get_content( $tsm_theme_list[$theme_key]['dir'] . '/_params_only.less' );
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/_default.less' );
                        $css_less .= '@breakpoint : ' . $breakpoint . 'px;';
                        $css_less .= tsm_file_get_content( $tsm_theme_list[$theme_key]['dir'] . '/_blue.less' );
                        $css_less .= tsm_file_get_content( $tsm_theme_list[$theme_key]['dir'] . '/_params_only.less' );
                        
                    endif;
                endif;
                
                foreach ( $theme_params_default as $theme_params_key => $theme_params_args ):
                    
                    $param_name = '@' . $theme_params_args['param_name'];
                    $param_val = '';
                    $param_unit = ( isset( $theme_params_args['unit'] ) ) ? $theme_params_args['unit'] : '';
                    
                    if ( array_key_exists( $theme_params_key, $theme_params_need_save ) ):
                        
                        if ( is_array( $theme_params_need_save[$theme_params_key]['val'] ) ):
                        
                            foreach ( $theme_params_need_save[$theme_params_key]['val'] as $val ):
                                $param_val .= ' ' . $val . $param_unit;
                            endforeach;
                            
                        else:
                            
                            $param_val = $theme_params_need_save[$theme_params_key]['val'] . $param_unit;
                            
                        endif;
                        
                    else:
                    
                        if ( is_array( $theme_params_args['default'] ) ):
                        
                            foreach ( $theme_params_args['default'] as $val_default ):
                                $param_val .= ' ' . $val_default . $param_unit;
                            endforeach;
                            
                        else:
                            
                            $param_val = $theme_params_args['default'] . $param_unit;
                            
                        endif;
                        
                    endif;
                    
                    $css_less .= $param_name . ':' . $param_val . ';';
                    
                endforeach;
                
                if ( file_exists( TSM_DIR . '/inc/less/components.less' ) ):
                
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/main.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/logo.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/simple-drop-down.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/content-drop-down.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/grid-system.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/search-place.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/_animations.less' );
                    
                    $animate_in_args = array( 'bounce', 'bounceIn', 'bounceInDown', 'bounceInLeft', 'bounceInRight', 'bounceInRight', 'fade', 'flash', 'lightSpeedIn', 'pulse', 'rollIn', 'rubberBand', 'shake', 'swing', 'tada', 'wobble', 'flip', 'flipInX', 'flipInY', 'glide', 'rotateIn', 'rotateInDownLeft', 'rotateInDownRight', 'rotateInUpLeft', 'rotateInUpRight', 'slideDown', 'slideDownBig', 'slideDownSmall', 'slideLeft', 'slideLeftBig', 'slideLeftSmall', 'slideRight', 'slideRightBig', 'slideRightSmall', 'slideUp', 'slideUpBig', 'slideUpSmall', 'zoomIn', 'zoomInDown', 'zoomInLeft', 'zoomInRight', 'zoomInUp' );
                    $animate_exits_args = array( 'bounceOut', 'bounceOutDown', 'bounceOutLeft', 'bounceOutRight', 'bounceOutUp', 'fadeOut', 'fadeOutDown', 'fadeOutDownBig', 'fadeOutLeft', 'fadeOutLeftBig', 'fadeOutRight', 'fadeOutRightBig', 'fadeOutUp', 'fadeOutUpBig', 'flipOutX', 'flipOutY', 'lightSpeedOut', 'rollOut', 'rotateOut', 'rotateOutDownLeft', 'rotateOutDownRight', 'rotateOutUpLeft', 'rotateOutUpRight', 'slideOutDown', 'slideOutLeft', 'slideOutRight', 'slideOutUp', 'zoomOut', 'zoomOutDown', 'zoomOutLeft', 'zoomOutRight', 'zoomOutUp' );
                    foreach ( $animate_in_args as $animate_file_name ):
                        
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/animate.css/' . $animate_file_name . '.less' );
                        
                    endforeach;
                    
                    foreach ( $animate_exits_args as $animate_file_name ):
                        
                        $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/animate.exits.css/' . $animate_file_name . '.less' );
                        
                    endforeach;
                    
                    
                endif;
                
                if ( file_exists( TSM_DIR . '/inc/less/_functions.less' ) ):
                    
                    $css_less .= '// ====================== CALL FUNCTIONS ======================
                                // interval between items
                                .mainItemInterval(@mainItemInterval);
                                .dropItemInterval(@dropItemInterval);
                                // rounded corners
                                .mainRoundedCorners(@mainRadiusTL, @mainRadiusTR, @mainRadiusBL, @mainRadiusBR);
                                .dropRoundedCorners(@dropRadiusTL, @dropRadiusTR, @dropRadiusBL, @dropRadiusBR);
                                // drop margin
                                .dropMargin(@dropFirstMargin, @dropMargin);
                                // arrows
                                .arrows(@arrowsSize, @arrowsOffset, @arrowsColor);
                                // ====================== RESPONSIVE ======================';
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/components/responsive.less' );
                    $css_less .= tsm_file_get_content( TSM_DIR . '/inc/less/_functions.less' );
                    
                endif;
                
                // Compile
                if ( $css_less != '' ):
                    try {
                        //echo $css_less;
                        $css_compile_from_less = $less->compile( $css_less );
                    } catch (exception $e) {
                        //$css_compile_from_less = "fatal error: " . $e->getMessage();
                        $css_compile_from_less = '';
                    }
                    
                endif;
                
            endif;
            
        endif;
    
    endif;
    
    return $css_compile_from_less;
    
}

/**
 *  Load menu preview via ajax
 *  @since 1.0 
 **/
function tsm_menu_preview_via_ajax(){
    
    $locations = get_nav_menu_locations();
    $nav_menu_html = '<h1>' . __( 'Please choose location for this menu!', 'tsm' ) . '</h1>';
    
    $menu_id = $_REQUEST['menu_id'];
    $menu_settings = get_post_meta( $menu_id, 'tsm_mega_menu_settings', true );
    $additional_css = '';   // CSS from live css editor
    $custom_css = '';   // User custom css
    if ( isset( $menu_settings['additional_css'] ) ):
    
        $additional_css = '<style id="tsm-preview-style" type="text/css">' . $menu_settings['additional_css'] . '</style>';
        
    endif;
    
    if ( !empty( $locations ) ):
        
        foreach ( $locations as $location => $loca_menu_id ):
        
            if ( $loca_menu_id == $menu_id ):
                
                ob_start();
                wp_nav_menu( array( 'theme_location' => $location, 'menu' => $menu_id ) );
                $nav_menu_html = ob_get_contents();
                ob_clean();
                ob_end_flush();
                
            endif;
            
        endforeach;
    
    else:
    
        $nav_menu_html = '<h1>' . __( 'Your theme does not support navigation menus !', 'tsm' ) . '</h1>';
    
    endif;
    
    $temp_uri = get_template_directory_uri();
    $ajax_url = admin_url( 'admin-ajax.php' );
    
    $html = '<!DOCTYPE HTML>
            <html>
            <head>
            	<meta http-equiv="content-type" content="text/html" />
            	<meta name="author" content="Le Manh Linh" />
                <script type=\'text/javascript\' src=\'' . TSM_URL . '/js/jquery.min.js?' . TSM_VERSION . '\'></script>
                <script type=\'text/javascript\' src=\'' . TSM_URL . '/js/zm.jquery.js?' . TSM_VERSION . '\'></script>
                <script type=\'text/javascript\' src=\'' . TSM_URL . '/js/tsm-frontend-jquery.js?' . TSM_VERSION . '\'></script>
                <script type=\'text/javascript\' src=\'' . $ajax_url . '?action=tsm_enqueue_script_via_ajax&#038;' . TSM_VERSION . '\'></script>
                <link rel=\'stylesheet\' id=\'tsm-frontend-style-css\'  href=\'' . TSM_URL . '/css/tsm-frontend-style.css?' . TSM_VERSION . '\' type=\'text/css\' media=\'all\' />
                <link rel=\'stylesheet\' id=\'font-awesome-css\'  href=\'' . TSM_URL . '/font-awesome/css/font-awesome.css?' . TSM_VERSION . '\' type=\'text/css\' media=\'all\' />
                <link rel=\'stylesheet\' id=\'tsm-megamenu-css\'  href=\'' . $ajax_url . '?action=tsm_enqueue_style_via_ajax&#038;' . TSM_VERSION . '\' type=\'text/css\' media=\'all\' />
                ' . $additional_css . $custom_css . '
            	<title>' . __( 'Mega Menu Live CSS Efitor', 'tsm' ) . '</title>
            </head>
            
            <body>
            
            	' . $nav_menu_html . '
            
            </body>
            
            <footer></footer>
            
            </html>';
    
    echo $html;
    
    die();
}
add_action( 'wp_ajax_tsm_menu_preview_via_ajax', 'tsm_menu_preview_via_ajax' );


/**
 *  Display Grid
 *  @since 1.0 
 **/
function tsm_menu_item_grid( $menu_item_data = array() ) {
    
    $html = '';
    $elem_index = 0;
    if ( !empty( $menu_item_data ) ):
        
        $row_html = '';
        // Each item is a row
        foreach ( $menu_item_data as $row ):
            
            $additional_class = isset( $row['additional_class'] ) ? sanitize_text_field( $row['additional_class'] ) : '';
            
            $col_html = '';
            if ( isset( $row['cols'] ) ):
                
                if ( !empty( $row['cols'] ) ):
                    
                    foreach ( $row['cols'] as $col ):
                        
                        $col_w = ( isset( $col['col_width'] ) ) ? intval( $col['col_width'] ) : 4;
                        $elem_html = '';
                        if ( isset( $col['elems'] ) ):
                            
                            if ( !empty( $col['elems'] ) ):
                                
                                foreach ( $col['elems'] as $elem_data ):
                                    
                                    $elem_index++;
                                    $elem_html .= tsm_elem_block( $elem_data, $elem_index );
                                    
                                endforeach;
                                
                            endif;
                            
                        endif;
                        
                        $col_html .= tsm_col_form( $elem_html, $col, false );
                        
                    endforeach;
                    
                endif;
                
            endif; // if ( !isset( $row['cols'] ) )
            
            $row_html .= tsm_row_form( $col_html, false );
        
        endforeach;
        
        $html .= $row_html;
    
    endif;
    
    return $html;
}

/**
 *  Display Element (or Widget) block html on grid for drag, sortable, edit ...
 *  @since 1.0 
 **/
function tsm_elem_block( $elem_data = array(), $elem_index = 1 ) {
    global $tsm_elems_map, $wp_widget_factory;
    
    $html = '';
    
    if ( isset( $elem_data['elem_type'] ) ):
    
        $elem_type = esc_attr( $elem_data['elem_type'] );
        $elem_control_html = tsm_get_elem_control();
        
        switch ( $elem_type ):
            
            case 'normal':
                    
                    $elem_key = ( isset( $elem_data['elem_key'] ) ) ? trim( $elem_data['elem_key'] ) : '';
                    $elem_data_json = ( isset( $elem_data['data_json'] ) ) ? $elem_data['data_json'] : '';
                    $elem_data_obj = json_decode( $elem_data_json );
                    $additional_class = ( isset( $elem_data['additional_class'] ) ) ? sanitize_text_field( $elem_data['additional_class'] ) : '';
                    $elem_title = isset( $tsm_elems_map[$elem_key]['name'] ) ? sanitize_text_field( $tsm_elems_map[$elem_key]['name'] ) : '';
                    $elem_desc = isset( $tsm_elems_map[$elem_key]['desc'] ) ? sanitize_text_field( $tsm_elems_map[$elem_key]['desc'] ) : '';
                    
                    $preview_data = tsm_elem_block_preview_data( $elem_key, $elem_data_obj );
                    $class = '';
                    
                    if ( isset( $elem_data_obj->fields->tax->val ) ):
                        
                        if ( !taxonomy_exists( $elem_data_obj->fields->tax->val ) ):
                            
                            $elem_title = __( 'Invalid taxonomy', 'tsm' );
                            $elem_desc = __( 'Invalid taxonomy. Please check or delete this item', 'tsm' );
                            $class .= ' tsm-error danger';
                            $elem_control_html = tsm_get_elem_control( true );
                            
                        endif;
                        
                    endif;
                    
                    if ( isset( $elem_data_obj->fields->posttype->val ) ):
                        
                        if ( !post_type_exists( $elem_data_obj->fields->posttype->val ) ):
                            
                            $elem_title = __( 'Invalid post type', 'tsm' );
                            $elem_desc = __( 'Invalid post type. Please check or delete this item', 'tsm' );
                            $class .= ' tsm-error danger';
                            $elem_control_html = tsm_get_elem_control( true );
                            
                        endif;
                        
                    endif;
                    
                    $preview_style = '';
                    if ( isset( $preview_data['bg_img'] ) ):
                        if ( $preview_data['bg_img'] != '' ):
                            $preview_style .= 'background: url("' . $preview_data['bg_img'] . '") center center no-repeat transparent;';
                        endif;    
                    endif;                                    
                    
                    $html .=    '<div data-elem-key=\'' . $elem_key . '\' data-elem-index=\'' . $elem_index . '\' data-elem-json=\'' . $elem_data_json . '\' data-additional-class=\'' . $additional_class . '\' class=\'tsm-elem ' . $class . '\' style=\'' . $preview_style . '\' >
                                    <div class=\'tsm-elem-inner\'>
                                        <label class=\'tsm-elem-title\'>' . $elem_title . '</label>
                                        ' . $elem_control_html . '
                                        <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                        <div class=\'tsm-desc\'>' . $elem_desc . '</div>
                                        <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                        <p class=\'tsm-preview-title\'>' . $preview_data['title'] . '</p>
                                    </div>
                                </div>';
                    
                break;
            
            case 'widget':
                    
                    $widget_key = ( isset( $elem_data['widget_key'] ) ) ? trim( $elem_data['widget_key'] ) : '';
                    $widget_instance = ( isset( $elem_data['widget_instance'] ) ) ? trim( $elem_data['widget_instance'] ) : '';
                    $additional_class = ( isset( $elem_data['additional_class'] ) ) ? sanitize_text_field( $elem_data['additional_class'] ) : '';
                    $widget_name = '';
                    $widget_desc = '';
                    
                    if ( count( $wp_widget_factory->widgets ) ):
                        
                        foreach( $wp_widget_factory->widgets as $wg_class => $info ):
                            
                            if ( $widget_key == $wg_class ):
                                
                                $widget = new $wg_class(); // $widget_key
                                $widget_name = sanitize_text_field( $widget->name );
                                $widget_options = $widget->widget_options;
                                $widget_desc = $widget_options['description'];
                                $widget_json = json_encode($widget); 
                                
                                $html .=    '<div data-widget-key=\'' . $widget_key . '\' data-wg-instance=\'' . $widget_instance . '\' data-elem-index=\'' . $elem_index . '\' data-widget=\'' . $widget_json . '\' class=\'tsm-elem tsm-widget\'>
                                                <label class="tsm-elem-title">' . $widget_name . '</label>
                                                <div class="clearfix visible-xs-block visible-sm-block visible-md-block visible-lg-block"></div>
                                                <div class="tsm-desc">' . $widget_desc . '</div>
                                                ' . $elem_control_html . '
                                            </div>';
                                
                                break;
                                
                            endif;
                            
                        endforeach;
                        
                    endif; 
                    
                break;
            
        endswitch;
    
    endif;
    
    return $html;
}

/**
 *  Preview for elem block
 *  @since 1.0 
 **/
function tsm_elem_block_preview_data( $elem_key = '', $elem_data ) {
    
    $elem_key = sanitize_key( $elem_key );
    $preview_data = array(
        'bg_img'    =>  '',
        'title'     =>  '',
        'desc'      =>  ''
    );
    
    switch ( $elem_key ):
    
        case 'link':
            $img_id = intval( $elem_data->fields->img->val );
            if ( $img_id > 0 ):
                $thumb = wp_get_attachment_image_src( $img_id, 'medium' );
                $img_src = $thumb['0'];
                $preview_data['bg_img'] = $img_src;
            endif;
            
            $title = urldecode( $elem_data->fields->text->val );
            $preview_data['title'] = tsm_excerpts_from_str( $title );
            
            break;
        
        case 'editor':
            $title = urldecode( $elem_data->fields->title->val );
            $preview_data['title'] = tsm_excerpts_from_str( $title );
            break;
        
        default:
            
            $short_code = ( isset( $elem_data->short_code ) ) ? $elem_data->short_code : '';
            $title = '';
            
            if ( $short_code == 'tsm_term_link_img' || $short_code == 'tsm_posttype_link_img' ):
                $img_id = intval( $elem_data->fields->img->val );
                if ( $img_id > 0 ):
                    $thumb = wp_get_attachment_image_src( $img_id, 'medium' );
                    $img_src = $thumb['0'];
                    $preview_data['bg_img'] = $img_src;
                endif;
                if ( isset( $elem_data->fields->title->val ) ){
                    $title = urldecode( $elem_data->fields->title->val );
                }
                else{
                    if ( isset( $elem_data->fields->text->val ) ){
                        $title = urldecode( $elem_data->fields->text->val );
                    }
                }
                $preview_data['title'] = tsm_excerpts_from_str( $title );
            endif;
            
            break;
        
    endswitch;
    
    return $preview_data;
}

/**
 *  List form html
 *  @since 1.0 
 **/
function tsm_list_form( $list_data = array(), $lv = 0, $echo = true ) {
    global $tsm_script_localize;
    
    $list_control_html = tsm_get_list_form_control();
    
    $lv = max( 0, intval( $lv ) );
    
    $list_data_default = array(
        'list_w'            =>  $tsm_script_localize['vars']['default_list_w'],
        'addition_class'    =>  '',
        'items_data'        =>  array()
    );
    
    if ( !is_array( $list_data ) ):
        if ( is_object( $list_data ) ):
            $list_data = (array) $list_data;
        else:
            $list_data = $list_data_default;
        endif;
    endif;
    $list_data = array_merge( $list_data_default, $list_data );
    
    extract( $list_data );
    
    $list_w = max( 50, intval( $list_w ) );
    $list_w = floor( $list_w / 5 ) * 5;
    
    $addition_class = trim( $addition_class ); 
    $class = 'tsm-list w-' . $list_w . ' tsm-list-lv-' . $lv . ' ' . $addition_class;
    
    if ( isset( $item_data['children'] ) ):
        if ( !empty( $item_data['children'] ) ):
            $class .= ' tsm-has-children';
        endif;
    endif;
    
    $html = '';
                    
    if ( !empty( $items_data ) ):  // Stop conditions
        
        $html .=    '<ul data-lv=\'' . $lv . '\' data-list-w=\'' . $list_w . '\' data-addition-class=\'' . $addition_class . '\' class=\'' . $class . '\'>';
        
        foreach ( $items_data as $item_data ):
            
            if ( isset( $item_data['children'] ) ):
                
                $html .= tsm_item_for_list_form( $item_data, tsm_list_form( $item_data['children'], $lv + 1, false ), false );
                
            else:
                
                $html .= tsm_item_for_list_form( $item_data, '', false );                
                
            endif;
            
        endforeach;
        
        // Add actions of lv 0
        if ( $lv == 0 ):
            
            $html .=    '<div class=\'tsm-action-wrap tsm-list-lv-0-actions-wrap\'>
                            <a href=\'#\' title=\'' . __( 'Make this list smaller', 'tsm' ) . '\' class=\'tsm-action tsm-make-list-lv-0-smaller\'><i class=\'fa fa-angle-left\'></i></a>
                            <a href=\'#\' title=\'' . __( 'Make this list bigger', 'tsm' ) . '\' class=\'tsm-action tsm-make-list-lv-0-bigger\'><i class=\'fa fa-angle-right\'></i></a>
                            <a class=\'tsm-action tsm-add-item-lv-0\' href=\'#\' title=\'' . __( 'Add an item' ) . '\'><i class=\'fa fa-plus\'></i></a>
                            <a class=\'tsm-action tsm-duplicate-item-lv-0\' href=\'#\' title=\'' . __( 'Duplicate this list', 'tsm' ) . '\'><i class="fa fa-files-o"></i></a>
                            <a class=\'tsm-action tsm-del-list-lv-0 tsm-delete\' href=\'#\' title=\'' . __( 'Delete all items' ) . '\'><i class=\'fa fa-trash\'></i></a>
                        </div>';
            
        endif;
        
        $html .=    '</ul>';
        
    endif;           
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}

/**
 *  Item for list form
 *  @since 1.0
 **/
function tsm_item_for_list_form( $item_data = array(), $content_html = '', $echo = true ) {
    
    $html = '';
    
    $item_data_default = array(
        'li_obj_id'             =>  '',     // Post type id or term id (if is custom link: id = 0)\
        'li_obj_name'           =>  '',     // post, page or tax name or any custom post type, custom taxonomy
        'li_obj_label'          =>  __( 'Custom' ),
        'li_link_type'          =>  'custom', // 'custom', 'post_type' or 'taxonomy'
        'link'                  =>  '',
        'text'                  =>  '',
        'icon'                  =>  '',     // Class of font icon
        'right_align_dropdown'  =>  'no',
        'addition_class'        =>  '',
        'children'              =>  array()  // Sub list
    );
    
    if ( !is_array( $item_data ) ):
        if ( is_object( $item_data ) ):
            $item_data = (array) $item_data;
        else:
            $item_data = $item_data_default;
        endif;
    endif;
    //echo "<pre>";
//        print_r($item_data);
//        echo "</pre>";
    $item_data = array_merge( $item_data_default, $item_data );
    
    $class = ( $item_data['right_align_dropdown'] == 'yes' ) ? 'tsm-li tsm-right-align' : 'tsm-li';
    $class .= ' ' . trim( $item_data['addition_class'] );
    
    if ( !empty( $item_data['children'] ) ):
        $class .= ' tsm-has-children';
    endif;
    
    $obj_id = ( max( 0, intval( $item_data['li_obj_id'] ) ) );
    $obj_name = sanitize_text_field( $item_data['li_obj_name'] );
    $obj_label = sanitize_text_field( $item_data['li_obj_label'] );
    $link_type = sanitize_key( $item_data['li_link_type'] );
    $link = esc_url( $item_data['link'] );
    $text = sanitize_text_field( $item_data['text'] );
    $right_align = ( $item_data['right_align_dropdown'] == 'yes' ) ? 'yes' : 'no';
    $addition_class = trim( $item_data['addition_class'] );
    $children_data = json_encode( $item_data['children'] );
    
    $icon_html = ( trim( $item_data['icon'] ) != '' ) ? '<i class=\'' . trim( $item_data['icon'] ) . '\'></i>' : '';
        
    //$item_data_json = json_encode( $item_data );
    
    $html .=    '<li class=\'' . $class . '\' data-obj-id="' . $obj_id . '" data-obj-name="' . $obj_name . '" data-obj-label="' . $obj_label . '" data-link-type=\'' . $link_type . '\' data-link=\'' . $link . '\' data-text=\'' . $text . '\' data-icon=\'' . trim( $item_data['icon'] ) . '\' data-align-right=\'' . $right_align . '\' data-addition-class=\''. $addition_class . '\'>
                    <span>' . $icon_html . sanitize_text_field( $item_data['text'] ) . '
                        ' . tsm_list_li_actions_form( false ) . '
                    </span>
                    ' . $content_html . '
                </li>';
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}

/**
 *  Dropdown html for frontend display
 *  @since 1.0 
 **/
function tsm_list_dropdown_frontend( $list_data = array(), $drop_direction = '' ) {
    global $tsm_script_localize;
    
    $list_data_default = array(
        'list_w'            =>  $tsm_script_localize['vars']['default_list_w'],
        'addition_class'    =>  '',
        'items_data'        =>  array()
    );
    
    if ( !is_array( $list_data ) ):
        if ( is_object( $list_data ) ):
            $list_data = (array) $list_data;
        else:
            $list_data = $list_data_default;
        endif;
    endif;
    $list_data = array_merge( $list_data_default, $list_data );
    
    extract( $list_data );
    
    $list_w = max( 50, intval( $list_w ) );
    $list_w = floor( $list_w / 5 ) * 5;
    
    $addition_class = trim( $addition_class ); 
    $class = 'tsm-list w-' . $list_w . ' ' . $addition_class . sanitize_text_field( $drop_direction );
    
    if ( isset( $item_data['children'] ) ):
        if ( !empty( $item_data['children'] ) ):
            $class .= ' tsm-has-children';
        endif;
    endif;
    
    $html = '';
                    
    if ( !empty( $items_data ) ):  // Stop conditions
        
        $html .=    '<ul data-list-w=\'' . $list_w . '\' data-addition-class=\'' . $addition_class . '\' class=\'tsm-sublist ' . $class . ' ' . $addition_class . '\'>';
        
        foreach ( $items_data as $item_data ):
            
            if ( isset( $item_data['children'] ) ):
                
                $html .= tsm_item_for_list_frontend( $item_data, tsm_list_dropdown_frontend( $item_data['children'] ) );
                
            else:
                
                $html .= tsm_item_for_list_frontend( $item_data );                
                
            endif;
            
        endforeach;
        
        $html .=    '</ul>';
        
    endif;  
    
    return $html;
}

/**
 *  Display an item for list on frontend
 *  @since 1.0 
 **/
function tsm_item_for_list_frontend( $item_data = array(), $content_html = '' ) {
    $html = '';
    
    $item_data_default = array(
        'li_obj_id'             =>  '',     // Post type id or term id (if is custom link: id = 0)\
        'li_obj_name'           =>  '',     // post, page or tax name or any custom post type, custom taxonomy
        'li_link_type'          =>  'custom', // 'custom', 'post_type' or 'taxonomy'
        'link'                  =>  '',
        'text'                  =>  '',
        'icon'                  =>  '',     // Class of font icon
        'right_align_dropdown'  =>  'no',
        'addition_class'        =>  '',
        'children'              =>  array()  // Sub list
    );
    
    if ( !is_array( $item_data ) ):
        if ( is_object( $item_data ) ):
            $item_data = (array) $item_data;
        else:
            $item_data = $item_data_default;
        endif;
    endif;
    $item_data = array_merge( $item_data_default, $item_data );
    
    $class = ( $item_data['right_align_dropdown'] == 'yes' ) ? 'tsm-li tsm-right-align' : 'tsm-li';
    $class .= ' ' . trim( $item_data['addition_class'] );
    
    if ( !empty( $item_data['children'] ) ):
        $class .= ' tsm-has-children';
    endif;
    
    $obj_id = ( max( 0, intval( $item_data['li_obj_id'] ) ) );
    $obj_name = sanitize_text_field( $item_data['li_obj_name'] );
    $link_type = sanitize_key( $item_data['li_link_type'] );
    $link = esc_url( $item_data['link'] );
    $text = sanitize_text_field( $item_data['text'] );
    $right_align = ( $item_data['right_align_dropdown'] == 'yes' ) ? 'yes' : 'no';
    $addition_class = trim( $item_data['addition_class'] );
    $children_data = json_encode( $item_data['children'] );
    
    $icon_html = ( trim( $item_data['icon'] ) != '' ) ? '<i class=\'' . trim( $item_data['icon'] ) . '\'></i>' : '';
    $caret_html = ( trim( $content_html ) != '' ) ? '<i class="tsm-caret fa fa-angle-right"></i>' : '';
        
    if ( $link != '' ):
        $class .= ' tsm-has-link';
    else:
        $class .= ' tsm-no-link';
    endif;
    
    switch ( $link_type ):
        
        case 'post_type':
        
            if ( get_permalink( $obj_id ) ):
            
                $link = get_permalink( $obj_id );
                
            endif;
            break;
             
        case 'taxonomy':
            
            if ( get_term_link( $obj_id ) ):
            
                $link = get_term_link( $obj_id, $obj_name );//apply_filters( 'term_link', get_term_link( $obj_id, $obj_name ) );
                
            endif;
            break;
        
    endswitch;
    
    $html .=    '<li class=\'' . $class . '\'>
                    <a href=\'' . $link . '\' title=\'' . $text . '\'>
                        ' . $icon_html . '
                        ' . $caret_html . '
                        ' . $text . '
                    </a>
                    ' . trim( $content_html ) . '
                </li>';
    
    return $html;
}

function tsm_list_li_actions_form( $echo = true ) {
    $html = '<span class=\'tsm-li-actions-wrap\'>
                <a href=\'#\' title=\'' . __( 'Make sublist smaller', 'tsm' ) . '\' class=\'tsm-action tsm-make-sublist-smaller\'><i class=\'fa fa-angle-left\'></i></a>
                <a href=\'#\' title=\'' . __( 'Make sublist bigger', 'tsm' ) . '\' class=\'tsm-action tsm-make-sublist-bigger\'><i class=\'fa fa-angle-right\'></i></a>
                <a href=\'#\' title=\'' . __( 'Add a child item', 'tsm' ) . '\' class=\'tsm-action tsm-add-child-item\'><i class=\'fa fa-plus\'></i></a>
                <a href=\'#\' title=\'' . __( 'Edit item', 'tsm' ) . '\' class=\'tsm-action tsm-edit-item\'><i class=\'fa fa-pencil\'></i></a>
                <a href=\'#\' title=\'' . __( 'Duplicate item', 'tsm' ) . '\' class=\'tsm-action tsm-duplicate-item\'><i class=\'fa fa-files-o\'></i></a>
                <a href=\'#\' title=\'' . __( 'Delete item', 'tsm' ) . '\' class=\'tsm-action tsm-del-item tsm-delete\'><i class=\'fa fa-times\'></i></a>
            </span>';
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
}

/**
 *  Row html 
 *  @since 1.0 
 **/
function tsm_row_form( $content_html = '', $echo = true ) {
    
    $row_control_html = tsm_get_row_control();
    
    $html =     '<div data-row-class="tsm-row" class="tsm-row tsm-row">
                    <div class="tsm-cols-holder">' . $content_html . '</div>
                    ' . $row_control_html . '
                </div>';
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}

/**
 *  Col html 
 *  @since 1.0 
 **/
function tsm_col_form( $content_html = '', $options = array(), $echo = true ) {
    
    $col_w = ( isset( $options['col_width'] ) ) ? max( 1, intval( $options['col_width'] ) ): 4;
    $class = ( isset( $options['additional_class'] ) ) ? wp_strip_all_tags( $options['additional_class'] ) : '';
    $col_control_html = tsm_get_col_control();
    
    $html =     '<div data-col-width="' . $col_w . '" data-col-class="' . $class . '" class="tsm-col tsm-col c-' . $col_w . ' ' . $class . '">
                    <div class="tsm-elems-holder">' . $content_html . '</div>
                    ' .$col_control_html . '
                </div>';
    
    if ( $echo ):
        echo $html;
    else:
        return $html;
    endif;
    
}

/**
 *  List form control
 *  @since 1.0 
 **/
function tsm_get_list_form_control() {
    $control_html = '<div class="tsm-action-wrap" style="padding-left: 5px; padding-right: 5px;">
                        <a href="#" class="tsm-action tsm-add-item" title="' . __( 'Add item', '' ) . '"><i class="fa fa-plus"></i></a>
                        <a href="#" class="tsm-action tsm-del-list tsm-del" title="' . __( 'Delete this list', '' ) . '"><i class="fa fa-times"></i></a>
                    </div>';
    return $control_html;
}

/**
 *  Row control
 *  @since 1.0 
 **/
function tsm_get_row_control() {
    $row_control_html = '<div class="tsm-action-wrap" style="padding-left: 5px; padding-right: 5px;">
                            <a href="#" class="tsm-action tsm-add-col" title="' . __( 'Add column', '' ) . '"><i class="fa fa-plus"></i></a>
                            <a href="#" class="tsm-action tsm-duplicate-row" title="' . __( 'Duplicate row', 'tsm' ) . '"><i class="fa fa-files-o"></i></a>
                            <a href="#" class="tsm-action tsm-del-row tsm-del" title="' . __( 'Delete row', '' ) . '"><i class="fa fa-times"></i></a>
                        </div>';
    return $row_control_html;
}

/**
 *  Col control
 *  @since 1.0 
 **/
function tsm_get_col_control() {
    $col_control_html = '<div class="tsm-action-wrap">
                            <a href="#" class="tsm-action tsm-edit-col-class tsm-float-right">' . __( 'Class', 'tsm' ) . '</a>
                            <a href="#" class="tsm-action tsm-add-elem"><i class="fa fa-plus"></i></a>
                            <a href="#" class="tsm-action tsm-duplicate-col" title="' . __( 'Duplicate column', 'tsm' ) . '"><i class="fa fa-files-o"></i></a>
                            <a href="#" class="tsm-action tsm-smaller-col"><i class="fa fa-angle-left"></i></a>
                            <a href="#" class="tsm-action tsm-bigger-col"><i class="fa fa-angle-right"></i></a>
                            <a href="#" class="tsm-action tsm-del-col tsm-del" title="' . __( 'Delete col', '' ) . '"><i class="fa fa-times"></i></a>
                        </div>';
    return $col_control_html;
}

/**
 *  Elems control
 *  @since 1.0 
 **/
function tsm_get_elem_control( $disable_edit = false ) {
    $edit_html = ( $disable_edit ) ? '' : '<a href="#" class="tsm-action tsm-edit-elem"><i class="fa fa-pencil"></i></a>';
    $elem_control_html = '<div class="tsm-elem-action-wrap">
                            ' . $edit_html . '
                            <a href="#" class="tsm-action tsm-duplicate-elem" title="' . __( 'Duplicate element', 'tsm' ) . '"><i class="fa fa-files-o"></i></a>
                            <a href="#" class="tsm-action tsm-del-elem tsm-del" title="' . __( 'Delete', '' ) . '"><i class="fa fa-times"></i></a>
                        </div>';
    return $elem_control_html;
}



