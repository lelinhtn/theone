<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


/*
 * Suported elements
 */

global $tsm_elems_map;

$tsm_elems_map = array();

// Link type, include image
$tsm_link = array(
    'name'      =>  __( 'Link &frasl; image', 'tsm' ),
    'desc'      =>  __( 'Link or image', 'tsm' ),
    'fields'    =>  array(
        'link'  =>  array(
            'title'         =>  __( 'Link', 'tsm' ),
            'type'          =>  'input_url',                // Validate url
            'placeholder'   =>  __( 'Enter URL', 'tsm' ),
            'val'           =>  '',                         // Value when loaded
            'default'       =>  '',                         // Display default value if "val" is empty
            'required'      =>  'no',                       // "yes" or "no"
            'validate_empty'=>  'no'                        // "yes" or "no"
        ),
        'text'  =>  array(
            'title'         =>  __( 'Text', 'tsm' ),
            'type'          =>  'input_text',
            'placeholder'   =>  __( 'Text display', 'tsm' ),
            'val'           =>  ''
        ),
        'img'   =>  array(
            'title'         =>  __( 'Image', 'tsm' ),
            'type'          =>  'img_upload',               // Using WP Media Upload
            'desc'          =>  '',                         // Description
            'val'           =>  ''                          // Image id (attachment id)
        ),
        'target'            =>  array(
            'title'         =>  __( 'Target', 'tsm' ),
            'type'          =>  'select',
            'options'       =>  array(
                '_blank'        =>  __( '_blank', 'tsm' ),  // Key is select value
                '_parent'       =>  __( '_parent', 'tsm' ),
                '_self'         =>  __( '_self', 'tsm' ),
                '_top'          =>  __( '_top', 'tsm' )
            ),
            'val'           =>  '_blank',
            'default'       =>  '_blank'
        ),
        'addition_class'    =>  array(
            'title'         =>  __( 'Additional Class', 'tsm' ),
            'placeholder'   =>  __( 'Additional Class', 'tsm' ),
            'type'          =>  'no_special_char',          // No special char input
            'val'           =>  ''
        )             
    ),
    'short_code'            =>  'tsm_link_img'              // Short code function to display this element
);

// Editor
$tsm_editor = array(
    'name'      =>  __( 'Text editor', 'tsm' ),
    'desc'      =>  __( 'Text, html', 'tsm' ),
    'fields'    =>  array(
        'title'     =>  array(
            'type'          =>  'input_text',
            'placeholder'   =>  __( 'Title', 'tsm' ),
            'val'           =>  '',
            'class'         =>  'tsm-title-input'
        ),
        'editor'    =>  array(
            'type'          =>  'editor',                   // Using wp editor
            'val'           =>  ''
        ),
        'addition_class'    =>  array(
            'title'         =>  __( 'Additional Class', 'tsm' ),
            'placeholder'   =>  __( 'Additional Class', 'tsm' ),
            'type'          =>  'no_special_char',          // No special char input
            'val'           =>  ''
        )
    ),
    'short_code'            =>  'tsm_text_html'
);

$tsm_elems_map = array(
    'link'      =>  $tsm_link,
    'editor'    =>  $tsm_editor
);

//tsm_custom_object_links();

/**
 *  Build link elements of custom objects
 *  @since 1.0
 **/
function tsm_custom_object_links(){
    global $tsm_elems_map;
    
    $args_builtin = array(
        'public'    =>  true,
        '_builtin'  =>  true
    );
    
    $args_not_builtin = array(
        'public'    =>  true,
        '_builtin'  =>  false
    );
    
    $output = 'objects'; // names or objects
    
    // Builin posttypes
    $post_types_builtin = get_post_types( $args_builtin, $output );
    
    foreach ( $post_types_builtin as $post_type ):
        
        if ( $post_type != 'attachment' ):
            
            $posttypes_select_opts = array();
            $args = array(
                'post_type' => $post_type->name,
                'post_status' => 'publish'
            );
            
            $posttypes_query = new WP_Query( $args );
            if( $posttypes_query->have_posts() ):
                while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); 
                    
                    $posttypes_select_opts[get_the_ID()] = htmlentities2( get_the_title() );
                
                endwhile;
                
            endif;
            wp_reset_query();  // Restore global post data stomped by the_post().
            
            if (  !empty( $posttypes_select_opts ) ):
            
                $custom_posttype_elem = array(
                    'name'      =>  $post_type->label,
                    'desc'      =>  $post_type->label . ' links',
                    'fields'    =>  array(
                        'posttype_id'  =>  array(
                            'title'         =>  __( 'Choose', 'tsm' ) . ' ' . $post_type->label,
                            'type'          =>  'select',                
                            'options'       =>  $posttypes_select_opts,
                            'placeholder'   =>  __( 'Choose', 'tsm' ) . ' ' . $post_type->label,
                            'val'           =>  '',                         // Value when loaded
                            'default'       =>  current( array_keys( $posttypes_select_opts ) ) // Display default value if "val" is empty
                        ),
                        'posttype'   =>  array(
                            'title'     =>  '',
                            'type'      =>  'input_hidden',
                            'val'       =>  $post_type->name,
                            'default'   =>  $post_type->name
                        ),
                        'text'  =>  array(
                            'title'         =>  __( 'Text', 'tsm' ),
                            'type'          =>  'input_text',
                            'placeholder'   =>  __( 'Text display', 'tsm' ),
                            'default'       =>  array_shift( $posttypes_select_opts ),
                            'val'           =>  '',
                            'title_preview' =>  'yes'
                        ),
                        'img'   =>  array(
                            'title'         =>  __( 'Image', 'tsm' ),
                            'type'          =>  'img_upload',               // Using WP Media Upload
                            'desc'          =>  '',                         // Description
                            'val'           =>  ''                          // Image id (attachment id)
                        ),
                        'target'            =>  array(
                            'title'         =>  __( 'Target', 'tsm' ),
                            'type'          =>  'select',
                            'options'       =>  array(
                                '_blank'        =>  __( '_blank', 'tsm' ),  // Key is select value
                                '_parent'       =>  __( '_parent', 'tsm' ),
                                '_self'         =>  __( '_self', 'tsm' ),
                                '_top'          =>  __( '_top', 'tsm' )
                            ),
                            'val'           =>  '_blank',
                            'default'       =>  '_blank'
                        ),
                        'addition_class'    =>  array(
                            'title'         =>  __( 'Additional Class', 'tsm' ),
                            'placeholder'   =>  __( 'Additional Class', 'tsm' ),
                            'type'          =>  'no_special_char',          // No special char input
                            'val'           =>  ''
                        )             
                    ),
                    'short_code'            =>  'tsm_posttype_link_img'  // Short code function to display this element
                );
                
                $post_type_link_key = $post_type->name . '_link';
                $tsm_elems_map[$post_type_link_key] = $custom_posttype_elem;
                
            endif;
        
        endif;
        
    endforeach;
    
    
    // Posttypes not builin
    $post_types_not_builtin = get_post_types( $args_not_builtin, $output, 'and' );
    
    if ( $post_types_not_builtin ):
        
        foreach ( $post_types_not_builtin as $post_type ):
            
            if ( $post_type != 'attachment' ):
                
                $posttypes_select_opts = array();
                $args = array(
                    'post_type' => $post_type->name,
                    'post_status' => 'publish'
                );
                
                $posttypes_query = new WP_Query( $args );
                if( $posttypes_query->have_posts() ):
                
                    while ( $posttypes_query->have_posts() ) : $posttypes_query->the_post(); 
                        
                        $posttypes_select_opts[get_the_ID()] = htmlentities2( get_the_title() );
                    
                    endwhile;
                    
                endif;
                wp_reset_query();  // Restore global post data stomped by the_post().
                
                if (  !empty( $posttypes_select_opts ) ):
                
                    $custom_posttype_elem = array(
                        'name'      =>  $post_type->label,
                        'desc'      =>  $post_type->label . ' links',
                        'fields'    =>  array(
                            'posttype_id'  =>  array(
                                'title'         =>  __( 'Choose', 'tsm' ) . ' ' . $post_type->label,
                                'type'          =>  'select',                
                                'options'       =>  $posttypes_select_opts,
                                'placeholder'   =>  __( 'Choose', 'tsm' ) . ' ' . $post_type->label,
                                'val'           =>  '',                         // Value when loaded
                                'default'       =>  current( array_keys( $posttypes_select_opts ) ) // Display default value if "val" is empty
                            ),
                            'posttype'   =>  array(
                                'title'     =>  '',
                                'type'      =>  'input_hidden',
                                'val'       =>  $post_type->name,
                                'default'   =>  $post_type->name
                            ),
                            'text'  =>  array(
                                'title'         =>  __( 'Text', 'tsm' ),
                                'type'          =>  'input_text',
                                'placeholder'   =>  __( 'Text display', 'tsm' ),
                                'default'       =>  array_shift( $posttypes_select_opts ),
                                'val'           =>  '',
                                'title_preview' =>  'yes'
                            ),
                            'img'   =>  array(
                                'title'         =>  __( 'Image', 'tsm' ),
                                'type'          =>  'img_upload',               // Using WP Media Upload
                                'desc'          =>  '',                         // Description
                                'val'           =>  ''                          // Image id (attachment id)
                            ),
                            'target'            =>  array(
                                'title'         =>  __( 'Target', 'tsm' ),
                                'type'          =>  'select',
                                'options'       =>  array(
                                    '_blank'        =>  __( '_blank', 'tsm' ),  // Key is select value
                                    '_parent'       =>  __( '_parent', 'tsm' ),
                                    '_self'         =>  __( '_self', 'tsm' ),
                                    '_top'          =>  __( '_top', 'tsm' )
                                ),
                                'val'           =>  '_blank',
                                'default'       =>  '_blank'
                            ),
                            'addition_class'    =>  array(
                                'title'         =>  __( 'Additional Class', 'tsm' ),
                                'placeholder'   =>  __( 'Additional Class', 'tsm' ),
                                'type'          =>  'no_special_char',          // No special char input
                                'val'           =>  ''
                            )             
                        ),
                        'short_code'            =>  'tsm_posttype_link_img'  // Short code function to display this element
                    );
                    
                    $post_type_link_key = $post_type->name . '_link';
                    $tsm_elems_map[$post_type_link_key] = $custom_posttype_elem;
                    
                endif;
            
            endif;
            
        endforeach;
        
    endif; // End if ( $post_types_not_builtin )
    
    // Tax builin
    $taxonomies_builtin = get_taxonomies( $args_builtin, $output ); 
    
    foreach ( $taxonomies_builtin as $taxonomy ):
        
        $terms_select_opts = array();
        $taxonomies = array( $taxonomy->name );
        
        $args = array(
            'hide_empty'    =>  false
        );
        $terms = get_terms( $taxonomies, $args );
        
        if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):
            
            /*
            foreach ( $terms as $term ):
                
                $terms_select_opts[$term->term_id] = htmlentities2( $term->name );
                
            endforeach;
            */
            
            tsm_custom_taxonomy_opt_walker( $terms_select_opts, $taxonomies );
            
            if ( !empty( $terms_select_opts ) ):
                
                $custom_tax_elem = array(
                    'name'      =>  $taxonomy->label,
                    'desc'      =>  $taxonomy->label . ' links',
                    'fields'    =>  array(
                        'term_id'  =>  array(
                            'title'         =>  __( 'Choose', 'tsm' ) . ' ' . $taxonomy->label,
                            'type'          =>  'select',                
                            'options'       =>  $terms_select_opts,
                            'placeholder'   =>  __( 'Choose', 'tsm' ) . ' ' . $taxonomy->label,
                            'val'           =>  '',                         // Value when loaded
                            'default'       =>  current( array_keys( $terms_select_opts ) ) // Display default value if "val" is empty
                        ),
                        'tax'   =>  array(
                            'title'     =>  '',
                            'type'      =>  'input_hidden',
                            'val'       =>  $taxonomy->name,
                            'default'   =>  $taxonomy->name
                        ),
                        'text'  =>  array(
                            'title'         =>  __( 'Text', 'tsm' ),
                            'type'          =>  'input_text',
                            'placeholder'   =>  __( 'Text display', 'tsm' ),
                            'default'       =>  array_shift( $terms_select_opts ),
                            'val'           =>  '',
                            'title_preview' =>  'yes'
                        ),
                        'img'   =>  array(
                            'title'         =>  __( 'Image', 'tsm' ),
                            'type'          =>  'img_upload',               // Using WP Media Upload
                            'desc'          =>  '',                         // Description
                            'val'           =>  ''                          // Image id (attachment id)
                        ),
                        'target'            =>  array(
                            'title'         =>  __( 'Target', 'tsm' ),
                            'type'          =>  'select',
                            'options'       =>  array(
                                '_blank'        =>  __( '_blank', 'tsm' ),  // Key is select value
                                '_parent'       =>  __( '_parent', 'tsm' ),
                                '_self'         =>  __( '_self', 'tsm' ),
                                '_top'          =>  __( '_top', 'tsm' )
                            ),
                            'val'           =>  '_blank',
                            'default'       =>  '_blank'
                        ),
                        'addition_class'    =>  array(
                            'title'         =>  __( 'Additional Class', 'tsm' ),
                            'placeholder'   =>  __( 'Additional Class', 'tsm' ),
                            'type'          =>  'no_special_char',          // No special char input
                            'val'           =>  ''
                        )             
                    ),
                    'short_code'            =>  'tsm_term_link_img'  // Short code function to display this element
                );
                
                $tax_link_key = $taxonomy->name . '_link';
                $tsm_elems_map[$tax_link_key] = $custom_tax_elem;
                
            endif;
        
        endif;
                
    endforeach;
    
    // Tax not builin
    $taxonomies_not_builtin = get_taxonomies( $args_not_builtin, $output, 'and' ); 
    
    if ( $taxonomies_not_builtin ):
        
        foreach ( $taxonomies_not_builtin as $taxonomy ):
            
            $terms_select_opts = array();
            $taxonomies = array( $taxonomy->name );
            
            $args = array(
                'hide_empty'    =>  false
            );
            $terms = get_terms( $taxonomies, $args );
            
            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):
                
                /*
                foreach ( $terms as $term ):
                    
                    $terms_select_opts[$term->term_id] = htmlentities2( $term->name );
                    
                endforeach;
                */
                
                tsm_custom_taxonomy_opt_walker( $terms_select_opts, $taxonomies );
                
                if ( !empty( $terms_select_opts ) ):
                    
                    $custom_tax_elem = array(
                        'name'      =>  $taxonomy->label,
                        'desc'      =>  $taxonomy->label . ' links',
                        'fields'    =>  array(
                            'term_id'  =>  array(
                                'title'         =>  __( 'Choose', 'tsm' ) . ' ' . $taxonomy->label,
                                'type'          =>  'select',                
                                'options'       =>  $terms_select_opts,
                                'placeholder'   =>  __( 'Choose', 'tsm' ) . ' ' . $taxonomy->label,
                                'val'           =>  '',                         // Value when loaded
                                'default'       =>  current( array_keys( $terms_select_opts ) ) // Display default value if "val" is empty
                            ),
                            'tax'   =>  array(
                                'title'     =>  '',
                                'type'      =>  'input_hidden',
                                'val'       =>  $taxonomy->name,
                                'default'   =>  $taxonomy->name
                            ),   
                            'text'  =>  array(
                                'title'         =>  __( 'Text', 'tsm' ),
                                'type'          =>  'input_text',
                                'placeholder'   =>  __( 'Text display', 'tsm' ),
                                'default'       =>  array_shift( $terms_select_opts ),
                                'val'           =>  '',
                                'title_preview' =>  'yes'
                            ),
                            'img'   =>  array(
                                'title'         =>  __( 'Image', 'tsm' ),
                                'type'          =>  'img_upload',               // Using WP Media Upload
                                'desc'          =>  '',                         // Description
                                'val'           =>  ''                          // Image id (attachment id)
                            ),
                            'target'            =>  array(
                                'title'         =>  __( 'Target', 'tsm' ),
                                'type'          =>  'select',
                                'options'       =>  array(
                                    '_blank'        =>  __( '_blank', 'tsm' ),  // Key is select value
                                    '_parent'       =>  __( '_parent', 'tsm' ),
                                    '_self'         =>  __( '_self', 'tsm' ),
                                    '_top'          =>  __( '_top', 'tsm' )
                                ),
                                'val'           =>  '_blank',
                                'default'       =>  '_blank'
                            ),
                            'addition_class'    =>  array(
                                'title'         =>  __( 'Additional Class', 'tsm' ),
                                'placeholder'   =>  __( 'Additional Class', 'tsm' ),
                                'type'          =>  'no_special_char',          // No special char input
                                'val'           =>  ''
                            )             
                        ),
                        'short_code'            =>  'tsm_term_link_img'  // Short code function to display this element
                    );
                    
                    $tax_link_key = $taxonomy->name . '_link';
                    $tsm_elems_map[$tax_link_key] = $custom_tax_elem;
                    
                endif;
            
            endif;
                    
        endforeach;
        
    endif;
    
}
add_action( 'admin_init', 'tsm_custom_object_links' );
