<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}



global $tsm_theme_list, $tsm_style_params_default;

$default_styles = array(
    'bamboo',
    'blue',
    'cedar',
    'coffee',
    'emberglow',
    'green',
    'honeysuckle',
    'nougat',
    'orange',
    'phlox',
    'pink',
    'purple',
    'quarry',
    'red',
    'teal',
    'yellow'
);

// Default params array
/*
 *   $a_default_param_exam  =>  array(
        'param_name'    =>  'mainBgColor',
        'display_name'  =>  __( 'Main Background Color', 'tsm' ),   
        'type'          =>  'color',        // Color picker
        'default'       =>  '#354A5F',      // First Color of blue.less
        'desc'          =>  ''              // Description
    )
    $a_default_param_exam  =>  array(
        'param_name'    =>  '',
        'display_name'  =>  __( '', 'tsm' ),   
        'type'          =>  '',        // Color picker
        'default'       =>  '',      // First Color of blue.less
        'desc'          =>  ''              // Description
    )
 */
 
 
/** DARK DEFAULT =========================== **/
 
$tsm_dark_default_params = array(
    'name'              =>  'dark-default',
    'display_name'      =>  __( 'Dark Default', 'tsm' ),
    'params'            =>  array(      // Vars in _params.less
        'main_bg_color'     =>  array(
            'param_name'    =>  'mainBgColor',
            'display_name'  =>  __( 'Main Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#354A5F',      // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'main_color'        =>  array(
            'param_name'    =>  'mainColor',
            'display_name'  =>  __( 'Main Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#fff',         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'main_item_hover_bg_color'  =>  array(
            'param_name'    =>  'mainItemHoverBgColor',
            'display_name'  =>  __( 'Main Item Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'drop_bg_color'     =>  array(
            'param_name'    =>  'dropBgColor',
            'display_name'  =>  __( 'Drop Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#354A5F',      // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'drop_color'        =>  array(
            'param_name'    =>  'dropColor',
            'display_name'  =>  __( 'Drop Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#fff',         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'drop_item_hover_bg_color'  =>  array(
            'param_name'    =>  'dropItemHoverBgColor',
            'display_name'  =>  __( 'Drop Item Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        /*'grid_bg_color'     =>  array(
            'param_name'    =>  'gridBgColor',
            'display_name'  =>  __( 'Grid Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#283847',      // Fourth Color of blue.less
            'base_color_key'=>  'fourth',
            'desc'          =>  ''              // Description
        ),*/
        /*'grid_color'        =>  array(
            'param_name'    =>  'gridColor',
            'display_name'  =>  __( 'Grid Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),*/
        'titles_color'      =>  array(
            'param_name'    =>  'titlesColor',
            'display_name'  =>  __( 'Titles Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        /*'links_hover_color' =>  array(
            'param_name'    =>  'linksHoverColor',
            'display_name'  =>  __( 'Links Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'input_bg_color'    =>  array(
            'param_name'    =>  'inputBgColor',
            'display_name'  =>  __( 'Input Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#283847',      // Fourth Color of blue.less
            'base_color_key'=>  'fourth',
            'desc'          =>  ''              // Description
        ),
        'input_color'       =>  array(
            'param_name'    =>  'inputColor',
            'display_name'  =>  __( 'Input Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#fff',         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'button_bg_color'   =>  array(
            'param_name'    =>  'buttonBgColor',
            'display_name'  =>  __( 'Button Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'button_color'      =>  array(
            'param_name'    =>  'buttonColor',
            'display_name'  =>  __( 'Button Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#fff',         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'search_hover_bg_color' =>  array(
            'param_name'    =>  'searchHoverBgColor',
            'display_name'  =>  __( 'Search Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#3598DC',      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'search_color'      =>  array(
            'param_name'    =>  'searchColor',
            'display_name'  =>  __( 'Search Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '#fff',         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'arrows_color'      =>  array(
            'param_name'    =>  'arrowsColor',
            'display_name'  =>  __( 'Arrows Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  '0',            // Default 0 mean none color (inherit)
            'desc'          =>  ''              // Description
        ),*/
        'main_item_height'  =>  array(
            'param_name'    =>  'mainItemHeight',
            'display_name'  =>  __( 'Main Item Height', 'tsm' ),   
            'type'          =>  'input_number', // Acept number only
            'default'       =>  50,              // In pixel
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Main item height in pixel', 'tsm' )    // Description
        ),
        'drop_item_height'  =>  array(
            'param_name'    =>  'dropItemHeight',
            'display_name'  =>  __( 'Drop Item Height', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  50,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Drop item height in pixel', 'tsm' )
        ),
        'padding'           =>  array(
            'param_name'    =>  'padding',
            'display_name'  =>  __( 'Padding', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  15,  
            'min'           =>  0,
            'max'           =>  '',    
            'unit'          =>  'px',
            'desc'          =>  __( 'In pixel', 'tsm' )
        ),
        'main_item_padding' =>  array(
            'param_name'    =>  'mainItemPadding',
            'display_name'  =>  __( 'Main Item Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols per row
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 20 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'drop_item_padding' =>  array(
            'param_name'    =>  'dropItemPadding',
            'display_name'  =>  __( 'Drop Item Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 20 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'grid_padding'      =>  array(
            'param_name'    =>  'gridPadding',
            'display_name'  =>  __( 'Grid Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 20 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'content_padding'   =>  array(
            'param_name'    =>  'contentPadding',
            'display_name'  =>  __( 'Content Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'input_padding'     =>  array(
            'param_name'    =>  'inputPadding',
            'display_name'  =>  __( 'Input Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'button_padding'    =>  array(
            'param_name'    =>  'buttonPadding',
            'display_name'  =>  __( 'Button Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'links_padding'     =>  array(
            'param_name'    =>  'linksPadding',
            'display_name'  =>  __( 'Link Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'titles_padding'    =>  array(
            'param_name'    =>  'titlesPadding',
            'display_name'  =>  __( 'Titles Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'search_padding'    =>  array(
            'param_name'    =>  'searchPadding',
            'display_name'  =>  __( 'Search Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 15 + 12.5 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'images_margin'      =>  array(
            'param_name'    =>  'imagesMargin',
            'display_name'  =>  __( 'Images Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'video_margin'      =>  array(
            'param_name'    =>  'videoMargin',
            'display_name'  =>  __( 'Video Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'drop_first_margin' =>  array(
            'param_name'    =>  'dropFirstMargin',
            'display_name'  =>  __( 'Drop First Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'drop_margin'       =>  array(
            'param_name'    =>  'dropMargin',
            'display_name'  =>  __( 'Drop Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'grid_radius'        =>  array(
            'param_name'    =>  'gridRadius',
            'display_name'  =>  __( 'Grid Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),    // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 5, 5, 5, 5 ),    // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-left, top-right, bottom-left, bottom-right in pixel', 'tsm' )   // Description
        )
    ),     
    'additional_less'   =>  '',         // Less code for styling. This code will be insert between ( @import "../../components.less"; AND @import "../../functions.less";)                 
    'dir'               =>  TSM_DIR . '/inc/less/themes/ts-megamenu-dark-default',         // Path to folder of _params.less file
    'default_style'     =>  $default_styles
);

/** END - DARK DEFAULT =========================== **/


/** DEFAULT =========================== **/
 
$tsm_default_theme_color = array(
    'first'     =>  '#354A5F',
    'second'    =>  '#3598DC',
    'third'     =>  '#FFF',
    'fourth'    =>  '#EAEAEA'
);
$tsm_default_params = array(
    'name'              =>  'default',
    'display_name'      =>  __( 'Default', 'tsm' ),
    'params'            =>  array(      // Vars in _params.less
        'main_bg_color'     =>  array(
            'param_name'    =>  'mainBgColor',
            'display_name'  =>  __( 'Main Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['first'],  // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'main_color'        =>  array(
            'param_name'    =>  'mainColor',
            'display_name'  =>  __( 'Main Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'main_item_hover_bg_color'  =>  array(
            'param_name'    =>  'mainItemHoverBgColor',
            'display_name'  =>  __( 'Main Item Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['second'], // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'drop_bg_color'     =>  array(
            'param_name'    =>  'dropBgColor',
            'display_name'  =>  __( 'Drop Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['third'],  // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'drop_color'        =>  array(
            'param_name'    =>  'dropColor',
            'display_name'  =>  __( 'Drop Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['first'],  // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'drop_item_hover_bg_color'  =>  array(
            'param_name'    =>  'dropItemHoverBgColor',
            'display_name'  =>  __( 'Drop Item Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'drop_item_hover_color'  =>  array(
            'param_name'    =>  'dropItemHoverColor',
            'display_name'  =>  __( 'Drop Item Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['third'],      // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        /*'grid_bg_color'     =>  array(
            'param_name'    =>  'gridBgColor',
            'display_name'  =>  __( 'Grid Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['fourth'],     // Fourth Color of blue.less
            'base_color_key'=>  'fourth',
            'desc'          =>  ''              // Description
        ),
        'grid_color'        =>  array(
            'param_name'    =>  'gridColor',
            'display_name'  =>  __( 'Grid Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['first'],      // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),*/
        'titles_color'      =>  array(
            'param_name'    =>  'titlesColor',
            'display_name'  =>  __( 'Titles Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        /*'links_hover_color' =>  array(
            'param_name'    =>  'linksHoverColor',
            'display_name'  =>  __( 'Links Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'input_bg_color'    =>  array(
            'param_name'    =>  'inputBgColor',
            'display_name'  =>  __( 'Input Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['fourth'],      // Fourth Color of blue.less
            'base_color_key'=>  'fourth',
            'desc'          =>  ''              // Description
        ),
        'input_color'       =>  array(
            'param_name'    =>  'inputColor',
            'display_name'  =>  __( 'Input Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['first'],         // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'button_bg_color'   =>  array(
            'param_name'    =>  'buttonBgColor',
            'display_name'  =>  __( 'Button Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'button_color'      =>  array(
            'param_name'    =>  'buttonColor',
            'display_name'  =>  __( 'Button Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'search_hover_bg_color' =>  array(
            'param_name'    =>  'searchHoverBgColor',
            'display_name'  =>  __( 'Search Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'search_color'      =>  array(
            'param_name'    =>  'searchColor',
            'display_name'  =>  __( 'Search Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_default_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),*/
        'main_item_height'  =>  array(
            'param_name'    =>  'mainItemHeight',
            'display_name'  =>  __( 'Main Item Height', 'tsm' ),   
            'type'          =>  'input_number', // Acept number only
            'default'       =>  50,              // In pixel
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Main item height in pixel', 'tsm' )    // Description
        ),
        'drop_item_height'  =>  array(
            'param_name'    =>  'dropItemHeight',
            'display_name'  =>  __( 'Drop Item Height', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  50,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Drop item height in pixel', 'tsm' )
        ),
        'padding'           =>  array(
            'param_name'    =>  'padding',
            'display_name'  =>  __( 'Padding', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  15,  
            'min'           =>  0,
            'max'           =>  '',    
            'unit'          =>  'px',
            'desc'          =>  __( 'In pixel', 'tsm' )
        ),
        'main_item_padding' =>  array(
            'param_name'    =>  'mainItemPadding',
            'display_name'  =>  __( 'Main Item Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols per row
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 20 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'drop_item_padding' =>  array(
            'param_name'    =>  'dropItemPadding',
            'display_name'  =>  __( 'Drop Item Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 20 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'grid_padding'      =>  array(
            'param_name'    =>  'gridPadding',
            'display_name'  =>  __( 'Grid Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 20 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'content_padding'   =>  array(
            'param_name'    =>  'contentPadding',
            'display_name'  =>  __( 'Content Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'input_padding'     =>  array(
            'param_name'    =>  'inputPadding',
            'display_name'  =>  __( 'Input Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'button_padding'    =>  array(
            'param_name'    =>  'buttonPadding',
            'display_name'  =>  __( 'Button Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'links_padding'     =>  array(
            'param_name'    =>  'linksPadding',
            'display_name'  =>  __( 'Link Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'titles_padding'    =>  array(
            'param_name'    =>  'titlesPadding',
            'display_name'  =>  __( 'Titles Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'search_padding'    =>  array(
            'param_name'    =>  'searchPadding',
            'display_name'  =>  __( 'Search Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 15 + 12.5 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'images_margin'      =>  array(
            'param_name'    =>  'imagesMargin',
            'display_name'  =>  __( 'Images Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'video_margin'      =>  array(
            'param_name'    =>  'videoMargin',
            'display_name'  =>  __( 'Video Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'drop_first_margin' =>  array(
            'param_name'    =>  'dropFirstMargin',
            'display_name'  =>  __( 'Drop First Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'drop_margin'       =>  array(
            'param_name'    =>  'dropMargin',
            'display_name'  =>  __( 'Drop Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 15, 15, 15, 15 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'grid_radius'        =>  array(
            'param_name'    =>  'gridRadius',
            'display_name'  =>  __( 'Grid Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),    // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 5, 5, 5, 5 ),    // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-left, top-right, bottom-left, bottom-right in pixel', 'tsm' )   // Description
        )
    ),     
    'additional_less'   =>  '',         // Less code for styling. This code will be insert between ( @import "../../components.less"; AND @import "../../functions.less";)                 
    'dir'               =>  TSM_DIR . '/inc/less/themes/ts-megamenu-default',         // Path to folder of _params.less file
    'default_style'     =>  $default_styles
);

/** END - DEFAULT =========================== **/

/** IMPRESSIVE =========================== **/
 
$tsm_impressive_theme_color = array(
    'first'     =>  '#FFF',
    'second'    =>  '#EEEEEE',
    'third'     =>  '#555',
    'fourth'    =>  '#3598DC',
    'six'       =>  '#BBBBBB'
);
$tsm_impressive_params = array(
    'name'              =>  'impressive',
    'display_name'      =>  __( 'Impressive', 'tsm' ),
    'params'            =>  array(      // Vars in _params.less
        'main_bg_color'     =>  array(
            'param_name'    =>  'mainBgColor',
            'display_name'  =>  __( 'Main Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['first'],  // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'main_color'        =>  array(
            'param_name'    =>  'mainColor',
            'display_name'  =>  __( 'Main Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'main_item_font_weight'  =>  array(
            'param_name'    =>  'mainItemFontWeight',
            'display_name'  =>  __( 'Main Item Font Weight', 'tsm' ),   
            'type'          =>  'select',        
            'default'       =>  'bold',             
            'options'       =>  array(
                'normal'    =>  array(
                    'val'       =>  'normal',
                    'display'   =>  __( 'Normal', 'tsm' )
                ),
                'bold'          =>  array(
                    'val'       =>  'bold',
                    'display'   =>  __( 'Bold', 'tsm' )
                ),
                'bolder'    =>  array(
                    'val'       =>  'bolder',
                    'display'   =>  __( 'Bolder', 'tsm' )
                ),
                'lighter'    =>  array(
                    'val'       =>  'lighter',
                    'display'   =>  __( 'Lighter', 'tsm' )
                ),
                'initial'    =>  array(
                    'val'       =>  'initial',
                    'display'   =>  __( 'Initial', 'tsm' )
                ),
                'inherit'   =>  array(
                    'val'       =>  'inherit',
                    'display'   =>  __( 'Inherit', 'tsm' )
                )
            ),
            'unit'          =>  'px',
            'desc'          =>  ''
        ),
        'main_item_interval'  =>  array(
            'param_name'    =>  'mainItemInterval',
            'display_name'  =>  __( 'Main Item Interval', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  10,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Interval in pixel', 'tsm' )
        ),
        'main_padding'  =>  array(
            'param_name'    =>  'mainPadding',
            'display_name'  =>  __( 'Main Padding', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  10,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Main Padding in pixel', 'tsm' )
        ),
        'main_item_hover_bg_color'  =>  array(
            'param_name'    =>  'mainItemHoverBgColor',
            'display_name'  =>  __( 'Main Item Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['fourth'], // Fourth Color of blue.less
            'base_color_key'=>  'fourth',
            'desc'          =>  ''              // Description
        ),
        'main_item_hover_color'  =>  array(
            'param_name'    =>  'mainItemHoverColor',
            'display_name'  =>  __( 'Main Item Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['first'], // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'drop_bg_color'     =>  array(
            'param_name'    =>  'dropBgColor',
            'display_name'  =>  __( 'Drop Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['first'],  // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'drop_color'        =>  array(
            'param_name'    =>  'dropColor',
            'display_name'  =>  __( 'Drop Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],  // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'drop_item_interval'  =>  array(
            'param_name'    =>  'dropItemInterval',
            'display_name'  =>  __( 'Drop Item Interval', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  10,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Interval in pixel', 'tsm' )
        ),
        'drop_padding'  =>  array(
            'param_name'    =>  'dropPadding',
            'display_name'  =>  __( 'Drop Padding', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  10,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Drop Padding in pixel', 'tsm' )
        ),
        'drop_item_hover_bg_color'  =>  array(
            'param_name'    =>  'dropItemHoverBgColor',
            'display_name'  =>  __( 'Drop Item Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        /*'grid_bg_color'     =>  array(
            'param_name'    =>  'gridBgColor',
            'display_name'  =>  __( 'Grid Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['second'],     // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'grid_color'        =>  array(
            'param_name'    =>  'gridColor',
            'display_name'  =>  __( 'Grid Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],      // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),*/
        'titles_color'      =>  array(
            'param_name'    =>  'titlesColor',
            'display_name'  =>  __( 'Titles Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],      // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        /*'links_hover_color' =>  array(
            'param_name'    =>  'linksHoverColor',
            'display_name'  =>  __( 'Links Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],      // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'input_bg_color'    =>  array(
            'param_name'    =>  'inputBgColor',
            'display_name'  =>  __( 'Input Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['second'],      // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'input_color'       =>  array(
            'param_name'    =>  'inputColor',
            'display_name'  =>  __( 'Input Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'input_hover_box_dhadow'    =>  array(
            'param_name'    =>  'inputHoverBoxShadow',
            'display_name'  =>  __( 'Input Hover Box Shadow', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['second'],         // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ), 
        'button_radius' =>  array(
            'param_name'    =>  'buttonRadius',
            'display_name'  =>  __( 'Button Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 5, 5, 5, 5 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-left, top-right, bottom-left, bottom-right in pixel', 'tsm' )   // Description
        ),
        'button_bg_color'   =>  array(
            'param_name'    =>  'buttonBgColor',
            'display_name'  =>  __( 'Button Background Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['fourth'],      // Fourth Color of blue.less
            'base_color_key'=>  'fourth',
            'desc'          =>  ''              // Description
        ),
        'button_color'      =>  array(
            'param_name'    =>  'buttonColor',
            'display_name'  =>  __( 'Button Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'button_hover_color'      =>  array(
            'param_name'    =>  'buttonHoverColor',
            'display_name'  =>  __( 'Button Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['first'],         // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'search_color'      =>  array(
            'param_name'    =>  'searchColor',
            'display_name'  =>  __( 'Search Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['first'],         // First Color of blue.less
            'base_color_key'=>  'first',
            'desc'          =>  ''              // Description
        ),
        'search_hover_color'      =>  array(
            'param_name'    =>  'searchHoverColor',
            'display_name'  =>  __( 'Search Hover Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['third'],         // Third Color of blue.less
            'base_color_key'=>  'third',
            'desc'          =>  ''              // Description
        ),
        'search_hover_bg_color'      =>  array(
            'param_name'    =>  'searchHoverBgColor',
            'display_name'  =>  __( 'Search Hover Bg Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['second'],         // Second Color of blue.less
            'base_color_key'=>  'second',
            'desc'          =>  ''              // Description
        ),
        'arrows_color'      =>  array(
            'param_name'    =>  'arrowsColor',
            'display_name'  =>  __( 'Arrows Color', 'tsm' ),   
            'type'          =>  'color',        // Color picker
            'default'       =>  $tsm_impressive_theme_color['six'],         // Six Color of blue.less
            'base_color_key'=>  'six',
            'desc'          =>  ''              // Description
        ),*/
        'main_item_height'  =>  array(
            'param_name'    =>  'mainItemHeight',
            'display_name'  =>  __( 'Main Item Height', 'tsm' ),   
            'type'          =>  'input_number', // Acept number only
            'default'       =>  39,              // In pixel
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Main item height in pixel', 'tsm' )    // Description
        ),
        'drop_item_height'  =>  array(
            'param_name'    =>  'dropItemHeight',
            'display_name'  =>  __( 'Drop Item Height', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  39,             
            'min'           =>  0,
            'max'           =>  500,
            'unit'          =>  'px',
            'desc'          =>  __( 'Drop item height in pixel', 'tsm' )
        ),
        'padding'           =>  array(
            'param_name'    =>  'padding',
            'display_name'  =>  __( 'Padding', 'tsm' ),   
            'type'          =>  'input_number',        
            'default'       =>  9,  
            'min'           =>  0,
            'max'           =>  '',    
            'unit'          =>  'px',
            'desc'          =>  __( 'In pixel', 'tsm' )
        ),
        'main_item_padding' =>  array(
            'param_name'    =>  'mainItemPadding',
            'display_name'  =>  __( 'Main Item Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols per row
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 14 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'drop_item_padding' =>  array(
            'param_name'    =>  'dropItemPadding',
            'display_name'  =>  __( 'Drop Item Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 14 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'grid_padding'      =>  array(
            'param_name'    =>  'gridPadding',
            'display_name'  =>  __( 'Grid Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 14, 14, 14, 14 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'content_padding'   =>  array(
            'param_name'    =>  'contentPadding',
            'display_name'  =>  __( 'Content Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 9, 14 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'input_padding'     =>  array(
            'param_name'    =>  'inputPadding',
            'display_name'  =>  __( 'Input Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 12, 12, 12, 12 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'button_padding'    =>  array(
            'param_name'    =>  'buttonPadding',
            'display_name'  =>  __( 'Button Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 12, 12, 12, 12 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'links_padding'     =>  array(
            'param_name'    =>  'linksPadding',
            'display_name'  =>  __( 'Link Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 9, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'titles_padding'    =>  array(
            'param_name'    =>  'titlesPadding',
            'display_name'  =>  __( 'Titles Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 9, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'search_padding'    =>  array(
            'param_name'    =>  'searchPadding',
            'display_name'  =>  __( 'Search Padding', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 9 + 12.5 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'images_margin'      =>  array(
            'param_name'    =>  'imagesMargin',
            'display_name'  =>  __( 'Images Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 9, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),
        'video_margin'      =>  array(
            'param_name'    =>  'videoMargin',
            'display_name'  =>  __( 'Video Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 9, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),  
        'main_item_margin'      =>  array(
            'param_name'    =>  'mainItemMargin',
            'display_name'  =>  __( 'Main Item Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  2,          // Number of cols
            'min'           =>  array( 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 10, 5 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-bottom, left-right in pixel', 'tsm' )   // Description
        ),  
        'drop_item_margin'      =>  array(
            'param_name'    =>  'dropItemMargin',
            'display_name'  =>  __( 'Drop Item Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 10, 10, 10, 10 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),  
        'drop_first_margin' =>  array(
            'param_name'    =>  'dropFirstMargin',
            'display_name'  =>  __( 'Drop First Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 0, 0, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'drop_margin'       =>  array(
            'param_name'    =>  'dropMargin',
            'display_name'  =>  __( 'Drop Margin', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 0, 0, 0, 0 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'main_item_radius'       =>  array(
            'param_name'    =>  'mainItemRadius',
            'display_name'  =>  __( 'Main Item Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 3, 3, 3, 3 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'drop_item_raduis'       =>  array(
            'param_name'    =>  'dropItemRaduis',
            'display_name'  =>  __( 'Drop Item Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),  // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 3, 3, 3, 3 ), // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top, right, bottom, left in pixel', 'tsm' )   // Description
        ),
        'grid_radius'        =>  array(
            'param_name'    =>  'gridRadius',
            'display_name'  =>  __( 'Grid Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),    // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 5, 5, 5, 5 ),    // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-left, top-right, bottom-left, bottom-right in pixel', 'tsm' )   // Description
        ),
        'links_radius'        =>  array(
            'param_name'    =>  'linksRadius',
            'display_name'  =>  __( 'Links Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),    // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 5, 5, 5, 5 ),    // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-left, top-right, bottom-left, bottom-right in pixel', 'tsm' )   // Description
        ),
        'input_radius'        =>  array(
            'param_name'    =>  'inputRadius',
            'display_name'  =>  __( 'Input Radius', 'tsm' ),   
            'type'          =>  'input_number_table',  
            'cols_num'      =>  4,          // Number of cols
            'min'           =>  array( 0, 0, 0, 0 ),    // Max number of items must equal to cols_num
            'max'           =>  array(),
            'default'       =>  array( 5, 5, 5, 5 ),    // Max number of items must equal to cols_num
            'unit'          =>  'px',         
            'desc'          =>  __( 'top-left, top-right, bottom-left, bottom-right in pixel', 'tsm' )   // Description
        )
    ),     
    'additional_less'   =>  '',         // Less code for styling. This code will be insert between ( @import "../../components.less"; AND @import "../../functions.less";)                 
    'dir'               =>  TSM_DIR . '/inc/less/themes/ts-megamenu-impressive',         // Path to folder of _params.less file
    'default_style'     =>  $default_styles
);

/** END - IMPRESSIVE =========================== **/


$tsm_theme_list = array(
    'dark_default'  =>  $tsm_dark_default_params,
    'default'       =>  $tsm_default_params,
    'impressive'    =>  $tsm_impressive_params
);




