<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


global $tsmAdminMenus;

$tsmAdminMenus = array(
    
    array(
        'page_title'        =>  '<i class="fa fa-cog"></i>' . __( 'Mega Menu Options', 'tsm' ),
        'menu_title'        =>  __( 'TSM Options', 'tsm' ),
        'cap'               =>  'manage_options',
        'slug'              =>  'tsm-options',
        'function'          =>  'tsm_admin_menus_page',
        'parent_slug'       =>  '',
        'icon'              =>  'dashicons-welcome-widgets-menus'
    )
    
);


function tsm_manager_menu(){
    global $tsmAdminMenus, $current_user;
   
    foreach( $tsmAdminMenus as $menu ):
        
        if( $menu['parent_slug'] == '' or empty( $menu['parent_slug'] ) ):
            
            $icon = ( isset( $menu['icon'] ) ) ? $menu['icon'] : 'dashicons-star-filled';
            
            add_menu_page( $menu['page_title'], $menu['menu_title'], $menu['cap'], $menu['slug'], $menu['function'], $icon );
             
        else:
        
            add_submenu_page( $menu['parent_slug'], $menu['page_title'] , $menu['menu_title'], $menu['cap'], $menu['slug'], $menu['function'] );
            
        endif;
        
    endforeach;
}
add_action('admin_menu', 'tsm_manager_menu');

function tsm_admin_menus_page(){
    
    if ( isset( $_REQUEST['page'] ) ):
    
        $page = $_REQUEST['page'];
        $file = TSM_DIR . '/inc/admin-pages/' . $page . '.php';

         if ( file_exists( $file ) ): 
         
            include_once $file;
            
         endif;
         
    endif;
}



