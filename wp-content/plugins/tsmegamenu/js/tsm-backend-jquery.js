jQuery(document).ready(function($){
    
    "use strict";
    
    tsm_script_localize.html['edit_li_form'] = $('#tsm-dropdown-li-form').html();
    
    var tsm_live_css_editor_control_update_count = 0;
    var tsm_invisible_editor_obj;
    if ( typeof tinyMCEPreInit !== "undefined" ) {
        tsm_invisible_editor_obj = tinyMCEPreInit.mceInit['tsm-invisible-editor'];   
    }
    
    $('body').magnificPopup({
        delegate: 'a.tsm-show-mega-form',
        removalDelay: 0, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function() {
                //this.st.mainClass = this.st.el.attr('data-effect');
                //$(this.st.el.attr('href')).find('.travel-booktour-title, .travel-askquestions-title').text(this.st.el.attr('data-tour-title'));
                ts_init_slider_range();
            },
            open: function() {
                // For fix sortable (I don't know why this issue happen with chrome)
                $('html, body').addClass('tsm-overflow-visible');
            },
            beforeClose: function() {
                // Save dropdown edit
                tsm_update_current_editing_li();
                
                // Remove dropdown edit form
                $('.tsm-megamenu-form-dropdown .tsm-edit-dropdown-list-li-form').remove();
                
                // Close all editing element
                $('.tsm-megamenu-form .tsm-edit-elem-form').html('').css({'display': 'none'});
                // Show grid
                $('.tsm-megamenu-form .tsm-grid').css({'display': 'block'});
                $('.tsm-megamenu-form .tsm-actions-wrap').css({'display': 'block'});
            },
            afterClose: function() {
                $('html, body').removeClass('tsm-overflow-visible');
            }
        },
        midClick: true  //allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    }); 
    
    // Close mega menu form
    $(document).on('click', '.tsm-megamenu-form-wrap .tsm-close-megamenu-form-btn', function(e){
        $.magnificPopup.close(); // Close popup that is currently opened (shorthand)
        e.preventDefault();
    });
    
    // Close mega menu form and save
    $(document).on('click', '.tsm-megamenu-form-wrap .tsm-close-and-save-mega-menu', function(e){
        $.magnificPopup.close(); // Close popup that is currently opened (shorthand)
        tsm_save_mega_menu();
        e.preventDefault();
    });
    
    $('.tsm-suported-elems-tabs').tabs();
    
    // Pagination
    $('.tsm-pagination li a').bind('click', function(e){
        
        var $this = $(this);
        var $thisLi = $this.closest('li');
        
        if ( !$thisLi.hasClass('tsm-cur-page') ){
            var $thisPagination = $this.closest('.tsm-pagination');
            var $thisTabContent = $thisPagination.closest('.tsm-tab-content');
            var thisPage = $this.attr('data-page');
            $thisPagination.find('li').removeClass('tsm-cur-page active');  
            $thisLi.addClass('tsm-cur-page active'); 
            $thisTabContent.find('.tsm-row').removeClass('tsm-cur-page-view').addClass('tsm-hidden');
            $thisTabContent.find('.tsm-row-' + thisPage).addClass('tsm-cur-page-view').removeClass('tsm-hidden');
        }
        
        e.preventDefault(); 
    });
    
    // Add mega menu row
    $(document).on('click', '.tsm-add-row', function(e){
        
        var $form = $(this).closest('.tsm-megamenu-form');
        var row_html = $('.tsm-row-col-form .tsm-row-form').html();
        
        //$form.find('.tsm-grid').append(row_html);
        
        if ( $form.find('.tsm-grid .tsm-row').length ){
            $form.find('.tsm-grid .tsm-row').last().after(row_html);
        }
        else{
            $form.find('.tsm-grid').prepend(row_html);
        }
        
        tsm_update_sortable();
        //tsm_update_clearfix();
        
        e.preventDefault(); 
    });
    
    // Add mega menu col
    $(document).on('click', '.tsm-grid .tsm-row .tsm-add-col', function(e){
        
        var $row = $(this).closest('.tsm-row');
        var col_html = $('.tsm-row-col-form .tsm-col-form').html();
        
        $row.find('.tsm-cols-holder').append(col_html);
        
        tsm_update_sortable();
        tsm_update_clearfix();
        
        e.preventDefault(); 
        
    });
    
    // Add mega menu col when click on the empty col holder
    $(document).on('click', '.tsm-grid .tsm-row .tsm-cols-holder', function(e){
        
        if ( $.trim($(this).html()) == '' ) {
            var $row = $(this).closest('.tsm-row');
            var col_html = $('.tsm-row-col-form .tsm-col-form').html();
            
            $row.find('.tsm-cols-holder').append(col_html);
            
            tsm_update_sortable();
            tsm_update_clearfix();
            
            e.preventDefault();    
        }
        
    });
    
    // Edit column custom class
    $(document).on('click', '.tsm-grid .tsm-col .tsm-edit-col-class', function(e){
        
        var thisCol = $(this).closest('.tsm-col');
        var cur_col_class = thisCol.attr('data-col-class');
        var new_col_class = prompt( tsm_script_localize.text.custom_col_class, cur_col_class );
        
        if ( new_col_class != null ) {
            thisCol.attr('data-col-class', new_col_class);
        }
        
        e.preventDefault(); 
    });
    
    $(document).on('hover', '.tsm-grid .tsm-col .tsm-edit-col-class', function(e){
        var thisCol = $(this).closest('.tsm-col');
        var cur_col_class = thisCol.attr('data-col-class');
        $(this).attr('title', cur_col_class);
        e.preventDefault(); 
    });
    
    // Show elems tabs when click add element
    $(document).on('click', '.tsm-grid .tsm-col .tsm-add-elem', function(e){
        
        var $this =  $(this);
        var thisForm = $this.closest('.tsm-megamenu-form');
        var thisCol = $this.closest('.tsm-col');
        var thisGrid = $this.closest('.tsm-grid');
        
        // Set this col is current col
        thisGrid.find('.tsm-row').removeClass('.tsm-cur-row').find('.tsm-col').removeClass('tsm-cur-col');
        thisCol.addClass('tsm-cur-col').closest('.tsm-row').addClass('tsm-cur-row');
        
        // Hidden Grid
        thisGrid.css({'display': 'none'});
        
        // Hidden edit form
        thisForm.find('.tsm-edit-elem-form').css({'display': 'none'});
        
        $('.tsm-suported-elems-tabs').tabs();
        
        // Show element tabs for chosen
        thisForm.find('.tsm-suported-elems-tabs').show('300');
        
        e.preventDefault();
    });
    
    // Show elems tabs when click on empty elems holder 
    $(document).on('click', '.tsm-grid .tsm-col .tsm-elems-holder', function(e){
        
        if ( $.trim($(this).html()) == '' ) {
            var $this =  $(this);
            var thisForm = $this.closest('.tsm-megamenu-form');
            var thisCol = $this.closest('.tsm-col');
            var thisGrid = $this.closest('.tsm-grid');
            
            // Set this col is current col
            thisGrid.find('.tsm-row').removeClass('.tsm-cur-row').find('.tsm-col').removeClass('tsm-cur-col');
            thisCol.addClass('tsm-cur-col').closest('.tsm-row').addClass('tsm-cur-row');
            
            // Hidden Grid
            thisGrid.css({'display': 'none'});
            
            // Hidden edit form
            thisForm.find('.tsm-edit-elem-form').css({'display': 'none'});
            
            $('.tsm-suported-elems-tabs').tabs();
            
            // Show element tabs for chosen
            thisForm.find('.tsm-suported-elems-tabs').show('300');
            
            e.preventDefault();    
        }
        
    });
    
    // Add element when click
    $(document).on('click', '.tsm-tab-content .tsm-elem', function(e){
        
        var $this = $(this);
        $this.addClass('tsm-chosen-elem');
        var thisForm = $this.closest('.tsm-megamenu-form');
        var curCol = thisForm.find('.tsm-grid .tsm-cur-row .tsm-cur-col');
        var thisElemKey = $this.attr('data-elem-key');
        var thisCloneHtml = '';
        var thisCloneHtml = $('<div>').append($this.clone()).html();
        
        curCol.find('.tsm-elems-holder').append(thisCloneHtml);
        $this.removeClass('tsm-chosen-elem');
        tsm_update_elem_index();
        tsm_update_sortable();
        
        // Hidden elems tabs
        thisForm.find('.tsm-suported-elems-tabs').css({'display': 'none'});
        
        // Hidden edit form
        thisForm.find('.tsm-edit-elem-form').css({'display': 'none'});
        
        // Show grid
        thisForm.find('.tsm-grid').slideDown('250');
        
        // Open edit elem form after added
        curCol.find('.tsm-elems-holder .tsm-chosen-elem .tsm-edit-elem').trigger('click');
        curCol.find('.tsm-elems-holder .tsm-chosen-elem').removeClass('tsm-chosen-elem');
        
        e.preventDefault();
    });
    
    // Cancel add elem
    $(document).on('click', '.tsm-megamenu-form .tsm-cancel-add-elem', function(e){
        
        var $this = $(this);
        var thisForm = $this.closest('.tsm-megamenu-form');
        
        // Hidden elems tabs
        thisForm.find('.tsm-suported-elems-tabs').css({'display': 'none'});
        
        // Hidden edit form
        thisForm.find('.tsm-edit-elem-form').css({'display': 'none'});
        
        // Show grid
        thisForm.find('.tsm-grid').slideDown('250');
        
        e.preventDefault();
        
    });
    
    // Remove Element
    $(document).on('click', '.tsm-grid .tsm-col .tsm-del-elem', function(e){
        
        var $this = $(this);
        var thisElem = $this.closest('.tsm-elem');
        var is_widget = thisElem.hasClass('tsm-widget');
        var c_msg = tsm_script_localize.confirm.del_elem;
        if ( is_widget ){
            c_msg = tsm_script_localize.confirm.del_widget;
        }
        
        if (tsm_is_locked(thisElem)){
            return false;
        }
        
        var c = confirm(c_msg); 
        
        if (!c){
            return false;
        }
        
        tsm_locked(thisElem);
        
        thisElem.hide(300, function(){
            tsm_unlock(thisElem);
            thisElem.remove();
        });
        
        e.preventDefault();
        
    }); // End remove elem
    
    // Smaller col
    $(document).on('click', '.tsm-grid .tsm-col .tsm-smaller-col', function(e){
        var $this = $(this);
        var thisCol = $this.closest('.tsm-col');
        var col_w = parseInt(thisCol.attr('data-col-width'));
        
        if (isNaN(col_w) || col_w < 0 || col_w > 12) {
            col_w = 4;
        }
        
        if ( col_w - 1 > 0 ) {
            var new_col_w = col_w - 1;
            thisCol.removeClass('c-' + col_w).attr('data-col-class', 'tsm-col c-' + new_col_w).attr('data-col-width', new_col_w).addClass('c-' + new_col_w);
        }
        
        tsm_update_clearfix();
        
        e.preventDefault(); 
    });
    
    // Bigger col
    $(document).on('click', '.tsm-grid .tsm-col .tsm-bigger-col', function(e){
        var $this = $(this);
        var thisCol = $this.closest('.tsm-col');
        var col_w = parseInt(thisCol.attr('data-col-width'));
        
        if (isNaN(col_w) || col_w < 0 || col_w > 12) {
            col_w = 4;
        }
        
        if ( col_w + 1 <= 12 ) {
            var new_col_w = col_w + 1;
            thisCol.removeClass('c-' + col_w).attr('data-col-class', 'tsm-col c-' + new_col_w).attr('data-col-width', new_col_w).addClass('c-' + new_col_w);
        }
        
        tsm_update_clearfix();
        
        e.preventDefault(); 
    });
    
    // Remove Column
    $(document).on('click', '.tsm-grid .tsm-col .tsm-del-col', function(e){
        
        var $this = $(this);
        var thisCol = $this.closest('.tsm-col');
        
        if (tsm_is_locked(thisCol)){
            return false;
        }
        
        var c = confirm(tsm_script_localize.confirm.del_col); 
        
        if (!c){
            return false;
        }
        
        tsm_locked(thisCol);
        
        thisCol.hide(300, function(){
            tsm_unlock(thisCol);
            thisCol.remove();
            tsm_update_clearfix();
        });
        
        e.preventDefault();
         
    }); // End remove col
    
    // Remove Row
    $(document).on('click', '.tsm-grid .tsm-row .tsm-del-row', function(e){
        
        var $this = $(this);
        var thisRow = $this.closest('.tsm-row');
        
        if (tsm_is_locked(thisRow)){
            return false;
        }
        
        var c = confirm(tsm_script_localize.confirm.del_row); 
        
        if (!c){
            return false;
        }
        
        tsm_locked(thisRow);
        
        thisRow.hide(300, function(){
            tsm_unlock(thisRow);
            thisRow.remove();
        });
        
        e.preventDefault();
         
    }); // End remove row
    
    // Show edit elem form when click edit elem
    $(document).on('click', '.tsm-grid .tsm-elem .tsm-edit-elem', function(e){
        
        var $this =  $(this);
        var thisForm = $this.closest('.tsm-megamenu-form');
        var thisElemForm = thisForm.find('.tsm-edit-elem-form');
        var thisCol = $this.closest('.tsm-col');
        var thisElem = $this.closest('.tsm-elem');
        var thisGrid = $this.closest('.tsm-grid');
        var widget_elem = (thisElem.hasClass('tsm-widget')) ? 'yes' : 'no';
        var data_elem; // It is instance if widget
        
        // Return if form is locked
        if ( tsm_is_locked(thisElemForm) ){
            return false;
        }
        
        // Set this col is current col, set current element
        thisGrid.find('.tsm-row').removeClass('.tsm-cur-row').find('.tsm-col').removeClass('tsm-cur-col');
        thisCol.addClass('tsm-cur-col').closest('.tsm-row').addClass('tsm-cur-row');
        thisGrid.find('.tsm-elem').removeClass('tsm-cur-elem');
        thisElem.addClass('tsm-cur-elem');
        
        // Hidden Grid
        thisGrid.css({'display': 'none'});
        
        // Hidden elems tabs
        thisForm.find('.tsm-suported-elems-tabs').css({'display': 'none'});
        
        // Hidden form actions
        thisForm.find('.tsm-actions-wrap').css({'display': 'none'});
        
        // Show edit elem form
        thisElemForm.show('300');
        
        // Element key
        var elem_key = '';
        
        if ( widget_elem == 'yes' ){
            elem_key = thisElem.attr('data-widget-key');
            data_elem = thisElem.attr('data-wg-instance');  
        }
        else{
            elem_key = thisElem.attr('data-elem-key');
            data_elem = thisElem.attr('data-elem-json');  // json string
            if ( data_elem != '' ){
                data_elem = JSON.parse(data_elem);
            }
            else{
                data_elem = {};
            }
        }
        
        var elem_index = parseInt(thisElem.attr('data-elem-index'));
        
        // Lock elem form
        tsm_lock_form(thisElemForm);      
        
        // Always check elem index
        while ( $('.tsm-grid .tsm-col .tsm-elem[data-elem-index=' + elem_index + ']').length > 1){
            elem_index++;
            thisElem.attr('data-elem-index', elem_index);
        }
        
        /*
        if ( typeof tinyMCE !== "undefined" ){
			tinyMCE.execCommand( "mceRemoveEditor", true, editor_id ); // mceRemoveEditor, mceAddEditor changed from TinyMCE v4.x
		} 
        */
        
        // Load elem form via ajax
        var data = {
            action          :   'tsm_load_elem_via_ajax',
            nonce           :   tsm_nonce.ajax_nonce,
            elem_key        :   elem_key,
            data_elem       :   data_elem,
            elem_index      :   elem_index,
            widget_elem     :   widget_elem             // "yes" or "no"
        }
        
        $.post(ajaxurl, data, function(response){
            if (thisElemForm.find('.tsm-elem-field-editor').length){
                if ( typeof tinyMCE !== "undefined" ){
                    
                    // Re-init all loaded editors
                    thisElemForm.find('.tsm-elem-field-editor').each(function(){
                        var editor_id = 'tsm-wp-editor-' + elem_index + '_' + field_num;
                        tinyMCE.execCommand( "mceRemoveEditor", true, editor_id );  // It's needed
                    });
    			} 
            }
                        
            thisElemForm.html(response);
            
            // If has editor in response (apply for element only - not widget)
            if (thisElemForm.find('.tsm-elem-field-editor').length){
                if ( typeof tinyMCE !== "undefined" ){
                    
                    // Re-init all loaded editors
                    thisElemForm.find('.tsm-elem-field-editor').each(function(){
                        var field_num = $(this).attr('data-field-num');
                        var editor_id = 'tsm-wp-editor-' + elem_index + '_' + field_num;
                        tinyMCE.execCommand( "mceRemoveEditor", true, editor_id );  // It's needed
                        tinyMCE.execCommand( "mceAddEditor", true, editor_id );
        				tsm_init_new_editor( editor_id );
                        switchEditors.go( editor_id, 'tmce' );
                    });
    			} 
            }
            tsm_update_chosen_elem(thisElemForm.find('.tsm-select2'));
            tsm_unlock_form(thisElemForm);
        });
        
        
        e.preventDefault(); 
    });
    
    // When choose custom post type or term
    $(document).on('change', '.tsm-edit-elem-form .tsm-field-type-select', function(e){
        
        var $this = $(this);
        var thisForm = $this.closest('.tsm-edit-elem-form');
        var thisText = $this.find('option:selected').text();
        thisText = thisText.replace(/^(-- )+/, '');
        thisForm.find('div[data-field-key="text"] .tsm-field-type-input_text').val(thisText);
        
        e.preventDefault(); 
    });
    
    // Done edit element
    $(document).on('click', '.tsm-edit-elem-form .tsm-done-edit-elem', function(e){
        
        var $this =  $(this);
        var thisForm = $this.closest('.tsm-megamenu-form');
        var thisElemForm = thisForm.find('.tsm-edit-elem-form');
        var thisGrid = thisForm.find('.tsm-grid');
        var curElem = thisForm.find('.tsm-cur-row .tsm-cur-col .tsm-cur-elem');
        var is_widget = curElem.hasClass('tsm-widget');
        var elem_index = thisElemForm.find('.tsm-elem-form-inner').attr('data-elem-index');
        var elem_key = '';
        
        // Return if form is locked
        if ( tsm_is_locked(thisElemForm) ){
            return false;
        }
        
        var err = tsm_validate_fields(thisElemForm);
        
        if ( err ) {
            return false;
        }
        
        if ( !is_widget ) {
            
            elem_key = curElem.attr('data-elem-key');
            
            var oldElemData = JSON.parse(curElem.attr('data-elem-json'));
            var elem_data = oldElemData;
            
            thisElemForm.find('.tsm-elem-field').each(function(){
                 var field_key = $(this).attr('data-field-key');
                 var field_type = $(this).attr('data-field-type');
                 var field_val = '';
                 var preview_txt = '';
                 var field_data_title_preview = $(this).attr('data-title-preivew');
                 var is_field_title_preview = false;
                 if ( typeof field_data_title_preview != 'undefined' && typeof field_data_title_preview !=  false ){
                    is_field_title_preview = field_data_title_preview == 'yes';
                 }
                 
                 if (field_type == 'editor') {
                    var field_num = $(this).attr('data-field-num');
                    var editor_id = 'tsm-wp-editor-' + elem_index + '_' + field_num;
                    field_val = tsm_fixed_encodeURIComponent( tinyMCE.get(editor_id).getContent() ); // Get editor content by id
                 } 
                 else{
                    field_val = tsm_fixed_encodeURIComponent( $(this).find('.tsm-field-type-' + field_type).val() );
                 }
                 
                 if ( elem_key == 'link' ){
                     var preview_img_src = '';
                     if (field_key == 'img' && field_val != '' && parseInt(field_val) != 0) {
                        preview_img_src = $(this).find('.tsm-img-thumb').attr('src');
                        if ( preview_img_src != '' ){
                            curElem.css({
                                'background' : 'url("' + preview_img_src + '") center center no-repeat transparent'
                            });
                         }  
                         else{
                            curElem.css({'background-image': 'none'});
                         }
                     }
                     
                     if (field_key == 'text') {
                        preview_txt = $(this).find('.tsm-field-type-' + field_type).val();
                        curElem.find('.tsm-preview-title').text(tsm_excerpts(preview_txt, 30, '...'));
                     }
                     
                 }
                 
                 if ( elem_key == 'editor' ) {
                    if (field_key == 'title') {
                        preview_txt = $(this).find('.tsm-field-type-' + field_type).val();
                        curElem.find('.tsm-preview-title').text(tsm_excerpts(preview_txt, 30, '...'));
                     }
                 }
                 
                 if ( elem_key != 'link' && elem_key != 'editor' ){
                    var preview_img_src = '';
                    if (field_key == 'img' && field_val != '' && parseInt(field_val) != 0) {
                        preview_img_src = $(this).find('.tsm-img-thumb').attr('src');
                        if ( preview_img_src != '' ){
                            curElem.css({
                                'background' : 'url("' + preview_img_src + '") center center no-repeat transparent'
                            });
                         }  
                         else{
                            curElem.css({'background-image': 'none'});
                         }
                    }
                    if ( $(this).find('.tsm-field-type-' + field_type).hasClass('tsm-text-input') ){
                        preview_txt = $(this).find('.tsm-field-type-' + field_type).val();
                        curElem.find('.tsm-preview-title').text(tsm_excerpts(preview_txt, 30, '...'));
                    }
                    if ( $(this).find('.tsm-field-type-' + field_type).hasClass('tsm-title-input') ){
                        preview_txt = $(this).find('.tsm-field-type-' + field_type).val();
                        curElem.find('.tsm-preview-title').text(tsm_excerpts(preview_txt, 30, '...'));
                    }
                 }
                 
                 
                 elem_data['fields'][field_key] = {
                    'val': field_val
                 }
                 
                 //for ( var i in elem_data.fields ) {
//                    if (i == field_key){
//                        elem_data.fields[i].val = field_val;
//                    }
//                 }
            });
            
            var elem_data_json = JSON.stringify(elem_data);
            curElem.attr('data-elem-json', elem_data_json);
            
        }
        else{ // is widget
            
            // Fix checked value = 'on' --> '1' 
            if (thisElemForm.find('.tsm-edit-elem-right-part form input[type=checkbox]').length) {
                thisElemForm.find('.tsm-edit-elem-right-part form input[type=checkbox]').each(function(){
                    var this_val = $(this).val();
                    if ($(this).is(':checked')){
                        if (this_val == 'on') {
                            $(this).val(1);   
                        }
                    }
                });
            }
            
            // Serialize widget form
            var wg_form_instance = tsm_fixed_encodeURIComponent( thisElemForm.find('.tsm-edit-elem-right-part form').serialize() );
            curElem.attr('data-wg-instance', wg_form_instance);
            
        }
        
        
        // Show Grid
        thisGrid.slideDown(300);
        
        // Hidden elems tabs
        thisForm.find('.tsm-suported-elems-tabs').css({'display': 'none'});
        
        // Show form actions
        thisForm.find('.tsm-actions-wrap').slideDown(300);
        
        // Hidden edit elem form
        thisElemForm.css({'display': 'none'}).html('');
        
        e.preventDefault(); 
    });

    
    // Add list child item of sub list
    $(document).on('click', '.tsm-list li .tsm-add-child-item', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li');
        var parentList = $this.closest('ul.tsm-list');
        var parentLv = parseInt(parentList.attr('data-lv'));
        
        if ( isNaN(parentLv) || parentLv < 0 ) {
            parentLv = 0;
            parentList.attr('data-lv', parentLv);
        }
        
        // If already has child list, add an item only
        if ( thisLi.find('> ul.tsm-list').length ) {
            var newChildListItem = tsm_script_localize.html.list_item;
            thisLi.find('> ul.tsm-list').append(newChildListItem);
        }
        else{
            // Add new child list with an item in it
            var newChildList = tsm_script_localize.html.list;
            thisLi.append(newChildList);
            thisLi.find('> ul.tsm-list').attr('data-lv', parentLv + 1);
        }
        tsm_update_sortable();
        tsm_update_list_level();
        
        e.preventDefault();
    });
    
    
    // Add item of level 0 list
    $(document).on('click', '.tsm-list-lv-0 .tsm-add-item-lv-0', function(e){
        
        var thisList = $(this).closest('ul.tsm-list-lv-0');
        
        if ( tsm_is_locked(thisList) ) {
            return false;
        }
        
        var newChildListItem = tsm_script_localize.html.list_item;
        thisList.find('.tsm-list-lv-0-actions-wrap').before(newChildListItem);
        tsm_update_sortable();
        
        e.preventDefault();
    });
    
    // Delete list lv 0
    $(document).on('click', '.tsm-list-lv-0 .tsm-del-list-lv-0', function(e){
        
        var thisList = $(this).closest('ul.tsm-list-lv-0');
        
        if ( tsm_is_locked(thisList) ) {
            return false;
        }
        
        var c = confirm(tsm_script_localize.confirm.del_list);
        
        if ( !c ){
            return false;
        }
        
        tsm_locked(thisList);
        
        thisList.hide(300, function(){
            thisList.remove();
        });
        
        e.preventDefault();
    });
    
    
    // Delete list item
    $(document).on('click', '.tsm-list li .tsm-del-item', function(e){
        
        var c = confirm(tsm_script_localize.confirm.del_li_item);
        
        if ( !c ) {
            return false;
        }
        
        var $this = $(this);
        var thisItem = $this.closest('.tsm-li');
        var thisUl = thisItem.closest('.tsm-list');
        
        if ( thisItem.find('> ul > li').length ) {
            var liChildrenHtml = thisItem.find('> ul').html();
        }
        
        // Move all li children level up
        thisItem.after(liChildrenHtml);
        thisItem.remove();
        if (!thisUl.find('> li.tsm-li').length){
            thisUl.remove();
        }
        
        tsm_update_list_level();
        tsm_update_sortable();
        
        e.preventDefault();
    });
    
    
    // Add new lv 0 list
    $(document).on('click', '.tsm-megamenu-form-dropdown .tsm-add-dropdown-list-btn', function(e){
        
        var $this = $(this);
        var thisForm = $this.closest('.tsm-megamenu-form-dropdown');
        var thisDropdownGrid = thisForm.find('.tsm-dropdown-grid');
        var err = false;
        var new_list_w = tsm_script_localize.vars.default_list_w;
        
        // Get total list width
        var total_list_w = 0;
        
        if (tsm_is_locked(thisForm)){
            return false;
        }
        
        thisDropdownGrid.find('ul.tsm-list-lv-0').each(function(){
            var this_list_w = parseInt($(this).attr('data-list-w'));
            
            if (isNaN(this_list_w) || this_list_w <= 0 || this_list_w % 10 != 0) {
                this_list_w = tsm_script_localize.vars.default_list_w;
                $(this).attr('data-list-w', this_list_w);
            }
            total_list_w += this_list_w;
        });
        
        if (total_list_w + new_list_w > 1200) {
            
            if (1200 - total_list_w >= 50) {
                new_list_w = 1200 - total_list_w;
            }
            else{
                alert(tsm_script_localize.text.max_total_list_width);
                err = true;   
            }
            
        }
        
        if (err) {
            return false;
        }
        
        thisDropdownGrid.append(tsm_script_localize.html.list_lv_0);
        tsm_update_last_list_lv_0_width(thisDropdownGrid);
        tsm_update_sortable();
        
        e.preventDefault(); 
    });
    
    // Make sublist smaller
    $(document).on('click', '.tsm-megamenu-form-dropdown .tsm-make-sublist-smaller', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        
        if ( tsm_is_locked(thisLi) ) {
            return false;
        }
        
        if ( thisLi.find('> ul.tsm-list').length ){
            var thisSubList = thisLi.find('> ul.tsm-list');
            var sub_list_w = parseInt(thisSubList.attr('data-list-w'));
            if ( isNaN(sub_list_w) ){
                sub_list_w = tsm_script_localize.$tsm_default_list_w;
                thisSubList.attr('data-list-w', sub_list_w);
            }
            if ( sub_list_w % 10 != 0 ) {
                sub_list_w = tsm_script_localize.$tsm_default_list_w;
            }
            
            if ( sub_list_w > 50 ) {
                thisSubList.removeClass('w-' + sub_list_w);
                sub_list_w -= 10;
                thisSubList.attr('data-list-w', sub_list_w).addClass('w-' + sub_list_w);
            }
            
        }
        
        e.preventDefault(); 
    });
    
    // Make sublist bigger
    $(document).on('click', '.tsm-megamenu-form-dropdown .tsm-make-sublist-bigger', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        
        if ( tsm_is_locked(thisLi) ) {
            return false;
        }
        
        if ( thisLi.find('> ul.tsm-list').length ){
            var thisSubList = thisLi.find('> ul.tsm-list');
            var sub_list_w = parseInt(thisSubList.attr('data-list-w'));
            if ( isNaN(sub_list_w) ){
                sub_list_w = tsm_script_localize.$tsm_default_list_w;
                thisSubList.attr('data-list-w', sub_list_w);
            }
            if ( sub_list_w % 10 != 0 ) {
                sub_list_w = tsm_script_localize.$tsm_default_list_w;
            }
            
            if ( sub_list_w < 1200 ) {
                thisSubList.removeClass('w-' + sub_list_w);
                sub_list_w += 10;
                thisSubList.attr('data-list-w', sub_list_w).addClass('w-' + sub_list_w);
            }
            
        }
        
        e.preventDefault(); 
    });
    
    // Make list level 0 smaller
    $(document).on('click', '.tsm-megamenu-form-dropdown .tsm-make-list-lv-0-smaller', function(e){
        
        var $this = $(this);
        var thisList = $this.closest('ul.tsm-list-lv-0');
        
        if ( tsm_is_locked(thisList) ) {
            return false;
        }
        
        var this_list_w = parseInt(thisList.attr('data-list-w'));
        
        if ( isNaN(this_list_w) || this_list_w % 10 != 0){
            this_list_w = tsm_script_localize.$tsm_default_list_w;
            thisList.attr('data-list-w', this_list_w);
        }
        
        if ( this_list_w > 50 ) {
            thisList.removeClass('w-' + this_list_w);
            this_list_w -= 10;
            thisList.attr('data-list-w', this_list_w).addClass('w-' + this_list_w);
        }
        
        e.preventDefault();
        
    });
    
    // Make list level 0 bigger
    $(document).on('click', '.tsm-megamenu-form-dropdown .tsm-make-list-lv-0-bigger', function(e){
        
        var $this = $(this);
        var thisList = $this.closest('ul.tsm-list-lv-0');
        var thisDropDownGrid = thisList.closest('.tsm-dropdown-grid');
        
        if ( tsm_is_locked(thisList) ) {
            return false;
        }
        
        var total_list_w = 0;
        thisDropDownGrid.find('ul.tsm-list-lv-0').each(function(){
            var this_list_w = parseInt($(this).attr('data-list-w'));
            
            if (isNaN(this_list_w) || this_list_w <= 0 || this_list_w % 10 != 0) {
                this_list_w = tsm_script_localize.vars.default_list_w;
                $(this).attr('data-list-w', this_list_w);
            }
            total_list_w += this_list_w;
        });
        
        if ( total_list_w + 10 > 1200 ) {
            alert(tsm_script_localize.text.max_total_list_width);
            return false;
        }
        
        var this_list_w = parseInt(thisList.attr('data-list-w'));
        
        if ( isNaN(this_list_w) || this_list_w % 10 != 0){
            this_list_w = tsm_script_localize.$tsm_default_list_w;
            thisList.attr('data-list-w', this_list_w);
        }
        
        if ( this_list_w < 1200 ) {
            thisList.removeClass('w-' + this_list_w);
            this_list_w += 10;
            thisList.attr('data-list-w', this_list_w).addClass('w-' + this_list_w);
        }
        
        e.preventDefault();
        
    });
    
    
    // Edit list li
    $(document).on('click', '.tsm-megamenu-form-dropdown ul.tsm-list li.tsm-li .tsm-edit-item', function(e){
        
        var $this= $(this);
        var thisLi = $this.closest('li.tsm-li');
        var thisMegamenuFormDropdown = thisLi.closest('.tsm-megamenu-form-dropdown');
        var li_obj_label = thisLi.attr('data-obj-label');
        
        if ( typeof li_obj_label == 'undefined' || typeof li_obj_label == false ){
            li_obj_label = tsm_script_localize.text.custom;
        }
        
        if ( tsm_is_locked(thisMegamenuFormDropdown) ) {
            return false;
        }
        
        // if this li is current, check form exists
        if ( thisLi.hasClass('tsm-current') ) {
            if ( !thisLi.find('.tsm-edit-dropdown-list-li-form').length ){
                thisLi.find('> span').after(tsm_script_localize.html.edit_li_form);
                thisLi.find('.tsm-select').addClass('tsm-select2');
                tsm_update_chosen_elem(thisLi.find('.tsm-select2'));
            }
            else{
                thisLi.find('.tsm-edit-dropdown-list-li-form').remove();
            }
        }
        else{
            // Remove all existed form
            $('.tsm-edit-dropdown-list-li-form').remove();
            
            // Set this li is current
            $('.tsm-megamenu-form-dropdown ul.tsm-list li.tsm-li').removeClass('tsm-current');
            thisLi.addClass('tsm-current');
            if ( !thisLi.find('.tsm-edit-dropdown-list-li-form').length ){
                thisLi.find('> span').after(tsm_script_localize.html.edit_li_form);
                thisLi.find('.tsm-select').addClass('tsm-select2');
                tsm_update_chosen_elem(thisLi.find('.tsm-select2'));
            }   
        }
        
        thisLi.find('.tsm-change-li-link-type span').html(li_obj_label);
        
        var li_text = thisLi.attr('data-text');
        var li_link = thisLi.attr('data-link');
        //var li_obj_id = thisLi.attr('data-obj-id');
//        var li_obj_name = thisLi.attr('data-obj-name');
//        var li_link_type = thisLi.attr('data-link-type');
        var li_icon = thisLi.attr('data-icon');
        var li_align_right = thisLi.attr('data-align-right');
        var li_addition_class = thisLi.attr('data-addition-class');
        
        if (typeof li_text === 'undefined' || li_text === false) {
            li_text = '';
        }
        
        if (typeof li_link === 'undefined' || li_link === false) {
            li_link = '';
        }
        
        if (typeof li_icon === 'undefined' || li_icon === false) {
            li_icon = '';
        }
        
        if (typeof li_align_right === 'undefined' || li_align_right === false) {
            li_align_right = 'no';
        }
        
        if (typeof li_addition_class === 'undefined' || li_addition_class === false) {
            li_addition_class = '';
        }
        
        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-text-input').val(li_text);
        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-link-input').val(li_link);
        //thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-link-obj-id-input').val(li_obj_id);
//        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-link-obj-name-input').val(li_obj_name);
//        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-link-obj-type-input').val(li_link_type);
        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-right-dropdown-select').val(li_align_right);
        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-icon-chosen').attr('data-icon-class', li_icon);
        if ( li_icon != '' ){
            thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-icon-chosen').html('<i class="' + li_icon + '"></i>');   
        }
        else{
            thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-icon-chosen').html('<i>' + tsm_script_localize.text.no_icon + '</i>');
        }
        
        thisLi.find('.tsm-edit-dropdown-list-li-form .tsm-li-addition-class-input').val(li_addition_class);
        
        e.preventDefault(); 
    });
    
    // Show/Hide change link type
    $(document).on('click', '.tsm-edit-dropdown-list-li-form .tsm-change-li-link-type', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        
        thisLi.find('.tsm-li-link-type-choser-wrap').slideToggle(300);
        
        e.preventDefault(); 
    });
    
    // Close chaneg link type
    $(document).on('click', '.tsm-edit-dropdown-list-li-form .tsm-close-li-type-btn', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        
        thisLi.find('.tsm-li-link-type-choser-wrap').slideUp(300);
        
        e.preventDefault(); 
    });
    
    // Update an item link info
    $(document).on('click', '.tsm-edit-dropdown-list-li-form .tsm-update-li-type-btn', function(e){
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        var cur_link_type = thisLi.attr('data-link-type');
        var obj_name = thisLi.find('.tsm-li-select-link-type').val(); // post type name, taxonomy name
        var obj_type = thisLi.find('.tsm-li-select-link-type option:selected').attr('data-link-type');
        var obj_label = thisLi.find('.tsm-li-select-link-type option:selected').html();
        
        if ( obj_type == cur_link_type && ( cur_link_type == 'custom' || cur_link_type == '' ) ){
            return false;
        }
        
        switch ( obj_type ){
            case 'custom':
                thisLi.attr('data-text', tsm_script_localize.text.new_item).attr('data-obj-label', tsm_script_localize.text.custom).find('.tsm-li-text-input').val(tsm_script_localize.text.new_item);
                thisLi.find('.tsm-li-link-input').val('#');
                break;
            case 'post_type': case 'taxonomy':
                if ( thisLi.find('.tsm-postypes-terms-chosen .tsm-ratio:checked').length ){
                    var obj_id = thisLi.find('.tsm-postypes-terms-chosen .tsm-ratio:checked').val();
                    var li_text = thisLi.find('.tsm-postypes-terms-chosen .tsm-ratio:checked').attr('data-original-title');
                    thisLi.attr('data-link-type', obj_type).attr('data-obj-id', obj_id).attr('data-obj-name', obj_name).attr('data-obj-label', obj_label);
                    thisLi.attr('data-text', li_text).find('.tsm-li-text-input').val(li_text);
                    thisLi.find('.tsm-li-link-input').val('#');
                }
                else{
                    
                }
                break;
        }
        
        thisLi.find('.tsm-change-li-link-type span').html(obj_label);
        tsm_update_current_editing_li();
        thisLi.find('.tsm-li-link-type-choser-wrap').slideUp(300);
        
        e.preventDefault();
    });
    
    // Close dropdown list li edit form
    $(document).on('click', '.tsm-edit-dropdown-list-li-form .tsm-close-btn', function(e){
        var thisForm = $(this).closest('.tsm-edit-dropdown-list-li-form');
        
        if (tsm_is_locked(thisForm)){
            return false;
        }
        
        tsm_update_current_editing_li();
        
        tsm_lock_form(thisForm);
        thisForm.hide(300, function(){
            tsm_unlock_form(thisForm);
            thisForm.remove();
        });
        e.preventDefault();
    });
    
    // Load posts, page, custom post types, terms, categories ...
    $(document).on('change', '.tsm-edit-dropdown-list-li-form .tsm-li-select-link-type', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        var obj_name = $this.val(); // post type name, taxonomy name
        var obj_type = $this.find('option:selected').attr('data-link-type');
        var limit = 15;
        var paged = 1;
        
        tsm_load_posttypes_terms_chosen_via_ajax(thisLi, obj_name, obj_type, limit, paged);
                
        e.preventDefault(); 
    });
    
    $(document).on('click', '.tsm-edit-dropdown-list-li-form .tsm-link-type-pagination li', function(e){
        
        var $this = $(this);
        var thisLi = $this.closest('li.tsm-li');
        var obj_name = $this.attr('data-obj-name'); // post type name, taxonomy name
        var obj_type = $this.attr('data-link-type');
        var limit = 15;
        var paged = $this.attr('data-page');
        
        tsm_load_posttypes_terms_chosen_via_ajax(thisLi, obj_name, obj_type, limit, paged);
        
        e.preventDefault(); 
    });
    
    function tsm_load_posttypes_terms_chosen_via_ajax($li, obj_name, obj_type, limit, paged){
        // Load elem form via ajax
        var data = {
            action          :   'tsm_load_posttypes_terms_chosen_via_ajax',
            obj_name        :   obj_name,
            obj_type        :   obj_type,
            limit           :   limit,
            paged           :   paged
        }
        
        $.post(ajaxurl, data, function(response){
            $li.find('.tsm-postypes-terms-chosen-wrap').html(response);
        });
    }
    
    $(document).on('change', '.tsm-megamenu-form-dropdown .tsm-edit-dropdown-list-li-form input[type=text], .tsm-megamenu-form-dropdown .tsm-edit-dropdown-list-li-form select', function(){
        tsm_update_current_editing_li();
    });
    
    function tsm_update_current_editing_li(){
        
        $('.tsm-megamenu-form-dropdown .tsm-edit-dropdown-list-li-form').each(function(){
            var thisForm = $(this);
            var thisLi = thisForm.closest('li.tsm-li');
            var li_text = thisForm.find('.tsm-li-text-input').val();
            var li_link = thisForm.find('.tsm-li-link-input').val();
            var li_icon = thisForm.find('.tsm-icon-chosen').attr('data-icon-class');
            var li_align_right = thisForm.find('.tsm-li-right-dropdown-select').val();
            var li_addition_class = thisForm.find('.tsm-li-addition-class-input').val();
            
            if ( li_text != '' ){
                thisLi.attr('data-text', li_text);
                if ( li_icon != '' ){
                    thisLi.find('> span').html('<i class="' + li_icon + '"></i>' + li_text + tsm_script_localize.html.edit_li_control);
                }
                else{
                    thisLi.find('> span').html(li_text + tsm_script_localize.html.edit_li_control);   
                }
            }
            thisLi.attr('data-link', li_link);
            thisLi.attr('data-icon', li_icon);
            thisLi.attr('data-align-right', li_align_right);
            thisLi.attr('data-addition-class', li_addition_class);
            
        });
        
    }
    
    
    // Update last lv 0 list. Max total list width must be 1200, if total width > 1200 destroy last list lv 0
    function tsm_update_last_list_lv_0_width( $dropDownGrid ) {
        
        var total_list_w = 0;
        $dropDownGrid.find('ul.tsm-list-lv-0').each(function(){
            var this_list_w = parseInt($(this).attr('data-list-w'));
            
            if (isNaN(this_list_w) || this_list_w <= 0 || this_list_w % 10 != 0) {
                this_list_w = tsm_script_localize.vars.default_list_w;
                $(this).attr('data-list-w', this_list_w);
            }
            total_list_w += this_list_w;
        });
        
        if (total_list_w > 1200) {
            var new_last_list_w = 50;
            var old_last_list_w = parseInt($dropDownGrid.find('ul.tsm-list-lv-0:last-child').attr('data-list-w'));
            
            /*
            if (isNaN(old_last_list_w) || old_last_list_w <= 0 || old_last_list_w % 10 != 0) {
                $dropDownGrid.find('ul.tsm-list-lv-0:last-child').attr('data-list-w', new_last_list_w);
            }
            */
            
            new_last_list_w = 1200 - total_list_w + old_last_list_w;
            
            // Destroy if minimum last list width < 50px
            if (new_last_list_w < 50) {
                $dropDownGrid.find('ul.tsm-list-lv-0:last-child').remove();
            }
            else{
                $dropDownGrid.find('ul.tsm-list-lv-0:last-child').attr('data-list-w', new_last_list_w).removeClass('w-' + old_last_list_w).addClass('w-' + new_last_list_w);
            }
            
        }
        
    }
    
    
    function tsm_update_list_level() {
        
        $('.tsm-dropdown-grid > ul.tsm-list-lv-0').each(function(){
            var $this = $(this);
            var thisLevel = parseInt( $this.attr('data-lv') );
            
            if ( isNaN( thisLevel ) || thisLevel < 0 ){
                thisLevel = 0;
                $this.attr('data-lv', thisLevel);
            }
            
            tsm_update_sub_list_level($this, thisLevel);
             
        });
    }
    
    function tsm_update_sub_list_level( $list, lv ) {
        
        $list.attr('data-lv', lv);
        
        $list.find('> li.tsm-li').removeClass('tsm-has-children');
        if ($list.find('> li.tsm-li > ul.tsm-list').length) {
            $list.find('> li.tsm-li > ul.tsm-list').each(function(){
                $(this).closest('li.tsm-li').addClass('tsm-has-children');
                tsm_update_sub_list_level($(this), lv + 1);
            });   
        }
    }
    
    // Dupliacate row
    $(document).on('click', '.tsm-grid .tsm-row .tsm-duplicate-row', function(e){
        
        var $row = $(this).closest('.tsm-row');
        var thisGrid = $row.closest('.tsm-grid');
        
        if ( tsm_is_locked( thisGrid ) ){
            return false;
        }
        
        var row_clone_html = $('<div>').append($row.clone()).html(); 
        
        $row.after(row_clone_html);
        
        tsm_reset_elem_index();
        tsm_update_sortable();
        tsm_update_clearfix();
        
        e.preventDefault(); 
        
    });
    
    // Dupliacate col
    $(document).on('click', '.tsm-grid .tsm-col .tsm-duplicate-col', function(e){
        
        var $col = $(this).closest('.tsm-col');
        var thisGrid = $col.closest('.tsm-grid');
        
        if ( tsm_is_locked( thisGrid ) ){
            return false;
        }
        
        var col_clone_html = $('<div>').append($col.clone()).html();
        
        $col.after(col_clone_html);
        
        tsm_reset_elem_index();
        tsm_update_sortable();
        tsm_update_clearfix();
        
        e.preventDefault(); 
        
    });
    
    // Dupliacate elem
    $(document).on('click', '.tsm-grid .tsm-elem .tsm-duplicate-elem', function(e){
        
        var $elem = $(this).closest('.tsm-elem');
        var thisGrid = $elem.closest('.tsm-grid');
        
        if ( tsm_is_locked( thisGrid ) ){
            return false;
        }
        
        var elem_clone_html = $('<div>').append($elem.clone()).html();
        
        $elem.after(elem_clone_html);
        
        tsm_reset_elem_index();
        tsm_update_sortable();
        tsm_update_clearfix();
        
        e.preventDefault(); 
        
    });
    
    // Dupliacate list
    $(document).on('click', '.tsm-dropdown-grid ul.tsm-list-lv-0 .tsm-duplicate-item-lv-0', function(e){
        
        var $thisUl = $(this).closest('ul.tsm-list-lv-0');
        var thisDropdownGrid = $thisUl.closest('.tsm-dropdown-grid');
        var this_ul_w = parseInt($thisUl.attr('data-list-w'));
        var this_ul_w_bak = this_ul_w;
        
        var total_list_w = 0;
        thisDropdownGrid.find('ul.tsm-list-lv-0').each(function(){
            var this_list_w = parseInt($(this).attr('data-list-w'));
            
            if (isNaN(this_list_w) || this_list_w <= 0 || this_list_w % 10 != 0) {
                this_list_w = tsm_script_localize.vars.default_list_w;
                $(this).attr('data-list-w', this_list_w);
            }
            total_list_w += this_list_w;
        });
        
        if ( total_list_w + this_ul_w > 1200 ) {
            while ( total_list_w + this_ul_w > 1200 && this_ul_w > 50 ) {
                this_ul_w -= 10;
            }
        }
        
        if ( this_ul_w < 50 || total_list_w + this_ul_w > 1200 ) {
            return alert( tsm_script_localize.text.max_total_list_width );
        }
        
        if ( tsm_is_locked( thisDropdownGrid ) ){
            return false;
        }
        
        //$thisUl.attr('data-list-w', this_ul_w);
        $thisUl.removeClass('w-' + this_ul_w_bak).addClass('w-' + this_ul_w).attr('data-list-w', this_ul_w);
        var ul_clone_html = $('<div>').append($thisUl.clone()).html();
        $thisUl.removeClass('w-' + this_ul_w).addClass('w-' + this_ul_w_bak).attr('data-list-w', this_ul_w_bak);
        
        $thisUl.after(ul_clone_html);
        
        tsm_update_sortable();
        
        e.preventDefault(); 
        
    });
    
    // Dupliacate li item
    $(document).on('click', '.tsm-dropdown-grid li.tsm-li .tsm-duplicate-item', function(e){
        
        var $liItem = $(this).closest('li.tsm-li');
        var thisDropdownGrid = $liItem.closest('.tsm-dropdown-grid');
        
        if ( tsm_is_locked( thisDropdownGrid ) ){
            return false;
        }
        
        var li_item_clone_html = $('<div>').append($liItem.clone()).html();
        
        $liItem.after(li_item_clone_html);
        
        tsm_update_sortable();
        
        e.preventDefault(); 
        
    });
    
// ********************************************************************************************************`//
//                                          SAVE MEGA MENU                                                  //
// ******************************************************************************************************** //
    
    // Wordpress save menu button click
    //$(document).on('click', 'input[type=submit].menu-save', function(){
//        
//        tsm_save_mega_menu();
//        
//    });
    var menu_has_been_saved = false;
    $( 'form#update-nav-menu, form#nav-menu-meta' ).submit( function(e) {
        
        if ( tsm_is_locked( $('#tsm-mega-menu-settings-form') ) || tsm_is_locked( $('.tsm-theme-settings-wrap') ) ){
            return false;
        }
        
        var form = $(this);
        tsm_save_mega_menu();
        
        tsm_wait_for_saving_mega_data();
        
    }); 
    
    function tsm_wait_for_saving_mega_data(){
        var max_times = 2500; 
        var times_out = false;
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > max_times){
                times_out = true;
                break;
            }
            if ( $('.tsm-satus-info-wrap').attr('data-saving-status') == 'no' ){
                break;
            }
        }
    }
    
    function tsm_sleep(milliseconds) {
      var start = new Date().getTime();
      for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
          break;
        }
      }
    }

    $(document).on('click', '.tsm-save-mega-menu', function(e){
        
        tsm_save_mega_menu();
        
        e.preventDefault();
        
    });
    
    function tsm_save_mega_menu() {
        
        if (tsm_is_locked($('.tsm-megamenu-form'))){
            return false;
        }
        
        $('.tsm-satus-info-wrap').attr('data-saving-status', 'yes');
        
        var menu_id = $('#tsm-mega-menu-settings-form').attr('data-selected-menu-id');
        var menu_data = {
            'settings'  :   {},
            'layout'    :   {}
        };
        var menu_settings = {};
        var menu_layout = { // 2 parts: Dropdown part and Grid part
            'layout_settings'   : {},   // Settings of each menu item
            'dropdown'          : {},   // Each item is a dropdown menu
            'grid'              : {}    // Each item is a grid
        }; 
        
        // Save menu settings
        var settings_form = $('#tsm-mega-menu-settings-form');
        var enable_mega_menu = (settings_form.find('.tsm-enable-mega-menu-settings').is(':checked')) ? 'yes' : 'no';
        //var menu_css_data = tsm_get_menu_css_color_scheme_data_for_save(); // Old version
        var menu_theme_params = tsm_get_menu_theme_params_for_save();
        
        menu_settings['enable_mega_menu'] =  enable_mega_menu;
        //menu_settings['menu_css_data'] = menu_css_data; // Old version
        menu_settings['menu_theme_params'] = menu_theme_params; // Modern version
        menu_settings['position'] = settings_form.find('#tsm-position-select').val();
        menu_settings['showon'] = settings_form.find('#tsm-show-on-select').val();
        menu_settings['responsive'] = settings_form.find('#tsm-responsive-select').val();
        menu_settings['fullwidth'] = settings_form.find('#tsm-fullwidth-select').val();
        menu_settings['sticky'] = settings_form.find('#tsm-sticky-select').val();
        menu_settings['fixed'] = settings_form.find('#tsm-fixed-select').val();
        menu_settings['effect'] = settings_form.find('#tsm-effect-select').val();   // Old version
        menu_settings['easing'] = settings_form.find('#tsm-easing-select').val();   // Old version
        menu_settings['speed'] = settings_form.find('#tsm-speed-input').val();      // Old version
        menu_settings['delay'] = settings_form.find('#tsm-delay-input').val();      // Old version
        menu_settings['breakpoint'] = settings_form.find('#tsm-break-point-input').val();
        menu_settings['zindex'] = settings_form.find('#tsm-z-index-input').val();
        menu_settings['show_effect'] = settings_form.find('#tsm-show-effect-select').val();     // Modern version
        menu_settings['show_speed'] = settings_form.find('#tsm-show-speed-input').val();        // Modern version
        menu_settings['show_delay'] = settings_form.find('#tsm-show-delay-input').val();        // Modern version
        menu_settings['hide_effect'] = settings_form.find('#tsm-hide-effect-select').val();     // Modern version
        menu_settings['hide_speed'] = settings_form.find('#tsm-hide-speed-input').val();        // Modern version
        menu_settings['hide_delay'] = settings_form.find('#tsm-hide-delay-input').val();        // Modern version
        menu_settings['additional_css'] = tsm_get_live_css();   // From live css editor
        menu_settings['live_css_editor_settings'] = tsm_get_live_css_editor_settings();
        menu_settings['custom_css'] = '';   // User css
        menu_settings['fonts'] = tsm_get_all_fonts_needed();
        
        
        if ($('.tsm-megamenu-form').length){
            tsm_lock_form($('.tsm-megamenu-form'));
        }
        
        var menu_item_num = 0;
        // Save layout settings of each menu item
        $('.tsm-menu-item-wrap').each(function(){
            
            var thisMenuItemForm = $(this);
            var menu_item_id = thisMenuItemForm.attr('data-menu-item-id');
            var show_caret = (thisMenuItemForm.find('#tsm-show-caret-' + menu_item_id).is(':checked')) ? 'yes': 'no';
            var caret = thisMenuItemForm.find('#tsm-caret-' + menu_item_id).val();
            var drop_direction = thisMenuItemForm.find('#tsm-drop-direction-' + menu_item_id).val();
            var icon_class = thisMenuItemForm.find('.tsm-choose-icon-label .tsm-icon-chosen').attr('data-icon-class');
            var font_family = thisMenuItemForm.find('#tsm-typography-' + menu_item_id).val();
            var mega_type = thisMenuItemForm.find('.tsm-mega-menu-type-btn').attr('data-mega-menu-type');
            
            menu_layout['layout_settings'][menu_item_num] = {
                'menu_item_id'  :   menu_item_id,
                'show_caret'    :   show_caret,
                'caret'         :   caret,
                'drop_direction':   drop_direction,
                'icon_class'    :   icon_class,
                'font_family'   :   font_family,
                'mega_type'     :   mega_type       // "grid" or "dropdown"
            }
            
            menu_item_num++;
        });
        
        
        
        var grid_num = 0; // form num
        
        // Save menu layout (grid)
        $('.tsm-megamenu-form').each(function(){
            
            var thisForm = $(this);
            var menu_item_id = thisForm.attr('data-menu-item-id');
            var thisGrid = thisForm.find('.tsm-grid');
            var row_num = 0;
            
            var layout_grid = {
                'grid_data'     :   {},     // Each item is a row data
                'menu_item_id'  :   '',
                'width'         :   'auto',
                'fullwidth'     :   'yes',  // "yes" or "no"
                'min_width'     :   0,
                'max_width'     :   980
            };
            var grid_width = thisGrid.find('.tsm-grid-width').val();
            var fullwidth = (thisGrid.find('.tsm-submenu-full-with').is(':checked')) ? 'yes' : 'no';
            var min_width = (thisGrid.find('.tsm-min-max-width.tsm-slider-range').slider( "values", 0 ));
            var max_width = (thisGrid.find('.tsm-min-max-width.tsm-slider-range').slider( "values", 1 ));
            
            layout_grid['width'] = grid_width;
            layout_grid['fullwidth'] = fullwidth;
            layout_grid['min_width'] = min_width;
            layout_grid['max_width'] = max_width;
                        
            // Colect data of each row in grid
            thisGrid.find('.tsm-row').each(function(){
                var thisRow = $(this);
                var rows_args = {
                    'additional_class'  :   '',
                    'cols'              :   {}
                };
                
                var col_num = 0;
                // Colect data of each col in row
                thisRow.find('.tsm-col').each(function(){
                    var thisCol = $(this);
                    var col_w = thisCol.attr('data-col-width'); 
                    var col_additional_class = thisCol.attr('data-col-class');
                    var cols_args = {
                        'additional_class'  :   col_additional_class,
                        'col_width'         :   col_w,     // 1, 2, ..., 12 (c-1 --> c-12);
                        'elems'             :   {}
                    };
                    
                    var elem_num = 0;
                    thisCol.find('.tsm-elem').each(function(){
                        var thisElem = $(this);
                        var elem_key = '';
                        var is_widget = thisElem.hasClass('tsm-widget');
                        var elems_args = {};
                        
                        if (is_widget) {
                            elem_key = thisElem.attr('data-widget-key');
                            var widget_instance = thisElem.attr('data-wg-instance');
                            elems_args = {
                                'elem_type'         :   'widget',   // widget or normal
                                'widget_key'        :   elem_key,
                                'widget_instance'   :   widget_instance,
                                'additional_class'  :   ''
                            }
                        }
                        else{
                            elem_key = thisElem.attr('data-elem-key');
                            var elem_data_json = thisElem.attr('data-elem-json');
                            elems_args = {
                                'elem_type'         :   'normal',   // widget or normal
                                'elem_key'          :   elem_key,
                                'data_json'         :   elem_data_json,
                                'additional_class'  :   ''
                                
                            }
                        }
                        
                        cols_args['elems'][elem_num] = elems_args;
                        
                        elem_num++;
                    });
                    
                    // Push a col data to rows data
                    rows_args['cols'][col_num] = cols_args;
                    
                    col_num++;
                });
                
                // Push a row data to grid data 
                layout_grid['grid_data'][row_num] = rows_args;
                row_num++;
            });
            
            layout_grid['menu_item_id'] = menu_item_id;
            
            menu_layout['grid'][grid_num] = layout_grid;
            
            grid_num++;
        }); // End Save menu layout (grid)
        
        
        var dropdown_num = 0;
        // Save dropdown
        $('.tsm-megamenu-form-dropdown').each(function(){
            var thisForm = $(this);
            var menu_item_id = thisForm.attr('data-menu-item-id');
            var thisDropdownGrid = thisForm.find('.tsm-dropdown-grid');
            var list_lv_0_num = 0;
            
            var list_layout = {
                'list_data'     :   {}, // Each item is a list 
                'menu_item_id'  :   ''
            };
            
            var list_lv_0_num = 0;
            thisDropdownGrid.find('> ul.tsm-list-lv-0').each(function(){
                var this_list_lv_0 = $(this);
                list_layout['list_data'][list_lv_0_num] = tsm_get_dropdown_list_data_recursive(this_list_lv_0);
                list_lv_0_num++;
            });
            
            list_layout['menu_item_id'] = menu_item_id;
            
            menu_layout['dropdown'][dropdown_num] = list_layout;
            
            dropdown_num++;
        }); // End Save menu layout dropdown
        
        menu_data['settings'] = menu_settings;
        menu_data['layout'] = menu_layout;
        
        $('.tsm-save-mega-menu i').removeClass('fa-floppy-o').addClass('fa-refresh fa-spin');
        tsm_show_status_info(tsm_script_localize.text.saving);
        
        // Save all data via ajax
        var data = {
            action          :   'tsm_save_mega_menu_data_via_ajax',
            nonce           :   tsm_nonce.ajax_nonce,
            menu_data       :   menu_data,
            menu_id         :   menu_id
        }
        
        return $.post(ajaxurl, data, function(response){
            $('.tsm-info-temp').html(response);
            
            $('.tsm-save-mega-menu i').removeClass('fa-refresh fa-spin').addClass('fa-floppy-o');
            tsm_unlock_form($('.tsm-megamenu-form'));
            $('.tsm-satus-info-wrap').attr('data-saving-status', 'no');
            tsm_show_status_info(tsm_script_localize.text.tsm_saved);
            tsm_hide_status_info();
        });
    }
    
    function tsm_get_dropdown_list_data_recursive( $dropdown_list ) {
        var list_lv = parseInt($dropdown_list.attr('data-lv'));
        var list_w = parseInt($dropdown_list.attr('data-list-w'));
        var list_additional_class = $dropdown_list.attr('data-addition-class');
        
        if (typeof list_lv === 'undefined' || list_lv === false || isNaN(list_lv)) {
            list_lv = 0;
        }
        
        if (typeof list_w === 'undefined' || list_w === false || isNaN(list_w)) {
            list_w = 250;
        }
        
        if (typeof list_additional_class !== 'undefined' && list_additional_class !== false) {
            list_additional_class = '';
        }
        
        var dropdown_list_data = {
            'list_w'            :   list_w,
            'addition_class'    :   list_additional_class,
            'items_data'        :   {}
        };
        
        var li_num = 0;
        $dropdown_list.find('> li.tsm-li').each(function(){
             var thisLi = $(this);
             var li_link = thisLi.attr('data-link');
             var li_text = thisLi.attr('data-text');
             var li_obj_id = thisLi.attr('data-obj-id');
             var li_obj_name = thisLi.attr('data-obj-name');
             var li_obj_label = thisLi.attr('data-obj-label');
             var li_link_type = thisLi.attr('data-link-type');
             var li_icon = thisLi.attr('data-icon');
             var li_align_right = thisLi.attr('data-align-right'); // "yes" or "no"
             var li_additional_class = thisLi.attr('data-addition-class');
             
             dropdown_list_data['items_data'][li_num] = {
                'link'      :   li_link,
                'text'      :   li_text,
                'li_obj_id' :   li_obj_id,
                'li_obj_name'   :   li_obj_name,
                'li_obj_label'  :   li_obj_label,
                'li_link_type'  :   li_link_type,
                'icon'      :   li_icon,
                'right_align_dropdown'  :   li_align_right,
                'addition_class'        :   li_additional_class,
                'children'              :   {}
             }
             
             if ( thisLi.find('> ul.tsm-list').length ) {
                var this_dropdown_child = thisLi.find('> ul.tsm-list');
                dropdown_list_data['items_data'][li_num]['children'] = tsm_get_dropdown_list_data_recursive(this_dropdown_child);
             }
             li_num++;
        });
         
        return dropdown_list_data;      
    }
    
    // Get menu theme params for saving (moderm version)
    function tsm_get_menu_theme_params_for_save() {
        var settingsForm = $('#tsm-mega-menu-settings-form');
        var menu_theme_key = '';
        var menu_theme_params = {};
        var menu_theme_data = {
            'theme_key'     : menu_theme_key,
            'theme_params'  : menu_theme_params
        };
        
        if ( settingsForm.find('#tsm-theme-select').length ){
            menu_theme_key = settingsForm.find('#tsm-theme-select').val();
            menu_theme_data['theme_key'] = menu_theme_key;
        }
        
        if ( menu_theme_key != '' && settingsForm.find('.tsm-theme-settings').length ) {
            var i = 0;
            var themeSettingsForm = settingsForm.find('.tsm-theme-settings');
            
            themeSettingsForm.find('.tsm-theme-param-wrap').each(function(){
                var param_key = $(this).attr('data-param-key');
                var param_type = $(this).attr('data-param-type');
                var param_val = ''; 
                
                if ( param_type == 'color'){
                    param_val = $(this).find('#tsm-' + param_key).attr('data-color');
                }
                
                if ( param_type == 'input_number' || param_type == 'select' ){
                    param_val = $(this).find('#tsm-' + param_key).val();
                }
                
                if ( param_type == 'input_number_table' ){
                    var param_val_args = {};
                    var j = 0;
                    $(this).find('.tsm-param').each(function(){
                         param_val_args[j] = $(this).val();
                         j++;
                    });
                    param_val = param_val_args; //JSON.stringify(param_val_args);
                    //alert(JSON.stringify(param_val));
                }
                
                menu_theme_params[param_key] = {
                    'val'   : param_val
                };
                
                i++;  // Will be removed because we dont need it
            });
            
            menu_theme_data['theme_params'] = menu_theme_params;
            
        }
        
        return menu_theme_data;
        
    }
    
    function tsm_hex_to_RGB(hex){
		hex = parseInt(hex.replace('#',''),16);
		return [hex >> 16, hex >> 8 & 0xFF, hex & 0xFF];
	}
    
    
    // Get menu css color scheme for saving (old version)
    function tsm_get_menu_css_color_scheme_data_for_save(){
        var template = $('#chooseTemplate').val();
        var css_arr_key = template.replace(/-/g, '_');
        var cur_temp_name = $('#chooseTemplate option:selected').text(); 
        var new_temp_name = tsm_script_localize.text.this_menu_css_temp;
        
        if ( new_temp_name != null ){
            var css_base = tsm_fixed_encodeURIComponent( $('#tsm-mega-menu-settings-form #getCss > textarea').text() );
            var tmp_colors = {};
            var i = 0;
            $('#changeTemplateColors .' + template + ' input.color').each(function(){
                
                var data_def = $(this).val();
                var data_lighten = '';
                var data_darken = '';
                var data_from = '';
                var input_type = '';
                if( $(this).attr('data-lighten') ){
                    data_lighten = $(this).attr('data-lighten');
                } 
                if( $(this).attr('data-darken') ){
                    data_darken = $(this).attr('data-darken');
                }
                if( $(this).attr('data-from') ){
                    data_from = $(this).attr('data-from');
                }  
                if( $(this).attr('type') ){
                    input_type = $(this).attr('type');
                }     
                var this_input_data = {
                    def         :   data_def,
                    lighten     :   data_lighten,
                    darken      :   data_darken,
                    from        :   data_from,
                    type        :   input_type
                };   
                tmp_colors[i] = this_input_data;
                i++;       
            });
            var new_color_scheme = $('#changeTemplateColors .' + template + ' input.color.main').val();
            
            var menu_css_data = {
                new_color_scheme:   new_color_scheme,
                tmp_colors      :   tmp_colors,
                template        :   template,
                css_arr_key     :   css_arr_key,
                new_temp_name   :   new_temp_name,
                css_base        :   css_base
            }  
            
            return menu_css_data;
        }
        
        return false;
    }
    
    
// ********************************************************************************************************`//
//                                        END SAVE MEGA MENU                                                //
// ******************************************************************************************************** //

    // Get all fonts need load (preview and frontend)
    function tsm_get_all_fonts_needed(){
        var fonts_str = '';
        var last_char = fonts_str.substr(fonts_str.length - 1, 1);
        if ( last_char != ',' && fonts_str != '' ){
            fonts_str += ',';
        }
        
        $('.tsm-google-font-select').each(function(){
             var $this = $(this);
             $this.find('option:selected').map(function(){
                var this_val = $.trim(this.value);
                if ( this_val != '' && fonts_str.indexOf(this_val) < 0 ){
                    fonts_str += this_val + ',';
                }
            });
        });
        last_char = fonts_str.substr(fonts_str.length - 1, 1);
        if ( last_char == ',' ){
            fonts_str = fonts_str.substring(0, fonts_str.length - 1);
        }
        
        return fonts_str;
    }

    
    // Load menu theme settings via ajax
    $(document).on('change', '#tsm-theme-select', function(e){
       
        var $this = $(this);
        var settingsForm = $this.closest('#tsm-mega-menu-settings-form');
        
        if ( tsm_is_locked( settingsForm ) ){
            return false;
        }
        
        var theme_key = $this.val();
        
        if ( theme_key == '' ) {
            settingsForm.find('.tsm-theme-settings-wrap').html('');
            return false;
        }
        
        tsm_lock_form( settingsForm );
        settingsForm.find('select').prop('disabled', 'disabled');
        tsm_update_chosen();
        
         // Load elem form via ajax
        var data = {
            action          :   'tsm_load_theme_settings_via_ajax',
            theme_key       :   theme_key
        }
        
        $.post(ajaxurl, data, function(response){
            settingsForm.find('.tsm-theme-settings-wrap').html(response);
            settingsForm.find('select').prop('disabled', false);
            tsm_update_chosen();
            tsm_update_color_picker();
            
            tsm_unlock_form(settingsForm);
        });
        
       
        e.preventDefault(); 
    });
    
    
    // Show icon choser class
    $(document).on('click', '.tsm-menu-item-options-wrap .tsm-choose-icon-label, .tsm-choose-li-icon-label', function(e){
        
        var thisIconFormId = $(this).attr('for');
        var thisIconForm = $('#' + thisIconFormId);
        
        if ( !thisIconForm.hasClass('tsm-has-been-loaded') ) {
            var listIconsHtml = '';
            if ($(this).hasClass('tsm-choose-li-icon-label')) {
                listIconsHtml = $('#tsm-icons-chooser-hidden-class').html();
            }
            else{
                listIconsHtml = $('#tsm-icons-chooser').html();
            }
            thisIconForm.html(listIconsHtml);
            thisIconForm.addClass('tsm-has-been-loaded');
        }
        
        thisIconForm.slideToggle(300);
        
        
        e.preventDefault();
    });
    
    // Change theme color scheme
    $(document).on('click', '.tsm-theme-settings-wrap .tsm-color-scheme-choser', function(e){
        var data_base_color_json = $(this).attr('data-base-color');
        
        if ( typeof data_base_color_json == 'undefined' && typeof data_base_color_json == false ){
            return false;
        }
        
        var data_base_color = JSON.parse(data_base_color_json);
        
        for (var base_color_key in data_base_color){
            if (typeof data_base_color[base_color_key] !== 'function') {
                $('.tsm-theme-settings-wrap .tsm-param-color[data-base-color-key="' + base_color_key + '"]').spectrum('set', data_base_color[base_color_key]);
                $('.tsm-theme-settings-wrap .tsm-param-color[data-base-color-key="' + base_color_key + '"]').attr('data-color', data_base_color[base_color_key]).val(data_base_color[base_color_key]);
            }
        }
        
        e.preventDefault();
    });
    
    // Toggle advance theme style options
    $(document).on('click', '.tsm-show-adv', function(e){
        
        var toggle_target = $($(this).attr('data-target'));
        
        if ( !toggle_target.length ){
            return false;
        }
        
        toggle_target.slideToggle(500);
        
        e.preventDefault(); 
    });
    
    // Disabled (for fixed delay)
    //tsm_update_live_preview_base_html();
    function tsm_update_live_preview_base_html(){
        $('#wpwrap').after(tsm_script_localize.html.live_preview_base_html);  
    }
    
    // Show/Hide live css preview
    $("a.tsm-show-hide-live-css-preview").on("click", function(e){
        e.preventDefault();
        var hrefval = $(this).attr("href");
        
        if(hrefval == "#tsm-live-preview-wrap") {
            var distance = $('#wpwrap').css('right');
            
            if(distance == "auto" || distance == "0px") {
                $(this).addClass("open");
                $('body,html').animate({
    				scrollTop: 0
    			}, 800, function(){
                    $('#tsm-live-css-control .tsm-control-section').each(function(){
                       //tsm_update_live_css_input_vals_and_ui($(this));
                       tsm_update_live_css_editor_inherit_val($(this)); 
                    });
                    tsm_update_live_css_preview();
    			});
                tsm_open_sidepage();
            } else {
                tsm_close_sidepage();
            }
        }
    }); // end click event handler
    
    $('.tsm-close-preview').on('click', function(e){
        e.preventDefault();
        tsm_close_sidepage();
    });
    
    $(document).on('click', '#tsm-live-preview-header .tsm-show-hide-control', function(e){
        
        $('#tsm-live-css-control').toggleClass('tsm-width-0');
        if ( $('#tsm-live-css-control').hasClass('tsm-width-0') ){
            $(this).html('<i class="fa fa-arrow-circle-left"></i>' + tsm_script_localize.text.show_accordion);
            $('#tsm-live-preview').removeClass('col-sm-7 col-md-8');
        }
        else{
            $(this).html('<i class="fa fa-arrow-circle-right"></i>' + tsm_script_localize.text.hidden_accordion);
            $('#tsm-live-preview').addClass('col-sm-7 col-md-8');
        }
        
        e.preventDefault(); 
    });
    
    function tsm_open_sidepage() {
        //$('#tsm-live-preview-wrap').show(0);
        $('#wpwrap').animate({
          right: '100%'
        }, 1000, 'easeOutBack'); 
    }
    
    function tsm_close_sidepage(){
        $("a.tsm-show-hide-live-css-preview").removeClass("open");
        //$('#tsm-live-preview-wrap').hide(0);
        $('#wpwrap').animate({
          right: '0px'
        }, 1000, 'easeOutQuint');  
    }
    
    // BEGIN LIVE EDITOR =============================================================
    
    // Disabled (for fixed delay)
    //tsm_load_live_editor();
    function tsm_load_live_editor(){
        
        tsm_show_status_info( tsm_script_localize.text.loading_preview_menu + '. ' + tsm_script_localize.text.loading_live_css_editor );
        
        var menu_id = $('#tsm-mega-menu-settings-form').attr('data-selected-menu-id');
        var preview = $('#tsm-live-preview iframe');
        var preview_url = ajaxurl + '?action=tsm_menu_preview_via_ajax&menu_id=' + menu_id;
        
        preview.attr('src', preview_url);
        
        // Load preview content form via ajax
        var data = {
            action          :   'tsm_live_css_editor_control_via_ajax',
            nonce           :   tsm_nonce.ajax_nonce,
            menu_id         :   menu_id
        }
        
        $.post(ajaxurl, data, function(response){
            $('#tsm-live-preview-wrap #tsm-live-css-control').html(response);
            tsm_update_accordion();
            
            tsm_hide_status_info();
            
        });
        
    }
    
    // Reload menu preview in iframe
    $(document).on('click', '.tsm-reload-preview-iframe', function(e){
       
        var menu_id = $('#tsm-mega-menu-settings-form').attr('data-selected-menu-id');
        var preview = $('#tsm-live-preview iframe');
        var preview_url = ajaxurl + '?action=tsm_menu_preview_via_ajax&menu_id=' + menu_id;
        
        $('.tsm-reload-preview-iframe i').addClass('fa-spin');
        
        preview.attr('src', preview_url);
       
        e.preventDefault(); 
    });
    
    // When preview load, update live css editor control
    tsm_update_live_css_editor_control();
    function tsm_update_live_css_editor_control(){
        
        var preview = $('#tsm-live-preview iframe');
        preview.load(function() {
            tsm_live_css_editor_control_update_count++;
            if ( tsm_live_css_editor_control_update_count <= 1 ){ // If is the first time update (on load)
                $('#tsm-live-css-control .tsm-live-editor-control-accordion .tsm-props-select').each(function(){
                    tsm_update_control_section_prop_vals($(this)); 
                });   
                
                tsm_update_chosen_elem($('#tsm-live-css-control .tsm-select2'));
                
                tsm_update_color_picker_in_wrap($('#tsm-live-css-control .tsm-live-css-control-form-wrap'));
                tsm_refresh_accordion();
                
            }
            else{
                tsm_update_live_css_preview();
            }
            
            var live_preview_wrap_h = $('#tsm-live-preview-wrap').height();
            var header_h = $('#tsm-live-preview-header').height();
            var live_preview_h = live_preview_wrap_h - header_h - 15;
            $('#tsm-live-preview').height(live_preview_h);  
            
            $('.tsm-reload-preview-iframe i').removeClass('fa-spin');
        });
    }
    
    $(document).on('change', '#tsm-live-css-control .tsm-live-editor-control-accordion .tsm-props-select', function(e){
        var $this = $(this);
        tsm_update_control_section_prop_vals($this);
        
        e.preventDefault(); 
    });
    
    function tsm_update_control_section_prop_vals($elem){
        
        var thisAccordion = $elem.closest('.tsm-live-editor-control-accordion');
        var thisControlSection = $elem.closest('.tsm-control-section');
        var this_selector = thisControlSection.attr('data-selector');
        var preview = $('#tsm-live-preview iframe');
        
        if ( !preview.contents().find('ul.ts-megamenu').length ){
            //alert(tsm_script_localize.text.wait_for_preview_loaded);
            $('.tsm-satus-info-wrap').html(tsm_script_localize.text.wait_for_preview_loaded).css({opacity: 1.0, visibility: "visible"}).animate({opacity: 0}, 1500);
            return false;
        }
        
        thisControlSection.find('.tsm-live-css-control-form-wrap').html('');
        
        var selectors_not = [];
        var props_need_add = []; 
        var props_need_remove = []; 
        
        $elem.find('option:selected').map(function(){
            var prop = this.value;
            //all_props_needed.push(prop);
            selectors_not.push('.tsm-live-css-control-section-form[data-prop="' + prop +'"]');
            if ( !thisControlSection.find('.tsm-live-css-control-section-form[data-prop="' + prop +'"]').length ){
                props_need_add.push(prop);
                var inherit_selector = $.trim($(this).attr('data-inherit-selector'));
                thisControlSection.find('.tsm-live-css-control-form-wrap').prepend(tsm_script_localize.html.live_css_control_section_forms[prop]);
                if ( inherit_selector != '' ){
                    thisControlSection.find('.tsm-prop-val').attr('data-inherit-selector', inherit_selector);
                }
            }
        });
        
        if ( selectors_not.length ){
            selectors_not = selectors_not.join(',');
            thisControlSection.find('.tsm-live-css-control-section-form').not(selectors_not).remove();
        }
        
        
        //$elem.find('option:selected').map(function(){
//            var prop = this.value;
//            var inherit_selector = $.trim($(this).attr('data-inherit-selector'));
//            thisControlSection.find('.tsm-live-css-control-form-wrap').prepend(tsm_script_localize.html.live_css_control_section_forms[prop]);
//            if ( inherit_selector != '' ){
//                thisControlSection.find('.tsm-prop-val').attr('data-inherit-selector', inherit_selector);
//            }
//        });
        // Set current css value
        tsm_update_live_css_input_vals_and_ui(thisControlSection, tsm_live_css_editor_control_update_count > 1);
    }
    
    $(document).on('change', '#tsm-live-css-control .tsm-prop-val, #tsm-live-css-control .tsm-props-select', function(){
        if ( $(this).hasClass('tsm-prop-val') ){
            var inherit_selector = $(this).attr('data-inherit-selector');
            if ( typeof inherit_selector != 'undefined' && typeof inherit_selector != false ){
                var data_type = $(this).attr('data-prop-type');
                if (data_type == 'color'){
                    $(inherit_selector).spectrum('set', $(this).attr('data-color'));
                    $(inherit_selector).val($(this).val()).attr('data-color', $(this).val());   
                }
                else{
                    $(inherit_selector).val($(this).val());
                }
            }
        }
        $('#tsm-live-css-control .tsm-update-live-css-preview-btn').removeClass('tsm-updated');
        //tsm_update_live_css_preview();
    });
    
    $(document).on('click', '#tsm-live-css-control .tsm-update-live-css-preview-btn', function(e){
        $('#tsm-live-css-control .tsm-update-live-css-preview-btn i').removeClass('fa-tick').addClass('fa-refresh fa-spin');
        tsm_update_live_css_preview();
        e.preventDefault(); 
    });
    
    // Update inherit val (hover)
    function tsm_update_live_css_editor_inherit_val( controlSection ){
        var preview = $('#tsm-live-preview iframe');
        var this_selector = controlSection.attr('data-selector');
        controlSection.find('.tsm-prop-val').each(function(){
            var $this = $(this);
            var prop = $this.closest('.tsm-live-css-control-section-form').attr('data-prop');
            var prop_type = $this.attr('data-prop-type');
            
            var inherit_selector = $this.attr('data-inherit-selector');
            if ( typeof inherit_selector != 'undefined' && typeof inherit_selector != false ){
                
                switch (prop_type){
                    case 'color':
                        var prop_val = ''; 
                        prop_val = $(inherit_selector).attr('data-color');
                        $this.attr('data-color', prop_val).val(prop_val);
                        $this.spectrum('set', prop_val);
                        break;
                }
                
            }
        });
    }
    
    // Update live css input values and UI
    function tsm_update_live_css_input_vals_and_ui( controlSection, updateUi ){
        
        tsm_show_status_info( tsm_script_localize.text.updating_live_preview_val_ui );
        
        var preview = $('#tsm-live-preview iframe');
        
        var this_selector = controlSection.attr('data-selector');
        
        if ( this_selector.indexOf("li.tsm-opened") > -1 || this_selector.indexOf("li.tsm-active") > -1 || this_selector.indexOf("li:hover") > -1 ){
            var selector_tmp = this_selector.replace(/(.tsm-opened|.tsm-active|:hover)/gi, '');
            //this_selector = this_selector.replace(/(.tsm-opened|.tsm-active|:hover)/gi, '');
            preview.contents().find(selector_tmp).addClass('tsm-opened tsm-active');
        }
        
        if ( this_selector.indexOf("a.tsm-link-hover") > -1 || this_selector.indexOf("a:hover") > -1 ){
            var selector_tmp = this_selector.replace(/(.tsm-link-hover|:hover)/gi, '');
            //this_selector = this_selector.replace(/(.tsm-link-hover|:hover)/gi, '');
            preview.contents().find(selector_tmp).addClass('tsm-link-hover');
        }
        
        controlSection.find('.tsm-live-css-control-section-form.tsm-need-refresh .tsm-prop-val').each(function(){
            var $this = $(this);
            var prop = $this.closest('.tsm-live-css-control-section-form').attr('data-prop');
            var prop_type = $this.attr('data-prop-type');
            switch (prop_type){
                case 'color':
                    var prop_val = ''; //preview.contents().find(this_selector).css(prop);
                    
                    var inherit_selector = $this.attr('data-inherit-selector');
                    if ( typeof inherit_selector != 'undefined' && typeof inherit_selector != false ){
                        prop_val = $(inherit_selector).attr('data-color');
                    }
                    else{
                        prop_val = preview.contents().find(this_selector).css(prop);
                    }
                    tsm_update_color_picker_elem($this);
                    $this.attr('data-color', prop_val).val(prop_val);
                    break;
                case 'padding': case 'margin': 
                    var prop_pos = $this.attr('data-pos');
                    var prop_val = preview.contents().find(this_selector).css(prop + '-' + prop_pos);
                    $this.val(prop_val);
                    break;
                case 'border-style': case 'border-color': case 'border-width':
                    var this_border_attr = prop_type.replace('border-', '');
                    prop_val = preview.contents().find(this_selector).css(prop + '-' + this_border_attr);
                    if ( this_border_attr == 'color' ){
                        $this.spectrum('set', prop_val);
                        $this.attr('data-color', prop_val).val(prop_val);
                    }
                    else{
                        $this.val(prop_val);   
                    }
                    break;
                default:
                    prop_val = preview.contents().find(this_selector).css(prop);
                    $this.val(prop_val);
                    break;
            }
            $this.closest('.tsm-live-css-control-section-form').addClass('tsm-has-been-updated');
        });
        
        controlSection.find('.tsm-live-css-control-section-form .tsm-has-been-updated').removeClass('tsm-has-been-updated tsm-need-refresh');
        
        preview.contents().find('.ts-megamenu *').removeClass('tsm-opened tsm-active tsm-link-hover');
        
        //if ( this_selector.indexOf("li.tsm-opened") > -1 || this_selector.indexOf("li.tsm-active") > -1 || this_selector.indexOf("li:hover") > -1 ){
//            preview.contents().find(this_selector).removeClass('tsm-opened tsm-active');
//        }
//        
//        if ( this_selector.indexOf("a.tsm-link-hover") > -1 || this_selector.indexOf("a:hover") > -1 ){
//            preview.contents().find(this_selector).removeClass('tsm-link-hover');
//        }
        
        if ( updateUi === true ) {
            tsm_update_chosen_elem(controlSection.find('.tsm-select2'));
            
            tsm_update_color_picker_in_wrap(controlSection.find('.tsm-live-css-control-form-wrap'));
            tsm_refresh_accordion();   
        }
        
        tsm_hide_status_info();
    }
    
    // Update live css preview
    function tsm_update_live_css_preview() {
        
        tsm_show_status_info( tsm_script_localize.text.updating_live_preview_css );
        
        var preview = $('#tsm-live-preview iframe');
        var css = tsm_get_live_css();
        if (preview.contents().find('head #tsm-preview-style').length){
            preview.contents().find('head #tsm-preview-style').html(css);
        }
        else{
            preview.contents().find('head').append('<style id="tsm-preview-style" type="text/css">' + css + '</style>');
        }
        
        // Load preview css via ajax
        var menu_id = $('#tsm-mega-menu-settings-form').attr('data-selected-menu-id');
        var menu_theme_params = tsm_get_menu_theme_params_for_save();
        var breakpoint = $('#tsm-break-point-input').val();   
        var fonts = tsm_get_all_fonts_needed();     
        
        // Load elem form via ajax
        var data = {
            action              :   'tsm_load_css_for_preview_via_ajax',
            nonce               :   tsm_nonce.ajax_nonce,
            menu_id             :   menu_id,
            menu_theme_params   :   menu_theme_params,
            breakpoint          :   breakpoint,
            fonts               :   fonts
        }
        
        $.post(ajaxurl, data, function(response){
            
            response = JSON.parse( response );
            
            tsm_show_status_info( tsm_script_localize.text.updating_live_preview_css );
            
            if ( preview.contents().find('#tsm-megamenu-css').length ){
                preview.contents().find('#tsm-megamenu-css').remove();
            }
            
            if (preview.contents().find('head #tsm-preview-css-via-ajax').length){
                preview.contents().find('head #tsm-preview-css-via-ajax').html(response.preview_css);
            }
            else{
                preview.contents().find('head').prepend('<style id="tsm-preview-css-via-ajax" type="text/css">' + response.preview_css + '</style>');
            }
            
            if (preview.contents().find('head #tsm-google-fonts-css').length){
                preview.contents().find('head #tsm-google-fonts-css').attr('href', response.google_font_url);
            }
            else{
                preview.contents().find('head').prepend("<link rel='stylesheet' id='tsm-google-fonts-css'  href='" + response.google_font_url + "' type='text/css' media='all' />");
            }
            
            tsm_hide_status_info(); 
            $('#tsm-live-css-control .tsm-update-live-css-preview-btn').addClass('tsm-updated').find('i').addClass('fa-tick').removeClass('fa-refresh fa-spin');;
        });
    }
    
    // Get css from live css editor
    function tsm_get_live_css() {
        var css = '';
        var preview = $('#tsm-live-preview iframe');
        var cssControlForm = $('#tsm-live-css-control');
        cssControlForm.find('.tsm-control-section').each(function(){ // Each selector
            var $this = $(this);
            if ($this.find('.tsm-prop-val').length){
                var selector = $this.attr('data-selector');
                css += selector + '{';
                $this.find('.tsm-live-css-control-section-form').each(function(){ // Each css prop
                    var prop = $(this).attr('data-prop');
                    var prop_val = '';
                    $(this).find('.tsm-prop-val').each(function(){
                        var prop_type = $(this).attr('data-prop-type');
                        if ( typeof prop_type != 'undefined' && typeof prop_type != false ){
                            if (prop_type == 'color' || prop_type == 'border-color'){
                                prop_val += $(this).attr('data-color') + ' '; 
                            }
                            else{
                                prop_val += $(this).val() + ' '; 
                            }
                        }
                        else{
                            prop_val += $(this).val() + ' ';   
                        }
                    });
                    css += prop + ': ' + prop_val + '; ';
                });
                css += '} ';
            }
        });
        return css;
    }
    
    function tsm_get_live_css_editor_settings(){
        var settings = {};
        
        $('#tsm-live-css-control .tsm-control-section').each(function(){
            var thisSection = $(this);
            var section_key = thisSection.attr('data-section-key');
            var avai_props = {};
            
            
            thisSection.find('select.tsm-props-select option').map(function(){
                if ($(this).is(':selected')){
                    //alert(this.value);
                    avai_props[this.value] = {
                        'selected'  :   true
                    };
                }
                else{
                    avai_props[this.value] = {
                        'selected'  :   false
                    };
                }
            });  
            settings[section_key] = {
                'avai_props'    :   avai_props
            };
        });
        
        return settings;
    }
    
    
    // END LIVE EDITOR ===============================================================
    
    // Icon list chooser paging
    $(document).on('click', '.tsm-icons-list-chooser-wrap .tsm-icons-list-goto-page', function(){
         
        var $this = $(this);
        var thisForm = $this.closest('.tsm-menu-item-icon');
        
        if (tsm_is_locked(thisForm)) {
            return false;
        }
        
        
        if (!$this.hasClass('tsm-cur-page')){
            var data_page = $this.attr('data-page');
            var data_limit = $this.attr('data-limit');
            var data_show_class = $this.attr('data-show-class');
            
            var data = {
                action:             'tsm_load_icons_list_page_via_ajax',
                data_page:          data_page,
                data_limit:         data_limit,
                data_show_class:    data_show_class
            }
            
            $this.find('span').prepend('<i class="fa fa-refresh fa-spin"></i>');
            tsm_locked(thisForm);
            
            $.post(ajaxurl, data, function(response){
                
                thisForm.html(response);
                tsm_unlock(thisForm);
                
            });
            
        }
         
    });
    
    // Choose icon
    $(document).on('click', '.tsm-icons-list-chooser-wrap .tsm-icon', function(e){
        
        var thisForm = $(this).closest('.tsm-menu-item-icon');
        var thisFormId = thisForm.attr('id');
        var iconClass = $(this).find('i').attr('class');
        $('label.tsm-choose-icon-label[for=' + thisFormId + '] .tsm-icon-chosen').attr('data-icon-class', iconClass).html('<i class="' + iconClass + '"></i>');
        tsm_update_current_editing_li();
        
        thisForm.slideUp(300);
        
        e.preventDefault(); 
    });
    
    // Remove icon
    $(document).on('click', '.tsm-icons-list-chooser-wrap .tsm-remove-icon', function(e){
        
        var thisForm = $(this).closest('.tsm-menu-item-icon');
        var thisFormId = thisForm.attr('id');        
        $('label.tsm-choose-icon-label[for=' + thisFormId + '] .tsm-icon-chosen').attr('data-icon-class', '').html('<i>' + tsm_script_localize.text.no_icon + '</i>');
        tsm_update_current_editing_li();
        
        thisForm.slideUp(300);
        
        e.preventDefault(); 
    });
    
    
    // Change a menu item mega type
    $(document).on('click', '.tsm-megamenu-form-wrap .tsm-mega-menu-type-btn', function(e){
        
        var thisFormWrap = $(this).closest('.tsm-megamenu-form-wrap');
        var curType = $(this).attr('data-mega-menu-type');
        
        switch (curType){
            case 'grid':
                thisFormWrap.find('.tsm-megamenu-form').css({'display': 'none'});
                thisFormWrap.find('.tsm-megamenu-form-dropdown').css({'display': 'block'});
                $(this).attr('data-mega-menu-type', 'dropdown');
                $(this).html('<i class="fa fa-exchange"></i>' + tsm_script_localize.text.use_grid);
                break;
            case 'dropdown':
                thisFormWrap.find('.tsm-megamenu-form').css({'display': 'block'});
                thisFormWrap.find('.tsm-megamenu-form-dropdown').css({'display': 'none'});
                $(this).attr('data-mega-menu-type', 'grid');
                $(this).html('<i class="fa fa-exchange"></i>' + tsm_script_localize.text.use_dropdown);
                break;
        } 
        
        e.preventDefault(); 
    });
    
    
    // Drag & drop element
    $('.tsm-tab-content .tsm-elem').draggable({
        connectToSortable: '.tsm-grid .tsm-col .tsm-elems-holder',
        helper: "clone",
        revert: false,
        stop: function( event, ui ) {
            tsm_update_elem_index();
        }
    });
    
    // Update sortable
    tsm_update_sortable();
    function tsm_update_sortable() {
        
        // Dropdown grid sortable
        $('.tsm-dropdown-grid').sortable({
            items: '> .tsm-list-lv-0'
        });
        
        // Items of each list
        $('.tsm-dropdown-grid .tsm-list').each(function(){
            $(this). sortable({
                items: '> li.tsm-li' 
            });
        });
        
        
        // Row sortable
        $('.tsm-grid').sortable({
            items: '.tsm-row'
        });
        
        $('.tsm-grid .tsm-col .tsm-elems-holder').sortable({
            items: '.tsm-elem',
            connectWith: '.tsm-grid .tsm-col .tsm-elems-holder'
        });
        
        $('.tsm-grid .tsm-row .tsm-cols-holder').sortable({
            items: '.tsm-col',
            connectWith: '.tsm-grid .tsm-row .tsm-cols-holder',
            sort: function( event, ui ) {
                //tsm_remove_clearfix();
                tsm_update_clearfix();
                //tsm_temp_hidden_elems_on_grid();
            },
            stop: function( event, ui ){
                tsm_update_clearfix();
                //tsm_temp_show_elems_on_grid();
            }
        });
    }
    
    // Hidden all elements (while needed)
    function tsm_temp_hidden_elems_on_grid() {
        $('.tsm-grid .tsm-elems-holder .tsm-elem').addClass('tsm-hidden');
    }
    
    // Show all elements
    function tsm_temp_show_elems_on_grid() {
        $('.tsm-grid .tsm-elems-holder .tsm-elem').removeClass('tsm-hidden');
    }
    
    // Remove clearfix
    function tsm_remove_clearfix() {
        $('.tsm-grid .tsm-cols-holder > .clearfix').remove();
    }
    
    // Update clearfix
    function tsm_update_clearfix() {
        
        // Remove clearfixs first
        tsm_remove_clearfix();
        
        
        $('.tsm-grid .tsm-row').each(function(){
            var $this = $(this);
            var thisGrid = $this.closest('.tsm-grid');
            var totalCol = $this.find('.tsm-col').length;
            
            if ( totalCol ) {
                totalCol = parseInt(totalCol);
                
                var sum_col_w = 0;
                $this.find('.tsm-col').each(function(){
                    // Except sorting elem...
                    if (!$(this).hasClass('ui-sortable-helper')){
                        var this_col_w = 0;
                        
                        if ($(this).hasClass('ui-sortable-placeholder')) {
                            this_col_w = parseInt(thisGrid.find('.tsm-col.ui-sortable-helper').attr('data-col-width'));
                        }
                        else{
                            this_col_w = parseInt($(this).attr('data-col-width'));
                        }
                        
                        sum_col_w += this_col_w;
                        
                        if (sum_col_w > 12){
                            sum_col_w = 0;
                            $(this).before('<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>');
                        }
                    }
                    
                });
            }
            
        });
    }
    
    // Update elem index 
    function tsm_update_elem_index() {
        var elem_index = 1;
        $('.tsm-grid .tsm-elem[data-elem-index="0"]').each(function(){
            while ($('.tsm-grid .tsm-elem[data-elem-index="' + elem_index + '"]').length){
                elem_index++;
            }  
            $(this).attr('data-elem-index', elem_index);
        });
    }
    
    // Reset elem index
    function tsm_reset_elem_index() {
        var elem_index = 1;
        $('.tsm-grid .tsm-elem').each(function(){
            $(this).attr('data-elem-index', elem_index);
            elem_index++;
        });
    }
    
    // Update Mega menu type (Grid or Dropdown)
    tsm_update_menu_type();
    function tsm_update_menu_type() {
        
        $('.tsm-megamenu-form-wrap').each(function(){
            
            var thisType = $(this).find('.tsm-mega-menu-type-btn').attr('data-mega-menu-type');
            
            switch ( thisType ){
                case 'grid':
                    $(this).find('.tsm-megamenu-form').css({'display': 'block'});
                    $(this).find('.tsm-megamenu-form-dropdown').css({'display': 'none'});
                    break;
                case 'dropdown':
                    $(this).find('.tsm-megamenu-form').css({'display': 'none'});
                    $(this).find('.tsm-megamenu-form-dropdown').css({'display': 'block'});
                    break;
            }
             
        });
        
    }
    
    // Show/Hidden min-max grid witdh
    $(document).on('change', '.tsm-grid .tsm-submenu-full-with', function(){
        var $this = $(this);
        var thisGrid = $this.closest('.tsm-grid');
        
        if ( tsm_is_locked( thisGrid ) ){
            return false;
        }
        
        var menu_item_id = $this.attr('data-menu-item-id');
        if ($this.is(':checked')){
            thisGrid.find('.tsm-min-max-width-wrap-' + menu_item_id + ', .tsm-grid-width' + menu_item_id).slideUp(300);
        }
        else{
            thisGrid.find('.tsm-min-max-width-wrap-' + menu_item_id + ', .tsm-grid-width' + menu_item_id).slideDown(300);
        }
        
    });
    
    // Slider rank
    function ts_init_slider_range() {
        $('.tsm-slider-range').each(function(){
            var $this = $(this);
            var min_val = parseInt($this.attr('data-min')); // range min
            var max_val = parseInt($this.attr('data-max')); // range max
            var val_min = parseInt($this.attr('data-val-min'));
            var val_max = parseInt($this.attr('data-val-max'));
            var amount_id = $this.attr('data-amount-id'); 
            
            if ( isNaN(min_val) ) {
                min_val = 0;
            }
            
            if ( isNaN(max_val) ) {
                max_val = 0;
            }
            
            if ( min_val > max_val ) {
                min_val = 0;
            }
            
            if ( isNaN(val_min) ) {
                val_min = 0;
            }
            
            if ( isNaN(val_max) ) {
                val_max = 0;
            }
            
            if ( val_min > val_max ) {
                val_min = 0;
            }
            
            $this.slider({
                range: true,
                min: min_val,
                max: max_val,
                values: [ val_min, val_max ],
                slide: function( event, ui ) {
                    if ( $('#' + amount_id).length ){
                        $('#' + amount_id).val( ui.values[ 0 ] + " " + ui.values[ 1 ] );
                        $this.attr('data-val-min', ui.values[ 0 ]);   
                        $this.attr('data-val-max', ui.values[ 1 ]);
                    }
                }
            }); 
            if ( $('#' + amount_id).length ){
                $('#' + amount_id).val( $this.slider( "values", 0 ) + " " + $this.slider( "values", 1 ) );
            }
            
        });   
    }
    ts_init_slider_range();
    
    // Fix can not click input in wordpress media frame content (Image details)
    $(document).on('click', '.tsm-elem-field-editor iframe #wp-image-toolbar .edit', function(e){
        
        if ( !$('.mfp-wrap').is(':hidden') ){
            $('.mfp-wrap').css({'display': 'none'});
        }
         
    });
    
    $(document).on('click', '.media-frame-content input[type=text], .media-frame-content select, .media-frame-content textarea', function(e){
        
        if ( !$('.mfp-wrap').is(':hidden') ){
            $('.mfp-wrap').css({'display': 'none'});
        }
        
        $(this).focus();
        
        e.preventDefault(); 
    });
    
    $(document).on('click', '.media-modal-backdrop, .media-modal-close, .media-modal-content .button.media-button-select', function(e){
        if ( $('.mfp-wrap').is(':hidden') ){
            $('.mfp-wrap').css({'display': 'block'});
        }
    });
    
    
    // Select 2 (chosen)
    tsm_update_chosen();
    function tsm_update_chosen() {
        
        $('.tsm-select2').each(function(){
            var this_w = $(this).attr('data-width');
            if ( typeof this_w === "undefined" || typeof this_w == false ){
                this_w = '100%';
            }
            $(this).chosen({
                allow_single_deselect: true,
                width: this_w
            }); 
        });
    }
    
    function tsm_update_chosen_elem($elem){
        if ($elem.length){
            
            $elem.each(function(){
                var this_w = $(this).attr('data-width');
                if ( typeof this_w === "undefined" || typeof this_w == false ){
                    this_w = '100%';
                }
                $(this).chosen({
                    allow_single_deselect: true,
                    width: this_w
                }); 
            }); 
        }
    }
    
    // Accordion
    tsm_update_accordion();
    function tsm_update_accordion() {
        
        if ( $('.tsm-accordion').length ){
            $('.tsm-accordion').accordion({
                collapsible: true,
                active: false,
                heightStyle: "content"
            }); 
        }
        
    }
    
    function tsm_refresh_accordion(){
        
        if ($('.tsm-accordion').length){
            $('.tsm-accordion').accordion( "refresh" );   
        }
        
    }
    
    // TSM Color picker
    // http://bgrins.github.io/spectrum/
    tsm_update_color_picker();
    function tsm_update_color_picker() {
        if ( $('.tsm-color-picker').length ){
            
            $('.tsm-color-picker').each(function(){
                var $this = $(this);
                var showInitial = $(this).attr('data-color') == '';
                $this.spectrum({
                    change: function(color){
                        if (color != null){
                            $this.attr('data-color', color.toRgbString());
                        }
                        else{
                            $this.attr('data-color', '');   
                        }
                    },
                    //move: function(color){
//                        if (color != null){
//                            $this.attr('data-color', color.toRgbString());
//                        }
//                        else{
//                            $this.attr('data-color', '');   
//                        }
//                    },
                    showInput: true,
                    showInitial: showInitial,
                    allowEmpty: true,
                    showAlpha: true,
                    showPalette: true,
                    clickoutFiresChange: true,
                    palette: [
                        ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
                        ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
                        ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
                        ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
                        ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
                        ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
                        ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
                        ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
                    ],
                    showButtons: false
                });    
            });   
        }
    }
    
    function tsm_update_color_picker_in_wrap(wrap) {
        if ( wrap.find('.tsm-color-picker').length ){
            
            wrap.find('.tsm-color-picker').each(function(){
                var $this = $(this);
                var showInitial = $(this).attr('data-color') == '';
                $this.spectrum({
                    change: function(color){
                        if (color != null){
                            $this.attr('data-color', color.toRgbString());
                        }
                        else{
                            $this.attr('data-color', '');   
                        }
                    },
                    //move: function(color){
//                        if (color != null){
//                            $this.attr('data-color', color.toRgbString());
//                        }
//                        else{
//                            $this.attr('data-color', '');   
//                        }
//                    },
                    showInput: true,
                    showInitial: showInitial,
                    allowEmpty: true,
                    showAlpha: true,
                    showPalette: true,
                    clickoutFiresChange: true,
                    palette: [
                        ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
                        ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
                        ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
                        ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
                        ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
                        ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
                        ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
                        ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
                    ],
                    showButtons: false
                });    
            });   
        }
    }
    
    function tsm_update_color_picker_elem($elem) {
        if ( $elem.length ){
            
            var showInitial = $elem.attr('data-color') == '';
            $elem.spectrum({
                move: function(color){
                    if (color != null){
                        $elem.attr('data-color', color.toRgbString());
                    }
                    else{
                        $elem.attr('data-color', '');   
                    }
                },
                showInput: true,
                showInitial: showInitial,
                allowEmpty: true,
                showAlpha: true,
                showPalette: true,
                clickoutFiresChange: true,
                palette: [
                    ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
                    ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
                    ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
                    ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
                    ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
                    ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
                    ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
                    ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
                ],
                showButtons: false
            });     
        }
    }
    
    // Show status info
    function tsm_show_status_info( status_info ) {
        $('.tsm-satus-info-wrap').html(status_info).removeClass('tsm-opacity-0');
    }
    
    // Hidden status info
    function tsm_hide_status_info() {
        $('.tsm-satus-info-wrap').addClass('tsm-opacity-0');//.animate({opacity: 0}, 500);
    }
    
    // Disable Move Up one, Down one, Under Kids, To the top ...
    $(document).on('click', '.field-move a', function(e){
        e.preventDefault();
        e.stopPropagation(); 
    });
    
    // WP Image Upload
    // Open wp media upload when click on media upload input
    $(document).on('click', '.tsm-wp-img-uploader', function(e){
        e.preventDefault();
        
        var $this = $(this);
        var targetUrl = $this.attr('data-target-url'); // Url field 
        var targetSrc = $this.attr('data-target-src'); // Preview image
        var targetId  = $this.attr('data-target-id');  // Attachment id
        
        //Button title
        var tsm_wp_img_uploader = wp.media.frames.file_frame = wp.media({
            title: tsm_script_localize.text.choose_img, 
            button: {
                text: tsm_script_localize.text.choose_img 
            },
            library:    {
                type: 'image'
            },
            multiple: false
        });
        //When a file is selected
        tsm_wp_img_uploader.on('select', function() {
            var attachment = tsm_wp_img_uploader.state().get('selection').first().toJSON();
            var img_preview_src = attachment.url;
            
            if ( attachment['sizes'].hasOwnProperty('medium') ){
                img_preview_src = attachment['sizes']['medium']['url'];
            }
            
            if (typeof targetUrl !== 'undefined' && targetUrl !== false) {
                $('#'+targetUrl).val(img_preview_src);
            }
            
            if (typeof targetSrc !== 'undefined' && targetSrc !== false) {
                $('#'+targetSrc).attr('src', img_preview_src);
            }
            
            if (typeof targetId !== 'undefined' && targetId !== false) {
                $('#'+targetId).val(attachment.id);
            }
            
        });
        //Open upload dialog
        tsm_wp_img_uploader.open();
    });
    
    // Init new wp editor
    function tsm_init_new_editor( editor_id ){
		if ( typeof tinyMCEPreInit.mceInit[editor_id] !== "undefined" ) {
			quicktags( { id : editor_id } );
			return;
		}

		var tsm_new_editor_obj = tsm_invisible_editor_obj;

		tinyMCEPreInit.mceInit[editor_id] = {
            elements                            :   editor_id,
            wpautop                             :   true,
            remove_linebreaks                   :   true,
            apply_source_formatting             :   false,
            theme_advanced_buttons1             :   "bold,italic,strikethrough,bullist,numlist,blockquote,justifyleft,justifycenter,justifyright,link,unlink,wp_more,spellchecker,fullscreen,wp_adv",
            theme_advanced_buttons2             :   "formatselect,underline,justifyfull,forecolor,pastetext,pasteword,removeformat,charmap,outdent,indent,undo,redo,wp_help",
            theme_advanced_buttons3             :   "",
            theme_advanced_buttons4             :   "",
            tabfocus_elements                   :   ":prev,:next",
            body_class                          :   editor_id,
            theme_advanced_resizing_use_cookie  :   true
        };
        
		tinyMCEPreInit.qtInit[editor_id] = {
            id          :   editor_id,
            buttons     :   "strong,em,link,block,del,ins,img,ul,ol,li,code,more,spell,close"
        };

		tinymce.extend( {}, tinyMCEPreInit.mceInit['tsm-invisible-editor'], tinyMCEPreInit.mceInit[editor_id] );
		quicktags( tinyMCEPreInit.qtInit[editor_id] );
	}
    
});