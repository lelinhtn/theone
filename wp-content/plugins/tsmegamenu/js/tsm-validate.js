
/** Create excerpts **/
function tsm_excerpts(txt, length, more_txt) {
    txt = txt.replace(/<\/?[^>]+>/gi, ''); //replace html tags
    txt = jQuery.trim(txt);  //trim whitespace
    if ( txt != '' && txt.length > length ){
        txt = txt.substring(0, length) + more_txt;
    }
    return txt;
}

/** Lock/unlock functions **/
    
function tsm_lock_form($elem) {
    $elem.addClass('tsm-locked').find('input, select, textarea, button, .button, .button-primary, .btn').addClass('tsm-locked');
}

function tsm_unlock_form($elem) {
    $elem.removeClass('tsm-locked').find('input, select, textarea, button, .button, .button-primary, .btn').removeClass('tsm-locked');
}

function tsm_locked($elem) {
    $elem.addClass('tsm-locked');
}

function tsm_unlock($elem) {
    $elem.removeClass('tsm-locked');
}

function tsm_is_locked($elem) {
    return $elem.hasClass('tsm-locked');
}

/** End Lock/unlock functions **/


// Validate required fields and empty fields
function tsm_validate_fields( $elemForm ) {
    
    var err = false;
    // Validate required fields
    $elemForm.find('.tsm-elem-field[data-field-required="yes"]').each(function(){
        var $this = jQuery(this);
        $this.find('input[type=text], input[type=hidden], select, textarea').each(function(){
            if (jQuery.trim(jQuery(this).val()) == '') {
                $this.addClass('tsm-error');
                err = true;
            } 
            else{
                $this.removeClass('tsm-error');
            }
        });
    });
    
    return err;
}

// remove all special characters
function tsm_remove_special_chars(txt){
    txt = txt.toLowerCase();
    txt = txt.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    txt = txt.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    txt = txt.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    txt = txt.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    txt = txt.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    txt = txt.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    txt = txt.replace(/đ/g, "d");
    txt = txt.replace(/[`~!@#$%^&*()|+\=?;:'",.<>\{\}\[\]\\\/]/gi, '_');
    return txt;
}

function tsm_fixed_encodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16);
    });
}


jQuery(document).ready(function($){
    "use strict";
    
    // No special char input
    $(document).on('change', '.tsm-no-special-chars-input', function(){
        var new_val = tsm_remove_special_chars($(this).val());
        $(this).val(new_val);
    });
    
    
    // Validate Url
    $(document).on('change', '.tsm-url-input', function(e){
        
        var regExp =/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
        var input_url = $(this).val();
        if (!regExp.test(input_url)){
            $(this).addClass('tsm-error');
        }else{
            $(this).removeClass('tsm-error');
        }
         
    });
    
    
    // Validate non-negative
    $(document).on('change', '.tsm-non-negative-input', function(){
        var minNum = $(this).attr('data-min');
        if (typeof minNum == 'undefined' || minNum == false || isNaN(parseInt(minNum))) {
            minNum = 0;
        }
        minNum = (minNum < 0) ? 0: minNum;
        var thisNum = $(this).val();
        if (isNaN(parseInt(thisNum)) || parseInt() <= 0){
            $(this).val(minNum);
        }
        else{
            $(this).val(parseInt(thisNum));
        } 
    });
    
    
    
});