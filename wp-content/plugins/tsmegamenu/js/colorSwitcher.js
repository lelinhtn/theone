jQuery(document).ready(function($){
    
    "use strict";
    
    var tmpStyleCont = $('#tmpStyleCont'),
		curTemplate = 'default',
		templates;
    
    if ( $('#chooseTemplate').length ){
        curTemplate = $('#chooseTemplate').val();
    }

	// http://bgrins.github.io/spectrum/
	function colorPickerStart() {
		$("#changeTemplateColors .color[type!=hidden]").spectrum({
			showAlpha: true,
			showInput: true,
			clickoutFiresChange: true,
			showButtons: false,
			change: function(color) {
				setColor($(this), color.toString());
				reloadCSS();
			}
		});

		var tmp = {};
		templates = $('#templates > div');
		templates.each(function() {
			var colors = {};
			$('#changeTemplateColors .'+$(this).attr('id')+' .color').each(function() {
				colors[$(this).attr('data-def')] = $(this).val();
			});
			tmp[$(this).attr('id')] = {
				text: $(this).text(),
				colors: colors
			};
		});
		templates = tmp; tmp = '';

		reloadCSS();
	}

	function reloadCSS() {
		var tmpStyleCont = $('#tmpStyleCont');
		if(!tmpStyleCont[0])
			tmpStyleCont = $('<style type="text/css" id="tmpStyleCont">').appendTo('head');
        
        /** Custom **/
        if (typeof templates[curTemplate] === 'undefined' || templates[curTemplate] === false){
            return '';
        }
        /** End Custom **/
        
		var styles = templates[curTemplate].text;

		// replace colors
		for(var k in templates[curTemplate].colors)
			styles = styles.split(k).join(templates[curTemplate].colors[k]);

		tmpStyleCont.html(styles);
		$('#getCss textarea').html(styles);
		return styles;
	}

	function setColor(obj, color) {
		obj.spectrum('set', color);
		$('#changeTemplateColors .'+curTemplate+' .color').each(function() {
			if($(this).attr('data-from') && $(this).attr('data-from') == obj.attr('data-def')) {
				var lessColor = $('#changeTemplateColors .'+curTemplate+' .color[data-def='+$(this).attr('data-from')+']').val();

				if($(this).attr('data-darken'))
					lessColor = lessDarken(lessColor, $(this).attr('data-darken'));

				if($(this).attr('data-lighten'))
					lessColor = lessLighten(lessColor, $(this).attr('data-lighten'));

				if($(this).attr('data-fade'))
					lessColor = lessFade(lessColor, $(this).attr('data-fade'));

				$(this).spectrum('set', lessColor);
				templates[curTemplate]['colors'][$(this).attr('data-def')] = lessColor;
			}
		});
		templates[curTemplate]['colors'][obj.attr('data-def')] = color;
	}

	// less color functions
	function hexToRGB(hex){
		hex = parseInt(hex.replace('#',''),16);
		return [hex >> 16, hex >> 8 & 0xFF, hex & 0xFF];
	}
	function lessColor(color) {
		return new less.tree.Color(hexToRGB(color), 1);
	}
	function lessAmount(val) {
		return new less.tree.Value(val);
	}
	function lessDarken(color, amount) {
		return less.tree.functions.darken(lessColor(color), lessAmount(amount)).toCSS();
	}
	function lessLighten(color, amount) {
		return less.tree.functions.lighten(lessColor(color), lessAmount(amount)).toCSS();
	}
	function lessFade(color, amount) {
		return less.tree.functions.fade(lessColor(color), lessAmount(amount)).toCSS();
	}


	// template change
	$(document).on('change', '#chooseTemplate', function() {
		curTemplate = $(this).val();
		$('#changeTemplateColors > div').each(function() {
			$(this).css('display', ($(this).hasClass(curTemplate)?'block':'none'));
		});
        /** Custom **/
        tsm_show_hidden_somthings_when_select_color_temp();
        /** End Custom **/
		reloadCSS();
	});

	// color schemes change
	$(document).on('change', '#predefined input[name=predefinedColor]', function() {
		var mainColor = $(this).attr('data-color');

		$('#changeTemplateColors .'+curTemplate+' .color').each(function() {
			if(!$(this).attr('data-from'))
				setColor($(this), $(this).hasClass('main')?mainColor:$(this).attr('data-def'));
		});
		reloadCSS();
	});

	// lighter
	$(document).on('click', '#light > span', function() {
		$('body #wpcontent').removeClass('l d').addClass($(this).attr('class'));
	});

	// css download
	$(document).on('click', '#getCss a.download', function() {
		var zip = new JSZip();
		zip.file("ts-megamenu.css", reloadCSS());
		$(this).attr("href", "data:application/zip;base64," + zip.generate());
	});
	$(document).on('click', '#getCss textarea', function() {
		$(this).select();
	});
    
    if ( !$('#light').is(':hidden') ){
        //$('body #wpcontent').addClass('d');   
    }
    
	// init
	colorPickerStart();
    
    /** ---- CUSTOMS ---- **/
    $(document).on('click', '.tsm-save-color-temp-as-btn', function(e){
        
        var $this = $(this);
        var template = curTemplate;
        var css_arr_key = template.replace(/-/g, '_');
        var cur_temp_name = $('#chooseTemplate option:selected').text(); 
        var new_temp_name = prompt( tsm_script_localize.text.new_temp_name, tsm_script_localize.text.custom + ' ' + cur_temp_name );
        
        if ( new_temp_name != null ){
            var css_base = tsm_fixed_encodeURIComponent( reloadCSS() );
            var tmp_colors = {};
            var i = 0;
            $('#changeTemplateColors .' + template + ' input.color').each(function(){
                
                var data_def = $(this).val();
                var data_lighten = '';
                var data_darken = '';
                var data_from = '';
                var input_type = '';
                if( $(this).attr('data-lighten') ){
                    data_lighten = $(this).attr('data-lighten');
                } 
                if( $(this).attr('data-darken') ){
                    data_darken = $(this).attr('data-darken');
                }
                if( $(this).attr('data-from') ){
                    data_from = $(this).attr('data-from');
                }  
                if( $(this).attr('type') ){
                    input_type = $(this).attr('type');
                }     
                var this_input_data = {
                    def         :   data_def,
                    lighten     :   data_lighten,
                    darken      :   data_darken,
                    from        :   data_from,
                    type        :   input_type
                };   
                tmp_colors[i] = this_input_data;
                i++;       
            });
            var new_color_scheme = $('#changeTemplateColors .' + template + ' input.color.main').val();
            
            var data = {
                action          :   'tsm_save_color_temp_as_via_ajax',
                nonce           :   tsm_nonce.ajax_nonce,
                new_color_scheme:   new_color_scheme,
                tmp_colors      :   tmp_colors,
                template        :   template,
                css_arr_key     :   css_arr_key,
                new_temp_name   :   new_temp_name,
                css_base        :   css_base
            }
            
            $this.find('i').removeClass('fa-floppy-o').addClass('fa-refresh fa-spin');
            //tsm_locked(thisForm);
            
            $.post(ajaxurl, data, function(response){
                
                $this.find('i').removeClass('fa-refresh fa-spin').addClass('fa-floppy-o');
                
            });
            
        }
        
        e.preventDefault();
    });
    
    
    $(document).on('click', '.tsm-save-color-temp-btn', function(e){
        
        var $this = $(this);
        var template = curTemplate;
        var css_arr_key = template.replace(/-/g, '_');
        var cur_temp_name = $('#chooseTemplate option:selected').text(); 
        var new_temp_name = prompt( tsm_script_localize.text.new_temp_name, cur_temp_name );
        
        if ( new_temp_name != null ){
            var css_base = tsm_fixed_encodeURIComponent( reloadCSS() );
            var tmp_colors = {};
            var i = 0;
            $('#changeTemplateColors .' + template + ' input.color').each(function(){
                
                var data_def = $(this).val();
                var data_lighten = '';
                var data_darken = '';
                var data_from = '';
                var input_type = '';
                if( $(this).attr('data-lighten') ){
                    data_lighten = $(this).attr('data-lighten');
                } 
                if( $(this).attr('data-darken') ){
                    data_darken = $(this).attr('data-darken');
                }
                if( $(this).attr('data-from') ){
                    data_from = $(this).attr('data-from');
                }  
                if( $(this).attr('type') ){
                    input_type = $(this).attr('type');
                }     
                var this_input_data = {
                    def         :   data_def,
                    lighten     :   data_lighten,
                    darken      :   data_darken,
                    from        :   data_from,
                    type        :   input_type
                };   
                tmp_colors[i] = this_input_data;
                i++;       
            });
            var new_color_scheme = $('#changeTemplateColors .' + template + ' input.color.main').val();
            
            var data = {
                action          :   'tsm_save_color_temp_via_ajax',
                nonce           :   tsm_nonce.ajax_nonce,
                new_color_scheme:   new_color_scheme,
                tmp_colors      :   tmp_colors,
                template        :   template,
                css_arr_key     :   css_arr_key,
                new_temp_name   :   new_temp_name,
                css_base        :   css_base
            }
            
            $this.find('i').removeClass('fa-floppy-o').addClass('fa-refresh fa-spin');
            //tsm_locked(thisForm);
            
            $.post(ajaxurl, data, function(response){
                
                $this.find('i').removeClass('fa-refresh fa-spin').addClass('fa-floppy-o');
                
            });
        }
    
        e.preventDefault();
    });
    
    $(document).on('click', '.tsm-del-color-temp-btn', function(e) {
        
        if ( tsm_is_locked( $('#cont') ) ){
            return false;
        }
        
        var $this = $(this);
        var c = confirm( tsm_script_localize.confirm.del_css_temp );
        
        if ( !c ){
            return false;
        }
        
        var template = curTemplate;
        var css_arr_key = template.replace(/-/g, '_');
        
        tsm_lock_form( $('#cont') );
        
        var data = {
            action          :   'tsm_del_color_temp_via_ajax',
            nonce           :   tsm_nonce.ajax_nonce,
            template        :   template,
            css_arr_key     :   css_arr_key
        }
        
        $this.find('i').removeClass('fa-times').addClass('fa-refresh fa-spin');
        
        $.post(ajaxurl, data, function(response){
            
            $this.find('i').removeClass('fa-refresh fa-spin').addClass('fa-times');
            tsm_unlock_form( $('#cont') );
            
        });
        
        
        e.preventDefault(); 
    });
    
    tsm_show_hidden_somthings_when_select_color_temp();
    // Show/Hidden save color temp button
    function tsm_show_hidden_somthings_when_select_color_temp(){
        $('.tsm-show-match-temp').css({'display': 'none'});
        $('.tsm-show-match-temp.tsm-show-match-temp-' + curTemplate).css({
            'display': 'inline-block'
        });
    }
});
