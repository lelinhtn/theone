jQuery(document).ready(function($){
    
    "use strict";
    
    $('.ts-megamenu > ul ul li').hover(function(){
        $(this).addClass('tsm-li-hover');
    }, function(){
        $(this).removeClass('tsm-li-hover');
    });
    
    $('.ts-megamenu a').hover(function(){
        $(this).addClass('tsm-link-hover');
    }, function(){
        $(this).removeClass('tsm-link-hover');
    });
    
});