/**
 * @name jQuery plugin Theme Studio Mega Menu 1.1
 * @author nK http://codecanyon.net/user/nKdev
 * @description Theme Studio Mega Menu - mega dropdown menu plugin.
 * 
 * @similar Pure CSS3 Theme Studio Mega Menu - http://codecanyon.net/item/ts-megamenu-css3-mega-drop-down-menu/7667949?ref=nKdev
 */
(function($) {
	var tsmId = 0,
		prefixes = {};

	// animation test
	$.extend($.support, {
	  animation: (function() {
    	if(window.opera && opera.toString() == "[object Opera]") {
    		return; }
	    var animation = false,
	      animationstring = 'animation',
	      keyframeprefix = '',
	      domPrefixes = 'Webkit Moz O ms Khtml'.split(' '),
	      pfx  = '',
	      elm = document.createElement('div');

	    if( elm.style.animationName !== undefined ) { animation = true; }    

	    if( animation === false ) {
	      for( var i = 0; i < domPrefixes.length; i++ ) {
	        if( elm.style[ domPrefixes[i] + 'AnimationName' ] !== undefined ) {
	          pfx = domPrefixes[ i ];
	          animationstring = pfx + 'Animation';
	          keyframeprefix = '-' + pfx.toLowerCase() + '-';
	          animation = true;
	          break;
	        }
	      }
	    }
	    return animation;
	  }())
	});

	// check meta tag
	if(!$('meta[name=viewport]')[0]) {
		$('head').append('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">');
	}

	$.fn.tsMegamenu = function(method) {
		var namespace = 'tsMegamenu';
		
		// plugin class that does all work
		window[namespace] = function(obj, settings) {
			var base = this;

			base.id = tsmId;
			tsmId++;
			// default settings
			base.settings = {
				position: '', // left, right, bottom [other values - top position]
				fixed: false,
				sticky: false,
				fullWidth: true,
				responsive: 'switch', // simple, switch[-margin], stack[-margin]
				showOn: 'click', // hover, click, toggle

        /* Effects list:
          bounce
          bounceIn
          bounceInDown
          bounceInLeft
          bounceInRight
          bounceInRight

          fade
          flash
          lightSpeedIn
          pulse
          rollIn
          rubberBand
          shake
          swing
          tada
          wobble

          flip
          flipInX
          flipInY

          rotateIn
          rotateInDownLeft
          rotateInDownRight
          rotateInUpLeft
          rotateInUpRight

          slideDown
          slideDownBig
          slideDownSmall
          slideLeft
          slideLeftBig
          slideLeftSmall
          slideRight
          slideRightBig
          slideRightSmall
          slideUp
          slideUpBig
          slideUpSmall

          zoomIn
          zoomInDown
          zoomInLeft
          zoomInRight
          zoomInUp
        */
				showEffect: {
					name: 'zoomIn',
					speed: 500, // animation speed (ms)
					delay: 200 // delay between animation (ms)
				},
        hideEffect: {
          name: 'fade',
          speed: 500, // animation speed (ms)
          delay: 200 // delay between animation (ms)
        }
			}
            
            var curShowOn = base.settings.showOn; // Le Manh Linh
			
			// extending with settings parameter
			if (settings) {
				for(var k in settings) {
					if(typeof settings[k] == 'object') {
						$.extend(base.settings[k], settings[k]);
					} else {
						base.settings[k] = settings[k];
					}
				}
                curShowOn = base.settings.showOn;
			}

			base.obj = obj; // actual DOM element
			base.$obj = $(obj); // jQuery version of DOM element
      base.$divDrop = $('li > ul, li > div', base.$obj);
			base.$liDrop = base.$divDrop.parent().find('> a'); // li with drop children

			// init method
			var _init = function () {
				// replace special tsm tags
				$('[tsm-size]', base.$obj).each(function(){$(this).css('width', $(this).attr('tsm-size'))});
				// base.$obj.html().replace(/tsm-size=\"(.*?)\"/gim, 'style=\"width: $1px\"');

				// set ID
				base.$obj.addClass('tsm-js-'+base.id);

				// set position menu
				if(base.settings.position == 'left' || base.settings.position == 'right' || base.settings.position == 'bottom')
					base.$obj.addClass('tsm-position-'+base.settings.position);

				// set fixed menu
				if(base.settings.fixed)
					base.$obj.addClass('tsm-fixed');

				// set full width
				if(base.settings.fullWidth)
					base.$obj.addClass('tsm-full-width');

				// add responsive
				if(base.settings.responsive) {
					var rType = base.settings.responsive;
					
					if(rType == 'stack-margin' || rType == 'switch-margin') {
						base.$obj.addClass('tsm-response-margin');
						rType = rType.replace('-margin', '');
					}
					
					base.$obj.addClass('tsm-response-'+rType);
				}

				if(!base.settings.effect) {
					base.settings.effect = {
						name: 'fade',
						speed: 0,
						delay: 0,
						easing: 'linear'
					}
				}

				// set effect
                
                var base_menu_wrap_id = '';
                var menu_wrap_id = base.$obj.closest('.tsm-menu-wrap').attr('id');
                if (typeof menu_wrap_id != 'undefined' && typeof menu_wrap_id != false){
                    base_menu_wrap_id = '#' + menu_wrap_id + ' ';
                }
                
				base.$styles = $([
          '<style>',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' > li > div,',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' li > ul,',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' > li:hover > div,',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' li:hover > ul {',
              'display: none;',
              'z-index: -1;',
            '}',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' > li.tsm-opened > div,',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' li.tsm-opened > ul {',
              'display: block;',
              'z-index: 20;',
            '}',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' > li.tsm-effect-on > div,',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' li.tsm-effect-on > ul {',
              '-webkit-animation-duration: ' + base.settings.showEffect.speed + 'ms;',
              'animation-duration: ' + base.settings.showEffect.speed + 'ms;',
              ' -webkit-animation-delay: ' + base.settings.showEffect.delay + 'ms;',
              'animation-delay: ' + base.settings.showEffect.delay + 'ms;',
            '}',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' > li.tsm-effect-off > div,',
            base_menu_wrap_id + '.ts-megamenu.tsm-js-'+base.id+' li.tsm-effect-off > ul {',
              '-webkit-animation-duration: ' + base.settings.hideEffect.speed + 'ms;',
              'animation-duration: ' + base.settings.hideEffect.speed + 'ms;',
              ' -webkit-animation-delay: ' + base.settings.hideEffect.delay + 'ms;',
              'animation-delay: ' + base.settings.hideEffect.delay + 'ms;',
            '}',
          '</style>'
        ].join(''));
				base.$obj.after(base.$styles);

				// call to bind events on DOM element
				_bindEvents();
			}
 
			// public destroy method
			base.destroy = function() {
				// unbind events
				base.$liDrop.off('.' + namespace);
				base.$liDrop.parent().off('.' + namespace);
				$(document).off('.' + namespace);
				
				// remove custom styles
				if(base.$styles) base.$styles.remove();

				// remove classes
				base.$obj.removeClass('tsm-js-'+base.id);
				base.$obj.removeClass('tsm-position-'+base.settings.position);
				if(base.settings.fixed)
					base.$obj.removeClass('tsm-fixed');
				if(base.settings.fullWidth)
					base.$obj.removeClass('tsm-full-width');
				if(base.settings.responsive)
					base.$obj.removeClass('tsm-response-'+base.settings.responsive);

				// object destruction
				base.$obj.removeData(namespace);
				delete base;
			}

      // show menu subs
      function effectEnd($this) {
        if($this.hasClass('tsm-effect-on')) {
          $this.removeClass('tsm-effect-on');
        }
        else if ($this.hasClass('tsm-effect-off')) {
          $this.removeClass('tsm-opened tsm-effect-off');
        }
      }

      function showSub($this) {
        if($this.hasClass('tsm-opened')) {
          return;
        }

        var $thisSub = $('> div, > ul', $this);
        $this.removeClass('tsm-effect-off ' + base.settings.hideEffect.name).addClass('tsm-opened tsm-effect-on ' + base.settings.showEffect.name);
        
        if(!$.support.animation || $thisSub.css('animation').indexOf('none') !== -1) {
          effectEnd($this);
        }
      }

      function hideSub($this, force) {
        $this.each(function() {
          if($(this).hasClass('tsm-effect-on ') || $(this).hasClass('tsm-effect-off') || $('.tsm-effect-on', $(this))[0] || (!force && this.parentElement.querySelector(':hover') === this)) {
            return;
          }

          var $thisSub = $(this).find('> div, > ul');
          $(this).addClass('tsm-effect-off ' + base.settings.hideEffect.name).removeClass('tsm-effect-on ' + base.settings.showEffect.name);
          if(!$.support.animation || $thisSub.css('animation').indexOf('none') !== -1) {
            effectEnd($(this));
          }
        });
      }
			
			// plugin events
			var _bindEvents;
			
            function initMegaMenu(){
                if(base.settings.responsiveClick){
                    if (!isNaN(parseInt(base.settings.breakpoint))){
                        var doc_w = $(document).width();
                        //alert(curShowOn);
                        var breakpoint = parseInt(base.settings.breakpoint);
                        if ( doc_w <= breakpoint ){
                            base.settings.showOn = 'click';
                        }
                        else{
                            base.settings.showOn = curShowOn;
                            //alert(base.settings.showOn);
                        }
                    }   
                }
                
                _bindEvents = function() {
                    // click events
                    if(base.settings.showOn == 'click') {
                      base.$liDrop.on('click.' + namespace, function(e) {
                        var li = $(this).parent();
                        if(li.hasClass('tsm-opened')) {
                          // hide current
                          hideSub(li, true);
                        } else {
                          // show current
                          showSub(li);
            
                          // hide siblings
                          hideSub(li.siblings('.tsm-opened'));
            
                          // hide siblings
                          hideSub(li.siblings().find('.tsm-opened'));
            
                          // hide multi column siblings
                          hideSub(li.parent().siblings().find('.tsm-opened'));
            
                          // prevent follow a link
                          e.preventDefault();
                          e.stopPropagation();
                        }
                      });
                      base.$liDrop.parent().on('mouseleave.' + namespace, function() {
                        // event code here
                        hideSub($(this), true);
                      });
                    }
                    // toggle events
                    else if(base.settings.showOn == 'toggle') {
                      base.$liDrop.on('click.' + namespace, function(e) {
                        var li = $(this).parent();
                        if(li.hasClass('tsm-opened')) {
                          // hide current
                          hideSub(li, true);
                        } else {
                          // show current
                          showSub(li);
            
                          // hide siblings
                          hideSub(li.siblings('.tsm-opened'));
            
                          // hide siblings
                          hideSub(li.siblings().find('.tsm-opened'));
            
                          // hide multi column siblings
                          hideSub(li.parent().siblings().find('.tsm-opened'));
            
                          // prevent follow a link
                          e.preventDefault();
                          e.stopPropagation();
                        }
                      });
                    }
                    // hover events
                    else {
                      base.$liDrop.on('mouseenter.' + namespace, function(e) {
                        // add opened class and prevent follow a link
                        showSub($(this).parent());
                        e.preventDefault();
                        e.stopPropagation();
                      });
                      base.$liDrop.parent().on('mouseleave.' + namespace, function() {
                        // remove opened class after mouse leave
                        hideSub($(this), true);
                      });
                    }
            
                    // close sub menus when click on document
                    $(document).on('click.' + namespace + ' touchstart.' + namespace, function(e) {
                      hideSub(base.$liDrop.parent());
                    });
            
                    // animation end
                    base.$divDrop.on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                      // remove animation classes
                      effectEnd($(this).parent());
            
                      // hide sub if mouse leave
                      if(base.settings.showOn == 'hover' || base.settings.showOn == 'click') {
                        hideSub($(this).parent());
                      }
                    });
            
            				// sticky menu
            				if(base.settings.sticky && !base.settings.fixed) {
            					var tsmHeight = base.$obj.outerHeight(true),
            						tsmPos = base.$obj.offset(),
            						wndH = $(window).height();
            
            					$(window).on('scroll.' + namespace, function() {
            						var stickyOn = 
            							(base.settings.position != 'bottom' && $(this).scrollTop() >= tsmPos.top)
            							|| (base.settings.position == 'bottom' && $(this).scrollTop() + wndH <= tsmPos.top + tsmHeight);
            						
            						if(stickyOn)
            							base.$obj.addClass('tsm-fixed');
            						else
            							base.$obj.removeClass('tsm-fixed');
            					}).scroll();
            				}
            			}
               // calling init
                _init();
              }
              
              initMegaMenu();
              
              $(window).resize(function(){
                base.destroy();
                initMegaMenu();
              });
		}
		
		// Method calling logic
		return this.each(function() {
			if (typeof $(this).data(namespace) == 'undefined') {
				// Create plugin for this element
				$(this).data(namespace, new window[namespace](this, method));
			} else if (typeof $(this).data(namespace) == 'object' && typeof method == 'undefined') {
				$.error('This element already has jQuery.' + namespace);
			} else if (typeof $(this).data(namespace) == 'object' && typeof $(this).data(namespace)[method] == 'function') {
				// Element has a plugin and method, call it
				$(this).data(namespace)[method].apply(this, Array.prototype.slice.call(arguments, 1));
			} else {
				$.error('Method ' + method + ' does not exist on jQuery.' + namespace);
			}
		});
	}
})(jQuery);

// bind polyfill from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
Function.prototype.bind||(Function.prototype.bind=function(t){if("function"!=typeof this)throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");var o=Array.prototype.slice.call(arguments,1),n=this,r=function(){},i=function(){return n.apply(this instanceof r&&t?this:t,o.concat(Array.prototype.slice.call(arguments)))};return r.prototype=this.prototype,i.prototype=new r,i});