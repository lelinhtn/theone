<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}

if( ! class_exists( 'Mega_Menu_Walker' ) ) :

/**
 * @package WordPress
 * @since 1.0.0
 * @uses Walker
 */
class TSM_Mega_Menu_Walker extends Walker_Nav_Menu {
    
	/**
	 * Starts the list before the elements are added.
	 *
	 * @see Walker::start_lvl()
	 *
	 * @since 1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {

		$indent = str_repeat("\t", $depth);

		$output .= "\n$indent<ul class=\"tsm-sub-menu w-200\">\n";
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @see Walker::end_lvl()
	 *
	 * @since 1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	/**
	 * Custom walker. Add the widgets into the menu.
	 *
	 * @see Walker::start_el()
	 *
	 * @since 1.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item   Menu item data object.
	 * @param int    $depth  Depth of menu item. Used for padding.
	 * @param array  $args   An array of arguments. @see wp_nav_menu()
	 * @param int    $id     Current item ID.
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $tsm_script_localize;
        
        $menu_item_settings_default = array(
            'menu_item_id'      =>  0,
            'show_caret'        =>  'yes',
            'caret'             =>  'fa fa-angle-down',
            'drop_direction'    =>  'tsm-drop-right',
            'icon_class'        =>  '',
            'mega_type'         =>  'grid'
        ); 
        
        $menu_item_settings = get_post_meta( $item->ID, 'tsm_menu_item_settings', true );
        
        if ( !is_array( $menu_item_settings ) ):
            
            $menu_item_settings = ( array ) $menu_item_settings;
            
        endif;
        
        $menu_item_settings = array_merge( $menu_item_settings_default, $menu_item_settings );

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		// Item Class
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $classes[] = 'menu-item-' . $item->ID;
        
        if ( $menu_item_settings['mega_type'] == 'grid' ):
            $classes[] = 'tsm-content-full'; // tsm-content-full
            $classes[] = 'tsm-grid';
        endif;
    
        //$classes[] = $menu_item_settings['drop_direction'];
        
        $class = join( ' ', apply_filters( 'tsm_nav_menu_css_class', array_filter( $classes ), $item, $args ) );

		// Item ID
		$id = esc_attr( apply_filters( 'tsm_nav_menu_item_id', "mega-menu-item-{$item->ID}", $item, $args ) );
		
		$output .= $indent . "<li class='{$class}' id='{$id}'>";
        $mega_html = '';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )     ? $item->target     : '';
		$atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
		$atts['href']   = ! empty( $item->url )        ? $item->url        : '';
        
        if ( $menu_item_settings['mega_type'] == 'grid' ):
        
            $mega_menu_item_data = array_filter( (array) get_post_meta( $item->ID, 'tsm_menu_item_grid_data', true ) ); // Grid data
            $menu_item_grid_options = array_filter( (array) get_post_meta( $item->ID, 'tsm_menu_item_grid_options', true ) );
            $menu_item_grid_options_default = array(
                'width'         =>  'auto',
                'fullwidth'     =>  'yes',
                'min_width'     =>  0,
                'max_width'     =>  980
            );
            $menu_item_grid_options = array_merge( $menu_item_grid_options_default, $menu_item_grid_options );
            
            $rows_html = '';
            if ( !empty( $mega_menu_item_data ) ):
                    
                $mega_row_default_args = array(
                    'additional_class'  =>  '',
                    'cols'              =>  array()
                );
                $total_rows = count( $mega_menu_item_data );
                $row_num = 0;
                
                foreach ( $mega_menu_item_data as $mega_row_args ): 
                    
                    $row_num++;
                    $mega_row_args = array_merge( $mega_row_default_args, $mega_row_args );
                    
                    $row_class = 'tsm-row tsm-row tsm-row-' . $row_num;
                    $row_class .= ( $row_num == 1 ) ? ' tsm-first-row' : '';
                    $row_class .= ( $row_num == $total_rows ) ? ' tsm-last-row': '';
                    $row_additional_class = isset( $mega_row_args['additional_class'] ) ? trim( $mega_row_args['additional_class'] ) : '';
                    
                    $cols_html = '';
                    if ( !empty( $mega_row_args['cols'] ) ):
                        
                        $default_col_args = array(
                            'additional_class'  =>  '',
                            'col_width'         =>  4,
                            'elems'             =>  array()
                        );
                        $total_cols = count( $mega_row_args['cols'] );
                        $col_num = 0;
                        
                        $total_col_w = 0; // Total cols width in this row
                        foreach ( $mega_row_args['cols'] as $cols_args ):
                            
                            $col_num++;
                            $cols_args = array_merge( $default_col_args, $cols_args );
                            
                            $col_w = isset( $cols_args['col_width'] ) ? intval( $cols_args['col_width'] ) : 4;
                            $total_col_w += $col_w;
                            $col_class = 'tsm-col tsm-col c-' . $col_w . ' tsm-col-' . $col_num;
                            $col_class .= ( $col_num == 1 ) ? ' tsm-first-col' : '';
                            $col_class .= ( $col_num == $total_cols ) ? ' tsm-last-col': '';
                            $col_additional_class = isset( $cols_args['additional_class'] ) ? trim( $cols_args['additional_class'] ) : '';
                            
                            $elem_html = '';
                            if ( !empty( $cols_args['elems'] ) ):
                                
                                $elem_num = 0;
                                $total_elems = count( $cols_args['elems'] );
                                foreach ( $cols_args['elems'] as $elem_args ):
                                    
                                    $elem_num++;
                                    $elem_type = ( isset( $elem_args['elem_type'] ) ) ? esc_attr( $elem_args['elem_type'] ) : '';
                                    $elem_wrap_class = 'tsm-mega-menu-item-wrap-' . $elem_num;
                                    $elem_wrap_class .= ( $elem_num == 1 ) ? ' first-child': '' ;
                                    $elem_wrap_class .= ( $elem_num == $total_elems ) ? ' last-child': '' ;
                                    
                                    switch ( $elem_type ):
                                        
                                        case 'normal':
                                            
                                                $data_elem = ( isset( $elem_args['data_json'] ) ) ? tsm_object_to_array_recursive( json_decode( $elem_args['data_json'] ) ) : array();
                                                $shortcode = tsm_gen_shortcode( $data_elem, $elem_wrap_class );
                                                $elem_html .= do_shortcode( $shortcode );
                                            
                                            break;
                                            
                                        case 'widget':
                                                
                                                $elem_html .= tsm_show_widget( $elem_args, $elem_wrap_class );
                                                
                                            break;
                                        
                                    endswitch;
                                    
                                endforeach;
                                
                            endif; // End if ( !empty( $cols_args['elems'] ) )
                            
                            $cols_html .=   '<div class="' . $col_class . ' ' . $col_additional_class . '">
                                                ' . $elem_html . '
                                            </div>';
                            
                            if ( $total_col_w % 12 == 0 ):
                                
                                $cols_html .= '<div class="tsm-clearfix tsm-visible-xs-block tsm-visible-sm-block tsm-visible-md-block tsm-visible-lg-block"></div>';
                                
                            endif;
                            
                        endforeach;
                        
                    endif; // End if ( !empty( $mega_row_args['cols'] ) )
                    
                    $rows_html .=   '<div class="' . $row_class . $row_additional_class . '">
                                        ' . $cols_html . '
                                    </div>
                                    <div class="tsm-clearfix tsm-visible-xs-block tsm-visible-sm-block tsm-visible-md-block tsm-visible-lg-block"></div>
                                    ';
                    
                endforeach;
                
                $mega_html_style = '';
                
                if ( $menu_item_grid_options['fullwidth'] != 'yes' ):
                    $grid_w = $menu_item_grid_options['width'];
                    if ( is_numeric( $grid_w ) ):
                        $grid_w .= 'px';
                    endif;
                    $mega_html_style = 'style=\'left:auto; width: ' . $menu_item_grid_options['width'] . '; min-width: ' . $menu_item_grid_options['min_width'] . 'px; max-width: ' . $menu_item_grid_options['max_width'] . 'px;\'';
                    
                endif;
                
                $mega_html .=   '<div class="tsm-content-wrap" ' . $mega_html_style . '>
                                        ' . $rows_html . '
                                </div><!-- .tsm-content-wrap -->';
                
            endif; // if ( !empty( $mega_menu_item_data ) )
            
        else: // $menu_item_settings['mega_type'] == 'dropdown'
            
            $mega_menu_item_dropdown_data = get_post_meta( $item->ID, 'tsm_menu_item_dropdown_data', true );
            
            $dropdown_html = '';
            $dropdown_inner_html = '';
            $is_multi_dropdown = count( $mega_menu_item_dropdown_data ) > 1; 
            $dropdown_data_default = array(
                'list_w'            =>  $tsm_script_localize['vars']['default_list_w'],
                'addition_class'    =>  '',
                'items_data'        =>  array()
            );
            
            // Multi dropdown
            if ( $is_multi_dropdown ):
                $total_dropdown_lv_0_w = 0;
                foreach ( $mega_menu_item_dropdown_data as $dropdown_data ):
                    $dropdown_data = array_merge( $dropdown_data_default, $dropdown_data );
                    $total_dropdown_lv_0_w += intval( $dropdown_data['list_w'] );
                    $dropdown_inner_html .= tsm_list_dropdown_frontend( $dropdown_data, $menu_item_settings['drop_direction'] );
                endforeach;
                
                $dropdown_html .=   '<div class="tsm-multi-column w-' . $total_dropdown_lv_0_w . '">';
                $dropdown_html .=   $dropdown_inner_html;
                $dropdown_html .=   '</div><!-- .tsm-multi-column -->';
            
            // Single dropdown
            else:
                
                foreach ( $mega_menu_item_dropdown_data as $dropdown_data ):
                    $dropdown_data = array_merge( $dropdown_data_default, $dropdown_data );
                    $dropdown_inner_html .= tsm_list_dropdown_frontend( $dropdown_data, $menu_item_settings['drop_direction'] );
                endforeach;
                $dropdown_html .=   $dropdown_inner_html;
                
            endif; // End if ( $is_multi_dropdown )
            
            $mega_html .= $dropdown_html;
            
        endif; // End if ( $menu_item_settings['mega_type'] == 'grid' )

		$atts = apply_filters( 'tsm_nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';

		foreach ( $atts as $attr => $value ) {

			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}

		}
        
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
        
        if ( trim( $menu_item_settings['icon_class'] ) != '' ):
            $item_output .= '<i class="' . trim( $menu_item_settings['icon_class'] ) . '"></i>';
        endif;

		/** This filter is documented in wp-includes/post-template.php */
		$item_output .= $args->link_before . apply_filters( 'tsm_the_title', $item->title, $item->ID ) . $args->link_after;
        
        // Has children
    	$children = get_posts( array( 'post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID ) );
    	if ( ( !empty( $children ) || $mega_html != '' ) && $menu_item_settings['show_caret'] == 'yes' ) {
            $item_output .= '<i class="tsm-caret ' . sanitize_text_field( $menu_item_settings['caret'] ) . '"></i>';
    	}
        
		$item_output .= '</a>';
        $item_output .= $mega_html;
		$item_output .= $args->after;

		$output .= apply_filters( 'tsm_walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
    
}

endif;