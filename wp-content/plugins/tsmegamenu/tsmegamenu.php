<?php

/*
 * Plugin Name: Theme Studio Mega Menu
 * Plugin URI:  http://themestudio.net
 * Description: Perfect Mega Menu for WordPress.
 * Version:     1.0
 * Author:      Lê Mạnh Linh
 * Author URI:  http://themestudio.net
 * License:     GPL-2.0+
 * Copyright:   2014 Lê Mạnh Linh
 */

// Covert css to scss
// http://css2sass.herokuapp.com/

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/** Define global constant **/
define( 'TSM_DIR', dirname( __FILE__ ) );
define( 'TSM_URL', plugins_url( '', __FILE__ ) );
define( 'TSM_VERSION', '1.0' );

if( !function_exists( 'wp_get_current_user' ) ) {
    include( ABSPATH . 'wp-includes/pluggable.php' ); 
}

/** Init **/
require_once( TSM_DIR . '/inc/init.php' );

/** Frontend nav menu walker **/
add_filter( 'wp_nav_menu_args', 'tsm_nav_menu_args', 1 );

/**
 * Use the TSM Mega Menu walker to output the menu
 * Resets all parameters used in the wp_nav_menu call
 * Wraps the menu in mega-menu IDs and classes
 *
 * @since 1.0
 * @param $args array
 * @return array
 */
function tsm_nav_menu_args( $args ) {
    
	$current_theme_location = $args['theme_location'];

	$locations = get_nav_menu_locations();

    if ( has_nav_menu( $current_theme_location ) ):
        
        $menu_id = ( isset( $locations[ $current_theme_location ] ) ) ? $locations[ $current_theme_location ] : '';
        $menu_settings = get_post_meta( $menu_id, 'tsm_mega_menu_settings', true );
        $breakpoint = isset( $menu_settings['breakpoint'] ) ? intval( $menu_settings['breakpoint'] ): '';
        $enable_menu = false;
        if ( isset( $menu_settings['enable_mega_menu'] ) ):
            $enable_menu = $menu_settings['enable_mega_menu'] == 'yes';
        endif;
        
        if ( $enable_menu ):
            
            $defaults = array(
    			'menu'            => $menu_id,
    			'container'       => 'div',
    			'container_class' => 'tsm-menu-wrap tsm-menu-wrap-' . $menu_id . ' tsm-menu-wrap-' . $current_theme_location . '-' . $menu_id,
    			'container_id'    => 'tsm-menu-wrap-' . $current_theme_location . '-' . $menu_id,
    			'menu_class'      => 'ts-megamenu',//  tsm-effect-fade tsm-full-width
    			'menu_id'         => 'tsm-menu-' . $current_theme_location . '-' . $menu_id,
    			'fallback_cb'     => 'wp_page_menu',
    			'before'          => '',
    			'after'           => '',
    			'link_before'     => '',
    			'link_after'      => '',
    			'items_wrap'      => '<ul data-breakpoint="' . $breakpoint . '" class="%2$s">%3$s</ul>',
    			'depth'           => 0,
    			'walker'          => new TSM_Mega_Menu_Walker()
    		);
    
    		$args = array_merge( $args, $defaults );
            
        endif; // End if ( $enable_menu )
        
    endif;

	return $args;
}


/** 
 * Load textdomain
 * @since 1.0
**/
function tsm_load_localization() {
    
	load_plugin_textdomain( 'tsm', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    
}
add_action( 'plugins_loaded', 'tsm_load_localization' );



