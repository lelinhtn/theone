<?php
/*
 * Plugin Name: The One Core
 * Plugin URI: 
 * Description: Functions add new post type, taxonomy, metaboxes, shortcodes, ...
 * Author: Theme Studio
 * Version: 1.0
 * Author URI: 
 */
 if ( ! defined( 'ABSPATH' ) ) {
	exit; // disable direct access
 }
 
 define( 'THEONECORE_VERSION', '1.0' );
 define( 'THEONECORE_BASE_URL', trailingslashit( plugins_url( 'theone-core' ) ) );
 define( 'THEONECORE_DIR_PATH', plugin_dir_path( __FILE__ ) );
 define( 'THEONECORE_LIBS', THEONECORE_DIR_PATH . '/libs/' );
 define( 'THEONECORE_LIBS_URL', THEONECORE_BASE_URL . '/libs/' );
 define( 'THEONECORE_CORE', THEONECORE_DIR_PATH . '/core/' );
 define( 'THEONECORE_CSS_URL', THEONECORE_BASE_URL . '/assets/css/' );
 define( 'THEONECORE_JS', THEONECORE_BASE_URL . '/assets/js/' );
 define( 'THEONECORE_IMG_URL', THEONECORE_BASE_URL . '/assets/images/' );  
 
/**
 * Load plugin textdomain
 */
if (!function_exists('theone_core_load_textdomain')) {
    function theone_core_load_textdomain() {
      load_plugin_textdomain( 'theone-core', false, THEONECORE_DIR_PATH . 'languages' ); 
    }
    add_action( 'plugins_loaded', 'theone_core_load_textdomain' );
}

/**
 * Load libs 
 */
if ( file_exists( THEONECORE_LIBS . 'init.php' ) ) {
    require_once THEONECORE_LIBS . 'init.php';
}

/**
 * Load core 
 */
if ( file_exists( THEONECORE_CORE . 'init.php' ) ) {
    require_once THEONECORE_CORE . 'init.php';
}

if ( !function_exists( 'theone_core_admin_fonts_url' ) ) {
    
    /**
     * Register Google fonts for The One.
     *
     * @since The One 1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function theone_core_admin_fonts_url() {
    	$fonts_url = '';
    	$fonts     = array();
    	$subsets   = 'latin,latin-ext';
    
    	/* translators: If there are characters in your language that are not supported by Oswald, translate this to 'off'. Do not translate into your own language. */
    	if ( 'off' !== _x( 'on', 'Oswald font: on or off', 'themestudio' ) ) {
    		$fonts[] = 'Oswald:400,300,700';
    	}
    
    	/* translators: To add an additional character subset specific to your language, translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language. */
    	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'themestudio' );
    
    	if ( 'cyrillic' == $subset ) {
    		$subsets .= ',cyrillic,cyrillic-ext';
    	} elseif ( 'greek' == $subset ) {
    		$subsets .= ',greek,greek-ext';
    	} elseif ( 'devanagari' == $subset ) {
    		$subsets .= ',devanagari';
    	} elseif ( 'vietnamese' == $subset ) {
    		$subsets .= ',vietnamese';
    	}
    
    	if ( $fonts ) {
    		$fonts_url = add_query_arg( array(
    			'family' => urlencode( implode( '|', $fonts ) ),
    			'subset' => urlencode( $subsets ),
    		), '//fonts.googleapis.com/css' );
    	}
    
    	return $fonts_url;
    }   
}


/**
 * Enqueue Admin Style  
 */
if (!function_exists('theone_core_admin_enqueue_style')) {
    
    function theone_core_admin_enqueue_style() {
        
        // Add custom fonts, used in the main stylesheet.
        wp_enqueue_style( 'theonecore-fonts', theone_core_admin_fonts_url(), array(), null );
        
        wp_register_style( 'theonecore-spectrum', THEONECORE_LIBS_URL . 'spectrum/spectrum.css', array(), THEONECORE_VERSION, 'all' );
        wp_enqueue_style( 'theonecore-spectrum' );
        
        wp_register_style( 'theonecore-admin-bootstrap', THEONECORE_CSS_URL . 'admin-bootstrap.css', array(), THEONECORE_VERSION, 'all' );
        wp_enqueue_style( 'theonecore-admin-bootstrap' );
        
        wp_register_style( 'theonecore-admin-style', THEONECORE_CSS_URL . 'admin-style.css', array(), THEONECORE_VERSION, 'all' );
        wp_enqueue_style( 'theonecore-admin-style' );
        
        wp_register_style( 'theonecore-admin', THEONECORE_CSS_URL . 'admin.css', array(), THEONECORE_VERSION, 'all' );
        wp_enqueue_style( 'theonecore-admin' );
        
    }
    add_action( 'admin_enqueue_scripts', 'theone_core_admin_enqueue_style' );
}

/**
 * Enqueue Frontend Styles 
 **/
if ( !function_exists( 'theone_core_css' ) ) {

	/*
	 * Load css
	*/
	function theone_core_css() {
        
        wp_register_style( 'theonecore-animate', THEONECORE_LIBS_URL. 'animate/animate.css', false, THEONECORE_VERSION, 'all' );
		wp_enqueue_style( 'theonecore-animate' );
        
        wp_register_style( 'theonecore-frontend-style', THEONECORE_CSS_URL. 'frontend-style.css', false, THEONECORE_VERSION, 'all' );
		wp_enqueue_style( 'theonecore-frontend-style' );

	}
	add_action( 'wp_enqueue_scripts', 'theone_core_css' );

}

/**
 * Enqueue Admin js  
 */
if (!function_exists('theone_core_admin_enqueue_js')) {
    
    function theone_core_admin_enqueue_js() {
        
        $screen = get_current_screen();
        
        /*
        if ( isset( $screen->post_type ) ) {
            if ( $screen->post_type == 'animated_column' ) {
                //global $linea_fonts;
                
                wp_register_script( 'theonecore-admin-animated_column', THEONECORE_JS . 'admin-animated_column.js', array(), THEONECORE_VERSION, true );
                wp_enqueue_script( 'theonecore-admin-animated_column' );
                //wp_localize_script( 'theonecore-admin-animated_column', 'linea_fonts', $linea_fonts );
                
            }   
        }
        */
        wp_register_script( 'theonecore-admin-animated_column', THEONECORE_JS . 'admin-animated_column.js', array(), THEONECORE_VERSION, true );
        wp_enqueue_script( 'theonecore-admin-animated_column' );
        
        wp_register_script( 'theonecore-spectrum', THEONECORE_LIBS_URL . 'spectrum/spectrum.js', array(), THEONECORE_VERSION, true );
        wp_enqueue_script( 'theonecore-spectrum' );
        
        wp_register_script( 'theonecore-admin-scripts', THEONECORE_JS . 'admin-scripts.js', array(), THEONECORE_VERSION, true );
        wp_enqueue_script( 'theonecore-admin-scripts' );
        
        $ts_vc_edit_nonce = wp_create_nonce( 'ts_vc_edit_nonce' );
        wp_localize_script( 'theonecore-admin-scripts', 'ts_vc_edit_nonce', $ts_vc_edit_nonce );
        
    }
    add_action( 'admin_enqueue_scripts', 'theone_core_admin_enqueue_js' );
}

if( !function_exists( 'theone_core_enqueue_js' ) ) {

	/*
	 * Load jquery
	*/
	function theone_core_enqueue_js() {
        wp_register_script( 'theonecore-wow.min', THEONECORE_LIBS_URL . 'wow/wow.min.js', false, THEONECORE_VERSION, true );
        wp_enqueue_script( 'theonecore-wow.min' );
		if ( !is_admin() ) {
            
            //wp_dequeue_script( 'jquery-effects-core' );
//            wp_dequeue_script( 'jquery-ui-tabs' );
            
            //wp_enqueue_script( 'jquery-effects-core' );
            //wp_enqueue_script( 'jquery-ui-tabs' );
            
            wp_register_script( 'theonecore-frontend-script', THEONECORE_JS . 'frontend-script.js', false, THEONECORE_VERSION, true );
            wp_enqueue_script( 'theonecore-frontend-script' );
			
		}
	}
	add_action('wp_enqueue_scripts','theone_core_enqueue_js');

}



