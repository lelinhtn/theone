jQuery(document).ready(function($){
    
    "use strict";
    
    // Add icon picker
    // Choose an icon
    $(document).on('click', '.theone-icons-list-chooser-wrap .theone-icon', function(e){
        
        var $this = $(this);
        var icon_class = $this.find('i').attr('class');
        
        if ( $('#theone_icon_class').length ) {
            var thisTd = $this.closest('td');
            
            if ( !thisTd.find('.cmb_metabox_description i').length ) {
                thisTd.find('.cmb_metabox_description').prepend('<i></i>');
            }
            thisTd.find('.cmb_metabox_description i').attr('class', '').addClass('theone-icon-preview ' + icon_class);
            thisTd.find('#theone_icon_class').val(icon_class);   
        }
        else{ // Quick edit post for VC
            var thisGroup = $this.closest('.ts-quick-edit-field-group');
            thisGroup.find('.ts-icon-class-input').val(icon_class).trigger('change');   
        }
        
        e.preventDefault();
        
    });
    
    // Ajax search icon
    $(document).on('change', '.theone-icons-list-chooser-wrap .ts-search-icon', function(e){
        
        var $this = $(this);
        var thisIconPicker = $this.closest('.iconpicker-wrap');
        var search_key = $this.val();
        var cur_page = thisIconPicker.find('.theone-pagination .theone-cur-page').attr('data-page');
        var icons_per_page = thisIconPicker.find('.theone-pagination .theone-cur-page').attr('data-limit');
        
        if ( thisIconPicker.find('.theone-icons-list-chooser-wrap').hasClass('loading') ) {
            return false;
        }
        
        var data = {
            action: 'theone_search_icon_via_ajax',
            search_key: search_key,
            cur_page: cur_page,
            icons_per_page: icons_per_page
        };
        
        thisIconPicker.find('.theone-icons-list-chooser-wrap').addClass('loading');
        
        $.post(ajaxurl, data, function(response){
            
            thisIconPicker.html(response['html']);
            
        });
        
        return false;
        e.preventDefault();
         
    });
    
    // Go to page (icon picker)
    $(document).on('click', '.theone-icons-list-chooser-wrap .theone-pagination li', function(e){
        
        if ( $(this).hasClass('theone-cur-page') ) {
            return false;
        }
        $('.theone-icons-list-chooser-wrap .theone-pagination li').removeClass('theone-cur-page active');
        $(this).addClass('theone-cur-page active');
        $('.theone-icons-list-chooser-wrap .ts-search-icon').trigger('change');
         
    });
    
    // Toggle Icon Picker
    $(document).on('click', '.toggle-iconpicker', function(e){
        
        $('.iconpicker-wrap').slideToggle();
        e.preventDefault();
        
    });
    
    // Show icon preview when document is ready
    function theone_show_preview_icon() {
        if ( $('#theone_icon_class').length ) {
            var icon_class = $('#theone_icon_class').val();
            if ( !$('#theone_icon_class').closest('td').find('.cmb_metabox_description i').length ) {
                $('#theone_icon_class').closest('td').find('.cmb_metabox_description').prepend('<i></i>');
            }
            $('#theone_icon_class').closest('td').find('.cmb_metabox_description i').attr('class', '');
            $('#theone_icon_class').closest('td').find('.cmb_metabox_description i').addClass('theone-icon-preview ' + icon_class);   
        }
    }
    theone_show_preview_icon();
    
    // When typing in the input icon class
    $(document).on('change', '#theone_icon_class', function(){
         theone_show_preview_icon();
    });
    
    
    $(document).on('change', '.ts-quick-edit-field-group .ts-icon-class-input', function(){
        var $this = $(this); 
        var thisGroup = $this.closest('.ts-quick-edit-field-group');
        var post_id = $this.closest('.ui-accordion-content').attr('data-post-id');
        var icon_class = $this.val();
        if ( !thisGroup.find('.vc_description i').length ) {
            thisGroup.find('.vc_description').prepend('<i></i>');
        }
        thisGroup.find('.vc_description i').attr('class', '');
        thisGroup.find('.vc_description i').addClass('theone-icon-preview ' + icon_class);   
        
        // Change icon preview on the accordion header
        $('.quick-edit-title-' + post_id + ' i').attr('class', '').addClass('theone-icon-acc-header-preview ' + icon_class);
    });
    
    
});