jQuery(document).ready(function($){
    "use strict";
    
    //alert('ok');
    
    // Quick edit dependency fields
    function theone_show_hide_dep_fields() {
        $('.ts-dependency').each(function(){
            
            var $this = $(this);
            var dep_on = $this.attr('data-dep-on');
            var dep_val = $this.attr('data-dep-val');
            var dep_compare = $this.attr('data-dep-compare');
            
            if ( $(dep_on).length ) {
                var current_dep_val = $(dep_on).val();
                switch ( dep_compare ) {
                    case '=':
                        if ( dep_val == current_dep_val ) {
                            $this.css({
                                'display' : '' 
                            });
                        }
                        else{
                           $this.css({
                                'display' : 'none' 
                            }); 
                        }
                        break;
                }
            }
             
        });
    }
    theone_show_hide_dep_fields();
    
    if ( $('.ts-quick-edit-posts-for-vc-wrap').length ) {
        $('.ts-quick-edit-posts-for-vc-wrap').accordion({
            collapsible: true,
            active : 'none',
            heightStyle: 'content'
        });
    }
    
    $(document).on('click', '.vc_edit-form-tabs-menu .vc_edit-form-tab-control .vc_edit-form-link', function(e){
        
        var tab_target = $(this).attr('href');
        if ( $(tab_target).find('.ts-quick-edit-posts-for-vc-wrap').length ) {
            $(tab_target).find('.ts-quick-edit-posts-for-vc-wrap').accordion({
                collapsible: true,
                active : 'none',
                heightStyle: 'content'
            });
        }
        theone_show_hide_dep_fields();
        theone_init_color_picker(); // Re-init color picker
    });
    
    
    // Quick edit animate column
    $(document).on('change', '.ts-quick-edit-posts-for-vc-wrap .ts-quick-edit-field', function(e){
        
        var $this = $(this);
        var quickEditContent = $this.closest('.ts-quick-edit-post-vc-content');
        var is_edit_title = $this.hasClass('ts-title-edit') ? 'yes' : 'no';
        var edit_for = '';
        var post_id = quickEditContent.attr('data-post-id');
        var new_val = $this.val();
        
        if ( !$this.hasClass('ts-title-edit') ) {
            edit_for = $this.attr('data-edit-for'); // Meta key
        }
        
        var data = {
            action: 'theone_quick_edit_post_via_ajax',
            is_edit_title: is_edit_title,
            post_id: post_id,
            new_val: new_val,
            edit_for: edit_for,
            ts_vc_edit_nonce: ts_vc_edit_nonce
        };
        
        $.post(ajaxurl, data, function(response){
            
            if ( is_edit_title == 'yes' ) {
                $('.quick-edit-title-' + post_id + ' .accordion-header-title').text(new_val);
            }
            console.log(response);
            
        });
        theone_show_hide_dep_fields();
        
    });
    
    // Load quick edit animated posts via ajax
    $(document).on('change', '.vc_edit_form_elements .ts_anim_cat_select', function(e){
  
        theone_update_quick_edit_anim_col_val();
        
        var $this = $(this);
        var curTab = $this.closest('.vc_edit-form-tab'); 
        var quick_edit_items_data = $('.vc_edit_form_elements input[name="quick_edit_items"]').val();
        
        var data = {
            action: 'theone_load_quick_edit_animated_posts_via_ajax',
            quick_edit_items_data: quick_edit_items_data,
            ts_vc_edit_nonce: ts_vc_edit_nonce
        };
        
        $('.vc_edit_form_elements .ts-quick-edit-posts-for-vc-wrap').closest('.edit_form_line').addClass('loading');
        
        $.post(ajaxurl, data, function(response){
            
            $('.vc_edit_form_elements .ts-quick-edit-posts-for-vc-wrap').closest('.edit_form_line').removeClass('loading').html(response['html']);
            theone_show_hide_dep_fields();
            theone_init_color_picker();
            
            // Reinit accordion
            $('.vc_edit_form_elements .vc_edit-form-tab .ts-quick-edit-posts-for-vc-wrap').accordion({
                collapsible: true,
                active : 'none',
                heightStyle: 'content'
            });
            
            // Show message log in console
            console.log(response['message']);
            
        });
        
    });
    
    // Trigger change animated cat select when change animated posts limit
    $(document).on('change', '.vc_edit_form_elements input[name="limit"]', function(){
        var thisForm = $(this).closest('.vc_edit_form_elements');
        if (thisForm.find('.ts_anim_cat_select').length) {
            thisForm.find('.ts_anim_cat_select').trigger('change');
        } 
    });
    
    // Update quick edit animated columns item value (param name = quick_edit_items)
    function theone_update_quick_edit_anim_col_val() {
        var cat_id = $('.vc_edit_form_elements .ts_anim_cat_select').val();
        var posts_limit = $('.vc_edit_form_elements input[name="limit"]').val();
        
        var val_json = {
            cat_id: cat_id,
            limit: posts_limit
        }
        $('.vc_edit_form_elements input[name="quick_edit_items"]').val(encodeURIComponent(JSON.stringify(val_json)));
        
    }
    
    // Init color picker
    function theone_init_color_picker() {
        $('.ts-color-picker').each(function(){
            var $this = $(this);
            var palette = ['#dcc6b9', '#bcc7d0', '#d2e3cf', '#879999', '#aa9d71'];
            $this.spectrum({
                preferredFormat: "rgb",
                showPalette: true,
                showSelectionPalette: true, // true by default
                //selectionPalette: ["red", "green", "blue"],
                showInput: true,
                showAlpha: true,
                clickoutFiresChange: true,
                showButtons: false,
                allowEmpty:true,
                palette: [
                    palette
                ],
                change: function(color) {
                    if ( $.trim( color ) != '' ) {
                        color = color.toHexString(); // Convert to hex string
                        //color = color.toHex()       // "ff0000"
                        //color = color.toHexString() // "#ff0000"
                        //color = color.toRgb()       // {"r":255,"g":0,"b":0}
                        //color = color.toRgbString() // "rgb(255, 0, 0)"
                        //color = color.toHsv()       // {"h":0,"s":1,"v":1}
                        //color = color.toHsvString() // "hsv(0, 100%, 100%)"
                        //color = color.toHsl()       // {"h":0,"s":1,"l":0.5}
                        //color = color.toHslString() // "hsl(0, 100%, 50%)"
                        //color = color.toName()      // "red"   
                    }
                },
                move: function(color) {
                    
                }
            }); 
        });
    }
    theone_init_color_picker();
    
    
    
    // OLD CODES BELLOW =====================
    
    // http://stackoverflow.com/questions/21858112/calling-wordpress-gallery-uploader-selector-from-metabox
    // Open wp media upload when click on media upload button
    $(document).on('click', '.ovic-fh-add-gallery-btn', function(e){
        e.preventDefault();
        
        var images = $( '#ovic-fh-gallery-field' ).val();
        var gallery_state = images ? 'gallery-edit' : 'gallery-library';
        
        // create new media frame
        // You have to create new frame every time to control the Library state as well as selected images
        var wp_media_frame = wp.media.frames.wp_media_frame = wp.media( {
            title:      'Choose Images', // it has no effect but I really want to change the title
            frame:      'post',
            toolbar:    'main-gallery',
            state:      gallery_state,
            library:    {
                type: 'image'
            },
            multiple: true
        } );
        
        // when open media frame, add the selected image to Gallery
        wp_media_frame.on( 'open', function() {
            var images = $( '#ovic-fh-gallery-field' ).val();
            
            if ( !images )
                return;
            
            var image_ids = images.split( ',' );
            var library = wp_media_frame.state().get( 'library' );
            var attachment;
            image_ids.forEach( function( id ) {
                attachment = wp.media.attachment( id );
                attachment.fetch();
                library.add( attachment ? [ attachment ] : [] );
            } );
        } );
        
        // when click Insert Gallery, run callback
        wp_media_frame.on( 'update', function() {
    
            var thumb_wrapper = $( '#ovic-fh-gallery-preview-wrap' );
            thumb_wrapper.html( '' );
            var image_urls = [];
            var image_ids = [];
            
            var library = wp_media_frame.state().get( 'library' );
    
            library.map( function( image ) {
                image = image.toJSON();
                image_urls.push( image.url );
                image_ids.push( image.id );
                var image_html =    '<div data-img-id="'+image.id+'" class="col-lg-2 col-md-3 col-sm-3 col-xs-4 ovic-fh-thumbnail-preview ovic-fh-sortable-item">'+
                                        '<img src="' + image.sizes.thumbnail.url + '" alt="" title="" />'+
                                        '<i class="ovic-fh-delete fa fa-times-circle-o"></i>'+
                                    '</div>';
                thumb_wrapper.append( image_html );
            } );
            
            if (typeof image_ids !== 'undefined' && image_ids.length > 0) {
                $( '#ovic-fh-gallery-field' ).val(image_ids.join());
            }
            else{
                $( '#ovic-fh-gallery-field' ).val('');
            }
            
            theone_update_procat_imgs();
            
        } );
        
        wp_media_frame.open();
        
    }); // End add gallery
    
    // Remove All Procat Images
    $(document).on('click', '.ovic-fh-del-all-procat-imgs', function(e){
        
        var c = confirm( 'Are you sure you want to delete all this product category images?' );
        
        if ( !c ) {
            return false;
        }
        
        $('#ovic-fh-gallery-preview-wrap').html('');
        $('#ovic-fh-gallery-field').val('');
        theone_update_procat_imgs();
         
        e.preventDefault();
        
    });
    
    // Remove a product cat image
    $(document).on('click', '#ovic-fh-gallery-preview-wrap .ovic-fh-delete', function(e){
        
        $(this).closest('.ovic-fh-thumbnail-preview').remove();
        
        // Reupdate procat imgs
        theone_update_procat_img_ids();
        theone_update_procat_imgs();
        
        e.preventDefault();
         
    });
    
    // Load images when choose procat
    $(document).on('change', '#fh-procat-select', function(e){
        
        theone_reload_procat_imgs();
        e.preventDefault();
         
    });
    
    theone_reload_procat_imgs();
    function theone_reload_procat_imgs() {
        var procat_id = $('#fh-procat-select').val();
        
        $('#ovic-fh-gallery-preview-wrap').html('<div class="updated theone-mesage below-h2"><ul><li>Loading...</li></ul></div>');
        $('.row-info').html('');
        $('.procat-img-actions-wrap').css({'display': 'none'});
        $('#fh-procat-select').prop('disabled', 'disabled');
        theone_update_procat_img_ids();
        var data = {
            action              :   'theone_load_procat_imgs_via_ajax',
            //nonce               :   theone_core_nonce.ajax_nonce,
            procat_id           :   procat_id
        }
        
        $.post(ajaxurl, data, function(response){
            
            $('#ovic-fh-gallery-preview-wrap').html(response['html']);
            theone_update_procat_img_ids();
            $('.row-info').html(response['message']);
            $('.procat-img-actions-wrap').css({'display': 'block'});
            $('#fh-procat-select').prop('disabled', false);
            
        });
    }
    
    
    function theone_update_procat_imgs() {
        var procat_id = $('#fh-procat-select').val();
        var img_ids = $('#ovic-fh-gallery-field').val();
        
        $('.row-info').html('<div class="updated theone-mesage below-h2"><ul><li>Updating...</li></ul></div>');
        $('.procat-img-actions-wrap').css({'display': 'none'});
        $('#fh-procat-select').prop('disabled', 'disabled');
        var data = {
            action              :   'theone_update_procat_imgs_via_ajax',
            //nonce               :   theone_core_nonce.ajax_nonce,
            procat_id           :   procat_id,
            img_ids             :   img_ids
        }
        
        $.post(ajaxurl, data, function(response){
            
            $('.row-info').html(response);
            $('.procat-img-actions-wrap').css({'display': 'block'});
            $('#fh-procat-select').prop('disabled', false);
            
        });
        
    }
    
    function theone_update_procat_img_ids() {
        var image_ids = [];
        $('#ovic-fh-gallery-preview-wrap .ovic-fh-thumbnail-preview').each(function(){
             image_ids.push($(this).attr('data-img-id'));
        });
        $( '#ovic-fh-gallery-field' ).val(image_ids.join());
    }
    
});