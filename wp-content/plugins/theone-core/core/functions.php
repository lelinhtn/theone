<?php
/**
 * Global Functions  
 * @package The One 1.0
 */
 
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


/**
 *  Display wp errors (notes) messages
 *  @param $notes: array or string of notes
 *  @since 1.0 
 **/
function theone_display_note( $notes, $type='error', $echo = true ){
    $html = '';
        
    if( !empty( $notes ) ){
         $class = ( $type == 'error' ) ? 'error' : 'updated';
         $class .= ' theone-mesage';
        
        $html .= '<div class="' . $class . ' below-h2" >'; // error
        if ( is_array( $notes ) ){
           $html .= '<ul>' ;
            foreach( $notes as $e ){
                 $html .= '<li>' . ( $e ) . '</li>';
            }
           $html .= '</ul>'; 
        }else {
            $html .= '<p>' . ( $notes ) . '</p>';
        }
        
        $html .= '</div>';
    }
    
    if ( $echo ) {
        echo $html;
    }else {
        return $html;
    }
}


/**
 *  Update products categories via ajax
 *  @since 1.0 
 **/
function theone_update_procat_imgs_via_ajax() {
    
	$errors = array(); $notes = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'ovic-fh-core-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'theone-core' );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'You do not have sufficient permissions to access this page!', 'theone-core' );
        
    endif;
    
    if ( empty( $errors ) ):
    
        $procat_id = isset( $_POST['procat_id'] ) ? intval( $_POST['procat_id'] ) : 0;
        $img_ids = isset( $_POST['img_ids'] ) ? sanitize_text_field( $_POST['img_ids'] ) : '';
        
        if ( $procat_id > 0 ):
            
            update_option( 'theone_procat_imgs_' . $procat_id, $img_ids );
            $notes[] = __( 'Product images updated', 'theone-core' );
            
        endif;
        
    endif;
    
    theone_display_note( $errors ); theone_display_note( $notes, 'updated' );
 	
    
    die();
}
add_action( 'wp_ajax_theone_update_procat_imgs_via_ajax', 'theone_update_procat_imgs_via_ajax' );


/**
 *  Load procat imgs via ajax
 *  @since 1.0 
 **/
function theone_load_procat_imgs_via_ajax() {
    
    $response = array(
        'message'   =>  '',
        'html'      =>  ''
    );
    $errors = array(); $notes = array();
    $nonce = ( isset( $_POST['nonce'] ) ) ? $_POST['nonce'] : '';
    
    // Security check
    if ( !wp_verify_nonce( $nonce, 'ovic-fh-core-ajax-nonce' ) ):
        
        $errors[] = __( 'Security check error!', 'theone-core' );
        $response['html'] .= theone_display_note( $errors, 'error', false );
        
    endif;
    
    if ( !current_user_can( 'manage_options' ) ):
        
        $errors[] = __( 'You do not have sufficient permissions to access this page!', 'theone-core' );
        $response['html'] .= theone_display_note( $errors, 'error', false );
        
    endif;
    
    if ( empty( $errors ) ):
    
        $procat_id = isset( $_POST['procat_id'] ) ? intval( $_POST['procat_id'] ) : 0;
        $img_ids = get_option( 'theone_procat_imgs_' . $procat_id, '' );
        
        if ( $img_ids != ''  ):
            
            $img_ids = explode( ',', $img_ids );
            
            foreach ( $img_ids as $img_id ):
                
                $thumb_img = wp_get_attachment_image( $img_id, 'thumbnail' );
                
                $response['html'] .= '<div data-img-id="' . $img_id . '" class="col-lg-2 col-md-3 col-sm-3 col-xs-4 ovic-fh-thumbnail-preview ovic-fh-sortable-item">' . $thumb_img . '<i class="ovic-fh-delete fa fa-times-circle-o"></i></div>';
                
            endforeach;
            
        else:
            
            $notes[] = __( 'No image to display!', 'theone-core' );
            $response['html'] .= theone_display_note( $notes, 'updated', false );
            
        endif;
        
    endif;
    
    wp_send_json( $response );
    
    
    die();
}
add_action( 'wp_ajax_theone_load_procat_imgs_via_ajax', 'theone_load_procat_imgs_via_ajax' );



if ( !function_exists( 'theone_custom_taxonomy_opt_walker' ) ) :
    /**
     *  Return terms array
     *  @since 1.0
     **/
    function theone_custom_taxonomy_opt_walker( &$terms_select_opts = array(), $taxonomy, $parent = 0, $lv = 0 ) {
        
        $terms = get_terms( $taxonomy, array( 'parent' => $parent, 'hide_empty' => false ) );
        
        if ( $parent > 0 ):
            $lv++;
        endif;
        
        //If there are terms
        if( count( $terms ) > 0 ):
            $prefix = '';
            if ( $lv > 0 ):
                for ( $i = 1; $i <= $lv; $i++ ):
                    $prefix .= '-- ';
                endfor;
            endif;
            
            //Cycle though the terms
            foreach ( $terms as $term ):
                $terms_select_opts[$term->term_id] = htmlentities2( $prefix . $term->name );
                
                //Function calls itself to display child elements, if any
                theone_custom_taxonomy_opt_walker( $terms_select_opts, $taxonomy, $term->term_id, $lv ); 
            endforeach;
            
        endif;
        
    }
endif; // Endif !function_exists( 'theone_custom_taxonomy_opt_walker' )

if ( !function_exists( 'theone_procats_select' ) ):
    function theone_procats_select( $selected_procat_ids = array(), $settings = array() ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
        $term_args = array();
        $default_settings = array(
            'multiple'          =>  false,
            'id'                =>  '',
            'name'              =>  '',
            'class'             =>  '',
            'first_option'      =>  false,
            'first_select_val'  =>  '',
            'first_select_text' =>  __( ' --------------- ', 'theone-core' )
        );
        
        $settings = wp_parse_args( $settings, $default_settings );
        
        $attrs = '';
        $attrs .= ( trim( $settings['id'] ) != '' ) ? 'id="' . esc_attr( $settings['id'] ) . '"' : '';
        $attrs .= ( trim( $settings['name'] ) != '' ) ? ' name="' . esc_attr( $settings['name'] ) . '"' : '';
        $attrs .= ( $settings['multiple'] === true ) ? ' multiple="true"' : '';
        
        theone_custom_taxonomy_opt_walker( $term_args, 'product_cat' );
        
        $html = '';
        if ( !empty( $term_args ) ):
            
            $html .= '<select ' . $attrs . ' class="theone-select ' . $settings['class'] . '">';
            
            if ( $settings['first_option'] ):
                $html .= '<option ' . selected( in_array( 0, $selected_procat_ids ), true, false ) . ' value="' . esc_attr( $settings['first_select_val'] ) . '">' . sanitize_text_field( $settings['first_select_text'] ) . '</option>';
            endif;
            
            foreach ( $term_args as $term_id => $term_name ):
                
                $html .= '<option ' . selected( in_array( $term_id, $selected_procat_ids ), true, false ) . ' value="' . $term_id . '">' . $term_name . '</option>';
                
            endforeach;
            $html .= '</select>';
            
        endif;
        
        $html .= ob_get_clean();
        
        return $html;
        
    }
endif; // Endif if ( !function_exists( 'theone_procats_select' ) )

if ( !function_exists( 'theone_custom_tax_select' ) ):
    function theone_custom_tax_select( $selected_procat_ids = array(), $settings = array() ) {
        
        $term_args = array();
        $default_settings = array(
            'tax'               =>  'category',
            'multiple'          =>  false,
            'id'                =>  '',
            'name'              =>  '',
            'class'             =>  '',
            'first_option'      =>  false,
            'first_option_val'  =>  '',
            'first_option_text' =>  __( ' --------------- ', 'theone-core' )
        );
        
        $settings = wp_parse_args( $settings, $default_settings );
        
        if ( !taxonomy_exists( $settings['tax'] ) ):
        
            return false;
            
        endif;
        
        $attrs = '';
        $attrs .= ( trim( $settings['id'] ) != '' ) ? 'id="' . esc_attr( $settings['id'] ) . '"' : '';
        $attrs .= ( trim( $settings['name'] ) != '' ) ? ' name="' . esc_attr( $settings['name'] ) . '"' : '';
        $attrs .= ( $settings['multiple'] === true ) ? ' multiple="true"' : '';
        
        theone_custom_taxonomy_opt_walker( $term_args, $settings['tax'] );
        
        $html = '';
        if ( !empty( $term_args ) ):
            
            $html .= '<select ' . $attrs . ' class="theone-select ' . esc_attr( $settings['class'] ) . '">';
            
            if ( $settings['first_option'] ):
                $html .= '<option ' . selected( in_array( 0, $selected_procat_ids ), true, false ) . ' value="' . esc_attr( $settings['first_option_val'] ) . '">' . sanitize_text_field( $settings['first_option_text'] ) . '</option>';
            endif;
            
            foreach ( $term_args as $term_id => $term_name ):
                
                $html .= '<option ' . selected( in_array( $term_id, $selected_procat_ids ), true, false ) . ' value="' . $term_id . '">' . $term_name . '</option>';
                
            endforeach;
            $html .= '</select>';
            
        endif;
        
        return $html;
        
    }
endif; // Endif if ( !function_exists( 'theone_custom_tax_select' ) )


if ( !function_exists( 'theone_order_by_rating_post_clauses' ) ) {
        
    /**
     * theone_order_by_rating_post_clauses function.
     *
     * @param array $args
     * @return array
     * @since 1.0
     */
    function theone_order_by_rating_post_clauses( $args ) {
    	global $wpdb;
    
    	$args['fields'] .= ", AVG( $wpdb->commentmeta.meta_value ) as average_rating ";
    
    	$args['where'] .= " AND ( $wpdb->commentmeta.meta_key = 'rating' OR $wpdb->commentmeta.meta_key IS null ) ";
    
    	$args['join'] .= "
    		LEFT OUTER JOIN $wpdb->comments ON($wpdb->posts.ID = $wpdb->comments.comment_post_ID)
    		LEFT JOIN $wpdb->commentmeta ON($wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id)
    	";
    
    	$args['orderby'] = "average_rating DESC, $wpdb->posts.post_date DESC";
    
    	$args['groupby'] = "$wpdb->posts.ID";
    
    	return $args;
    }
}


if ( !function_exists( 'theone_order_by_popularity_post_clauses' ) ) {
    function theone_order_by_popularity_post_clauses( $args ) {
    	global $wpdb;
    
    	$args['orderby'] = "$wpdb->postmeta.meta_value+0 DESC, $wpdb->posts.post_date DESC";
    
    	return $args;
    }   
}


if ( !function_exists( 'theone_get_the_excerpt' ) ) {
    
    function theone_get_the_excerpt( $content = '', $length = 55, $more = '...' ) {
        
        $content = wp_strip_all_tags( strip_shortcodes( $content ), true );
        $length = max( 1, intval( $length ) );
        $excerpt = substr( $content, 0, $length );
        
        if ( strlen( $excerpt ) < strlen( $content ) ):
            $excerpt = $excerpt . $more;
        endif;
        
        return $excerpt;
        
    }
    
}

if ( !function_exists( 'theone_get_img_src_by_id' ) ) {
    
    /**
     *  Return img src 
     **/
    function theone_get_img_src_by_id( $img_id = 0, $size = 'full' ) {
        
        $img_id = max( 0, intval( $img_id ) );
        $src = '';
        
        if ( $img_id > 0 ):
        
            $thumb = wp_get_attachment_image_src( $img_id, $size );
            $src = $thumb['0'];
        
        endif;
        
        return $src;
        
    }
    
}

if ( !function_exists( 'theone_no_image' ) ) {
    
    /**
     * No image generator 
     * @since 1.0
     * @param $size: array, image size
     * @param $echo: bool, echo or return no image url
     **/
    function theone_no_image( $size = array( 'width' => 500, 'height' => 500 ), $echo = false, $transparent = false ) {
        
        $plugin_dir = THEONECORE_DIR_PATH;
        $plugin_uri = THEONECORE_BASE_URL;
        
        $suffix = ( $transparent ) ? '_transparent' : '';
        
        if ( !is_array( $size ) || empty( $size ) ):
            $size = array( 'width' => 500, 'height' => 500 );
        endif;
        
        if ( !is_numeric( $size['width'] ) && $size['width'] == '' || $size['width'] == null ):
            $size['width'] = 'auto';
        endif;
        
        if ( !is_numeric( $size['height'] ) && $size['height'] == '' || $size['height'] == null ):
            $size['height'] = 'auto';
        endif;
        
        // base image must be exist
        $img_base_fullpath = $plugin_dir . '/assets/images/noimage/no_image' . $suffix . '.png';
        $no_image_src = $plugin_uri . '/assets/images/noimage/no_image' . $suffix . '.png';
        
        
        // Check no image exist or not
        if ( !file_exists( $plugin_dir . '/assets/images/noimage/no_image' . $suffix . '-' . $size['width'] . 'x' . $size['height'] . '.png' ) ):
        
            $no_image = wp_get_image_editor( $img_base_fullpath );
            
            if ( !is_wp_error( $no_image ) ):
                $no_image->resize( $size['width'], $size['height'], true );
            	$no_image_name = $no_image->generate_filename( $size['width'] . 'x' . $size['height'], $plugin_dir . '/assets/images/noimage/', null );
                $no_image->save( $no_image_name );
            endif;
            
        endif;
        
        // Check no image exist after resize
        $noimage_path_exist_after_resize = $plugin_dir . '/assets/images/noimage/no_image' . $suffix . '-' . $size['width'] . 'x' . $size['height'] . '.png';
        
        if ( file_exists( $noimage_path_exist_after_resize ) ):
            $no_image_src = $plugin_uri . '/assets/images/noimage/no_image' . $suffix . '-' . $size['width'] . 'x' . $size['height'] . '.png';
        endif;
        
        if ( $echo ):
            echo $no_image_src;
        else:
            return $no_image_src;
        endif;
    
    }
}

if ( !function_exists( 'theone_get_the_excerpt_max_charlength' ) ) {
    function theone_get_the_excerpt_max_charlength( $charlength ) {
    	$excerpt = get_the_excerpt();
        
    	$charlength++;
    
    	if ( mb_strlen( $excerpt ) > $charlength ) {
    		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
    		$exwords = explode( ' ', $subex );
    		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
    		if ( $excut < 0 ) {
    			$subex = mb_substr( $subex, 0, $excut );
    		} 
    		$subex .= '...';
            $excerpt = $subex;
    	} 
        
        return $excerpt;
    }   
}


add_action( 'after_setup_theme', 'ts_init_vc_global', 1 );

function ts_init_vc_global() {
	// Check if Visual Composer is installed
    if ( ! defined( 'WPB_VC_VERSION' ) ) {
        return;
    }

    if ( version_compare( WPB_VC_VERSION, '4.2', '<' ) ) {
        
		add_action( 'init', 'ts_add_vc_global_params', 100 );
        
        add_shortcode_param( 'theone_icon_box_preview', 'theone_icon_box_preview' );
        add_shortcode_param( 'theone_animated_column_preview', 'theone_animated_column_preview' );
        add_shortcode_param( 'theone_select_tes_cat_field', 'theone_select_tes_cat_field' );
        add_shortcode_param( 'theone_select_client_cat_field', 'theone_select_client_cat_field' );
        add_shortcode_param( 'theone_select_animated_column_cat_field', 'theone_select_animated_column_cat_field' );
        add_shortcode_param( 'theone_select_member_cat_field', 'theone_select_member_cat_field' );
        add_shortcode_param( 'theone_quick_edit_animated_columns_field', 'theone_quick_edit_animated_columns_field' );
        
    } else {
        
    	add_action( 'vc_after_mapping', 'ts_add_vc_global_params' );
        
        add_shortcode_param( 'theone_icon_box_preview', 'theone_icon_box_preview' );
        add_shortcode_param( 'theone_animated_column_preview', 'theone_animated_column_preview' );
        add_shortcode_param( 'theone_select_tes_cat_field', 'theone_select_tes_cat_field' );
        add_shortcode_param( 'theone_select_client_cat_field', 'theone_select_client_cat_field' );
        add_shortcode_param( 'theone_select_animated_column_cat_field', 'theone_select_animated_column_cat_field' );
        add_shortcode_param( 'theone_select_member_cat_field', 'theone_select_member_cat_field' );
        add_shortcode_param( 'theone_quick_edit_animated_columns_field', 'theone_quick_edit_animated_columns_field' );
        
    }
}

function ts_add_vc_global_params() {
    
	vc_set_template_dir( THEONECORE_DIR_PATH . '/core/shortcodes/vc_templates/');    
    
    global $vc_setting_row, $vc_setting_col, $vc_setting_column_inner, $vc_setting_icon_shortcode;    
    //vc_add_params( 'vc_row', $vc_setting_row );
    vc_add_params( 'vc_icon', $vc_setting_icon_shortcode );
    vc_add_params( 'vc_column', $vc_setting_col );
    vc_add_params( 'vc_column_inner', $vc_setting_column_inner );
    
    add_shortcode_param( 'theone_select_adv_cat', 'theone_param_select_adv_cat_field' );
    add_shortcode_param( 'theone_param_num_field', 'theone_param_num_field' );
    add_shortcode_param( 'theone_select_product_cat_field', 'theone_param_select_product_cat_field' );
    add_shortcode_param( 'theone_select_cat_field', 'theone_select_cat_field' );
    add_shortcode_param( 'theone_select_portfolio_cat_field', 'theone_select_portfolio_cat_field' );
    
}



//------------------------------//
// Theone shortcode params      //
//------------------------------//

function theone_icon_box_preview( $settings, $value ) {
    
    if ( file_exists( THEONECORE_DIR_PATH . '/assets/images/icon-boxes-preview/' . esc_attr( $value ) . '.jpg' ) ) {
        return '<div class="icon-box-preview" style="padding: 15px; border: 1px solid #ccc; background-color: #efefef;">
                    <img style="max-width: 100%;" src="' . THEONECORE_BASE_URL . '/assets/images/icon-boxes-preview/' . esc_attr( $value ) . '.jpg" alt="' . esc_attr( 'Icon Preview', 'theone-core' ) . '" />
                    <input type="hidden" name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value" value="' . esc_attr( $value ) . '" />
                </div>';
    }
    
    return  '';
    
}

function theone_animated_column_preview( $settings, $value ) {
    
    if ( file_exists( THEONECORE_DIR_PATH . '/assets/images/animated-column-preview/' . esc_attr( $value ) . '.jpg' ) ) {
        $preview_img_src = THEONECORE_BASE_URL . '/assets/images/animated-column-preview/' . esc_attr( $value ) . '.jpg';
        $preview_img_hover_src = THEONECORE_BASE_URL . '/assets/images/animated-column-preview/' . esc_attr( $value ) . '-hover.jpg';
        return '<div class="theone-preview-shortcode" style="padding: 15px; border: 1px solid #ccc; background-color: #efefef;">
                    <img class="preview-img" style="max-width: 100%;" src="' . esc_url( $preview_img_src ) . '" alt="' . esc_attr( 'Preview Image', 'theone-core' ) . '" />
                    <img class="preview-img-hover" style="max-width: 100%;" src="' . esc_url( $preview_img_hover_src ) . '" alt="' . esc_attr( 'Preview Image Hover', 'theone-core' ) . '" />
                    <p class="normal-preview-desc">' . __( 'Normal preview', 'theone-core' ) . '</p>
                    <p class="hover-preview-desc">' . __( 'Hover preview', 'theone-core' ) . '</p>
                    <input type="hidden" name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value" value="' . esc_attr( $value ) . '" />
                </div>';
    }
    
    return  '';
    
}

function theone_select_tes_cat_field( $settings, $value ) {
    
    return  '<div class="select_cat_block">'
                . theone_custom_tax_select( 
                    array( $value ), 
                    array( 
                        'tax'   => 'testimonial_cat',
                        'class' => 'wpb_vc_param_value', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_option_val'  =>  '0',
                        'first_option_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}

function theone_select_client_cat_field( $settings, $value ) {
    
    return  '<div class="select_cat_block">'
                . theone_custom_tax_select( 
                    array( $value ), 
                    array( 
                        'tax'   => 'client_cat',
                        'class' => 'wpb_vc_param_value', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_option_val'  =>  '0',
                        'first_option_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}

function theone_select_animated_column_cat_field( $settings, $value ) {
    
    return  '<div class="select_cat_block">'
                . theone_custom_tax_select( 
                    array( $value ), 
                    array( 
                        'tax'   => 'animated_column_cat',
                        'class' => 'wpb_vc_param_value ts_anim_cat_select', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_option_val'  =>  '0',
                        'first_option_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}

function theone_select_member_cat_field( $settings, $value ) {
    
    return  '<div class="select_cat_block">'
                . theone_custom_tax_select( 
                    array( $value ), 
                    array( 
                        'tax'   => 'member_cat',
                        'class' => 'wpb_vc_param_value', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_option_val'  =>  '0',
                        'first_option_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}


/**
 *  List "animated_column" posts field in the shortcode edit form with VC
 *  Allow quick edit: Excerpt, icon class, read more link, use read more link or permalink, title, thumbnail...
 *  @param $value       string  JSON format, content category id and maximum number of animated columns load (limit)
 *  @param $settings    array
 *  @since 1.0
 **/
function theone_quick_edit_animated_columns_field( $settings, $value ) {
    
    /**
     *  data-edit-for="theone_icon_class" --> meta key is "theone_icon_class"
     **/
    
    $value_default = array(
        'cat_id'    =>  0,
        'limit'     =>  8
    );
    
    $value_bak = $value;
    if ( trim( $value ) == '' ) {
        $value = $value_default;
    }
    else{
        $value = json_decode( $value );
        $value = wp_parse_args( $value, $value_default );
    }
    
    $args = array(
		'post_type' 			=> 'animated_column',
		'post_status' 			=> 'publish',
        'showposts'             => intval( $value['limit'] )
	);
    
    $animated_column_cat_id = intval( $value['cat_id'] );
    if ( $animated_column_cat_id > 0 ):
        
        $args['tax_query'] = array(
            array(
                'taxonomy'  => 'animated_column_cat',
                'field'     => 'id',
                'terms'     => $animated_column_cat_id
            )
        );
        
    endif;
    
    $query = new WP_Query( $args );
    
    $html = '';
    
    if ( $query->have_posts() ) {
        
        while ( $query->have_posts() ): $query->the_post();
            
            $icon_class = get_post_meta( get_the_ID(), 'theone_icon_class', true );
            $bg_color = get_post_meta( get_the_ID(), 'theone_animate_bg_color', true );
            $bg_color = trim( $bg_color ) == '' ? '#dcc6b9' : $bg_color;
            $use_permalink = get_post_meta( get_the_ID(), 'theone_use_permalink', true ) == 'on';
            $custom_link = $use_permalink ? '' : get_post_meta( get_the_ID(), 'theone_animate_link', true );
            $use_permalink_select_id = uniqid( 'use-permalink-' );
            
            $html .= '<h3 class="quick-edit-title quick-edit-title-' . get_the_ID() . '">
                        <span class="accordion-header-title">' . sanitize_text_field( get_the_title() ) . '</span>
                        <i class="theone-icon-acc-header-preview ' . esc_attr( $icon_class ) . '""></i>
                    </h3>';
            $html .= '<div data-post-id="' . get_the_ID() . '" class="ts-quick-edit-post-vc-content">';
            
            $html .= '<div class="ts-quick-edit-field-group vc_column">
                        <div class="wpb_element_label">' . __( 'Title', 'theone-core' ) . '</div>
                        <div class="edit_form_line">
                            <input class="wpb-textinput ts-title-edit ts-quick-edit-field textfield" type="text" value="' . sanitize_text_field( get_the_title() ) . '">
                        </div><!-- /.edit_form_line -->
                     </div><!-- /.ts-quick-edit-field-group -->';
            
            $html .= '<div class="ts-quick-edit-field-group vc_column">
                        <div class="wpb_element_label">' . __( 'Icon', 'theone-core' ) . '</div>
                        <div class="edit_form_line">
                            <input data-edit-for="theone_icon_class" class="wpb-textinput ts-meta-edit ts-quick-edit-field ts-icon-class-input textfield" type="text" value="' . esc_attr( $icon_class ) . '">
                            <span class="vc_description vc_clearfix"><i class="theone-icon-preview ' . esc_attr( $icon_class ) . '"></i>' . __( 'Enter or choose class from the list below', 'theone-core' ) . '</span>
                            <a href="#" class="toggle-iconpicker">' . __( 'Icon picker', 'theone-core' ) . '</a>
                            <div class="iconpicker-wrap">
                                ' . theonecorefont_icons_chooser( '', 1, 96, false, false ) . '
                            </div><!-- /.iconpicker-wrap -->
                        </div><!-- /.edit_form_line -->
                     </div><!-- /.ts-quick-edit-field-group -->';
            
            $html .= '<div class="ts-quick-edit-field-group vc_column">
                        <div class="wpb_element_label">' . __( 'Use Permalink As Read More Link', 'theone-core' ) . '</div>
                        <div class="edit_form_line">
                            <select data-edit-for="theone_use_permalink" id="' . esc_attr( $use_permalink_select_id ) . '" class="ts-meta-edit ts-quick-edit-field ts-select">
                                <option value="">' . __( 'No', 'theone-core' ) . '</option>
                                <option ' . selected( $use_permalink, true, false ) . ' value="on">' . __( 'Yes', 'theone-core' ) . '</option>
                            </select>
                            <span class="vc_description vc_clearfix"></span>
                        </div><!-- /.edit_form_line -->
                     </div><!-- /.ts-quick-edit-field-group -->';
            
            $html .= '<div data-dep-on="#' . esc_attr( $use_permalink_select_id ) . '" data-dep-val="" data-dep-compare="=" class="ts-quick-edit-field-group ts-dependency vc_column">
                        <div class="wpb_element_label">' . __( 'Read More Link', 'theone-core' ) . '</div>
                        <div class="edit_form_line">
                            <input data-edit-for="theone_animate_link" class="wpb-textinput ts-meta-edit ts-quick-edit-field ts-animate-link-input textfield" type="text" value="' . esc_url( $custom_link ) . '">
                            <span class="vc_description vc_clearfix"></span>
                        </div><!-- /.edit_form_line -->
                     </div><!-- /.ts-quick-edit-field-group -->';
            
            $html .= '<div data-dep-on="#vc_edit-form-tabs select[name=\'style\']" data-dep-val="colorful-animate" data-dep-compare="=" class="ts-quick-edit-field-group ts-dependency vc_column">
                        <div class="wpb_element_label">' . __( 'Background Color', 'theone-core' ) . '</div>
                        <div class="edit_form_line">
                            <input data-edit-for="theone_animate_bg_color" class="wpb-textinput ts-meta-edit ts-quick-edit-field ts-animate-bg-color-input ts-color-input ts-color-picker textfield" type="text" value="' . esc_url( $bg_color ) . '">
                            <span class="vc_description vc_clearfix">' . __( 'This configuration only apply for "Style 2"', 'theone-core' ) . '</span>
                        </div><!-- /.edit_form_line -->
                     </div><!-- /.ts-quick-edit-field-group -->';
            
            $html .= '</div><!-- /.ts-quick-edit-post-vc-content -->';
            
        endwhile;
    }
    
    $html = '<div class="ts-quick-edit-posts-for-vc-wrap">' . $html . '</div><!-- /.ts-quick-edit-posts-for-vc-wrap -->';
    
    $html .= '<input data-val-json="' . esc_attr( json_encode($value) ) . '" type="hidden" name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value" value="' . htmlentities2( $value_bak ) . '" />';    
    
    wp_reset_postdata();
    
    return $html;
    
}


function theone_team_member_list( $query_args, $cur_page = 1, $show_pagination = true, $members_per_row = 4, $max_chars = 100 ) {
    
    $html = '';
    $items_html = '';
    $pagination_html = '';
    $pre_page_html = '';
    $next_page_html = '';
    
    $total_query_args = array(
        'post_type'     =>  'member',
        'showposts'     =>  -1,
        'post_status'   =>  array( 'publish' )
    );
    
    if ( isset( $query_args['tax_query'] ) ) {
        $total_query_args['tax_query'] = $query_args['tax_query'];
    }
    
    $post_per_page = isset( $query_args['showposts'] ) ? max( 1, intval( $query_args['showposts'] ) ) : 8;
    $members_per_row = max( 1, intval( $members_per_row ) );
    
    $total_query_posts = new WP_Query( $total_query_args );
    $total_mems = $total_query_posts->found_posts;
    $total_pages = ceil( $total_mems / $post_per_page );
    $cur_page = max( 1, min( intval( $cur_page ), $total_pages ) );
    
    if ( $total_pages > 1 && $show_pagination ) {
        
        if ( $cur_page > 1 ) {
            $pre_page_html .= '<li class="ts-members-pagi-li"><a class="ts-go-to-page" data-go-to-page="' . intval( $cur_page - 1 ) . '" href="#"><i class="fa fa-angle-left"></i></a></li>';
        }
        
        if ( $cur_page < $total_pages ) {
            $next_page_html .= '<li class="ts-members-pagi-li"><a class="ts-go-to-page" data-go-to-page="' . intval( $cur_page + 1 ) . '" href="#"><i class="fa fa-angle-right"></i></a></li>';
        }
        
        $pagination_html .= '<div class="pagination ts-members-pagination">';
        $pagination_html .= '<ul>';
        $pagination_html .= $pre_page_html;
        
        for ( $i = 1; $i <= $total_pages; $i ++ ):
            
            $li_class = 'ts-members-pagi-li';
            $li_class .= ( $i == $cur_page ) ? ' page-active' : '';
            
            $pagination_html .= '<li class="' . esc_attr( $li_class ) . '"><a class="ts-go-to-page" data-go-to-page="' . intval( $i ) . '" href="#">' . intval( $i ) . '</a></li>';
            
        endfor;
        
        $pagination_html .= $next_page_html;
        $pagination_html .= '</ul>';
        $pagination_html .= '</div><!-- /.ts-members-pagination -->';
    }
    
    wp_reset_postdata();
    
    $query_posts = new WP_Query( $query_args );
    if( $query_posts->have_posts() ):
        
        $col_class = '';
        if ( $members_per_row <= 2 ) {
            $col_class = 'col-xs-12 col-sm-6';
        }
        
        if ( $members_per_row == 3 ) {
            $col_class = 'col-xs-12 col-sm-4';
        }
        
        if ( $members_per_row == 4 ) {
            $col_class = 'col-xs-12 col-sm-6 col-md-3';
        }
        
        while ( $query_posts->have_posts() ) : $query_posts->the_post(); 
            
            $post_id = get_the_ID();
            $excerpt = theone_get_the_excerpt_max_charlength( $max_chars ); 
            
            $thumb_src = '';
            if ( has_post_thumbnail() ) {
                $thumb_src = theone_get_img_src_by_id( get_post_thumbnail_id(), '536x588' );
            }
            else{
                $thumb_src = theone_no_image( array( 'width' => 536, 'height' => 588 ), false, false );
            }
            
            $mem_position = get_post_meta( $post_id, 'theone_member_position', true );
            
            $member_links_html = '';
            
            $fb_link = get_post_meta( $post_id, 'theone_fb_link', true );
            $tw_link = get_post_meta( $post_id, 'theone_tw_link', true );
            $gplus_link = get_post_meta( $post_id, 'theone_gplus_link', true );
            $in_link = get_post_meta( $post_id, 'theone_in_link', true );
            $yt_link = get_post_meta( $post_id, 'theone_yt_link', true );
            $vimeo_link = get_post_meta( $post_id, 'theone_vimeo_link', true );
            $pinterest_link = get_post_meta( $post_id, 'theone_pinterest_link', true );
            
            if ( trim( $fb_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $fb_link ) . '"><i class="fa fa-facebook"></i></a>';
            }
            if ( trim( $tw_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $tw_link ) . '"><i class="fa fa-twitter"></i></a>';
            }
            if ( trim( $gplus_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $gplus_link ) . '"><i class="fa fa-google-plus"></i></a>';
            }
            if ( trim( $in_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $in_link ) . '"><i class="fa fa-linkedin"></i></a>';
            }
            if ( trim( $yt_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $yt_link ) . '"><i class="fa fa-youtube"></i></a>';
            }
            if ( trim( $vimeo_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $vimeo_link ) . '"><i class="fa fa-vimeo-square"></i></a>';
            }
            if ( trim( $pinterest_link ) != '' ) {
                $member_links_html .= '<a target="_blank" href="' . esc_attr( $pinterest_link ) . '"><i class="fa fa-pinterest"></i></a>';
            }
            
            $items_html .= '<div class="team-item ' . $col_class . '">
            					<div class="team-item-inner">
                                    <div class="team-thumb">
                						<img src="' . esc_url( $thumb_src ) . '" alt="' . esc_attr( get_the_title() ) . '">
                                        <div class="ts-team-overlay">
                                            <p>' . sanitize_text_field( $excerpt ) . '</p>
                                            <div class="team-icons">
                                                ' . $member_links_html . '
                                            </div>
                                        </div><!-- /.ts-team-overlay -->
                					</div><!-- /.team-thumb -->
                					<div class="info-team">
                						<h5>' . get_the_title() . '</h5>
                						<div class="team-company">' . sanitize_text_field( $mem_position ) . '</div>
                					</div><!-- /.info-team -->
                                </div><!-- /.team-item-inner -->
            				</div><!-- /.team-item -->';
            
        endwhile;
        
        $html .=    '<div class="ts-team">
                        <div class="row">
                            ' . $items_html . '
                        </div><!-- /.row -->
                        ' . $pagination_html . '
                    </div><!-- /.ts-team -->';
        
    endif;
    
    wp_reset_postdata();
    
    return $html;
    
}


function ts_load_team_members_via_ajax() {
    
    $member_cat_id = isset( $_POST['mem_cat_id'] ) ? max( 0, intval( $_POST['mem_cat_id'] ) ) : 0;
    $post_per_page = isset( $_POST['post_per_page'] ) ? max( 1, intval( $_POST['post_per_page'] ) ) : 8; // limit
    $mem_per_row = isset( $_POST['mem_per_row'] ) ? max( 1, intval( $_POST['mem_per_row'] ) ) : 4;
    $max_chars = isset( $_POST['max_chars'] ) ? max( 1, intval( $_POST['max_chars'] ) ) : 100;
    $go_to_page = isset( $_POST['go_to_page'] ) ? max( 1, intval( $_POST['go_to_page'] ) ) : 1;
    
    $query_args = array(
        'post_type'     =>  'member',
        'showposts'     =>  $post_per_page,
        'post_status'   =>  array( 'publish' ),
        'paged'         =>  $go_to_page
    );
    
    if ( $member_cat_id > 0 ):
    
        $query_args['tax_query'] = array(
    		array(
    			'taxonomy'   => 'member_cat',
    			'field'      => 'ids',
    			'terms'      => $member_cat_id
    		)
    	);
        
    endif;
    
    $response = array(
        'html' => ''
    );
    
    $response['html'] = theone_team_member_list( $query_args, $go_to_page, true, $mem_per_row, $max_chars );
    
    wp_send_json( $response );
    
    die();
}
add_action( 'wp_ajax_ts_load_team_members_via_ajax', 'ts_load_team_members_via_ajax' );
add_action( 'wp_ajax_nopriv_ts_load_team_members_via_ajax', 'ts_load_team_members_via_ajax' );


/**
 * Convert color hex to rgba format 
 **/
if ( !function_exists( 'ts_color_hex2rgba' ) ) {
    function ts_color_hex2rgba( $hex, $alpha = 1 ) {
        $hex = str_replace( "#", "", $hex );
        
        if( strlen($hex) == 3 ) {
          $r = hexdec( substr( $hex, 0, 1 ).substr( $hex, 0, 1 ) );
          $g = hexdec( substr( $hex, 1, 1 ).substr( $hex, 1, 1 ) );
          $b = hexdec( substr( $hex, 2, 1 ).substr( $hex, 2, 1 ) );
        } else {
          $r = hexdec( substr( $hex, 0, 2 ) );
          $g = hexdec( substr( $hex, 2, 2 ) );
          $b = hexdec( substr( $hex, 4, 2 ) );
        }
        $rgb = array( $r,  $g,  $b );
        return 'rgba( ' . implode( ', ', $rgb ) . ', ' . $alpha . ' )'; // returns the rgb values separated by commas
    }
}

if ( !function_exists( 'ts_color_rgb2hex' ) ) {
    function ts_color_rgb2hex( $rgb ) {
       $hex = '#';
       $hex .= str_pad( dechex($rgb[0]), 2, '0', STR_PAD_LEFT );
       $hex .= str_pad( dechex($rgb[1]), 2, '0', STR_PAD_LEFT );
       $hex .= str_pad( dechex($rgb[2]), 2, '0', STR_PAD_LEFT );
    
       return $hex; // returns the hex value including the number sign (#)
    }
}

if ( !function_exists( 'ts_rev_sliders_args' ) ) {
    /**
     *  Return args of Revolution sliders
     *  @since 1.0 
     **/
    function ts_rev_sliders_args() {
        global $wpdb;
        
        $args = array();
        
        if ( shortcode_exists( 'rev_slider' ) ) {
            
            $sql =  "SELECT rs.title, rs.alias " . 
                    "FROM {$wpdb->prefix}revslider_sliders rs " . 
                    "ORDER BY rs.title ASC ";
            
            $rows = $wpdb->get_results( $sql );
            
            if ( count( $rows ) > 0 ) {
                
                foreach ( $rows as $row ):
                    
                    $args[esc_attr( $row->alias )] = sanitize_text_field( $row->title );
                
                endforeach;
                
            }
            
        }
        
        return $args;
        
    }
}


// New VC field types ================
// https://wpbakery.atlassian.net/wiki/display/VC/Create+New+Param+Type
function theone_param_select_adv_cat_field( $settings, $value ) {
    
    $terms = get_terms( 'adv_cat', array( 'hide_empty' => false ) );
    $select_html = '<select name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value" >';
    $select_html .= '<option value="0">' . __( 'All Adv Categories', 'theone-core' ) . '</option>';
    
    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ):
        
        foreach ( $terms as $term ):
            
            $select_html .= '<option ' . selected( esc_attr( $value ) == esc_attr( $term->term_id ), true, false ) . ' value="' . esc_attr( $term->term_id ) . '">' . $term->name . '</option>';
            
        endforeach;
        
    endif;
    
    $select_html .= '</select>';
    
    return  '<div class="select_adv_cat_block">'
                . $select_html
            . '</div>'; 
}

function theone_param_num_field( $settings, $value ) {
    
    $value = max( 1, intval( $value ) );
    
    return  '<div>
                <input type="text" name="' . esc_attr( $settings['param_name'] ) . '" class="wpb_vc_param_value nl-num-input" value="' . esc_attr( $value ) . '" />
            </div>'; 
}


function theone_param_select_product_cat_field( $settings, $value ) {
    
    return  '<div class="select_adv_cat_block">'
                . theone_procats_select( 
                    array( $value ), 
                    array( 
                        'class' => 'wpb_vc_param_value', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_select_val'  =>  '0',
                        'first_select_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}

function theone_select_cat_field( $settings, $value ) {
    
    return  '<div class="select_adv_cat_block">'
                . theone_custom_tax_select( 
                    array( $value ), 
                    array( 
                        'tax'   => 'category',
                        'class' => 'wpb_vc_param_value', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_option_val'  =>  '0',
                        'first_option_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}

function theone_select_portfolio_cat_field( $settings, $value ) {
    
    return  '<div class="select_adv_cat_block">'
                . theone_custom_tax_select( 
                    array( $value ), 
                    array( 
                        'tax'   => 'portfolio_cat',
                        'class' => 'wpb_vc_param_value', 
                        'name'  => $settings['param_name'],
                        'first_option' => true,
                        'first_option_val'  =>  '0',
                        'first_option_text' =>  __( ' --- All Categories --- ', 'theone-core' )
                    ) 
                )
            . '</div>'; 
}



if ( ! function_exists( 'theone_get_the_category_list' ) ) {
    
    /**
     * Retrieve category list in either HTML list or custom format. Modified from get_the_category_list()
     *
     * @since 1.0 
     *
     * @param string $separator Optional, default is empty string. Separator for between the categories.
     * @param string $parents Optional. How to display the parents.
     * @param int $post_id Optional. Post ID to retrieve categories.
     * @return string
     */
    function theone_get_the_category_list( $separator = '', $parents='', $post_id = false ) {
    	global $wp_rewrite;
    	if ( ! is_object_in_taxonomy( get_post_type( $post_id ), 'category' ) ) {
    		/** This filter is documented in wp-includes/category-template.php */
    		return apply_filters( 'the_category', '', $separator, $parents );
    	}
    
    	$categories = get_the_category( $post_id );
    	if ( empty( $categories ) ) {
    		/** This filter is documented in wp-includes/category-template.php */
    		return apply_filters( 'the_category', __( 'Uncategorized' ), $separator, $parents );
    	}
    
    	$rel = ( is_object( $wp_rewrite ) && $wp_rewrite->using_permalinks() ) ? 'rel="category tag"' : 'rel="category"';
    
    	$thelist = '';
    	if ( '' == $separator ) {
    		foreach ( $categories as $category ) {
    			$thelist .= "\n\t";
    			switch ( strtolower( $parents ) ) {
    				case 'multiple':
    					if ( $category->parent )
    						$thelist .= get_category_parents( $category->parent, true, $separator );
    					$thelist .= '<a class="name-category" href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name.'</a>';
    					break;
    				case 'single':
    					$thelist .= '<a class="name-category" href="' . esc_url( get_category_link( $category->term_id ) ) . '"  ' . $rel . '>';
    					if ( $category->parent )
    						$thelist .= get_category_parents( $category->parent, false, $separator );
    					$thelist .= $category->name.'</a>';
    					break;
    				case '':
    				default:
    					$thelist .= '<a class="name-category" href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name.'</a>';
    			}
    		}
    	} else {
    		$i = 0;
    		foreach ( $categories as $category ) {
    			if ( 0 < $i )
    				$thelist .= $separator;
    			switch ( strtolower( $parents ) ) {
    				case 'multiple':
    					if ( $category->parent )
    						$thelist .= get_category_parents( $category->parent, true, $separator );
    					$thelist .= '<a class="name-category" href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name.'</a>';
    					break;
    				case 'single':
    					$thelist .= '<a class="name-category" href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>';
    					if ( $category->parent )
    						$thelist .= get_category_parents( $category->parent, false, $separator );
    					$thelist .= "$category->name</a>";
    					break;
    				case '':
    				default:
    					$thelist .= '<a class="name-category" href="' . esc_url( get_category_link( $category->term_id ) ) . '" ' . $rel . '>' . $category->name.'</a>';
    			}
    			++$i;
    		}
    	}
    
    	/**
    	 * Filter the category or list of categories.
    	 *
    	 * @since 1.2.0
    	 *
    	 * @param array  $thelist   List of categories for the current post.
    	 * @param string $separator Separator used between the categories.
    	 * @param string $parents   How to display the category parents. Accepts 'multiple',
    	 *                          'single', or empty.
    	 */
    	return apply_filters( 'the_category', $thelist, $separator, $parents );
    }
    
}

if ( ! function_exists( 'theone_resize_image' ) ) {
	/**
	 * @param int $attach_id
	 * @param string $img_url
	 * @param int $width
	 * @param int $height
	 * @param bool $crop
     * @param bool $place_hold          Using place hold image if the image does not exist
     * @param bool $use_real_img_hold   Using real image for holder if the image does not exist
	 *
	 * @since 1.0
	 * @return array
	 */
	function theone_resize_image( $attach_id = null, $img_url = null, $width, $height, $crop = false, $place_hold = true, $use_real_img_hold = true ) {
		// this is an attachment, so we have the ID
		$image_src = array();
		if ( $attach_id ) {
			$image_src = wp_get_attachment_image_src( $attach_id, 'full' );
			$actual_file_path = get_attached_file( $attach_id );
			// this is not an attachment, let's use the image url
		} else if ( $img_url ) {
			$file_path = str_replace( get_site_url(), get_home_path(), $img_url );
            $actual_file_path = rtrim( $file_path, '/' );
            if ( !file_exists( $actual_file_path ) ) {
                $file_path = parse_url( $img_url );
                $actual_file_path = rtrim( ABSPATH, '/' ) . $file_path['path'];
            } 
			$orig_size = getimagesize( $actual_file_path );
			$image_src[0] = $img_url;
			$image_src[1] = $orig_size[0];
			$image_src[2] = $orig_size[1];
		}
		if ( ! empty( $actual_file_path ) && file_exists( $actual_file_path ) ) {
			$file_info = pathinfo( $actual_file_path );
			$extension = '.' . $file_info['extension'];

			// the image path without the extension
			$no_ext_path = $file_info['dirname'] . '/' . $file_info['filename'];

			$cropped_img_path = $no_ext_path . '-' . $width . 'x' . $height . $extension;

			// checking if the file size is larger than the target size
			// if it is smaller or the same size, stop right here and return
			if ( $image_src[1] > $width || $image_src[2] > $height ) {

				// the file is larger, check if the resized version already exists (for $crop = true but will also work for $crop = false if the sizes match)
				if ( file_exists( $cropped_img_path ) ) {
					$cropped_img_url = str_replace( basename( $image_src[0] ), basename( $cropped_img_path ), $image_src[0] );
					$vt_image = array(
						'url' => $cropped_img_url,
						'width' => $width,
						'height' => $height
					);

					return $vt_image;
				}

				// $crop = false
				if ( $crop == false ) {
					// calculate the size proportionaly
					$proportional_size = wp_constrain_dimensions( $image_src[1], $image_src[2], $width, $height );
					$resized_img_path = $no_ext_path . '-' . $proportional_size[0] . 'x' . $proportional_size[1] . $extension;

					// checking if the file already exists
					if ( file_exists( $resized_img_path ) ) {
						$resized_img_url = str_replace( basename( $image_src[0] ), basename( $resized_img_path ), $image_src[0] );

						$vt_image = array(
							'url' => $resized_img_url,
							'width' => $proportional_size[0],
							'height' => $proportional_size[1]
						);

						return $vt_image;
					}
				}

				// no cache files - let's finally resize it
				$img_editor = wp_get_image_editor( $actual_file_path );

				if ( is_wp_error( $img_editor ) || is_wp_error( $img_editor->resize( $width, $height, $crop ) ) ) {
					return array(
						'url' => '',
						'width' => '',
						'height' => ''
					);
				}

				$new_img_path = $img_editor->generate_filename();

				if ( is_wp_error( $img_editor->save( $new_img_path ) ) ) {
					return array(
						'url' => '',
						'width' => '',
						'height' => ''
					);
				}
				if ( ! is_string( $new_img_path ) ) {
					return array(
						'url' => '',
						'width' => '',
						'height' => ''
					);
				}

				$new_img_size = getimagesize( $new_img_path );
				$new_img = str_replace( basename( $image_src[0] ), basename( $new_img_path ), $image_src[0] );

				// resized output
				$vt_image = array(
					'url' => $new_img,
					'width' => $new_img_size[0],
					'height' => $new_img_size[1]
				);

				return $vt_image;
			}

			// default output - without resizing
			$vt_image = array(
				'url' => $image_src[0],
				'width' => $image_src[1],
				'height' => $image_src[2]
			);

			return $vt_image;
		}
        else {
            if ( $place_hold ) {
                $width = intval( $width );
                $height = intval( $height );
                
                // Real image place hold (https://unsplash.it/)
                if ( $use_real_img_hold ) {
                    $random_time = time() + rand( 1, 100000 );
                    $vt_image = array(
        				'url' => 'https://unsplash.it/' . $width . '/' . $height . '?random&time=' . $random_time,
        				'width' => $width,
        				'height' => $height
        			); 
                }
                else{
                    // Random color
                    $color = str_pad( dechex( mt_rand( 1, 255 ) ), 2, '0', STR_PAD_LEFT ) . str_pad( dechex( mt_rand( 1, 255 ) ), 2, '0', STR_PAD_LEFT ) . str_pad( dechex( mt_rand( 1, 255 ) ), 2, '0', STR_PAD_LEFT );
                    $vt_image = array(
        				'url' => 'http://placehold.it/' . $width . 'x' . $height . '/' . $color . '/ffffff/',
        				'width' => $width,
        				'height' => $height
        			);   
                }
                
                return $vt_image;
            }
        }

		return false;
	}
}


function theone_quick_edit_post_via_ajax() {
    
    $response = array(
        'html'      =>  '',
        'message'   =>  ''
    );
    
    // Only admin can quick edit
    if ( !current_user_can( 'manage_options' ) ) {
        $response['message'] = __( 'You have not permission to edit this post', 'theone-core' );
        wp_send_json( $response );
    }
    
    $ts_vc_edit_nonce = isset( $_POST['ts_vc_edit_nonce'] ) ? $_POST['ts_vc_edit_nonce'] : '';
    
    if ( !wp_verify_nonce( $ts_vc_edit_nonce, 'ts_vc_edit_nonce' ) ) {
        $response['message'] = __( 'Security check error. Please re-loggin.', 'theone-core' );
        wp_send_json( $response );
    }
    
    $is_edit_title = isset( $_POST['is_edit_title'] ) ? $_POST['is_edit_title'] == 'yes' : false;
    $post_id = isset( $_POST['post_id'] ) ? intval( $_POST['post_id'] ) : 0;
    $new_val = isset( $_POST['new_val'] ) ? $_POST['new_val'] : '';
    $edit_for = isset( $_POST['edit_for'] ) ? $_POST['edit_for'] : ''; // Should be post meta key if is not title edit
    
    if ( $is_edit_title ) {
        $post_args = array(
            'ID'            =>  $post_id,
            'post_title'    =>  $new_val
        );
        wp_update_post( $post_args );
        $response['message'] = __( 'Title updated', 'theone-core' );
    }
    else{
        update_post_meta( $post_id, $edit_for, $new_val );
        $response['message'] = __( 'Post meta data updated', 'theone-core' );
    }
    
    wp_send_json( $response );
    
    die();
}
add_action( 'wp_ajax_theone_quick_edit_post_via_ajax', 'theone_quick_edit_post_via_ajax' );


function theone_load_quick_edit_animated_posts_via_ajax() {
    
    $response = array(
        'html'      =>  '',
        'message'   =>  ''
    );
    
    // Only admin can quick edit
    if ( !current_user_can( 'manage_options' ) ) {
        $response['message'] = __( 'You have not permission to edit this post', 'theone-core' );
        wp_send_json( $response );
    }
    
    $ts_vc_edit_nonce = isset( $_POST['ts_vc_edit_nonce'] ) ? $_POST['ts_vc_edit_nonce'] : '';
    
    if ( !wp_verify_nonce( $ts_vc_edit_nonce, 'ts_vc_edit_nonce' ) ) {
        $response['message'] = __( 'Security check error. Please re-loggin.', 'theone-core' );
        wp_send_json( $response );
    }
    
    $quick_edit_items_data = isset( $_POST['quick_edit_items_data'] ) ? urldecode( $_POST['quick_edit_items_data'] ) : '';
        
    
    $settings = array(
        'param_name' => 'quick_edit_items'
    );
    
    $response['html'] .= theone_quick_edit_animated_columns_field( $settings, $quick_edit_items_data );
    
    wp_send_json( $response );
    
    die();
    
}
add_action( 'wp_ajax_theone_load_quick_edit_animated_posts_via_ajax', 'theone_load_quick_edit_animated_posts_via_ajax' );

