<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
    /*
     * Register post type client
    */
    function theone_clients_post_type() 
    {
        $labels = array(
            'name' 					=> __('Clients', 'post type general name', 'theone-core'),
            'singular_name' 		=> __('Client', 'post type singular name', 'theone-core'),
            'add_new' 				=> __('Add New', 'theone-core'),
            'all_items' 			=> __('All Clients', 'theone-core'),
            'add_new_item' 			=> __('Add New Client', 'theone-core'),
            'edit_item' 			=> __('Edit Client', 'theone-core'),
            'new_item' 				=> __('New Client', 'theone-core'),
            'view_item' 			=> __('View Client', 'theone-core'),
            'search_items' 			=> __('Search Clients', 'theone-core'),
            'not_found' 			=> __('No Client Found', 'theone-core'),
            'not_found_in_trash' 	=> __('No Client Found in Trash', 'theone-core'), 
            'parent_item_colon' 	=> '',
        );
        
        $args = array(
            'labels' 			=> $labels,
            'public' 			=> true,
            'show_ui' 			=> true,
            'capability_type' 	=> 'post',
            'hierarchical' 		=> false,
            'rewrite' 			=> array('slug' => 'clients', 'with_front' => true),
            'query_var' 		=> true,
            'show_in_nav_menus' => false,
            'menu_icon' 		=> 'dashicons-awards',
            'supports' 			=> array( 'title', 'thumbnail', 'author', 'editor' ),
        );
        
        register_post_type( 'client' , $args );  
        
    }
    add_action('init', 'theone_clients_post_type');
?>