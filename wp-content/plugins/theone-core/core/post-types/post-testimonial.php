<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
    /*
     * Register post type testimonial
    */
    function theone_testimonials_post_type() 
    {
        $labels = array(
            'name' 					=> __('Testimonials', 'post type general name', 'theone-core'),
            'singular_name' 		=> __('Testimonial', 'post type singular name', 'theone-core'),
            'add_new' 				=> __('Add New', 'theone-core'),
            'all_items' 			=> __('All Testimonials', 'theone-core'),
            'add_new_item' 			=> __('Add New Testimonial', 'theone-core'),
            'edit_item' 			=> __('Edit Testimonial', 'theone-core'),
            'new_item' 				=> __('New Testimonial', 'theone-core'),
            'view_item' 			=> __('View Testimonial', 'theone-core'),
            'search_items' 			=> __('Search Testimonials', 'theone-core'),
            'not_found' 			=> __('No Testimonial Found', 'theone-core'),
            'not_found_in_trash' 	=> __('No Testimonial Found in Trash', 'theone-core'), 
            'parent_item_colon' 	=> '',
        );
        
        $args = array(
            'labels' 			=> $labels,
            'public' 			=> true,
            'show_ui' 			=> true,
            'capability_type' 	=> 'post',
            'hierarchical' 		=> false,
            'rewrite' 			=> array('slug' => 'testimonials', 'with_front' => true),
            'query_var' 		=> true,
            'show_in_nav_menus' => false,
            'menu_icon' 		=> 'dashicons-format-quote',
            'supports' 			=> array('title', 'thumbnail', 'excerpt', 'editor', 'author',),
        );
        
        register_post_type( 'testimonial' , $args );  
        
        //register_taxonomy(' testimonial_cat', 
//            array( 'testimonial' ), 
//            array(
//                'hierarchical' 		=> true, 
//                'label' 			=> __( 'Testimonial Categories' ),
//                'show_admin_column'	=> true,
//                'singular_label' 	=> 'Testimonial Category', 
//                'rewrite' 			=> true,
//                'query_var' 		=> true,
//            )
//        );
    }
    add_action('init', 'theone_testimonials_post_type');
?>