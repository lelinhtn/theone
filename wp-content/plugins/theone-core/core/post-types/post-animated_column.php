<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
    /*
     * Register post type animated_column
    */
    function theone_animated_columns_post_type() 
    {
        $labels = array(
            'name' 					=> __('Animated Columns', 'post type general name', 'theone-core'),
            'singular_name' 		=> __('Animated Column', 'post type singular name', 'theone-core'),
            'add_new' 				=> __('Add New', 'testmonials', 'theone-core'),
            'all_items' 			=> __('All Animated Columns', 'theone-core'),
            'add_new_item' 			=> __('Add New Animated Column', 'theone-core'),
            'edit_item' 			=> __('Edit Animated Column', 'theone-core'),
            'new_item' 				=> __('New Animated Column', 'theone-core'),
            'view_item' 			=> __('View Animated Column', 'theone-core'),
            'search_items' 			=> __('Search Animated Columns', 'theone-core'),
            'not_found' 			=> __('No Animated Column Found', 'theone-core'),
            'not_found_in_trash' 	=> __('No Animated Column Found in Trash', 'theone-core'), 
            'parent_item_colon' 	=> '',
        );
        
        $args = array(
            'labels' 			=> $labels,
            'public' 			=> true,
            'show_ui' 			=> true,
            'capability_type' 	=> 'post',
            'hierarchical' 		=> false,
            'rewrite' 			=> array('slug' => 'animated_columns', 'with_front' => true),
            'query_var' 		=> true,
            'show_in_nav_menus' => false,
            'menu_icon' 		=> 'dashicons-screenoptions',
            'supports' 			=> array( 'title', 'thumbnail', 'author', 'editor', 'excerpt' ),
        );
        
        register_post_type( 'animated_column' , $args );  
        
    }
    add_action('init', 'theone_animated_columns_post_type');
?>