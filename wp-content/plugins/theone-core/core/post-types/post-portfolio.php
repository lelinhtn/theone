<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
    /*
     * Register post type portfolio
    */
    function theone_portfolios_post_type() 
    {
        $labels = array(
            'name' 					=> __('Portfolios', 'post type general name', 'theone-core'),
            'singular_name' 		=> __('Portfolio', 'post type singular name', 'theone-core'),
            'add_new' 				=> __('Add New', 'theone-core'),
            'all_items' 			=> __('All Portfolios', 'theone-core'),
            'add_new_item' 			=> __('Add New Portfolio', 'theone-core'),
            'edit_item' 			=> __('Edit Portfolio', 'theone-core'),
            'new_item' 				=> __('New Portfolio', 'theone-core'),
            'view_item' 			=> __('View Portfolio', 'theone-core'),
            'search_items' 			=> __('Search Portfolios', 'theone-core'),
            'not_found' 			=> __('No Portfolio Found', 'theone-core'),
            'not_found_in_trash' 	=> __('No Portfolio Found in Trash', 'theone-core'), 
            'parent_item_colon' 	=> '',
        );
        
        $args = array(
            'labels' 			=> $labels,
            'public' 			=> true,
            'show_ui' 			=> true,
            'capability_type' 	=> 'post',
            'hierarchical' 		=> false,
            'rewrite' 			=> array('slug' => 'portfolios', 'with_front' => true),
            'query_var' 		=> true,
            'show_in_nav_menus' => false,
            'menu_icon' 		=> 'dashicons-schedule',
            'supports' 			=> array('title', 'thumbnail', 'excerpt', 'editor', 'author',),
        );
        
        register_post_type( 'portfolio' , $args );  
        
    }
    add_action('init', 'theone_portfolios_post_type');
?>