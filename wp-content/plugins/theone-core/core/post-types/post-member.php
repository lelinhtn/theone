<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
    /*
     * Register post type member
    */
    function theone_members_post_type() 
    {
        $labels = array(
            'name' 					=> __('Members', 'post type general name', 'theone-core'),
            'singular_name' 		=> __('Member', 'post type singular name', 'theone-core'),
            'add_new' 				=> __('Add New', 'theone-core'),
            'all_items' 			=> __('All Members', 'theone-core'),
            'add_new_item' 			=> __('Add New Member', 'theone-core'),
            'edit_item' 			=> __('Edit Member', 'theone-core'),
            'new_item' 				=> __('New Member', 'theone-core'),
            'view_item' 			=> __('View Member', 'theone-core'),
            'search_items' 			=> __('Search Members', 'theone-core'),
            'not_found' 			=> __('No Member Found', 'theone-core'),
            'not_found_in_trash' 	=> __('No Member Found in Trash', 'theone-core'), 
            'parent_item_colon' 	=> '',
        );
        
        $args = array(
            'labels' 			=> $labels,
            'public' 			=> true,
            'show_ui' 			=> true,
            'capability_type' 	=> 'post',
            'hierarchical' 		=> false,
            'rewrite' 			=> array('slug' => 'members', 'with_front' => true),
            'query_var' 		=> true,
            'show_in_nav_menus' => false,
            'menu_icon' 		=> 'dashicons-groups',
            'supports' 			=> array( 'title', 'thumbnail', 'author', 'editor' ),
        );
        
        register_post_type( 'member' , $args );  
        
    }
    add_action('init', 'theone_members_post_type');
?>