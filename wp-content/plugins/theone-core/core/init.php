<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Load Global Linea Font Icons ($linea_fonts)
 */
if ( file_exists( THEONECORE_CORE . 'linea-font-icons.php' ) ) {
    require_once THEONECORE_CORE . 'linea-font-icons.php';
} 


add_action( 'init', 'theone_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function theone_initialize_cmb_meta_boxes() {
    
	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once THEONECORE_LIBS . '/admin/metaboxes/init.php';;

}

/**
 * Load custom post type 
 */
 $postTypeArgs = array( 'testimonial', 'client', 'member', 'portfolio', 'animated_column' ); 
 if ( is_array( $postTypeArgs ) && !empty( $postTypeArgs ) ):
    foreach ( $postTypeArgs as $postType ):
        $postType = sanitize_key( $postType );
        $filePath = THEONECORE_CORE . 'post-types/post-' . $postType . '.php';
        if ( file_exists( $filePath ) ):
            include_once $filePath;
        endif;
    endforeach;
 endif;
 
/**
 * Load custom taxonomies 
 */
 $taxonomiesArgs = array( 'brand', 'designer', 'testimonial', 'client', 'member', 'portfolio', 'animated_column' ); 
 if ( is_array( $taxonomiesArgs ) && !empty( $taxonomiesArgs ) ):
    foreach ( $taxonomiesArgs as $taxonomy ):
        $taxonomy = sanitize_key( $taxonomy );
        $filePath = THEONECORE_CORE . 'taxonomies/taxonomy-' . $taxonomy . '.php';
        if ( file_exists( $filePath ) ):
            include_once $filePath;
        endif;
    endforeach;
 endif;
 
/**
 * Load Post type metaboxes 
 */
 include_once THEONECORE_CORE . 'metaboxes/post-type-metaboxes/global-metaboxes.php';
 
 $postTypeMetaboxesArgs = array( 'testimonial', 'client', 'member', 'animated_column' ); 
 if ( is_array( $postTypeMetaboxesArgs ) && !empty( $postTypeMetaboxesArgs ) ):
    foreach ( $postTypeMetaboxesArgs as $post_type ):
        $post_type = sanitize_key( $post_type );
        $filePath = THEONECORE_CORE . 'metaboxes/post-type-metaboxes/metaboxes-' . $post_type . '.php';
        if ( file_exists( $filePath ) ):
            include_once $filePath;
        endif; 
    endforeach;
 endif;
 
/**
 * Load Taxonomies metaboxes 
 */
 $taxonomyMetaboxesArgs = array( 'brand', 'designer', 'pa_color' ); 
 if ( is_array( $taxonomyMetaboxesArgs ) && !empty( $taxonomyMetaboxesArgs ) ):
    foreach ( $taxonomyMetaboxesArgs as $taxonomy ):
        $taxonomy = sanitize_key( $taxonomy );
        $filePath = THEONECORE_CORE . 'metaboxes/taxonomy-metaboxes/metaboxes-tax-' . $taxonomy . '.php';
        if ( file_exists( $filePath ) ):
            include_once $filePath;
        endif;
    endforeach;
 endif;
 
/**
 * Load Global Functions
 */
if ( file_exists( THEONECORE_CORE . 'functions.php' ) ) {
    require_once THEONECORE_CORE . 'functions.php';
} 

/**
 * Load VC Global Custom
 */
if ( file_exists( THEONECORE_CORE . 'shortcodes/ts_vc_global.php' ) ) {
    require_once THEONECORE_CORE . 'shortcodes/ts_vc_global.php';
} 
/**
 * Load Shortcode
 */
if ( file_exists( THEONECORE_CORE . 'shortcodes/shortcodes.php' ) ) {
    require_once THEONECORE_CORE . 'shortcodes/shortcodes.php';
}

/**
 * Load VC Map
 */
if ( file_exists( THEONECORE_CORE . 'shortcodes/vc_map.php' ) ) {
    require_once THEONECORE_CORE . 'shortcodes/vc_map.php';
}
?>