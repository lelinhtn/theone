<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_advertise_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_advertise_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'theone_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['advertise_metabox'] = array(

	  'title' => __( 'Advertise Metas', 'theone-core' ),
	  'pages' => array( 'advertise' ),
	  'context'    => 'normal',
	  'id'         => $prefix . 'advertise_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
		       'name' => __( 'Use permalink', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'use_permalink',
		       'type' => 'checkbox'
		   	),
            array(
		       'name' => __( 'Advertise Link', 'theone-core' ),
		       'desc' => __( 'Use custom advertise link if "Use permalink" is not checked', 'theone-core' ),
		       'id' => $prefix . 'advertise_link',
		       'type' => 'text_url'
		   	)
	  	)

	); 

	return $meta_boxes;
}

