<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */
  

add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'fashion_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['post_metabox'] = array(

	  'title' => 'Post metas',
	  'pages' => array('post'),
	  'context'    => 'normal',
	  'id'         => $prefix . 'post_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
		   	array(
		       'name' => 'Image gallery',
		       'desc' => '',
		       'id' => $prefix .'image_gallery',
		       'type' => 'file_list',
		       'preview_size' => array( 100, 100 ), // Default: array( 50, 50 )
		   	),
		   	array(
		       'name' => 'Post image',
		       'desc' => 'Upload an image or enter an URL.',
		       'id' => $prefix . 'post_image',
		       'type' => 'file',
		       'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
		   	),
		   	array(
			    'name' => __( 'Video Embed', 'theonefashion' ),
			    'desc' => __( 'Enter a youtube, twitter, or instagram URL. Supports services listed at <a href="http://codex.wordpress.org/Embeds">http://codex.wordpress.org/Embeds</a>.', 'theonefashion' ),
			    'id'   => $prefix . 'post_video_embed',
			    'type' => 'oembed',
		   	),
		   	array(
			    'name' => __( 'Audio Embed', 'theonefashion' ),
			    'desc' => __( 'Enter a soundcloud URL.', 'theonefashion' ),
			    'id'   => $prefix . 'post_audio_embed',
			    'type' => 'oembed',
		   	),
	  	)

	); 

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['our_team_metabox'] = array(
		'id'         => $prefix. '_our_team_metabox',
		'title'      => __( 'Our Team Metas', 'theonefashion' ),
		'pages'      => array( 'our_team' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		// 'cmb_styles' => true, // Enqueue the CMB stylesheet on the frontend
		'fields'     => array(
			array(
				'name' => __( 'Job', 'theonefashion' ),
				'desc' => __( 'field description (optional)', 'theonefashion' ),
				'id'   => $prefix . 'our_team_job',
				'type' => 'text',
			),
			array(
				'name' => __( 'Our Team URL', 'theonefashion' ),
				'desc' => __( 'field description (optional)', 'theonefashion' ),
				'id'   => $prefix . 'our_team_url',
				'type' => 'text_url',
			)
		)
	);


	/**
	 * Metabox for the user profile screen
	 */
	$meta_boxes['user_edit'] = array(
		'id'         => 'user_edit',
		'title'      => __( 'User Profile Metabox', 'cmb' ),
		'pages'      => array( 'user' ), // Tells CMB to use user_meta vs post_meta
		'show_names' => true,
		'cmb_styles' => false, // Show cmb bundled styles.. not needed on user profile page
		'fields'     => array(
			array(
				'name'     => __( 'Extra Info', 'cmb' ),
				'desc'     => __( 'field description (optional)', 'cmb' ),
				'id'       => $prefix . 'exta_info',
				'type'     => 'title',
				'on_front' => false,
			),
			array(
				'name'    => __( 'Avatar', 'cmb' ),
				'desc'    => __( 'field description (optional)', 'cmb' ),
				'id'      => $prefix . 'avatar',
				'type'    => 'file',
				'save_id' => true,
			),
			array(
				'name' => __( 'Facebook URL', 'cmb' ),
				'desc' => __( 'field description (optional)', 'cmb' ),
				'id'   => $prefix . 'facebookurl',
				'type' => 'text_url',
			)
		)
	);

	/**
	 * Metabox for an options page. Will not be added automatically, but needs to be called with
	 * the `cmb_metabox_form` helper function. See wiki for more info.
	 */
	$meta_boxes['options_page'] = array(
		'id'      => 'options_page',
		'title'   => __( 'Theme Options Metabox', 'cmb' ),
		'show_on' => array( 'key' => 'options-page', 'value' => array( $prefix . 'theme_options', ), ),
		'fields'  => array(
			array(
				'name'    => __( 'Site Background Color', 'cmb' ),
				'desc'    => __( 'field description (optional)', 'cmb' ),
				'id'      => $prefix . 'bg_color',
				'type'    => 'colorpicker',
				'default' => '#ffffff'
			),
		)
	);

	// Add other metaboxes as needed 
    
	return $meta_boxes;
}