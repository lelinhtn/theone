<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !function_exists( 'theone_custom_metabox_iconpicker' ) ) {
    
    /**
     * Custom metabox field icon picker 
     **/
    function theone_custom_metabox_iconpicker( $field_args, $escaped_value, $object_id, $object_type, $field_type_object ) {
        
        echo $field_type_object->input( array( 'type' => 'text' ) );
        
        echo '<a href="#" class="toggle-iconpicker">' . __( 'Icon picker', 'theone-core' ) . '</a>';
        echo '<div class="iconpicker-wrap" style="display: none;">';
        theonecorefont_icons_chooser( '', 1, 96, true, false );
        echo '</div><!-- /.iconpicker-wrap -->';
        
        
    }
    add_action( 'cmb_render_theone_iconpicker', 'theone_custom_metabox_iconpicker', 10, 5 ); //action: cmb_render_{field-type}
    
}

if ( !function_exists( 'theone_validate_iconpicker_field' ) ) {
    
    function theone_validate_iconpicker_field( $override_value, $value ) {
        
        // Do something fun here 
        return $value;
        
    }
    add_filter( 'cmb_validate_theone_iconpicker', 'theone_validate_iconpicker_field' ); // filter: cmb_validate_{field-type}
       
}


/**
 * Include and setup custom metaboxes and fields.
 *
 * @category The One Core
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_animated_column_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_animated_column_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'theone_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['animated_column_metabox'] = array(

	  'title' => __( 'Animated Column Metas', 'theone-core' ),
	  'pages' => array( 'animated_column' ),
	  'context'    => 'normal',
	  'id'         => $prefix . 'animated_column_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
		       'name' => __( 'Icon', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'icon_class',
		       'type' => 'theone_iconpicker',
               'desc' => __( 'Enter or choose class from the list below', 'theone-core' )
		   	),
            array(
		       'name' => __( 'Read More Link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'animate_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Use permalink', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'use_permalink',
		       'type' => 'checkbox',
               'desc' => ''
		   	)
	  	)

	); 

	return $meta_boxes;
}

