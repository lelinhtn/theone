<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_global_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_global_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_ts_';
    
    $font_classes_url = 'http://fonts.themestudio.net/stroke-gap-icons-webfont/';
    

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['global_metabox'] = array(

	  'title' => __( 'Custom Title Metas', 'theone-core' ),
	  'pages' => array( 'page', 'post', 'member', 'testimonial', 'portfolio', 'product' ),
	  'context'    => 'normal',
	  'id'         => 'theone_global_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
                'name' => __( 'Use Custom Title', 'theone-core' ),
                'desc' => __( 'If <strong>Yes</strong>, custom title will show on the frontend', 'theone-core' ),
                'id' => '_ts_use_custom_title',
                'type' => 'select',
                'options' => array(
                    'no' => __( 'No', 'theone-core' ),
                    'yes' => __( 'Yes', 'theone-core' ),
                ),
                'default' => 'no',
		   	),
            array(
		       'name' => __( 'Custom Header Title', 'theone-core' ),
		       'desc' => '',
		       'id' => '_ts_custom_header_title',
		       'type' => 'text'
		   	),
            array(
		       'name' => __( 'Header Subtitle', 'theone-core' ),
		       'desc' => '',
		       'id' => '_ts_header_subtitle',
		       'type' => 'text'
		   	),
            array(
                'name' => __( 'Header Background Tyle', 'theone-core' ),
                'desc' => __( 'Background type can be a slider or a image', 'theone-core' ),
                'id' => '_ts_header_bg_type',
                'type' => 'select',
                'options' => array(
                    'global' => __( 'Using global setting in the Theone Options', 'theone-core' ),
                    'image' => __( 'Image', 'theone-core' ),
                    'slider' => __( 'Slider', 'theone-core' ),
                ),
                'default' => 'global',
		   	),
            array(
                'name' => __( 'Background Image', 'theone-core' ),
                'desc' => __( 'Upload a background image or enter an URL. Background image only appear if background type is "Image".', 'theone-core' ),
                'id' => '_ts_header_bg_src',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
            ),
            array(
                'name' => __( 'Header Background Slider', 'theone-core' ),
                'desc' => __( 'Choose a slider for header background. Background slider only appear if background type is "Slider".', 'theone-core' ),
                'id' => '_ts_header_rev_slider_alias',
                'type' => 'select',
                'options' => ts_rev_sliders_args(),
		   	),
            array(
		       'name' => __( 'Icon', 'theone-core' ),
		       'desc' => sprintf( __( 'Paste you font icon class here. You can find the font class here: <a href="%s" title="Stroke-Gap-Icons" target="_blank">Stroke-Gap-Icons</a>', 'theone-core' ), $font_classes_url ),
		       'id' => '_ts_header_font_class',
		       'type' => 'text'
		   	),
            array(
                'name' => __( 'Show Breadcrumb', 'theone-core' ),
                'desc' => __( 'If <strong>Yes</strong>, custom title will show on the frontend', 'theone-core' ),
                'id' => '_ts_header_show_breadcrumb',
                'type' => 'select',
                'options' => array(
                    'yes' => __( 'Yes', 'theone-core' ),
                    'no' => __( 'No', 'theone-core' ),
                ),
                'default' => 'yes',
		   	),
	  	)

	); 

	return $meta_boxes;
}

