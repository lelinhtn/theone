<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_member_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_member_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'theone_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['member_metabox'] = array(

	  'title' => __( 'Member Metas', 'theone-core' ),
	  'pages' => array( 'member' ),
	  'context'    => 'normal',
	  'id'         => $prefix . 'member_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
		       'name' => __( 'Member Position', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'member_position',
		       'type' => 'text'
		   	),
            array(
		       'name' => __( 'Facebook link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'fb_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Twitter link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'tw_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Google Plus link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'gplus_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Linkedin link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'in_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Youtube link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'yt_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Vimeo link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'vimeo_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Pinterest link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'pinterest_link',
		       'type' => 'text_url'
		   	),
	  	)

	); 

	return $meta_boxes;
}

