<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_testimonial_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_testimonial_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'theone_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['testimonial_metabox'] = array(

	  'title' => __( 'Testimonial Metas', 'theone-core' ),
	  'pages' => array( 'testimonial' ),
	  'context'    => 'normal',
	  'id'         => $prefix . 'testimonial_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
		       'name' => __( 'Client Position', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'client_position',
		       'type' => 'text'
		   	)
	  	)

	); 

	return $meta_boxes;
}

