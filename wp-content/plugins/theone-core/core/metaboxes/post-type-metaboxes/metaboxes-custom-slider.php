<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_slider_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_slider_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'theone_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['many_posttype_metabox'] = array(

	  'title' => __( 'Custom Slider', 'theone-core' ),
	  //'pages' => array( 'post', 'page', 'product', 'testimonial', 'member', 'advertise', 'client' ),
	  'context'    => 'normal',
	  'id'         => $prefix . 'slider_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
		       'name' => __( 'Slider', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'slider',
		       'type' => 'text_url'
		   	)
	  	)

	); 

	return $meta_boxes;
}

