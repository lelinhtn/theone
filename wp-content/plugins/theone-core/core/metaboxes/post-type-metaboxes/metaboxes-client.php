<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Include and setup custom metaboxes and fields.
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/webdevstudios/Custom-Metaboxes-and-Fields-for-WordPress
 */

add_filter( 'cmb_meta_boxes', 'theone_client_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function theone_client_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = 'theone_';


	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$meta_boxes['client_metabox'] = array(

	  'title' => __( 'Client Metas', 'theone-core' ),
	  'pages' => array( 'client' ),
	  'context'    => 'normal',
	  'id'         => $prefix . 'client_metas',
	  'priority'   => 'low',
	  'show_names' => true, // Show field names on the left
	  'fields' => array(
            array(
                'name' => __( 'Primary logo', 'theone-core' ),
                'desc' => __( 'Upload client logo', 'theone-core' ),
                'id' => $prefix . 'client_logo',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ), // limit to just attachments with array( 'attachment' )
            ),
            array(
                'name' => __( 'Secondary logo', 'theone-core' ),
                'desc' => __( 'Upload client logo', 'theone-core' ),
                'id' => $prefix . 'client_secondary_logo',
                'type' => 'file',
                'allow' => array( 'url', 'attachment' ), // limit to just attachments with array( 'attachment' )
                'description' => __( '', 'theone-core' )
            ),
            array(
		       'name' => __( 'Client link', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'client_link',
		       'type' => 'text_url'
		   	),
            array(
		       'name' => __( 'Use permalink', 'theone-core' ),
		       'desc' => '',
		       'id' => $prefix . 'use_permalink',
		       'type' => 'checkbox',
               'desc' => ''
		   	)
	  	)

	); 

	return $meta_boxes;
}

