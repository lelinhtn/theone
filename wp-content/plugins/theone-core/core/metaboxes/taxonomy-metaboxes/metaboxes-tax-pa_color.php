<?php

/**
 * Metaboxes pa_colors Taxonomy 
 * @package OViC Core 1.0
 * @author Theme Studio 
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( is_admin() && class_exists('Tax_Meta_Class') ){
    
    /* 
    * Prefix of meta keys, optional
    * NOTICE: Rules naming variable: $'theme-name'_'taxonomy-name'_prefix. Ex: $theone_prefix, $theone_config,...
    */
    $theone_prefix = 'theone_';
    
    /* 
    * Config
    */
    $theone_config = array(
        'id'        => 'theone_tax_metabox',
        'title'     => 'Color Attribute Meta Box',
        'pages'     => array( 'pa_color' ),
        'context'   => 'normal',
        'fields'    => array(),
        'local_images' => false,
        'use_with_theme' => false
    ); 
    
    $theone_meta  =  new Tax_Meta_Class( $theone_config );
    
    /*
    * Add Fields
    */ 
    $theone_meta->addColor($theone_prefix . 'color', array('name'=> __('Select Color ','theonefashion')));
    
    /*
    * Finish Meta Box Decleration
    */
    $theone_meta->Finish();
    
    /*
    * Manage Taxonomy Columns
    */
    add_filter( 'manage_edit-pa_color_columns', 'fs_manage_tax_pa_color_columns' );
    add_filter( 'manage_pa_color_custom_column', 'fs_manage_tax_pa_color_column', 10, 3 );
    if (!function_exists('fs_manage_tax_pa_color_columns')) {
        
        function fs_manage_tax_pa_color_columns( $columns ) {
        
        	$newColumns = array();
        	$newColumns['cb'] = $columns['cb']; 
        	$newColumns['pa_color_tax_thumb'] = __( 'Color', 'theonefashion' );
            $newColumns['name'] = $columns['name']; 
             
        	unset( $columns['slug'] ); 
              
            return array_merge( $newColumns, $columns ); 
        }
    }
    if (!function_exists('fs_manage_tax_pa_color_column')) {
        function fs_manage_tax_pa_color_column( $columns, $column, $id ) {
            
            global $theone_prefix;
            
            switch ( $column ) :
            
                case 'pa_color_tax_thumb' :
                    $color = get_tax_meta( $id, $theone_prefix . 'color', false );  
                    if ( $color != '' ) : 
                        $columns = '<span class="color_thumbnail" style="background:'. $color .'"></span>';
                    else : 
                        $columns = '<span class="color_thumbnail"></span>';
                    endif; 
                    break; 
                    
            endswitch; 
        	
        	return $columns;
        }
    } 
}