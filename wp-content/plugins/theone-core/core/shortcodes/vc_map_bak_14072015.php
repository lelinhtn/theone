<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

// Uncomment this area for the lastest blogs
// add_action( 'vc_before_init', 'theone_lastestBlogCarousel' );
function theone_lastestBlogCarousel() {
    vc_map( 
        array(
            'name'        => __( 'Lastest Blog Carousel', 'theone-core' ),
            'base'        => 'theone_lastest_blog_carousel', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'value'         => __( 'LATEST BLOGS', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'theone_select_cat_field',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select category', 'theone-core' ),
                    'param_name'    => 'cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Limit', 'theone-core' ),
                    'param_name'    => 'limit',
                    'value'         => 8,
                    'description'   => __( 'Maximum of blog posts', 'theone-core' )
                ),
            )
        )
    );
}


//add_action( 'vc_before_init', 'theone_brandLogoCarousel' );
function theone_brandLogoCarousel() {
    vc_map( 
        array(
            'name'        => __( 'Brand Logos Carousel', 'theone-core' ),
            'base'        => 'theone_brand_logos_carousel', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'value'         => __( 'LATEST BLOGS', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Limit', 'theone-core' ),
                    'param_name'    => 'limit',
                    'value'         => 12,
                    'description'   => __( 'Maximum of blog posts', 'theone-core' )
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Background image', 'theone-core' ),
                    'param_name'    => 'bg_img_id',
                    'value'         => '',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'checkbox',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Enable parallax', 'theone-core' ),
                    'param_name'    => 'enable_parallax',
                    'value'         => array(
                        'Yes'       =>  'yes'
                    ),
                    'std'           =>  'no',
                    'description'   => __( '', 'theone-core' )
                )          
            )
        )
    );
}



// add_action( 'vc_before_init', 'theone_imgAdvsGroup' );
function theone_imgAdvsGroup() {
    vc_map( 
        array(
            'name'        => __( 'Image Advs Group', 'theone-core' ),
            'base'        => 'theone_img_advs_group', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 1', 'theone-core' ),
                    'param_name'    => 'img1',
                    'value'         => '',
                    'description'   => __( 'Top - Left', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 1 link', 'theone-core' ),
                    'param_name'    => 'link1',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 2', 'theone-core' ),
                    'param_name'    => 'img2',
                    'value'         => '',
                    'description'   => __( 'Bottom - Left', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 2 link', 'theone-core' ),
                    'param_name'    => 'link2',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 3', 'theone-core' ),
                    'param_name'    => 'img3',
                    'value'         => '',
                    'description'   => __( 'Top - Right', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 3 link', 'theone-core' ),
                    'param_name'    => 'link3',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 4', 'theone-core' ),
                    'param_name'    => 'img4',
                    'value'         => '',
                    'description'   => __( 'Bottom - Right', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image 4 link', 'theone-core' ),
                    'param_name'    => 'link4',
                    'description'   => __( '', 'theone-core' )
                )
            )
        )
    );
}

// add_action( 'vc_before_init', 'theone_socialLinks' );
function theone_socialLinks() {
    vc_map( 
        array(
            'name'        => __( 'Social Links', 'theone-core' ),
            'base'        => 'theone_social_links', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Facebook', 'theone-core' ),
                    'param_name'    => 'facebook_link',
                    'value'         => '',
                    'description'   => __( 'Facebook link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Twitter', 'theone-core' ),
                    'param_name'    => 'twitter_link',
                    'value'         => '',
                    'description'   => __( 'Twitter link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Google Plus', 'theone-core' ),
                    'param_name'    => 'gplus_link',
                    'value'         => '',
                    'description'   => __( 'Google Plus link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Linkedin', 'theone-core' ),
                    'param_name'    => 'linkedin_link',
                    'value'         => '',
                    'description'   => __( 'Linkedin link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Pinterest', 'theone-core' ),
                    'param_name'    => 'pinterest_link',
                    'value'         => '',
                    'description'   => __( 'Pinterest link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'RSS', 'theone-core' ),
                    'param_name'    => 'rss_link',
                    'value'         => '',
                    'description'   => __( 'RSS Feed link', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


// add_action( 'vc_before_init', 'theone_teamMember' );
function theone_teamMember() {
    vc_map( 
        array(
            'name'        => __( 'Team Member', 'theone-core' ),
            'base'        => 'theone_team_member', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image', 'theone-core' ),
                    'param_name'    => 'img_id',
                    'value'         => '',
                    'description'   => __( 'Member image', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Name', 'theone-core' ),
                    'param_name'    => 'member_name',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( 'Member name', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Position', 'theone-core' ),
                    'param_name'    => 'position',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Short Description', 'theone-core' ),
                    'param_name'    => 'description',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Facebook', 'theone-core' ),
                    'param_name'    => 'facebook_link',
                    'value'         => '',
                    'description'   => __( 'Facebook link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Twitter', 'theone-core' ),
                    'param_name'    => 'twitter_link',
                    'value'         => '',
                    'description'   => __( 'Twitter link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Google Plus', 'theone-core' ),
                    'param_name'    => 'gplus_link',
                    'value'         => '',
                    'description'   => __( 'Google Plus link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Linkedin', 'theone-core' ),
                    'param_name'    => 'linkedin_link',
                    'value'         => '',
                    'description'   => __( 'Linkedin link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Pinterest', 'theone-core' ),
                    'param_name'    => 'pinterest_link',
                    'value'         => '',
                    'description'   => __( 'Pinterest link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'RSS', 'theone-core' ),
                    'param_name'    => 'rss_link',
                    'value'         => '',
                    'description'   => __( 'RSS Feed link', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Read more link', 'theone-core' ),
                    'param_name'    => 'readmore_link',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}




// THEONE shortcodes =========================

add_action( 'vc_before_init', 'ts_singleService' );
function ts_singleService() {
    global $gap_icons, $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone Single Service', 'theone-core' ),
            'base'        => 'ts_single_service', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Service Style', 'theone-core' ),
                    'param_name'    => 'service_style',
                    'value' => array(
    					__( '[Plumber] Style 1 (Icon, title, desc, link, bg image, overlay)', 'theone-core' ) => 'style-1', // Plumber   
    					__( '[Plumber] Style 2 (Icon bigger, title, desc, link button, bg image, align center)', 'theone-core' ) => 'style-2',
                        __( '[Plumber] Style 3 (Icon, title, desc, link button, bg image, align center)', 'theone-core' ) => 'style-3',
                        __( '[Plumber] Style 4 (Icon, title, desc, link, bg image)', 'theone-core' ) => 'style-4', 
                        __( '[Plumber] Style 5 (Title, subtitle, desc, link, no icon)', 'theone-core' ) => 'style-5',
                        __( '[Contruction] Style 6 (Icon, title, desc and readmore hidden, align center)', 'theone-core' ) => 'style-6', // Contruction
                        __( '[Contruction] Style 7 (Icon, title, desc, readmore, align left)', 'theone-core' ) => 'style-7',
                        __( '[Contruction] Style 8 (Icon, title, desc, align left)', 'theone-core' ) => 'style-8',
                        __( '[Contruction] Style 9 (Icon, title, desc and readmore hidden, bg image hover, align center)', 'theone-core' ) => 'style-9',
                        __( '[Contruction] Style 10 (Icon, title, desc and readmore hidden, bg image, color overlay, align center)', 'theone-core' ) => 'style-10',
                        __( '[Mechanic] Style 11 (Icon, title, desc, link button, image, align center)', 'theone-core' ) => 'style-11', // Mechanic
                        __( '[Mechanic] Style 12 (Icon, title, desc, link button, bg image)', 'theone-core' ) => 'style-12',
                        __( '[Mechanic] Style 13 (title, desc, link button, align center)', 'theone-core' ) => 'style-13',
                        __( '[Mechanic] Style 24 (title, desc, link, bg image, align center, hover overlay)', 'theone-core' ) => 'style-24', // Added after so key is style-24
                        __( '[Clearning] Style 14 (Icon, title, desc, link, image)', 'theone-core' ) => 'style-14', // Clearning
                        __( '[Clearning] Style 15 (Icon, title, desc, align center)', 'theone-core' ) => 'style-15',
                        __( '[Clearning] Style 25 (Icon, title, desc and readmore hidden, bg image, color overlay, align center)', 'theone-core' ) => 'style-25',  // Added after so key is style-24
                        __( '[Carpenter] Style 16 (Icon, title, desc, link, image, hover overlay)', 'theone-core' ) => 'style-16', // Carpenter
                        __( '[Metal Construction] Style 17 (Icon, title, desc, link, bg image, align center)', 'theone-core' ) => 'style-17', // Metal Construction
                        __( '[Metal Construction] Style 17a (Icon, title, desc, link, align center)', 'theone-core' ) => 'style-17a', 
                        __( '[Metal Construction] Style 18 (Icon bigger, title, desc, link, align center)', 'theone-core' ) => 'style-18',
                        __( '[Mining] Style 19 (Icon, title, desc, link, bg image)', 'theone-core' ) => 'style-19', // Mining
                        __( '[Mining] Style 20 (Icon, title, desc, link, bg image, align center)', 'theone-core' ) => 'style-20',
                        __( '[Maintenance] Style 21 (Icon bigger, title, desc, link button, bg image, align center)', 'theone-core' ) => 'style-21',  // Maintenance
                        __( '[Maintenance] Style 22 (Icon, title, desc, link button, bg white, align center)', 'theone-core' ) => 'style-22',
                        __( '[Maintenance] Style 23 (Icon, title, desc, link button, bg black, align center)', 'theone-core' ) => 'style-23',
                        __( '[Maintenance] Style 26 (Icon, title, desc, link button, bg image, bg overlay, align center)', 'theone-core' ) => 'style-26', // Jump number because added after
                        __( '[Electrician] Style 27 (Icon, title, desc, link, align center)', 'theone-core' ) => 'style-27', // Electrician
                        __( '[Electrician] Style 28 (Icon, title, desc, link, align left)', 'theone-core' ) => 'style-28',
                        __( '[Renovation] Style 29 (Icon, title, desc, link, align center)', 'theone-core' ) => 'style-29', // Renovation  
                        __( '[Renovation] Style 33 (Icon, title, desc, link, align center)', 'theone-core' ) => 'style-33', // Added after so key is style-33  
                        __( '[Renovation] Style 34 (Icon, title, desc, link, align center)', 'theone-core' ) => 'style-34', // Added after so key is style-34
                        __( '[Gardner] Style 30 (Icon, title, desc, link, align center)', 'theone-core' ) => 'style-30', // Gardner
                        __( '[Gardner] Style 31 (Icon, title, desc, link, align center, no border)', 'theone-core' ) => 'style-31',
                        __( '[Gardner] Style 32 (Icon bg dark, title, desc, link, align center)', 'theone-core' ) => 'style-32',
                        __( '[Autoshop] Style 35 (Title, subtitle, bg image)', 'theone-core' ) => 'style-35',
    				),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Service Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Service Subtitle', 'theone-core' ),
                    'param_name'    => 'subtitle',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'service_style',
    				    'value' => array( 'style-4', 'style-5', 'style-35' )
    			   	),
                ),
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => false, // default true, display an "EMPTY" icon?
    					'type' => 'elegant',
    					'source' => $gap_icons,					
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'service_style',
    				    'value' => array( 'style-1', 'style-2', 'style-3', 'style-4', 'style-6', 'style-7', 'style-8', 'style-9', 'style-11', 
                                        'style-12', 'style-14', 'style-15', 'style-16', 'style-17', 'style-17a', 'style-18', 'style-19', 'style-20', 'style-21',
                                        'style-22', 'style-23', 'style-24', 'style-25', 'style-26', 'style-27', 'style-28', 'style-29', 'style-30',
                                        'style-31', 'style-32', 'style-33', 'style-34' )
    			   	),
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Content', 'theone-core' ),
                    'param_name'    => 'content',
                    'value'         => __( '<p>I am test text block. Click edit button to change this text.</p>', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Link Button', 'theone-core' ),
                    'param_name'    => 'link_btn',
                    'value'         => '',
                    'description'   => __( '', 'theone-core' )
                ),             
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Service Background Image', 'theone-core' ),
                    'param_name'    => 'bg_img_id',
                    'value'         => '',
                    'description'   => __( 'Top - Left', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'service_style',
    				    'value' => array( 'style-1', 'style-2', 'style-3', 'style-9', 'style-10', 'style-11', 'style-12', 'style-13', 
                                        'style-14', 'style-16', 'style-17', 'style-19', 'style-20', 'style-21', 'style-24', 
                                        'style-25', 'style-26', 'style-35' )
    			   	),
                ),   
                array(
                    'type' => 'colorpicker',
                    'class' => '',
                    'heading' => __( 'Service Overlay Color', 'my-text-domain' ),
                    'param_name' => 'overlay_color',
                    'value' => 'rgba(0, 0, 0, 0.8)',  // Old: rgba(3, 82, 159, 0.8)
                    'description' => __( '', 'my-text-domain' ),
                    'dependency' => array(
    				    'element' => 'service_style',
    				    'value' => array( 'style-1', 'style-2', 'style-3', 'style-4', 'style-10', 'style-11', 'style-17', 'style-17a',
                                        'style-18', 'style-20', 'style-21', 'style-24', 'style-25', 'style-26',  ) 
    			   	),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}

add_action( 'vc_before_init', 'ts_singleFeature' );
function ts_singleFeature() {
    global $gap_icons, $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone Single Features', 'theone-core' ),
            'base'        => 'ts_single_feature', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => false, // default true, display an "EMPTY" icon?
    					'type' => 'elegant',
    					'source' => $gap_icons,					
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Content', 'theone-core' ),
                    'param_name'    => 'content',
                    'value'         => __( '<p>I am test text block. Click edit button to change this text.</p>', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Feature link', 'theone-core' ),
                    'param_name'    => 'feature_link',
                    'value'         => '',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


add_action( 'vc_before_init', 'ts_titleDesc' );
function ts_titleDesc() {
    global $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone title with description', 'theone-core' ),
            'base'        => 'ts_title_desc', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title Style', 'theone-core' ),
                    'param_name'    => 'title_desc_style',
                    'value' => array(
    					__( 'Construction', 'theone-core' ) => 'construction', 
    					__( 'Plumber', 'theone-core' ) => 'plumber',
                        __( 'Mechanic', 'theone-core' ) => 'mechanic',
                        __( 'Cleaning', 'theone-core' ) => 'cleaning', 
                        __( 'Autoshop', 'theone-core' ) => 'autoshop', 
                        __( 'Carpenter', 'theone-core' ) => 'carpenter', 
                        __( 'Maintenance', 'theone-core' ) => 'maintenance', 
                        __( 'Metal Construction', 'theone-core' ) => 'metal_construction', 
                        __( 'Electrician', 'theone-core' ) => 'electrician', 
                        __( 'Mining', 'theone-core' ) => 'mining', 
                        __( 'Renovation', 'theone-core' ) => 'renovation',
                        __( 'Movers', 'theone-core' ) => 'movers', 
                        __( 'Gardner', 'theone-core' ) => 'gardner',
                        __( 'Fuel Industry', 'theone-core' ) => 'fuel_industry', 
                        __( 'Logistics', 'theone-core' ) => 'logistics'
    				),
                    'std' => 'plumber',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title font size', 'theone-core' ),
                    'param_name'    => 'title_font_size',
                    'value'         => '30',
                    'description'   => __( 'Font size unit is pixel (px)', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title font weight', 'theone-core' ),
                    'param_name'    => 'title_font_weight',
                    'value'         => '400'
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Sub Title', 'theone-core' ),
                    'param_name'    => 'subtitle',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Sub title font size', 'theone-core' ),
                    'param_name'    => 'subtitle_font_size',
                    'value'         => '12',
                    'description'   => __( 'Font size unit is pixel (px)', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Sub title font weight', 'theone-core' ),
                    'param_name'    => 'subtitle_font_weight',
                    'value'         => '400'
                ),
                array(
                    'type'          => 'textarea_html',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Content', 'theone-core' ),
                    'param_name'    => 'content',
                    'value'         => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Align', 'theone-core' ),
                    'param_name' => 'align',
                    'value' => array(
                        __( 'Center', 'theone-core' ) => 'center',
                        __( 'Left', 'theone-core' ) => 'left',
                        __( 'Right', 'theone-core' ) => 'right',	
                        __( 'Inherit', 'theone-core' ) => 'inherit'		    
                    )
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


add_action( 'vc_before_init', 'ts_callToAction' );
function ts_callToAction() {
    vc_map( 
        array(
        	'name' => __('Theone call to action','theone-core'),
            'base' => 'ts_call_to_action',
            'class' => '',
            'category' => __('Theone','theone-core'),
            'params' => array(
               	array(
                      'type' => 'textarea_html',
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('Text','theone-core'),
                      'param_name' => 'content',
                      'value' => __( 'Theone is a business theme which perfectly suited Construction company, Cleaning agency, Mechanic workshop and any kind of handyman business.', 'theone-core' ),
                      'description' => __('Enter Call to action content ','theone-core')
                ),
        		array(
                      'type' => 'vc_link',
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('Link Button','theone-core'),
                      'param_name' => 'link_btn',
                      'description' => __('','theone-core')
                ),
        		array(
        			   'type' => 'dropdown',
        			   'heading' => __( 'Border', 'theone-core' ),
        			   'param_name' => 'choose_border',
        			   'value' => array(
                            __( 'Border', 'theone-core' ) => 'yes',
                            __( 'No border', 'theone-core' ) => 'no',
        			   ),			   
        			   'description' => __( 'Choose border to action display.', 'theone-core' )
                ),
        		array(
        	        'type' => 'colorpicker',
        	        'holder' => 'div',
        	        'class' => '',
        	        'heading' => __('Choose border color','theone-core'),
        	        'param_name' => 'border_color',
        	        'value' => '#e5e5e5',	
        	        'dependency' => array(
    				    'element' => 'choose_border',
    				    'value' => array( 'yes')
    			   	),        
        	        'description' => __('Choose border color','theone-core'),	        
                ), 
        		array(
                      'type' => 'textfield',
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('Border radius', 'theone-core'),
                      'param_name' => 'border_radius',
                      'value' => '3',              
                      'description' => __('Enter values of border radius (pixel).', 'theone-core')
                ),
                array(
        	        'type' => 'colorpicker',
        	        'holder' => 'div',
        	        'class' => '',
        	        'heading' => __('Background color', 'theone-core'),
        	        'param_name' => 'bg_color',
        	        'value' => '',
        	        'description' => __('Choose background color', 'theone-core'),	        
        	   ),
        		array(
        	        'type' => 'colorpicker',
        	        'holder' => 'div',
        	        'class' => '',
        	        'heading' => __('Button color','theone-core'),
        	        'param_name' => 'button_color',
        	        'value' => '#ffffff',
        	        'description' => __('Choose button text color','theone-core'),	        
                ),
                array(
        	        'type' => 'colorpicker',
        	        'holder' => 'div',
        	        'class' => '',
        	        'heading' => __('Button hover color','theone-core'),
        	        'param_name' => 'button_hover_color',
        	        'value' => '#ffffff',
        	        'description' => __('Choose button text hover color','theone-core'),	        
                ),
        		array(
        	        'type' => 'colorpicker',
        	        'holder' => 'div',
        	        'class' => '',
        	        'heading' => __('Button background color', 'theone-core'),
        	        'param_name' => 'button_bg_color',
        	        'value' => '#404040',
        	        'description' => __('Choose background color for button', 'theone-core'),	        
        	   ),
       		   array(
        	        'type' => 'colorpicker',
        	        'holder' => 'div',
        	        'class' => '',
        	        'heading' => __('Button background hover color', 'theone-core'),
        	        'param_name' => 'button_hover_bg_color',
        	        'value' => '#a6a6a6',
        	        'description' => __('Choose background color for button hover', 'theone-core'),	        
        	   )

            )
        )
    );
}



add_action( 'vc_before_init', 'ts_lastestNews' );
function ts_lastestNews() {
    vc_map( 
        array(
        	'name' => __('Theone tastest news','theone-core'),
            'base' => 'ts_latest_news',
            'class' => '',
            'category' => __('Theone','theone-core'),
            'params' => array(
               	array(
                    'type' => 'textfield',
                    'heading' => __( 'Number of posts', 'theone-core' ),
                    'param_name' => 'number_of_posts',
                    'value' => '2',
                    'description' => __( 'Number of posts to display.', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Posts per row', 'theone-core' ),
                    'param_name' => 'posts_per_row',
                    'value' => array(
                        __( '4 posts', 'theone-core' ) => '4',
                        __( '3 posts', 'theone-core' ) => '3',
                        __( '2 posts', 'theone-core' ) => '2',
                        __( '2 posts', 'theone-core' ) => '1',				    
                    ),
                    'std' => '2',
                    'description' => __( 'Posts per row on large screen.', 'theone-core' )
                ),

            )
        )
    );
}

add_action( 'vc_before_init', 'theone_testimonialCarousel' );
function theone_testimonialCarousel() {
    vc_map( 
        array(
            'name'        => __( 'Theone Testimonial Carousel', 'theone-core' ),
            'base'        => 'theone_testimonial_carousel', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'theone_select_tes_cat_field', // Custom VC type
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select category', 'theone-core' ),
                    'param_name'    => 'tes_cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number of posts per slide', 'theone-core' ),
                    'param_name'    => 'posts_per_slide',
                    'value'         => 3,
                    'description'   => __( 'Number of posts per slide on large screen', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Limit', 'theone-core' ),
                    'param_name'    => 'limit',
                    'value'         => 8,
                    'description'   => __( 'Maximum number of testimonials', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Excerpt max chars', 'theone-core' ),
                    'param_name'    => 'max_chars',
                    'value'         => 100,
                    'description'   => __( 'Maximum chars of testimonial excerpt', 'theone-core' )
                ),
                array(
        			   'type' => 'dropdown',
        			   'heading' => __( 'Show pagination', 'theone-core' ),
        			   'param_name' => 'show_pagination',
        			   'value' => array(
                            __( 'Yes', 'theone-core' ) => 'true',
                            __( 'No', 'theone-core' ) => 'false',
        			   ),			   
        			   'description' => __( '', 'theone-core' )
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'theone_clientLogoCarousel' );
function theone_clientLogoCarousel() {
    vc_map( 
        array(
            'name'        => __( 'Theone client logos carousel', 'theone-core' ),
            'base'        => 'ts_client_logos_carousel', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'theone_select_client_cat_field', // Custom VC type
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select category', 'theone-core' ),
                    'param_name'    => 'client_cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number of posts per slide', 'theone-core' ),
                    'param_name'    => 'posts_per_slide',
                    'value'         => 6,
                    'description'   => __( 'Number of posts per slide on large screen', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Limit', 'theone-core' ),
                    'param_name'    => 'limit',
                    'value'         => 12,
                    'description'   => __( 'Maximum number of client logos', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show border per logo', 'theone-core' ),
                    'param_name' => 'border_per_logo',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No Border', 'theone-core' ) => 'no'				    
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' )
                ),
            )
        )
    );
}

add_action( 'vc_before_init', 'theone_membersGrid' );
function theone_membersGrid() {
    vc_map( 
        array(
            'name'        => __( 'Theone members grid', 'theone-core' ),
            'base'        => 'ts_members_grid', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'theone_select_member_cat_field', // Custom VC type
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select category', 'theone-core' ),
                    'param_name'    => 'member_cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Member per row', 'theone-core' ),
                    'param_name' => 'members_per_row',
                    'value' => array(
                        __( '4 members', 'theone-core' ) => '4',
                        __( '3 members', 'theone-core' ) => '3',
                        __( '2 members', 'theone-core' ) => '2',
                        __( '2 members', 'theone-core' ) => '1',				    
                    ),
                    'std' => '4',
                    'description' => __( 'Members per row on large screen.', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show pagination', 'theone-core' ),
                    'param_name' => 'show_pagination',
                    'value' => array(
                        __( 'No', 'theone-core' ) => 'no',
                        __( 'Yes', 'theone-core' ) => 'yes',				    
                    )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Limit', 'theone-core' ),
                    'param_name'    => 'limit',
                    'value'         => 12,
                    'description'   => __( 'Maximum number of members to display', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Excerpt max chars', 'theone-core' ),
                    'param_name'    => 'max_chars',
                    'value'         => 150,
                    'description'   => __( 'Maximum chars of testimonial excerpt', 'theone-core' )
                ),
            )
        )
    );
}




add_action( 'vc_before_init', 'theone_pricesTable' );
function theone_pricesTable() {
    global $gap_icons;
    vc_map( 
        array(
            'name'        => __( 'Theone pricing table', 'theone-core' ),
            'base'        => 'ts_prices_table', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'value'         => __( 'Plan No 1', 'theone-core' ),
                    'description'   => __( 'Pricing table title. Ex: Plan No 1', 'theone-core' )
                ),
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => false, // default true, display an "EMPTY" icon?
    					'type' => 'elegant',
    					'source' => $gap_icons,					
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price', 'theone-core' ),
                    'param_name'    => 'price',
                    'value'         => '50'
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Unit', 'theone-core' ),
                    'param_name'    => 'unit',
                    'value'         => '$'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Unit Position', 'theone-core' ),
                    'param_name' => 'unit_position',
                    'value' => array(
                        __( 'Before Price', 'theone-core' ) => 'before',
                        __( 'After Price', 'theone-core' ) => 'after'			    
                    ),
                    'std' => 'before'
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Time', 'theone-core' ),
                    'param_name'    => 'price_time',
                    'value'         => __( 'Monthly', 'theone-core' )
                ),
                array(
                      'type' => 'textarea_html',
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('Text','theone-core'),
                      'param_name' => 'content',
                      'value' => __( 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.', 'theone-core' ),
                      'description' => __('Short content of price table','theone-core')
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 1', 'theone-core' ),
                    'param_name'    => 'field_1',
                    'value'         => __( 'Option Number 1', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 2', 'theone-core' ),
                    'param_name'    => 'field_2',
                    'value'         => __( 'Option Number 2', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 3', 'theone-core' ),
                    'param_name'    => 'field_3',
                    'value'         => __( 'Option Number 3', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 4', 'theone-core' ),
                    'param_name'    => 'field_4',
                    'value'         => __( 'Option Number 4', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 5', 'theone-core' ),
                    'param_name'    => 'field_5',
                    'value'         => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 6', 'theone-core' ),
                    'param_name'    => 'field_6',
                    'value'         => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 7', 'theone-core' ),
                    'param_name'    => 'field_7',
                    'value'         => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Field 8', 'theone-core' ),
                    'param_name'    => 'field_8',
                    'value'         => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Price Link Button', 'theone-core' ),
                    'param_name'    => 'link_btn',
                    'value'         => '',
                    'description'   => __( '', 'theone-core' )
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'ts_iconText' );
function ts_iconText() {
    vc_map( 
        array(
            'name'        => __( 'Theone icon text', 'theone-core' ),
            'base'        => 'ts_icon_text', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'value'         => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => false, // default true, display an "EMPTY" icon?
    					'type' => 'fontawesome',				
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'text',
                    'value'         => __( 'This is sample text', 'theone-core' ),
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'ts_blockQuote' );
function ts_blockQuote() {
    global $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone block quote', 'theone-core' ),
            'base'        => 'ts_block_quote', // shortcode
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Block Quote Style', 'theone-core' ),
                    'param_name' => 'quote_style',
                    'value' => array(
                        __( 'Style 1', 'theone-core' ) => '1',
                        __( 'Style 2', 'theone-core' ) => '2',
                        __( 'Style 3', 'theone-core' ) => '3',
                        __( 'Style 4', 'theone-core' ) => '4',				    
                    )
                ),
                array(
                      'type' => 'textarea_html',
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('Quote','theone-core'),
                      'param_name' => 'content',
                      'value' => __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in molestie. Curabitur pellentesque massa eu nulla consequat sed porttitor arcu porttitor. Quisque volutpat pharetra felis, eu cursus lorem molestie vitae condimentum tristique vel, eleifend sed turpis.', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Author', 'theone-core' ),
                    'param_name'    => 'author',
                    'value'         => __( 'Begha, Book Name', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}

add_action( 'vc_before_init', 'ts_bulletsList' );
function ts_bulletsList() {
    global $gap_icons, $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone bullets list', 'theone-core' ),
            'base'        => 'ts_bullets_list',  
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                      'type' => 'exploded_textarea',
                      'value' => 'arrow_triangle-right_alt2|Creative template|http://theone.themestudio.net', 
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('List', 'theone-core'),
                      'param_name' => 'list',
                      'description' => __( 'Input list items here. Divide each item by linebreaks (Enter). <br /> An item fllowing this structure: {icon (or number)}|{item text}}{item link}. <br /> For icons, you can use font <a href="http://fontawesome.io/icons/" target="__blank">Awesome</a> icon or <a href="http://www.elegantthemes.com/blog/resources/elegant-icon-font" target="__blank">Elegant</a> font icon.', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


add_action( 'vc_before_init', 'ts_subscribeForm' );
function ts_subscribeForm() {
    
    // Appears only shortcode mc4wp_form exist
    global $gap_icons, $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone subscribe form', 'theone-core' ),
            'base'        => 'ts_subscribe_form',  
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'form_style',
                    'value' => array(
                        __( 'Style 1', 'theone-core' ) => '1',
                        __( 'Style 2', 'theone-core' ) => '2'			    
                    )
                ),
                array(
                      'type' => 'textarea',
                      'value' => '', 
                      'holder' => 'div',
                      'class' => '',
                      'heading' => __('Text', 'theone-core'),
                      'param_name' => 'subscribe_text',
                      'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
    
}


add_action( 'vc_before_init', 'ts_simpleButton' );
function ts_simpleButton() {
    
    // Appears only shortcode mc4wp_form exist
    global $gap_icons, $ts_vc_animate_effects;
    vc_map( 
        array(
            'name'        => __( 'Theone simple button', 'theone-core' ),
            'base'        => 'ts_simple_btn',  
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'btn_style',
                    'value' => array(
                        __( 'Normal Button Style', 'theone-core' ) => 'normal',
                        __( 'Primary Button Style', 'theone-core' ) => 'primary'			    
                    )
                ),
                array(
                    'type'          => 'vc_link',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Link', 'theone-core' ),
                    'param_name'    => 'link_btn',
                    'value'         => 'url:' . htmlentities2( urlencode( get_home_url() ) ) . '|title:Button|target:%20_blank'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Align', 'theone-core' ),
                    'param_name' => 'align_class',
                    'value' => array(
                        __( 'Initial', 'theone-core' ) => 'initial',
                        __( 'Center', 'theone-core' ) => 'center',
                        __( 'Left', 'theone-core' ) => 'left',
                        __( 'Right', 'theone-core' ) => 'right',			    
                    )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Additional classes', 'theone-core' ),
                    'param_name'    => 'additional_classes',
                    'description'   => __( 'Input additional classes here', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_animate_effects,
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
    
}


add_action( 'vc_before_init', 'ts_productsCarousel' );
function ts_productsCarousel() {
    
    if ( !class_exists( 'WooCommerce' ) ):
        return false;
    endif;
    
    vc_map( 
        array(
            'name'        => __( 'Theone Products Carousel', 'theone-core' ),
            'base'        => 'ts_products_carousel',  
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Subtitle', 'theone-core' ),
                    'param_name'    => 'subtitle',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'theone_select_product_cat_field',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select products category', 'theone-core' ),
                    'param_name'    => 'product_cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number of products load', 'theone-core' ),
                    'param_name'    => 'products_num',
                    'value'         => '8',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Criteria of products', 'theone-core' ),
                    'param_name' => 'products_criteria',
                    'value' => array(
                        __( 'Recent Products', 'theone-core' ) => 'recent_products',
                        __( 'Best Selling Products', 'theone-core' ) => 'best_selling_products',
                        __( 'Top Rated Products', 'theone-core' ) => 'top_rated_products',
                        __( 'Featured Products', 'theone-core' ) => 'featured_products',
                        __( 'Sale Products', 'theone-core' ) => 'sale_products',			    
                    )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number of items per slide', 'theone-core' ),
                    'param_name'    => 'product_per_slide',
                    'value'         => '4',
                    'description'   => __( 'On the screen min width 1200px', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Products per item', 'theone-core' ),
                    'param_name' => 'product_per_item',
                    'value' => array(
                        __( '1', 'theone-core' ) => '1',
                        __( '2', 'theone-core' ) => '2',
                        __( '3', 'theone-core' ) => '3',
                        __( '4', 'theone-core' ) => '4',
                        __( '5', 'theone-core' ) => '5',
                        __( '6', 'theone-core' ) => '6',		    
                    ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show pagination', 'theone-core' ),
                    'param_name' => 'pagination',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
    
}


add_action( 'vc_before_init', 'ts_productsCarousel2' );
function ts_productsCarousel2() {
    
    if ( !class_exists( 'WooCommerce' ) ):
        return false;
    endif;
    
    vc_map( 
        array(
            'name'        => __( 'Theone Products Carousel 2', 'theone-core' ),
            'base'        => 'ts_products_carousel_2',  
            'class'       => '',
            'category'    => __( 'Theone', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'theone_select_product_cat_field',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select products category', 'theone-core' ),
                    'param_name'    => 'product_cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number of products load', 'theone-core' ),
                    'param_name'    => 'products_num',
                    'value'         => '8',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Criteria of products', 'theone-core' ),
                    'param_name' => 'products_criteria',
                    'value' => array(
                        __( 'Recent Products', 'theone-core' ) => 'recent_products',
                        __( 'Best Selling Products', 'theone-core' ) => 'best_selling_products',
                        __( 'Top Rated Products', 'theone-core' ) => 'top_rated_products',
                        __( 'Featured Products', 'theone-core' ) => 'featured_products',
                        __( 'Sale Products', 'theone-core' ) => 'sale_products',			    
                    )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show rating', 'theone-core' ),
                    'param_name' => 'show_rating',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number of items per slide', 'theone-core' ),
                    'param_name'    => 'product_per_slide',
                    'value'         => '1',
                    'description'   => __( 'On the screen min width 1200px', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Products per item', 'theone-core' ),
                    'param_name' => 'product_per_item',
                    'value' => array(
                        __( '1', 'theone-core' ) => '1',
                        __( '2', 'theone-core' ) => '2',
                        __( '3', 'theone-core' ) => '3',
                        __( '4', 'theone-core' ) => '4',
                        __( '5', 'theone-core' ) => '5',
                        __( '6', 'theone-core' ) => '6',		    
                    ),
                    'std'   =>  '3'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show pagination', 'theone-core' ),
                    'param_name' => 'pagination',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    )
                ),
                array(
                    'type'          => 'css_editor', // Not done
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
    
}








