<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
	/*
	 * Button shortcode
	*/
	function theone_button_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
	      'color' => 'white',
	      'style' => '',
	      'text' => '',
		  'size' => '',
	      'url' => '',
	      'icon' => '',
	      'icon_right' => ''
	      ), $atts ) );
		
		$alt = ($style != '') ? 'alt' : '';
		$icon_i = ($icon != '') ? '<i class="fa '.$icon.'"></i>' : '';
		$icon_p = ($icon_right != '') ? 'icon-right' : '';

		if($url) {
	      return '<a class="button ' . $color .' '. $size . ' ' . $alt . ' ' . $icon_p . '" href="' . $url . '">'.$icon_i.'' . $text . $content . '</a>';
		} else {
			return '<a class="button ' . $color . '" href="">' . $text . $content . '</a>';
		}
	}
	add_shortcode('theone-button', 'theone_button_shortcode');


	/*
	 * Clear shortcode
	*/
	function theone_clear_shortcode() {
	   return '<div class="clear"></div>';
	}
	add_shortcode( 'theone-clear', 'theone_clear_shortcode' );



	/*
	 * separator shortcode
	*/
	function theone_separator_shortcode() {
	   return '<div class="separator"></div>';
	}
	add_shortcode( 'theone-separator', 'theone_separator_shortcode' );

	/*
	 * space shortcode
	*/
	function theone_space_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'height' => '60'
	    ), $atts ) );
	   return '<div style="clear:both; width:100%; height:'.$height.'px"></div>';
	}
	add_shortcode( 'theone-space', 'theone_space_shortcode' );

	/*
	 * line break shortcode
	*/
	function theone_line_break_shortcode() {
		return '<br />';
	}
	add_shortcode( 'theone-br', 'theone_line_break_shortcode' );


	/*
	 * Lists shortcode
	*/
	function theone_list_shortcode( $atts, $content = null )
	{
		extract( shortcode_atts( array(
			'icon' => 'ok'
	    ), $atts ) );
		
		return '<div class="customlist list-icon-fa-'.$icon.'">'.do_shortcode($content).'</div>';
	}
	add_shortcode('theone-list', 'theone_list_shortcode');


	/*
	 * Dropcap shortcode
	*/
	function theone_dropcap_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'style' => '1',
			'text' => ''
	    ), $atts ) );
		  
		return '<span class="dropcap' . $style . '">' . $text . $content .'</span>';
	}
	add_shortcode('theone-dropcap', 'theone_dropcap_shortcode');


	/*
	 * Highlighted shortcode
	*/
	function theone_highlighted_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'color' => 'dark',
			'text' => ''
	    ), $atts ) );
		  
		return '<span class="highlight ' . $color . '">' . $text . $content .'</span>';
	}
	add_shortcode('theone-highlighted', 'theone_highlighted_shortcode');


	/*
	 * Quote Shortcode
	*/
	function theone_quote_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'text' => '',
			'author' => ''
	    ), $atts ) );
		
		$output = '';

		$output .= '<h2 class="parallax-quote">"' . $text.'"</h2>'; 
		$output .= '<span class="quote-author">' . $author.'</span>'; 

		return $output;
	}

	add_shortcode('theone-quote', 'theone_quote_shortcode');


	/*
	 * Fun Fact Shortcode
	*/
	function theone_funfact_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'numbercolumn' => '',
			'title' => '',
			'img' => '',
			'data_to1' => '34',
			'icon1' => '',
			'funfact_text1' => '',
			'data_to2' => '',
			'icon2' => 'moon-briefcase',
			'funfact_text2' => '',
			'data_to3' => '',
			'icon3' => '',
			'funfact_text3' => '',
			'data_to4' => '',
			'icon4' => '',
			'funfact_text4' => ''
	    ), $atts ) );
		$urlimg = wp_get_attachment_url($img);

		$output = '';
			$output .= '<div id="bg-parallax2" style="background-image: url('.$urlimg.')">';
			$output .= '<div class="parallax-over"></div>';
				$output .= '<div class="container">';
	                $output .= '<div class="row">';
	                    $output .= '<div class="tt"><h2>'.$title.'</h2></div>';
						$output .= '<div class="ct">';
							if ($numbercolumn == '1') {
								$output .= '<div class="box col-sm-12 col-xs-12 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon1.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to1.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text1.'</p>';
								$output .= '</div>';
							}
							elseif ($numbercolumn == '2') {
								$output .= '<div class="box col-sm-6 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon1.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to1.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text1.'</p>';
								$output .= '</div>';
								$output .= '<div class="box col-sm-6 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon2.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to2.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text2.'</p>';
								$output .= '</div>';
							}
							elseif ($numbercolumn == '3') {
								$output .= '<div class="box col-sm-4 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon1.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to1.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text1.'</p>';
								$output .= '</div>';
								$output .= '<div class="box col-sm-4 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon2.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to2.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text2.'</p>';
								$output .= '</div>';
								$output .= '<div class="box col-sm-4 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon3.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to3.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text3.'</p>';
								$output .= '</div>';
							}
							else{
								$output .= '<div class="box col-sm-3 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon1.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to1.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text1.'</p>';
								$output .= '</div>';
								$output .= '<div class="box col-sm-3 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon2.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to2.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text2.'</p>';
								$output .= '</div>';
								$output .= '<div class="box col-sm-3 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon3.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to3.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text3.'</p>';
								$output .= '</div>';
								$output .= '<div class="box col-sm-3 col-xs-6 col-vs-12">';
								    $output .= '<div class="icofont"><span class="moon-'.$icon4.'"></span></div>';
								    $output .= '<p class="countup">'.$data_to4.'</p>';
								    $output .= '<p class="more-ct">'.$funfact_text4.'</p>';
								$output .= '</div>';
							}
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div>';
			$output .= '</div>';

		return $output;
	}
	add_shortcode('theone-funfact', 'theone_funfact_shortcode');


	/*
	 * Section Title Shortcode
	*/
	function theone_stitle_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'title' => '',
			'subtitle' => ''
	    ), $atts ) );
		
		$output = '';

		$output .= '<h1 class="section-title">' . $title.'</h1>'; 
		if($subtitle != "") {
			$output .= '<h2 class="section-tagline">' . $subtitle.'</h2>'; 
		}

		return $output;
	}
	add_shortcode('theone-section-title', 'theone_stitle_shortcode');


	/*
	 * Skillbar Shortcode
	*/
	function theone_skillbar_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'values' => '',
			'units' => '%'
	    ), $atts ) );

		wp_enqueue_script('theone-waypoints');
		wp_enqueue_script('theone-custom-skills', get_template_directory_uri() . '/js/custom/custom-skills.js', array('jquery'), '1.0', false );


	    $array_values = explode(",", $values);
		
		$output = '';

		foreach($array_values as $skill_value) {
			$data = explode("|", $skill_value);
			$output .= '<div class="skillbar clearfix" data-percent="'.$data['0'] . $units.'">';
				$output .= '<div class="skillbar-title"><span>'.$data['1'].'</span></div>';
				$output .= '<div class="skillbar-bar"></div>';
				$output .= '<div class="skill-bar-percent">'.$data['0'].$units .'</div>';
			$output .= '</div>';
		}


		return $output;
	}
	add_shortcode('theone-skillbar', 'theone_skillbar_shortcode');


	/*
	 * Social Share Blog
	*/
	function delicious_social_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'title' => ''
	    ), $atts ) );

	    wp_enqueue_script('theone-social');
		  
		$output = '';
		
		$output .= '<div class="share-options">';
			if(!empty($title)) { $output .= '<h6>'.$title.'</h6>'; }
			$output .= '<a href="" class="twitter-sharer" onClick="twitterSharer()"><i class="fa fa-twitter"></i></a>';
			$output .= '<a href="" class="facebook-sharer" onClick="facebookSharer()"><i class="fa fa-facebook"></i></a>';
			$output .= '<a href="" class="pinterest-sharer" onClick="pinterestSharer()"><i class="fa fa-pinterest"></i></a>';
			$output .= '<a href="" class="google-sharer" onClick="googleSharer()"><i class="fa fa-google-plus"></i></a>';
			$output .= '<a href="" class="delicious-sharer" onClick="deliciousSharer()"><i class="fa fa-share"></i></a>';
			$output .= '<a href="" class="linkedin-sharer" onClick="linkedinSharer()"><i class="fa fa-linkedin"></i></a>';
		$output .= '</div>';
		$output .= '<p></p>';
		
		return $output;
	}
	add_shortcode('theone-social-block', 'delicious_social_shortcode');


	/*
	 * Columns shortcode
	*/
	function delicious_column_shortcode( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'size' => 'one-half',
			'text' => '',
			'position' => ''
	    ), $atts ) );

		if(!empty($position)) {
			return '<div class="percent-' . $size . ' column-' . $position . '"> '.do_shortcode($content).'</div>';
		} else {
			return '<div class="percent-' . $size . '"> ' .do_shortcode($content). '</div>';
		}
	}
	add_shortcode('theone-column', 'delicious_column_shortcode');


	/*
	 * Pricing Table placebo shortcode
	*/
	function delicious_pricing_table_placebo( $atts, $content = null ) {
		return do_shortcode($content);
	}
	add_shortcode( 'theone-table_placebo', 'delicious_pricing_table_placebo' );


	/*
	 * Pricing Table shortcode
	*/
	function delicious_pricing_table( $atts, $content = null ) {
		global $theone_table;
		extract(shortcode_atts(array(
			'columns' => '4'
	    ), $atts));
		
		$columnsNr = '';
		$finished_table = '';

		switch ($columns) {
			case '2':
				$columnsNr .= 'cols-2';
				break;
			case '3':
				$columnsNr .= 'cols-3';
				break;
			case '4':
				$columnsNr .= 'cols-4';
				break;
			case '5':
				$columnsNr .= 'cols-5';
				break;
			case '6':
				$columnsNr .= 'cols-6';
				break;
		}

		do_shortcode($content);

		$columnContent = '';
		if (is_array($theone_table)) {

			for ($i = 0; $i < count($theone_table); $i++) {
				$columnClass = 'pricing-column'; $n = $i + 1;
				$columnClass .= ( $n % 2 ) ?  '' : ' even-column';
				$columnClass .= ( $theone_table[$i]['featured'] ) ?  ' featured-column' : '';
				$columnClass .= ( $n == count($theone_table) ) ?  ' last-column' : '';
				$columnClass .= ( $n == 1 ) ?  ' first-column' : '';
				$columnContent .= '<div class="'.$columnClass.' '.$columnsNr.'">'; 
				$columnContent .= '<div class="pricing-header">';
				if (( $theone_table[$i]['featured'] ) == '1' ) {
					$columnContent .= '<div class="column-shadow"></div>';
				}			
				$columnContent .='<div class="package-title">'.$theone_table[$i]['title'].'</div><div class="package-value"><span class="package-currency">'.$theone_table[$i]['currency'].'</span><span class="package-price">'.$theone_table[$i]['price'].'</span><span class="package-time">'.$theone_table[$i]['interval'].'</span></div></div>';
				$columnContent .= '<div class="package-features">'.str_replace(array("\r\n", "\n", "\r", "<p></p>"), array("", "", "", ""), $theone_table[$i]['content']).'</div>';
				$columnContent .= '</div>'; 
			}
			$finished_table = '<div class="pricing-table">'.$columnContent.'</div>';
		}
		
		$theone_table = '';
		
		return $finished_table;
		
	}
	add_shortcode('theone-pricing-table', 'delicious_pricing_table');


	/*
	 * Single Column shortcode
	*/
	function delicious_shortcode_pricing_column( $atts, $content = null ) {
		global $theone_table;
		extract(shortcode_atts(array(
			'title' => '',
			'price' => '',
			'currency' => '',
			'interval' => '',
			'featured' => 'false'
	    ), $atts));
		
		$featured = strtolower($featured);
		
		$column['title'] = $title;
		$column['price'] = $price;
		$column['currency'] = $currency;
		$column['interval'] = $interval;
		$column['featured'] = ( $featured == 'true' || $featured == 'yes' || $featured == '1' ) ? true : false;
		$column['content'] = do_shortcode($content);
		
		$theone_table[] = $column;
	}
	add_shortcode('theone-pricing-column', 'delicious_shortcode_pricing_column');


	/*
	 * signup area
	*/
	function shortcode_signup( $atts, $content = null )
	{	  
		return '<div class="signup">'. do_shortcode($content) .'</div>';
	}
	add_shortcode('theone-signup', 'shortcode_signup');



	/*
	 * Clients Shortcode
	*/
	function delicious_clients( $atts, $content = null ) {
		extract( shortcode_atts( array(
			'images' => '',
			'links' => ''
	    ), $atts ) );
			$array_images = explode(",", $images);
			$array_links = explode(",", $links);
			$clientItem = '';


			$clientItem .= '<div class="ct client">';
			
				$image_markup = ''; 
				$i = 0;
				foreach($array_images as $single_image) {
					
					$image_attributes = wp_get_attachment_url( $single_image );
					$image_markup = '<img src="'.$image_attributes.'" />';

					$client_link = ($array_links['0'] != '') ? $array_links[$i] : '#' ;
					
						$clientItem .='<a href="'.$client_link.'">';
						$clientItem .= $image_markup;
						$clientItem .='</a>';

					$i++;
				}
			$clientItem .= '</div>'; 
		
		return $clientItem;
		
	}
	add_shortcode('theone-clients', 'delicious_clients');


	/*
	 * Blog Grid Shortcode
	*/
	function delicious_blog_grid($atts, $content = null) {
		extract(shortcode_atts(array(
			"title" => "", 
			"desc" => "", 
			"number" => "3", 
			"columns" => "3",
			"categories" => ""
			
		), $atts));
		
		global $post;
		
		
		$output = '';
			$blog_array_cats = get_terms('category', array('hide_empty' => false));
			if(empty($categories)) {
				foreach($blog_array_cats as $blog__array_cat) {
					$categories .= $blog__array_cat->slug .', ';
				}
			}
			
			$args = array(
				'orderby'=> 'post_date',
				'order' => 'date',
				'post_type' => 'post',
				'category_name' => $categories,
				'posts_per_page' => $number
			);
			
			$my_query = new WP_Query($args);
			if( $my_query->have_posts() ) {

				$output .='<div id="lasted-post">';
				$output .='<div class="title">';
	                $output .='<h1>'.$title.'</h1>';
	                $output .='<span class="icofont moon-file-8"></span>';
	            $output .='</div>';
	            $output .='<p class="text">'.$desc.'</p>';
				$output .= '<div id="owl-lasted">';
				
					while ($my_query->have_posts()) : $my_query->the_post();
				
						ob_start();  
						get_template_part('loop/content-lasted', 'post');
						$result = ob_get_contents();  
						ob_end_clean();
						$output .= $result;
				
					endwhile; 
				
			
				$output .= '</div>';	
				$output .= '</div>';	
				}
			wp_reset_query();
		return $output;
	}
	add_shortcode("theone-blog-grid", "delicious_blog_grid");	


	/*
	 * Portfolio Grid Shortcode
	*/
	function theone_portfolio($atts, $content = null) {
		extract(shortcode_atts(array(
			"number" => "-1",
			"categories" => ""
		), $atts));
		
		global $post;
		global $theone;
		
		// General args
		$args = array(
			'post_type' => 'theone_portfolio',
			'posts_per_page' => $number,
		);

		// Category args
		if ( $categories !== '' ) {
			$terms = explode( ' ', $categories);
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'theone_portfolio_cats',
					'field' => 'id',
					'terms' => $terms,
					'operator' => 'IN',
				)
			);
		}

		// Do the query
		$block_query = new WP_Query( $args );
		$output = '';

		if ($theone['portfolio_style']==1){
			ob_start();  
				get_template_part('content-parts/portfolio-ajax', 'section');
				$result = ob_get_contents();  
			ob_end_clean();
			$output .= $result;

	            $output .= '<div class="ct wow fadeInUp" data-wow-delay=".4s">';
	            	ob_start();  
		    			get_template_part('content-parts/portfolio', 'filter');
						$result = ob_get_contents();  
					ob_end_clean();
					$output .= $result;

					$output .= '<div id="container-work" class="">';
						if ( $block_query->have_posts() ) : 
							while ( $block_query->have_posts() ) : $block_query->the_post();
					    		ob_start();  
					    			get_template_part( 'loop/content-portfolio', 'blog' );
									$result = ob_get_contents();  
								ob_end_clean();
								$output .= $result;
							endwhile;	
						else : 
							ob_start();  
					    		get_template_part('loop/content-blog','none');
								$result = ob_get_contents();  
							ob_end_clean();
							$output .= $result;
						endif;
	                $output .= '</div>';
				$output .= '</div>';

		} else {
			$output .= '<div class="ct wow fadeInUp" data-wow-delay=".4s">';
	            $output .= '<div id="owl">';
					if ( $block_query->have_posts() ) : 
						while ( $block_query->have_posts() ) : $block_query->the_post();
				    		ob_start();  
				    			get_template_part( 'loop/content-portfolio', 'lightbox' );
								$result = ob_get_contents();  
							ob_end_clean();
							$output .= $result;
						endwhile;	
					else : 
						ob_start();  
				    		get_template_part('loop/content-blog','none');
							$result = ob_get_contents();  
						ob_end_clean();
						$output .= $result;
					endif;
	            $output .= '</div>';
	        $output .= '</div>';
		}

		return $output;
	}
	add_shortcode("theone-portfolio", "theone_portfolio");


	/*
	 * Portfolio Grid Shortcode
	*/
	function delicious_portfolio_grid($atts, $content = null) {
		extract(shortcode_atts(array(
			"number" => "-1",
			"categories" => ""
		), $atts));
		
		global $post;
		
		//setting a random id
		$rnd_id = theone_random_id(3);   

		$token = wp_generate_password(5, false, false);
		
		wp_enqueue_script('theone-isotope');	
		wp_enqueue_script('theone-custom-isotope-portfolio');
		
		wp_localize_script( 'theone-custom-isotope-portfolio', 'theone_grid_' .$token, array( 'id' => $rnd_id));	
		
			$layout = get_post_meta($post->ID,'theone_portfolio_columns',true);
			$navig = get_post_meta($post->ID,'theone_portfolio_navigation',true);
			$nav_number = get_post_meta($post->ID,'theone_nav_number',true);	
			
			$cats = explode(",", $categories);
			
			$portfolio_categs = get_terms('portfolio_cats', array('hide_empty' => false));
			$categ_list = '';
			
			foreach ($cats as $categ) {
				foreach($portfolio_categs as $portfolio_categ) {
					if($categ === $portfolio_categ->name) {
						$categ_list .= $portfolio_categ->slug . ', ';
					}
				}
			}
				
			//fallback categories
				$args = array(
					'post_type'=>'portfolio',
					'taxonomy' => 'portfolio_cats'
				);		
				$categ_fall = get_categories( $args );
				$categ_use = array();
				$i = 0;
				foreach($categ_fall as $cate) {
					$categ_use[$i] = $cate->name; 
					$i++;
				}
				$cats = array_filter($cats);
				if(empty($cats)) {
					$cats = array_merge($cats, $categ_use);
				}			
				
				
				$term_list = '';
				$list = '';
				
				foreach ($cats as $cat) {
					$to_replace = array(' ', '/', '&');
					$intermediate_replace = strtolower(str_replace($to_replace, '-', $cat));
					$str = preg_replace('/--+/', '-', $intermediate_replace);
					if (function_exists('icl_t')) { 
					$term_list .= '<li><a href="#filter" data-option-value=".'. get_taxonomy_cat_ID($cat) .'">' . icl_t('Portfolio Category', 'Term '.get_taxonomy_cat_ID( $cat ).'', $cat) . '</a></li>';
					}
					else 
					$term_list .= '<li><a href="#filter" data-option-value=".'. get_taxonomy_cat_ID($cat) .'">' . $cat . '</a></li>';
					$list .= $cat . ', ';
				}		
				
			
		$output = '';
			$output .= '<section class="patti-grid" id="gridwrapper_'.$rnd_id.'" data-token="' . $token .'">';
					$output .= '<section id="options">';
						$output .= '<ul id="filters" class="option-set clearfix" data-option-key="filter">';
							$output .= '<li><a href="#filter" data-option-value="*" class="selected active">'.__('All', 'delicious').'</a></li>';
							$output .= $term_list;
						$output .= '</ul>';
					$output .= '</section>';
					$output .= '<div class="space"></div>';
					
				$output .= '<section id="portfolio-wrapper">';
					$output .= '<ul class="portfolio grid isotope grid_'.$rnd_id.'">';

					$args = array(
						'post_type'=>'portfolio',
						'posts_per_page' => $number,
						'term' => 'portfolio_cats',
						'portfolio_cats' => $categ_list
					);
					
					$my_query = new WP_Query($args);
					if( $my_query->have_posts() ) {
						while ($my_query->have_posts()) : $my_query->the_post();

						$terms = get_the_terms( get_the_ID(), 'portfolio_cats' );
						$term_val = '';
						if($terms) { foreach ($terms as $term) { $term_val .=get_taxonomy_cat_ID($term->name) .' '; } }
						
						$portf_icon = get_post_meta($post->ID,'theone_portf_icon',true);						
						$portf_thumbnail = get_post_meta($post->ID,'theone_portf_thumbnail',true);	
						$portf_link = get_post_meta($post->ID,'theone_portf_link',true);						
						$thumb_id = get_post_thumbnail_id($post->ID);
						
						$image_url = wp_get_attachment_url($thumb_id);
						
						$grid_thumbnail = $image_url;
						$item_class = 'item-small';
						
						switch ($portf_thumbnail) {
							case 'portfolio-big':
								$grid_thumbnail = aq_resize($image_url, 566, 440, true);
								$item_class = 'item-wide';
								break;
							case 'portfolio-small':
								$grid_thumbnail = aq_resize($image_url, 281, 219, true);
								$item_class = 'item-small';
								break;
							case 'half-horizontal':
								$grid_thumbnail = aq_resize($image_url, 566, 219, true);
								$item_class = 'item-long';
								break;
							case 'half-vertical':
								$grid_thumbnail = aq_resize($image_url, 281, 440, true);
								$item_class = 'item-high';
								break;							
						}		

						$copy = $terms;
						$res = '';
						if($terms) {
							foreach ( $terms as $term ) {
								if (function_exists('icl_t')) { 
									$res .= icl_t('Portfolio Category', 'Term '.get_taxonomy_cat_ID( $term->name ).'', $term->name);
								}
								else $res .= $term->name;
								if (next($copy )) {
									$res .=  ', ';
								}
							}
						}					

						$output .= '<li class="grid-item '.$term_val.' '.$item_class.'">';

						$inner_output = '';
						$inner_output .= '<div class="grid-item-on-hover">';
							$inner_output .= '<div class="grid-text">';
								$inner_output .= '<h1>'.get_the_title().'</h1>';
							$inner_output .= '</div>';
							$inner_output .= '<div><span>';	
								$inner_output .= $res;
							$inner_output	.='</span></div>';
						$inner_output .= '</div>';
						$inner_output .= '<img src="'. $grid_thumbnail.'" alt="" />';				
						
						$test_link = '';
						if($portf_icon == 'link_to_page') {
								$test_link = '<a href="'.get_permalink($post->ID).'">'.$inner_output.'</a>';
						} else if($portf_icon == 'link_to_link') {
							$test_link = '<a href="'.$portf_link.'">'.$inner_output.'</a>';
						}
						else if($portf_icon == 'lightbox_to_image') {
							$test_link = '<a href="'. wp_get_attachment_url($thumb_id) .'" rel="prettyPhoto[portf_gal]" title="'. get_the_title() .'">'.$inner_output.'</a>';
						}												
						
						$output .= $test_link;
			
						$output .= '</li>';
					endwhile; 
					}
					wp_reset_query(); 
					$output .= '</ul>';
				$output .= '</section>';
		$output .= '</section>';
		$output .= '<div class="space"></div>';	

		return $output;
	}
	add_shortcode("theone-portfolio-grid", "delicious_portfolio_grid");	



	/*
	 * Services Item
	*/
	function delicious_services($atts, $content = null) {
		extract(shortcode_atts(array(
			"id" => ''
		), $atts));

		global $post;
		
		$args = array(
			'post_type' => 'services',
			'posts_per_page' => 1,
			'p' => $id
		);
		
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) :
		while ($my_query->have_posts()) : $my_query->the_post();


		$service_icon = get_post_meta($post->ID, 'theone_service_icon', true);
		$service_text = get_post_meta($post->ID, 'theone_service_text', true);
		
		$service_class ='';
		
		$retour = '';
		$retour .= '<div class="theone-service-wrapper">';
			$retour .='<div class="theone-service-item">';
				$retour .= '<i class="fa '.$service_icon.'"></i>';
				$retour .='<h3 class="service-title">'.get_the_title().'</h3>';
			$retour .='</div>';

			$retour .='<div class="theone-service-hover">';
				$retour .= '<i class="fa '.$service_icon.'"></i><h3>'.get_the_title().'</h3>';	
				$retour .='<p>'.wp_kses_post($service_text).'</p>';			
			$retour .= '</div>';
		$retour .='</div>';

		endwhile; else:
		$retour ='';
		$retour .= "nothing found.";
		endif;

		//Reset Query
	    wp_reset_query();
		
		return $retour;
	}

	add_shortcode("theone-service", "delicious_services");



	/*
	 * Service List for Visual Composer
	*/
	function delicious_vc_services($atts, $content = null) {
		extract(shortcode_atts(array(
			"ids" => ''
		), $atts));

		$service_ids = explode(",", $ids);

		$output = '';
		$output .= '<div class="homepage-services">';

		foreach ($service_ids as $sid) {
			$output .= do_shortcode("[theone-service id=".$sid."]");
		}

		$output .= '</div>';
		return $output;
	}
	add_shortcode("theone-services", "delicious_vc_services");



	/*
	 * Team Member
	*/
	function delicious_member($atts, $content = null) {
		extract(shortcode_atts(array(
			"id" => ''
		), $atts));

		global $post;

		$args = array(
			'post_type' => 'team',
			'posts_per_page' => 1,
			'p' => $id
		);
		
		$team_query = new WP_Query($args);
		if( $team_query->have_posts() ) :
		while ($team_query->have_posts()) : $team_query->the_post();
		
		$member_text = get_post_meta($post->ID, 'theone_member_text', true);
		$position = get_post_meta($post->ID, 'theone_member_position', true);
		$twitter = get_post_meta($post->ID, 'theone_member_twitter', true);
		$facebook = get_post_meta($post->ID, 'theone_member_facebook', true);
		$email = get_post_meta($post->ID, 'theone_member_mail', true);
		$linkedin = get_post_meta($post->ID, 'theone_member_linkedin', true);
		$google = get_post_meta($post->ID, 'theone_member_google', true);
		
		$mail = is_email($email);
		
		$image = get_the_post_thumbnail( $id, 'member-thumb', array('class' => 'team-avatar') );
		$url_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		
		$retour ='';
		$retour .='<div class="team-member">';
				$retour .= '<a href="'.$url_image.'" rel="prettyPhoto" title="'.get_the_title().'">';						
					$retour .= '<span class="item-on-hover"><span class="hover-image"><i class="fa fa-search"></i></span></span>';						
					$retour .= $image;
				$retour .= '</a>';
				
				$retour .='<div class="team-text">';
					$retour .='<h3><span>';
					$retour .= get_the_title();
					$retour .='</span></h3>';
					if(!empty($position)) {
					$retour .='<h6>'.$position.'</h6>'; }
					$retour .='<p>'.wp_kses_post($member_text).'</p>';
				$retour .='</div>';
			
				$retour .='<div class="team-social">';
					if(!empty($mail)) {
					$retour .='<a href="mailto:'.$mail.'"><i class="fa fa-envelope"></i></a>';  }	
					if(!empty($facebook)) {
					$retour .='<a href="'.esc_url($facebook).'"><i class="fa fa-facebook"></i></a>'; }				
					if(!empty($twitter)) {
					$retour .='<a href="'.esc_url($twitter).'"><i class="fa fa-twitter"></i></a>'; }
					if(!empty($google)) {
					$retour .='<a href="'.esc_url($google).'"><i class="fa fa-google-plus"></i></a>'; }						
					if(!empty($linkedin)) {
					$retour .='<a href="'.esc_url($linkedin).'"><i class="fa fa-linkedin"></i></a>'; }			
				$retour .='</div>';
		$retour .='</div>';

		 endwhile; else:
		 $retour ='';
		 $retour .= "nothing found.";
		 endif;

	    //Reset Query
	    wp_reset_query();
		
		return $retour;
	}
	add_shortcode("theone-team-member", "delicious_member");



	/*
	 * Testimonial Item
	*/
	function delicious_testimonials($atts, $content = null) {
		extract(shortcode_atts(array(
			"id" => ''
		), $atts));

		global $post;
		
		$args = array(
			'post_type' => 'testimonials',
			'posts_per_page' => 1,
			'p' => $id
		);
		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) :
		while ($my_query->have_posts()) : $my_query->the_post();
		
		$testimonial_desc = get_post_meta($post->ID, 'theone_testimonial_desc', true);	
		$testimonial_name = get_post_meta($post->ID, 'theone_testimonial_name', true);	
		$testimonial_details = get_post_meta($post->ID, 'theone_testimonial_details', true);	
		
		$retour ='';
		
		$retour .='<div class="testimonial-item">';
		$retour .='<p>'.wp_kses_post($testimonial_desc).'</p>';
		$retour .='<p>';
		$retour .='<span class="testimonial-name">'.esc_html($testimonial_name).'</span>, <span class="testimonial-position">'.esc_html($testimonial_details).'</span>';
		$retour .='</p>';
		$retour .='</div>';

		endwhile; else:
		$retour ='';
		$retour .= "nothing found.";
		endif;

		//Reset Query
	    wp_reset_query();
		
		return $retour;
	}
	add_shortcode("theone-testimonial", "delicious_testimonials");


	/*
	 * Testimonial Carousel for Visual Composer
	*/
	function delicious_theone_testimonials($atts, $content = null) {
		extract(shortcode_atts(array(
			"ids" => ''
		), $atts));

		$testimonial_ids = explode(",", $ids);

		$rnd_id = theone_random_id(3);
		$token = wp_generate_password(5, false, false);

		wp_enqueue_script('custom-testimonials', get_template_directory_uri() . '/js/custom/custom-testimonials.js', array('jquery'), '1.0', false );	
		wp_localize_script( 'custom-testimonials', 'theone_testimonials_' . $token, array( 'id' => $rnd_id) );		

		$output = '';
		$output .= '<div class="testimonials-carousel">';
			$output .= '<div id="owl-testimonials-'.$rnd_id.'" class="owl-carousel testimonials-slider" data-token="' . $token .'">';

		foreach ($testimonial_ids as $tid) {
			$output .= do_shortcode("[theone-testimonial id=".$tid."]");
		}

			$output .= '</div>';
		$output .= '</div>';
		return $output;
	}
	add_shortcode("theone-testimonials", "delicious_theone_testimonials");



	/*
	 * Portfolio Slider for Visual Composer
	*/
	function delicious_portfolio_slider($atts, $content = null) {
		extract(shortcode_atts(array(
			"images" => '',
			'thumb_size' => 'gallery-thumb'
		), $atts));

		$portfolio_images = explode(",", $images);

		$rnd_id = theone_random_id(3);
		$token = wp_generate_password(5, false, false);

		wp_enqueue_script('custom-portfolio-slider', get_template_directory_uri() . '/js/custom/custom-slider.js', array('jquery'), '1.0', false );	
		wp_localize_script( 'custom-portfolio-slider', 'theone_slider_' . $token, array( 'id' => $rnd_id) );		

		$output = '';
		$output .= '<div class="portfolio-slider-wrapper">';
			$output .= '<div id="owl-slider-'.$rnd_id.'" class="owl-carousel portfolio-slider" data-token="' . $token .'">';

			foreach($portfolio_images as $single_image) {
				
				$img_size = '';
				$alt = trim(strip_tags( get_post_meta($single_image, '_wp_attachment_image_alt', true) ));
				if (function_exists('wpb_getImageBySize')) {
					$img_size = wpb_getImageBySize(array('attach_id' => (int)$single_image, 'thumb_size' => $thumb_size));
				}
				$output .='<div class="slider-item">';
				$output .='<a title="'.$alt.'" href="'.$img_size['p_img_large']['0'].'" rel="prettyPhoto[project_gal]">';
				$output .= $img_size['thumbnail'];
				$output .='</a>';
				$output .='</div>';
			}

			$output .= '</div>';
			$output .= '<div class="slider-nav"></div>';
		$output .= '</div>';
		return $output;
	}
	add_shortcode("theone-portfolio-slider", "delicious_portfolio_slider");



	/*
	 * Google Map Shortcode
	*/
	function delicious_map_shortcode($atts, $content = null) {
		extract(shortcode_atts(array(
			"button_text" => '',
			"latitude" => '37.422117',
			"longitude" => '-122.084053',
			'pin_title' => 'Company Headquarters',
			'pin_desc' => 'Now that you visited our website, how about <br/> checking out our office too?'
		), $atts));


		$rnd_id = theone_random_id(3);
		
		$token = wp_generate_password(5, false, false);

		$site_url = get_stylesheet_directory_uri();

		wp_enqueue_script('theone-api-map', 'http://maps.google.com/maps/api/js?sensor=false', false );	
		wp_enqueue_script('theone-custom-map', get_template_directory_uri() . '/js/custom/custom-map.js', array('theone-api-map'), '1.0', false );	
		wp_localize_script( 'theone-custom-map', 'theone_map_'. $token, array( 'id' => $rnd_id, 'site_url' => $site_url, 'latitude' => $latitude, 'longitude' => $longitude, 'pin_title' => $pin_title, 'pin_desc' => $pin_desc) );		

		$output = '';
		$output .= '<div class="map-wrapper" id="delicious_map_'.$rnd_id.'" data-token="' . $token .'">';
			$output .='<a class="button-map close-map"><span>'.$button_text.'</span></a>';
			$output .='<div id="google_map_'.$rnd_id.'"></div>';
		$output .='</div>';

		return $output;
	}

	add_shortcode("theone-google-map", "delicious_map_shortcode");



	/*
	 * CF7 Shortcode Hack
	*/
	function mycustom_wpcf7_form_elements( $form ) {
		$form = do_shortcode( $form );

		return $form;
	}
	add_filter( 'wpcf7_form_elements', 'mycustom_wpcf7_form_elements' );



	/*
	 * Twitter Carousel Shortcode
	*/
	function delicious_twitter_carousel($atts, $content = null) {
		extract(shortcode_atts(array(
			"twitter_username" => '',
			"twitter_postcount" => '',
			"twitter_consumer_key" => '',
			"twitter_consumer_secret" => '',
			"twitter_access_token" => '',
			"twitter_access_token_secret" => '',
			"img" => ''
		), $atts));

		$transName = 'list_tweets';
	    $cacheTime = 20;

	    if(false === ($twitterData = get_transient($transName) ) ){
	    	// require_once ('includes/twitteroauth.php');
			$twitterConnection = new TwitterOAuth(
								$twitter_consumer_key,			// Consumer Key
								$twitter_consumer_secret,   		// Consumer secret
								$twitter_access_token,       		// Access token
								$twitter_access_token_secret    	// Access token secret
								);

			$twitterData = $twitterConnection->get(
					  'statuses/user_timeline',
					  array(
					    'screen_name'     => $twitter_username,
					    'count'           => $twitter_postcount,
					    'exclude_replies' => false
					  )
					);

			if($twitterConnection->http_code != 200)
			{
				$twitterData = get_transient($transName);
			}

	        // Save our new transient.
	        set_transient($transName, $twitterData, 60 * $cacheTime);
	    }

	    $output = '';

	  	if(!empty($twitterData) || !isset($twitterData['error'])) {
			$i=0;
			$hyperlinks = true;
			$encode_utf8 = false;
			$twitter_users = true;
			$update = true;
			$urlimg = wp_get_attachment_url($img);
			$output .= '<div id="bg-parallax3" style="background-image: url('.$urlimg.')" data-stellar-background-ratio="0.1">';
				$output .= '<div class="parallax-over"></div>';
				$output .= '<div class="ct">';
		            $output .= '<div class="icofont"><h2><span class="moon-twitter"></span></h2></div>';
		            $output .= '<div id="owl2">';
		            
		        foreach($twitterData as $item){
		            $msg = $item['text'];
		            $permalink = $item['user']['url'];
		            $user_twitter = $item['user']['screen_name'];
					$retweet = 'http://twitter.com/intent/retweet?tweet_id='. $item['id_str'];
					$tweet_reply = 'http://twitter.com/intent/tweet?in_reply_to='. $item['id_str'];
					$tweet_favorite = 'http://twitter.com/intent/favorite?tweet_id='. $item['id_str'];
		            if($encode_utf8) $msg = utf8_encode($msg);
		                $msg = encode_tweet($msg);
		            $link = $permalink;

		            if ($hyperlinks) {    $msg = hyperlinks($msg); }
		            if ($twitter_users)  { $msg = twitter_users($msg); }

		            if($update) {
						$time = strtotime($item['created_at']);

						if ( ( abs( time() - $time) ) < 86400 )
							$h_time = sprintf( __('%s ago', 'delicious'), human_time_diff( $time ) );
						else
							$h_time = time_elapsed_string($time);

						$output .= '<div class="item">';
		                    $output .= '<p>'.$msg.'</p>';
		                    $output .= '<a href="'.$retweet.'">'.$permalink.'</a>';
		                    $output .= '<a class="button" href="'.$tweet_reply.'">@'.$user_twitter.'</a>';
		                $output .= '</div>';
		            }

		            $i++;
		            if ( $i >= $twitter_postcount ) break;
		        }

				$output .= '</div>';
				$output .= '</div>';
			$output .= '</div>';
	    }

	    return $output;
	}
	add_shortcode("theone-twitter-carousel", "delicious_twitter_carousel");


	/*
	 * Shortcodes Filter
	*/ 
	function theone_the_content_filter($content) {
	 
		// array of custom shortcodes
		$block = join("|",array("theone-portfolio-grid","theone-blog-grid", "theone-signup", "theone-pricing-column", "theone-button", "theone-funfact", "theone-skillbar"));
	 
		// opening tag
		$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
			
		// closing tag
		$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
	 
		return $rep;
	}
	add_filter("the_content", "theone_the_content_filter");


	/*
	 * Functions for Twitter Shortcode
	*/
	function time_elapsed_string($ptime) {
	    $etime = time() - $ptime;
	    if ($etime < 1)
	    {
	        return '0 seconds';
	    }
	    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
	                30 * 24 * 60 * 60       =>  'month',
	                24 * 60 * 60            =>  'day',
	                60 * 60                 =>  'hour',
	                60                      =>  'minute',
	                1                       =>  'second'
	                );
	    foreach ($a as $secs => $str)
	    {
	        $d = $etime / $secs;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
	        }
	    }
	}


    // Find links and create the hyperlinks
	function hyperlinks($text) {
	    $text = preg_replace('/\b([a-zA-Z]+:\/\/[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&#038;%#+$*!]*)\b/i',"<a href=\"$1\" class=\"twitter-link\">$1</a>", $text);
	    $text = preg_replace('/\b(?<!:\/\/)(www\.[\w_.\-]+\.[a-zA-Z]{2,6}[\/\w\-~.?=&#038;%#+$*!]*)\b/i',"<a href=\"http://$1\" class=\"twitter-link\">$1</a>", $text);

	    // match name@address
	    $text = preg_replace("/\b([a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]*\@[a-zA-Z][a-zA-Z0-9\_\.\-]*[a-zA-Z]{2,6})\b/i","<a href=\"mailto://$1\" class=\"twitter-link\">$1</a>", $text);
	        //mach #trendingtopics. Props to Michael Voigt
	    $text = preg_replace('/([\.|\,|\:|\|\|\>|\{|\(]?)#{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<a href=\"http://twitter.com/#search?q=$2\" class=\"twitter-link\">#$2</a>$3 ", $text);
	    return $text;
	}


	// Find twitter usernames and link to them
	function twitter_users($text) {
	       $text = preg_replace('/([\.|\,|\:|\|\|\>|\{|\(]?)@{1}(\w*)([\.|\,|\:|\!|\?|\>|\}|\)]?)\s/i', "$1<a href=\"http://twitter.com/$2\" class=\"twitter-user\">@$2</a>$3 ", $text);
	       return $text;
	}

    // Encode single quotes in your tweets
    function encode_tweet($text) {
            $text = mb_convert_encoding( $text, "HTML-ENTITIES", "UTF-8");
            return $text;
    }


    function theone_time_line ($atts, $content = null)
    {
    	extract(
    		shortcode_atts(
	    		array(
					"number" => '',
				), 
    			$atts
    		)
    	);

    	// General args
		$args = array(
			'post_type' => 'theone_time_line',
			'posts_per_page' => $number,
			'order' => 'ASC',
		);

		// Do the query
		$block_query = new WP_Query( $args );

    	$output = '';

    	

    	if ( $block_query->have_posts() ) : 
    		$dem = 1;
    		$output .= '<div class="timeline">';
    			$output .= '<ul>';
					while ( $block_query->have_posts() ) : $block_query->the_post();
			    		ob_start();
			    			if ($dem<=4) {
			    				get_template_part('loop/content-time-line', 'post');
			    			} else {
			    				get_template_part('loop/content-time-line', 'more');
			    			}
							$result = ob_get_contents();
						ob_end_clean();
						$output .= $result;
						$dem++;
					endwhile;
				$output .= '</ul>';
				if ($dem > 4) {
			    	$output .= '<a class="loadmore" href="#">loadmore</a>';
				}
			$output .= '</div>';
		else : 
			$output .= '<div class="timeline">';
				ob_start();  
		    		get_template_part('loop/content-blog','none');
					$result = ob_get_contents();  
				ob_end_clean();
				$output .= $result;
			$output .= '</div>';
		endif;
    	
		return $output;
    }
    add_shortcode('theone-time-line', 'theone_time_line');


    /*
     * subscribe shortcode
    */
    function theone_subscribe ($atts, $content = null)
    {
    	extract(
    		shortcode_atts(
	    		array(
					'title' => '',
			        'desc' => '',
					'img' => '',
				), 
    			$atts
    		)
    	);

    	$urlimg = wp_get_attachment_url($img);

    	$output = '';

    	$output .= '<div id="subscribe" style="background-image: url('.$urlimg.')" data-stellar-background-ratio="0.1">';
	      $output .= '<div class="parallax-over"></div>';
	      $output .= '<div class="container">';
	        $output .= '<div class="row">';
	          $output .= '<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">';
	            $output .= '<div class="inner">';
	              $output .= '<h2><span>'.$title.'</span></h2>';
	              $output .= '<p class="text">'.$desc.'</p>';
		            ob_start();  
			    		get_template_part('content-parts/content-mail','chimp');
						$result = ob_get_contents();  
					ob_end_clean();
					$output .= $result;
	            $output .= '</div>';
	          $output .= '</div>';
	        $output .= '</div>';
	      $output .= '</div>';
	    $output .= '</div>';
    	
		return $output;
    }
    add_shortcode('theone_subscribe', 'theone_subscribe');
    
    
    function theone_compare_button_sc( $atts, $content = null ) {
        $atts = shortcode_atts(array(
            'product' => false,
            'type' => 'default',
            'container' => 'yes'
        ), $atts);
    
        $product_id = 0;
    
        /**
         * Retrieve the product ID in these steps:
         * - If "product" attribute is not set, get the product ID of current product loop
         * - If "product" contains ID, post slug or post title
         */
        if ( ! $atts['product'] ) {
            global $product;
            $product_id = isset( $product->id ) && $product->exists() ? $product->id : 0;
        } else {
            global $wpdb;
            $product = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE ID = %d OR post_name = %s OR post_title = %s LIMIT 1", $atts['product'], $atts['product'], $atts['product'] ) );
            if ( ! empty( $product ) ) {
                $product_id = $product->ID;
            }
        }
    
        // if product ID is 0, maybe the product doesn't exists or is wrong.. in this case, doesn't show the button
        if ( empty( $product_id ) ) return;
    
        ob_start();
        if ( $atts['container'] == 'yes' ) echo '<div class="woocommerce product compare-button">';
        theone_add_compare_link( $product_id, array(
            'button_or_link' => ( $atts['type'] == 'default' ? false : $atts['type'] ),
            'button_text' => empty( $content ) ? 'default' : $content
        ) );
        if ( $atts['container'] == 'yes' ) echo '</div>';
        return ob_get_clean();
    }
    add_shortcode( 'theone_compare_button', 'theone_compare_button_sc' );
    
    
    /** WC Shortcode **/
    add_shortcode( 'theone_recent_products', 'theone_recent_products' );
    add_shortcode( 'theone_best_selling_products', 'theone_best_selling_products' );
    add_shortcode( 'theone_featured_products', 'theone_featured_products' );
    add_shortcode( 'theone_sale_products', 'theone_sale_products' );
    add_shortcode( 'theone_top_rated_products', 'theone_top_rated_products' );
    add_shortcode( 'theone_social_links', 'theone_social_links' );
    
    function theone_recent_products( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
    	global $woocommerce_loop;
    
    	extract( shortcode_atts( array(
    		'per_page' 	      => '12',
            'paged'           => '1',
    		'columns' 	      => '4',
    		'orderby' 	      => 'date',
    		'order' 	      => 'desc',
            'ex_products_id'  => '0',
            'product_cat_id'  => '0',
            'loop_wrap'       => 'yes'
    	), $atts ) );
        
        $ex_products_id = explode( ',', $ex_products_id );
    
    	$meta_query = WC()->query->get_meta_query();
    
    	$args = array(
    		'post_type'				=> 'product',
    		'post_status'			=> 'publish',
            'post__not_in'          => $ex_products_id,
    		'ignore_sticky_posts'	=> 1,
    		'posts_per_page' 		=> $per_page,
            'paged'                 => $paged,
    		'orderby' 				=> $orderby,
    		'order' 				=> $order,
    		'meta_query' 			=> $meta_query
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
    
    	ob_start();
    
    	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
    
    	$woocommerce_loop['columns'] = $columns;
    
    	if ( $products->have_posts() ) : ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_start(); ?>
            <?php endif; ?>
    
    			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
    
    				<?php wc_get_template_part( 'content', 'product' ); ?>
    
    			<?php endwhile; // end of the loop. ?>
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_end(); ?>
            <?php endif; ?>
    
    	<?php endif;
    
    	wp_reset_postdata();
    
        if ( $loop_wrap == 'yes' ):
    	   return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
        else:
            return ob_get_clean();
        endif;
    }
    
    
    function theone_best_selling_products( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
    	global $woocommerce_loop;
    
    	extract( shortcode_atts( array(
    		'per_page'        => '12',
            'paged'           => '1',
    		'columns'         => '4',
            'ex_products_id'  => '0',    
            'product_cat_id'  => '0',
            'loop_wrap'       => 'yes'
    	), $atts ) );
        
        $ex_products_id = explode( ',', $ex_products_id );
    
    	$args = array(
    		'post_type' 			=> 'product',
    		'post_status' 			=> 'publish',
            'post__not_in'          => $ex_products_id,
    		'ignore_sticky_posts'   => 1,
    		'posts_per_page'		=> $per_page,
            'paged'                 => $paged,
    		'meta_key' 		 		=> 'total_sales',
    		'orderby' 		 		=> 'meta_value_num',
    		'meta_query' 			=> array(
    			array(
    				'key' 		=> '_visibility',
    				'value' 	=> array( 'catalog', 'visible' ),
    				'compare' 	=> 'IN'
    			)
    		)
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
    
    	ob_start();
    
    	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
    
    	$woocommerce_loop['columns'] = $columns;
    
    	if ( $products->have_posts() ) : ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_start(); ?>
            <?php endif; ?>
    
    			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
    
    				<?php wc_get_template_part( 'content', 'product' ); ?>
    
    			<?php endwhile; // end of the loop. ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_end(); ?>
            <?php endif; ?>
    
    	<?php endif;
    
    	wp_reset_postdata();
    
        if ( $loop_wrap == 'yes' ):
    	   return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
        else:
            return ob_get_clean();
        endif;
    }
    
    
    function theone_featured_products( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
    	global $woocommerce_loop;
    
    	extract( shortcode_atts( array(
    		'per_page'          => '12',
            'paged'             => '1',
    		'columns' 	        => '4',
    		'orderby' 	        => 'date',
    		'order' 	        => 'desc',
            'ex_products_id'    => '0',
            'product_cat_id'  => '0',
            'loop_wrap'         => 'yes'
    	), $atts ) );
        
        $ex_products_id = explode( ',', $ex_products_id );
    
    	$args = array(
    		'post_type'				=> 'product',
    		'post_status' 			=> 'publish',
            'post__not_in'          => $ex_products_id,
    		'ignore_sticky_posts'	=> 1,
    		'posts_per_page' 		=> $per_page,
            'paged'                 => $paged,
    		'orderby' 				=> $orderby,
    		'order' 				=> $order,
    		'meta_query'			=> array(
    			array(
    				'key' 		=> '_visibility',
    				'value' 	=> array('catalog', 'visible'),
    				'compare'	=> 'IN'
    			),
    			array(
    				'key' 		=> '_featured',
    				'value' 	=> 'yes'
    			)
    		)
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
    
    	ob_start();
    
    	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
    
    	$woocommerce_loop['columns'] = $columns;
    
    	if ( $products->have_posts() ) : ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_start(); ?>
            <?php endif; ?>
    
    			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
    
    				<?php wc_get_template_part( 'content', 'product' ); ?>
    
    			<?php endwhile; // end of the loop. ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_end(); ?>
            <?php endif; ?>
    
    	<?php endif;
    
    	wp_reset_postdata();
    
        if ( $loop_wrap == 'yes' ):
    	   return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
        else:
            return ob_get_clean();
        endif;
    }
    
    
    function theone_sale_products( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
    	global $woocommerce_loop;
    
    	extract( shortcode_atts( array(
    		'per_page'       => '12',
            'paged'          => '1',
    		'columns'        => '4',
    		'orderby'        => 'title',
    		'order'          => 'asc',
            'ex_products_id' => '0',
            'product_cat_id'  => '0',
            'loop_wrap'      => 'yes'
    	), $atts ) );
        
        $ex_products_id = explode( ',', $ex_products_id );
    
    	// Get products on sale
    	$product_ids_on_sale = wc_get_product_ids_on_sale();
        
        $product_ids_on_sale = array_diff( $product_ids_on_sale, $ex_products_id );
    
    	$meta_query   = array();
    	$meta_query[] = WC()->query->visibility_meta_query();
    	$meta_query[] = WC()->query->stock_status_meta_query();
    	$meta_query   = array_filter( $meta_query );
    
    	$args = array(
    		'posts_per_page'	=> $per_page,
            'paged'             => $paged,
    		'orderby' 			=> $orderby,
    		'order' 			=> $order,
    		'no_found_rows' 	=> 1,
    		'post_status' 		=> 'publish',
    		'post_type' 		=> 'product',
    		'meta_query' 		=> $meta_query,
    		'post__in'			=> $product_ids_on_sale
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
    
    	ob_start();
    
    	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
    
    	$woocommerce_loop['columns'] = $columns;
    
    	if ( $products->have_posts() ) : ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_start(); ?>
            <?php endif; ?>
    
    			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
    
    				<?php wc_get_template_part( 'content', 'product' ); ?>
    
    			<?php endwhile; // end of the loop. ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_end(); ?>
            <?php endif; ?>
    
    	<?php endif;
    
    	wp_reset_postdata();
        
        if ( $loop_wrap == 'yes' ):
    	   return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
        else:
            return ob_get_clean();
        endif;
    }
    
    
    function theone_top_rated_products( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
    	global $woocommerce_loop;
    
    	extract( shortcode_atts( array(
    		'per_page'       => '12',
            'paged'          => '1',
    		'columns'        => '4',
    		'orderby'        => 'title',
    		'order'          => 'asc',
            'ex_products_id' => '0',
            'product_cat_id' => '0',
            'loop_wrap'      => 'yes'
    		), $atts ) );
      
        $ex_products_id = explode( ',', $ex_products_id );
    
    	$args = array(
    		'post_type' 			=> 'product',
    		'post_status' 			=> 'publish',
            'post__not_in'          => $ex_products_id,
    		'ignore_sticky_posts'   => 1,
    		'orderby' 				=> $orderby,
    		'order'					=> $order,
    		'posts_per_page' 		=> $per_page,
            'paged'                 => $paged,
    		'meta_query' 			=> array(
    			array(
    				'key' 			=> '_visibility',
    				'value' 		=> array('catalog', 'visible'),
    				'compare' 		=> 'IN'
    			)
    		)
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
    
    	ob_start();
    
    	add_filter( 'posts_clauses', 'theone_order_by_rating_post_clauses' );
    
    	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
    
    	remove_filter( 'posts_clauses', 'theone_order_by_rating_post_clauses' );
    
    	$woocommerce_loop['columns'] = $columns;
    
    	if ( $products->have_posts() ) : ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_start(); ?>
            <?php endif; ?>
    
    			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
    
    				<?php wc_get_template_part( 'content', 'product' ); ?>
    
    			<?php endwhile; // end of the loop. ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_end(); ?>
            <?php endif; ?>
    
    	<?php endif;
    
    	wp_reset_postdata();
    
        if ( $loop_wrap == 'yes' ):
    	   return '<div class="woocommerce columns-' . $columns . '">' . ob_get_clean() . '</div>';
        else:
            return ob_get_clean();
        endif;
    }
    
    
    /**
     *  VC Shortcodes define
     **/
    
    
    function theone_icon_link_box( $atts ) {
        extract( shortcode_atts( array(
            'icon_class'    =>  '',
            'link'          =>  ''
        ), $atts ) );
        
        $icon_class = esc_attr( $icon_class );
        
        if ( function_exists( 'vc_build_link' ) ):
            $link = vc_build_link( $link );
        else:
            $link = array();
        endif; 
        
        $html = '';
        
        if ( !empty( $link ) ):
            
            $icon = ( $icon_class != '' ) ? '<i class="fa ' . $icon_class . '"></i>' : '';
            
            $html .=    '<div class="link-icon-box">
                            <h3>' . $icon . '<a target="' . esc_attr( $link['target'] ) . '" href="' . esc_url( $link['url'] ) . '">' . sanitize_text_field( $link['title'] ) . '</a></h3>
                        </div>';
            
        endif;
        
        return $html;
      
    }
    
    function theone_sale_products_carousel( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
    	global $woocommerce_loop;
    
    	extract( shortcode_atts( array(
            'title'         =>  '',
            'end_date'      =>  '',
            'same_end_date' =>  'yes',
    		'per_page'       => '12',
            'paged'          => '1',
    		'columns'        => '3',
    		'orderby'        => 'title',
    		'order'          => 'asc',
            'ex_products_id' => '0',
            'loop_wrap'      => 'yes'
    	), $atts ) );
        
        $ex_products_id = explode( ',', $ex_products_id );
        
        $title_html =   '<h2 class="section-title">
                            <span>' . sanitize_text_field( $title ) . '</span>
                            <span class="pull-right title-info">
                                <span>' . __( 'End In' ) . '</span>
                                <i class="fa fa-clock-o"></i>
                                <span data-end-time="' . date( 'Y/m/d', strtotime( $end_date ) ) . '" class="js-countdown">' . $end_date . '</span>
                            </span>
                        </h2>';
        
        $compare = ( trim( $same_end_date ) == 'yes' ) ? '=' : '<=';
    
    	// Get products on sale
    	//$product_ids_on_sale = wc_get_product_ids_on_sale();
        $product_ids_on_sale = theone_wc_get_product_ids_on_sale_before_end_date( $end_date, $compare );
        
        $product_ids_on_sale = array_diff( $product_ids_on_sale, $ex_products_id );
        $product_ids_on_sale[] =  0;
    
    	$meta_query   = array();
    	$meta_query[] = WC()->query->visibility_meta_query();
    	$meta_query[] = WC()->query->stock_status_meta_query();
    	$meta_query   = array_filter( $meta_query );
    
    	$args = array(
    		'posts_per_page'	=> $per_page,
            'paged'             => $paged,
    		'orderby' 			=> $orderby,
    		'order' 			=> $order,
    		'no_found_rows' 	=> 1,
    		'post_status' 		=> 'publish',
    		'post_type' 		=> 'product',
    		'meta_query' 		=> $meta_query,
    		'post__in'			=> $product_ids_on_sale
    	);
        
    	ob_start();
    
    	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
    
    	$woocommerce_loop['columns'] = $columns;
    
    	if ( $products->have_posts() ) : ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_start(); ?>
            <?php endif; ?>
    
    			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
    
    				<?php wc_get_template_part( 'content', 'product' ); ?>
    
    			<?php endwhile; // end of the loop. ?>
            
            <?php if ( $loop_wrap == 'yes' ): ?>
    		  <?php woocommerce_product_loop_end(); ?>
            <?php endif; ?>
    
    	<?php endif;
    
    	wp_reset_postdata();
        
        if ( $loop_wrap == 'yes' ):
            return '<div class="woocommerce nl-products-carousel-wrap nl-columns-' . $columns . '">' . $title_html . ob_get_clean() . '</div>';
        else:
            return $title_html . ob_get_clean();
        endif;
    }
    
    function theone_advs_carousel( $atts ) {
        extract( shortcode_atts( array(
            'term_id'       =>  '0',
            'advs_num'      =>  '4'
        ), $atts ) );
        
        $term_id = intval( $term_id );
        $advs_num = max( 1, intval( $advs_num ) );
        
        $html = '';
        
        $query_args = array(
            'post_type' =>  'advertise',
            'showposts' =>  $advs_num,
            'post_status'   =>  array( 'publish' )
        );
        if ( $term_id > 0 ):
            $query_args['tax_query'] = array(
        		array(
        			'taxonomy'   => 'adv_cat',
        			'field'      => 'ids',
        			'terms'      => $procat_id
        		)
        	);
        endif;
        
        $query_advs = new WP_Query( $query_args );
        
        if ( $query_advs->have_posts() ):
            
            $html .= '<div class="advs-wrap">
                        <ul class="advs-list">';
            while ( $query_advs->have_posts() ): $query_advs->the_post();
                
                $adv_link = trim( esc_url( get_post_meta( get_the_ID(), 'theone_advertise_link', true ) ) );
                $use_permalink = trim( get_post_meta( get_the_ID(), 'theone_use_permalink', true ) );
                $use_permalink = $use_permalink == 'on' || $use_permalink == 'yes';
                
                if ( $use_permalink ):
                    $adv_link = get_permalink();
                endif;
                
                $thumb_src = '';
                if ( has_post_thumbnail() ):
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), '460x440' );
                    $thumb_src = $thumb['0'];
                else:
                    if ( function_exists( 'theone_no_image' ) ):
                        $thumb_src = theone_no_image( array( 'width' => 460, 'height' => 440 ), false, false );
                    endif;
                endif;
                
                $img_html = '';
                if ( $thumb_src != '' ):
                    $img_html = '<div class="img-wrap">
                                    <img src="' . esc_url( $thumb_src ) . '" alt="' . get_the_title() . '" />
                                </div>';
                endif;
                
                if ( $img_html != '' && $adv_link != '' ):
                    $img_html = '<a class="hover-overlay" href="' . $adv_link . '" title="' . get_the_title() . '">' . $img_html . '</a>';
                endif;
                
                $content_html = '';
                if ( trim( get_the_content() ) != '' ):
                    
                    $content_html .= '<div class="adv-content">' . wpautop( do_shortcode( get_the_content() ) ) . '</div>';
                    
                endif;
                
                $html .=    '<li class="adv-item">
                                <h3 class="adv-title">' . get_the_title() . '</h3>
                                ' . $img_html . '
                                ' . $content_html . '
                            </li>';
            
            endwhile;
            $html .=    '</ul><!-- .advs-list -->
                    </div><!-- .advs-wrap -->';
        
        endif;
        
        return $html;
      
    }  
    
    
    function theone_product_tabs( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
    
    	extract( shortcode_atts( array(
            'title'             =>  '',
            'product_cat_id'    =>  '0',
            'products_num'      =>  '',
            'recent_products'   =>  'yes',
    		'best_selling_products'   => 'yes',
            'featured_products' =>  'yes',
            'sale_products'     =>  'yes',
            'top_rated_products'    =>  'yes',
            'enable_carousel'   =>  'yes',
            'product_per_slide' =>  '4'
    	), $atts ) );
        
        $products_num = max( 1, intval( $products_num ) );
        $product_per_slide = max( 1, intval( $product_per_slide ) );
        
        
        $tabs_args = array(
            'recent_products'       =>  __( 'New Arrivals', 'ovictheme' ),
            'best_selling_products' =>  __( 'Best Sellers', 'ovictheme' ),
            'featured_products'     =>  __( 'Featured Products', 'ovictheme' ),
            'sale_products'         =>  __( 'Top Sales', 'ovictheme' ),
            'top_rated_products'    =>  __( 'Top Rated', 'ovictheme' )
        );
        
        $enable_recent_tab = trim( strtolower( $recent_products ) ) == 'yes';
        $enable_best_selling_tab = trim( strtolower( $best_selling_products ) ) == 'yes';
        $enable_featured_tab = trim( strtolower( $featured_products ) ) == 'yes';
        $enable_sale_tab = trim( strtolower( $sale_products ) ) == 'yes';
        $enable_top_rated_tab = trim( strtolower( $top_rated_products ) ) == 'yes';
        $enable_carousel = trim( strtolower( $enable_carousel ) ) == 'yes';
        
        if ( !( $enable_recent_tab || $enable_best_selling_tab || $enable_featured_tab || $enable_sale_tab || $enable_top_rated_tab ) ):
            return '';
        endif;
        
        $html = '';
        $tab_ul_html = '';
        $tab_contents_html = '';
        $title_html = '';
        
        if ( trim( $title ) != '' ):
            
            $title_html .= '<h2 class="section-title">' . sanitize_text_field( $title ) . '</h2>';
            
        endif;
        
        $tab_id = uniqid( 'nl-products-tabs-' );
        $tab_ul_html .= '<ul class="pull-right">';
        
        foreach ( $tabs_args as $tab_key => $tab_name ):
            
            $tab_content_id = uniqid( $tab_key . '-' );
            if ( trim( strtolower( $$tab_key ) ) == 'yes' ):
                
                $tab_content_class = ( $enable_carousel ) ? 'nl-has-carousel' : '';
                $tab_shortcode = sprintf( '[theone_%s per_page="%d" columns="%d" product_cat_id="%d" ]', $tab_key, $products_num, $product_per_slide, $product_cat_id );
                $tab_ul_html .= '<li><a href="#' . $tab_content_id . '">' . $tab_name . '</a></li>';
                $tab_contents_html .= '<div data-products-per-slide="' . $product_per_slide . '" id="' . $tab_content_id . '" class="nl-tab-content ' . $tab_content_class . '">';
                $tab_contents_html .= do_shortcode( $tab_shortcode );
                $tab_contents_html .= '</div>';
                
            endif;
            
            
        endforeach;
        
        $tab_ul_html .= '</ul>';
        
        $html .= '<div class="nl-products-tabs-wrap">';
        $html .= '<div id="' . $tab_id . '" class="nl-product-tabs hl-core-tabs nl-tabs">';
        $html .= '<div class="nl-tab-header-wrap">' . $title_html . $tab_ul_html . '</div>'; 
        $html .= $tab_contents_html;
        $html .= '</div><!-- .nl-product-tabs -->';
        $html .= '</div><!-- .nl-products-tabs-wrap -->';
        
        
        return $html;
        
                   
    }
    
    
    function theone_intro_txt_img( $atts, $content = null ) {
        
        extract( shortcode_atts( array(
            'title'             =>  '',
            'read_more'         =>  '',
            'bg_img'            =>  ''
    	), $atts ) );
        
        if ( function_exists( 'vc_build_link' ) ):
            $read_more = vc_build_link( $read_more );
        else:
            $read_more = array();
        endif; 
        
        $content = wpb_js_remove_wpautop( $content, true );
        //$content = wpautop( do_shortcode( $content ) );
        
        $html = '';
        $title_html = '';
        $content_html = '';
        $readmore_html = '';
        $content_wrap_style = '';
        
        if ( trim( $title ) != '' ):
            
            $title_html .= '<h3 class="block-title">' . sanitize_text_field( $title ) . '</h3>';
            
        endif;
        
        if ( trim( $content ) != '' ):
            
            $content_html .= $content;
            
        endif;
        
        if ( !empty( $read_more ) ):
            
            $readmore_html .= sprintf( '<a class="readmore" target="%s" href="%s" title="%s">%s</a>', esc_attr( $read_more['target'] ), esc_url( $read_more['url'] ), sanitize_text_field( $read_more['title'] ), sanitize_text_field( $read_more['title'] ) );
            
        endif;
        
        if ( intval( $bg_img ) > 0 ):
        
            $img = wp_get_attachment_image_src( $bg_img, 'full' );
            $content_wrap_style = 'style="background: url(' . esc_url( $img[0] ) . ') no-repeat center center;"';
            
        endif;
        
        $html .= $title_html;
        $html .= '<div class="block-content" ' . $content_wrap_style . '>';
        $html .= '<div class="wrapper">';
        $html .= '<div class="content">';
        $html .= $content_html . $readmore_html;
        $html .= '</div><!-- .content -->';
        $html .= '</div><!-- .wrapper -->';
        $html .= '</div><!-- .block-content -->';
        
        return $html;
        
    }
    
    
    function theone_lastest_blog_carousel( $atts ) {
        
        extract( shortcode_atts( array(
            'title'     =>  '',
            'cat_id'    =>  0,
            'limit'     =>  8
    	), $atts ) );
        
        $limit = max( 1, intval( $limit ) );
        $cat_id = intval( $cat_id );
        
        $html = '';
        $title_html = '';  
        $items_html = '';
        
        if ( trim( $title ) != '' ):
            
            $title_html .= '<h3 class="block-title">' . sanitize_text_field( $title ) . '</h3>';
            
        endif;
        
        $query_args = array(
            'showposts'     =>  $limit,
            'post_status'   =>  array( 'publish' )
        );
        
        if ( $cat_id > 0 ):
        
            $query_args['tax_query'] = array(
        		array(
        			'taxonomy'   => 'category',
        			'field'      => 'ids',
        			'terms'      => $cat_id
        		)
        	);
            
        endif;
        
        $query_posts = new WP_Query( $query_args );
        if( $query_posts->have_posts() ):
            
            $thumb_size = 'full';
            $img_size = get_intermediate_image_sizes();
            
            if ( isset( $img_size['370x415'] ) ):
                $thumb_size = '370x415';
            endif;
            
            while ( $query_posts->have_posts() ) : $query_posts->the_post(); 
                
                $item_style = '';
                
                if ( has_post_thumbnail() ):
                    
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), $thumb_size );
                    $thumb_url = $thumb['0'];
                    $item_style = 'style="background: url(' . esc_url( $thumb_url ) . ') no-repeat center center #fe983d;"';
                    
                else:
                    
                    $item_style = 'style="background: #fe983d;"';
                
                endif;
                
                $comments_count = wp_count_comments( get_the_ID() );
                
                $items_html .= '<div class="item" ' . $item_style . '>';
                $items_html .= '<div class="block-content">';
                $items_html .= '<div class="wrapper">';
                $items_html .= '<div class="content">';
                $items_html .= '<h4>' . get_the_title() . '</h4>';
                $items_html .= '<p class="author-info">' . __( 'post by:', 'theone-core' ) . ' ' . get_the_author_meta( 'display_name' ) . htmlentities2( ' / ' ) . get_the_date() . htmlentities2( ' / ' ) . __( 'comments:', 'theone-core' ) . ' ' . $comments_count->approved . '</p>';
                $items_html .= wpautop( theone_get_the_excerpt( get_the_content(), 100 ) );
                $items_html .= '<a class="readmore" href="' . get_permalink() . '" title="' . get_the_title() . '">' . __( 'Read more', 'theone-core' ) . '</a>';
                $items_html .= '</div><!-- .content -->';
                $items_html .= '</div><!-- .wrapper -->';
                $items_html .= '</div><!-- .block-content -->';
                $items_html .= '</div><!-- .item -->';
                
            endwhile;
            
            $html .= $title_html;
            $html .= '<div class="lastest-blog-block-wrap">';
            $html .= '<div class="lastest-blog-carousel theone-carousel effect-zoomIn" data-items-per-slide="1">';
            $html .= $items_html;
            $html .= '</div><!-- .lastest-blog-carousel -->';
            $html .= '</div><!-- .lastest-blog-block-wrap -->';
            
        endif;
        
        return $html;
        
    }
    
    
    function theone_brand_logos_carousel( $atts ) {
        
        extract( shortcode_atts( array(
            'title'             =>  '',
            'limit'             =>  12,
            'bg_img_id'         =>  0,
            'enable_parallax'   =>  'yes'
    	), $atts ) );
        
        
        
        ob_start();
        echo "<pre>";
            print_r($atts);
            echo "</pre>";
        return ob_get_clean();
    }
    
    function theone_img_advs_group( $atts ) {
        
        extract( shortcode_atts( array(
            'img1'      =>  0,
            'link1'     =>  '',
            'img2'      =>  0,
            'link2'     =>  '',
            'img3'      =>  0,
            'link3'     =>  '',
            'img4'      =>  0,
            'link4'     =>  ''
    	), $atts ) );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        
        if ( function_exists( 'vc_build_link' ) ):
            $link1 = vc_build_link( $link1 );
            $link2 = vc_build_link( $link2 );
            $link3 = vc_build_link( $link3 );
            $link4 = vc_build_link( $link4 );
        else:
            $link1 = $link_default;
            $link2 = $link_default;
            $link3 = $link_default;
            $link4 = $link_default;
        endif; 
        
        
        
        $html = '';
        $col_left_html = '';
        $col_right_html = '';
        
        if ( intval( $img1 ) > 0 ):
        
            $image1 = wp_get_attachment_image_src( $img1, '580x220' );
            if ( trim( $link1['url'] ) != '' ):
                
                $col_left_html .= '<div><a class="hover-overlay" href="' . esc_url( $link1['url'] ) . '" title="' . esc_attr( $link1['title'] ) . '" ><img src="' . esc_url( $image1[0] ) . '" alt="" /></a></div>';
                
            else:
            
                $col_left_html .= '<div><img src="' . esc_url( $image1[0] ) . '" alt="" /></div>';
            
            endif;
            
        endif;
        
        if ( intval( $img2 ) > 0 ):
        
            $image2 = wp_get_attachment_image_src( $img2, '580x230' );
            if ( trim( $link2['url'] ) != '' ):
                
                $col_left_html .= '<div><a class="hover-overlay" href="' . esc_url( $link2['url'] ) . '" title="' . esc_attr( $link2['title'] ) . '" ><img src="' . esc_url( $image2[0] ) . '" alt="" /></a></div>';
                
            else:
            
                $col_left_html .= '<div><img src="' . esc_url( $image2[0] ) . '" alt="" /></div>';
            
            endif;
            
        endif;
        
        if ( intval( $img3 ) > 0 ):
        
            $image3 = wp_get_attachment_image_src( $img3, '285x460' );
            if ( trim( $link3['url'] ) != '' ):
                
                $col_right_html .= '<div class="col-xs-6"><a class="hover-overlay" href="' . esc_url( $link3['url'] ) . '" title="' . esc_attr( $link3['title'] ) . '" ><img src="' . esc_url( $image3[0] ) . '" alt="" /></a></div>';
                
            else:
            
                $col_right_html .= '<div class="col-xs-6"><img src="' . esc_url( $image3[0] ) . '" alt="" /></div>';
            
            endif;
            
        endif;
        
        if ( intval( $img4 ) > 0 ):
        
            $image4 = wp_get_attachment_image_src( $img4, '285x460' );
            if ( trim( $link4['url'] ) != '' ):
                
                $col_right_html .= '<div class="col-xs-6"><a class="hover-overlay" href="' . esc_url( $link4['url'] ) . '" title="' . esc_attr( $link4['title'] ) . '" ><img src="' . esc_url( $image4[0] ) . '" alt="" /></a></div>';
                
            else:
            
                $col_right_html .= '<div class="col-xs-6"><img src="' . esc_url( $image4[0] ) . '" alt="" /></div>';
            
            endif;
            
        endif;
        
        $col_left_html = '<div class="col-sm-6 col-left">' . $col_left_html . '</div><!-- .col-left -->';
        $col_right_html = '<div class="col-sm-6 col-right">' . $col_right_html . '</div><!-- .col-right -->';
        
        $html = '<div class="theone-advs-imgs-block row">' . $col_left_html . $col_right_html . '</div><!-- .theone-advs-imgs-block -->';
        
        return $html;
        
    }
    
    function theone_social_links( $atts ) {
        
        extract( shortcode_atts( array(
            'facebook_link'     =>  '',
            'twitter_link'      =>  '',
            'gplus_link'        =>  '',
            'linkedin_link'     =>  '',
            'pinterest_link'    =>  '',
            'rss_link'          =>  '',
            'css'               =>  ''
    	), $atts ) );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        $html = '';
        
        if ( function_exists( 'vc_build_link' ) ):
            $facebook_link = vc_build_link( $facebook_link );
            $twitter_link = vc_build_link( $twitter_link );
            $gplus_link = vc_build_link( $gplus_link );
            $linkedin_link = vc_build_link( $linkedin_link );
            $pinterest_link = vc_build_link( $pinterest_link );
            $rss_link = vc_build_link( $rss_link );
        else:
            $facebook_link = $link_default;
            $twitter_link = $link_default;
            $gplus_link = $link_default;
            $linkedin_link = $link_default;
            $pinterest_link = $link_default;
            $rss_link = $link_default;
        endif;    
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;    
        
        if ( trim( $facebook_link['url'] ) != '' ):
        
            $html .= '<a class="social-link facebook-link" target="' . $facebook_link['target'] . '" href="' . $facebook_link['url'] . '" title="' . $facebook_link['title'] . '"><i class="fa fa-facebook"></i></a>';
        
        endif;
        
        if ( trim( $twitter_link['url'] ) != '' ):
        
            $html .= '<a class="social-link twitter-link" target="' . $twitter_link['target'] . '" href="' . $twitter_link['url'] . '" title="' . $twitter_link['title'] . '"><i class="fa fa-twitter"></i></a>';
        
        endif;
        
        if ( trim( $gplus_link['url'] ) != '' ):
        
            $html .= '<a class="social-link gplus-link" target="' . $gplus_link['target'] . '" href="' . $gplus_link['url'] . '" title="' . $gplus_link['title'] . '"><i class="fa fa-google-plus"></i></a>';
        
        endif;
        
        if ( trim( $linkedin_link['url'] ) != '' ):
        
            $html .= '<a class="social-link linkedin-link" target="' . $linkedin_link['target'] . '" href="' . $linkedin_link['url'] . '" title="' . $linkedin_link['title'] . '"><i class="fa fa-linkedin"></i></a>';
        
        endif;
        
        if ( trim( $pinterest_link['url'] ) != '' ):
        
            $html .= '<a class="social-link pinterest-link" target="' . $pinterest_link['target'] . '" href="' . $pinterest_link['url'] . '" title="' . $pinterest_link['title'] . '"><i class="fa fa-pinterest"></i></a>';
        
        endif;
        
        if ( trim( $rss_link['url'] ) != '' ):
        
            $html .= '<a class="social-link rss-link" target="' . $rss_link['target'] . '" href="' . $rss_link['url'] . '" title="' . $rss_link['title'] . '"><i class="fa fa-rss"></i></a>';
        
        endif;
        
        $html = '<div class="social-links-wrap ' . $css_class . '">' . $html . '</div>';
        
        return $html;
    }
    
    
    function theone_team_member( $atts ) {
        
        extract( shortcode_atts( array(
            'img_id'            =>  0,
            'member_name'       =>  '',
            'position'          =>  '',
            'description'       =>  '',
            'facebook_link'     =>  '',
            'twitter_link'      =>  '',
            'gplus_link'        =>  '',
            'linkedin_link'     =>  '',
            'pinterest_link'    =>  '',
            'rss_link'          =>  '',
            'readmore_link'     =>  '',
            'css'               =>  ''
    	), $atts ) );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        $member_name = sanitize_text_field( $member_name );
        $position = sanitize_text_field( $position );
        $description = wpautop( $description );
        
        if ( function_exists( 'vc_build_link' ) ):
            $facebook_link = vc_build_link( $facebook_link );
            $twitter_link = vc_build_link( $twitter_link );
            $gplus_link = vc_build_link( $gplus_link );
            $linkedin_link = vc_build_link( $linkedin_link );
            $pinterest_link = vc_build_link( $pinterest_link );
            $rss_link = vc_build_link( $rss_link );
            $readmore_link = vc_build_link( $readmore_link );
        else:
            $facebook_link = $link_default;
            $twitter_link = $link_default;
            $gplus_link = $link_default;
            $linkedin_link = $link_default;
            $pinterest_link = $link_default;
            $rss_link = $link_default;
            $readmore_link = $link_default;
        endif;
        
        $html = '';
        $img_html = '';
        $social_links_html = '';
        $img_src = '';
        $css_class = '';
        
        if ( intval( $img_id ) > 0 ):
            
            $img = wp_get_attachment_image_src( intval( $img_id ), '270x290' );
            $img_src = $img[0];
            
        else:
        
            $img_src = theone_no_image( array( 'width' => 270, 'height' => 290 ) );
        
        endif;
        
        $img_html = '<img src="' . esc_url( $img_src ) . '" alt="' . esc_attr( $member_name ) . '" />';
        
        
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;    
        
        if ( trim( $facebook_link['url'] ) != '' ):
        
            $social_links_html .= '<a class="social-link facebook-link" target="' . $facebook_link['target'] . '" href="' . $facebook_link['url'] . '" title="' . $facebook_link['title'] . '"><i class="fa fa-facebook"></i></a>';
        
        endif;
        
        if ( trim( $twitter_link['url'] ) != '' ):
        
            $social_links_html .= '<a class="social-link twitter-link" target="' . $twitter_link['target'] . '" href="' . $twitter_link['url'] . '" title="' . $twitter_link['title'] . '"><i class="fa fa-twitter"></i></a>';
        
        endif;
        
        if ( trim( $gplus_link['url'] ) != '' ):
        
            $social_links_html .= '<a class="social-link gplus-link" target="' . $gplus_link['target'] . '" href="' . $gplus_link['url'] . '" title="' . $gplus_link['title'] . '"><i class="fa fa-google-plus"></i></a>';
        
        endif;
        
        if ( trim( $linkedin_link['url'] ) != '' ):
        
            $social_links_html .= '<a class="social-link linkedin-link" target="' . $linkedin_link['target'] . '" href="' . $linkedin_link['url'] . '" title="' . $linkedin_link['title'] . '"><i class="fa fa-linkedin"></i></a>';
        
        endif;
        
        if ( trim( $pinterest_link['url'] ) != '' ):
        
            $social_links_html .= '<a class="social-link pinterest-link" target="' . $pinterest_link['target'] . '" href="' . $pinterest_link['url'] . '" title="' . $pinterest_link['title'] . '"><i class="fa fa-pinterest"></i></a>';
        
        endif;
        
        if ( trim( $rss_link['url'] ) != '' ):
        
            $social_links_html .= '<a class="social-link rss-link" target="' . $rss_link['target'] . '" href="' . $rss_link['url'] . '" title="' . $rss_link['title'] . '"><i class="fa fa-rss"></i></a>';
        
        endif;
        
        $social_links_html = '<div class="social-links-wrap">' . $social_links_html . '</div>';
        
        
        $html .= $img_html . $social_links_html;
        
        if ( trim( $member_name ) != '' ):
            $html .= '<h4>' . $member_name . '</h4>';
        endif;
        
        if ( trim( $position ) != '' ):
            $html .= '<em>' . $position . '</em>';
        endif;
        
        if ( trim( $description ) != '' ):
            $html .= '<div class="desc">' . $description . '</div>';
        endif;
        
        if ( trim( $readmore_link['url'] ) != '' ):
            $html .= '<em><a class="readmore-link" target="' . $readmore_link['target'] . '" href="' . $readmore_link['url'] . '" title="' . $readmore_link['title'] . '">' . $readmore_link['title'] . '</a></em>';
        endif;
        
        $html = '<div class="team-member-wrap ' . $css_class . '">' . $html . '</div>';
        
        return $html;
    }
    
    
    
    /**
     *  Theone shortcodes ========================
     **/
    
    function ts_single_service( $atts, $content = null ) {
        
        extract( shortcode_atts( array(
            'service_style' =>  '',
            'title'         =>  '',
            'subtitle'      =>  '',
            'icon'          =>  '',
            'link_btn'      =>  '',
            'bg_img_id'     =>  '0',
            'overlay_color' =>  '',
            'css_animation' =>  '',
            'css'           =>  '' 
    	), $atts ) );
        
        $plumber_services = array( 'style-1', 'style-2', 'style-3', 'style-4', 'style-5' );  // Plumber
        $construction_services = array( 'style-6', 'style-7', 'style-8', 'style-9', 'style-10' ); // Construction
        $mechanic_services = array( 'style-11', 'style-12', 'style-13', 'style-24' ); // Mechanic (style-24 is added after complete)
        $clearning_services = array( 'style-14', 'style-15', 'style-25' ); // Clearning (style-25 is added after complete)
        $carpenter_services = array( 'style-16' ); // Carpenter
        $metal_construction_services = array( 'style-17', 'style-17a', 'style-18' ); // Metal Construction
        $mining_services = array( 'style-19', 'style-20' ); // Mining
        $maintenance_services = array( 'style-21', 'style-22', 'style-23', 'style-26' ); // Maintenance (style-26 is added after complete)
        $electrician_services = array( 'style-27', 'style-28' ); // Electrician
        $renovation_services = array( 'style-29', 'style-33', 'style-34' ); // Renovation (style-33, style-34 are added after complete)
        $gardner_services = array( 'style-30', 'style-31', 'style-32' ); // Gardner
        $autoshop_services = array( 'style-35' );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
            $content = wpb_js_remove_wpautop( $content, true );   
        }
        
        if ( function_exists( 'vc_build_link' ) ):
            $link_btn = vc_build_link( $link_btn );
        else:
            $link_btn = $link_default;
        endif;
        
        $img_src = '';
        $thumbnail_size = '555x393'; // Default Plumber 
        $thumbnail_size_args = array( 555, 393 );
        
        switch ( $service_style ):
            
            case 'style-12': case 'style-1': case 'style-14': case 'style-16': case 'style-19':
                $thumbnail_size = '555x393';
                break;
            
            case 'style-17': case 'style-20': case 'style-33': case 'style-34': case 'style-25': 
                $thumbnail_size = '405x402';
                break;
            
            case 'style-11': case 'style-24': 
                $thumbnail_size = '410x407';
                break;
            
            case 'style-13': 
                $thumbnail_size = '160x160';
                break;
            
            case 'style-21': case 'style-26': case 'style-9': case 'style-3': case 'style-2': case 'style-10': case 'style-34': 
                $thumbnail_size = '444x444';
                break;
        
        endswitch;
        
        $thumbnail_size_args = explode( 'x', $thumbnail_size );
        
        if ( intval( $bg_img_id ) > 0 ):
            
            $img_src = theone_get_img_src_by_id( $bg_img_id, $thumbnail_size );
            
        else:
            
            $img_src = theone_no_image( array( 'width' => 540, 'height' => 536 ), false, true );
        
        endif;
        
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        $service_class = 'ts-service service-' . esc_attr( $service_style ) . ' ' . $css_animation . ' ' . $css_class;
        
        $overlay_style = '';
        if ( trim( $overlay_color ) != '' ) {
            $overlay_style = 'style="background-color: ' . $overlay_color . ';"';
        }
        else{
            $overlay_style = 'style="background-color: transparent;"';
        }
        
        $read_more_html = '';
        if ( trim( $link_btn['title'] ) != '' ):
            $read_more_html = '<a class="more-link ts-button-sv" href="' . esc_url( $link_btn['url'] ) . '">' . sanitize_text_field( $link_btn['title'] ) . '</a>';
        endif;
        
        ob_start();
        
        ?>
        
        <?php if ( in_array( $service_style, $plumber_services ) || $service_style == 'style-11' ): // is plumber services or style 11 (mechanic) ?>
        
            <?php if ( $service_style != 'style-5' ): ?>
            
            <div class="<?php echo esc_attr( $service_class ); ?>">
                                
                <div class="ts-service-item">
                    <div class="ts-inner">
                        <img alt="<?php _e( 'Service-img', 'theone-core' ); ?>" src="<?php echo esc_url( $img_src ); ?>" />
                        <div class="ts-overlay" <?php echo $overlay_style; ?>>
                            <div class="ts-overlay-middle">
                                <div class="ts-overlay-content">
                                    <span class="<?php echo $icon; ?>"></span>
                                    <h3><a href="#" title="<?php echo esc_attr( $title ); ?>"><?php echo sanitize_text_field( $title ); ?></a></h3>
                                    <div class="ts-excerpt"><?php echo strip_tags( apply_filters( 'the_content', $content ) ); ?></div>
                                    <?php echo $read_more_html; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.ts-service-item-->
                    
            </div> <!-- /.ts-service -->
            
            <?php else: // Service style 5 (No icon, Title, Subtitle, Description, Link) ?>
                
                <div class="ts-intro <?php echo esc_attr( $css_animation ); ?>">
                    <div class="number"><?php echo sanitize_text_field( $subtitle ); ?></div>
                    <h5><?php echo sanitize_text_field( $title ); ?></h5>
                    <?php echo apply_filters( 'the_content', $content ); ?>
                    <?php echo $read_more_html; ?>
                </div><!-- /.ts-intro -->
                
            <?php endif; ?>
        
        <?php endif; // End if ( in_array( $service_style, $plumber_services ) ) ?>
        
        <?php if ( in_array( $service_style, $construction_services ) || $service_style == 'style-15' || $service_style == 'style-25' || 
        in_array( $service_style, array_merge( $electrician_services, $renovation_services, $gardner_services ) ) ): // Services html of Construction, Clearning (style-15, style-25), Electrician, Renovation, Gardner ?>
        
            <?php 
            $contruction_service_html = '';
            
            $title_html = '';
            if ( trim( $link_btn['url'] ) != '' ) {
                $title_html = '<h3><a href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $title ) . '" target="' . esc_attr( $link_btn['target'] ) . '">' . sanitize_text_field( $title ) . '</a></h3>';
            }
            else{
                $title_html = '<h3>' . sanitize_text_field( $title ) . '</h3>';
            }
            
            switch ( $service_style ):
            
                case 'style-6':
                    $contruction_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                            						<div class="content-service">
                            							<span class="' . $icon . '"></span>
                            							' . $title_html . '
                            						</div>
                            						<div class="content-hover">
                            							<div class="ts-table">
                            								<div class="ts-table-cell">
                            									<p>' . strip_tags( apply_filters( 'the_content', $content ) ) . '</p>
                            									' . $read_more_html . '
                            								</div>
                            							</div>
                            						</div>
                            					</div>';
                    break;
                
                case 'style-7':
                    
                    $contruction_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                            						<div class="title-service">
                            							' . $title_html . '
                                                        <span class="' . $icon . '"></span>
                            						</div>
                            						 <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                            						' . $read_more_html . '
                            					</div>';
                    break;
                
                case 'style-8': case 'style-15': case 'style-27' : case 'style-28': case 'style-29': case 'style-30': case 'style-31': case 'style-32': case 'style-33': case 'style-34':
                    
                    $contruction_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                                	                <span class="' . $icon . '"></span>
                                	                ' . $title_html . '
                                	                <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                		        </div>';
                    
                    break;
                
                case 'style-9':
                    
                    $contruction_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                            						<figure><img src="' . esc_url( $img_src ) . '" alt=""></figure>
                            						<div class="content-service">
                            							<span class="' . $icon . '"></span>
                            							<h3>' . sanitize_text_field( $title ) . '</h3>
                            						</div>
                            						<div class="content-hover">
                            							<div class="ts-table">
                            								<div class="ts-table-cell">
                            									<p>' . strip_tags( apply_filters( 'the_content', $content ) ) . '</p>
                            									' . $read_more_html . '
                            								</div>
                            							</div>
                            						</div>
                            					</div>';
                    
                    break;
                
                case 'style-10': case 'style-25':
                    
                    $contruction_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                            						<figure><img src="' . esc_url( $img_src ) . '" alt=""></figure>
                            						<div class="content-service">
                            							<span class="' . $icon . '"></span>
                            							<h3>' . sanitize_text_field( $title ) . '</h3>
                            						</div>
                            						<div class="content-hover">
                            							<div class="ts-table">
                            								<div class="ts-table-cell">
                            									<p>' . strip_tags( apply_filters( 'the_content', $content ) ) . '</p>
                            									' . $read_more_html . '
                            								</div>
                            							</div>
                            						</div>
                            					</div>';
                    
                    break;
                
            endswitch;
            
            echo $contruction_service_html; 
            ?>
        
        <?php endif; // End if ( in_array( $service_style, $construction_services ) ) ?>
        
        <?php if ( in_array( $service_style, $mechanic_services ) || $service_style == 'style-14' ): // Services html of Mechanic, Clearning (style-14) ?>
        
            <?php
            
            $title_html = '';
            if ( trim( $link_btn['url'] ) != '' ) {
                $title_html = '<h3><a href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $title ) . '" target="' . esc_attr( $link_btn['target'] ) . '">' . sanitize_text_field( $title ) . '</a></h3>';
            }
            else{
                $title_html = '<h3>' . sanitize_text_field( $title ) . '</h3>';
            }
            
            $mechanic_service_html = '';
            $img_html = trim( $img_src ) != '' ? '<img src="' . esc_url( $img_src ) . '" alt="">' : '';
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '"></span>' : '';
            
            switch ( $service_style ):
            
                case 'style-12': case 'style-14':
                    $mechanic_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                                                    <div class="ts-service-item">
                                                        <div class="ts-inner">
                                                        	<div class="ts-service-img">
                                                                ' . $img_html . '
                                                            	' . $icon_html . '
                                                            </div>
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                    ' . $read_more_html . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!--/.ts-service-item-->
                                                </div>';
                    break;
                
                case 'style-13':
                    $mechanic_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                                                    <div class="ts-service-item">
                                                        <div class="ts-inner">
                                                        	<div class="ts-service-img align-left">
                                                            	' . $img_html . '
                                                            </div>
                                                            <div class="ts-service-content align-right">
                                                                ' . $title_html . '
                                                                <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                            </div>
                                                        </div>
                                                    </div><!--/.ts-service-item-->
                                                </div>';
                    break;     
                      
                case 'style-24': 
                    $mechanic_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                                                <div class="ts-service-item">
                                                    <div class="ts-inner">
                                                        ' . $img_html . '
                                                        <div class="ts-overlay" ' . $overlay_style . '>
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $icon_html . '
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/.ts-service-item-->
                                            </div>';
                    break;
                
                
            endswitch;
            
            echo $mechanic_service_html; 
            
            ?>
        
        <?php endif; // End if ( in_array( $service_style, $mechanic_services ) ) ?>
        
        <?php if ( in_array( $service_style, $carpenter_services ) ): // Services html of Carpenter ?>
            <?php
            
            $title_html = '';
            if ( trim( $link_btn['url'] ) != '' ) {
                $title_html = '<h3><a href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $title ) . '" target="' . esc_attr( $link_btn['target'] ) . '">' . sanitize_text_field( $title ) . '</a></h3>';
            }
            else{
                $title_html = '<h3>' . sanitize_text_field( $title ) . '</h3>';
            }
            
            $img_html = trim( $img_src ) != '' ? '<img src="' . esc_url( $img_src ) . '" alt="">' : '';
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '"></span>' : '';
            
            $carpenter_service_html = '';
            
            switch ( $service_style ):
            
                case 'style-16':
                    $carpenter_service_html .= '<div class="' . esc_attr( $service_class ) . '">
                                                    <div class="ts-service-item">
                                                        <div class="ts-inner">
                                                        	<div class="ts-service-img">
                                                                <div class="img-service-overlay">
                                                                    ' . $img_html . '
                                                                </div><!-- /.img-service-overlay -->
                                                            	' . $icon_html . '
                                                            </div>
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                    ' . $read_more_html . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.ts-service-item -->
                                                </div>';
                    break;   
                
            endswitch;
            
            echo $carpenter_service_html; 
            
            ?>
        <?php endif; // End if ( in_array( $service_style, $carpenter_services ) ) ?>
        
        <?php if ( in_array( $service_style, $metal_construction_services ) ): // Services html of Metal Construction ?>
            <?php
            
            $title_html = '';
            if ( trim( $link_btn['url'] ) != '' ) {
                $title_html = '<h3><a href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $title ) . '" target="' . esc_attr( $link_btn['target'] ) . '">' . sanitize_text_field( $title ) . '</a></h3>';
            }
            else{
                $title_html = '<h3>' . sanitize_text_field( $title ) . '</h3>';
            }
            
            $img_html = trim( $img_src ) != '' ? '<img src="' . esc_url( $img_src ) . '" alt="">' : '';
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '"></span>' : '';
            
            $metal_construction_service_html = '';
            
            switch ( $service_style ):
            
                case 'style-17': 
                    $metal_construction_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                            <div class="ts-service-item">
                                                                <div class="ts-inner">
                                                                    ' . $img_html . '
                                                                    <div class="ts-overlay" ' . $overlay_style . '>
                                                                        <div class="ts-overlay-middle">
                                                                            <div class="ts-overlay-content">
                                                                                ' . $icon_html . '
                                                                                ' . $title_html . '
                                                                                <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><!--/.ts-service-item-->
                                                        </div> <!-- /.ts-service -->';
                    break;  
                
                case 'style-17a': case 'style-18':
                    $metal_construction_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                            <div class="ts-service-item">
                                                                <div class="ts-inner">
                                                                    <div class="ts-overlay" ' . $overlay_style . '>
                                                                        <div class="ts-overlay-middle">
                                                                            <div class="ts-overlay-content">
                                                                                ' . $icon_html . '
                                                                                ' . $title_html . '
                                                                                <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div><!--/.ts-service-item-->
                                                        </div> <!-- /.ts-service -->';
                    break;  
                
            endswitch;
            
            echo $metal_construction_service_html; 
            
            ?>
        <?php endif; // End if ( in_array( $service_style, $metal_construction_services ) ) ?>
        
        <?php if ( in_array( $service_style, $mining_services ) || in_array( $service_style, $maintenance_services ) ): // Services html of Mining, Maintenance ?>
            <?php
            
            $title_html = '';
            if ( trim( $link_btn['url'] ) != '' ) {
                $title_html = '<h3><a href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $title ) . '" target="' . esc_attr( $link_btn['target'] ) . '">' . sanitize_text_field( $title ) . '</a></h3>';
            }
            else{
                $title_html = '<h3>' . sanitize_text_field( $title ) . '</h3>';
            }
            
            $img_html = trim( $img_src ) != '' ? '<img src="' . esc_url( $img_src ) . '" alt="">' : '';
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '"></span>' : '';
            
            $mining_service_html = '';
            
            switch ( $service_style ):
            
                case 'style-19': case 'style-20':
                    $mining_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                <div class="ts-service-item">
                                                    <div class="ts-inner">
                                                        ' . $img_html . '
                                                        <div class="ts-overlay" ' . $overlay_style . '>
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $icon_html . '
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/.ts-service-item-->
                                            </div> <!-- /.ts-service -->';
                    break;  
                
                case 'style-21': 
                    $mining_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                <div class="ts-service-item">
                                                    <div class="ts-inner">
                                                        ' . $img_html . '
                                                        <div class="ts-overlay" ' . $overlay_style . '>
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $icon_html . '
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                    ' . $read_more_html . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/.ts-service-item-->
                                            </div> <!-- /.ts-service -->';
                    break;  
                
                case 'style-22': case 'style-23':
                    $mining_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                <div class="ts-service-item">
                                                    <div class="ts-inner">
                                                        <div class="ts-overlay">
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $icon_html . '
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                    ' . $read_more_html . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/.ts-service-item-->
                                            </div> <!-- /.ts-service -->';
                    break; 
                   
                case 'style-26':
                    $mining_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                <div class="ts-service-item">
                                                    <div class="ts-inner">
                                                        ' . $img_html . '
                                                        <div class="ts-overlay">
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $icon_html . '
                                                                    ' . $title_html . '
                                                                    <div class="ts-excerpt">' . strip_tags( apply_filters( 'the_content', $content ) ) . '</div>
                                                                    ' . $read_more_html . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/.ts-service-item-->
                                            </div> <!-- /.ts-service -->';
                    break;   
                
            endswitch;
            
            echo $mining_service_html; 
            
            ?>
        <?php endif; // End if ( in_array( $service_style, $mining_services ) ) ?>
        
        <?php if ( in_array( $service_style, $autoshop_services ) ): // Services html of Autoshop ?>
            <?php
            
            $title_html = '';
            if ( trim( $link_btn['url'] ) != '' ) {
                $title_html = '<h3><a href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $title ) . '" target="' . esc_attr( $link_btn['target'] ) . '">' . sanitize_text_field( $title ) . '</a></h3>';
            }
            else{
                $title_html = '<h3>' . sanitize_text_field( $title ) . '</h3>';
            }
            
            $subtitle_html = '';
            if ( trim( $subtitle ) != '' ) {
                $subtitle_html .= '<span class="sub-title">' . sanitize_text_field( $subtitle ) . '</span>';
            }
            
            $img_html = trim( $img_src ) != '' ? '<img src="' . esc_url( $img_src ) . '" alt="">' : '';
            
            $autoshop_service_html = '';
            
            switch ( $service_style ):
                   
                case 'style-35':
                    $autoshop_service_html .= '<div class="' . esc_attr( $service_class ) . '">             
                                                <div class="ts-service-item">
                                                    <div class="ts-inner">
                                                        ' . $img_html . '
                                                        <div class="ts-overlay">
                                                            <div class="ts-overlay-middle">
                                                                <div class="ts-overlay-content">
                                                                    ' . $title_html . '
                                                                    ' . $subtitle_html . '
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!--/.ts-service-item-->
                                            </div> <!-- /.ts-service -->';
                    break;   
                
            endswitch;
            
            echo $autoshop_service_html; 
            
            ?>
        <?php endif; // End if ( in_array( $service_style, $autoshop_services ) ) ?>
        
        <?php
        return ob_get_clean();
        
    }
    
    function ts_single_feature( $atts, $content = null ) {
        
        extract( shortcode_atts( array(
            'icon'          =>  '',
            'feature_link'  =>  '',
            'css_animation' =>  '',
            'css'           =>  ''
    	), $atts ) );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
            $content = wpb_js_remove_wpautop( $content, true );   
        }
        
        if ( function_exists( 'vc_build_link' ) ):
            $feature_link = vc_build_link( $feature_link );
        else:
            $feature_link = $link_default;
        endif;
        
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        $feature_class = 'item-features ' . $css_animation . ' ' . $css_class;
        
        $feature_title = '';
        if ( trim( $feature_link['title'] ) != '' ) {
            $feature_title .= '<h6>';
            if ( trim( $feature_link['url'] ) != '' ) {
                $feature_title .= '<a href="' . esc_url( $feature_link['url'] ) . '" title="' . esc_attr( $feature_link['title'] ) . '">' . sanitize_text_field( $feature_link['title'] ) . '</a>';
            }
            else{
                $feature_title .= sanitize_text_field( $feature_link['title'] );
            }
            $feature_title .= '</h6>';
        }
        
        
        ob_start();
        ?>
        
        <div class="<?php echo esc_attr( $feature_class ); ?>">
            
            <span class="<?php echo esc_attr( $icon ); ?>"></span>
            <?php echo $feature_title; ?>
            <div>
                <?php echo apply_filters( 'the_content', $content ); ?>
            </div>
                
        </div> <!-- /.item-features -->
        
        <?php
        return ob_get_clean();
        
    }
    
    
    function ts_title_desc( $atts, $content = null ) {
        
        extract( shortcode_atts( array(
            'title_desc_style' => 'plumber',
            'title' => '',
            'title_font_size' => '',
            'subtitle' => '',
            'title_font_weight' => '',
            'subtitle_font_size' => '',
            'subtitle_font_weight' => '',
            'align' => 'inherit',
            'css_animation' => 'no',
            'css' => ''
    	), $atts ) );     
        
        if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
            $content = wpb_js_remove_wpautop( $content, true );   
        }
        
        $align_class = '';
        if ( trim( $align ) != '' ) {
            $align_class = 'align-' . esc_attr( $align );   
        }
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
                
        $title_style = '';
        if ( trim( $title_font_size ) != '' ) {
            $title_font_size = max( 0, intval( $title_font_size ) );
            $title_style .= 'font-size: ' . $title_font_size . 'px;';
        }
        
        if ( trim( $title_font_weight ) != '' ) {
            $title_style .= 'font-weight: ' . strip_tags( $title_font_weight ) . ';';
        }
        
        if ( trim( $title_style ) != '' ) {
            $title_style = 'style="' . $title_style . '"';
        }
        
        $subtitle_style = '';
        if ( trim( $subtitle_font_size ) != '' ) {
            $subtitle_font_size = max( 0, intval( $subtitle_font_size ) );
            $subtitle_style .= 'font-size: ' . $subtitle_font_size . 'px;';
        }
        
        if ( trim( $subtitle_font_weight ) != '' ) {
            $subtitle_style .= 'font-weight: ' . strip_tags( $subtitle_font_weight ) . ';';
        }
        
        if ( trim( $subtitle_style ) != '' ) {
            $subtitle_style = 'style="' . $subtitle_style . '"';
        }
        
        $subtitle_html = '';
        if ( trim( $subtitle ) != '' ):
            $subtitle_html .= '<div class="subtitle" ' . $subtitle_style . '>' . sanitize_text_field( $subtitle ) . '</div>';
        endif;
        
        ob_start();
        ?>
        
        <div class="section-title clearfix title-desc-style-<?php echo esc_attr( $title_desc_style ); ?> <?php echo esc_attr( $align_class . ' ' . $css_animation . ' ' . $css_class ); ?> ">
            <?php if ( trim( $title_desc_style ) == 'gardner' ): ?>
                <h2 <?php echo $title_style; ?>><?php echo sanitize_text_field( $title ); ?></h2>
                <?php echo $subtitle_html; ?>
            <?php else: ?>
                <?php echo $subtitle_html; ?>
                <h2 <?php echo $title_style; ?>><?php echo sanitize_text_field( $title ); ?></h2>
            <?php endif; ?>
            <div class="title-description"><?php echo apply_filters( 'the_content', $content ); ?></div>
        </div>
        
        <?php
        return ob_get_clean();
        
    }
    
    function ts_call_to_action( $atts , $content = null) {
        $html = $css ='';
    
        extract( shortcode_atts( array(
            'link_btn' =>  '',
            'choose_border' =>  'yes',
            'border_color' =>  '',
            'border_radius'  =>  '0',
            'bg_color' => '',
            'button_color' =>  '',
            'button_hover_color' =>  '',
            'button_bg_color' =>  '',
            'button_hover_bg_color' =>  ''
    	), $atts ) );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        if ( function_exists( 'vc_build_link' ) ):
            $link_btn = vc_build_link( $link_btn );
        else:
            $link_btn = $link_default;
        endif;
        
        $border_radius = max( 0, intval( $border_radius ) );
        
        
        $html = '';
        $html_style = '';
        $button_style = '';
        
        if ( $choose_border == 'yes' ) {
            $html_style .= 'border-style: solid; border-width: 1px; '; 
        }
        
        $html_style .= trim( $border_color ) != '' ? 'border-color: ' . $border_color . '; ': '';
        $html_style .= 'border-radius: ' . esc_attr( $border_radius ) . 'px; ';
        $html_style .= trim( $bg_color ) != '' ? 'background-color: ' . $bg_color . '; ': 'background-color: transparent;';
        
        $button_style .= trim( $button_color ) != '' ? 'color: ' . $button_color . '; ': '';
        $button_style .= trim( $button_bg_color ) != '' ? 'background-color: ' . $button_bg_color . '; ': '';
        
        
        $html .= '<div class="home-featured ts-call-to-action clearfix" style="' . $html_style . '">';
        
        if ( trim( $link_btn['url'] ) != '' ) {
            $html .= '<a style="' . $button_style . '" data-color="' . esc_attr( $button_color ) . '" data-bg-color="' . esc_attr( $button_bg_color ) . '" data-hover-color="' . esc_attr( $button_hover_color ) . '" data-hover-bg-color="' . esc_attr( $button_hover_bg_color ) . '" class="btn btn-grey ts-hover-js" href="' . esc_url( $link_btn['url'] ) . '" target="' . esc_attr( $link_btn['target'] ) . '" title="' . esc_attr( $link_btn['title'] ) . '">' . sanitize_text_field( $link_btn['title'] ) . '</a>';
        }
        $html .= apply_filters( 'the_content', $content );
        
        $html .= '</div><!-- /.ts-call-to-action -->';
        
        return $html;
    }
    
    
    function ts_latest_news( $atts ) {
        
        extract( shortcode_atts( array(
            'number_of_posts' => '2',
            'posts_per_row' => '2'
    	), $atts ) );
        
        
        $number_of_posts = max( 1, intval( $number_of_posts ) );
        $posts_per_row = min( $number_of_posts, max( 1, intval( $posts_per_row ) ) );
        
        $args = array(
            'posts_per_page' => $number_of_posts,
            'post_type' => 'post'	 
        );
        
        $posts_query = new WP_Query( $args );
        
        $html = '';
        
        if ( $posts_query->have_posts() ) {
            
            $col_class = '';
            if ( $posts_per_row <= 2 ) {
                $col_class = 'col-xs-6';
            }
            
            if ( $posts_per_row == 3 ) {
                $col_class = 'col-xs-6 col-sm-4';
            }
            
            if ( $posts_per_row == 4 ) {
                $col_class = 'col-xs-6 col-sm-6 col-md-3';
            }
            
            $html .= '<div class="ts-latestnews">';
            $html .= '<div class="row">';
            
            while ( $posts_query->have_posts() ) {
                $posts_query->the_post();
                $post_id = get_the_ID();
                $format = get_post_format( $post_id );
                $icon_class = 'fa fa-file-text';
                
                $thumb_src = '';
                if ( has_post_thumbnail() ) {
                    $thumb_src = theone_get_img_src_by_id( get_post_thumbnail_id(), '536x426' );
                }
                else{
                    $thumb_src = theone_no_image( array( 'width' => 536, 'height' => 426 ), false, true );
                }
                
                switch ( $format ):
                
                    case 'aside':
                        $icon_class = 'fa fa-file-text';
                        break;
                    case 'chat':
                        $icon_class = 'fa fa-comments-o';
                        break;
                    case 'gallery':
                        $icon_class = 'fa fa-image';
                        break;
                    case 'link':
                        $icon_class = 'fa fa-link';
                        break;
                    case 'image':
                        $icon_class = 'fa fa-image';
                        break;
                    case 'quote':
                        $icon_class = 'fa fa-quote-left';
                        break;
                    case 'status':
                        $icon_class = 'fa fa-file-text';
                        break;
                    case 'video':
                        $icon_class = 'fa fa-video-camera';
                        break;
                    case 'audio':
                        $icon_class = 'fa fa-file-audio-o';
                        break;
                    default:
                        $icon_class = 'fa fa-file-text';
                        break;
                
                endswitch;
                
                $excerpt = theone_get_the_excerpt_max_charlength( 85 ); 
                
                $html .= '<div class="' . $col_class . '">';
                $html .= '<div class="item-post">';
                $html .= '<div class="img-post">';
                $html .= '<figure><a href="' . get_permalink() . '"><img src="' . $thumb_src . '" alt="' . esc_attr( get_the_title() ) . '"></a></figure>';
                $html .= '<span class="icon-post-type"><i class="' . $icon_class . '"></i></span>';
                $html .= '</div><!-- /.img-post -->';
                $html .= '<div class="info-post">
							<h5><a href="' . get_permalink() . '">' . get_the_title() . '</a></h5>
							<ul class="meta-post">
								<li>' . __( 'On', 'theone-core' ) . ' : <span class="date"><a href="#">' . get_the_date() . '</a></span></li>
								<li>' . __( 'By', 'theone-core' ) . ' : <span class="author"><a href="' . get_author_posts_url( get_the_author_meta( 'ID' ) ) . '">' . get_the_author_meta( 'display_name' ) . '</a></span></li>
							</ul>
							<div>' . $excerpt . '</div>
						</div><!-- /.info-post -->';
                $html .= '</div><!-- /.item-post -->';
                $html .= '</div><!-- /' . $col_class . ' -->';
                
            }
            
            $html .= '</div><!-- /.row -->';
            $html .= '</div><!-- /.ts-latestnews -->';
            
        }
        
        return $html;
        
    }
    
    
    function theone_testimonial_carousel( $atts ) {
        
        extract( shortcode_atts( array(
            'tes_cat_id'        =>  0,
            'limit'             =>  8,
            'posts_per_slide'   =>  3,
            'max_chars'         =>  100,
            'show_pagination'   =>  'true'
    	), $atts ) );
        
        $limit = max( 1, intval( $limit ) );
        $posts_per_slide = max( 1, intval( $posts_per_slide ) );
        $max_chars = max( 1, intval( $max_chars ) );
        $tes_cat_id = intval( $tes_cat_id );
        
        $html = '';
        $items_html = '';
        
        $query_args = array(
            'post_type'     =>  'testimonial',
            'showposts'     =>  $limit,
            'post_status'   =>  array( 'publish' )
        );
        
        if ( $tes_cat_id > 0 ):
        
            $query_args['tax_query'] = array(
        		array(
        			'taxonomy'   => 'testimonial_cat',
        			'field'      => 'ids',
        			'terms'      => $tes_cat_id
        		)
        	);
            
        endif;
        
        $query_posts = new WP_Query( $query_args );
        if( $query_posts->have_posts() ):
        
            while ( $query_posts->have_posts() ) : $query_posts->the_post(); 
                
                $thumb_src = '';
                if ( has_post_thumbnail() ) {
                    $thumb_src = theone_get_img_src_by_id( get_post_thumbnail_id(), 'thumbnail' );
                }
                else{
                    $thumb_src = theone_no_image( array( 'width' => 150, 'height' => 150 ), false, false );
                }
                
                $excerpt = theone_get_the_excerpt_max_charlength( $max_chars ); 
                $client_pos = get_post_meta( get_the_ID(), 'theone_client_position', true );
                
                $items_html .= '<div class="item-testimonial">
                					<div class="quote-client">
                						<p>' . $excerpt . '</p>
                					</div><!-- /.quote-client -->
                					<div class="info-client">
                						<div class="avata"><img src="' . esc_url( $thumb_src ) . '" alt=""></div>
                						<span class="client-name">' . get_the_title() . '</span>
                						<span class="client-position">' . sanitize_text_field( $client_pos ) . '</span>
                					</div><!-- /.info-client -->
                				</div><!-- /.item-testimonial -->';
                
            endwhile;
            
            $html .= '<div class="ts-testimonial-slide" data-itemslide="' . intval( $posts_per_slide ) . ' " data-pagination="' . esc_attr( $show_pagination ) . '">';
            $html .= $items_html;
            $html .= '</div><!-- /.ts-testimonial-slide -->';
            
        endif;
        
        wp_reset_postdata();
        
        return $html;
        
    }
    
    
    function ts_client_logos_carousel( $atts ) {
        
        extract( shortcode_atts( array(
            'client_cat_id'     =>  0,
            'limit'             =>  8,
            'posts_per_slide'   =>  3,
            'border_per_logo'   =>  'yes'
    	), $atts ) );
        
        $limit = max( 1, intval( $limit ) );
        $posts_per_slide = max( 1, intval( $posts_per_slide ) );
        $client_cat_id = intval( $client_cat_id );
        $slide_class = ( $border_per_logo == 'no' ) ? 'no-border': '';
        $item_class = ( $border_per_logo == 'no' ) ? 'item-no-border': '';
        
        $html = '';
        $items_html = '';
        
        $query_args = array(
            'post_type'     =>  'client',
            'showposts'     =>  $limit,
            'post_status'   =>  array( 'publish' )
        );
        
        if ( $client_cat_id > 0 ):
        
            $query_args['tax_query'] = array(
        		array(
        			'taxonomy'   => 'client_cat',
        			'field'      => 'ids',
        			'terms'      => $client_cat_id
        		)
        	);
            
        endif;
        
        $query_posts = new WP_Query( $query_args );
        if( $query_posts->have_posts() ):
            
            while ( $query_posts->have_posts() ) : $query_posts->the_post(); 
                
                $thumb_src = '';
                if ( has_post_thumbnail() ) {
                    $thumb_src = theone_get_img_src_by_id( get_post_thumbnail_id(), '184x106' );
                }
                else{
                    $thumb_src = theone_no_image( array( 'width' => 184, 'height' => 106 ), false, false );
                }
                
                $link_href = '';
                $link = get_post_meta( get_the_ID(), 'theone_client_link', true );
                $use_permalink = get_post_meta( get_the_ID(), 'theone_use_permalink', true ) == 'on';
                
                if ( $use_permalink ) {
                    $link_href = get_permalink();
                }
                else{
                    if ( trim( $link ) != '' ) {
                        $link_href = esc_url( $link );
                    }   
                }
                
                $items_html .= '<div class="client-item ' . $item_class . '" data-use-permalink="' . esc_attr( $use_permalink ) . '">
                					<figure>';
                
                if ( trim( $link_href ) != '' ) {
                    $items_html .= '<a href="' . $link_href . '" target="__blank">';
                }
                                                    
                $items_html .=          '<img src="' . esc_url( $thumb_src ) . '" alt="' . esc_attr( get_the_title() ) . '" title="' . esc_attr( get_the_title() ) . '" />';
                
                if ( trim( $link_href ) != '' ) {
                    $items_html .= '</a>';
                }
                
                $items_html .=      '</figure>
                				</div>';
                
            endwhile;
            
            $html .= '<div class="ts-client-slide ' . $slide_class . '" data-itemslide="' . intval( $posts_per_slide ) . ' " data-pagination="false">';
            $html .= $items_html;
            $html .= '</div><!-- /.ts-client-slide -->';
            
        endif;
        
        wp_reset_postdata();
        
        return $html;
        
    }
    
    /** Team Members Grid **/
    function ts_members_grid( $atts ) {
        
        extract( shortcode_atts( array(
            'member_cat_id'     =>  0,
            'cur_page'          =>  1,  
            'limit'             =>  8,
            'show_pagination'   =>  'no',
            'members_per_row'   =>  4,
            'max_chars'         =>  100
    	), $atts ) );
        
        $cur_page = max( 1, intval( $cur_page ) );
        $limit = max( 1, intval( $limit ) );
        $members_per_row = max( 1, intval( $members_per_row ) );
        $member_cat_id = intval( $member_cat_id );
        $max_chars = max( 1, intval( $max_chars ) );
        
        $html = '';
        
        $query_args = array(
            'post_type'     =>  'member',
            'showposts'     =>  $limit,
            'post_status'   =>  array( 'publish' )
        );
        
        if ( $member_cat_id > 0 ):
        
            $query_args['tax_query'] = array(
        		array(
        			'taxonomy'   => 'member_cat',
        			'field'      => 'ids',
        			'terms'      => $member_cat_id
        		)
        	);
            
        endif;
        
        $html .= '<div class="ts-team-wrap" data-mem-cat-id="' . intval( $member_cat_id ) . '" data-post-per-page="' . intval( $limit ) . '" data-mem-per-row="' . intval( $members_per_row ) . '" data-max-chars="' . intval( $max_chars ) . '">';
        $html .= theone_team_member_list( $query_args, $cur_page, $show_pagination == 'yes', $members_per_row, $max_chars ); // Locate in funtions.php of theone-core
        $html .= '</div><!-- /.ts-team-wrap -->';
        
        return $html;
        
    }
    
    
    function ts_prices_table( $atts, $content = null ) {
        
        extract( shortcode_atts( array(
            'title'         =>  '',
            'icon'          =>  '',
            'price'         =>  '',
            'unit'          =>  '',
            'unit_position' =>  '',
            'price_time'    =>  '',
            'field_1'       =>  '',
            'field_2'       =>  '',
            'field_3'       =>  '',
            'field_4'       =>  '',
            'field_5'       =>  '',
            'field_6'       =>  '',
            'field_7'       =>  '',
            'field_8'       =>  '',
            'link_btn'      =>  ''
    	), $atts ) );
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        if ( function_exists( 'vc_build_link' ) ):
            $link_btn = vc_build_link( $link_btn );
        else:
            $link_btn = $link_default;
        endif;
        
        
        $html = '';
        $price_top_html = '';
        $price_mid_html = '';
        $price_bottom_html = '';
        
        $price_top_html .= '<div class="pricing-header">';
        
        if ( trim( $icon ) != '' ) {
            $price_top_html .= '<span class="' . esc_attr( $icon ) . '"></span>';
        }
        
        if ( trim( $title ) != '' ) {
            $price_top_html .= '<h3>' . sanitize_text_field( $title ) . '</h3>';
        }
        
        $price_top_html .= '</div><!-- /.pricing-header -->';
        
        if ( trim( $price . $unit . $price_time ) != '' ) {
            
            $price_mid_html .= '<div class="pricing-price-midd">';
            $price_mid_html .= '<div class="pricing-price">';
            
            if ( trim( $unit_position ) == 'before' && trim( $unit ) != '' ) {
                $price_mid_html .= '<span class="unit">' . esc_html( $unit ) . ' </span>';
            }
            
            $price_mid_html .= sanitize_text_field( $price );
            
            if ( trim( $unit_position ) == 'after' && trim( $unit ) != '' ) {
                $price_mid_html .= '<span class="unit">' . esc_html( $unit ) . ' </span>';
            }
            
            $price_mid_html .= '</div><!-- /.pricing-price -->';
            
            if ( trim( $price_time ) != '' ) {
                $price_mid_html .= '<div class="pricing-time">' . sanitize_text_field( $price_time ) . '</div>';
            }
            
            $price_mid_html .= '</div><!-- /.pricing-price-midd -->';
            
        }
        
        if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
            $content = wpb_js_remove_wpautop( $content, true );   
        }
        
        $price_bottom_html .= '<div class="pricing-bottom">';
        
        if ( trim( $content ) != '' ) {
            $price_bottom_html .= strip_tags( apply_filters( 'the_content', $content ) );
        }
        
        $price_fields_html = '';
        for ( $i = 1; $i <= 8; $i++ ) {
            $field_i = 'field_' . $i;
            if ( trim( $$field_i ) != '' ) {
                $price_fields_html .= '<li>' . sanitize_text_field( $$field_i ) . '</li>';
            }
        }
        
        if ( $price_fields_html != '' ) {
            $price_bottom_html .= '<ul>' . $price_fields_html . '</ul>';
        }
        
        if ( trim( $link_btn['title'] ) != '' ) {
            $price_bottom_html .= '<a class="btn black" href="' . esc_url( $link_btn['url'] ) . '" title="' . esc_attr( $link_btn['title'] ) . '" >' . sanitize_text_field( $link_btn['title'] ) . '</a>';
        }
        
        $price_bottom_html .= '</div><!-- /.pricing-bottom -->';
        
        $html .= '<div class="pricing-item">';
        $html .= $price_top_html . $price_mid_html . $price_bottom_html;
        $html .= '</div><!-- /.pricing-item -->';
        
        return $html;
    }
    
    
    function ts_icon_text( $atts ) {
        
        extract( shortcode_atts( array(
            'title'     =>  '',
            'icon'      =>  '',
            'text'      =>  ''
    	), $atts ) );
        
        
        $html = '<div class="icon-text-wrap">';
        
        if ( trim( $title ) != '' ) {
            $html .= '<div class="info-title">' . sanitize_text_field( $title ) . '</div>';
        }
        
        $html .= '<div class="icon-text">';
        
        if ( trim( $icon ) != '' ) {
            $html .= '<i class="' . esc_attr( $icon ) . '"></i>';
        }
        
        $html .= sanitize_text_field( $text );
        
        $html .= '</div><!-- /.icon-text -->';
        
        $html .= '</div><!-- /.icon-text-wrap -->';
        
        return $html;
        
    }
    
    
    function ts_block_quote( $atts, $content = null ) {
        
        extract( shortcode_atts( array(
            'quote_style'   =>  '',
            'author'        =>  '',
            'css_animation' =>  'no',
            'css'           =>  ''
    	), $atts ) );
        
        if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
            $content = wpb_js_remove_wpautop( $content, true );   
        }
        
        if ( trim( $content ) != '' ) {
            $content = strip_tags( apply_filters( 'the_content', $content ) );
        }   
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif;  
        
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        
        $html = '';
        
        $html .= '<div class="blockquotes blockquotes-style-' . esc_attr( $quote_style ) . ' ' . esc_attr( $css_animation ) . ' ' . esc_attr( $css_class ) . '">
                    <span class="quote-content">' . $content . '</span>
                    <span class="quote-author">' . sanitize_text_field( $author ) . '</span>
                </div>';
        
        return $html;
             
    }
    
    function ts_bullets_list( $atts ) {
        
        extract( shortcode_atts( array(
            'list'          =>  '',
            'icon'          =>  '',
            'css_animation' =>  'no',
            'css'           =>  ''
    	), $atts ) );
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        
        $list_args = explode( ',', $list );
        
        $html = '';
        
        if ( !empty( $list_args ) ) {
            
            $html .= '<div class="bullet-list-wrap ' . esc_attr( $css_animation ) . ' ' . esc_attr( $css_class ) . '">';
            $html .= '<ul class="bullet-list list-style">';
            foreach ( $list_args as $list_item ):
            
                $list_item_args = explode( '|', $list_item );
                $item_icon = isset( $list_item_args[0] ) ? $list_item_args[0] : '';
                $item_text = isset( $list_item_args[1] ) ? $list_item_args[1] : '';
                $item_url = isset( $list_item_args[2] ) ? $list_item_args[2] : '';
                
                $has_icon_class = trim( $item_icon ) != '' ? 'has-icon': 'no-icon';
                
                if ( is_numeric( $item_icon ) ) {
                    $item_icon = '<span class="bullet bullet-number">' . $item_icon . '. </span>';
                }
                else {
                    if ( trim( $item_icon ) != '' ) {
                        $item_icon = '<i class="bullet bullet-icon ' . esc_attr( $item_icon ) . '"></i>';
                    }
                }
                
                if ( trim( $item_text ) != '' ) {
                    if ( trim( $item_url ) != '' ) {
                        $html .= '<li class="has-link ' . esc_attr( $has_icon_class ) . '">' . $item_icon .  '<a href="' . esc_url( $item_url ) . '">' . $item_text . '</a></li>';
                    }
                    else{
                        $html .= '<li class="no-link ' . esc_attr( $has_icon_class ) . '">' . $item_icon . $item_text . '</li>';
                    }
                }
            
            endforeach;
            
            $html .= '</ul>';
            $html .= '</div><!-- /.bullet-list-wrap -->';
            
        }
        
        return $html;
        
    }
    
    
    function ts_subscribe_form( $atts ) {
        
        extract( shortcode_atts( array(
            'form_style'        =>  '1',
            'subscribe_text'    =>  '',
            'css_animation'     =>  'no',
            'css'               =>  ''
    	), $atts ) );
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        
        $html = '';
        $shortcode = '[mc4wp_form]';
        
        $html .= '<div class="ts-subscribe-form-section ' . $css_animation . ' ' . $css_class . ' subscribe-form-section-style-' . esc_attr( $form_style ) . '">';
        if ( trim( $subscribe_text ) != '' ) {
            $html .= '<div class="ts-subscribe-text">' . sanitize_text_field( $subscribe_text ) . '</div>';
        }
        $html .= '<div class="ts-subscribe-form">' . do_shortcode( $shortcode ) . '</div>';
        $html .= '</div>';
        
        return $html;
        
    }
    
    function ts_simple_btn( $atts ) {
        
        extract( shortcode_atts( array(
            'btn_style'             =>  'normal',
            'link_btn'              =>  '',
            'align_class'           =>  'initial',
            'additional_classes'    =>  '',
            'css_animation'         =>  'no',
            'css'                   =>  ''
    	), $atts ) );
        
        $html = '';
        
        $link_default = array(
            'url'       =>  '',
            'title'     =>  '',
            'target'    =>  ''
        );
        
        if ( function_exists( 'vc_build_link' ) ):
            $link_btn = vc_build_link( $link_btn );
        else:
            $link_btn = $link_default;
        endif;
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        $css_animation = ( trim( esc_attr( $css_animation ) ) != '' && trim( esc_attr( $css_animation ) ) != 'no' ) ? 'ts-need-animate wpb_animate_when_almost_visible wpb_' . esc_attr( $css_animation ) : '';
        
        if ( trim( $link_btn['title'] ) != '' ) {
            if ( trim( $link_btn['url'] ) != '' ) {
                $html .= '<a href="' . esc_url( $link_btn['target'] ) . '" target="' . esc_attr( $link_btn['target'] ) . '" title="' . esc_attr( $link_btn['title'] ) . '" class="ts-button btn button-' . esc_attr( $btn_style . ' ' . ' button-align-' . $align_class . ' ' . $css_animation . ' ' . $css_class . ' ' . $additional_classes ) . '">' . sanitize_text_field( $link_btn['title'] ) . '</a>';
            }
            else{
                $html .= '<input type="button" checked="ts-button btn button-' . esc_attr( $btn_style . ' ' . $css_animation . ' ' . $css_class . ' ' . $additional_classes ) . '" value="' . sanitize_text_field( $link_btn['title'] ) . '" title="' . esc_attr( $link_btn['title'] ) . '" />';
            }
        }
        
        return $html;
    }
    
    
    function ts_products_carousel( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
        global $woocommerce_loop;
        
        extract( shortcode_atts( array(
            'title'             =>  '',
            'subtitle'          =>  '',
            'product_cat_id'    =>  '0',
            'products_num'      =>  '8',
            'products_criteria' =>  'recent_products',
            'product_per_slide' =>  '4',
            'product_per_item'  =>  '1',
            'pagination'        =>  'yes',
            'css'               =>  ''
    	), $atts ) );
        
        $product_per_slide = max( 0, intval( $product_per_slide ) );
        $products_num = intval( $products_num );
        $product_per_item = max( 1, intval( $product_per_item ) );
        $pagination = $pagination == 'yes' ? 'true': 'false';
        
        //$ex_products_id = explode( ',', $ex_products_id );
        $meta_key = '';
        $orderby = 'date';  // Recent
    
    	$args = array(
    		'post_type' 			=> 'product',
    		'post_status' 			=> 'publish',
            //'post__not_in'          => $ex_products_id,
            'showposts'             => $products_num,
    		'ignore_sticky_posts'   => 1,
    		'meta_query' 			=> array(
    			array(
    				'key' 		=> '_visibility',
    				'value' 	=> array( 'catalog', 'visible' ),
    				'compare' 	=> 'IN'
    			)
    		)
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
        
        
        if ( trim( $products_criteria ) == 'best_selling_products' ) {
            $meta_key = 'total_sales';
            $orderby = 'meta_value_num';
        }
        
        if ( trim( $products_criteria ) == 'featured_products' ) {
            $args['meta_query'][] = array(
				'key' 		=> '_featured',
				'value' 	=> 'yes'
			);
        }
        
        if ( trim( $products_criteria ) == 'sale_products' ) {
            
            // Get products on sale
            $product_ids_on_sale = wc_get_product_ids_on_sale();
            $meta_query   = array();
        	$meta_query[] = WC()->query->visibility_meta_query();
        	$meta_query[] = WC()->query->stock_status_meta_query();
        	$meta_query   = array_filter( $meta_query );
            
            $args['meta_query'] = $meta_query;
            
            $args['post__in'] = $product_ids_on_sale;
            
        }
        
        if ( trim( $meta_key ) != '' ) {
            $args['meta_key'] = $meta_key;
            $args['orderby'] = $orderby;   
        }
        
        
        if ( trim( $products_criteria ) == 'top_rated_products' ) {
            add_filter( 'posts_clauses', 'theone_order_by_rating_post_clauses' );
        
        	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
        
        	remove_filter( 'posts_clauses', 'theone_order_by_rating_post_clauses' );
        }
        else{
            $products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
        }
        
        
        $woocommerce_loop['columns'] = $product_per_slide;
        
        $html = '';
        
        
        $title_html = trim( $title ) != '' ? '<h3 class="products-carousel-title">' . sanitize_text_field( $title ) . '</h3>' : '';
        $subtitle_html = trim( $subtitle ) != '' ? '<span class="products-carousel-subtitle">' . sanitize_text_field( $subtitle ) . '</span>' : '';
        $carousel_html = '';
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        ob_start();
        
        $total_products = $products->post_count;
        $i = 0;
        
        if ( $products->have_posts() ) : ?>
            
            <div class="product-item-wrap">
            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                
                <?php
                    $i++;
                ?>
				<?php wc_get_template_part( 'content', 'product' ); ?>
                
                <?php if ( $i % $product_per_item == 0 && $i < $total_products ): ?>
                    </div><!-- /.product-item-wrap -->
                    <div class="product-item-wrap">
                <?php endif; ?>

			<?php endwhile; // end of the loop. ?>
            </div><!-- /.product-item-wrap -->
    
    	<?php endif;
        
    	wp_reset_postdata();
        
        $carousel_html .= ob_get_clean();
        
        $html .= '<div class="ts-products-carousel-wrap ' . esc_attr( $css_class ) . '">' .  
                    $subtitle_html . $title_html . 
                    '<div data-itemslide="' . intval( $product_per_slide ) . '" data-pagination="' . esc_attr( $pagination ) . '" class="ts-products-carousel">
                        ' . $carousel_html . '
                    </div><!-- /.ts-products-carousel -->' . 
                '</div><!-- /.ts-products-carousel-wrap -->';
        
        return $html;
    }
    
    
    function ts_products_carousel_2( $atts ) {
        
        if ( !class_exists( 'WooCommerce' ) ):
            return false;
        endif;
        
        global $woocommerce_loop;
        
        extract( shortcode_atts( array(
            'title'             =>  '',
            'product_cat_id'    =>  '0',
            'products_num'      =>  '8',
            'products_criteria' =>  'recent_products',
            'show_rating'       =>  'true',
            'product_per_slide' =>  '4',
            'product_per_item'  =>  '1',
            'pagination'        =>  'yes',
            'css'               =>  ''
    	), $atts ) );
        
        $show_rating = $show_rating == 'true';
        
        $product_per_slide = max( 0, intval( $product_per_slide ) );
        $products_num = intval( $products_num );
        $product_per_item = max( 1, intval( $product_per_item ) );
        $pagination = $pagination == 'yes' ? 'true': 'false';
        
        //$ex_products_id = explode( ',', $ex_products_id );
        $meta_key = '';
        $orderby = 'date';  // Recent
    
    	$args = array(
    		'post_type' 			=> 'product',
    		'post_status' 			=> 'publish',
            //'post__not_in'          => $ex_products_id,
            'showposts'             => $products_num,
    		'ignore_sticky_posts'   => 1,
    		'meta_query' 			=> array(
    			array(
    				'key' 		=> '_visibility',
    				'value' 	=> array( 'catalog', 'visible' ),
    				'compare' 	=> 'IN'
    			)
    		)
    	);
        
        $product_cat_id = intval( $product_cat_id );
        if ( $product_cat_id > 0 ):
            
            $args['tax_query'] = array(
                array(
                    'taxonomy'  => 'product_cat',
                    'field'     => 'id',
                    'terms'     => $product_cat_id
                )
            );
            
        endif;
        
        
        if ( trim( $products_criteria ) == 'best_selling_products' ) {
            $meta_key = 'total_sales';
            $orderby = 'meta_value_num';
        }
        
        if ( trim( $products_criteria ) == 'featured_products' ) {
            $args['meta_query'][] = array(
				'key' 		=> '_featured',
				'value' 	=> 'yes'
			);
        }
        
        if ( trim( $products_criteria ) == 'sale_products' ) {
            
            // Get products on sale
            $product_ids_on_sale = wc_get_product_ids_on_sale();
            $meta_query   = array();
        	$meta_query[] = WC()->query->visibility_meta_query();
        	$meta_query[] = WC()->query->stock_status_meta_query();
        	$meta_query   = array_filter( $meta_query );
            
            $args['meta_query'] = $meta_query;
            
            $args['post__in'] = $product_ids_on_sale;
            
        }
        
        if ( trim( $meta_key ) != '' ) {
            $args['meta_key'] = $meta_key;
            $args['orderby'] = $orderby;   
        }
        
        
        if ( trim( $products_criteria ) == 'top_rated_products' ) {
            add_filter( 'posts_clauses', 'theone_order_by_rating_post_clauses' );
        
        	$products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
        
        	remove_filter( 'posts_clauses', 'theone_order_by_rating_post_clauses' );
        }
        else{
            $products = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $args, $atts ) );
        }
        
        
        $woocommerce_loop['columns'] = $product_per_slide;
        
        $html = '';
        
        
        $title_html = trim( $title ) != '' ? '<h3 class="products-carousel-title widget-title">' . sanitize_text_field( $title ) . '</h3>' : '';
        $carousel_html = '';
        
        $css_class = '';
        if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
            $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
        endif; 
        
        ob_start();
        
        $total_products = $products->post_count;
        $i = 0;
        
        if ( $products->have_posts() ) : ?>
            
            <ul class="product-item-wrap product_list_widget">
            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                
                <?php
                    $i++;
                ?>
				<?php wc_get_template( 'content-widget-product.php', array( 'show_rating' => $show_rating ) ); ?>
                
                <?php if ( $i % $product_per_item == 0 && $i < $total_products ): ?>
                    </ul><!-- /.product-item-wrap -->
                    <ul class="product-item-wrap product_list_widget">
                <?php endif; ?>

			<?php endwhile; // end of the loop. ?>
            </ul><!-- /.product-item-wrap -->
    
    	<?php endif;
        
    	wp_reset_postdata();
        
        $carousel_html .= ob_get_clean();
        
        $html .= '<div class="ts-products-carousel-wrap woocommerce">' .  
                    $title_html . 
                    '<div data-itemslide="' . intval( $product_per_slide ) . '" data-pagination="' . esc_attr( $pagination ) . '" class="ts-products-carousel">
                        ' . $carousel_html . '
                    </div><!-- /.ts-products-carousel -->' . 
                '</div><!-- /.ts-products-carousel-wrap -->';
        
        return $html;
    }
    
    
    add_shortcode( 'theone_icon_link_box', 'theone_icon_link_box' );
    add_shortcode( 'theone_sale_products_carousel', 'theone_sale_products_carousel' );
    add_shortcode( 'theone_advs_carousel', 'theone_advs_carousel' );
    add_shortcode( 'theone_product_tabs', 'theone_product_tabs' );
    add_shortcode( 'theone_intro_txt_img', 'theone_intro_txt_img' );
    add_shortcode( 'theone_lastest_blog_carousel', 'theone_lastest_blog_carousel' );
    add_shortcode( 'theone_brand_logos_carousel', 'theone_brand_logos_carousel' );
    add_shortcode( 'theone_img_advs_group', 'theone_img_advs_group' );
    
    add_shortcode( 'ts_prices_table', 'ts_prices_table' );
    add_shortcode( 'theone_team_member', 'theone_team_member' );
    add_shortcode( 'ts_block_quote', 'ts_block_quote' );
    add_shortcode( 'ts_bullets_list', 'ts_bullets_list' );
    add_shortcode( 'ts_subscribe_form', 'ts_subscribe_form' );
    add_shortcode( 'ts_single_service', 'ts_single_service' );
    add_shortcode( 'ts_single_feature', 'ts_single_feature' );
    add_shortcode( 'ts_title_desc', 'ts_title_desc' );
    add_shortcode( 'ts_call_to_action', 'ts_call_to_action' );
    add_shortcode( 'ts_latest_news', 'ts_latest_news' );
    add_shortcode( 'theone_testimonial_carousel', 'theone_testimonial_carousel' );
    add_shortcode( 'ts_client_logos_carousel', 'ts_client_logos_carousel' );
    add_shortcode( 'ts_members_grid', 'ts_members_grid' );
    add_shortcode( 'ts_icon_text', 'ts_icon_text' );
    add_shortcode( 'ts_simple_btn', 'ts_simple_btn' );
    add_shortcode( 'ts_products_carousel', 'ts_products_carousel' );
    add_shortcode( 'ts_products_carousel_2', 'ts_products_carousel_2' );
    
?>