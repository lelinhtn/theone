<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

// -----------------------------------------------------------------------------------------------------------
// ANIMATION EFFECT
// -----------------------------------------------------------------------------------------------------------
global $ts_vc_anim_effects_in, $ts_vc_anim_effects_out, $ts_vc_animate_effects;

$ts_vc_anim_effects_out = array(
    __( '--- No Animation ---', 'theone-core' ) => '',
    __( 'bounceOut', 'theone-core' ) => 'bounceOut',
    __( 'bounceOutDown', 'theone-core' ) => 'bounceOutDown',
    __( 'bounceOutLeft', 'theone-core' ) => 'bounceOutLeft',
    __( 'bounceOutRight', 'theone-core' ) => 'bounceOutRight',
    __( 'bounceOutUp', 'theone-core' ) => 'bounceOutUp',
    __( 'fadeOut', 'theone-core' ) => 'fadeOut',
    __( 'fadeOutDown', 'theone-core' ) => 'fadeOutDown',
    __( 'fadeOutDownBig', 'theone-core' ) => 'fadeOutDownBig',
    __( 'fadeOutLeft', 'theone-core' ) => 'fadeOutLeft',
    __( 'fadeOutLeftBig', 'theone-core' ) => 'fadeOutLeftBig',
    __( 'fadeOutRight', 'theone-core' ) => 'fadeOutRight',
    __( 'fadeOutRightBig', 'theone-core' ) => 'fadeOutRightBig',
    __( 'fadeOutUp', 'theone-core' ) => 'fadeOutUp',
    __( 'fadeOutUpBig', 'theone-core' ) => 'fadeOutUpBig',
    __( 'flipOutX', 'theone-core' ) => 'flipOutX',
    __( 'flipOutY', 'theone-core' ) => 'flipOutY',
    __( 'lightSpeedOut', 'theone-core' ) => 'lightSpeedOut',
    __( 'rotateOut', 'theone-core' ) => 'rotateOut',
    __( 'rotateOutDownLeft', 'theone-core' ) => 'rotateOutDownLeft',
    __( 'rotateOutDownRight', 'theone-core' ) => 'rotateOutDownRight',
    __( 'rotateOutUpLeft', 'theone-core' ) => 'rotateOutUpLeft',
    __( 'rotateOutUpRight', 'theone-core' ) => 'rotateOutUpRight',
    __( 'slideOutUp', 'theone-core' ) => 'slideOutUp',
    __( 'slideOutDown', 'theone-core' ) => 'slideOutDown',
    __( 'slideOutLeft', 'theone-core' ) => 'slideOutLeft',
    __( 'slideOutRight', 'theone-core' ) => 'slideOutRight',
    __( 'zoomOut', 'theone-core' ) => 'zoomOut',
    __( 'zoomOutDown', 'theone-core' ) => 'zoomOutDown',
    __( 'zoomOutLeft', 'theone-core' ) => 'zoomOutLeft',
    __( 'zoomOutRight', 'theone-core' ) => 'zoomOutRight',
    __( 'zoomOutUp', 'theone-core' ) => 'zoomOutUp',
    __( 'rollOut', 'theone-core' ) => 'rollOut',
    __( 'hinge', 'theone-core' ) => 'hinge',
);

$ts_vc_anim_effects_in = array(
    __( '--- No Animation ---', 'theone-core' ) => '',
    __( 'bounce', 'theone-core' ) => 'bounce',
    __( 'flash', 'theone-core' ) => 'flash',
    __( 'pulse', 'theone-core' ) => 'pulse',
    __( 'rubberBand', 'theone-core' ) => 'rubberBand',
    __( 'shake', 'theone-core' ) => 'shake',
    __( 'swing', 'theone-core' ) => 'swing',
    __( 'tada', 'theone-core' ) => 'tada',
    __( 'wobble', 'theone-core' ) => 'wobble',
    __( 'jello', 'theone-core' ) => 'jello',
    __( 'bounceIn', 'theone-core' ) => 'bounceIn',
    __( 'bounceInDown', 'theone-core' ) => 'bounceInDown',
    __( 'bounceInLeft', 'theone-core' ) => 'bounceInLeft',
    __( 'bounceInRight', 'theone-core' ) => 'bounceInRight',
    __( 'bounceInUp', 'theone-core' ) => 'bounceInUp',
    __( 'fadeIn', 'theone-core' ) => 'fadeIn',
    __( 'fadeInDown', 'theone-core' ) => 'fadeInDown',
    __( 'fadeInDownBig', 'theone-core' ) => 'fadeInDownBig',
    __( 'fadeInLeft', 'theone-core' ) => 'fadeInLeft',
    __( 'fadeInLeftBig', 'theone-core' ) => 'fadeInLeftBig',
    __( 'fadeInRight', 'theone-core' ) => 'fadeInRight',
    __( 'fadeInRightBig', 'theone-core' ) => 'fadeInRightBig',
    __( 'fadeInUp', 'theone-core' ) => 'fadeInUp',
    __( 'fadeInUpBig', 'theone-core' ) => 'fadeInUpBig',
    __( 'flip', 'theone-core' ) => 'flip',
    __( 'flipInX', 'theone-core' ) => 'flipInX',
    __( 'flipInY', 'theone-core' ) => 'flipInY',
    __( 'lightSpeedIn', 'theone-core' ) => 'lightSpeedIn',
    __( 'rotateIn', 'theone-core' ) => 'rotateIn',
    __( 'rotateInDownLeft', 'theone-core' ) => 'rotateInDownLeft',
    __( 'rotateInDownRight', 'theone-core' ) => 'rotateInDownRight',
    __( 'rotateInUpLeft', 'theone-core' ) => 'rotateInUpLeft',
    __( 'rotateInUpRight', 'theone-core' ) => 'rotateInUpRight',
    __( 'slideInUp', 'theone-core' ) => 'slideInUp',
    __( 'slideInDown', 'theone-core' ) => 'slideInDown',
    __( 'slideInLeft', 'theone-core' ) => 'slideInLeft',
    __( 'slideInRight', 'theone-core' ) => 'slideInRight',
    __( 'zoomIn', 'theone-core' ) => 'zoomIn',
    __( 'zoomInDown', 'theone-core' ) => 'zoomInDown',
    __( 'zoomInLeft', 'theone-core' ) => 'zoomInLeft',
    __( 'zoomInRight', 'theone-core' ) => 'zoomInRight',
    __( 'zoomInUp', 'theone-core' ) => 'zoomInUp',
    __( 'rollIn', 'theone-core' ) => 'rollIn',
);

$ts_vc_animate_effects = array(
    __( '--- No Animation ---', 'theone-core' ) => '',
    __( 'bounce', 'theone-core' ) => 'bounce',
    __( 'flash', 'theone-core' ) => 'flash',
    __( 'pulse', 'theone-core' ) => 'pulse',
    __( 'rubberBand', 'theone-core' ) => 'rubberBand',
    __( 'shake', 'theone-core' ) => 'shake',
    __( 'swing', 'theone-core' ) => 'swing',
    __( 'tada', 'theone-core' ) => 'tada',
    __( 'wobble', 'theone-core' ) => 'wobble',
    __( 'jello', 'theone-core' ) => 'jello',
    __( 'bounceIn', 'theone-core' ) => 'bounceIn',
    __( 'bounceInDown', 'theone-core' ) => 'bounceInDown',
    __( 'bounceInLeft', 'theone-core' ) => 'bounceInLeft',
    __( 'bounceInRight', 'theone-core' ) => 'bounceInRight',
    __( 'bounceInUp', 'theone-core' ) => 'bounceInUp',
    __( 'bounceOut', 'theone-core' ) => 'bounceOut',
    __( 'bounceOutDown', 'theone-core' ) => 'bounceOutDown',
    __( 'bounceOutLeft', 'theone-core' ) => 'bounceOutLeft',
    __( 'bounceOutRight', 'theone-core' ) => 'bounceOutRight',
    __( 'bounceOutUp', 'theone-core' ) => 'bounceOutUp',
    __( 'fadeIn', 'theone-core' ) => 'fadeIn',
    __( 'fadeInDown', 'theone-core' ) => 'fadeInDown',
    __( 'fadeInDownBig', 'theone-core' ) => 'fadeInDownBig',
    __( 'fadeInLeft', 'theone-core' ) => 'fadeInLeft',
    __( 'fadeInLeftBig', 'theone-core' ) => 'fadeInLeftBig',
    __( 'fadeInRight', 'theone-core' ) => 'fadeInRight',
    __( 'fadeInRightBig', 'theone-core' ) => 'fadeInRightBig',
    __( 'fadeInUp', 'theone-core' ) => 'fadeInUp',
    __( 'fadeInUpBig', 'theone-core' ) => 'fadeInUpBig',
    __( 'fadeOut', 'theone-core' ) => 'fadeOut',
    __( 'fadeOutDown', 'theone-core' ) => 'fadeOutDown',
    __( 'fadeOutDownBig', 'theone-core' ) => 'fadeOutDownBig',
    __( 'fadeOutLeft', 'theone-core' ) => 'fadeOutLeft',
    __( 'fadeOutLeftBig', 'theone-core' ) => 'fadeOutLeftBig',
    __( 'fadeOutRight', 'theone-core' ) => 'fadeOutRight',
    __( 'fadeOutRightBig', 'theone-core' ) => 'fadeOutRightBig',
    __( 'fadeOutUp', 'theone-core' ) => 'fadeOutUp',
    __( 'fadeOutUpBig', 'theone-core' ) => 'fadeOutUpBig',
    __( 'flip', 'theone-core' ) => 'flip',
    __( 'flipInX', 'theone-core' ) => 'flipInX',
    __( 'flipInY', 'theone-core' ) => 'flipInY',
    __( 'flipOutX', 'theone-core' ) => 'flipOutX',
    __( 'flipOutY', 'theone-core' ) => 'flipOutY',
    __( 'lightSpeedIn', 'theone-core' ) => 'lightSpeedIn',
    __( 'lightSpeedOut', 'theone-core' ) => 'lightSpeedOut',
    __( 'rotateIn', 'theone-core' ) => 'rotateIn',
    __( 'rotateInDownLeft', 'theone-core' ) => 'rotateInDownLeft',
    __( 'rotateInDownRight', 'theone-core' ) => 'rotateInDownRight',
    __( 'rotateInUpLeft', 'theone-core' ) => 'rotateInUpLeft',
    __( 'rotateInUpRight', 'theone-core' ) => 'rotateInUpRight',
    __( 'rotateOut', 'theone-core' ) => 'rotateOut',
    __( 'rotateOutDownLeft', 'theone-core' ) => 'rotateOutDownLeft',
    __( 'rotateOutDownRight', 'theone-core' ) => 'rotateOutDownRight',
    __( 'rotateOutUpLeft', 'theone-core' ) => 'rotateOutUpLeft',
    __( 'rotateOutUpRight', 'theone-core' ) => 'rotateOutUpRight',
    __( 'slideInUp', 'theone-core' ) => 'slideInUp',
    __( 'slideInDown', 'theone-core' ) => 'slideInDown',
    __( 'slideInLeft', 'theone-core' ) => 'slideInLeft',
    __( 'slideInRight', 'theone-core' ) => 'slideInRight',
    __( 'slideOutUp', 'theone-core' ) => 'slideOutUp',
    __( 'slideOutDown', 'theone-core' ) => 'slideOutDown',
    __( 'slideOutLeft', 'theone-core' ) => 'slideOutLeft',
    __( 'slideOutRight', 'theone-core' ) => 'slideOutRight',
    __( 'zoomIn', 'theone-core' ) => 'zoomIn',
    __( 'zoomInDown', 'theone-core' ) => 'zoomInDown',
    __( 'zoomInLeft', 'theone-core' ) => 'zoomInLeft',
    __( 'zoomInRight', 'theone-core' ) => 'zoomInRight',
    __( 'zoomInUp', 'theone-core' ) => 'zoomInUp',
    __( 'zoomOut', 'theone-core' ) => 'zoomOut',
    __( 'zoomOutDown', 'theone-core' ) => 'zoomOutDown',
    __( 'zoomOutLeft', 'theone-core' ) => 'zoomOutLeft',
    __( 'zoomOutRight', 'theone-core' ) => 'zoomOutRight',
    __( 'zoomOutUp', 'theone-core' ) => 'zoomOutUp',
    __( 'hinge', 'theone-core' ) => 'hinge',
    __( 'rollIn', 'theone-core' ) => 'rollIn',
    __( 'rollOut', 'theone-core' ) => 'rollOut',
);

// -----------------------------------------------------------------------------------------------------------
// BORDER STYLE SELECT
// -----------------------------------------------------------------------------------------------------------
global $ts_border_styles;
$ts_border_styles = array(
    __( 'Solid', 'theone-core' ) => 'solid',
    __( 'Dotted', 'theone-core' ) => 'dotted',
    __( 'Dashed', 'theone-core' ) => 'dashed',
    __( 'None', 'theone-core' ) => 'none',
    __( 'Hidden', 'theone-core' ) => 'hidden',
    __( 'Double', 'theone-core' ) => 'double',
    __( 'Groove', 'theone-core' ) => 'groove',
    __( 'Ridge', 'theone-core' ) => 'ridge',
    __( 'Inset', 'theone-core' ) => 'inset',
    __( 'Outset', 'theone-core' ) => 'outset'
);


//Icon element
//if( !function_exists( 'ts_icon_element_fonts_enqueue' ) ){ 
//	function ts_icon_element_fonts_enqueue( $font ) {
//		switch ( $font ) {
//			case 'fontawesome':
//				wp_enqueue_style( 'font-awesome' );
//				break;
//			case 'openiconic':
//				wp_enqueue_style( 'vc_openiconic' );
//				break;
//			case 'typicons':
//				wp_enqueue_style( 'vc_typicons' );
//				break;
//			case 'entypo':
//				wp_enqueue_style( 'vc_entypo' );
//				break;
//			case 'linecons':
//				wp_enqueue_style( 'vc_linecons');
//			case 'elegant':
//				wp_enqueue_style( 'ts_elegant');							
//			default:
//				do_action( 'ts_icon_element_fonts_enqueue', $font ); // hook to custom do enqueue style
//		}
//	}
//}