<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


add_action( 'vc_before_init', 'theoneTextBox' );
function theoneTextBox() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Text Box', 'theone-core' ),
            'base'        => 'theone_textbox', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'std'           => __( 'NICE HEADER HERE', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Description', 'theone-core' ),
                    'param_name'    => 'desc',
                    'std'           => __( 'Ultricies nobis. Earum minim? Aperiam ratione officiis fuga, nesciunt? Netus integer, alias mollis ultricies et aperiam sollicitudin! Deserunt. Adipisicing earum nec in? Posuere dui vehicula possimus fusce! Nihil. Neque harum nostrud quam sapiente voluptas volutpat. Aliqua porttitor id nibh ultricies class, hendrerit, pellentesque ipsam atque, officiis aute laoreet, fermentum quidem minus! Reiciendis! Pharetra arcu, urna accusamus placeat ipsa vulputate. senectus convallis', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type' => 'colorpicker',
                    'class' => '',
                    'heading' => __( 'Text color', 'theone-core' ),
                    'param_name' => 'color',
                    'value' => '', 
                    'description' => __( 'Choose text color', 'theone-core' )
                ),
                array(
                    'type'          => 'checkbox',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Border Position', 'theone-core' ),
                    'param_name'    => 'border_position',
                    'value'         => array(
                        __( 'Top', 'theone-core' )      =>  'line-top',
                        __( 'Bottom', 'theone-core' )   =>  'line-bottom',
                        __( 'Left', 'theone-core' )     =>  'line-left',
                        __( 'Right', 'theone-core' )    =>  'line-right',
                    ),
                    'std'           =>  'no',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Border Color', 'theone-core' ),
                    'param_name'    => 'border_color',
                    'std'           => '#aa9d71',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Border Width', 'theone-core' ),
                    'param_name'    => 'border_width',
                    'std'           => '3',
                    'description'   => __( 'Border width unit is pixel (px).', 'theone-core' )
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneLatestPost' );
function theoneLatestPost() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Latest Posts', 'theone-core' ),
            'base'        => 'theone_latest_posts', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Classic Style', 'theone-core' ) => 'style-classic',
                        __( 'Fullwidth Modern', 'theone-core' ) => 'fullwidth-modern',
                        __( 'List Style 1', 'theone-core' ) => 'list-style1',
                        __( 'List Style 2', 'theone-core' ) => 'list-style2',	
                        __( 'List Style Width BG Image (Parallax Effect)', 'theone-core' ) => 'list-style-bgimg',				    
                    ),
                    'std' => 'style-classic',
                    'description' => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'theone_select_cat_field',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Select Category', 'theone-core' ),
                    'param_name'    => 'cat_id',
                    'value'         => '0',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Excerpt Max Chars Length', 'theone-core' ),
                    'param_name'    => 'max_chars',
                    'std'           => '120',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Read More Button Text', 'theone-core' ),
                    'param_name'    => 'read_more_text',
                    'std'           => __( 'LEARN MORE', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'style-classic' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Limit', 'theone-core' ),
                    'param_name'    => 'post_limit',
                    'std'           => '3',
                    'description'   => __( 'Maximum of post will be shown', 'theone-core' )
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Posts Per Row', 'theone-core' ),
                    'param_name'    => 'posts_per_row',
                    'value' => array(
                        __( '2', 'theone-core' ) => '2',
                        __( '3', 'theone-core' ) => '3',			    
                        __( '4', 'theone-core' ) => '4',
                    ),
                    'std'           => '3',
                    'description'   => __( 'Number of posts per row on large screen.', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'style-classic', 'fullwidth-modern' )
    			   	),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Place Hold Image', 'theone-core' ),
                    'param_name' => 'place_hold',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',			    
                    ),
                    'std' => 'yes',
                    'description' => __( 'Using place hold image if the post thumbnail does not exist.', 'theone-core' )
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneSkillsBarChart' );
function theoneSkillsBarChart() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Skills Bar Chart', 'theone-core' ),
            'base'        => 'theone_skills_bar_chart', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'bar_text',
                    'std'           => __( 'Design', 'theone-core' ),
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'bar_value',
                    'std'           => 80,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Bar Color', 'theone-core' ),
                    'param_name'    => 'bar_color',
                    'std'           => '#aa9d71',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Bar Background Color', 'theone-core' ),
                    'param_name'    => 'bar_bg_color',
                    'std'           => '#f3f3f3',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'bar_height',
                    'std'           => 5,
                    'description'   => __( 'Height of skill bar. Default 5px.', 'theone-core' ),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}

add_action( 'vc_before_init', 'theoneSkillsCircleChart' );
function theoneSkillsCircleChart() {
    global $linea_fonts;
    vc_map( 
        array(
            'name'        => __( 'The One Skills Circle Chart', 'theone-core' ),
            'base'        => 'theone_skills_circle_chart', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'std'           => __( 'The Tite', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Number Center', 'theone-core' ) => 'number-center',
                        __( 'Icon Center', 'theone-core' ) => 'icon-center',
                        __( 'Text Center', 'theone-core' ) => 'text-center',			    
                    ),
                    'std' => 'number-center',
                    'description' => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Size', 'theone-core' ),
                    'param_name'    => 'circle_size',
                    'std'           => 194,
                    'description'   => __( 'Circle width and height (pixel).', 'theone-core' ),
                ),
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => false, // default true, display an "EMPTY" icon?
    					'type' => 'linea',
    					'source' => $linea_fonts,					
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'icon-center' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'bar_text',
                    'std'           => __( 'The One', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'text-center' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'bar_value',
                    'std'           => 80,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text Color', 'theone-core' ),
                    'param_name'    => 'text_color',
                    'std'           => '#aa9d71',
                    'description'   => __( 'Color of text or icon or number', 'theone-core' )
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Bar Color', 'theone-core' ),
                    'param_name'    => 'circle_bar_color',
                    'std'           => '#aa9d71',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Bar Background Color', 'theone-core' ),
                    'param_name'    => 'circle_track_color',
                    'std'           => '#f3f3f3',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'line_width',
                    'std'           => 5,
                    'description'   => __( 'Circle width line width. Default 5px.', 'theone-core' ),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}

add_action( 'vc_before_init', 'theoneSkillsCircleDiagram' );
function theoneSkillsCircleDiagram() {
    global $linea_fonts;
    vc_map( 
        array(
            'name'        => __( 'The One Skills Circle Diagram', 'theone-core' ),
            'base'        => 'theone_skills_circle_diagram', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Default Text', 'theone-core' ),
                    'param_name'    => 'default_text',
                    'std'           => __( 'SKILLS', 'theone-core' ),
                    'description'   => __( 'Default text show in the center', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Diagram Dimension', 'theone-core' ),
                    'param_name'    => 'dimension',
                    'std'           => 500,
                    'description'   => __( 'Diagram width and height (pixel).', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text Color', 'theone-core' ),
                    'param_name'    => 'text_color',
                    'std'           => '#fff',
                    'description'   => __( 'Color of text or icon or number', 'theone-core' )
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Center Circle Color', 'theone-core' ),
                    'param_name'    => 'center_circle_color',
                    'std'           => '#27282d',
                    'description'   => __( '', 'theone-core' )
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Number Of Chart Items', 'theone-core' ),
                    'param_name' => 'num_of_charts',
                    'value' => array(
                        __( '1 chart', 'theone-core' ) => 1,
                        __( '2 charts', 'theone-core' ) => 2,
                        __( '3 charts', 'theone-core' ) => 3,
                        __( '4 charts', 'theone-core' ) => 4,
                        __( '5 charts', 'theone-core' ) => 5,
                        __( '6 charts', 'theone-core' ) => 6,			    
                    ),
                    'std' => 5,
                    'description' => __( '', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'chart_item_1_text',
                    'std'           => __( 'JavaScript', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 1', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '1', '2', '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'chart_item_1_value',
                    'std'           => 95,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                    'group'         => __( 'Chart Item 1', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '1', '2', '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Color', 'theone-core' ),
                    'param_name'    => 'chart_item_1_circle_color',
                    'std'           => '#aa9d71',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 1', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '1', '2', '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'chart_item_2_text',
                    'std'           => __( 'CSS3', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 2', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '2', '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'chart_item_2_value',
                    'std'           => 90,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                    'group'         => __( 'Chart Item 2', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '2', '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Color', 'theone-core' ),
                    'param_name'    => 'chart_item_2_circle_color',
                    'std'           => '#f2d6ae',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 2', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '2', '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'chart_item_3_text',
                    'std'           => __( 'HTML5', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 3', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'chart_item_3_value',
                    'std'           => 80,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                    'group'         => __( 'Chart Item 3', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Color', 'theone-core' ),
                    'param_name'    => 'chart_item_3_circle_color',
                    'std'           => '#d2e3cf',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 3', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '3', '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'chart_item_4_text',
                    'std'           => __( 'PHP', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 4', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'chart_item_4_value',
                    'std'           => 53,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                    'group'         => __( 'Chart Item 4', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Color', 'theone-core' ),
                    'param_name'    => 'chart_item_4_circle_color',
                    'std'           => '#968693',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 4', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '4', '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'chart_item_5_text',
                    'std'           => __( 'MySQL', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 5', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'chart_item_5_value',
                    'std'           => 45,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                    'group'         => __( 'Chart Item 5', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Color', 'theone-core' ),
                    'param_name'    => 'chart_item_5_circle_color',
                    'std'           => '#e7c1cc',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 5', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '5', '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'chart_item_6_text',
                    'std'           => __( 'WordPress', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 6', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '6' )
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Value', 'theone-core' ),
                    'param_name'    => 'chart_item_6_value',
                    'std'           => 64,
                    'description'   => __( 'Value percent. Min is 0, max is 100.', 'theone-core' ),
                    'group'         => __( 'Chart Item 6', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '6' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Circle Color', 'theone-core' ),
                    'param_name'    => 'chart_item_6_circle_color',
                    'std'           => '#e7c1cc',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Chart Item 6', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'num_of_charts',
    				    'value' => array( '6' )
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneCallToAction' );
function theoneCallToAction() {
    global $linea_fonts, $ts_border_styles;
    vc_map( 
        array(
            'name'        => __( 'The One Call To Action', 'theone-core' ),
            'base'        => 'theone_cta', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Default', 'theone-core' ) => 'style-default',
                        __( 'Text Align Center', 'theone-core' ) => 'style-center',	    
                    ),
                    'std' => 'style-default',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'std'           => __( 'ARE YOU READY TO TAKE IT TO THE NEXT LEVEL?', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Description', 'theone-core' ),
                    'param_name'    => 'desc',
                    'std'           => __( 'Quaerat auctor excepteur varius debitis vestibulum saepe orci facilis cupidatat', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => true, // default true, display an "EMPTY" icon?
    					'type' => 'linea',
    					'source' => $linea_fonts,					
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' )
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text Color', 'theone-core' ),
                    'param_name'    => 'text_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Text', 'theone-core' ),
                    'param_name'    => 'button_text',
                    'std'           => __( 'Get Started', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Link', 'theone-core' ),
                    'param_name'    => 'button_link',
                    'std'           => '#',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Button Link Target', 'theone-core' ),
                    'param_name' => 'link_target',
                    'value' => array(
                        __( '_self', 'theone-core' ) => '_self',
                        __( '_blank', 'theone-core' ) => '_blank',
                        __( '_parent', 'theone-core' ) => '_parent',
                        __( '_top', 'theone-core' ) => '_top',			    
                    ),
                    'std' => '_self',
                    'description' => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Text Color', 'theone-core' ),
                    'param_name'    => 'btn_text_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Text Hover Color', 'theone-core' ),
                    'param_name'    => 'btn_text_hover_color',
                    'std'           => 'rgb(170, 157, 113)',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Button Border Radius', 'theone-core' ),
                    'param_name' => 'btn_border_radius',
                    'std' => '0',
                    'description' => __( 'Border radius unit is pixel', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Button Border Style', 'theone-core' ),
                    'param_name' => 'btn_border_style',
                    'value' => $ts_border_styles,
                    'std' => 'none',
                    'description' => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Button Border width', 'theone-core' ),
                    'param_name' => 'btn_border_width',
                    'std' => '2',
                    'description' => __( 'Border width unit is pixel', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'btn_border_style',
    				    'value' => array( 'solid', 'dotted', 'dashed', 'hidden', 'double', 'groove', 'ridge', 'inset', 'outset'  )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Border Color', 'theone-core' ),
                    'param_name'    => 'btn_border_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'btn_border_style',
    				    'value' => array( 'solid', 'dotted', 'dashed', 'hidden', 'double', 'groove', 'ridge', 'inset', 'outset'  )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Bg Color', 'theone-core' ),
                    'param_name'    => 'btn_bg_color',
                    'std'           => '',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'btn_border_style',
    				    'value' => array( '', 'none' )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Bg Hover Color', 'theone-core' ),
                    'param_name'    => 'btn_bg_hover_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Button options', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'btn_border_style',
    				    'value' => array( '', 'none' )
    			   	),
                ),              
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                )
            )
        )
    );
}

add_action( 'vc_before_init', 'theoneButton' );
function theoneButton() {
    global $linea_fonts, $ts_border_styles;
    vc_map( 
        array(
            'name'        => __( 'The One Button', 'theone-core' ),
            'base'        => 'theone_button', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Hover Animation Effect', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Default', 'theone-core' ) => 'button-default',
                        __( 'Outline', 'theone-core' ) => 'button-outline',
                        __( '3D', 'theone-core' ) => 'button-3d',	    
                    ),
                    'std' => 'button-default',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'hover_effect',
                    'value' => array(
                        __( 'Default', 'theone-core' ) => '',
                        __( 'Bg Color Fill To Right', 'theone-core' ) => 'button-hover-1',
                        __( 'Emerge', 'theone-core' ) => 'button-hover-2',
                        __( 'Downward + Bg Color Fill To Bottom', 'theone-core' ) => 'button-hover-3',
                        __( 'Oscillate', 'theone-core' ) => 'button-hover-4',
                        __( 'Bg Color Fill To Bottom + Show Icon', 'theone-core' ) => 'button-hover-5',
                        __( 'Oscillate + Show Icon', 'theone-core' ) => 'button-hover-6',
                        __( 'Bg Color Fill To Bottom', 'theone-core' ) => 'button-hover-7',
                        __( 'Small Scale', 'theone-core' ) => 'button-hover-8',
                        __( 'Expand To Right', 'theone-core' ) => 'button-hover-9',
                        __( 'Running Cross', 'theone-core' ) => 'button-hover-10',
                        __( 'Bg Color Vertical Expansion', 'theone-core' ) => 'button-hover-11',
                        __( 'Bg Color Horizontal Expansion', 'theone-core' ) => 'button-hover-12',	    
                    ),
                    'std' => '',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'iconpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Icon', 'theone-core' ),
                    'param_name'    => 'icon',
                    'settings' => array(
    					'emptyIcon' => true, // default true, display an "EMPTY" icon?
    					'type' => 'linea',
    					'source' => $linea_fonts,					
    				),
    				'description' => __( 'Select icon from library.', 'theone-core' )
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Text', 'theone-core' ),
                    'param_name'    => 'button_text',
                    'std'           => __( 'I\'m Button', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Link', 'theone-core' ),
                    'param_name'    => 'button_link',
                    'std'           => '#',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Button Link Target', 'theone-core' ),
                    'param_name' => 'link_target',
                    'value' => array(
                        __( '_self', 'theone-core' ) => '_self',
                        __( '_blank', 'theone-core' ) => '_blank',
                        __( '_parent', 'theone-core' ) => '_parent',
                        __( '_top', 'theone-core' ) => '_top',			    
                    ),
                    'std' => '_self',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Text Color', 'theone-core' ),
                    'param_name'    => 'btn_text_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Text Hover Color', 'theone-core' ),
                    'param_name'    => 'btn_text_hover_color',
                    'std'           => 'rgb(170, 157, 113)',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Button Border Radius', 'theone-core' ),
                    'param_name' => 'btn_border_radius',
                    'std' => '0',
                    'description' => __( 'Border radius unit is pixel', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Button Border Style', 'theone-core' ),
                    'param_name' => 'btn_border_style',
                    'value' => $ts_border_styles,
                    'std' => 'none',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'button-outline'  )
    			   	),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Button Border width', 'theone-core' ),
                    'param_name' => 'btn_border_width',
                    'std' => '2',
                    'description' => __( 'Border width unit is pixel', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'button-outline'  )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Border Color', 'theone-core' ),
                    'param_name'    => 'btn_border_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'button-outline'  )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Bg Color', 'theone-core' ),
                    'param_name'    => 'btn_bg_color',
                    'std'           => '#aa9d71',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Bg Hover Color', 'theone-core' ),
                    'param_name'    => 'btn_bg_hover_color',
                    'std'           => '#000',
                    'description'   => __( '', 'theone-core' ),
                ),  
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Shadow Color', 'theone-core' ),
                    'param_name'    => 'btn_shadow_color',
                    'std'           => '#887d55',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'button-3d' )
    			   	),
                ),   
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button Shadow Hover Color', 'theone-core' ),
                    'param_name'    => 'btn_shadow_hover_color',
                    'std'           => '#96af8a',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'style',
    				    'value' => array( 'button-3d' )
    			   	),
                ),           
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneDropcapText' );
function theoneDropcapText() {
    global $ts_vc_anim_effects_in, $ts_border_styles;
    vc_map( 
        array(
            'name'        => __( 'The One Dropcap Text', 'theone-core' ),
            'base'        => 'theone_dropcap_text', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text', 'theone-core' ),
                    'param_name'    => 'text_desc',
                    'std'           => __( 'periam ratione officiis fuga, nesciunt? Netus integer, alias mollis ultricies et aperiam osuere dui vehicula possimus fusce.Neque harum nostrud quam sapiente voluptas volutpat landitiis pretium nibh nisl taciti nam Blanditiis pretium nibh nisl taciti nam, auctor aut recusandae totam nascetur proident nesciunt nisl illum deserunt temporibus? Nibh quidem, sed felis, commodo aliquam sagittis dui etiam unde possimus, earum voluptatum! Sociosqu conubia. Pulvinar autem? Dui molestiae iste iure, ratione parturient', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Dropcap Font Size', 'theone-core' ),
                    'param_name'    => 'dropcap_font_size',
                    'std'           => '35',
                    'description'   => __( 'Font size unit is pixel.', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Dropcap Color', 'theone-core' ),
                    'param_name'    => 'dropcap_color',
                    'std'           => '#000',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Dropcap Bg Color', 'theone-core' ),
                    'param_name'    => 'dropcap_bg_color',
                    'std'           => '',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Dropcap Border Radius', 'theone-core' ),
                    'param_name' => 'dropcap_border_radius',
                    'std' => '0',
                    'description' => __( 'Border radius unit is pixel', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Dropcap Border Style', 'theone-core' ),
                    'param_name' => 'dropcap_border_style',
                    'value' => $ts_border_styles,
                    'std' => 'none',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Button Border width', 'theone-core' ),
                    'param_name' => 'dropcap_border_width',
                    'std' => '1',
                    'description' => __( 'Border width unit is pixel', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'dropcap_border_style',
    				    'value' => array( 'solid', 'dotted', 'dashed', 'hidden', 'double', 'groove', 'ridge', 'inset', 'outset'  )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Dropcap Border Color', 'theone-core' ),
                    'param_name'    => 'dropcap_border_color',
                    'std'           => '#000',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'dropcap_border_style',
    				    'value' => array( 'solid', 'dotted', 'dashed', 'hidden', 'double', 'groove', 'ridge', 'inset', 'outset'  )
    			   	),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' ),
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneAdvancedGmap' );
function theoneAdvancedGmap() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Advanced Gmap', 'theone-core' ),
            'base'        => 'theone_advanced_gmap', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Map Type', 'theone-core' ),
                    'param_name' => 'map_type',
                    'value' => array(
                        __( 'ROADMAP', 'theone-core' ) => 'ROADMAP',
                        __( 'SATELLITE', 'theone-core' ) => 'SATELLITE',
                        __( 'HYBRID', 'theone-core' ) => 'HYBRID',
                        __( 'TERRAIN', 'theone-core' ) => 'TERRAIN',	    
                    ),
                    'std' => 'ROADMAP',
                    'description' => __( 'ROADMAP displays the default road map view. SATELLITE displays Google Earth satellite images. HYBRID displays a mixture of normal and satellite views. TERRAIN displays a physical map based on terrain information.', 'theone-core' ),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Hue', 'theone-core' ),
                    'param_name'    => 'hue',
                    'std'           => __( '', 'theone-core' ),
                    'description'   => __( 'An RGB hex string. indicates the basic color.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Lightness', 'theone-core' ),
                    'param_name'    => 'lightness',
                    'std'           => '1',
                    'description'   => __( 'A floating point value between -100 and 100. Indicates the percentage change in brightness of the element.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Saturation', 'theone-core' ),
                    'param_name'    => 'saturation',
                    'std'           => '-100',
                    'description'   => __( 'A floating point value between -100 and 100. Indicates the percentage change in intensity of the basic color to apply to the element.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Address', 'theone-core' ),
                    'param_name'    => 'address',
                    'std'           => '',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Longitude', 'theone-core' ),
                    'param_name'    => 'longitude',
                    'std'           => '105.807298',
                    'description'   => __( '1. Open <a href="https://www.google.com/maps" target="_blank">Google Maps</a><br />2. Right-click the place or area on the map.<br />3. Select <b>What\'s here</b>?<br />4. Under the search box, an info card with coordinates will appear.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Latitude', 'theone-core' ),
                    'param_name'    => 'latitude',
                    'std'           => '21.582668',
                    'description'   => __( '1. Open <a href="https://www.google.com/maps" target="_blank">Google Maps</a><br />2. Right-click the place or area on the map.<br />3. Select <b>What\'s here</b>?<br />4. Under the search box, an info card with coordinates will appear.', 'theone-core' ),
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Pin Icon', 'theone-core' ),
                    'param_name'    => 'pin_icon',
                    'std'           => '',
                    'description'   => __( 'If not choose, default pin icon will show.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Zoom', 'theone-core' ),
                    'param_name'    => 'zoom',
                    'std'           => '14',
                    'description'   => __( 'Most roadmap imagery is available from zoom levels 0 to 18.', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Map Height', 'theone-core' ),
                    'param_name'    => 'map_height',
                    'std'           => '375',
                    'description'   => __( 'Map height unit is pixel.', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneImgBox' );
function theoneImgBox() {
    global $ts_vc_anim_effects_in, $ts_border_styles;
    vc_map( 
        array(
            'name'        => __( 'The One Image Box', 'theone-core' ),
            'base'        => 'theone_img_box', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Image width short content', 'theone-core' ) => 'img-short-content',
                        __( 'Image width caption', 'theone-core' ) => 'img-caption',   
                    ),
                    'std' => 'img-short-content',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'attach_image',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Image', 'theone-core' ),
                    'param_name'    => 'img_id',
                    'std'           => '',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Title', 'theone-core' ),
                    'param_name'    => 'title',
                    'std'           => __( 'Hymenaeos Dignissimos', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textarea',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Short Content', 'theone-core' ),
                    'param_name'    => 'short_content',
                    'std'           => __( 'Morbi vulputate arcu mi, sit amet mattis metus viverra a. Interdum et malesuada fames ac ante ipsum primis in faucibus.Morbi vulputate arcu mi, sit amet mattis metus viverra a. Interdum et.Morbi vulputate arcu mi, sit amet mattis metus viverra a. Interdum et malesuada fames', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-short-content' ),
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Short Description', 'theone-core' ),
                    'param_name'    => 'short_desc',
                    'std'           => __( 'Enim illum nonummy', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-caption' ),
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Link', 'theone-core' ),
                    'param_name'    => 'link',
                    'std'           => '#',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show Read More Button', 'theone-core' ),
                    'param_name' => 'show_read_more_btn',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',   
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-short-content' ),
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Read More Text', 'theone-core' ),
                    'param_name'    => 'read_more_text',
                    'std'           => __( 'Learn More', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'show_read_more_btn',
    				    'value' => array( 'yes' ),
    			   	),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Link Target', 'theone-core' ),
                    'param_name' => 'link_target',
                    'value' => array(
                        __( '_self', 'theone-core' ) => '_self',
                        __( '_blank', 'theone-core' ) => '_blank',
                        __( '_parent', 'theone-core' ) => '_parent',
                        __( '_top', 'theone-core' ) => '_top',			    
                    ),
                    'std' => '_self',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'show_read_more_btn',
    				    'value' => array( 'yes' ),
    			   	),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show View Large Image', 'theone-core' ),
                    'param_name' => 'show_view_large_img',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-caption' ),
    			   	),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Caption Inner', 'theone-core' ),
                    'param_name' => 'caption_inner',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-caption' ),
    			   	),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Caption Position', 'theone-core' ),
                    'param_name' => 'caption_position',
                    'value' => array(
                        __( 'Top', 'theone-core' ) => 'top',
                        __( 'Bottom', 'theone-core' ) => 'bottom',		    
                    ),
                    'std' => 'bottom',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'caption_inner',
    				    'value' => array( 'yes' ),
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Text Color', 'theone-core' ),
                    'param_name'    => 'text_color',
                    'std'           => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-short-content' ),
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Background Color', 'theone-core' ),
                    'param_name'    => 'bg_color',
                    'std'           => __( '', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-short-content' ),
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Border Radius', 'theone-core' ),
                    'param_name'    => 'border_radius',
                    'std'           => 0,
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-caption' ),
    			   	),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Border Style', 'theone-core' ),
                    'param_name' => 'border_style',
                    'value' => $ts_border_styles,
                    'std' => 'none',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'img-caption' ),
    			   	),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Border width', 'theone-core' ),
                    'param_name' => 'border_width',
                    'std' => '6',
                    'description' => __( 'Border width unit is pixel', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'border_style',
    				    'value' => array( 'solid', 'dotted', 'dashed', 'hidden', 'double', 'groove', 'ridge', 'inset', 'outset'  )
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Border Color', 'theone-core' ),
                    'param_name'    => 'border_color',
                    'std'           => '#fff',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element' => 'border_style',
    				    'value' => array( 'solid', 'dotted', 'dashed', 'hidden', 'double', 'groove', 'ridge', 'inset', 'outset'  )
    			   	),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );
}


add_action( 'vc_before_init', 'theoneIconBox' );
function theoneIconBox() {
    global $ts_vc_anim_effects_in, $linea_fonts;
    
    $icon_box_styles = array();
    $preview_imgs = array();
    for ( $i = 1; $i <= 10; $i++ ) {
        $icon_box_styles[sprintf( __( 'Style %d', 'theone-core' ), $i )] = 'iconbox-style' . $i;
        $preview_imgs[] = array(
            'type'          => 'theone_icon_box_preview',
            'class'         => '',
            'heading'       => __( 'Preview Example', 'theone-core' ),
            'param_name'    => 'preview_image' . $i,
            'std'           => 'iconbox-style' . $i,
            'description'   => __( '', 'theone-core' ),
            'dependency' => array(
			    'element' => 'style',
			    'value' => array( 'iconbox-style' . $i )
		   	),
        );
    }
    
    
    $options = array(
        'name'        => __( 'The One Icon Box', 'theone-core' ),
        'base'        => 'theone_icon_box', // shortcode
        'class'       => '',
        'category'    => __( 'The One', 'theone-core'),
        'params'      => array(
            array(
                'type' => 'dropdown',
                'heading' => __( 'Style', 'theone-core' ),
                'param_name' => 'style',
                'value' => $icon_box_styles,
                'std' => 'iconbox-style1',
                'description' => __( '', 'theone-core' ),
            )
        )
    );
    
    foreach ( $preview_imgs as $preview_img ) {
        $options['params'][] = $preview_img;
    }
    
    $options['params'][] = array(
        'type'          => 'iconpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Icon', 'theone-core' ),
        'param_name'    => 'icon',
        'settings' => array(
			'emptyIcon' => true, // default true, display an "EMPTY" icon?
			'type' => 'linea',
			'source' => $linea_fonts,					
		),
		'description' => __( 'Select icon from library.', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'textfield',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Title', 'theone-core' ),
        'param_name'    => 'title',
        'std'           => __( 'The One', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'textarea',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Short Description', 'theone-core' ),
        'param_name'    => 'short_desc',
        'std'           => __( 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Icon Color', 'theone-core' ),
        'param_name'    => 'icon_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Icon Hover Color', 'theone-core' ),
        'param_name'    => 'icon_hover_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style2', 'iconbox-style8', 'iconbox-style9' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Icon Bg Color', 'theone-core' ),
        'param_name'    => 'icon_bg_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style2', 'iconbox-style4', 'iconbox-style5', 'iconbox-style6', 'iconbox-style8' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Icon Hover Bg Color', 'theone-core' ),
        'param_name'    => 'icon_hover_bg_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style2', 'iconbox-style8' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Text Color', 'theone-core' ),
        'param_name'    => 'text_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Box Background Color', 'theone-core' ),
        'param_name'    => 'content_box_bg_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style6' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Front Background Color', 'theone-core' ),
        'param_name'    => 'front_bg_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style10' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'colorpicker',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Back Background Color', 'theone-core' ),
        'param_name'    => 'back_bg_color',
        'std'           => __( '', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style10' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'textfield',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Link', 'theone-core' ),
        'param_name'    => 'link',
        'std'           => '#',
        'description'   => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type' => 'dropdown',
        'heading' => __( 'Show Read More Button', 'theone-core' ),
        'param_name' => 'show_read_more_btn',
        'value' => array(
            __( 'Yes', 'theone-core' ) => 'yes',
            __( 'No', 'theone-core' ) => 'no',   
        ),
        'std' => 'yes',
        'description' => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'style',
		    'value' => array( 'iconbox-style3', 'iconbox-style5', 'iconbox-style6', 'iconbox-style7' ),
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'textfield',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Read More Text', 'theone-core' ),
        'param_name'    => 'read_more_text',
        'std'           => __( 'Learn More', 'theone-core' ),
        'description'   => __( '', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'show_read_more_btn',
		    'value' => array( 'yes' ),
	   	),
    );
    
    $options['params'][] = array(
        'type' => 'dropdown',
        'heading' => __( 'Link Target', 'theone-core' ),
        'param_name' => 'link_target',
        'value' => array(
            __( '_self', 'theone-core' ) => '_self',
            __( '_blank', 'theone-core' ) => '_blank',
            __( '_parent', 'theone-core' ) => '_parent',
            __( '_top', 'theone-core' ) => '_top',			    
        ),
        'std' => '_self',
        'description' => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'dropdown',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'CSS Animation', 'theone-core' ),
        'param_name'    => 'css_animation',
        'value'         => $ts_vc_anim_effects_in,
        'std'           => 'fadeInUp',
        'description'   => __( '', 'theone-core' ),
    );
    
    $options['params'][] = array(
        'type'          => 'textfield',
        'holder'        => 'div',
        'class'         => '',
        'heading'       => __( 'Animation Delay', 'theone-core' ),
        'param_name'    => 'animation_delay',
        'std'           => '0.4',
        'description'   => __( 'Delay unit is second.', 'theone-core' ),
        'dependency' => array(
		    'element'   => 'css_animation',
		    'not_empty' => true,
	   	),
    );
    
    $options['params'][] = array(
        'type'          => 'css_editor',
        'heading'       => __( 'Css', 'theone-core' ),
        'param_name'    => 'css',
        'group'         => __( 'Design options', 'theone-core' ),
    );
    
    vc_map( $options );
    
}


add_action( 'vc_before_init', 'theonePortfolioCarousel' );
function theonePortfolioCarousel() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Portfolio Carousel', 'theone-core' ),
            'base'        => 'theone_portfolio_carousel', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Text inner image', 'theone-core' ) => 'ts-carousel-modern',
                        __( 'Text outer image', 'theone-core' ) => 'ts-carousel-mini',		    
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'theone_select_portfolio_cat_field',
                    'heading' => __( 'Select Category', 'theone-core' ),
                    'param_name' => 'portfolio_cat_id',
                    'std' => 0,
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number Of Items', 'theone-core' ),
                    'param_name'    => 'limit',
                    'std'           => 8,
                    'description'   => __( 'Maximum number of items load', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number Of Items', 'theone-core' ),
                    'param_name'    => 'items_per_slide',
                    'std'           => 4,
                    'description'   => __( 'Number of items to display on large screen', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show Link Icon', 'theone-core' ),
                    'param_name' => 'show_link_icon',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show View Large Image Icon', 'theone-core' ),
                    'param_name' => 'show_view_large_icon',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',
                        __( 'No', 'theone-core' ) => 'no',		    
                    ),
                    'std' => 'yes',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show Nav Control', 'theone-core' ),
                    'param_name' => 'show_navcontrol',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'true',
                        __( 'No', 'theone-core' ) => 'false',		    
                    ),
                    'std' => 'true',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Show Dots Pagination', 'theone-core' ),
                    'param_name' => 'show_dots',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'true',
                        __( 'No', 'theone-core' ) => 'false',		    
                    ),
                    'std' => 'false',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Autoplay', 'theone-core' ),
                    'param_name' => 'autoplay',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'true',
                        __( 'No', 'theone-core' ) => 'false',		    
                    ),
                    'std' => 'false',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Autoplay Timeout', 'theone-core' ),
                    'param_name' => 'autoplay_timeout',
                    'std' => 4000,
                    'description' => __( 'In milliseconds', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Right To Left', 'theone-core' ),
                    'param_name' => 'rtl',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'true',
                        __( 'No', 'theone-core' ) => 'false',		    
                    ),
                    'std' => 'false',
                    'description' => __( 'Slide goes from right to left', 'theone-core' ),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );   
}


add_action( 'vc_before_init', 'theoneClientsCarousel' );
function theoneClientsCarousel() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Clients Carousel', 'theone-core' ),
            'base'        => 'theone_clients_carousel', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Classic', 'theone-core' ) => 'client-classic',	
                        __( 'Border Items', 'theone-core' ) => 'client-boxed',	    
                    ),
                    'std' => 'client-classic',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'theone_select_client_cat_field',
                    'heading' => __( 'Select Category', 'theone-core' ),
                    'param_name' => 'client_cat_id',
                    'std' => 0,
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number Of Items', 'theone-core' ),
                    'param_name'    => 'limit',
                    'std'           => 8,
                    'description'   => __( 'Maximum number of items load', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Items per slide', 'theone-core' ),
                    'param_name'    => 'items_per_slide',
                    'std'           => 5,
                    'description'   => __( 'Number of items to display on large screen', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Use secondary logo', 'theone-core' ),
                    'param_name' => 'use_secondary_logo',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',	
                        __( 'No', 'theone-core' ) => 'no',	  
                    ),
                    'std' => 'no',
                    'description' => __( 'If <b>Yes</b>, the secondary logo will be used. If not found, use primary logo.', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Autoplay', 'theone-core' ),
                    'param_name' => 'autoplay',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'true',
                        __( 'No', 'theone-core' ) => 'false',		    
                    ),
                    'std' => 'true',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Autoplay Timeout', 'theone-core' ),
                    'param_name' => 'autoplay_timeout',
                    'std' => 4000,
                    'description' => __( 'In milliseconds', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Right To Left', 'theone-core' ),
                    'param_name' => 'rtl',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'true',
                        __( 'No', 'theone-core' ) => 'false',		    
                    ),
                    'std' => 'false',
                    'description' => __( 'Slide goes from right to left', 'theone-core' ),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );   
}


add_action( 'vc_before_init', 'theoneClientsGrid' );
function theoneClientsGrid() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Clients Grid', 'theone-core' ),
            'base'        => 'theone_clients_grid', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Grid classic', 'theone-core' ) => 'client-classic',	
                        __( 'Grid box items', 'theone-core' ) => 'client-boxed',	 
                        __( 'Grid opening', 'theone-core' ) => 'client-grid',	
                        __( 'Grid with thumbnail', 'theone-core' ) => 'client-imagethumbs',	   
                    ),
                    'std' => 'client-grid',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'theone_select_client_cat_field',
                    'heading' => __( 'Select Category', 'theone-core' ),
                    'param_name' => 'client_cat_id',
                    'std' => 0,
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number Of Items', 'theone-core' ),
                    'param_name'    => 'limit',
                    'std'           => 8,
                    'description'   => __( 'Maximum number of items load', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number Of Items', 'theone-core' ),
                    'param_name'    => 'items_per_row',
                    'std'           => 4,
                    'description'   => __( 'Number of items to display on large screen. Maximum 6 items per row.', 'theone-core' ),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Use secondary logo', 'theone-core' ),
                    'param_name' => 'use_secondary_logo',
                    'value' => array(
                        __( 'Yes', 'theone-core' ) => 'yes',	
                        __( 'No', 'theone-core' ) => 'no',	  
                    ),
                    'std' => 'no',
                    'description' => __( 'If <b>Yes</b>, the secondary logo will be used. If not found, use primary logo.', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Show follow previous item', 'theone-core' ),
                    'param_name'    => 'follow_delay',
                    'std'           => '0.2',
                    'description'   => __( 'Time delay to show follow previous item. Time unit is second', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );   
}


add_action( 'vc_before_init', 'theoneAnimatedColumn' );
function theoneAnimatedColumn() {
    global $ts_vc_anim_effects_in;
    vc_map( 
        array(
            'name'        => __( 'The One Animated Column', 'theone-core' ),
            'base'        => 'theone_animated_column', // shortcode
            'class'       => '',
            'category'    => __( 'The One', 'theone-core'),
            'params'      => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Style', 'theone-core' ),
                    'param_name' => 'style',
                    'value' => array(
                        __( 'Style 1', 'theone-core' ) => 'multiple-column',	
                        __( 'Style 2', 'theone-core' ) => 'colorful-animate',	 
                        __( 'Style 3', 'theone-core' ) => 'image-background',	
                        __( 'Style 4', 'theone-core' ) => 'simple-icon',	   
                    ),
                    'std' => 'multiple-column',
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type' => 'theone_animated_column_preview',
                    'heading' => __( 'Preview Example', 'theone-core' ),
                    'param_name' => 'preview_style_1',
                    'std' => 'multiple-column',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'multiple-column' ),
    			   	),
                ),
                array(
                    'type' => 'theone_animated_column_preview',
                    'heading' => __( 'Preview Example', 'theone-core' ),
                    'param_name' => 'preview_style_2',
                    'std' => 'colorful-animate',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'colorful-animate' ),
    			   	),
                ),
                array(
                    'type' => 'theone_animated_column_preview',
                    'heading' => __( 'Preview Example', 'theone-core' ),
                    'param_name' => 'preview_style_3',
                    'std' => 'image-background',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'image-background' ),
    			   	),
                ),
                array(
                    'type' => 'theone_animated_column_preview',
                    'heading' => __( 'Preview Example', 'theone-core' ),
                    'param_name' => 'preview_style_4',
                    'std' => 'simple-icon',
                    'description' => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'simple-icon' ),
    			   	),
                ),
                array(
                    'type' => 'theone_select_animated_column_cat_field',
                    'heading' => __( 'Select Category', 'theone-core' ),
                    'param_name' => 'animated_column_cat_id',
                    'std' => 0,
                    'description' => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Read More Text', 'theone-core' ),
                    'param_name'    => 'readmore_text',
                    'std'           => __( 'Learn More', 'theone-core' ),
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value' => array( 'multiple-column', 'colorful-animate', 'image-background' ),
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Number Of Items', 'theone-core' ),
                    'param_name'    => 'limit',
                    'std'           => 8,
                    'description'   => __( 'Maximum number of items load', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Items per row', 'theone-core' ),
                    'param_name'    => 'items_per_row',
                    'std'           => 4,
                    'description'   => __( 'Number of items to display on large screen. Maximum 6 items per row.', 'theone-core' ),
                ),
                /*
                array(
                    'type'          => 'colorpicker',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Hover background color', 'theone-core' ),
                    'param_name'    => 'hover_bg_color',
                    'std'           => '#879999',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value'     => array( 'multiple-column' ),
    			   	),
                ),
                */
                array(
                    'type'          => 'colorpicker',
                    'class'         => '',
                    'heading'       => __( 'Odd overlay color', 'theone-core' ),
                    'param_name'    => 'odd_overlay_color',
                    'std'           => 'rgba(170,153,113,0.8)',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value'     => array( 'image-background' ),
    			   	),
                ),
                array(
                    'type'          => 'colorpicker',
                    'class'         => '',
                    'heading'       => __( 'Even overlay color', 'theone-core' ),
                    'param_name'    => 'even_overlay_color',
                    'std'           => 'rgba(0,0,0,0.7)',
                    'description'   => __( '', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'style',
    				    'value'     => array( 'image-background' ),
    			   	),
                ),
                array(
                    'type'          => 'theone_quick_edit_animated_columns_field',
                    'class'         => '',
                    'heading'       => __( 'Quick edit items', 'theone-core' ),
                    'param_name'    => 'quick_edit_items', // JSON format, cat_id and limit
                    'std'           => '',
                    'group'         => __( 'Quick Edit Items', 'theone-core' ),
                ),
                array(
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'CSS Animation', 'theone-core' ),
                    'param_name'    => 'css_animation',
                    'value'         => $ts_vc_anim_effects_in,
                    'std'           => 'fadeInUp',
                    'description'   => __( '', 'theone-core' ),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Animation Delay', 'theone-core' ),
                    'param_name'    => 'animation_delay',
                    'std'           => '0.4',
                    'description'   => __( 'Delay unit is second.', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Show follow previous item', 'theone-core' ),
                    'param_name'    => 'follow_delay',
                    'std'           => '0.2',
                    'description'   => __( 'Time delay to show follow previous item. Time unit is second', 'theone-core' ),
                    'dependency' => array(
    				    'element'   => 'css_animation',
    				    'not_empty' => true,
    			   	),
                ),
                array(
                    'type'          => 'css_editor',
                    'heading'       => __( 'Css', 'theone-core' ),
                    'param_name'    => 'css',
                    'group'         => __( 'Design options', 'theone-core' ),
                ),
            )
        )
    );   
}




