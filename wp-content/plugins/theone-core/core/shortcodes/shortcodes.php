<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


function theone_textbox( $atts ) {
    
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_textbox', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'title'             =>  '',
        'desc'              =>  '',
        'color'             =>  '',
        'border_position'   =>  '',
        'border_color'      =>  '',
        'border_width'      =>  '',
        'css_animation'     =>  '',
        'animation_delay'   =>  '0.4', // In second
        'css'               =>  '',
	), $atts ) );
    
    $css_class = '';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;  
    
    if ( !is_numeric( $animation_delay ) ) {
        $animation_delay = '0';
    }
    $animation_delay = $animation_delay . 's';
    
    $border_width = max( 0, intval( $border_width ) ) . 'px';
    $border_pos_style = '';
    if ( trim( $border_position ) != '' ) {
        $border_pos_args = explode( ',', $border_position );
        
        if ( !empty( $border_pos_args ) ) {
            foreach ( $border_pos_args as $border_pos ) {
                $pos = str_replace( 'line-', '', $border_pos );
                $border_pos_style .= ' border-' . esc_attr( $pos ) . '-color: ' . $border_color . ' !important; border-' . esc_attr( $pos ) . '-width: ' . esc_attr( $border_width ) . ' !important;';
            }   
        }
        
        $border_position = implode( ' ', explode( ',', $border_position ) );
    }
    
    $html = '';
    
    $html .= '<div class="ts-textbox wow ' . esc_attr( $css_class . ' ' . $border_position . ' ' . $css_animation ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" style="color: ' . esc_attr( $color ) . '; ' . $border_pos_style . '">';
    if ( trim( $title ) != '' ) {
        $html .= '<h4 style="color: ' . esc_attr( $color ) . ';">' . sanitize_text_field( $title ) . '</h4>';
    }
    if ( trim( $desc ) != '' ) {
        $html .= '<p>' . sanitize_text_field( $desc ) . '</p>';
    }
	$html .= '</div><!-- /.ts-textbox -->';
    
    return $html;
}

function theone_latest_posts( $atts ) {
    
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_latest_posts', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'style'             =>  '',
        'cat_id'            =>  '',
        'max_chars'         =>  '',
        'read_more_text'    =>  '',
        'post_limit'        =>  '120',
        'posts_per_row'     =>  '',
        'place_hold'        =>  'yes',
        'css_animation'     =>  '',
        'animation_delay'   =>  '0.4', // In second
        'css'               =>  '',
	), $atts ) );
    
    $css_class = '';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;  
    
    if ( !is_numeric( $animation_delay ) ) {
        $animation_delay = 0;
    }
    $animation_delay_num = $animation_delay;
    
    $max_chars = intval( $max_chars );
    $post_limit = intval( $post_limit );
    $posts_per_row = min( max( 2, intval( $posts_per_row ) ), 4 );
    $place_hold = trim( $place_hold ) == 'yes';
    
    $post_item_class = 'item-post ' . $css_animation . ' ';
    switch ( $posts_per_row ):
        
        case 2:
            $post_item_class .= ' col-sm-6';
            break;
        
        case 3:
            $post_item_class .= ' col-sm-4';
            break;
        
        case 4:
            $post_item_class .= ' col-sm-6 col-md-3';
            break;
        
    endswitch;
    
    $html = '';
    $html_loop_items = '';
    $html_inner_before = '';
    $html_inner_after = '';
    
    $query_args = array(
        'showposts'     =>  $post_limit,
        'post_status'   =>  array( 'publish' )
    );
    
    if ( $cat_id > 0 ):
    
        $query_args['tax_query'] = array(
    		array(
    			'taxonomy'   => 'category',
    			'field'      => 'ids',
    			'terms'      => $cat_id
    		)
    	);
        
    endif;
    
    $query_posts = new WP_Query( $query_args );
    if ( $query_posts->have_posts() ):
    
        $html_inner_before =   '<div class="ts-lastest-posts ' . esc_attr( $style . ' ' . $css_class ) . '">
                                    <div class="row">';
        $html_inner_after =        '</div><!-- /.row -->
                                </div><!-- /.ts-lastest-posts .' . esc_attr( $style . ' ' . $css_class ) . ' -->';
        
        if ( in_array( $style, array( 'list-style-bgimg' ) ) ) {
            $html_inner_before = '<div class="ts-lastest-posts ' . esc_attr( $style . ' ' . $css_class ) . '">';
            $html_inner_after = '</div><!-- /.ts-lastest-posts .' . esc_attr( $style . ' ' . $css_class ) . ' -->';
        }
        
        $total_posts = $query_posts->post_count;
        $i = 0;
        
        while ( $query_posts->have_posts() ) : $query_posts->the_post(); 
            
            $i++;
            $animation_delay_num += 0.2;
            
            switch ( $style ):
            
                case 'style-classic':
                    
                    $thumb = array(
        				'url' => '',
        				'width' => 0,
        				'height' => 0
        			);
                    
                    if ( $place_hold ) {
                        $thumb = theone_resize_image( null, null, 720, 462, true, $place_hold );
                    }
                    
                    if ( has_post_thumbnail() ):
                        
                        $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 720, 462, true, $place_hold );
                        
                    endif;
                    
                    $img_post_html = '';
                    if ( trim( $thumb['url'] ) != '' ) {
                        $img_post_html .= '<div class="img-post">
                								<figure><img src="' . esc_url( $thumb['url'] ) . '" alt=""></figure>
                								<div class="post-hover">
                									<a href="' . get_permalink() . '"><span class="icon icon-arrows-plus"></span></a>
                								</div>
                							</div>';
                    }
                    
                    $comments_count = wp_count_comments( get_the_ID() );
                    
                    $excerpt_text = trim( get_the_content() ) != '' ? '<p>' . theone_get_the_excerpt_max_charlength( 250 ) . '</p>' : '';
                    $read_more_html = trim( $read_more_text ) != '' ? '<a class="ts-readmore" href="' . get_the_permalink() . '">' . sanitize_text_field( $read_more_text ) . '</a>' : '';
                    
                    $html_loop_items .= '<div class="' . esc_attr( $post_item_class ) . ' wow" data-wow-delay="' . esc_attr( $animation_delay_num ) . 's" >
                							' . $img_post_html . '
                							<div class="info-post">
                								<div class="meta-post">
                									<span class="date-post">' . get_the_date() . '</span>
                									<a href="' . get_comments_link() . '" class="comment-post">' . $comments_count->approved . '<i class="fa fa-comment-o"></i></a>
                								</div>
                								<h6><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h6>
                								' . $excerpt_text . '
                								' . $read_more_html . '
                							</div><!-- /.info-post -->
                						</div><!-- /.' . esc_attr( $post_item_class ) . ' -->';
                      
                    
                    switch ( $posts_per_row ):
                        
                        case 2:
                            if ( $i % $posts_per_row == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>';
                            endif;
                            break;
                        
                        case 3:
                            if ( $i % $posts_per_row == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>';
                            endif;
                            break;
                        
                        case 4:
                            if ( $i % $posts_per_row == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-md-block visible-lg-block"></div>';
                            endif;
                            if ( $i % 2 == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-sm-block"></div>';
                            endif;
                            break;
                        
                    endswitch;
                    
                    break;
                
                case 'fullwidth-modern':
                    $thumb = array(
        				'url' => '',
        				'width' => 0,
        				'height' => 0
        			);
                    
                    if ( $place_hold ) {
                        $thumb = theone_resize_image( null, null, 479, 374, true, $place_hold );
                    }
                    
                    if ( has_post_thumbnail() ):
                        
                        $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 479, 374, true, $place_hold );
                        
                    endif;
                    
                    $img_post_html = '';
                    if ( trim( $thumb['url'] ) != '' ) {
                        $img_post_html .= '<div class="img-post">
                								<figure><img src="' . esc_url( $thumb['url'] ) . '" alt=""></figure>
                							</div>';
                    }
                    
                    $comments_count = wp_count_comments( get_the_ID() );
                    
                    //$excerpt_text = trim( get_the_content() ) != '' ? '<p>' . theone_get_the_excerpt_max_charlength( 250 ) . '</p>' : '';
                    $read_more_html = trim( $read_more_text ) != '' ? '<a class="ts-linkreamore" href="' . get_the_permalink() . '"><span class="icon icon-arrows-slide-right2"></span></a>' : '';
                    
                    $html_loop_items .= '<div class="' . esc_attr( $post_item_class ) . ' wow" data-wow-delay="' . esc_attr( $animation_delay_num ) . 's" >
                                            ' . $img_post_html . '
                                            <div class="info-post">
                    							<h6><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h6>
                    							<div class="meta-post">
                    								<span class="date-post">' . get_the_date() . '</span>
                    								<a href="' . get_comments_link() . '" class="comment-post">' . $comments_count->approved . '<i class="fa fa-comment-o"></i></a>
                    							</div>
                    							' . $read_more_html . '
                    						</div><!-- /.info-post -->
                                        </div><!-- /.item-post -->';
                   
                    switch ( $posts_per_row ):
                        
                        case 2:
                            if ( $i % $posts_per_row == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>';
                            endif;
                            break;
                        
                        case 3:
                            if ( $i % $posts_per_row == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-sm-block visible-md-block visible-lg-block"></div>';
                            endif;
                            break;
                        
                        case 4:
                            if ( $i % $posts_per_row == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-md-block visible-lg-block"></div>';
                            endif;
                            if ( $i % 2 == 0 && $i < $total_posts ):
                                $html_loop_items .= '<div class="clearfix visible-sm-block"></div>';
                            endif;
                            break;
                        
                    endswitch;
                    
                    break;
                
                case 'list-style1':
                    $thumb = array(
        				'url' => '',
        				'width' => 0,
        				'height' => 0
        			);
                    
                    if ( $place_hold ) {
                        $thumb = theone_resize_image( null, null, 526, 334, true, $place_hold );
                    }
                    
                    if ( has_post_thumbnail() ):
                        
                        $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 526, 334, true, $place_hold );
                        
                    endif;
                    
                    $comments_count = wp_count_comments( get_the_ID() );
                    
                    $excerpt_text = trim( get_the_content() ) != '' ? '<p>' . theone_get_the_excerpt_max_charlength( 250 ) . '</p>' : '';
                    
                    if ( $i % 2 == 0 ) { // even
                        $read_more_html = trim( $read_more_text ) != '' ? '<a class="ts-linkreamore" href="' . get_the_permalink() . '"><span class="icon icon-arrows-slim-left"></span></a>' : '';
                    }
                    else{ // odd
                        $read_more_html = trim( $read_more_text ) != '' ? '<a class="ts-linkreamore" href="' . get_the_permalink() . '"><span class="icon icon-arrows-slim-right"></span></a>' : '';   
                    }
                    
                    $img_post_html = '';
                    if ( trim( $thumb['url'] ) != '' ) {
                        $img_post_html .= '<div class="img-post">
                								<figure><img src="' . esc_url( $thumb['url'] ) . '" alt=""></figure>
                                                ' . $read_more_html . '
                							</div>';
                    }
                    
                    $html_loop_items .= '<div class="item-post ' . esc_attr( $css_animation ) . ' wow" data-wow-delay="' . esc_attr( $animation_delay_num ) . 's" >
                                            ' . $img_post_html . '
                                            <div class="info-post">
                    							<h6><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h6>
                    							<div class="meta-post">
                    								<span class="date-post">' . get_the_date() . '</span>
                        							<a href="' . get_comments_link() . '" class="comment-post">' . $comments_count->approved . '<i class="fa fa-comment-o"></i></a>
                    							</div>
                    							' . $excerpt_text . '
                    						</div><!-- /.info-post -->
                                        </div><!-- /item-post -->';
                    
                    break;
                
                case 'list-style2':
                    $comments_count = wp_count_comments( get_the_ID() );
                    
                    $cat_list_html = theone_get_the_category_list();
                    
                    $html_loop_items .= '<div class="item-post ' . esc_attr( $css_animation ) . ' wow" data-wow-delay="' . esc_attr( $animation_delay_num ) . 's" >
                        					<div class="container">
                        						<div class="info-post">
                        							' . $cat_list_html . '
                        							<h6><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h6>
                        							<div class="meta-post">
                        							     <span class="date-post">' . get_the_date() . '</span>
                        							     <a href="' . get_comments_link() . '" class="comment-post">' . $comments_count->approved . '<i class="fa fa-comment-o"></i></a>
                        							</div>
                        						</div>
                        					</div>
                        				</div>';
                    
                    break;
                
                case 'list-style-bgimg':
                    
                    $thumb = array(
        				'url' => '',
        				'width' => 0,
        				'height' => 0
        			);
                    
                    if ( $place_hold ) {
                        $thumb = theone_resize_image( null, null, 1900, 1488, true, $place_hold );
                    }
                    
                    if ( has_post_thumbnail() ):
                        
                        $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 1900, 1488, true, $place_hold );
                        
                    endif;
                    
                    $style_background_img = ( trim( $thumb['url'] ) != '' ) ? 'style="background-image: url(\'' . esc_url( $thumb['url'] ) . '\');"' : 'style="background-image: none;"';
                    
                    $comments_count = wp_count_comments( get_the_ID() );
                    $cat_list_html = theone_get_the_category_list();
                    
                    $icon_class = 'icon-basic-sheet-txt';
                    $post_format = get_post_format();
                    
                    switch ( $post_format ):
                        
                        case 'aside':
                            $icon_class = 'icon-basic-sheet-txt';
                            break;
                        
                        case 'image': 
                            $icon_class = 'icon-basic-picture';
                            break;
                        
                        case 'gallery':
                            $icon_class = 'icon-basic-picture-multiple'; 
                            break;
                        
                        case 'quote':
                            break;
                        
                        case 'status':
                            break;
                        
                        case 'video':
                            $icon_class = 'icon-basic-video'; 
                            break;
                        
                        case 'audio':
                            $icon_class = 'icon-basic-headset'; 
                            break;
                        
                        case 'chat':
                            $icon_class = 'icon-basic-message-multiple';
                            break;
                        
                        default:
                            $icon_class = 'icon-basic-sheet-txt';
                            break;
                        
                    endswitch;
                    
                    $icon_html = '<span class="icon ' . esc_attr( $icon_class ) . '"></span>';
                    
                    $html_loop_items .= '<div data-post-format="' . esc_attr( $post_format ) . '" class="item-post bg-parallax ' . esc_attr( $css_animation ) . ' wow" data-wow-delay="' . esc_attr( $animation_delay_num ) . 's" ' . $style_background_img . '>
                        					<div class="info-post">
                        						' . $icon_html . '
                        						<h6><a href="' . get_the_permalink() . '">' . get_the_title() . '</a></h6>
                        						<div class="meta-post">
                        							' . $cat_list_html . '
                        							<span class="date-post">' . get_the_date() . '</span>
                   							     <a href="' . get_comments_link() . '" class="comment-post">' . $comments_count->approved . '<i class="fa fa-comment-o"></i></a>
                        						</div>
                        					</div>
                        				</div><!-- /.item-post -->';
                    
                    break;
            
            endswitch;
            
            // Reset animatation delay each row
            if ( $i % $posts_per_row == 0 ) {
                $animation_delay_num = $animation_delay;
            }
        
        endwhile;
        
        $html .= '<div class="ts-lastest-posts-wrap">';
        $html .= $html_inner_before;
        $html .= $html_loop_items;
        $html .= $html_inner_after;
        $html .= '</div><!-- /.ts-lastest-posts-wrap -->';
        
    endif;
    
    return $html;
}

function theone_skills_bar_chart( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_skills_bar_chart', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'bar_text'      =>  '',
        'bar_value'     =>  80,
        'bar_color'     =>  '',
        'bar_bg_color'  =>  '',
        'bar_height'    =>  5,
        'css'           =>  '',
	), $atts ) );
    
    $css_class = '';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $bar_text_html = trim( $bar_text ) != '' ? '<span class="skillbar-title">' . sanitize_text_field( $bar_text ) . '</span>' : '';
    $bar_value = min( 100, max( 0, intval( $bar_value ) ) );
    $bar_bg_color_style = trim( $bar_bg_color ) != '' ? 'style="background-color: ' . esc_attr( $bar_bg_color ) . ';"' : '';
    
    $html = '<div class="ts-skillbars">
				<div class="item-skillbar" data-height="' . intval( $bar_height ) . '" data-percent="' . intval( $bar_value ) . '" data-bgskill="' . esc_attr( $bar_color ) . '">
            		' . $bar_text_html . '
            		<div class="skill-bar-bg" ' . $bar_bg_color_style . '>
            			<div class="skillbar-bar">
            				<div class="skill-bar-percent">' . intval( $bar_value ) . '&#37;</div>
            			</div>
            	    </div><!-- /.skill-bar-bg -->
            	</div><!-- /.item-skillbar -->
			</div><!-- /.ts-skillbars -->';
    
    return $html;
    
}


function theone_skills_circle_chart( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_skills_circle_chart', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'title'                 =>  '',
        'style'                 =>  'number-center',
        'circle_size'           =>  194,
        'icon'                  =>  '',
        'bar_text'              =>  '',
        'bar_value'             =>  '',
        'text_color'            =>  '',
        'circle_bar_color'      =>  '',
        'circle_track_color'    =>  '',
        'line_width'            =>  5,
        'css'                   =>  '',
	), $atts ) );
    
    $css_class = '';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $bar_value = min( 100, max( 0, intval( $bar_value ) ) );
    
    $html = '';
    
    $title_html = trim( $title ) != '' ? '<h5 class="skill-title">' . sanitize_text_field( $title ) . '</h5>' : '';
    
    $text_html = '';
    $text_style = trim( $text_color ) != '' ? 'style="color: ' . esc_attr( $text_color ) . ';"' : '';
    switch ( $style ) {
        case 'number-center': 
            $text_html .= '<span class="chart-percent" ' . $text_style . '>' . intval( $bar_value ) . '&#37;</span>';  
            break;
        case 'text-center':
            if ( trim( $bar_text ) != '' ) {
                $text_html .= '<span class="chart-text" ' . $text_style . '>' . sanitize_text_field( $bar_text ) . '</span>';   
            }
            break;
        case 'icon-center':
            if ( trim( $icon ) != '' ) {
                $text_html .= '<span class="' . esc_attr( $icon ) . '" ' . $text_style . '></span>';
            }
            break;
        default:
            break;
    }
    
    $html .= '<div class="ts-progressbar ' . esc_attr( $css_class ) . '">
				<div class="ts-chart" data-trackColor="' . esc_attr( $circle_track_color ) . '" data-barColor="' . esc_attr( $circle_bar_color ) . '" data-lineWidth="' . intval( $line_width ) . '" data-size="' .  intval( $circle_size ) . '" data-percent="' . intval( $bar_value ) . '">
	             	' . $text_html . '
	            </div>
			   	' . $title_html . '
			</div>';
    
    return $html;
    
}

function theone_skills_circle_diagram( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_skills_circle_diagram', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'default_text'              =>  '',
        'dimension'                 =>  '',
        'text_color'                =>  '#ffffff',
        'center_circle_color'       =>  '#27282d',
        'num_of_charts'             =>  5,
        'chart_item_1_text'         =>  '',
        'chart_item_1_value'        =>  50,
        'chart_item_1_circle_color' =>  '#aa9d71',
        'chart_item_2_text'         =>  '',
        'chart_item_2_value'        =>  50,
        'chart_item_2_circle_color' =>  '#aa9d71',
        'chart_item_3_text'         =>  '',
        'chart_item_3_value'        =>  50,
        'chart_item_3_circle_color' =>  '#aa9d71',
        'chart_item_4_text'         =>  '',
        'chart_item_4_value'        =>  50,
        'chart_item_4_circle_color' =>  '#aa9d71',
        'chart_item_5_text'         =>  '',
        'chart_item_5_value'        =>  50,
        'chart_item_5_circle_color' =>  '#aa9d71',
        'chart_item_6_text'         =>  '',
        'chart_item_6_value'        =>  50,
        'chart_item_6_circle_color' =>  '#aa9d71',
        'css'                       =>  '',
	), $atts ) );
    
    $css_class = '';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $num_of_charts = min( 6, max( 0, intval( $num_of_charts ) ) );
    
    $html = '';
    $items_html = '';
    
    if ( $num_of_charts > 0 ) {
        
        for ( $i = 1; $i <= $num_of_charts; $i++ ):
            $chart_text_key = 'chart_item_' . $i . '_text';
            $chart_val_key = 'chart_item_' . $i . '_value';
            $chart_circle_color_key = 'chart_item_' . $i . '_circle_color';
            
            $items_html .= '<div class="skill-chart-item">
        						<span class="text">' . sanitize_text_field( $$chart_text_key ) . '</span>
        						<input type="hidden" class="percent" value="' . min( 100, max( 0, intval( $$chart_val_key ) ) ) . '" />
        						<input type="hidden" class="color" value="' . esc_attr( $$chart_circle_color_key ) . '" />
        					</div>';
        
        endfor;
        
        $diagram_id = uniqid( 'ts-diagram-' );
        $html .= '<div class="ts-skill-diagram ' . esc_attr( $css_class ) . '" data-dimension="' . intval( $dimension ) . '" data-default-textcolor="' . esc_attr( $text_color ) . '" data-circlecolor="' . esc_attr( $center_circle_color ) . '"  data-default-text="' . esc_attr( $default_text ) . '">
    				<div id="' . esc_attr( $diagram_id ) . '" class="ts-diagram"></div>
    				<div class="ts-skill-chart">
    					' . $items_html . '
    				</div>
    			</div>';
        
    }
    
    return $html;
    
}

function theone_cta( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_cta', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'style'                     =>  '',
        'title'                     =>  '',
        'desc'                      =>  '',
        'icon'                      =>  '',
        'text_color'                =>  '',
        'button_text'               =>  '',
        'button_link'               =>  '',
        'link_target'               =>  '',
        'btn_text_color'            =>  '',
        'btn_text_hover_color'      =>  '',
        'btn_bg_color'              =>  '',
        'btn_bg_hover_color'        =>  '',
        'btn_border_radius'         =>  '',
        'btn_border_width'          =>  '',
        'btn_border_color'          =>  '',
        //'btn_border_hover_color'    =>  '',
        'btn_border_style'          =>  '',
        'css'                       =>  '',
	), $atts ) );
    
    $css_class = $style;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $cta_text_color_style = trim( $text_color ) != '' ? 'style="color: ' . esc_attr( $text_color ) . ';"' : '';
    
    $html = '';
    $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '"></span>' : '';
    $title_html = trim( $title ) != '' ? '<h3 ' . $cta_text_color_style . '>' . sanitize_text_field( $title ) . '</h3>' : '';
    $desc_html = trim( $desc ) != '' ? '<p>' . sanitize_text_field( $desc ) . '</p>' : '';
    $btn_html = '';
    $btn_class = 'ts-button-shortcode';
    $btn_style = '';
    $bg_attrs = '';
    $btn_style .= 'border-radius: ' . intval( $btn_border_radius ) . 'px;';
    if ( trim( $btn_border_style ) != 'none' && trim( $btn_border_style ) != '' ) {
        $btn_class .= ' button-outline';
        $btn_style .= trim( $btn_border_style ) != '' ? ' border-style: ' . esc_attr( $btn_border_style ) . '; border-color: ' . esc_attr( $btn_border_color ) . '; border-witdh: ' . intval( $btn_border_width ) . 'px;' : '';
    }
    else{
        $bg_attrs .= 'data-bg="' . esc_attr( $btn_bg_color ) . '" data-hoverbg="' . esc_attr( $btn_bg_hover_color ) . '"';
    }
    
    $btn_style = 'style="' . $btn_style . '"';
    
    if ( trim( $button_text ) != '' ) {
        $btn_html .= '<a ' . $btn_style . ' class="' . esc_attr( $btn_class ) . '" href="' . esc_attr( $button_link ) . '" ' . $bg_attrs . ' target="' . esc_attr( $link_target ) . '" data-color="' . esc_attr( $btn_text_color ) . '" data-colorhover="' . esc_attr( $btn_text_hover_color ) . '">
						<span class="ts-button-hover"></span>
						<span class="ts-button-text">' . sanitize_text_field( $button_text ) . '</span>
					</a>';
    }
    
    $html .= '<div class="ts-cta ' . esc_attr( $css_class ) . '">
				<div class="left-cta" ' . $cta_text_color_style . '>
					' . $icon_html . '
					<div class="content-cta">
						' . $title_html . '
						' . $desc_html . '
					</div>
				</div>
				<div class="right-cta">
				    ' . $btn_html . '
				</div>
			</div>';
    
    return $html;
    
}


function theone_button( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_button', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'style'                     =>  '',
        'hover_effect'              =>  '',
        'icon'                      =>  '',
        'text_color'                =>  '',
        'button_text'               =>  '',
        'button_link'               =>  '',
        'link_target'               =>  '',
        'btn_text_color'            =>  '',
        'btn_text_hover_color'      =>  '',
        'btn_bg_color'              =>  '',
        'btn_bg_hover_color'        =>  '',
        'btn_border_radius'         =>  '',
        'btn_border_width'          =>  '',
        'btn_border_color'          =>  '',
        'btn_border_style'          =>  '',
        'btn_shadow_color'          =>  '',
        'btn_shadow_hover_color'    =>  '',
        'css'                       =>  '',
	), $atts ) );
    
    $css_class = $style . ' ' . $hover_effect;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $btn_attrs = 'data-bg="' . esc_attr( $btn_bg_color ) . '" data-color="' . esc_attr( $text_color ) . '" data-hoverbg="' . esc_attr( $btn_bg_hover_color ) . '" data-colorhover="' . esc_attr( $btn_text_color ) . '"';
    
    $html = '';
    $icon_html = trim( $icon ) != '' ? '<span class="ts-icon-button"><span class="' . esc_attr( $icon ) . '"></span></span>' : '';
    $btn_html = '';
    $btn_class = 'ts-button-shortcode';
    $btn_style = '';
    $btn_style .= 'border-radius: ' . intval( $btn_border_radius ) . 'px;';
    if ( trim( $style ) == 'button-outline' ) {
        $btn_style .= trim( $btn_border_style ) != '' ? ' border-style: ' . esc_attr( $btn_border_style ) . '; border-color: ' . esc_attr( $btn_border_color ) . '; border-witdh: ' . intval( $btn_border_width ) . 'px;' : '';
    }
    if ( trim( $style ) == 'button-3d' ) {
        $btn_attrs .= ' data-colorshadow="' . esc_attr( $btn_shadow_color ) . '" data-shadowhover="' . esc_attr( $btn_shadow_hover_color ) . '"';
    }
    
    $btn_style = 'style="' . $btn_style . '"';
    
    $btn_html .=    '<a href="#" class="ts-button-shortcode ' . esc_attr( $css_class ) . '" ' . $btn_attrs . ' ' . $btn_style .'>
    					' . $icon_html . '
    					<span class="ts-button-hover"></span>
    					<span class="ts-button-text">' . sanitize_text_field( $button_text ) . '</span>
    				</a>';
    
    $html .= $btn_html;
    return $html;
    
}

function theone_dropcap_text( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_dropcap_text', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'text_desc'                 =>  '',
        'dropcap_font_size'         =>  '',
        'dropcap_color'             =>  '',
        'dropcap_bg_color'          =>  '',
        'dropcap_border_radius'     =>  '',
        'dropcap_border_style'      =>  '',
        'dropcap_border_width'      =>  '',
        'dropcap_border_color'      =>  '',
        'css_animation'             =>  '',
        'animation_delay'           =>  '',
        'css'                       =>  '',
	), $atts ) );
    
    $css_class = '';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    if ( !is_numeric( $animation_delay ) ) {
        $animation_delay = '0';
    }
    $animation_delay = $animation_delay . 's';
    
    $html = '';
    $first_char_html = '';
    $remaining_text_html = '';
    
    $text_desc = trim( $text_desc );
    if ( $text_desc != '' ) {
        $first_char = substr( $text_desc, 0, 1 );
        $remaining_text_html = sanitize_text_field( substr( $text_desc, 1 ) );
        
        $dropcap_style = 'font-size: ' . intval( $dropcap_font_size ) . 'px;';
        $dropcap_style .= trim( $dropcap_color ) != '' ? ' color: ' . esc_attr( $dropcap_color ) . ';' : '';
        $dropcap_style .= trim( $dropcap_bg_color ) != '' ? ' background-color: ' . esc_attr( $dropcap_bg_color ) . ';' : '';
        $dropcap_style .= ' border-radius: ' . esc_attr( $dropcap_border_radius ) . 'px;';
        $dropcap_style .= trim( $dropcap_border_style ) != '' ? ' border-style: ' . esc_attr( $dropcap_border_style ) . ';' : '';
        $dropcap_style .= ' border-width: ' . esc_attr( $dropcap_border_width ) . 'px;';
        $dropcap_style .= trim( $dropcap_border_color ) != '' ? ' border-color: ' . esc_attr( $dropcap_border_color ) . ';' : '';
        
        $dropcap_style = 'style="' . $dropcap_style . '"';
        
        $first_char_html .= '<span class="dropcap" ' . $dropcap_style . ' >' . sanitize_text_field( $first_char ) .'</span>';
        
    }
    
    $html .=    '<div class="text-dropcap wow ' . esc_attr( $css_animation . ' ' . $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '">
                    <p>' . $first_char_html . $remaining_text_html .'</p>
                </div><!-- /.text-dropcap -->';
    
    return $html;
    
}

function theone_advanced_gmap( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_advanced_gmap', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'map_type'          =>  'ROADMAP',
        'hue'               =>  '',
        'lightness'         =>  '',
        'saturation'        =>  '',
        'address'           =>  '',
        'longitude'         =>  '',
        'latitude'          =>  '',
        'pin_icon'          =>  '',
        'zoom'              =>  '',
        'map_height'        =>  375,
        'css_animation'     =>  '',
        'animation_delay'   =>  '',
        'css'               =>  '',
	), $atts ) );
    
    $css_class = 'ts-advanced-gmaps-wrap wow ' . $css_animation;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    if ( !is_numeric( $animation_delay ) ) {
        $animation_delay = '0';
    }
    $animation_delay = $animation_delay . 's';
    
    $html = '';
    
    $map_id = uniqid( 'google-map-' );
    
    $pin_icon = intval( $pin_icon );
    $pin_icon_url = THEONECORE_IMG_URL . '/marker.png';
    if ( $pin_icon > 0 ) {
        $pin_img = theone_resize_image( $pin_icon, null, 52, 58, true, true );
        $pin_icon_url = isset( $pin_img['url'] ) ? $pin_img['url'] : $pin_icon_url;
    }
    
    $html .=    '<div class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '">
                    <div id="' . esc_attr( $map_id ) . '" 
            			class="ts-advanced-gmaps" 
            			data-hue="' . esc_attr( $hue ) . '" 
            			data-lightness="' . intval( $lightness ) . '" 
            			data-saturation="' . intval( $saturation ) . '" 
            			data-modify-coloring="true" 
            			data-draggable="true" 
            			data-scale-control="true" 
            			data-map-type-control="true" 
            			data-zoom-control="true" 
            			data-pan-control="true" 
                        data-scrollwheel="true"
            			data-address="' . esc_attr( $address ) . '" 
            			data-longitude="' . esc_attr( $longitude ) . '" 
            			data-latitude="' . esc_attr( $latitude ) . '" 
            			data-pin-icon="' . esc_url( $pin_icon_url ) . '" 
            			data-zoom="14" 
            			style="height: ' . intval( $map_height ) . 'px; width: 100%;" 
            			data-map-type="' . esc_attr( $map_type ) . '">
                    </div><!-- /.ts-advanced-gmaps -->
                </div><!-- /.ts-advanced-gmaps-wrap -->';
    
    return $html;
    
}

function theone_img_box( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_img_box', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'style'                 =>  'img-short-content',
        'img_id'                =>  '',
        'title'                 =>  '',
        'short_content'         =>  '',
        'short_desc'            =>  '',
        'link'                  =>  '',
        'show_read_more_btn'    =>  'yes',
        'read_more_text'        =>  '',
        'link_target'           =>  '',
        'show_view_large_img'   =>  'yes',
        'caption_inner'         =>  'yes',
        'caption_position'      =>  'bottom',
        'text_color'            =>  '',
        'bg_color'              =>  '',
        'border_radius'         =>  '',
        'border_style'          =>  '',
        'border_width'          =>  6,
        'border_color'          =>  '',
        'css_animation'         =>  '',
        'animation_delay'       =>  '',
        'css'                   =>  '',
	), $atts ) );
    
    $html = '';
    $title_html = '';
    $content_or_desc_html = '';
    $img_html = '';
    $content_hover_html = '';
    $readmore_html = '';
    $content_or_caption_post_class = '';
    
    $html_style = '';
    $img_style = '';
    
    $css_class = 'ts-imagebox wow ' . $css_animation;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    if ( !is_numeric( $animation_delay ) ) {
        $animation_delay = '0';
    }
    $animation_delay = $animation_delay . 's';
    
    $img = array(
		'url' => '',
		'width' => 540,
		'height' => 354
	);
    $img_large = array(
		'url' => '',
		'width' => 1156,
		'height' => 779
	);
    
    if ( $img_id > 0 ) {
        $img = theone_resize_image( $img_id, null, $img['width'], $img['height'], true, true );
        $img_large = theone_resize_image( $img_id, null, $img_large['width'], $img_large['height'], true, true );
    }
    
    if ( trim( $style ) == 'img-caption' ) { // Frame box
        $css_class .= ' image-frame';
        if ( trim( $caption_inner ) == 'yes' ){
            $css_class .= ' inner-caption ' . $caption_position;
        }
        else{
            $css_class .= ' our-caption';
        }
        $img['width'] = 540; $img['height'] = 465; 
        $img = theone_resize_image( $img_id, null, $img['width'], $img['height'], true, true );
        $content_hover_html .= '<a href="' . esc_url( $link ) . '" target="' . esc_attr( $link_target ) . '"><span class="icon icon-arrows-plus"></span></a>';
        if ( $show_view_large_img == 'yes' ) {
            $content_hover_html .= '<a rel="prettyPhoto" href="' . esc_url( $img_large['url'] ) . '" title="' . esc_attr( $title ) . '"><span class="icon icon-basic-magnifier"></span></a>';
        }
        $content_or_desc_html .= trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
        
        $img_style .= trim( $border_radius ) != '' ? ' border-radius: ' . esc_attr( $border_radius ) . 'px;' : '';
        $img_style .= trim( $border_style ) != '' ? ' border-style: ' . esc_attr( $border_style ) . ';' : '';
        $img_style .= trim( $border_color ) != '' ? ' border-color: ' . esc_attr( $border_color ) . ';' : '';
        $img_style .= trim( $border_width ) != '' ? ' border-width: ' . esc_attr( $border_width ) . 'px;' : '';
        $img_style = 'style="' . $img_style . '"';
        
        $content_or_caption_post_class .= 'caption-post';
    }
    else{ // Frame box
        if ( trim( $show_read_more_btn ) == 'yes' ) {
            $readmore_html .= '<a href="' . esc_url( $link ) . '" class="ts-readmore" target="' . esc_attr( $link_target ) . '">' . sanitize_text_field( $read_more_text ) . '</a>';
        }
        $content_hover_html .= '<a href="' . esc_url( $link ) . '" target="' . esc_attr( $link_target ) . '"><span class="icon icon-arrows-plus"></span></a>';
        $content_or_desc_html .= trim( $short_content ) != '' ? '<p>' . sanitize_text_field( $short_content ) . '</p>' : '';
        
        $html_style .= trim( $text_color ) ? ' color: ' . $text_color . 'px;' : '';
        $html_style .= trim( $bg_color ) ? ' background-color: ' . $bg_color . 'px;' : '';
        
        $content_or_caption_post_class .= 'content-post';
    }
    
    $title_html .= trim( $title ) != '' ? '<h4>' . sanitize_text_field( $title ) . '</h4>' : '';
    
    if ( trim( $img['url'] ) != '' ) {
        $img_html .= '<figure>
						<img src="' . esc_url( $img['url'] ) . '" alt="">
					</figure>';
    }
    
    $html_style = 'style="' . $html_style . '"';
    
    $html = '<div class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $html_style . '>
				<div class="img-post" ' . $img_style . '>
					' . $img_html . '
					<div class="hover-post">
						<div class="content-hover">
							' . $content_hover_html . '
						</div>
					</div>
				</div>
				<div class="' . esc_attr( $content_or_caption_post_class ) . '">
					' . $title_html . '
					' . $content_or_desc_html . '
					' . $readmore_html . '
				</div>
			</div>';
    
    
    return $html;
    
}

function theone_icon_box( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_icon_box', $atts ) : $atts;

    extract( shortcode_atts( array(
        'style'                 =>  'iconbox-style1',
        'icon'                  =>  '',
        'title'                 =>  '',
        'short_desc'            =>  '',
        'icon_color'            =>  '',
        'icon_hover_color'      =>  '',
        'icon_bg_color'         =>  '',
        'icon_hover_bg_color'   =>  '',
        'text_color'            =>  '',
        'content_box_bg_color'  =>  '',
        'front_bg_color'        =>  '', // Style 10
        'back_bg_color'         =>  '', // Style 10
        'link'                  =>  '',
        'show_read_more_btn'    =>  '',
        'read_more_text'        =>  '',
        'link_target'           =>  '',
        'css_animation'         =>  '',
        'animation_delay'       =>  '',
        'css'                   =>  '',
	), $atts ) );
    
    $html = '';
    
    $css_class = 'ts-iconbox wow ' . $style . ' ' . $css_animation;
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    if ( !is_numeric( $animation_delay ) ) {
        $animation_delay = '0';
    }
    $animation_delay = $animation_delay . 's';
    
    $icon_style = '';
    $icon_style .= trim( $icon_color ) != '' ? ' color: ' . esc_attr( $icon_color ) . ';' : '';
    $text_style = '';
    $text_style .= trim( $text_color ) != '' ? ' color: ' . esc_attr( $text_color ) . ';' : '';
    $box_style = '';
    $box_style .= $text_style;
    
    switch ( $style ):
    
        case 'iconbox-style1':
        
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . '>' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            $html .= '<div class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . '>
						<h3 ' . $text_style . '>
							' . $icon_html . '
							' . $link_text_html . '
						</h3>
						' . $short_desc_html . '
					</div>';
                    
            break;
        
        case 'iconbox-style2':
            
            $css_class .= ' icon-css-via-js';
            $html_atts = '';
            $html_atts .= 'data-icon-color="' . esc_attr( $icon_color ) . '" data-icon-hover-color="' . esc_attr( $icon_hover_color ) . '" data-icon-bg-color="' . esc_attr( $icon_bg_color ) . '" data-icon-hover-bg-color="' . esc_attr( $icon_hover_bg_color ) . '"';
            $html_atts .= ' data-text-color="' . esc_attr( $text_color ) . '"';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '">' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $html .= '<div class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $html_atts . ' >
						' . $icon_html . '
						<h3>' . $link_text_html . '</h3>
						' . $short_desc_html . '
					</div>';
            
            
            
            break;
        
        case 'iconbox-style3':
            
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . '>' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $readmore_btn_html = '';
            if ( trim( $show_read_more_btn ) == 'yes' ) {
                $readmore_btn_html .= '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . ' class="ts-readmore">' . sanitize_text_field( $read_more_text ) . '</a>';
            }
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . ' >
						' . $icon_html . '
						<h3>' . $link_text_html . '</h3>
						' . $short_desc_html . '
						' . $readmore_btn_html . '
					</div>';
            
            break;
     
        case 'iconbox-style4':
            
            $icon_style .= trim( $icon_bg_color ) != '' ? ' background-color: ' . esc_attr( $icon_bg_color ) . ';' : '';
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . '>' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . ' >
						' . $icon_html . '
						<h3>' . $link_text_html . '</h3>
						' . $short_desc_html . '
					</div>';
            
            break;
     
        case 'iconbox-style5':
            
            $icon_style .= trim( $icon_bg_color ) != '' ? ' background-color: ' . esc_attr( $icon_bg_color ) . ';' : '';
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . '>' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $readmore_btn_html = '';
            if ( trim( $show_read_more_btn ) == 'yes' ) {
                $readmore_btn_html .= '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . ' class="ts-readmore">' . sanitize_text_field( $read_more_text ) . '</a>';
            }                        
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . ' >
						' . $icon_html . '
						<h3>' . $link_text_html . '</h3>
						' . $short_desc_html . '
						' . $readmore_btn_html . '
					</div>';
        
            break;
        
        case 'iconbox-style6':
            
            $icon_style .= trim( $icon_bg_color ) != '' ? ' background-color: ' . esc_attr( $icon_bg_color ) . ';' : '';
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            $content_box_style = trim( $content_box_bg_color ) != '' ? 'style="background-color: ' . esc_attr( $content_box_bg_color ) . ';"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . '>' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $readmore_btn_html = '';
            if ( trim( $show_read_more_btn ) == 'yes' ) {
                $readmore_btn_html .= '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . ' class="ts-readmore">' . sanitize_text_field( $read_more_text ) . '</a>';
            }                        
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . ' >
						' . $icon_html . '
						<div class="content-iconbox" ' . $content_box_style . '>
							<h3>' . $link_text_html . '</h3>
							' . $short_desc_html . '
							' . $readmore_btn_html . '
						</div>
					</div>';
            
            break;
     
        case 'iconbox-style7':
            
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            $content_box_style = trim( $content_box_bg_color ) != '' ? 'style="background-color: ' . esc_attr( $content_box_bg_color ) . ';"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . '>' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $readmore_btn_html = '';
            if ( trim( $show_read_more_btn ) == 'yes' ) {
                $readmore_btn_html .= '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '" ' . $text_style . ' class="ts-readmore">' . sanitize_text_field( $read_more_text ) . '</a>';
            }                        
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . ' >
						<h3>' . $link_text_html . '</h3>
						' . $icon_html . '
						' . $short_desc_html . '
						' . $readmore_btn_html . '
					</div>';
            
            break;
     
        case 'iconbox-style8':
            
            $css_class .= ' icon-css-via-js';
            $html_atts = '';
            $html_atts .= 'data-icon-color="' . esc_attr( $icon_color ) . '" data-icon-hover-color="' . esc_attr( $icon_hover_color ) . '" data-icon-bg-color="' . esc_attr( $icon_bg_color ) . '" data-icon-hover-bg-color="' . esc_attr( $icon_hover_bg_color ) . '"';
            $html_atts .= ' data-text-color="' . esc_attr( $text_color ) . '"';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '">' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $html_atts . '>
						' . $icon_html . '
						<div class="content-iconbox">
							<h3>' . $link_text_html . '</h3>
							' . $short_desc_html . '
						</div>
					</div>';
            
            break;
        
        case 'iconbox-style9':
            
            $css_class .= ' icon-css-via-js';
            $html_atts = '';
            $html_atts .= 'data-icon-color="' . esc_attr( $icon_color ) . '" data-icon-hover-color="' . esc_attr( $icon_hover_color ) . '" data-icon-bg-color="" data-icon-hover-bg-color=""';
            $html_atts .= ' data-text-color="' . esc_attr( $text_color ) . '"';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '">' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $html_atts . '>
						' . $icon_html . '
						<div class="content-iconbox">
							<h3>' . $link_text_html . '</h3>
							' . $short_desc_html . '
						</div>
					</div>';
                
            break;
     
        case 'iconbox-style10':
            
            $icon_style = trim( $icon_style ) != '' ? 'style="' . trim( $icon_style ) . '"' : '';
            $text_style = trim( $text_style ) != '' ? 'style="' . trim( $text_style ) . '"' : '';
            $box_style = trim( $box_style ) != '' ? 'style="' . trim( $box_style ) . '"' : '';
            $front_style = trim( $front_bg_color ) != '' ? 'style="background-color: ' . esc_attr( $front_bg_color ) . ';"' : '';
            $back_style = trim( $back_bg_color ) != '' ? 'style="background-color: ' . esc_attr( $back_bg_color ) . ';"' : '';
            
            $icon_html = trim( $icon ) != '' ? '<span class="' . esc_attr( $icon ) . '" ' . $icon_style . '></span>' : '';
            $link_text_html = trim( $link ) != '' ? '<a href="' . esc_attr( $link ) . '" target="' . esc_attr( $link_target ) . '">' . sanitize_text_field( $title ) . '</a>' : sanitize_text_field( $title );
            $short_desc_html = trim( $short_desc ) != '' ? '<p>' . sanitize_text_field( $short_desc ) . '</p>' : '';
            
            $iconbox_unid_id = uniqid( 'ts-iconbox-' );
            
            $html .= '<div id="' . esc_attr( $iconbox_unid_id ) . '" class="' . esc_attr( $css_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . '" ' . $box_style . ' >
						<div class="icon-title" ' . $front_style . '>
							' . $icon_html . '
							<h3 ' . $text_style . '>' . sanitize_text_field( $title ) . '</h3>
						</div>
						<div class="content-hover" ' . $back_style . '>
							<div class="ts-table">
								<div class="ts-table-cell">
									<h3>' . $link_text_html . '</h3>
									' . $short_desc_html . '
								</div>
							</div>
						</div>
					</div>';
            
            break;
    
    endswitch;
    
    return $html;
    
}

function theone_portfolio_carousel( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_portfolio_carousel', $atts ) : $atts;

    extract( shortcode_atts( array(
        'style'                 =>  'ts-carousel-modern',
        'portfolio_cat_id'      =>  0,
        'limit'                 =>  8,
        'items_per_slide'       =>  4,
        'show_link_icon'        =>  'yes',
        'show_view_large_icon'  =>  'yes',
        'show_navcontrol'       =>  'true',
        'show_dots'             =>  'false',
        'autoplay'              =>  'false',
        'autoplay_timeout'      =>  4000, // milliseconds
        'rtl'                   =>  'false',
        'css'                   =>  '',
	), $atts ) );
    
    $css_class = 'ts-portfolio-carousel-wrap';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $args = array(
		'post_type' 			=> 'portfolio',
		'post_status' 			=> 'publish',
        'showposts'             => intval( $limit )
	);
    
    $portfolio_cat_id = intval( $portfolio_cat_id );
    if ( $portfolio_cat_id > 0 ):
        
        $args['tax_query'] = array(
            array(
                'taxonomy'  => 'portfolio_cat',
                'field'     => 'id',
                'terms'     => $portfolio_cat_id
            )
        );
        
    endif;
    
    $query = new WP_Query( $args );
    
    $html = '';
    
    if ( $query->have_posts() ){
        
        $items_html = '';
        //$carousel_img_uniq_class = uniqid( 'img-portfolio-' );
        
        while ( $query->have_posts() ): $query->the_post();
            
            $thumb = array(
				'url' => '',
				'width' => 0,
				'height' => 0
			);
            $thumb_large = $thumb;
            
            $thumb = theone_resize_image( null, null, 475, 356, true, true );
            $thumb_large = theone_resize_image( null, null, 950, 730, true, true );
            
            if ( has_post_thumbnail() ):
                
                $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 475, 356, true, true );
                $thumb_large = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 950, 730, true, true );
                
            endif;
            
            $icon_link_html = trim( $show_link_icon ) == 'yes' ? '<a href="' . get_permalink() . '"><span class="icon icon-arrows-plus"></span></a>' : '';
            $border_right_html = ( trim( $show_link_icon ) == 'yes' && trim( $show_view_large_icon ) == 'yes' ) ? '<span class="border-right"></span>' : '';
            $icon_view_html =  trim( $show_view_large_icon ) == 'yes' ? '<a href="' . esc_url( $thumb_large['url'] ) . '" rel="prettyPhoto"><span class="icon icon-basic-magnifier"></span></a>' : '';
            $cat_list_html = get_the_term_list( get_the_ID(), 'portfolio_cat', '', ', ' );
            
            $items_html .=  '<div class="item-portfolio">
            					<div class="img-portfolio">
            						<figure><img src="' . esc_url( $thumb['url'] ) . '" alt=""></figure>
            						<div class="content-hover">
            							' . $icon_link_html . '
            							' . $border_right_html . '
            							' . $icon_view_html . '
            						</div>
            					</div>
            					<div class="info-porfolio">
            						<h4><a href="' . get_permalink() . '">' . get_the_title() . '</a></h4>
            						<span class="list-category">' . $cat_list_html . '</span>
            					</div>
            				</div>';
        
        endwhile;
        
        $data_margin = trim( $style == 'ts-carousel-modern' ) ? 0 : 30; 
                   
        $html .=    '<div class="' . esc_attr( $css_class ) . '">
                        <div class="ts-owl-carousel ts-portfolio-carousel ' . esc_attr( $style ) . '" data-number="' . intval( $items_per_slide ) . '" data-loop="true" data-Dots="' . esc_attr( $show_dots ) . '" data-navControl = "' . esc_attr( $show_navcontrol ) . '" data-autoPlay="' . esc_attr( $autoplay ) . '" data-autoPlayTimeout="' . esc_attr( $autoplay_timeout ) . '" data-margin="' . esc_attr( $data_margin ) . '" data-rtl="' . esc_attr( $rtl ) . '" >
                            ' . $items_html . '
                        </div><!-- /.ts-portfolio-carousel -->
                    </div><!-- /.' . esc_attr( $css_class ) . ' -->';
    }
    
    wp_reset_postdata();
    
    return $html;
    
}


function theone_clients_carousel( $atts ) {
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_clients_carousel', $atts ) : $atts;

    extract( shortcode_atts( array(
        'style'                 =>  'ts-carousel-modern',
        'client_cat_id'         =>  0,
        'limit'                 =>  8,
        'items_per_slide'       =>  4,
        'use_secondary_logo'    =>  'no',
        'autoplay'              =>  'false',
        'autoplay_timeout'      =>  4000, // milliseconds
        'rtl'                   =>  'false',
        'css'                   =>  '',
	), $atts ) );
    
    $css_class = 'ts-client-carousel-wrap';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $args = array(
		'post_type' 			=> 'client',
		'post_status' 			=> 'publish',
        'showposts'             => intval( $limit )
	);
    
    $client_cat_id = intval( $client_cat_id );
    if ( $client_cat_id > 0 ):
        
        $args['tax_query'] = array(
            array(
                'taxonomy'  => 'client_cat',
                'field'     => 'id',
                'terms'     => $client_cat_id
            )
        );
        
    endif;
    
    $query = new WP_Query( $args );
    
    $html = '';
    
    if ( $query->have_posts() ){
        
        $items_html = '';
        $use_secondary_logo = trim( $use_secondary_logo ) == 'yes';
        
        while ( $query->have_posts() ): $query->the_post();
            
            $client_logo_url = THEONECORE_BASE_URL . '/noimage/no-client-logo.png';
            $client_logo_id = 0;
            
            if ( $use_secondary_logo ) {
                $client_logo_id = intval( get_post_meta( get_the_ID(), 'theone_client_secondary_logo_id', true ) );
                $client_logo_url = wp_get_attachment_url( $client_logo_id );
                $client_logo_path = str_replace( get_site_url(), rtrim( ABSPATH, '/' ), $client_logo_url );
                if ( !file_exists( $client_logo_path ) ) {
                    $client_logo_id = intval( get_post_meta( get_the_ID(), 'theone_client_logo_id', true ) );
                }
            }
            else{
                $client_logo_id = intval( get_post_meta( get_the_ID(), 'theone_client_logo_id', true ) );
            }
            
            if ( $client_logo_id > 0 ) {
                $client_logo_url = wp_get_attachment_url( $client_logo_id );
                $client_logo_path = str_replace( get_site_url(), rtrim( ABSPATH, '/' ), $client_logo_url );
                if ( !file_exists( $client_logo_path ) ) {
                    $client_logo_url = THEONECORE_BASE_URL . '/noimage/no-client-logo.png';
                }
            }
            
            $use_permalink = get_post_meta( get_the_ID(), 'theone_use_permalink', true ) == 'on';
            $client_link = $use_permalink ? get_permalink() : get_post_meta( get_the_ID(), 'theone_client_link', true );
            
            $items_html .=  '<div class="client-item">
        						<a href="' . esc_url( $client_link ) . '" target="__blank">
        							<figure><img src="' . esc_url( $client_logo_url ) . '" alt=""></figure>
        						</a>
        					</div>';
        
        endwhile;
        
        $data_margin = 0; 
               
        $html .=    '<div class="' . esc_attr( $css_class ) . '">
                        <div class="ts-owl-carousel ts-client-carousel ts-client-shortcode has-slide ' . esc_attr( $style ) . '" data-number="' . intval( $items_per_slide ) . '" data-loop="true" data-Dots="false" data-navControl = "false" data-autoPlay="' . esc_attr( $autoplay ) . '" data-autoPlayTimeout="' . esc_attr( $autoplay_timeout ) . '" data-margin="' . esc_attr( $data_margin ) . '" data-rtl="' . esc_attr( $rtl ) . '" >
                            ' . $items_html . '
                        </div><!-- /.ts-client-carousel -->
                    </div><!-- /.' . esc_attr( $css_class ) . ' -->';
    }
    
    wp_reset_postdata();
    
    return $html;
    
}

function theone_clients_grid( $atts ) {
    
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_clients_grid', $atts ) : $atts;

    extract( shortcode_atts( array(
        'style'                 =>  'client-grid',
        'client_cat_id'         =>  0,
        'limit'                 =>  8,
        'items_per_row'         =>  4,
        'use_secondary_logo'    =>  'no',
        'css_animation'         =>  '',
        'animation_delay'       =>  0.4, // seconds
        'follow_delay'          =>  0.2,
        'css'                   =>  '',
	), $atts ) );
    
    $css_class = 'ts-client-shortcode-wrap';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $args = array(
		'post_type' 			=> 'client',
		'post_status' 			=> 'publish',
        'showposts'             => intval( $limit )
	);
    
    $client_cat_id = intval( $client_cat_id );
    if ( $client_cat_id > 0 ):
        
        $args['tax_query'] = array(
            array(
                'taxonomy'  => 'client_cat',
                'field'     => 'id',
                'terms'     => $client_cat_id
            )
        );
        
    endif;
    
    $query = new WP_Query( $args );
    
    $items_per_row = min( 6, max( 1, intval( $items_per_row ) ) );
    $number_name_args = array( 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'night', 'ten' );
    
    $html = '';
    
    if ( $query->have_posts() ) {
        
        $items_html = '';
        $use_secondary_logo = trim( $use_secondary_logo ) == 'yes';
        if ( !is_numeric( $animation_delay ) ) {
            $animation_delay = '0';
        }
        if ( !is_numeric( $follow_delay ) ) {
            $follow_delay = '0';
        }
        $animation_delay_initial = $animation_delay;
        
        while ( $query->have_posts() ): $query->the_post();
            
            $cur_post_num = $query->current_post + 1; // First post is number 1
            $cur_row_num = ceil( $cur_post_num / $items_per_row );
            $items_class = 'client-item';
            $items_class .= ' row-' . ceil( $cur_post_num / $items_per_row );
            
            $items_class .= ( $cur_post_num == 1 ) ? ' item-number-' . $cur_post_num . ' item-first' : ' item-number-' . $cur_post_num;
            if ( $cur_post_num == $query->post_count ) {
                $items_class .= ' item-last';
            }
            
            if ( $cur_row_num == 1 ) {
                $items_class .= ' row-first';
            }
            
            if ( $cur_row_num == ceil( $query->post_count / $items_per_row ) ) {
                $items_class .= ' row-last row-end';
            }
            
            if ( $query->current_post % $items_per_row == 0 ) {
                // Reset $animation_delay each row to make sure there is no item must waited too long before appear
                $animation_delay = $animation_delay_initial;
            }
            
            $item_thumb_bg_html = ''; // Grid with thumbnail style
            
            $client_logo_url = THEONECORE_BASE_URL . '/noimage/no-client-logo.png';
            $client_logo_id = 0;
            
            // Use secondary logo?
            if ( $use_secondary_logo ) {
                $client_logo_id = intval( get_post_meta( get_the_ID(), 'theone_client_secondary_logo_id', true ) );
                $client_logo_url = wp_get_attachment_url( $client_logo_id );
                $client_logo_path = str_replace( get_site_url(), rtrim( ABSPATH, '/' ), $client_logo_url );
                if ( !file_exists( $client_logo_path ) ) { // Not found secondary logo --> set primary logo
                    $client_logo_id = intval( get_post_meta( get_the_ID(), 'theone_client_logo_id', true ) );
                }
            }
            else{ // Use primary logo
                $client_logo_id = intval( get_post_meta( get_the_ID(), 'theone_client_logo_id', true ) );
            }
            
            if ( $client_logo_id > 0 ) {
                $client_logo_url = wp_get_attachment_url( $client_logo_id );
                $client_logo_path = str_replace( get_site_url(), rtrim( ABSPATH, '/' ), $client_logo_url );
                if ( !file_exists( $client_logo_path ) ) { // Not found logo --> use no logo picture as placeholder logo
                    $client_logo_url = THEONECORE_BASE_URL . '/noimage/no-client-logo.png';
                }
            }
            
            $use_permalink = get_post_meta( get_the_ID(), 'theone_use_permalink', true ) == 'on';
            $client_link = $use_permalink ? get_permalink() : get_post_meta( get_the_ID(), 'theone_client_link', true );
            
            if ( trim( $style ) == 'client-imagethumbs' ) {
                $thumb = array(
    				'url' => '',
    				'width' => 0,
    				'height' => 0
    			);
                
                $thumb = theone_resize_image( null, null, 475, 332, true, true );
                
                if ( has_post_thumbnail() ):
                    $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 475, 332, true, true );
                endif;
                
                $item_thumb_bg_html .= '<div class="ts-bg-client" style="background-image: url(' . esc_url( $thumb['url'] ) . ');"></div>';  
            }
            
            $items_class .= ' wow ' . $css_animation;
            
            $items_html .= '<div class="' . esc_attr( $items_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . 's" >
                            	<a href="' . esc_url( $client_link ) . '">
                                    ' . $item_thumb_bg_html . '
                            		<figure><img src="' . esc_url( $client_logo_url ) . '" alt=""></figure>
                            	</a>
                            </div>';
            
            $animation_delay += $follow_delay; // Increase animation delay each loop
            
        endwhile;
        
        $grid_class = 'ts-client-shortcode ' . $style;
        $grid_class .= isset( $number_name_args[$items_per_row - 1] ) ? ' ' . $number_name_args[$items_per_row - 1] . '-item' : '';
        
        $html .=    '<div class="' . esc_attr( $css_class ) . '">
                        <div class="' . esc_attr( $grid_class ) . '" data-itemrow="' . esc_attr( $items_per_row ) . '">
                            ' . $items_html . '
                        </div>
                    </div><!-- /.' . esc_attr( $css_class ) . ' -->';
        
    }    
    
    wp_reset_postdata();
    
    return $html;
}

function theone_animated_column( $atts ) {
    
    $atts = function_exists( 'vc_map_get_attributes' ) ? vc_map_get_attributes( 'theone_animated_column', $atts ) : $atts;
    
    extract( shortcode_atts( array(
        'style'                     =>  'multiple-column',
        'animated_column_cat_id'    =>  0,
        'readmore_text'             =>  '',
        'limit'                     =>  8,
        'items_per_row'             =>  4,
        'odd_overlay_color'         =>  '',
        'even_overlay_color'        =>  '',
        'css_animation'             =>  '',
        'animation_delay'           =>  0.4,  //second
        'follow_delay'              =>  0.2,
        'css'                       =>  '',
	), $atts ) );
    
    $css_class = 'ts-shortcode-column-wrap';
    if ( function_exists( 'vc_shortcode_custom_css_class' ) ):
        $css_class .= apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), '', $atts );
    endif;
    
    $args = array(
		'post_type' 			=> 'animated_column',
		'post_status' 			=> 'publish',
        'showposts'             => intval( $limit )
	);
    
    $animated_column_cat_id = intval( $animated_column_cat_id );
    if ( $animated_column_cat_id > 0 ):
        
        $args['tax_query'] = array(
            array(
                'taxonomy'  => 'animated_column_cat',
                'field'     => 'id',
                'terms'     => $animated_column_cat_id
            )
        );
        
    endif;
    
    $query = new WP_Query( $args );
    
    $items_per_row = min( 6, max( 1, intval( $items_per_row ) ) );
    $number_name_args = array( 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'night', 'ten' );
    
    $html = '';
    
    if ( $query->have_posts() ) {
        
        $items_html = '';
        if ( !is_numeric( $animation_delay ) ) {
            $animation_delay = '0';
        }
        if ( !is_numeric( $follow_delay ) ) {
            $follow_delay = '0';
        }
        $animation_delay_initial = $animation_delay;
        
        while ( $query->have_posts() ): $query->the_post(); 
            
            $icon_html = '';
            $icon_class = get_post_meta( get_the_ID(), 'theone_icon_class', true );
            if ( trim( $icon_class ) != '' ) {
                $icon_html = '<span class="' . esc_attr( $icon_class ) . '"></span>';
            }
            
            $excerpt_html = '<p>' . theone_get_the_excerpt_max_charlength( 180 ) . '</p>';
            
            $use_permalink = get_post_meta( get_the_ID(), 'theone_use_permalink', true ) == 'on';
            $item_link = $use_permalink ? get_permalink() : get_post_meta( get_the_ID(), 'theone_animate_link', true );
            $reamore_html = trim( $item_link ) != '' ? '<a href="' . esc_attr( $item_link ) . '" class="link-readmore">' . sanitize_text_field( $readmore_text ) . '</a>' : ''; 
            
            $cur_post_num = $query->current_post + 1; // First post is number 1
            $cur_row_num = ceil( $cur_post_num / $items_per_row );
            $items_class = 'column-item';
            $items_class .= ' row-' . ceil( $cur_post_num / $items_per_row );
            
            $items_class .= ( $cur_post_num == 1 ) ? ' item-number-' . $cur_post_num . ' item-first' : ' item-number-' . $cur_post_num;
            if ( $cur_post_num == $query->post_count ) {
                $items_class .= ' item-last';
            }
            
            if ( $cur_row_num == 1 ) {
                $items_class .= ' row-first';
            }
            
            if ( $cur_row_num == ceil( $query->post_count / $items_per_row ) ) {
                $items_class .= ' row-last row-end';
            }
            
            if ( $query->current_post % $items_per_row == 0 ) {
                // Reset $animation_delay each row to make sure there is no item must waited too long before appear
                $animation_delay = $animation_delay_initial;
            }
            
            $items_class .= ' wow ' . $css_animation;
            
            switch ( $style ):
            
                case 'multiple-column':
                    
                    $items_html .= '<div class="' . esc_attr( $items_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . 's" >
                    					<div class="column-title">
                    						' . $icon_html . '
                    						<h3>' . get_the_title() . '</h3>
                    					</div>
                    					<div class="content-hover">
                    						<div class="ts-table">
                    							<div class="ts-table-cell">
                    								' . $excerpt_html . '
                    								' . $reamore_html . '
                    							</div>
                    						</div>
                    					</div>
                    				</div>';
                    
                    break;
                    
                case 'colorful-animate':
                    
                    $bg_color = get_post_meta( get_the_ID(), 'theone_animate_bg_color', true );
                    $bg_color = trim( $bg_color ) == '' ? '#dcc6b9' : $bg_color;
                    $item_style = 'style="background-color: ' . esc_attr( $bg_color ) . ';"';
                    
                    // Need ts-button class
                    $reamore_html = trim( $item_link ) != '' ? '<a href="' . esc_attr( $item_link ) . '" class="ts-button">' . sanitize_text_field( $readmore_text ) . '</a>' : ''; 
                    
                    $items_html .= '<div class="' . esc_attr( $items_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . 's" ' . $item_style . '>
                                    	<div class="column-title">
                                    		' . $icon_html . '
                                    		<h3>' . get_the_title() . '</h3>
                                    	</div>
                                    	<div class="content-hover">
                                    		' . $excerpt_html . '
                                    		' . $reamore_html . '
                                    	</div>
                                    </div>';
                    
                    break;
                
                case 'image-background':
                    
                    $thumb = array(
        				'url' => '',
        				'width' => 0,
        				'height' => 0
        			);
                    $thumb_large = $thumb;
                    
                    $thumb = theone_resize_image( null, null, 475, 364, true, true );
                    
                    if ( has_post_thumbnail() ):
                        
                        $thumb = theone_resize_image( get_post_thumbnail_id( get_the_ID() ), null, 475, 364, true, true );
                        
                    endif;
                    
                    $items_html .= '<div class="' . esc_attr( $items_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . 's" >
                    					<div class="background-column" style="background-image: url(' . esc_url( $thumb['url'] ) . ');">
                    					</div>
                    					<div class="content-column">
                    						' . $icon_html . '
                    						<h3>' . get_the_title() . '</h3>
                    						' . $excerpt_html . '
                    						' . $reamore_html . '
                    					</div>
                    				</div>';
                    
                    break;
                
                case 'simple-icon':
                    
                    $items_html .= '<div class="' . esc_attr( $items_class ) . '" data-wow-delay="' . esc_attr( $animation_delay ) . 's" >
                    					<a target="_self" href="' . esc_url( $item_link ) . '">
                    						<div class="column-icon">
                    							' . $icon_html . '
                    						</div>
                    						<div class="column-title">
                    							<h3>' . get_the_title() . '</h3>
                    						</div>
                    					</a>
                    				</div>';
                    
                    break;
                    
            endswitch;
            
            $animation_delay += $follow_delay; // Increase animation delay each loop
            
        endwhile;
        
        $column_class = 'shortcode-column ' . $style;
        $column_class .= isset( $number_name_args[$items_per_row - 1] ) ? ' ' . $number_name_args[$items_per_row - 1] . '-column' : '';
        
        $html .=    '<div class="' . esc_attr( $css_class ) . '">
                        <div class="' . esc_attr( $column_class ) . '">
                        ' . $items_html . '
                        </div><!-- /.' . esc_attr( $column_class ) . ' -->
                    </div><!-- /.' . esc_attr( $css_class ) . ' -->';
        
    }
    
    wp_reset_postdata();
    
    return $html;
}


// Actions add shortcodes
add_shortcode( 'theone_textbox', 'theone_textbox' );
add_shortcode( 'theone_latest_posts', 'theone_latest_posts' );
add_shortcode( 'theone_skills_bar_chart', 'theone_skills_bar_chart' );
add_shortcode( 'theone_skills_circle_chart', 'theone_skills_circle_chart' );
add_shortcode( 'theone_skills_circle_diagram', 'theone_skills_circle_diagram' );
add_shortcode( 'theone_cta', 'theone_cta' );
add_shortcode( 'theone_button', 'theone_button' );
add_shortcode( 'theone_dropcap_text', 'theone_dropcap_text' );
add_shortcode( 'theone_advanced_gmap', 'theone_advanced_gmap' );
add_shortcode( 'theone_img_box', 'theone_img_box' );
add_shortcode( 'theone_icon_box', 'theone_icon_box' );
add_shortcode( 'theone_portfolio_carousel', 'theone_portfolio_carousel' );
add_shortcode( 'theone_clients_carousel', 'theone_clients_carousel' );
add_shortcode( 'theone_clients_grid', 'theone_clients_grid' );
add_shortcode( 'theone_animated_column', 'theone_animated_column' );





