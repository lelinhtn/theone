<?php
/**
 * Custom Taxonomies
 * @package  Nella Core 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if (!function_exists('theone_core_create_taxonomy_brand')) {
    
    function theone_core_create_taxonomy_brand() {
        // Taxonomy 
        $labels = array(
        	'name'                       => _x( 'Brands', 'product brands', 'theone-core' ),
        	'singular_name'              => _x( 'Brand', 'product brand', 'theone-core' ),
        	'menu_name'                  => __( 'Brands', 'theone-core' ),
        	'all_items'                  => __( 'All Brand', 'theone-core' ),
        	'parent_item'                => '',
        	'parent_item_colon'          => '',
        	'new_item_name'              => __( 'New Brand', 'theone-core' ),
        	'add_new_item'               => __( 'Add New Brand', 'theone-core' ),
        	'edit_item'                  => __( 'Edit Brand', 'theone-core' ),
        	'update_item'                => __( 'Update Brand', 'theone-core' ), 
        	'search_items'               => __( 'Search Brand', 'theone-core' ),
        	'add_or_remove_items'        => __( 'Add New or Delete Brand', 'theone-core' ),
        	'choose_from_most_used'      => __( 'Choose from most used', 'theone-core' ),
        	'not_found'                  => __( 'Brand not found', 'theone-core' ),
        ); 
        $args = array(
        	'labels'                     => $labels,
        	'hierarchical'               => true,
        	'public'                     => true,
        	'show_ui'                    => true,
        	'show_admin_column'          => true,
        	'show_in_nav_menus'          => true,
        	'show_tagcloud'              => false, 
            'hierarchical'               => true
        );
        register_taxonomy( 'product_brand', array( 'product' ), $args );  
        //flush_rewrite_rules();
    }
    add_action('init', 'theone_core_create_taxonomy_brand');
} 
