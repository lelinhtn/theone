<?php
/**
 * Custom Taxonomies
 * @package  Nella Core 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if (!function_exists('theone_core_create_taxonomy_member')) {
    
    function theone_core_create_taxonomy_member() {
        // Taxonomy 
        $labels = array(
        	'name'                       => _x( 'Member Categories', 'Member Categories', 'theone-core' ),
        	'singular_name'              => _x( 'Member Category', 'Member Category', 'theone-core' ),
        	'menu_name'                  => __( 'Member Categories', 'theone-core' ),
        	'all_items'                  => __( 'All Member Categoties', 'theone-core' ),
        	'parent_item'                => '',
        	'parent_item_colon'          => '',
        	'new_item_name'              => __( 'New Member Category', 'theone-core' ),
        	'add_new_item'               => __( 'Add New Member Category', 'theone-core' ),
        	'edit_item'                  => __( 'Edit Member Category', 'theone-core' ),
        	'update_item'                => __( 'Update Member Category', 'theone-core' ), 
        	'search_items'               => __( 'Search Member Category', 'theone-core' ),
        	'add_or_remove_items'        => __( 'Add New or Delete Member Category', 'theone-core' ),
        	'choose_from_most_used'      => __( 'Choose from most used', 'theone-core' ),
        	'not_found'                  => __( 'Member category not found', 'theone-core' ),
        ); 
        $args = array(
        	'labels'                     => $labels,
        	'hierarchical'               => true,
        	'public'                     => true,
        	'show_ui'                    => true,
        	'show_admin_column'          => true,
        	'show_in_nav_menus'          => true,
        	'show_tagcloud'              => false, 
            'hierarchical'               => true
        );
        register_taxonomy( 'member_cat', array( 'member' ), $args );  
        //flush_rewrite_rules();
    }
    add_action('init', 'theone_core_create_taxonomy_member');
} 
