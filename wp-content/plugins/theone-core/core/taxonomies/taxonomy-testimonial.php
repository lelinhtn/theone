<?php
/**
 * Custom Taxonomies
 * @package  Nella Core 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if (!function_exists('theone_core_create_taxonomy_testimonial')) {
    
    function theone_core_create_taxonomy_testimonial() {
        // Taxonomy 
        $labels = array(
        	'name'                       => _x( 'Testimonial Categories', 'Testimonial Categories', 'theone-core' ),
        	'singular_name'              => _x( 'Testimonial Category', 'Testimonial Category', 'theone-core' ),
        	'menu_name'                  => __( 'Testimonial Categories', 'theone-core' ),
        	'all_items'                  => __( 'All Testimonial Categoties', 'theone-core' ),
        	'parent_item'                => '',
        	'parent_item_colon'          => '',
        	'new_item_name'              => __( 'New Testimonial Category', 'theone-core' ),
        	'add_new_item'               => __( 'Add New Testimonial Category', 'theone-core' ),
        	'edit_item'                  => __( 'Edit Testimonial Category', 'theone-core' ),
        	'update_item'                => __( 'Update Testimonial Category', 'theone-core' ), 
        	'search_items'               => __( 'Search Testimonial Category', 'theone-core' ),
        	'add_or_remove_items'        => __( 'Add New or Delete Testimonial Category', 'theone-core' ),
        	'choose_from_most_used'      => __( 'Choose from most used', 'theone-core' ),
        	'not_found'                  => __( 'Testimonial category not found', 'theone-core' ),
        ); 
        $args = array(
        	'labels'                     => $labels,
        	'hierarchical'               => true,
        	'public'                     => true,
        	'show_ui'                    => true,
        	'show_admin_column'          => true,
        	'show_in_nav_menus'          => true,
        	'show_tagcloud'              => false, 
            'hierarchical'               => true
        );
        register_taxonomy( 'testimonial_cat', array( 'testimonial' ), $args );  
        //flush_rewrite_rules();
    }
    add_action('init', 'theone_core_create_taxonomy_testimonial');
} 
