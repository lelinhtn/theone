<?php
/**
 * Custom Taxonomies
 * @package  Nella Core 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if ( !function_exists( 'theone_core_create_taxonomy_animated_column' ) ) {
    
    function theone_core_create_taxonomy_animated_column() {
        // Taxonomy 
        $labels = array(
        	'name'                       => _x( 'Animated Column Categories', 'Animated Column Categories', 'theone-core' ),
        	'singular_name'              => _x( 'Animated Column Category', 'Animated Column Category', 'theone-core' ),
        	'menu_name'                  => __( 'Animated Column Categories', 'theone-core' ),
        	'all_items'                  => __( 'All A.Col Categoties', 'theone-core' ),
        	'parent_item'                => '',
        	'parent_item_colon'          => '',
        	'new_item_name'              => __( 'New Animated Column Category', 'theone-core' ),
        	'add_new_item'               => __( 'Add New Animated Column Category', 'theone-core' ),
        	'edit_item'                  => __( 'Edit Animated Column Category', 'theone-core' ),
        	'update_item'                => __( 'Update Animated Column Category', 'theone-core' ), 
        	'search_items'               => __( 'Search Animated Column Category', 'theone-core' ),
        	'add_or_remove_items'        => __( 'Add New or Delete Animated Column Category', 'theone-core' ),
        	'choose_from_most_used'      => __( 'Choose from most used', 'theone-core' ),
        	'not_found'                  => __( 'Animated Column category not found', 'theone-core' ),
        ); 
        $args = array(
        	'labels'                     => $labels,
        	'hierarchical'               => true,
        	'public'                     => true,
        	'show_ui'                    => true,
        	'show_admin_column'          => true,
        	'show_in_nav_menus'          => true,
        	'show_tagcloud'              => false, 
            'hierarchical'               => true
        );
        register_taxonomy( 'animated_column_cat', array( 'animated_column' ), $args );  
        //flush_rewrite_rules();
    }
    add_action('init', 'theone_core_create_taxonomy_animated_column');
} 
