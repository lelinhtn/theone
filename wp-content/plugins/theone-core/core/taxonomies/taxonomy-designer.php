<?php
/**
 * Custom Taxonomies
 * @package  Nella Core 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if (!function_exists('theone_core_create_taxonomy_designer')) {
    
    function theone_core_create_taxonomy_designer() {
        // Taxonomy 
        $labels = array(
        	'name'                       => _x( 'Designers', 'product brands', 'theone-core' ),
        	'singular_name'              => _x( 'Designer', 'product brand', 'theone-core' ),
        	'menu_name'                  => __( 'Designers', 'theone-core' ),
        	'all_items'                  => __( 'All Designers', 'theone-core' ),
        	'parent_item'                => '',
        	'parent_item_colon'          => '',
        	'new_item_name'              => __( 'New Designer', 'theone-core' ),
        	'add_new_item'               => __( 'Add New Designer', 'theone-core' ),
        	'edit_item'                  => __( 'Edit Designer', 'theone-core' ),
        	'update_item'                => __( 'Update Designer', 'theone-core' ), 
        	'search_items'               => __( 'Search Designer', 'theone-core' ),
        	'add_or_remove_items'        => __( 'Add New or Delete Designer', 'theone-core' ),
        	'choose_from_most_used'      => __( 'Choose from most used', 'theone-core' ),
        	'not_found'                  => __( 'Designer not found', 'theone-core' ),
        ); 
        $args = array(
        	'labels'                     => $labels,
        	'hierarchical'               => true,
        	'public'                     => true,
        	'show_ui'                    => true,
        	'show_admin_column'          => true,
        	'show_in_nav_menus'          => true,
        	'show_tagcloud'              => false, 
            'hierarchical'               => true
        );
        register_taxonomy( 'product_designer', array( 'product' ), $args );  
        //flush_rewrite_rules();
    }
    add_action('init', 'theone_core_create_taxonomy_designer');
} 
