<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

if( !function_exists( 'wp_get_current_user' ) ) {
    include( ABSPATH . 'wp-includes/pluggable.php' ); 
}

/** Nonce for security check **/
global $theone_core_nonce;
$theone_core_nonce = array(
    'ajax_nonce'  =>  wp_create_nonce( 'ovic-fh-core-ajax-nonce' )
);

/**
 * Load Redux Framework 
 */
if ( !class_exists( 'ReduxFramework' ) && file_exists( THEONECORE_LIBS . 'admin/reduxframework/ReduxCore/framework.php' ) ) {
    require_once( THEONECORE_LIBS . 'admin/reduxframework/ReduxCore/framework.php' );
}
 
/**
 * Load class icon font 
 */
if ( file_exists( THEONECORE_LIBS . 'class/ovic-class-icon-font.php' ) ) {
    require_once THEONECORE_LIBS . 'class/ovic-class-icon-font.php';
}

/**
 * Load Metaboxes Framework 
 */

if ( file_exists( THEONECORE_LIBS . 'admin/Tax-meta-class/Tax-meta-class.php' ) ) {
    require_once THEONECORE_LIBS . 'admin/Tax-meta-class/Tax-meta-class.php';
}

/**
 * Product cats images menu 
 **/
if ( file_exists( THEONECORE_LIBS . 'admin/ovic-product-cats-images.php' ) ) {
    require_once THEONECORE_LIBS . 'admin/ovic-product-cats-images.php';
}

if (!function_exists('theone_admin_load_font_awesome')) {
    
    function theone_admin_load_font_awesome() {
        //wp_deregister_style( 'font-awesome' );
        wp_register_style( 'ovic-font-awesome', THEONECORE_LIBS . 'font-awesome/css/font-awesome.min.css');
        wp_enqueue_script( 'ovic-font-awesome' );
    }
    add_action( 'admin_enqueue_scripts', 'theone_admin_load_font_awesome' );
}
