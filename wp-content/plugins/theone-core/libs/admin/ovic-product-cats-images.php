<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

function theone_register_product_cats_imgs_menu() {
    
    if ( class_exists( 'WooCommerce' ) ):
    
        add_menu_page( 
            __( 'Product Categories Images', 'theonefashion' ), 
            __( 'Product Cat Imgs', 'theonefashion' ),
            'manage_options', 
            'fh-product-categories-images', 
            'theone_product_cats_imgs', 
            'dashicons-welcome-widgets-menus',
            58
        );
    
    endif;
}
add_action( 'admin_menu', 'theone_register_product_cats_imgs_menu' );


function theone_product_cats_imgs() {
    
    $pro_cat_select_settings = array(
        'multiple'          =>  false,
        'id'                =>  'fh-procat-select',
        'name'              =>  '',
        'class'             =>  '',
        'first_option'      =>  false
    );
    
    ?>
    
    <div class="wrap ovic-fh-wrap">
        <h2><?php _e( 'Product Categories Images', 'theonefashion' ); ?></h2>
        
        <div class="ovic-fh-content-wrap">
            
            <table class="widefat fixed" cellspacing="0">
                <thead>
                    <tr>
                
                        <th id="columnname" width="250" class="manage-column column-columnname" scope="col"><?php _e( 'Product Category', 'theonefashion' ); ?></th>
                        <th id="columnname" class="manage-column column-columnname num" scope="col"><?php _e( 'Images', 'theonefashion' ); ?></th> 
                
                    </tr>
                </thead>
            
                <tfoot>
                <tr>
            
                        <th class="manage-column column-columnname" scope="col"></th>
                        <th class="manage-column column-columnname num" scope="col"></th>
            
                </tr>
                </tfoot>
            
                <tbody>
                    <tr class="alternate" valign="top"> 
                        <td class="column-columnname">
                            
                            <?php echo theone_procats_select( array(), $pro_cat_select_settings ); ?>
                            
                        </td>
                        <td class="column-columnname">
                            <div class="row-procat-images">
                                
                                <div id="ovic-fh-gallery-preview-wrap">
                                
                                </div>
                                
                                <input type="hidden" id="ovic-fh-gallery-field" value="" />
                                
                            </div><!-- .row-procat-images -->
                            
                            <div class="row-actions procat-img-actions-wrap">
                                <span><a class="ovic-fh-upload-procat-imgs ovic-fh-add-gallery-btn" href="#"><?php _e( 'Upload Images', 'theonefashion' ); ?></a> |</span>
                                <span class="trash"><a class="ovic-fh-del-all-procat-imgs" href="#"><?php _e( 'Delete All Images', 'theonefashion' ); ?></a></span>
                            </div>
                            
                            <div class="row-info">
                            
                            </div>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            
        </div><!-- .ovic-fh-content-wrap -->
    
    </div><!-- .wrap -->
    
    <?php
}